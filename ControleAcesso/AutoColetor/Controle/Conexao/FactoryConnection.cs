using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using AutoColetor;
namespace AutoColetor
{
   public class FactoryConnection
    {
        public SqlConnection getConnection()
       {
           DataTable dt = new DataTable("CONEXAO");
           DataRow dr;

           dt.Columns.Add("IP", typeof(string));
           dt.Columns.Add("USUARIO", typeof(string));
           dt.Columns.Add("SENHA", typeof(string));
           dt.Columns.Add("BANCO", typeof(string));

           dt.ReadXml("configuracoes.xml");
           dr = dt.Rows[0];

            string connectionString = "Server=" + dr.ItemArray[0] + "; Database=" + dr.ItemArray[3] + "; UID=" + dr.ItemArray[1] + ";PWD=" + dr.ItemArray[2] + "";
            return new SqlConnection(connectionString);
        }
     }
}
