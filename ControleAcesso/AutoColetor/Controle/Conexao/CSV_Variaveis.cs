using System;
public class CSV_Variaveis
{
    public CSV_Variaveis()
	{
       

	}

    static string AP_NomeBanner = "http://srv.interacao.com.br/F1Revestir2009/Layout/imagens/Topo.gif";
    static string AP_NomeEvento = "Forum 2009";
    static string AP_EmailEvento = "forum2009@interacao.com.br";
    static string AP_LinkNomeEvento = "http://srv.interacao.com.br/F1Revestir2009";
    //static string AP_Diretorio = "\\Workspace\\internet\\F1Revestir2009\\";
    //static string AP_DiretorioUpLoadVip = "\\Workspace\\internet\\F1Revestir2009\\";
    static string AP_Diretorio = "e:\\home\\exposicoes\\web\\2009\\F1Revestir2009\\";
    static string AP_DiretorioUpLoadVip = "e:\\home\\exposicoes\\web\\2009\\F1Revestir2009\\";

    static string AP_CodigoFicha = "";

    static DateTime AP_InicioCadastro;
    static DateTime AP_FimCadastro;


    public static string MI_ObterBanner()
    {
        return AP_NomeBanner;
    }

    public static string MI_ObterDiretorio(string prstIdFormulario)
    {
        string stHtml;
        stHtml = CSV_FuncoesGlobais.MS_LerHTML(AP_Diretorio +  prstIdFormulario);
        return stHtml;
    }
    public static string MI_ObterDiretorioRaiz(string prstIdFormulario)
    {
        string stHtml;
        stHtml = CSV_FuncoesGlobais.MS_LerHTML(prstIdFormulario);
        return stHtml;
    }
    public static string MI_ObterEnderecoDiretorio()
    {
        return AP_Diretorio;
    }
    public static string MI_ObterDiretorioVipUpLoad()
    {
        return AP_DiretorioUpLoadVip;
    }

    public static string MI_ObterLinkNomeEvento()
    {
        return AP_LinkNomeEvento;
    }

    public static string MI_ObterNomeEvento()
    {
        return AP_NomeEvento;
    }

    public static string MI_ObterEmailEvento()
    {
        return AP_EmailEvento;
    }

    public static void setCodFicha(string prstFicha)
    {
        AP_CodigoFicha = prstFicha;
    }

    public static string getCodFicha()
    {
        return AP_CodigoFicha;
    }
    
    public static void setInicioCadastro(DateTime prstData)
    {
        AP_InicioCadastro = prstData;
    }

    public static void setFimCadastro(DateTime prstData)
    {
        AP_FimCadastro = prstData;
    }

    public static DateTime getInicioCadastro()
    {
        return AP_InicioCadastro;
    }

    public static DateTime getFimCadastro()
    {
        return AP_FimCadastro;
    }
}
