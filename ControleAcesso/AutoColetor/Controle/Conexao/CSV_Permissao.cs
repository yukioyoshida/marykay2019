using System;
using System.Data;

public class CSV_Permissao
{
    public CSV_Permissao()
	{
	}

    static DataTable dtPermissao;

    public static void MI_MontaPermissao()
    {
        dtPermissao = new DataTable("Permissao");
        dtPermissao.Columns.Add("Valor", typeof(string));
        dtPermissao.ReadXml("Permissao.xml");
    }

    public static DataTable getTablePermissoes()
    {
        return dtPermissao;
    }
}
