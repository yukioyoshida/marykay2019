using System;
using System.Data;
using AutoColetor;

public abstract class CAB_CadastrarPedido : ICadastrarPedido
{
     public abstract void MS_Incluir(CTO_Pedido ITO_Pedido);
     public abstract void MS_Alterar(CTO_Pedido ITO_Pedido);
     public abstract void MS_Excluir(CTO_Pedido ITO_Pedido);
     public abstract CTO_Pedido MS_Obter(int pritObjRef);
     public abstract CTO_Pedido MS_Obter(string prstID);
     public abstract DataTable MS_ObterNovoPedido();
     public abstract CTO_Pedido MS_ObterPeloNumeroPedido(string stPedido);
     public abstract DataTable MS_ObterPedidosPorCliente(Int32 itIdCliente);
     public abstract DataTable MS_ObterQuantidadesPorItens(string stItem);
     public abstract DataTable MS_ObterQuantidadesPorItensPorCategoria(string stItem, string stCategoria);
     public abstract DataTable MS_ObterItensComprados(Int32 itIdVisitante);
     public abstract DataTable MS_ObterTodosOsPedidos();
     public abstract void MS_ConfirmaPagamento(string stNumeroPedido);
     public abstract void MS_CancelamentoPedido(string stNumeroPedido);
     public abstract void MS_AlterarValor(string stNumeroPedido, string stValor, string stObs);
     public abstract void MS_AlterarVencimento(string stNumeroPedido, string stValor, string stObs);
     public abstract void MS_CadastraFormaPagamento(string stNumeroPedido, string stFormaPagamento, string stNumeroParcelas);
}

