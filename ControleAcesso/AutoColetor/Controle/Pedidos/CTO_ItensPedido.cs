using System;
namespace AutoColetor 
{

    public class CTO_ItensPedido 
    {
          //  Declaracao dos atributos Puros
        int AP_ObjRef;
        int AP_IdPedido;
        string AP_Descricao;

        public CTO_ItensPedido () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public int getObjRef()
        {
            return AP_ObjRef;
        }
        public int getIdPedido()
        {
            return AP_IdPedido;
        }
        public string getDescricao()
        {
            return AP_Descricao;
        }
         //  Declaracao dos Metodos set
        public void setObjRef( int prObjRef)
        {
            AP_ObjRef = prObjRef;
        }
        public void setIdPedido( int prIdPedido)
        {
            AP_IdPedido = prIdPedido;
        }
        public void setDescricao( string prDescricao)
        {
            AP_Descricao = prDescricao;
        }
    }
}

