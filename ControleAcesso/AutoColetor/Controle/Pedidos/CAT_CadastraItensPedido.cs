using System;
using System.Data;
using AutoColetor;

public class CAT_CadastrarItensPedido : CAB_CadastrarItensPedido
{
     public CAT_CadastrarItensPedido()
     {
     }
     public override void MS_Incluir(CTO_ItensPedido ITO_ItensPedido)
     {
          DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
          IDAO_ItensPedido.MS_Incluir(ITO_ItensPedido);
     }
     public override void MS_Alterar(CTO_ItensPedido ITO_ItensPedido)
     {
         DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
         IDAO_ItensPedido.MS_Alterar(ITO_ItensPedido);
     }
     public override void MS_Excluir(CTO_ItensPedido ITO_ItensPedido)
     {
         DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
         IDAO_ItensPedido.MS_Excluir(ITO_ItensPedido);
     }
     public override CTO_ItensPedido MS_Obter(int pritObjRef)
     {
           DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
           return IDAO_ItensPedido.MS_Obter(pritObjRef);
     }
     public override CTO_ItensPedido MS_Obter(string prstID)
     {
          DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
          return IDAO_ItensPedido.MS_Obter(prstID);
     }
     public override CTO_ItensPedido MS_ObterPeloNumeroPedido(string prstID)
     {
         DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
         return IDAO_ItensPedido.MS_ObterPeloNumeroPedido(prstID);
     }
     //MS_ObterItensDoPedido
     public DataTable MS_ObterItensDoPedido(Int32 prstID)
     {
         DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
         return IDAO_ItensPedido.MS_ObterItensDoPedido(prstID);
     }
}

