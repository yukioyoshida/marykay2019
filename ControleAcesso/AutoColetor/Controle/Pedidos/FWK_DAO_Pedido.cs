using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using AutoColetor;
namespace AutoColetor
{
    public class FWK_DAO_Pedido : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                           "          ObjRef, " +
                           "          IdVisitante, " +
                           "          NumeroPedido, " +
                           "          Situacao, " +
                           "          DataPedido, " +
                           "          HoraPedido, " +
                           "          ValorPedido, " +
                           "          DataVencimento, " +
                           "          Observacao, " +
                           "          CPFCNPJSACADO, " +
                           "          NomeSacado, " +
                           "          EnderecoSacado, " +
                           "          NumeroSacado, " +
                           "          BairroSacado, " +
                           "          CidadeSacado, " +
                           "          UFSacado, " +
                           "          CEPSacado, " +
                           "          PaisSacado, " +
                           "          DataCancelamento, " +
                           "          FormaPagamento, " +
                           "          Categoria, " +
                           "          Convite, " +
                           "          ConviteEntregue ";

        public FWK_DAO_Pedido()
        {
        }

        public void MS_Incluir(CTO_Pedido ITO_Pedido)
        {
            int itObjRef = MS_NovoObjRef();
            MI_SetSql ("INSERT INTO tblPedido (" +
                       "          ObjRef, " +
                       "          IdVisitante, " +
                       "          NumeroPedido, " +
                       "          Situacao, " +
                       "          DataPedido, " +
                       "          HoraPedido, " +
                       "          ValorPedido, " +
                       "          DataVencimento, " +
                       "          Observacao, " +
                       "          CPFCNPJSACADO, " +
                       "          NomeSacado, " +
                       "          EnderecoSacado, " +
                       "          NumeroSacado, " +
                       "          BairroSacado, " +
                       "          CidadeSacado, " +
                       "          UFSacado, " +
                       "          CEPSacado, " +
                       "          PaisSacado, " +
                       "          DataCancelamento, " +
                       "          FormaPagamento, " +
                       "          Categoria, " +
                       "          Convite, " +
                       "          ConviteEntregue) " + 
                       " VALUES (" + 
                       "          ?ObjRef, " +
                       "          ?IdVisitante, " +
                       "          ?NumeroPedido, " +
                       "          ?Situacao, " +
                       "          ?DataPedido, " +
                       "          ?HoraPedido, " +
                       "          ?ValorPedido, " +
                       "          ?DataVencimento, " +
                       "          ?Observacao, " +
                       "          ?CPFCNPJSACADO, " +
                       "          ?NomeSacado, " +
                       "          ?EnderecoSacado, " +
                       "          ?NumeroSacado, " +
                       "          ?BairroSacado, " +
                       "          ?CidadeSacado, " +
                       "          ?UFSacado, " +
                       "          ?CEPSacado, " +
                       "          ?PaisSacado, " +
                       "          ?DataCancelamento, " +
                       "          ?FormaPagamento, " +
                       "          ?Categoria, " +
                       "          ?Convite, " +
                       "          ?ConviteEntregue) ");

            MI_AddParameters("?ObjRef", itObjRef);
            MI_AddParameters("?IdVisitante", ITO_Pedido.getIdVisitante());
            MI_AddParameters("?NumeroPedido", ITO_Pedido.getNumeroPedido());
            MI_AddParameters("?Situacao", ITO_Pedido.getSituacao());
            MI_AddParameters("?DataPedido", ITO_Pedido.getDataPedido());
            MI_AddParameters("?HoraPedido", ITO_Pedido.getHoraPedido());
            MI_AddParameters("?ValorPedido", ITO_Pedido.getValorPedido());
            MI_AddParameters("?DataVencimento", ITO_Pedido.getDataVencimento());
            MI_AddParameters("?Observacao", ITO_Pedido.getObservacao());
            MI_AddParameters("?CPFCNPJSACADO", ITO_Pedido.getCPFCNPJSACADO());
            MI_AddParameters("?NomeSacado", ITO_Pedido.getNomeSacado());
            MI_AddParameters("?EnderecoSacado", ITO_Pedido.getEnderecoSacado());
            MI_AddParameters("?NumeroSacado", ITO_Pedido.getNumeroSacado());
            MI_AddParameters("?BairroSacado", ITO_Pedido.getBairroSacado());
            MI_AddParameters("?CidadeSacado", ITO_Pedido.getCidadeSacado());
            MI_AddParameters("?UFSacado", ITO_Pedido.getUFSacado());
            MI_AddParameters("?CEPSacado", ITO_Pedido.getCEPSacado());
            MI_AddParameters("?PaisSacado", ITO_Pedido.getPaisSacado());
            MI_AddParameters("?DataCancelamento", ITO_Pedido.getDataCancelamento());
            MI_AddParameters("?FormaPagamento", ITO_Pedido.getFormaPagamento());
            MI_AddParameters("?Categoria", ITO_Pedido.getCategoria());
            MI_AddParameters("?Convite", ITO_Pedido.getConvite());
            MI_AddParameters("?ConviteEntregue", ITO_Pedido.getConviteEntregue());
            MI_AbrirConexao();
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_Alterar(CTO_Pedido ITO_Pedido)
        {
            MI_SetSql ("UPDATE tblPedido SET " +
                       "          ObjRef = ?ObjRef, " +
                       "          IdVisitante = ?IdVisitante, " +
                       "          NumeroPedido = ?NumeroPedido, " +
                       "          Situacao = ?Situacao, " +
                       "          DataPedido = ?DataPedido, " +
                       "          HoraPedido = ?HoraPedido, " +
                       "          ValorPedido = ?ValorPedido, " +
                       "          DataVencimento = ?DataVencimento, " +
                       "          Observacao = ?Observacao, " +
                       "          CPFCNPJSACADO = ?CPFCNPJSACADO, " +
                       "          NomeSacado = ?NomeSacado, " +
                       "          EnderecoSacado = ?EnderecoSacado, " +
                       "          NumeroSacado = ?NumeroSacado, " +
                       "          BairroSacado = ?BairroSacado, " +
                       "          CidadeSacado = ?CidadeSacado, " +
                       "          UFSacado = ?UFSacado, " +
                       "          CEPSacado = ?CEPSacado, " +
                       "          PaisSacado = ?PaisSacado, " +
                       "          DataCancelamento = ?DataCancelamento, " +
                       "          FormaPagamento = ?FormaPagamento, " +
                       "          Categoria = ?Categoria, " +
                       "          Convite = ?Convite, " +
                       "          ConviteEntregue = ?ConviteEntregue " + 
                       "WHERE ObjRef = ?ObjRef ");
            MI_AddParameters("?ObjRef", ITO_Pedido.getObjRef());
            MI_AddParameters("?IdVisitante", ITO_Pedido.getIdVisitante());
            MI_AddParameters("?NumeroPedido", ITO_Pedido.getNumeroPedido());
            MI_AddParameters("?Situacao", ITO_Pedido.getSituacao());
            MI_AddParameters("?DataPedido", ITO_Pedido.getDataPedido());
            MI_AddParameters("?HoraPedido", ITO_Pedido.getHoraPedido());
            MI_AddParameters("?ValorPedido", ITO_Pedido.getValorPedido());
            MI_AddParameters("?DataVencimento", ITO_Pedido.getDataVencimento());
            MI_AddParameters("?Observacao", ITO_Pedido.getObservacao());
            MI_AddParameters("?CPFCNPJSACADO", ITO_Pedido.getCPFCNPJSACADO());
            MI_AddParameters("?NomeSacado", ITO_Pedido.getNomeSacado());
            MI_AddParameters("?EnderecoSacado", ITO_Pedido.getEnderecoSacado());
            MI_AddParameters("?NumeroSacado", ITO_Pedido.getNumeroSacado());
            MI_AddParameters("?BairroSacado", ITO_Pedido.getBairroSacado());
            MI_AddParameters("?CidadeSacado", ITO_Pedido.getCidadeSacado());
            MI_AddParameters("?UFSacado", ITO_Pedido.getUFSacado());
            MI_AddParameters("?CEPSacado", ITO_Pedido.getCEPSacado());
            MI_AddParameters("?PaisSacado", ITO_Pedido.getPaisSacado());
            MI_AddParameters("?DataCancelamento", ITO_Pedido.getDataCancelamento());
            MI_AddParameters("?FormaPagamento", ITO_Pedido.getFormaPagamento());
            MI_AddParameters("?Categoria", ITO_Pedido.getCategoria());
            MI_AddParameters("?Convite", ITO_Pedido.getConvite());
            MI_AddParameters("?ConviteEntregue", ITO_Pedido.getConviteEntregue());

            MI_AbrirConexao();
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_Excluir(CTO_Pedido ITO_Pedido)
        {
            MI_SetSql ("DELETE FROM tblPedido WHERE ObjRef = ?ObjRef");
            MI_AddParameters("?ObjRef", ITO_Pedido.getObjRef());

            MI_AbrirConexao();
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public CTO_Pedido MS_Obter(int pritObjRef)
        {
            CTO_Pedido ITO_Pedido = new CTO_Pedido();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          IdVisitante, " +
                       "          NumeroPedido, " +
                       "          Situacao, " +
                       "          DataPedido, " +
                       "          HoraPedido, " +
                       "          ValorPedido, " +
                       "          DataVencimento, " +
                       "          Observacao, " +
                       "          CPFCNPJSACADO, " +
                       "          NomeSacado, " +
                       "          EnderecoSacado, " +
                       "          NumeroSacado, " +
                       "          BairroSacado, " +
                       "          CidadeSacado, " +
                       "          UFSacado, " +
                       "          CEPSacado, " +
                       "          PaisSacado, " +
                       "          DataCancelamento, " +
                       "          FormaPagamento, " +
                       "          Categoria, " +
                       "          Convite, " +
                       "          ConviteEntregue " + 
                       "FROM tblPedido " + 
                       "WHERE Objref = ?ObjRef ");
            MI_AddParameters("?Objref", pritObjRef);
            MI_AbrirConexao();
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Pedido = null;
            else
                ITO_Pedido = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Pedido;
        }

        public CTO_Pedido MI_DataSetToEntidade()
        {
            CTO_Pedido ITO_Pedido = new CTO_Pedido();            
            try
            {
                ITO_Pedido.setObjRef(AP_DataReader.GetInt32(0));
            }
            catch
            {
                ITO_Pedido.setObjRef(0);
            }
            try
            {
                ITO_Pedido.setIdVisitante(AP_DataReader.GetInt32(1));
            }
            catch
            {
                ITO_Pedido.setIdVisitante(0);
            }
            try
            {
                ITO_Pedido.setNumeroPedido(AP_DataReader.GetString(2));
            }
            catch
            {
                ITO_Pedido.setNumeroPedido("");
            }
            try
            {
                ITO_Pedido.setSituacao(AP_DataReader.GetString(3));
            }
            catch
            {
                ITO_Pedido.setSituacao("");
            }
            try
            {
            ITO_Pedido.setDataPedido(AP_DataReader.GetDateTime(4));
            }
            catch
            {
            }
            try
            {
                ITO_Pedido.setHoraPedido(AP_DataReader.GetString(5));
            }
            catch
            {
                ITO_Pedido.setHoraPedido("");
            }
            try
            {
                ITO_Pedido.setValorPedido(AP_DataReader.GetString(6));
            }
            catch
            {
                ITO_Pedido.setValorPedido("");
            }
            try
            {
            ITO_Pedido.setDataVencimento(AP_DataReader.GetDateTime(7));
            }
            catch
            {
            }
            try
            {
            ITO_Pedido.setObservacao(AP_DataReader.GetDateTime(8));
            }
            catch
            {
            }
            try
            {
                ITO_Pedido.setCPFCNPJSACADO(AP_DataReader.GetString(9));
            }
            catch
            {
                ITO_Pedido.setCPFCNPJSACADO("");
            }
            try
            {
                ITO_Pedido.setNomeSacado(AP_DataReader.GetString(10));
            }
            catch
            {
                ITO_Pedido.setNomeSacado("");
            }
            try
            {
                ITO_Pedido.setEnderecoSacado(AP_DataReader.GetString(11));
            }
            catch
            {
                ITO_Pedido.setEnderecoSacado("");
            }
            try
            {
                ITO_Pedido.setNumeroSacado(AP_DataReader.GetString(12));
            }
            catch
            {
                ITO_Pedido.setNumeroSacado("");
            }
            try
            {
                ITO_Pedido.setBairroSacado(AP_DataReader.GetString(13));
            }
            catch
            {
                ITO_Pedido.setBairroSacado("");
            }
            try
            {
                ITO_Pedido.setCidadeSacado(AP_DataReader.GetString(14));
            }
            catch
            {
                ITO_Pedido.setCidadeSacado("");
            }
            try
            {
                ITO_Pedido.setUFSacado(AP_DataReader.GetString(15));
            }
            catch
            {
                ITO_Pedido.setUFSacado("");
            }
            try
            {
                ITO_Pedido.setCEPSacado(AP_DataReader.GetString(16));
            }
            catch
            {
                ITO_Pedido.setCEPSacado("");
            }
            try
            {
                ITO_Pedido.setPaisSacado(AP_DataReader.GetString(17));
            }
            catch
            {
                ITO_Pedido.setPaisSacado("");
            }
            try
            {
            ITO_Pedido.setDataCancelamento(AP_DataReader.GetDateTime(18));
            }
            catch
            {
            }
            try
            {
                ITO_Pedido.setFormaPagamento(AP_DataReader.GetString(19));
            }
            catch
            {
                ITO_Pedido.setFormaPagamento("");
            }
            try
            {
                ITO_Pedido.setCategoria(AP_DataReader.GetString(20));
            }
            catch
            {
                ITO_Pedido.setCategoria("");
            }
            try
            {
                ITO_Pedido.setConvite(AP_DataReader.GetString(21));
            }
            catch
            {
                ITO_Pedido.setConvite("");
            }
            try
            {
                ITO_Pedido.setConviteEntregue(AP_DataReader.GetString(22));
            }
            catch
            {
                ITO_Pedido.setConviteEntregue("");
            }
            return ITO_Pedido;
        }

        public CTO_Pedido MS_ObterTodos()
        {
            CTO_Pedido ITO_Pedido = new CTO_Pedido();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          IdVisitante, " +
                       "          NumeroPedido, " +
                       "          Situacao, " +
                       "          DataPedido, " +
                       "          HoraPedido, " +
                       "          ValorPedido, " +
                       "          DataVencimento, " +
                       "          Observacao, " +
                       "          CPFCNPJSACADO, " +
                       "          NomeSacado, " +
                       "          EnderecoSacado, " +
                       "          NumeroSacado, " +
                       "          BairroSacado, " +
                       "          CidadeSacado, " +
                       "          UFSacado, " +
                       "          CEPSacado, " +
                       "          PaisSacado, " +
                       "          DataCancelamento, " +
                       "          FormaPagamento, " +
                       "          Categoria, " +
                       "          Convite, " +
                       "          ConviteEntregue " + 
                       "FROM tblPedido " );
            MI_AbrirConexao();
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Pedido = null;
            else
                ITO_Pedido = MI_DataSetToEntidade();
            MI_FecharConexao();
            return ITO_Pedido;
        }

        public CTO_Pedido MS_ObterProximo()
        {
            CTO_Pedido ITO_Pedido = new CTO_Pedido();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_Pedido = null;
            else
                ITO_Pedido = MI_DataSetToEntidade();
                return ITO_Pedido;
        }

        public int MS_NovoObjRef()
        {
            int itObjRef;
            MI_SetSql("Select Max(ObjRef) from tblPedido");
            MI_AbrirConexao();
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 1;
            }
            else
            {
                itObjRef = AP_DataReader.GetInt32(0);
                itObjRef++;
            }
            MI_FecharConexao();
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblPedido");
            return MI_ExecuteDataSet();
        }

    }
}

