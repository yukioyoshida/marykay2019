using System;
using System.Data;
using AutoColetor;
namespace AutoColetor
{
    public class DAO_ItensPedido : FWK_DAO_ItensPedido
    {
        public DAO_ItensPedido()
        {
        }

        public CTO_ItensPedido MS_Obter(string prstID)
        {
            CTO_ItensPedido ITO_ItensPedido = new CTO_ItensPedido();
            MI_SetSql (AP_SelectALL + 
                       "FROM tblItensPedido " + 
                       "WHERE ID = @ID ");
            MI_AddParameters("@ID", prstID);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_ItensPedido = null;
            else
                ITO_ItensPedido = MI_DataSetToEntidade();
            return ITO_ItensPedido;
        }

        public CTO_ItensPedido MS_ObterPeloNumeroPedido(string prstID)
        {
            CTO_ItensPedido ITO_ItensPedido = new CTO_ItensPedido();
            MI_SetSql(AP_SelectALL +
                       "FROM tblItensPedido  " +
                       "WHERE IdPedido = ?ID ");
            MI_AddParameters("?ID", prstID);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_ItensPedido = null;
            else
                ITO_ItensPedido = MI_DataSetToEntidade();
            return ITO_ItensPedido;
        }

        public DataTable MS_ObterItensDoPedido(Int32 prstID)
        {
            MI_SetSql("SELECT * FROM tblItensPedido WHERE " +
                       "IdPedido = ?ID                    ");
            MI_AddParameters("?ID", prstID);
            return MI_ExecuteDataSet();
        }
    }
}

