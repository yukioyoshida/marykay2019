using System;
using System.Data;
using AutoColetor;

public class CAT_CadastrarPedido : CAB_CadastrarPedido
{
     public CAT_CadastrarPedido()
     {
     }
     public override void MS_Incluir(CTO_Pedido ITO_Pedido)
     {
          DAO_Pedido IDAO_Pedido = new DAO_Pedido();
          IDAO_Pedido.MS_Incluir(ITO_Pedido);
     }
     public override void MS_Alterar(CTO_Pedido ITO_Pedido)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         IDAO_Pedido.MS_Alterar(ITO_Pedido);
     }
     public override void MS_Excluir(CTO_Pedido ITO_Pedido)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         IDAO_Pedido.MS_Excluir(ITO_Pedido);
     }
     public override CTO_Pedido MS_Obter(int pritObjRef)
     {
           DAO_Pedido IDAO_Pedido = new DAO_Pedido();
           return IDAO_Pedido.MS_Obter(pritObjRef);
     }
     public override CTO_Pedido MS_Obter(string prstID)
     {
          DAO_Pedido IDAO_Pedido = new DAO_Pedido();
          return IDAO_Pedido.MS_Obter(prstID);
     }
     public override DataTable MS_ObterNovoPedido()
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterNovoPedido();
     }
     public override CTO_Pedido MS_ObterPeloNumeroPedido(string stPedido)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterPeloNumeroPedido(stPedido);
     }

     public override DataTable MS_ObterPedidosPorCliente(Int32 itIdCliente)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterPedidosPorCliente(itIdCliente);
     }

     public override DataTable MS_ObterQuantidadesPorItens(string stItem)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterQuantidadesPorItens(stItem);
     }

     public override DataTable MS_ObterQuantidadesPorItensPorCategoria(string stItem, string stCategoria)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterQuantidadesPorItensPorCategoria(stItem, stCategoria);
     }

     public override DataTable MS_ObterItensComprados(Int32 itIdVisitante)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterItensComprados(itIdVisitante);
     }

     public override DataTable MS_ObterTodosOsPedidos()
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterTodosOsPedidos();
     }

     public override void MS_ConfirmaPagamento(string stNumeroPedido)
     { 
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         IDAO_Pedido.MS_ConfirmaPagamento(stNumeroPedido);
     }

    public override void MS_CancelamentoPedido(string stNumeroPedido)
    {
        DAO_Pedido IDAO_Pedido = new DAO_Pedido();
        IDAO_Pedido.MS_CancelamentoPedido(stNumeroPedido);
    }

    public override void MS_AlterarValor(string stNumeroPedido, string stValor, string stObs)
    {
        DAO_Pedido IDAO_Pedido = new DAO_Pedido();
        IDAO_Pedido.MS_AlterarValor(stNumeroPedido, stValor, stObs);
    }
    
    public override void MS_AlterarVencimento(string stNumeroPedido, string stValor, string stObs)
    {
        DAO_Pedido IDAO_Pedido = new DAO_Pedido();
        IDAO_Pedido.MS_AlterarVencimento(stNumeroPedido, stValor, stObs);
    }

    public override void MS_CadastraFormaPagamento(string stNumeroPedido, string stFormaPagamento, string stNumeroParcelas)
    {
        DAO_Pedido IDAO_Pedido = new DAO_Pedido();
        IDAO_Pedido.MS_CadastraFormaPagamento(stNumeroPedido, stFormaPagamento, stNumeroParcelas);
    }
    
    public void MS_ConfirmaRecebimentoConvite(string stUsuario, string stPedido)
    {
        DAO_Pedido IDAO_Pedido = new DAO_Pedido();
        IDAO_Pedido.MS_ConfirmaRecebimentoConvite(stUsuario, stPedido);
    }
    public void MS_CadastraTipoPagamento(string stNumeroPedido, string stValor, string stFormaPagamento)
    {
        DAO_Pedido IDAO_Pedido = new DAO_Pedido();
        IDAO_Pedido.MS_CadastraTipoPagamento(stNumeroPedido, stValor, stFormaPagamento);
    }
    
}

