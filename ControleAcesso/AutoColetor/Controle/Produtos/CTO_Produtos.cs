using System;
namespace AutoColetor 
{

    public class CTO_Produtos 
    {
          //  Declaracao dos atributos Puros
        int AP_ObjRef;
        string AP_Codigo;
        string AP_Descricao;

        public CTO_Produtos () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public int getObjRef()
        {
            return AP_ObjRef;
        }
        public string getCodigo()
        {
            return AP_Codigo;
        }
        public string getDescricao()
        {
            return AP_Descricao;
        }
         //  Declaracao dos Metodos set
        public void setObjRef( int prObjRef)
        {
            AP_ObjRef = prObjRef;
        }
        public void setCodigo( string prCodigo)
        {
            AP_Codigo = prCodigo;
        }
        public void setDescricao( string prDescricao)
        {
            AP_Descricao = prDescricao;
        }
    }
}

