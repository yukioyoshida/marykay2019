using System;
using System.Data;
using AutoColetor;

public class CAT_CadastrarProdutos : CAB_CadastrarProdutos
{
     public CAT_CadastrarProdutos()
     {
     }
     public override void MS_Incluir(CTO_Produtos ITO_Produtos)
     {
          DAO_Produtos IDAO_Produtos = new DAO_Produtos();
          IDAO_Produtos.MS_Incluir(ITO_Produtos);
     }
     public override void MS_Alterar(CTO_Produtos ITO_Produtos)
     {
         DAO_Produtos IDAO_Produtos = new DAO_Produtos();
         IDAO_Produtos.MS_Alterar(ITO_Produtos);
     }
     public override void MS_Excluir(CTO_Produtos ITO_Produtos)
     {
         DAO_Produtos IDAO_Produtos = new DAO_Produtos();
         IDAO_Produtos.MS_Excluir(ITO_Produtos);
     }
     public override CTO_Produtos MS_Obter(int pritObjRef)
     {
           DAO_Produtos IDAO_Produtos = new DAO_Produtos();
           return IDAO_Produtos.MS_Obter(pritObjRef);
     }
     public override CTO_Produtos MS_Obter(string prstID)
     {
          DAO_Produtos IDAO_Produtos = new DAO_Produtos();
          return IDAO_Produtos.MS_Obter(prstID);
     }

     public override CTO_Produtos MS_ObterProdutoPeloCodigo(string prstID)
     {
         DAO_Produtos IDAO_Produtos = new DAO_Produtos();
         return IDAO_Produtos.MS_ObterProdutoPeloCodigo(prstID);
     }
     public  CTO_Produtos MS_ObterDescricaoRegraPeloCodigo(string prstID)
     {
         DAO_Produtos IDAO_Produtos = new DAO_Produtos();
         return IDAO_Produtos.MS_ObterDescricaoRegraPeloCodigo(prstID);
     }
    
     public DataTable MS_ObterTodosOsProdutos()
     {
         DAO_Produtos IDAO_Produtos = new DAO_Produtos();
         return IDAO_Produtos.MS_ObterTodosOsProdutos();
     }

      public DataTable MS_ObterProdutoPeloCodigoDataSet(string prstID)
      {
          DAO_Produtos IDAO_Produtos = new DAO_Produtos();
          return IDAO_Produtos.MS_ObterProdutoPeloCodigoDataSet(prstID);
      }
}

