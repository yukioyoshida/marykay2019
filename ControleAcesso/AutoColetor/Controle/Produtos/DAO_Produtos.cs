using System;
using System.Data;
using AutoColetor;
namespace AutoColetor
{
    public class DAO_Produtos : FWK_DAO_Produtos
    {
        public DAO_Produtos()
        {
        }

        public CTO_Produtos MS_Obter(string prstID)
        {
            CTO_Produtos ITO_Produtos = new CTO_Produtos();
            MI_SetSql (AP_SelectALL + 
                       "FROM tblregras " + 
                       "WHERE cdProduto = @ID ");
            MI_AddParameters("@ID", prstID);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Produtos = null;
            else
                ITO_Produtos = MI_DataSetToEntidade();
            return ITO_Produtos;
        }

        public CTO_Produtos MS_ObterProdutoPeloCodigo(string prstID)
        {
            CTO_Produtos ITO_Produtos = new CTO_Produtos();
            MI_SetSql(AP_SelectALL +
                       "FROM tblProduto " +
                       "WHERE cdProduto = @ID ");
            MI_AddParameters("@ID", prstID);
            MI_AbrirConexao();
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Produtos = null;
            else
                ITO_Produtos = MI_DataSetToEntidade();
            MI_FecharConexao();
            return ITO_Produtos;
        }
        public CTO_Produtos MS_ObterDescricaoRegraPeloCodigo(string prstID)
        {
            CTO_Produtos ITO_Produtos = new CTO_Produtos();
            MI_SetSql(AP_SelectALL +
                       "FROM tblregras " +
                       "WHERE cdProduto = @ID ");
            MI_AddParameters("@ID", prstID);
            MI_AbrirConexao();
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Produtos = null;
            else
                ITO_Produtos = MI_DataSetToEntidade();
            MI_FecharConexao();
            return ITO_Produtos;
        }

        public DataTable MS_ObterTodosOsProdutos()
        {
            MI_SetSql("SELECT * FROM tblregras Order by dsProduto, cdProduto");
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterProdutoPeloCodigoDataSet(string prstID)
        {
            MI_SetSql("SELECT * " +
                       "FROM tblregras " +
                       "WHERE cdProduto = @ID ");
            MI_AddParameters("@ID", prstID);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterRoteiroPorId(string prCodigo)
        {
            MI_SetSql("SELECT * FROM tblRoteiro WHERE cdRoteiro = @ID");
            MI_AddParameters("@ID", prCodigo);
            return MI_ExecuteDataSet();
        }
    }
}

