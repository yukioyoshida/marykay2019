using System;
using System.Data;
using AutoColetor;

public class CAT_CadastrarVisitante : CAB_CadastrarVisitante
{
     public CAT_CadastrarVisitante()
     {
     }
     public override void MS_Incluir(CTO_Visitante ITO_Visitante)
     {
          DAO_Visitante IDAO_Visitante = new DAO_Visitante();
          IDAO_Visitante.MS_Incluir(ITO_Visitante);
     }
     public override void MS_Alterar(CTO_Visitante ITO_Visitante)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         IDAO_Visitante.MS_Alterar(ITO_Visitante);
     }
     public override void MS_Excluir(CTO_Visitante ITO_Visitante)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         IDAO_Visitante.MS_Excluir(ITO_Visitante);
     }
     public override CTO_Visitante MS_Obter(int pritObjRef)
     {
           DAO_Visitante IDAO_Visitante = new DAO_Visitante();
           return IDAO_Visitante.MS_Obter(pritObjRef);
     }

     public override CTO_Visitante MS_Obter1()
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_Obter1();
     }

     public override CTO_Visitante MS_ObterPorCPF(string prstCPF)
     {
           DAO_Visitante IDAO_Visitante = new DAO_Visitante();
           return IDAO_Visitante.MS_ObterPorCPF(prstCPF);
     }

     public override DataTable MS_ObterPorCPFTabela(string prstCPF)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_ObterPorCPFTabela(prstCPF);
     }

     public override CTO_Visitante MS_ObterPorNome(string prstNome)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_ObterPorNome(prstNome);
     }
     
     public override DataTable MS_ObterPorNomeTabela(string prstNome)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_ObterPorNomeTabela(prstNome);
     }

     public override CTO_Visitante MS_ObterPorCodigo(string prstCodigo)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_ObterPorCodigo(prstCodigo);
     }

     public override CTO_Visitante MS_ObterPorEmail(string prstEmail)
     {
           DAO_Visitante IDAO_Visitante = new DAO_Visitante();
           return IDAO_Visitante.MS_ObterPorEmail(prstEmail);
     }
     public override DataTable MS_LocalizarRegistroPorFiltro(string prstCriterio, string prstAtributo)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_LocalizarRegistroPorFiltro(prstCriterio,prstAtributo);
     }
     
    public override string MS_GerarCodigoBarras()
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_GerarCodigoBarras();
    }
    public override DataTable MS_ObterRegistro(string prstPalavra, string prContendo, string stTipoBusca)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterRegistro(prstPalavra, prContendo, stTipoBusca);
    }
    
    public override void MS_ConfirmaImpressao(Int64 prstCodigo)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_ConfirmaImpressao(prstCodigo);
    }

    public void MS_ConfirmaImpressaoF1(Int64 prstCodigo)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_ConfirmaImpressaoF1(prstCodigo);
    }
    public void MS_ConfirmaImpressaoF2(Int64 prstCodigo)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_ConfirmaImpressaoF2(prstCodigo);
    }
    public void MS_ConfirmaImpressaoF3(Int64 prstCodigo)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_ConfirmaImpressaoF3(prstCodigo);
    }

    public void MS_ConfirmaImpressaoF4(Int64 prstCodigo)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_ConfirmaImpressaoF4(prstCodigo);
    }

    public void MS_ConfirmaImpressaoF5(Int64 prstCodigo)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_ConfirmaImpressaoF5(prstCodigo);
    }


    public override DataTable MS_ObterCampos()
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterCampos();
    }
    
    public override DataTable MS_ObterRegistroGrid(string prstChave)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterRegistroGrid(prstChave);
    }
    public DataTable MS_ObterRegistroGrid(string prstChave, string stCampoCodigo)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterRegistroGrid(prstChave, stCampoCodigo);
    }
    
    public DataTable MI_GerarNumeroInscricao(string stTipoForum)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MI_GerarNumeroInscricao(stTipoForum);
    }

    public void MS_PresenteEvento(string prstCodigo, string stDia)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_PresenteEvento(prstCodigo, stDia);
    }

    public DataTable MS_ObterRespostaSaida(string prstCodigoBarras)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterRespostaSaida(prstCodigoBarras);
    }

    public void MS_SalvarPerguntas(string prstCodigoBarras, string prstPergunta1, string prstPergunta2)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_SalvarPerguntas(prstCodigoBarras, prstPergunta1, prstPergunta2);
    }

    public DataTable MS_ObterQuestionario()
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterQuestionario();
    }
    public DataTable MS_ObterPorCodigoDat(string prstCodigo)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterPorCodigoDat(prstCodigo);
    }

    public DataTable MS_ObterCodigoBarrasColetor(string prCodigoBarras)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterCodigoBarrasColetor(prCodigoBarras);
    }

    public DataTable MS_ObterNomeProduto(string CdProduto)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterNomeProduto(CdProduto);
    }
}

