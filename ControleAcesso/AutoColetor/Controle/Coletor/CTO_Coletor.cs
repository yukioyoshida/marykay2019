using System;
namespace AutoColetor 
{

    public class CTO_Coletor 
    {
          //  Declaracao dos atributos Puros
        int AP_ObjRef;
        string AP_Coletor = "";
        string AP_Codigo = "";
        string AP_Fluxo = "";
        DateTime AP_DataEntrada;
        string AP_HoraEntrada = "";
        DateTime AP_DataSaida;
        string AP_HoraSaida = "";
        string AP_Permanencia = "";
        string AP_AutorizadoPor = "";
        string AP_EntradaConcedida = "";

        public CTO_Coletor () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public int getObjRef()
        {
            return AP_ObjRef;
        }
        public string getColetor()
        {
            return AP_Coletor;
        }
        public string getCodigo()
        {
            return AP_Codigo;
        }
        public string getFluxo()
        {
            return AP_Fluxo;
        }
        public DateTime getDataEntrada()
        {
            return AP_DataEntrada;
        }
        public string getHoraEntrada()
        {
            return AP_HoraEntrada;
        }
        public DateTime getDataSaida()
        {
            return AP_DataSaida;
        }
        public string getHoraSaida()
        {
            return AP_HoraSaida;
        }
        public string getPermanencia()
        {
            return AP_Permanencia;
        }
        public string getAutorizadoPor()
        {
            return AP_AutorizadoPor;
        }
        public string getEntradaConcedida()
        {
            return AP_EntradaConcedida;
        }
         //  Declaracao dos Metodos set
        public void setObjRef( int prObjRef)
        {
            AP_ObjRef = prObjRef;
        }
        public void setColetor( string prColetor)
        {
            AP_Coletor = prColetor;
        }
        public void setCodigo( string prCodigo)
        {
            AP_Codigo = prCodigo;
        }
        public void setFluxo( string prFluxo)
        {
            AP_Fluxo = prFluxo;
        }
        public void setDataEntrada( DateTime prDataEntrada)
        {
            AP_DataEntrada = prDataEntrada;
        }
        public void setHoraEntrada( string prHoraEntrada)
        {
            AP_HoraEntrada = prHoraEntrada;
        }
        public void setDataSaida( DateTime prDataSaida)
        {
            AP_DataSaida = prDataSaida;
        }
        public void setHoraSaida( string prHoraSaida)
        {
            AP_HoraSaida = prHoraSaida;
        }
        public void setPermanencia( string prPermanencia)
        {
            AP_Permanencia = prPermanencia;
        }
        public void setAutorizadoPor(string prAutorizado)
        {
            AP_AutorizadoPor = prAutorizado;
        }
        public void setEntradaConcedida(string prstEntradaConcedida)
        {
            AP_EntradaConcedida = prstEntradaConcedida;
        }
    }
}

