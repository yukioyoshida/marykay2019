using System.Data;
using System;
using AutoColetor;
namespace AutoColetor
{
    public class DAO_Coletor : FWK_DAO_Coletor
    {
        public DAO_Coletor()
        {
        }

        public CTO_Coletor MS_Obter(string prstID)
        {
            CTO_Coletor ITO_Coletor = new CTO_Coletor();
            MI_SetSql (AP_SelectALL + 
                       "FROM tblColetorFeira " + 
                       "WHERE ObjRef = ?ID ");
            MI_AddParameters("?ID", prstID);
            MI_AbrirConexao();
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Coletor = null;
            else
                ITO_Coletor = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Coletor;
        }

        public CTO_Coletor MS_ObterPeloCodigo(string prstID)
        {
            CTO_Coletor ITO_Coletor = new CTO_Coletor();
            MI_SetSql(AP_SelectALL +
                       "FROM tblColetorFeira        " +
                       "WHERE Codigo = ?ID AND      " + 
                       "DataEntrada = curdate() AND " +
                       "Permanencia is null         ");
            MI_AddParameters("?ID", prstID);
            MI_AbrirConexao();
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Coletor = null;
            else
                ITO_Coletor = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Coletor;
        }
        
        public CTO_Coletor MS_ObterPeloCodigoETerminal(string prstID, string stNumeroTerminal)
        {
            CTO_Coletor ITO_Coletor = new CTO_Coletor();
            MI_SetSql(AP_SelectALL +
                       "FROM tblColetorFeira        " +
                       "WHERE Codigo = @ID AND      " +
                       "DataEntrada = CAST(GETDATE() AS DATE) AND " +
                       "Coletor = @Coletor         ");
            MI_AddParameters("@ID", prstID);
            MI_AddParameters("@Coletor", stNumeroTerminal);
            MI_AbrirConexao();
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Coletor = null;
            else
                ITO_Coletor = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Coletor;
        }

        public DataTable MS_ObterPeloCodigoChave(string prstID)
        {
            MI_SetSql("SELECT * FROM tblCodigoChave WHERE " +
                      "CodigoAutorizacao = ?ID            ");
            MI_AddParameters("?ID", prstID);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterTipoInscricao(string prCodigo)
        {
            MI_SetSql("SELECT TipodeInscricao, Consultora FROM tblInscricao WHERE CodigoBarras = @ID");
            MI_AddParameters("@ID", prCodigo);
            return MI_ExecuteDataSet();
        }
    }
}

