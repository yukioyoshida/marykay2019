using System;
using AutoColetor;

public interface ICadastrarColetor
{
    void MS_Incluir(CTO_Coletor ITO_Coletor, string prProduto);
     void MS_Alterar(CTO_Coletor ITO_Coletor);
     void MS_Excluir(CTO_Coletor ITO_Coletor);
     CTO_Coletor MS_Obter(int pritObjRef);
     CTO_Coletor MS_Obter(string prstID);
     CTO_Coletor MS_ObterPeloCodigo(string prstID);
}

