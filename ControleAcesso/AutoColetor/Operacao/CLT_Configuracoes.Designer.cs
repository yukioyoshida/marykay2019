﻿namespace AutoColetor
{
    partial class CLT_Configuracoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CLT_Configuracoes));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cmbObterDia = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.imgBotSair = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.edtTerminal = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbFluxo = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gdwPermissao = new System.Windows.Forms.DataGridView();
            this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dEL = new System.Windows.Forms.DataGridViewImageColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbPermissao = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btInserir = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.chkRealizaInsercao = new System.Windows.Forms.CheckBox();
            this.chkDuplicidade = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btInserirAlertaEspecial = new System.Windows.Forms.Button();
            this.cmbAlertaEspecial = new System.Windows.Forms.ComboBox();
            this.edtAlertaEspecial = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgBotSair)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gdwPermissao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbObterDia
            // 
            this.cmbObterDia.FormattingEnabled = true;
            this.cmbObterDia.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05"});
            this.cmbObterDia.Location = new System.Drawing.Point(20, 43);
            this.cmbObterDia.Margin = new System.Windows.Forms.Padding(4);
            this.cmbObterDia.Name = "cmbObterDia";
            this.cmbObterDia.Size = new System.Drawing.Size(97, 24);
            this.cmbObterDia.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Dia:";
            // 
            // imgBotSair
            // 
            this.imgBotSair.Image = ((System.Drawing.Image)(resources.GetObject("imgBotSair.Image")));
            this.imgBotSair.Location = new System.Drawing.Point(1095, 15);
            this.imgBotSair.Margin = new System.Windows.Forms.Padding(4);
            this.imgBotSair.Name = "imgBotSair";
            this.imgBotSair.Size = new System.Drawing.Size(44, 43);
            this.imgBotSair.TabIndex = 7;
            this.imgBotSair.TabStop = false;
            this.imgBotSair.Click += new System.EventHandler(this.imgBotSair_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1024, 747);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 5;
            this.button1.Text = "Salvar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 87);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Coletor";
            // 
            // edtTerminal
            // 
            this.edtTerminal.Location = new System.Drawing.Point(24, 107);
            this.edtTerminal.Margin = new System.Windows.Forms.Padding(4);
            this.edtTerminal.Name = "edtTerminal";
            this.edtTerminal.Size = new System.Drawing.Size(1113, 22);
            this.edtTerminal.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(123, 23);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Fluxo:";
            this.label3.Visible = false;
            // 
            // cmbFluxo
            // 
            this.cmbFluxo.FormattingEnabled = true;
            this.cmbFluxo.Items.AddRange(new object[] {
            "ENTRADA",
            "SAIDA"});
            this.cmbFluxo.Location = new System.Drawing.Point(127, 43);
            this.cmbFluxo.Margin = new System.Windows.Forms.Padding(4);
            this.cmbFluxo.Name = "cmbFluxo";
            this.cmbFluxo.Size = new System.Drawing.Size(164, 24);
            this.cmbFluxo.TabIndex = 1;
            this.cmbFluxo.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(20, 139);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1119, 10);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            // 
            // gdwPermissao
            // 
            this.gdwPermissao.AllowUserToAddRows = false;
            this.gdwPermissao.AllowUserToResizeColumns = false;
            this.gdwPermissao.AllowUserToResizeRows = false;
            this.gdwPermissao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdwPermissao.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Codigo,
            this.Descricao,
            this.dEL});
            this.gdwPermissao.Location = new System.Drawing.Point(16, 236);
            this.gdwPermissao.Margin = new System.Windows.Forms.Padding(4);
            this.gdwPermissao.Name = "gdwPermissao";
            this.gdwPermissao.RowHeadersWidth = 10;
            this.gdwPermissao.Size = new System.Drawing.Size(1123, 391);
            this.gdwPermissao.TabIndex = 14;
            this.gdwPermissao.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdwPermissao_CellContentClick);
            this.gdwPermissao.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdwPermissao_CellDoubleClick);
            // 
            // Codigo
            // 
            this.Codigo.HeaderText = "Codigo";
            this.Codigo.Name = "Codigo";
            this.Codigo.Width = 160;
            // 
            // Descricao
            // 
            this.Descricao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Descricao.DefaultCellStyle = dataGridViewCellStyle1;
            this.Descricao.HeaderText = "Descricao";
            this.Descricao.Name = "Descricao";
            // 
            // dEL
            // 
            this.dEL.HeaderText = "";
            this.dEL.Image = global::AutoColetor.Properties.Resources.Finalizar;
            this.dEL.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.dEL.Name = "dEL";
            this.dEL.ReadOnly = true;
            this.dEL.Width = 35;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 155);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(176, 20);
            this.label4.TabIndex = 15;
            this.label4.Text = "Permissão de acesso:";
            // 
            // cmbPermissao
            // 
            this.cmbPermissao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPermissao.FormattingEnabled = true;
            this.cmbPermissao.Location = new System.Drawing.Point(16, 203);
            this.cmbPermissao.Margin = new System.Windows.Forms.Padding(4);
            this.cmbPermissao.Name = "cmbPermissao";
            this.cmbPermissao.Size = new System.Drawing.Size(1077, 24);
            this.cmbPermissao.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(-19, 736);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1157, 50);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // btInserir
            // 
            this.btInserir.Location = new System.Drawing.Point(1103, 201);
            this.btInserir.Margin = new System.Windows.Forms.Padding(4);
            this.btInserir.Name = "btInserir";
            this.btInserir.Size = new System.Drawing.Size(36, 28);
            this.btInserir.TabIndex = 4;
            this.btInserir.Text = "+";
            this.btInserir.UseVisualStyleBackColor = true;
            this.btInserir.Click += new System.EventHandler(this.btInserir_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(381, 174);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 17);
            this.label5.TabIndex = 18;
            this.label5.Text = "Realiza inserção:";
            this.label5.Visible = false;
            // 
            // chkRealizaInsercao
            // 
            this.chkRealizaInsercao.AutoSize = true;
            this.chkRealizaInsercao.Location = new System.Drawing.Point(597, 177);
            this.chkRealizaInsercao.Margin = new System.Windows.Forms.Padding(4);
            this.chkRealizaInsercao.Name = "chkRealizaInsercao";
            this.chkRealizaInsercao.Size = new System.Drawing.Size(56, 21);
            this.chkRealizaInsercao.TabIndex = 19;
            this.chkRealizaInsercao.Text = "Não";
            this.chkRealizaInsercao.UseVisualStyleBackColor = true;
            this.chkRealizaInsercao.Visible = false;
            // 
            // chkDuplicidade
            // 
            this.chkDuplicidade.AutoSize = true;
            this.chkDuplicidade.Location = new System.Drawing.Point(597, 158);
            this.chkDuplicidade.Margin = new System.Windows.Forms.Padding(4);
            this.chkDuplicidade.Name = "chkDuplicidade";
            this.chkDuplicidade.Size = new System.Drawing.Size(56, 21);
            this.chkDuplicidade.TabIndex = 21;
            this.chkDuplicidade.Text = "Não";
            this.chkDuplicidade.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(381, 158);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(209, 17);
            this.label6.TabIndex = 20;
            this.label6.Text = "Permite duplicidade de entrada:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 175);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(145, 17);
            this.label7.TabIndex = 22;
            this.label7.Text = "Produtos necessários";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(16, 644);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(439, 20);
            this.label8.TabIndex = 23;
            this.label8.Text = "Reconhecimento Especial (Utilizado somente para alerta)";
            // 
            // btInserirAlertaEspecial
            // 
            this.btInserirAlertaEspecial.Location = new System.Drawing.Point(1103, 665);
            this.btInserirAlertaEspecial.Margin = new System.Windows.Forms.Padding(4);
            this.btInserirAlertaEspecial.Name = "btInserirAlertaEspecial";
            this.btInserirAlertaEspecial.Size = new System.Drawing.Size(36, 28);
            this.btInserirAlertaEspecial.TabIndex = 25;
            this.btInserirAlertaEspecial.Text = "+";
            this.btInserirAlertaEspecial.UseVisualStyleBackColor = true;
            this.btInserirAlertaEspecial.Click += new System.EventHandler(this.btInserirAlertaEspecial_Click);
            // 
            // cmbAlertaEspecial
            // 
            this.cmbAlertaEspecial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAlertaEspecial.FormattingEnabled = true;
            this.cmbAlertaEspecial.Location = new System.Drawing.Point(16, 667);
            this.cmbAlertaEspecial.Margin = new System.Windows.Forms.Padding(4);
            this.cmbAlertaEspecial.Name = "cmbAlertaEspecial";
            this.cmbAlertaEspecial.Size = new System.Drawing.Size(1077, 24);
            this.cmbAlertaEspecial.TabIndex = 24;
            // 
            // edtAlertaEspecial
            // 
            this.edtAlertaEspecial.Location = new System.Drawing.Point(16, 700);
            this.edtAlertaEspecial.Margin = new System.Windows.Forms.Padding(4);
            this.edtAlertaEspecial.Name = "edtAlertaEspecial";
            this.edtAlertaEspecial.Size = new System.Drawing.Size(1121, 22);
            this.edtAlertaEspecial.TabIndex = 26;
            // 
            // CLT_Configuracoes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1314, 800);
            this.Controls.Add(this.edtAlertaEspecial);
            this.Controls.Add(this.btInserirAlertaEspecial);
            this.Controls.Add(this.cmbAlertaEspecial);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.chkDuplicidade);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.chkRealizaInsercao);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btInserir);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cmbPermissao);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gdwPermissao);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cmbFluxo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.edtTerminal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.imgBotSair);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbObterDia);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CLT_Configuracoes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CLT_Configuracoes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CLT_Configuracoes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgBotSair)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gdwPermissao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbObterDia;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox imgBotSair;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edtTerminal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbFluxo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView gdwPermissao;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbPermissao;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btInserir;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkRealizaInsercao;
        private System.Windows.Forms.CheckBox chkDuplicidade;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descricao;
        private System.Windows.Forms.DataGridViewImageColumn dEL;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btInserirAlertaEspecial;
        private System.Windows.Forms.ComboBox cmbAlertaEspecial;
        private System.Windows.Forms.TextBox edtAlertaEspecial;
    }
}