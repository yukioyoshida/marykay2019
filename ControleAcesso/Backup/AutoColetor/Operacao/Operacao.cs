﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace AutoColetor
{
    public partial class lbl : Form
    {
        bool AP_Cancelado;
        string AP_CodigoGeral;

        public lbl()
        {
            InitializeComponent();
        }

        static bool AP_RealizaPergunta = true;
        static int AP_QuantidadeLeitura = 0;
        static string AP_Produto = "";

        private void Operacao_Load(object sender, EventArgs e)
        {
            MI_CarregarFundo();
            MI_ObterDia();
            MI_ObterFluxo();
            MI_PosicionarPainel();
            MI_CarregaPermissoes();

            edtCodigoBarras.Focus();
        }

        private void MI_CarregarFundo()
        {
            Thread ts = new Thread(MI_AlterarFundo);
            ts.Start();
        }

        private void MI_AlterarFundo()
        {
            this.BackgroundImage = Properties.Resources.Fundo;
        }

        private void MI_PosicionarPainel()
        {
            pnlCodigoBarras.Location = new Point((this.Width/2) - (pnlCodigoBarras.Size.Width / 2), pnlCodigoBarras.Location.Y);
        }

        private void edtCodigoBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            string prstCodigo = edtCodigoBarras.Text; 
            if (e.KeyChar == 13)
            {
                AP_CodigoGeral = "";
                if (edtCodigoBarras.Text == "0")
                {
                    edtCodigoBarras.Text = string.Empty; 
                    tmrSituacao.Enabled = false;
                    tmrSituacao.Enabled = true;
                    tmrSituacao.Interval = 1;
                    return;
                }

                // ANALISAR O CÓDIGO
                AP_Cancelado = false;

                CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
                DataTable dtAnalizarCodigo = IAT_Visitante.MS_ObterCodigoBarrasColetor(prstCodigo);
                if (dtAnalizarCodigo.Rows.Count > 0)
                {
                    if (dtAnalizarCodigo.Rows[0]["Situacao"].ToString().ToUpper() == "ATIVO")
                    {
                        try { prstCodigo = prstCodigo.Substring(0, 10); }
                        catch { }
                    }
                    else
                    {
                        AP_Cancelado = true;
                    }
                }

                AP_CodigoGeral = prstCodigo;

                if (lblCodAux.Text == string.Empty)
                {
                    MI_RegistraColetor(prstCodigo);
                    edtCodigoBarras.Text = string.Empty;
                }
                else
                {
                    if (prstCodigo != lblCodAux.Text)
                    {
                        if (MI_ValidaCodigoAutorizacao(edtCodigoBarras.Text))
                        {
                            MI_RegistraColetorAutorizacao(lblCodAux.Text, prstCodigo);
                            edtCodigoBarras.Text = string.Empty;
                            lblCodAux.Text = "";
                        }
                        else
                        {
                            CAT_CadastrarColetor IAT_Coletor = new CAT_CadastrarColetor();
                            CTO_Coletor ITO_Coletor = new CTO_Coletor();

                            if (lblFluxo.Text.ToUpper() == "ENTRADA")
                            {
                                ITO_Coletor = new CTO_Coletor();
                                ITO_Coletor.setColetor(MI_ObterNumeroTerminal());
                                ITO_Coletor.setCodigo(prstCodigo);
                                ITO_Coletor.setAutorizadoPor(edtCodigoBarras.Text);
                                ITO_Coletor.setFluxo(lblFluxo.Text);
                                //Data e Hora estão diretamente no FWK DAO

                                ITO_Coletor.setEntradaConcedida("N");
                                IAT_Coletor.MS_Incluir(ITO_Coletor, "");
                            }

                            MI_CarregaStatus(false, "PERMISSÃO NÃO CONCEDIDA!");
                            lblCodAux.Text = "";
                            edtCodigoBarras.Text = "";
                            tmrSituacao.Enabled = true;
                        }
                    }
                }
            }
        }

        private void MI_RegistraColetorAutorizacao(string prstCodigo, string stAutorizou)
        {
            CAT_CadastrarColetor IAT_Coletor = new CAT_CadastrarColetor();
            CTO_Coletor ITO_Coletor = new CTO_Coletor();

            if (lblFluxo.Text.ToUpper() == "ENTRADA")
            {
                ITO_Coletor = new CTO_Coletor();
                ITO_Coletor.setColetor(MI_ObterNumeroTerminal());
                ITO_Coletor.setCodigo(prstCodigo);
                ITO_Coletor.setAutorizadoPor(stAutorizou);
                ITO_Coletor.setFluxo(lblFluxo.Text);
                //Data e Hora estão diretamente no FWK DAO

                ITO_Coletor.setEntradaConcedida("S");
                IAT_Coletor.MS_Incluir(ITO_Coletor, "");
                MI_MarcaNaBaseVisitante(prstCodigo);
                MI_CarregaStatus(true, "ACESSO AUTORIZADO");
            }
        }

        private bool MI_ValidaCodigoAutorizacao(string prstCodigo)
        { 
            bool boResult = true;

            CAT_CadastrarColetor IAT_Coletor = new CAT_CadastrarColetor();
            DataTable dtCodigos = IAT_Coletor.MS_ObterPeloCodigoChave(prstCodigo);

            if (dtCodigos.Rows.Count == 0)
                boResult = false;

            return boResult;
        }

        private void MI_RegistraColetor(string prstColetor)
        {
            if (prstColetor.Trim() != string.Empty)
            {
                CAT_CadastrarColetor IAT_Coletor = new CAT_CadastrarColetor();
                CTO_Coletor ITO_Coletor = new CTO_Coletor();

                if (lblFluxo.Text.ToUpper() == "ENTRADA")
                {
                    ////Fecha entrada sem saida
                    //ITO_Coletor = IAT_Coletor.MS_ObterPeloCodigo(prstColetor);
                    //if (ITO_Coletor != null)
                    //{
                    //    ITO_Coletor.setFluxo(lblFluxo.Text);
                    //    ITO_Coletor.setPermanencia(MI_ObterPermanancia(ITO_Coletor));
                    //    IAT_Coletor.MS_Alterar(ITO_Coletor);
                    //}

                    ITO_Coletor = new CTO_Coletor();
               
                    ITO_Coletor.setCodigo(prstColetor);
                    ITO_Coletor.setFluxo(lblFluxo.Text);
                    //Data e Hora estão diretamente no FWK DAO

                    if (MI_PossuiAcesso(prstColetor))
                    {
                        if (MI_VerificaEntradaDuplicidade(prstColetor))
                        {
                            ITO_Coletor.setEntradaConcedida("S");
                            //ITO_Coletor.setColetor(AP_Produto);
                            ITO_Coletor.setColetor(MI_ObterNumeroTerminal());

                            IAT_Coletor.MS_Incluir(ITO_Coletor, AP_Produto);
                            MI_MarcaNaBaseVisitante(prstColetor);
                            MI_CarregaStatus(true, "ACESSO AUTORIZADO "+ AP_Produto);
                        }
                        else
                        {
                            ITO_Coletor.setColetor(MI_ObterNumeroTerminal());
                            MI_CarregaStatus(false, "ENTRADA JÁ REALIZADA");
                            ITO_Coletor.setEntradaConcedida("N");
                            IAT_Coletor.MS_Incluir(ITO_Coletor, "");
                        }
                    }
                    else
                    {
                        if (MI_SolicitaAutorizacaoInsert())
                        {
                            if (chkInsercao.Checked == true)
                            {
                                lblCodAux.Text = prstColetor;
                                MI_HabilitaAutorizacao();
                            }
                            else
                            {
                                ITO_Coletor.setColetor(MI_ObterNumeroTerminal());
                                MI_CarregaStatus(false, "Ops Ainda ainda não é sua vez");
                                ITO_Coletor.setEntradaConcedida("N");
                                IAT_Coletor.MS_Incluir(ITO_Coletor, "");
                                MI_MarcaNaBaseVisitante(prstColetor);
                            }
                        }
                        else
                        {
                            ITO_Coletor.setColetor(MI_ObterNumeroTerminal());
                            MI_CarregaStatus(false, "Ops Ainda ainda não é sua vez");
                            ITO_Coletor.setEntradaConcedida("N");
                            IAT_Coletor.MS_Incluir(ITO_Coletor, "");
                            MI_MarcaNaBaseVisitante(prstColetor);
                        }
                    }
                }
                else
                {
                    AP_QuantidadeLeitura++;
                    ITO_Coletor = IAT_Coletor.MS_ObterPeloCodigo(prstColetor);
                    if (ITO_Coletor != null)
                    {
                        ITO_Coletor.setFluxo(lblFluxo.Text);
                        ITO_Coletor.setPermanencia(MI_ObterPermanancia(ITO_Coletor));
                        IAT_Coletor.MS_Alterar(ITO_Coletor);

                        if (MI_RealizaPergunta())
                        {
                            VerificaNecessidadePergunta(prstColetor);
                        }
                    }
                }
            }
        }

        private bool MI_SolicitaAutorizacaoInsert()
        { 
            bool boResult = true;

            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));
            dt.Columns.Add("TERMINAL", typeof(string));
            dt.Columns.Add("DIA", typeof(string));
            dt.Columns.Add("FLUXO", typeof(string));
            dt.Columns.Add("DUPLICIDADE", typeof(string));
            dt.Columns.Add("INSERCAO", typeof(string));

            dt.ReadXml("configuracoes.xml");
            dr = dt.Rows[0];

            if (dr["INSERCAO"].ToString() == "1")
            {
                return boResult = false;
            }

            return boResult;
        }

        private void MI_CarregaStatus(bool boResult, string stTexto)
        {

            if ((AP_Cancelado == true) && (stTexto == "Ops Ainda ainda não é sua vez"))
                stTexto = "CREDENCIAL CANCELADA";

            if (boResult)
            {
                MI_VerificarReconhecimentoEspecial();
                tmrSituacao.Enabled = false;
                tmrSituacao.Enabled = true;
                tmrSituacao.Interval = 3000;
                lblSTATUS.Text = stTexto; // "ACESSO AUTORIZADO";
                lblSTATUS.BackColor = System.Drawing.Color.GreenYellow;
            }
            else
            {
                tmrSituacao.Enabled = false;
                tmrSituacao.Enabled = true;
                tmrSituacao.Interval = 3000;
                lblSTATUS.Text = stTexto; // "ACESSO AUTORIZADO";
                lblSTATUS.BackColor = System.Drawing.Color.Red;
            }
        }

        private void MI_VerificarReconhecimentoEspecial()
        {
            string[] listReconhecimento = new string[100];
            listReconhecimento = MI_ObterReconhecimentoEspecial().Split(';');

            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtRegistro = IAT_Visitante.MS_ObterPorCodigoDat(AP_CodigoGeral);

            /* CANCELA A AÇÃO */
            if (dtRegistro.Rows.Count == 0)
                return;

            DataRow Roww = dtRegistro.Rows[0];
            CAT_CadastrarPedido IAT_Pedido = new CAT_CadastrarPedido();
            DataTable dtCompra = IAT_Pedido.MS_ObterItensComprados(Convert.ToInt32(Roww["ObjRef"]));

            foreach (string Item in listReconhecimento)
            {
                foreach (DataRow Row in dtCompra.Rows)
                {
                    if (Row["cdProduto"].ToString() == Item)
                    {
                        picStar.Visible = true;
                        lblReconhecimento.Text = MI_ObterNomeProduto(Item);
                        lblReconhecimento.Visible = true;
                        break;
                    }
                }
            }
        }

        private string MI_ObterNomeProduto(string prCdProduto)
        {
            string prNomeProduto = string.Empty;

            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtProduto = IAT_Visitante.MS_ObterNomeProduto(prCdProduto);

            if (dtProduto.Rows.Count > 0)
                prNomeProduto = dtProduto.Rows[0]["dsProduto"].ToString();

            return prNomeProduto;
        }

        private string MI_ObterReconhecimentoEspecial()
        {
            string prReconhecimento = string.Empty;

            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));
            dt.Columns.Add("TERMINAL", typeof(string));
            dt.Columns.Add("DIA", typeof(string));
            dt.Columns.Add("FLUXO", typeof(string));
            dt.Columns.Add("DUPLICIDADE", typeof(string));
            dt.Columns.Add("INSERCAO", typeof(string));
            dt.Columns.Add("RECONHECIMENTOESPECIAL", typeof(string));

            dt.ReadXml("configuracoes.xml");
            dr = dt.Rows[0];
            prReconhecimento = dr.ItemArray[9].ToString();

            return prReconhecimento;
        }

        private void MI_HabilitaAutorizacao()
        {
            tmrSituacao.Enabled = false;
            tmrSituacao.Enabled = true;
            tmrSituacao.Interval = 5000;

            lblSTATUS.Text = "AUTORIZAÇÃO";
            lblSTATUS.BackColor = System.Drawing.Color.Yellow;
        }

        private bool MI_VerificaEntradaDuplicidade(string prstCodigo)
        {
            bool boResult = true;

            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));
            dt.Columns.Add("TERMINAL", typeof(string));
            dt.Columns.Add("DIA", typeof(string));
            dt.Columns.Add("FLUXO", typeof(string));
            dt.Columns.Add("DUPLICIDADE", typeof(string));
            dt.Columns.Add("INSERCAO", typeof(string));

            dt.ReadXml("configuracoes.xml");
            dr = dt.Rows[0];

            if (dr["DUPLICIDADE"].ToString() == "1")
            {
                CAT_CadastrarColetor IAT_Coletor = new CAT_CadastrarColetor();
                CTO_Coletor ITO_Coletor = IAT_Coletor.MS_ObterPeloCodigoETerminal(prstCodigo, dr["TERMINAL"].ToString());

                if (ITO_Coletor != null)
                    return boResult = false;
            }
            
            return boResult;
        }

        private bool MI_RealizaPergunta()
        {
            CAT_CadastrarVisitante IAT_Questionario = new CAT_CadastrarVisitante();
            DataTable dtRegistros = IAT_Questionario.MS_ObterQuestionario();
            DataRow drLinha;

            if (dtRegistros.Rows.Count > 0)
            {
                drLinha = dtRegistros.Rows[0];
                if (drLinha.ItemArray[0].ToString() == "N")
                    return false;
            }

            return AP_RealizaPergunta;
        }

        private void VerificaNecessidadePergunta(string prstCodigoBarras)
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtRegistro = IAT_Visitante.MS_ObterRespostaSaida(prstCodigoBarras);
            DataRow drLinha;

            if (dtRegistro.Rows.Count > 0)
            {
                drLinha = dtRegistro.Rows[0];
                if (drLinha.ItemArray[1].ToString().Trim() == "")
                {
                    MI_FazPergunta(prstCodigoBarras);
                }
            }
        }

        private void MI_FazPergunta(string prstCodigoBarras)
        {
        }

        private string MI_ObterPermanancia(CTO_Coletor ITO_Coletor)
        {
            TimeSpan tsPermanencia;

            DateTime dtDataEntrada = Convert.ToDateTime(ITO_Coletor.getDataEntrada().ToString().Replace("00:00:00","") + " " + ITO_Coletor.getHoraEntrada());
            DateTime dtDataSaida = DateTime.Now;

            tsPermanencia = dtDataSaida.Subtract(dtDataEntrada);

            return tsPermanencia.Hours.ToString() + ":" + tsPermanencia.Minutes.ToString() + ":" + tsPermanencia.Seconds.ToString();
        }

        private void MI_MarcaNaBaseVisitante(string stCodigo)
        {
            string stDia="";
            string stDiaEvento = lblDia.Text.ToString();
            switch (stDiaEvento)
            {
                case "01":
                    stDia = "PresenteDia1";
                    break;

                case "02":
                    stDia = "PresenteDia2";
                    break;

                case "03":
                    stDia = "PresenteDia3";
                    break;

                case "04":
                    stDia = "PresenteDia4";
                    break;

                case "05":
                    stDia = "PresenteDia5";
                    break;

            }
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            IAT_Visitante.MS_PresenteEvento(stCodigo, stDia);
        }

        private string MI_ObterNumeroTerminal()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));
            dt.Columns.Add("TERMINAL", typeof(string));
            dt.Columns.Add("DIA", typeof(string));
            dt.Columns.Add("FLUXO", typeof(string));

            dt.ReadXml("configuracoes.xml");
            dr = dt.Rows[0];

            return dr.ItemArray[4].ToString();
        }

        private void imgBotSair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente deseja sair do sistema?", "Finalizar o Sistema", MessageBoxButtons.YesNo) == DialogResult.Yes)
                Application.Exit(); 
        }

        private void tmrDataHora_Tick(object sender, EventArgs e)
        {
            lblDataHora.Text = "Data/Hora: " + System.DateTime.Now.ToString(); 
        }

        private void MI_ObterDia()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));
            dt.Columns.Add("TERMINAL", typeof(string));
            dt.Columns.Add("DIA", typeof(string));
            dt.Columns.Add("FLUXO", typeof(string));

            dt.ReadXml("configuracoes.xml");
            dr = dt.Rows[0];

            lblDia.Text = dr.ItemArray[5].ToString();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
        }

        private void configuraçõesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CLT_Configuracoes IAT_Congif = new CLT_Configuracoes();
            IAT_Congif.ShowDialog();
            MI_ObterDia();
            MI_ObterFluxo();
            MI_CarregaPermissoes();
        }

        private void MI_ObterFluxo()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));
            dt.Columns.Add("TERMINAL", typeof(string));
            dt.Columns.Add("DIA", typeof(string));
            dt.Columns.Add("FLUXO", typeof(string));

            dt.ReadXml("configuracoes.xml");
            dr = dt.Rows[0];

            lblFluxo.Text = dr.ItemArray[6].ToString();
        }

        private void edtCodigoBarras_TextChanged(object sender, EventArgs e)
        {
        }

        private void tmrSituacao_Tick(object sender, EventArgs e)
        {
            if ((lblSTATUS.BackColor == System.Drawing.Color.Yellow) && (lblCodAux.Text != ""))
            {
                CAT_CadastrarColetor IAT_Coletor = new CAT_CadastrarColetor();
                CTO_Coletor ITO_Coletor = new CTO_Coletor();

                ITO_Coletor = new CTO_Coletor();
                ITO_Coletor.setColetor(MI_ObterNumeroTerminal());
                ITO_Coletor.setCodigo(lblCodAux.Text);
                ITO_Coletor.setFluxo(lblFluxo.Text);

                ITO_Coletor.setEntradaConcedida("N");
                IAT_Coletor.MS_Incluir(ITO_Coletor, "");
            }
            
            lblCodAux.Text = "";
            lblSTATUS.Text = "";
            lblSTATUS.BackColor = System.Drawing.Color.Transparent;

            picStar.Visible = false;
            lblReconhecimento.Visible = false;

            tmrSituacao.Enabled = false;
        }

        private bool MI_PossuiAcesso(string prstCodigo)
        {
            bool boResult = false;

            DataTable dtNecessarios = CSV_Permissao.getTablePermissoes();

            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtRegistro = IAT_Visitante.MS_ObterPorCodigoDat(prstCodigo);

            if (dtNecessarios.Rows.Count == 0)
            {
                return boResult = true;
            }
            
            if (dtRegistro.Rows.Count == 0)
            {
                return boResult = false;
            }

            if (dtNecessarios.Rows[0]["Valor"].ToString() == "")
                return boResult = true;

            DataRow Roww = dtRegistro.Rows[0];

            CAT_CadastrarPedido IAT_Pedido = new CAT_CadastrarPedido();
            DataTable dtCompra = IAT_Pedido.MS_ObterItensComprados(Convert.ToInt32(Roww["ObjRef"]));

            foreach (DataRow Row in dtNecessarios.Rows)
            {
                foreach (DataRow Linha in dtCompra.Rows)
                {
                    if (Row.ItemArray[0].ToString().ToUpper().Trim().Contains(Linha.ItemArray[0].ToString().ToUpper().Trim()))
                    {
                        AP_Produto = MI_ObterDescricao(Linha.ItemArray[0].ToString().ToUpper().Trim());
                        return boResult = true;
                    }

                    if (Linha.ItemArray[0].ToString().ToUpper().Trim().Contains(Row.ItemArray[0].ToString().ToUpper().Trim()))
                    {
                        AP_Produto = MI_ObterDescricao(Linha.ItemArray[0].ToString().ToUpper().Trim());
                        return boResult = true;
                    }
                }
            }

            return boResult;
        }

        private DataTable MI_ObterNecessarios()
        {
            DataTable dtNecessarios = new DataTable();

            return dtNecessarios;
        }

        private void MI_CarregaPermissoes()
        { 
            lblTerminal.Text = "Terminal: " + MI_ObterNumeroTerminal();

            DataTable dtPermissoes = CSV_Permissao.getTablePermissoes();

            gdwPermissao.Rows.Clear();
            foreach (DataRow Row in dtPermissoes.Rows)
            {
                gdwPermissao.Rows.Add(Row.ItemArray[0].ToString(), "");
            }
            
            foreach (DataGridViewRow LINHA in gdwPermissao.Rows)
            {
                LINHA.Cells[1].Value = MI_ObterDescricao2(LINHA.Cells[0].Value.ToString());

                LINHA.Cells[0].Style.ForeColor = System.Drawing.Color.Black;
                LINHA.Cells[1].Style.ForeColor = System.Drawing.Color.Black;
            }
        }

        private string MI_ObterDescricao(string prstCodigo)
        {
            string stRetorno = string.Empty;

            CAT_CadastrarProdutos IAT_Produtos = new CAT_CadastrarProdutos();
            CTO_Produtos ITO_Produtos = IAT_Produtos.MS_ObterProdutoPeloCodigo(prstCodigo);

            if (ITO_Produtos != null)
            {
                stRetorno = ITO_Produtos.getDescricao().ToString();
            }

            return stRetorno;
        }
        private string MI_ObterDescricao2(string prstCodigo)
        {
            string stRetorno = string.Empty;

            CAT_CadastrarProdutos IAT_Produtos = new CAT_CadastrarProdutos();
            CTO_Produtos ITO_Produtos = IAT_Produtos.MS_ObterDescricaoRegraPeloCodigo(prstCodigo);

            if (ITO_Produtos != null)
            {
                stRetorno = ITO_Produtos.getDescricao().ToString();
            }

            return stRetorno;
        }
    }
}
