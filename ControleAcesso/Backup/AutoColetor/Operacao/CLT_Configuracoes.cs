﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AutoColetor;

namespace AutoColetor
{
    public partial class CLT_Configuracoes : Form
    {
        public CLT_Configuracoes()
        {
            InitializeComponent();
        }
     
        private void CLT_Configuracoes_Load(object sender, EventArgs e)
        {
            MI_ObterDia();
            MI_CarregaAtividades();
            MI_CarregaPermissoes();
        }
        private void MI_ObterDia()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));
            dt.Columns.Add("TERMINAL", typeof(string));
            dt.Columns.Add("DIA", typeof(string));
            dt.Columns.Add("FLUXO", typeof(string));
            dt.Columns.Add("DUPLICIDADE", typeof(string));
            dt.Columns.Add("INSERCAO", typeof(string));
            dt.Columns.Add("RECONHECIMENTOESPECIAL", typeof(string));

            dt.ReadXml("configuracoes.xml");
            dr = dt.Rows[0];
            edtTerminal.Text = dr.ItemArray[4].ToString();
            cmbObterDia.Text = dr.ItemArray[5].ToString();
            cmbFluxo.Text = dr.ItemArray[6].ToString();
            edtAlertaEspecial.Text = dr.ItemArray[9].ToString();

            if (dr["DUPLICIDADE"].ToString() == "1")
                chkDuplicidade.Checked = true;
            else
                chkDuplicidade.Checked = false;

            if (dr["INSERCAO"].ToString() == "1")
                chkRealizaInsercao.Checked = true;
            else
                chkRealizaInsercao.Checked = false;
        }

        private void imgBotSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));
            dt.Columns.Add("TERMINAL", typeof(string));
            dt.Columns.Add("DIA", typeof(string));
            dt.Columns.Add("FLUXO", typeof(string));
            dt.Columns.Add("DUPLICIDADE", typeof(string));
            dt.Columns.Add("INSERCAO", typeof(string));
            dt.Columns.Add("EVENTO", typeof(string));
            dt.Columns.Add("RECONHECIMENTOESPECIAL", typeof(string));

            dt.ReadXml("configuracoes.xml");
            dt.Rows[0][4] = edtTerminal.Text;
            dt.Rows[0][5] = cmbObterDia.Text;
            dt.Rows[0][6] = cmbFluxo.Text;

            if (chkDuplicidade.Checked == true)
                dt.Rows[0][7] = "1";
            else
                dt.Rows[0][7] = "0";

            if (chkRealizaInsercao.Checked == true)
                dt.Rows[0][8] = "1";
            else
                dt.Rows[0][8] = "0";

            dt.Rows[0]["EVENTO"] = MI_ObterEvento();
            dt.Rows[0]["RECONHECIMENTOESPECIAL"] = edtAlertaEspecial.Text.Trim();

            dt.WriteXml("configuracoes.xml");

            MI_SalvaPermissoes();

            MessageBox.Show("Alterado com sucesso!","Atenção!");
            this.Close();
        }

        private string MI_ObterEvento()
        {
            string stEvento = "";
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));
            dt.Columns.Add("TERMINAL", typeof(string));
            dt.Columns.Add("DIA", typeof(string));
            dt.Columns.Add("FLUXO", typeof(string));
            dt.Columns.Add("EVENTO", typeof(string));
            dt.Columns.Add("RECONHECIMENTOESPECIAL", typeof(string));

            dt.ReadXml("configuracoes.xml");
            dr = dt.Rows[0];

            return stEvento = dr["EVENTO"].ToString();
        }

        private void MI_SalvaPermissoes()
        {
            DataTable dtPermissao = new DataTable("Permissao");
            dtPermissao.Columns.Add("Valor", typeof(string));
            dtPermissao.ReadXml("Permissao.xml");

            dtPermissao.Rows.Clear();
            
            foreach (DataGridViewRow Row in gdwPermissao.Rows)
            {
                DataRow drLinhaInsert = dtPermissao.NewRow();
                drLinhaInsert["Valor"] = Row.Cells[0].Value.ToString();
                drLinhaInsert.EndEdit();
                dtPermissao.Rows.Add(drLinhaInsert);
            }

            dtPermissao.WriteXml("Permissao.xml");
            CSV_Permissao.MI_MontaPermissao();
        }

        private void MI_CarregaAtividades()
        {
            CAT_CadastrarProdutos IAT_Produtos = new CAT_CadastrarProdutos();
            DataTable dtProdutos = IAT_Produtos.MS_ObterTodosOsProdutos();

            /* INSERIR PRODUTOS NAS PERMISSÕES */
            cmbPermissao.Items.Add("");
            foreach (DataRow Row in dtProdutos.Rows)
            {
                cmbPermissao.Items.Add(Row["cdProduto"].ToString() + " " + Row["dsProduto"].ToString());
            }
            cmbPermissao.Text = "";

            /* INSERIR PRODUTOS NO RECONHECIMENTO ESPECIAL */
            cmbAlertaEspecial.Items.Add("");
            foreach (DataRow Row in dtProdutos.Rows)
            {
                cmbAlertaEspecial.Items.Add(Row["cdProduto"].ToString() + " " + Row["dsProduto"].ToString());
            }

            cmbAlertaEspecial.Text = "";
        }
        
        private void MI_CarregaPermissoes()
        {
            DataTable dtPermissoes = CSV_Permissao.getTablePermissoes();

            foreach (DataRow Row in dtPermissoes.Rows)
            {
                gdwPermissao.Rows.Add(Row.ItemArray[0].ToString(),"");
            }

            foreach (DataGridViewRow LINHA in gdwPermissao.Rows)
            {
                LINHA.Cells[1].Value = MI_ObterDescricao(LINHA.Cells[0].Value.ToString());
            }

            foreach (DataGridViewRow Row in gdwPermissao.Rows)
            {
                Row.Height = 30;
            }

        }

        private string MI_ObterDescricao(string prstCodigo)
        {
            string stRetorno = string.Empty;

            CAT_CadastrarProdutos IAT_Produtos = new CAT_CadastrarProdutos();
            CTO_Produtos ITO_Produtos = IAT_Produtos.MS_ObterDescricaoRegraPeloCodigo(prstCodigo);

            if (ITO_Produtos != null)
            {
                stRetorno = ITO_Produtos.getDescricao().ToString();
            }

            return stRetorno;
        }

        private void btInserir_Click(object sender, EventArgs e)
        {  
            if (cmbPermissao.Text.Trim() != string.Empty)
            {
                int itQuantidade2 = 0;
                string[] codigos = cmbPermissao.Text.Split(';');
                if(codigos.Count()>1)
                  itQuantidade2 = 4* codigos.Count() + codigos.Count()-1;
                else
                    itQuantidade2 = 4;
                int itQuantidade = cmbPermissao.Text.Trim().Length;

                gdwPermissao.Rows.Add(cmbPermissao.Text.Trim().Substring(0, itQuantidade2), cmbPermissao.Text.Trim().Substring(itQuantidade2));
                cmbPermissao.Text = "";

                foreach (DataGridViewRow Row in gdwPermissao.Rows)
                {
                    Row.Height = 30;
                }
            }
        }

        private void gdwPermissao_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            gdwPermissao.Rows.Remove(gdwPermissao.Rows[e.RowIndex]);
        }

        private void gdwPermissao_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 2)
                {
                    gdwPermissao.Rows.Remove(gdwPermissao.CurrentRow);
                }
            }
            catch { }
        }

        private void btInserirAlertaEspecial_Click(object sender, EventArgs e)
        {
            if (cmbAlertaEspecial.Text.Trim() != string.Empty)
            {
                int itQuantidade2 = 0;

                string[] codigos = cmbAlertaEspecial.Text.Split(';');
                if (codigos.Count() > 1)
                    itQuantidade2 = 4 * codigos.Count() + codigos.Count() - 1;
                else
                    itQuantidade2 = 4;
                int itQuantidade = cmbAlertaEspecial.Text.Trim().Length;

                edtAlertaEspecial.Text = edtAlertaEspecial.Text + cmbAlertaEspecial.Text.Trim().Substring(0, itQuantidade2) + ";";
                cmbAlertaEspecial.Text = "";

                foreach (DataGridViewRow Row in gdwPermissao.Rows)
                {
                    Row.Height = 30;
                }
            }
        }

    }
}
