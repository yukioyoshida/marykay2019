﻿namespace AutoColetor
{
    partial class lbl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(lbl));
            this.edtCodigoBarras = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDataHora = new System.Windows.Forms.Label();
            this.tmrDataHora = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.configuraçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label4 = new System.Windows.Forms.Label();
            this.lblFluxo = new System.Windows.Forms.Label();
            this.lblDia = new System.Windows.Forms.Label();
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.tmrSituacao = new System.Windows.Forms.Timer(this.components);
            this.chkInsercao = new System.Windows.Forms.CheckBox();
            this.lblCodAux = new System.Windows.Forms.Label();
            this.gdwPermissao = new System.Windows.Forms.DataGridView();
            this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlCodigoBarras = new System.Windows.Forms.Panel();
            this.picStar = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.imgBotSair = new System.Windows.Forms.PictureBox();
            this.lblReconhecimento = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdwPermissao)).BeginInit();
            this.pnlCodigoBarras.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picStar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBotSair)).BeginInit();
            this.SuspendLayout();
            // 
            // edtCodigoBarras
            // 
            this.edtCodigoBarras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.edtCodigoBarras.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtCodigoBarras.Font = new System.Drawing.Font("Verdana", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtCodigoBarras.ForeColor = System.Drawing.Color.White;
            this.edtCodigoBarras.Location = new System.Drawing.Point(122, 27);
            this.edtCodigoBarras.MaxLength = 20;
            this.edtCodigoBarras.Name = "edtCodigoBarras";
            this.edtCodigoBarras.Size = new System.Drawing.Size(495, 78);
            this.edtCodigoBarras.TabIndex = 0;
            this.edtCodigoBarras.TextChanged += new System.EventHandler(this.edtCodigoBarras_TextChanged);
            this.edtCodigoBarras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.edtCodigoBarras_KeyPress);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(14, 755);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Sair do Sistema";
            // 
            // lblDataHora
            // 
            this.lblDataHora.AutoSize = true;
            this.lblDataHora.BackColor = System.Drawing.Color.Transparent;
            this.lblDataHora.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataHora.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblDataHora.Location = new System.Drawing.Point(27, 34);
            this.lblDataHora.Name = "lblDataHora";
            this.lblDataHora.Size = new System.Drawing.Size(95, 16);
            this.lblDataHora.TabIndex = 8;
            this.lblDataHora.Text = "Verificando...";
            // 
            // tmrDataHora
            // 
            this.tmrDataHora.Enabled = true;
            this.tmrDataHora.Interval = 1000;
            this.tmrDataHora.Tick += new System.EventHandler(this.tmrDataHora_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(29, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "DIA:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuraçõesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1102, 24);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // configuraçõesToolStripMenuItem
            // 
            this.configuraçõesToolStripMenuItem.Name = "configuraçõesToolStripMenuItem";
            this.configuraçõesToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.configuraçõesToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.configuraçõesToolStripMenuItem.Text = "Configurações";
            this.configuraçõesToolStripMenuItem.Click += new System.EventHandler(this.configuraçõesToolStripMenuItem_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(30, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "FLUXO:";
            // 
            // lblFluxo
            // 
            this.lblFluxo.AutoSize = true;
            this.lblFluxo.BackColor = System.Drawing.Color.Transparent;
            this.lblFluxo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblFluxo.Location = new System.Drawing.Point(81, 75);
            this.lblFluxo.Name = "lblFluxo";
            this.lblFluxo.Size = new System.Drawing.Size(69, 13);
            this.lblFluxo.TabIndex = 13;
            this.lblFluxo.Text = "Verificando...";
            // 
            // lblDia
            // 
            this.lblDia.AutoSize = true;
            this.lblDia.BackColor = System.Drawing.Color.Transparent;
            this.lblDia.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblDia.Location = new System.Drawing.Point(63, 55);
            this.lblDia.Name = "lblDia";
            this.lblDia.Size = new System.Drawing.Size(69, 13);
            this.lblDia.TabIndex = 14;
            this.lblDia.Text = "Verificando...";
            // 
            // lblSTATUS
            // 
            this.lblSTATUS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSTATUS.BackColor = System.Drawing.Color.Transparent;
            this.lblSTATUS.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSTATUS.Location = new System.Drawing.Point(33, 534);
            this.lblSTATUS.Name = "lblSTATUS";
            this.lblSTATUS.Size = new System.Drawing.Size(1035, 83);
            this.lblSTATUS.TabIndex = 18;
            this.lblSTATUS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tmrSituacao
            // 
            this.tmrSituacao.Interval = 3000;
            this.tmrSituacao.Tick += new System.EventHandler(this.tmrSituacao_Tick);
            // 
            // chkInsercao
            // 
            this.chkInsercao.AutoSize = true;
            this.chkInsercao.BackColor = System.Drawing.Color.Transparent;
            this.chkInsercao.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInsercao.ForeColor = System.Drawing.Color.White;
            this.chkInsercao.Location = new System.Drawing.Point(30, 373);
            this.chkInsercao.Name = "chkInsercao";
            this.chkInsercao.Size = new System.Drawing.Size(189, 20);
            this.chkInsercao.TabIndex = 19;
            this.chkInsercao.Text = "Inserir novo participante";
            this.chkInsercao.UseVisualStyleBackColor = false;
            this.chkInsercao.Visible = false;
            // 
            // lblCodAux
            // 
            this.lblCodAux.BackColor = System.Drawing.Color.White;
            this.lblCodAux.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodAux.ForeColor = System.Drawing.Color.Yellow;
            this.lblCodAux.Location = new System.Drawing.Point(30, 352);
            this.lblCodAux.Name = "lblCodAux";
            this.lblCodAux.Size = new System.Drawing.Size(142, 18);
            this.lblCodAux.TabIndex = 20;
            this.lblCodAux.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCodAux.Visible = false;
            // 
            // gdwPermissao
            // 
            this.gdwPermissao.AllowUserToAddRows = false;
            this.gdwPermissao.AllowUserToDeleteRows = false;
            this.gdwPermissao.AllowUserToResizeColumns = false;
            this.gdwPermissao.AllowUserToResizeRows = false;
            this.gdwPermissao.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gdwPermissao.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(142)))), ((int)(((byte)(142)))));
            this.gdwPermissao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdwPermissao.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Codigo,
            this.Descricao});
            this.gdwPermissao.Location = new System.Drawing.Point(32, 122);
            this.gdwPermissao.Name = "gdwPermissao";
            this.gdwPermissao.ReadOnly = true;
            this.gdwPermissao.RowHeadersVisible = false;
            this.gdwPermissao.Size = new System.Drawing.Size(1036, 198);
            this.gdwPermissao.TabIndex = 26;
            // 
            // Codigo
            // 
            this.Codigo.HeaderText = "Codigo";
            this.Codigo.Name = "Codigo";
            this.Codigo.ReadOnly = true;
            this.Codigo.Width = 160;
            // 
            // Descricao
            // 
            this.Descricao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Descricao.HeaderText = "Descricao";
            this.Descricao.Name = "Descricao";
            this.Descricao.ReadOnly = true;
            // 
            // lblTerminal
            // 
            this.lblTerminal.AutoSize = true;
            this.lblTerminal.BackColor = System.Drawing.Color.Transparent;
            this.lblTerminal.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTerminal.Location = new System.Drawing.Point(27, 102);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(67, 17);
            this.lblTerminal.TabIndex = 27;
            this.lblTerminal.Text = "Terminal:";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Location = new System.Drawing.Point(20, 326);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1048, 8);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            // 
            // pnlCodigoBarras
            // 
            this.pnlCodigoBarras.BackColor = System.Drawing.Color.Transparent;
            this.pnlCodigoBarras.Controls.Add(this.edtCodigoBarras);
            this.pnlCodigoBarras.Controls.Add(this.pictureBox3);
            this.pnlCodigoBarras.Location = new System.Drawing.Point(275, 376);
            this.pnlCodigoBarras.Name = "pnlCodigoBarras";
            this.pnlCodigoBarras.Size = new System.Drawing.Size(728, 137);
            this.pnlCodigoBarras.TabIndex = 30;
            // 
            // picStar
            // 
            this.picStar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picStar.BackColor = System.Drawing.Color.Transparent;
            this.picStar.Image = global::AutoColetor.Properties.Resources.Star;
            this.picStar.Location = new System.Drawing.Point(907, 620);
            this.picStar.Name = "picStar";
            this.picStar.Size = new System.Drawing.Size(161, 50);
            this.picStar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picStar.TabIndex = 31;
            this.picStar.TabStop = false;
            this.picStar.Visible = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::AutoColetor.Properties.Resources.fnd_codigo_barras;
            this.pictureBox3.Location = new System.Drawing.Point(111, 12);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(525, 117);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 28;
            this.pictureBox3.TabStop = false;
            // 
            // imgBotSair
            // 
            this.imgBotSair.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.imgBotSair.BackColor = System.Drawing.Color.Transparent;
            this.imgBotSair.Image = ((System.Drawing.Image)(resources.GetObject("imgBotSair.Image")));
            this.imgBotSair.Location = new System.Drawing.Point(48, 717);
            this.imgBotSair.Name = "imgBotSair";
            this.imgBotSair.Size = new System.Drawing.Size(33, 35);
            this.imgBotSair.TabIndex = 6;
            this.imgBotSair.TabStop = false;
            this.imgBotSair.Click += new System.EventHandler(this.imgBotSair_Click);
            // 
            // lblReconhecimento
            // 
            this.lblReconhecimento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblReconhecimento.BackColor = System.Drawing.Color.Transparent;
            this.lblReconhecimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReconhecimento.Location = new System.Drawing.Point(20, 675);
            this.lblReconhecimento.Name = "lblReconhecimento";
            this.lblReconhecimento.Size = new System.Drawing.Size(1048, 28);
            this.lblReconhecimento.TabIndex = 32;
            this.lblReconhecimento.Text = "lblReconhecimento";
            this.lblReconhecimento.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblReconhecimento.Visible = false;
            // 
            // lbl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(58)))), ((int)(((byte)(58)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1102, 782);
            this.Controls.Add(this.lblReconhecimento);
            this.Controls.Add(this.picStar);
            this.Controls.Add(this.pnlCodigoBarras);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblTerminal);
            this.Controls.Add(this.gdwPermissao);
            this.Controls.Add(this.lblCodAux);
            this.Controls.Add(this.chkInsercao);
            this.Controls.Add(this.lblSTATUS);
            this.Controls.Add(this.lblDia);
            this.Controls.Add(this.lblFluxo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblDataHora);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.imgBotSair);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "lbl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Auto Coletor";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Operacao_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdwPermissao)).EndInit();
            this.pnlCodigoBarras.ResumeLayout(false);
            this.pnlCodigoBarras.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picStar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBotSair)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edtCodigoBarras;
        private System.Windows.Forms.PictureBox imgBotSair;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblDataHora;
        private System.Windows.Forms.Timer tmrDataHora;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem configuraçõesToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblFluxo;
        private System.Windows.Forms.Label lblDia;
        private System.Windows.Forms.Label lblSTATUS;
        private System.Windows.Forms.Timer tmrSituacao;
        private System.Windows.Forms.CheckBox chkInsercao;
        private System.Windows.Forms.Label lblCodAux;
        private System.Windows.Forms.DataGridView gdwPermissao;
        private System.Windows.Forms.Label lblTerminal;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descricao;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel pnlCodigoBarras;
        private System.Windows.Forms.PictureBox picStar;
        private System.Windows.Forms.Label lblReconhecimento;
    }
}

