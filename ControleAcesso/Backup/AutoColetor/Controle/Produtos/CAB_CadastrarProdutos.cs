using System;
using AutoColetor;

public abstract class CAB_CadastrarProdutos : ICadastrarProdutos
{
     public abstract void MS_Incluir(CTO_Produtos ITO_Produtos);
     public abstract void MS_Alterar(CTO_Produtos ITO_Produtos);
     public abstract void MS_Excluir(CTO_Produtos ITO_Produtos);
     public abstract CTO_Produtos MS_Obter(int pritObjRef);
     public abstract CTO_Produtos MS_Obter(string prstID);
     public abstract CTO_Produtos MS_ObterProdutoPeloCodigo(string prstID);
}

