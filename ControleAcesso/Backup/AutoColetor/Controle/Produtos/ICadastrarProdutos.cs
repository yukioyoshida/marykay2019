using System;
using AutoColetor;

public interface ICadastrarProdutos
{
     void MS_Incluir(CTO_Produtos ITO_Produtos);
     void MS_Alterar(CTO_Produtos ITO_Produtos);
     void MS_Excluir(CTO_Produtos ITO_Produtos);
     CTO_Produtos MS_Obter(int pritObjRef);
     CTO_Produtos MS_Obter(string prstID);
     CTO_Produtos MS_ObterProdutoPeloCodigo(string prstID);
}

