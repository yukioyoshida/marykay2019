using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using AutoColetor;
namespace AutoColetor
{
    public class FWK_DAO_Produtos : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                          
                           "          cdProduto, " +
                           "          dsProduto ";

        public FWK_DAO_Produtos()
        {
        }

        public void MS_Incluir(CTO_Produtos ITO_Produtos)
        {
         
            MI_SetSql ("INSERT INTO tblregras (" +

                       "        cdProduto, " +
                           "          dsProduto ) " + 
                       " VALUES (" + 
                      
                       "          ?Codigo, " +
                       "          ?Descricao) ");

        
            MI_AddParameters("?Codigo", ITO_Produtos.getCodigo());
            MI_AddParameters("?Descricao", ITO_Produtos.getDescricao());

            MI_ExecuteNonQuery();
        }

        public void MS_Alterar(CTO_Produtos ITO_Produtos)
        {
            MI_SetSql ("UPDATE tblregras SET " +
                   
                       "          cdProduto = ?Codigo, " +
                       "          dsProduto = ?Descricao " +
                       "WHERE cdProduto = ?Codigo ");
         
            MI_AddParameters("?Codigo", ITO_Produtos.getCodigo());
            MI_AddParameters("?Descricao", ITO_Produtos.getDescricao());

            MI_ExecuteNonQuery();
        }

        public void MS_Excluir(CTO_Produtos ITO_Produtos)
        {
            MI_SetSql ("DELETE FROM tblregras WHERE cdProduto = ?ObjRef");
            MI_AddParameters("?ObjRef", ITO_Produtos.getCodigo());
            
            MI_ExecuteNonQuery();
        }

        public CTO_Produtos MS_Obter(int pritObjRef)
        {
            CTO_Produtos ITO_Produtos = new CTO_Produtos();
            MI_SetSql ("SELECT " + 
                       "          cdProduto, " +
                       "          dsProduto " + 
                       "FROM tblregras " +
                       "WHERE cdProduto = ?ObjRef ");
            MI_AddParameters("?Objref", pritObjRef);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Produtos = null;
            else
                ITO_Produtos = MI_DataSetToEntidade();
            return ITO_Produtos;
        }

        public CTO_Produtos MI_DataSetToEntidade()
        {
            CTO_Produtos ITO_Produtos = new CTO_Produtos();            
           
            try
            {
                ITO_Produtos.setCodigo(AP_DataReader.GetString(0));
            }
            catch
            {
                ITO_Produtos.setCodigo("");
            }
            try
            {
                ITO_Produtos.setDescricao(AP_DataReader.GetString(1));
            }
            catch
            {
                ITO_Produtos.setDescricao("");
            }
            return ITO_Produtos;
        }

        public CTO_Produtos MS_ObterTodos()
        {
            CTO_Produtos ITO_Produtos = new CTO_Produtos();
            MI_SetSql ("SELECT " + 
                       
                       "          cdProduto, " +
                       "          dsProduto " + 
                       "FROM tblregras " );
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Produtos = null;
            else
                ITO_Produtos = MI_DataSetToEntidade();
            return ITO_Produtos;
        }

        public CTO_Produtos MS_ObterProximo()
        {
            CTO_Produtos ITO_Produtos = new CTO_Produtos();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_Produtos = null;
            else
                ITO_Produtos = MI_DataSetToEntidade();
                return ITO_Produtos;
        }

        //public int MS_NovoObjRef()
        //{
        //    int itObjRef;
        //    MI_SetSql("Select Max(ObjRef) from tblregras");
        //    MI_ExecuteQuery();
        //    if (AP_DataReader.IsDBNull(0))
        //    {
        //        itObjRef = 1;
        //    }
        //    else
        //    {
        //        itObjRef = AP_DataReader.GetInt32(0);
        //        itObjRef++;
        //    }
        //    return itObjRef;
        //}

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblregras");
            return MI_ExecuteDataSet();
        }

    }
}

