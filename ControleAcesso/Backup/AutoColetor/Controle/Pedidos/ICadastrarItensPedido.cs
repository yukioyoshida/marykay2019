using System;
using AutoColetor;

public interface ICadastrarItensPedido
{
     void MS_Incluir(CTO_ItensPedido ITO_ItensPedido);
     void MS_Alterar(CTO_ItensPedido ITO_ItensPedido);
     void MS_Excluir(CTO_ItensPedido ITO_ItensPedido);
     CTO_ItensPedido MS_Obter(int pritObjRef);
     CTO_ItensPedido MS_Obter(string prstID);
     CTO_ItensPedido MS_ObterPeloNumeroPedido(string prstID);
}

