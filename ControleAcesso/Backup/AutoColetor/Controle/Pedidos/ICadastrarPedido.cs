using System;
using System.Data;
using AutoColetor;

public interface ICadastrarPedido
{
    void MS_Incluir(CTO_Pedido ITO_Pedido);
    void MS_Alterar(CTO_Pedido ITO_Pedido);
    void MS_Excluir(CTO_Pedido ITO_Pedido);
    CTO_Pedido MS_Obter(int pritObjRef);
    CTO_Pedido MS_Obter(string prstID);
    DataTable MS_ObterNovoPedido();
    CTO_Pedido MS_ObterPeloNumeroPedido(string stPedido);
    DataTable MS_ObterPedidosPorCliente(Int32 itIdCliente);
    DataTable MS_ObterQuantidadesPorItens(string stItem);
    DataTable MS_ObterQuantidadesPorItensPorCategoria(string stItem, string stCategoria);
    DataTable MS_ObterItensComprados(Int32 itIdVisitante);
    DataTable MS_ObterTodosOsPedidos();
    void MS_ConfirmaPagamento(string stNumeroPedido);
    void MS_CancelamentoPedido(string stNumeroPedido);
    void MS_AlterarValor(string stNumeroPedido, string stValor, string stObs);
    void MS_AlterarVencimento(string stNumeroPedido, string stValor, string stObs);
    void MS_CadastraFormaPagamento(string stNumeroPedido, string stFormaPagamento, string stNumeroParcelas);
}

