using System;
namespace AutoColetor 
{

    public class CTO_Pedido 
    {
          //  Declaracao dos atributos Puros
        int AP_ObjRef;
        int AP_IdVisitante;
        string AP_NumeroPedido;
        string AP_Situacao;
        DateTime AP_DataPedido;
        string AP_HoraPedido;
        string AP_ValorPedido;
        DateTime AP_DataVencimento;
        DateTime AP_Observacao;
        string AP_CPFCNPJSACADO;
        string AP_NomeSacado;
        string AP_EnderecoSacado;
        string AP_NumeroSacado;
        string AP_BairroSacado;
        string AP_CidadeSacado;
        string AP_UFSacado;
        string AP_CEPSacado;
        string AP_PaisSacado;
        DateTime AP_DataCancelamento;
        string AP_FormaPagamento;
        string AP_Categoria;
        string AP_Convite;
        string AP_ConviteEntregue;

        public CTO_Pedido () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public int getObjRef()
        {
            return AP_ObjRef;
        }
        public int getIdVisitante()
        {
            return AP_IdVisitante;
        }
        public string getNumeroPedido()
        {
            return AP_NumeroPedido;
        }
        public string getSituacao()
        {
            return AP_Situacao;
        }
        public DateTime getDataPedido()
        {
            return AP_DataPedido;
        }
        public string getHoraPedido()
        {
            return AP_HoraPedido;
        }
        public string getValorPedido()
        {
            return AP_ValorPedido;
        }
        public DateTime getDataVencimento()
        {
            return AP_DataVencimento;
        }
        public DateTime getObservacao()
        {
            return AP_Observacao;
        }
        public string getCPFCNPJSACADO()
        {
            return AP_CPFCNPJSACADO;
        }
        public string getNomeSacado()
        {
            return AP_NomeSacado;
        }
        public string getEnderecoSacado()
        {
            return AP_EnderecoSacado;
        }
        public string getNumeroSacado()
        {
            return AP_NumeroSacado;
        }
        public string getBairroSacado()
        {
            return AP_BairroSacado;
        }
        public string getCidadeSacado()
        {
            return AP_CidadeSacado;
        }
        public string getUFSacado()
        {
            return AP_UFSacado;
        }
        public string getCEPSacado()
        {
            return AP_CEPSacado;
        }
        public string getPaisSacado()
        {
            return AP_PaisSacado;
        }
        public DateTime getDataCancelamento()
        {
            return AP_DataCancelamento;
        }
        public string getFormaPagamento()
        {
            return AP_FormaPagamento;
        }
        public string getCategoria()
        {
            return AP_Categoria;
        }
        public string getConvite()
        {
            return AP_Convite;
        }
        public string getConviteEntregue()
        {
            return AP_ConviteEntregue;
        }
         //  Declaracao dos Metodos set
        public void setObjRef( int prObjRef)
        {
            AP_ObjRef = prObjRef;
        }
        public void setIdVisitante( int prIdVisitante)
        {
            AP_IdVisitante = prIdVisitante;
        }
        public void setNumeroPedido( string prNumeroPedido)
        {
            AP_NumeroPedido = prNumeroPedido;
        }
        public void setSituacao( string prSituacao)
        {
            AP_Situacao = prSituacao;
        }
        public void setDataPedido( DateTime prDataPedido)
        {
            AP_DataPedido = prDataPedido;
        }
        public void setHoraPedido( string prHoraPedido)
        {
            AP_HoraPedido = prHoraPedido;
        }
        public void setValorPedido( string prValorPedido)
        {
            AP_ValorPedido = prValorPedido;
        }
        public void setDataVencimento( DateTime prDataVencimento)
        {
            AP_DataVencimento = prDataVencimento;
        }
        public void setObservacao( DateTime prObservacao)
        {
            AP_Observacao = prObservacao;
        }
        public void setCPFCNPJSACADO( string prCPFCNPJSACADO)
        {
            AP_CPFCNPJSACADO = prCPFCNPJSACADO;
        }
        public void setNomeSacado( string prNomeSacado)
        {
            AP_NomeSacado = prNomeSacado;
        }
        public void setEnderecoSacado( string prEnderecoSacado)
        {
            AP_EnderecoSacado = prEnderecoSacado;
        }
        public void setNumeroSacado( string prNumeroSacado)
        {
            AP_NumeroSacado = prNumeroSacado;
        }
        public void setBairroSacado( string prBairroSacado)
        {
            AP_BairroSacado = prBairroSacado;
        }
        public void setCidadeSacado( string prCidadeSacado)
        {
            AP_CidadeSacado = prCidadeSacado;
        }
        public void setUFSacado( string prUFSacado)
        {
            AP_UFSacado = prUFSacado;
        }
        public void setCEPSacado( string prCEPSacado)
        {
            AP_CEPSacado = prCEPSacado;
        }
        public void setPaisSacado( string prPaisSacado)
        {
            AP_PaisSacado = prPaisSacado;
        }
        public void setDataCancelamento( DateTime prDataCancelamento)
        {
            AP_DataCancelamento = prDataCancelamento;
        }
        public void setFormaPagamento( string prFormaPagamento)
        {
            AP_FormaPagamento = prFormaPagamento;
        }
        public void setCategoria( string prCategoria)
        {
            AP_Categoria = prCategoria;
        }
        public void setConvite( string prConvite)
        {
            AP_Convite = prConvite;
        }
        public void setConviteEntregue( string prConviteEntregue)
        {
            AP_ConviteEntregue = prConviteEntregue;
        }
    }
}

