using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using AutoColetor;
namespace AutoColetor
{
    public class FWK_DAO_ItensPedido : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                           "          ObjRef, " +
                           "          IdPedido, " +
                           "          Descricao ";

        public FWK_DAO_ItensPedido()
        {
        }

        public void MS_Incluir(CTO_ItensPedido ITO_ItensPedido)
        {
            int itObjRef = MS_NovoObjRef();
            MI_SetSql ("INSERT INTO tblItensPedido (" +
                       "          ObjRef, " +
                       "          IdPedido, " +
                       "          Descricao) " + 
                       " VALUES (" + 
                       "          ?ObjRef, " +
                       "          ?IdPedido, " +
                       "          ?Descricao) ");

            MI_AddParameters("?ObjRef", itObjRef);
            MI_AddParameters("?IdPedido", ITO_ItensPedido.getIdPedido());
            MI_AddParameters("?Descricao", ITO_ItensPedido.getDescricao());

            MI_ExecuteNonQuery();
        }

        public void MS_Alterar(CTO_ItensPedido ITO_ItensPedido)
        {
            MI_SetSql ("UPDATE tblItensPedido SET " +
                       "          ObjRef = ?ObjRef, " +
                       "          IdPedido = ?IdPedido, " +
                       "          Descricao = ?Descricao " + 
                       "WHERE ObjRef = ?ObjRef ");
            MI_AddParameters("?ObjRef", ITO_ItensPedido.getObjRef());
            MI_AddParameters("?IdPedido", ITO_ItensPedido.getIdPedido());
            MI_AddParameters("?Descricao", ITO_ItensPedido.getDescricao());

            MI_ExecuteNonQuery();
        }

        public void MS_Excluir(CTO_ItensPedido ITO_ItensPedido)
        {
            MI_SetSql ("DELETE FROM tblItensPedido WHERE ObjRef = ?ObjRef");
            MI_AddParameters("?ObjRef", ITO_ItensPedido.getObjRef());
            
            MI_ExecuteNonQuery();
        }

        public CTO_ItensPedido MS_Obter(int pritObjRef)
        {
            CTO_ItensPedido ITO_ItensPedido = new CTO_ItensPedido();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          IdPedido, " +
                       "          Descricao " + 
                       "FROM tblItensPedido " + 
                       "WHERE Objref = ?ObjRef ");
            MI_AddParameters("?Objref", pritObjRef);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_ItensPedido = null;
            else
                ITO_ItensPedido = MI_DataSetToEntidade();
            return ITO_ItensPedido;
        }

        public CTO_ItensPedido MI_DataSetToEntidade()
        {
            CTO_ItensPedido ITO_ItensPedido = new CTO_ItensPedido();            
            try
            {
                ITO_ItensPedido.setObjRef(AP_DataReader.GetInt32(0));
            }
            catch
            {
                ITO_ItensPedido.setObjRef(0);
            }
            try
            {
                ITO_ItensPedido.setIdPedido(AP_DataReader.GetInt32(1));
            }
            catch
            {
                ITO_ItensPedido.setIdPedido(0);
            }
            try
            {
                ITO_ItensPedido.setDescricao(AP_DataReader.GetString(2));
            }
            catch
            {
                ITO_ItensPedido.setDescricao("");
            }
            return ITO_ItensPedido;
        }

        public CTO_ItensPedido MS_ObterTodos()
        {
            CTO_ItensPedido ITO_ItensPedido = new CTO_ItensPedido();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          IdPedido, " +
                       "          Descricao " + 
                       "FROM tblItensPedido " );
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_ItensPedido = null;
            else
                ITO_ItensPedido = MI_DataSetToEntidade();
            return ITO_ItensPedido;
        }

        public CTO_ItensPedido MS_ObterProximo()
        {
            CTO_ItensPedido ITO_ItensPedido = new CTO_ItensPedido();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_ItensPedido = null;
            else
                ITO_ItensPedido = MI_DataSetToEntidade();
                return ITO_ItensPedido;
        }

        public int MS_NovoObjRef()
        {
            int itObjRef;
            MI_SetSql("Select Max(ObjRef) from tblItensPedido");
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 1;
            }
            else
            {
                itObjRef = AP_DataReader.GetInt32(0);
                itObjRef++;
            }
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblItensPedido");
            return MI_ExecuteDataSet();
        }

    }
}

