using System;
using System.Data;
using AutoColetor;

namespace AutoColetor
{
    public class DAO_Pedido : FWK_DAO_Pedido
    {
        public DAO_Pedido()
        {
        }

        public CTO_Pedido MS_Obter(string prstID)
        {
            CTO_Pedido ITO_Pedido = new CTO_Pedido();
            MI_SetSql (AP_SelectALL + 
                       "FROM tblPedido " + 
                       "WHERE ObjRef = @ID ");
            MI_AddParameters("@ID", prstID);
            MI_AbrirConexao();
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Pedido = null;
            else
                ITO_Pedido = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Pedido;
        }

        public DataTable MS_ObterNovoPedido()
        {
            MI_SetSql("SELECT MAX(NumeroPedido + 1)NUMERO FROM tblPedido");
            return MI_ExecuteDataSet();
        }

        public CTO_Pedido MS_ObterPeloNumeroPedido(string stPedido)
        {
            CTO_Pedido ITO_Pedido = new CTO_Pedido();
            MI_SetSql(AP_SelectALL +
                       "FROM tblPedido " +
                       "WHERE NumeroPedido = @ID ");
            MI_AddParameters("@ID", stPedido);
            MI_AbrirConexao();
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Pedido = null;
            else
                ITO_Pedido = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Pedido;
        }

        public DataTable MS_ObterPedidosPorCliente(Int32 itIdCliente)
        {
            MI_SetSql("SELECT * FROM tblPedido Where          " +
                      "IdVisitante = @IdVisistante AND        " +
                      "Situacao <> @Situacao Order by ObjRef  ");
            MI_AddParameters("@IdVisistante", itIdCliente);
            MI_AddParameters("@Situacao", "CANCELADO");
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterQuantidadesPorItens(string stItem)
        {
            MI_SetSql("SELECT * FROM tblPedido as A, tblItensPedido as B WHERE    " +
                      "(A.Situacao <> 'CANCELADO' AND A.ObjRef = B.IdPedido) AND  " +
                      "(B.Descricao LIKE @Item )                                  ");
            MI_AddParameters("@Item", "%" + stItem + "%");
            return MI_ExecuteDataSet();
        }
        
        public DataTable MS_ObterQuantidadesPorItensPorCategoria(string stItem, string stCategoria)
        {
            MI_SetSql("SELECT * FROM tblPedido as A, tblItensPedido as B WHERE    " +
                      "(A.Situacao <> 'CANCELADO' AND A.ObjRef = B.IdPedido) AND  " +
                      "(B.Descricao LIKE @Item AND A.Categoria = @Categoria )     ");
            MI_AddParameters("@Item", "%" + stItem + "%");
            MI_AddParameters("@Categoria", stCategoria);
            return MI_ExecuteDataSet();
        }
        
        public DataTable MS_ObterItensComprados(Int32 itIdVisitante)
        {
            MI_SetSql("SELECT B.cdProduto FROM tblPedido AS A, tblItensPedido AS B WHERE " +
                      "(A.dsSituacao = 'PAGO' AND A.cdPedido = B.cdPedido) AND              " +
                      "A.cdCliente = @IdVisitante                                      ");
            MI_AddParameters("@IdVisitante", itIdVisitante);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterTodosOsPedidos()
        {
            MI_SetSql("SELECT * FROM tblPedido WHERE Situacao <> 'CANCELADO' Order by cdPedido");
            return MI_ExecuteDataSet();
        }

        public void MS_ConfirmaPagamento(string stNumeroPedido)
        {
            MI_SetSql("Update tblPedido SET               " +
                      "Situacao = @Situacao,              " +
                      "DataPagamento = curdate()          " +
                      "WHERE NumeroPedido = @NumeroPedido ");
            MI_AddParameters("@Situacao", "PAGO");
            MI_AddParameters("@NumeroPedido", stNumeroPedido);
            MI_AbrirConexao();
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_CancelamentoPedido(string stNumeroPedido)
        {
            MI_SetSql("Update tblPedido SET               " +
                      "Situacao = @Situacao,              " +
                      "DataCancelamento = curdate()       " +
                      "WHERE NumeroPedido = @NumeroPedido ");
            MI_AddParameters("@Situacao", "CANCELADO");
            MI_AddParameters("@NumeroPedido", stNumeroPedido);
            MI_AbrirConexao();
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_AlterarValor(string stNumeroPedido, string stValor, string stObs)
        {
            MI_SetSql("Update tblPedido SET               " +
                      "ValorPedido = @Valor,              " +
                      "Observacao = @Observacao           " +
                      "WHERE NumeroPedido = @NumeroPedido ");
            MI_AddParameters("@NumeroPedido", stNumeroPedido);
            MI_AddParameters("@Valor", stValor);
            MI_AddParameters("@Observacao", stObs);
            MI_AbrirConexao();
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }
        
        public void MS_AlterarVencimento(string stNumeroPedido, string stValor, string stObs)
        {
            MI_SetSql("Update tblPedido SET               " +
                      "DataVencimento = @Valor,           " +
                      "Observacao = @Observacao           " +
                      "WHERE NumeroPedido = @NumeroPedido ");
            MI_AddParameters("@NumeroPedido", stNumeroPedido);
            MI_AddParameters("@Valor", stValor);
            MI_AddParameters("@Observacao", stObs);
            MI_AbrirConexao();
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_CadastraFormaPagamento(string stNumeroPedido, string stFormaPagamento, string stNumeroParcelas)
        {
            MI_SetSql("Insert tblformaspagamento SET      " +
                      "NumeroPedido = @Pedido,            " +
                      "FormaPagamento = @Forma,           " +
                      "NumeroParcelas = @Numero,          " +
                      "Data = curdate()                   ");
            MI_AddParameters("@Pedido", stNumeroPedido);
            MI_AddParameters("@Forma", stFormaPagamento);
            MI_AddParameters("@Numero", stNumeroParcelas);
            MI_AbrirConexao();
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }
        
        public void MS_ConfirmaRecebimentoConvite(string stUsuario, string stPedido)
        {
            MI_SetSql("Insert tblRecebimentoConvite SET  " +
                      "USUARIO = @USUARIO,               " +
                      "PEDIDO = @PEDIDO,                 " +
                      "DATA = curdate()                  ");
            MI_AddParameters("@USUARIO", stUsuario);
            MI_AddParameters("@PEDIDO", stPedido);
            MI_AbrirConexao();
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_CadastraTipoPagamento(string stNumeroPedido, string stValor, string stFormaPagamento)
        {
            MI_SetSql("Insert tblTiposPagamento SET     " +
                      "NumeroPedido = @Pedido,          " +
                      "Valor = @Valor,                  " +
                      "FormaPagamento = @FormaPagamento ");
            MI_AddParameters("@Pedido", stNumeroPedido);
            MI_AddParameters("@Valor", stValor);
            MI_AddParameters("@FormaPagamento", stFormaPagamento);
            MI_AbrirConexao();
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }
    }
}

