using System;
using System.Data;
namespace AutoColetor
{
    public class DAO_Visitante : FWK_DAO_Visitante
    {
        public DAO_Visitante()
        {
        }


        public DataTable MS_ObterRegistroGrid(string prstChave)
        {
            MI_SetSql(AP_SelectALL +
                      "FROM tblVisitante             " +
                      "WHERE CodigoBarras Like @ID or   " +
                      "NomeCompleto Like @ID       or   " +
                      "CPF like @ID and DataOrigemAlteracao >= '2008-11-10'      ");
            MI_AddParameters("@ID", prstChave+"%");
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterRegistroGrid(string prstChave, string stCampoCodigo)
        {

            MI_SetSql(AP_SelectALL +
                      "FROM tblVisitante             " +
                      "WHERE " + stCampoCodigo + " = @ID and DataOrigemAlteracao >= '2008-11-10'      ");
            MI_AddParameters("@ID", prstChave);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterCampos()
        {
            MI_SetSql("SHOW FIELDS FROM tblvisitante");
            return MI_ExecuteDataSet();
        }

        public void MS_ConfirmaImpressao(Int64 prstCodigo)
        {
            MI_AbrirConexao();
            MI_SetSql("Update tblVisitante set " +
                      "EmissaoCracha = @Valor  " +
                      "Where CodigoBarras = @CodigoBarras  ");
            MI_AddParameters("@Valor", "S");
            MI_AddParameters("@CodigoBarras", prstCodigo);
            MI_ExecuteQuery();
            MI_FecharConexao();
        }
        public void MS_ConfirmaImpressaoF1(Int64 prstCodigo)
        {
            MI_AbrirConexao();
            MI_SetSql("Update tblVisitante set " +
                      "EmitidoF1 = @Valor  " +
                      "Where CodigoBarras = @CodigoBarras  ");
            MI_AddParameters("@Valor", "S");
            MI_AddParameters("@CodigoBarras", prstCodigo);
            MI_ExecuteQuery();
            MI_FecharConexao();
        }

        public void MS_ConfirmaImpressaoF2(Int64 prstCodigo)
        {
            MI_AbrirConexao();
            MI_SetSql("Update tblVisitante set " +
                      "EmitidoF2 = @Valor  " +
                      "Where CodigoBarras = @CodigoBarras  ");
            MI_AddParameters("@Valor", "S");
            MI_AddParameters("@CodigoBarras", prstCodigo);
            MI_ExecuteQuery();
            MI_FecharConexao();
        }

        public void MS_ConfirmaImpressaoF3(Int64 prstCodigo)
        {
            MI_AbrirConexao();
            MI_SetSql("Update tblVisitante set " +
                      "EmitidoF3 = @Valor  " +
                      "Where CodigoBarras = @CodigoBarras  ");
            MI_AddParameters("@Valor", "S");
            MI_AddParameters("@CodigoBarras", prstCodigo);
            MI_ExecuteQuery();
            MI_FecharConexao();
        }

        public void MS_ConfirmaImpressaoF4(Int64 prstCodigo)
        {
            MI_AbrirConexao();
            MI_SetSql("Update tblVisitante set " +
                      "EmitidoF4 = @Valor  " +
                      "Where CodigoBarras = @CodigoBarras  ");
            MI_AddParameters("@Valor", "S");
            MI_AddParameters("@CodigoBarras", prstCodigo);
            MI_ExecuteQuery();
            MI_FecharConexao();
        }

        public void MS_ConfirmaImpressaoF5(Int64 prstCodigo)
        {
            MI_AbrirConexao();
            MI_SetSql("Update tblVisitante set " +
                      "EmitidoF5 = @Valor  " +
                      "Where CodigoBarras = @CodigoBarras  ");
            MI_AddParameters("@Valor", "S");
            MI_AddParameters("@CodigoBarras", prstCodigo);
            MI_ExecuteQuery();
            MI_FecharConexao();
        }
        public DataTable MS_ObterRegistro(string prstPalavra, string prContendo, string stTipoBusca)
        {
            MI_SetSql("Select CodigoBarras as 'CODIGO',   " +
                      "CPF,                               " +
                      "NomeCompleto,                      " +
                      "RazaoSocial as 'EMPRESA',          " +
                      "NomeFantasia as 'FANTASIA',        " +
                      "Cargo as 'CARGO'                   " +
                      "From tblVisitante Where            " +
                      "" + stTipoBusca + " like @Valor    ");
            if (prContendo == "1")
            {
                MI_AddParameters("@Valor", "%" + prstPalavra + "%");
            }
            else
            {
                MI_AddParameters("@Valor", prstPalavra + "%");
            }
            return MI_ExecuteDataSet();
        }

        public string MS_GerarCodigoBarras()
        {
            Int64 itCodigoBarras = 0;
            MI_AbrirConexao();
            MI_SetSql("Select MAX(Chave)Valor From tblVisitante");
            MI_ExecuteQuery();
            try
            {
                itCodigoBarras = Convert.ToInt64(AP_DataReader.GetString(0));
                itCodigoBarras++;
            }
            catch
            {
                itCodigoBarras = 7000000;
            }
            MI_FecharConexao();
            return itCodigoBarras.ToString();
        }

        public DataTable MS_LocalizarRegistroPorFiltro(string prstCriterio, string prstAtributo)
        {
            MI_SetSql("SELECT " +
           "          NumeroTerminal, " +
           "          Chave, " +
           "          CodigoBarras, " +
           "          EmissaoCracha, " +
           "          PresenteDia1, " +
           "          PresenteDia2, " +
           "          PresenteDia3, " +
           "          PresenteDia4, " +
           "          PresenteDia5, " +
           "          OrigemInsercao, " +
           "          DataOrigemInsercao, " +
           "          HoraOrigemInsercao, " +
           "          UsuarioOrigemInsercao, " +
           "          OrigemAlteracao, " +
           "          DataOrigemAlteracao, " +
           "          HoraOrigemAlteracao, " +
           "          UsuarioOrigemAlteracao, " +
           "          MetodoInsercao, " +
           "          ParticipouEdicao, " +
           "          CategoriaRegistro, " +
           "          OrigemConvite, " +
           "          EmailLogin, " +
           "          CPF, " +
           "          Senha, " +
           "          Tratamento, " +
           "          Nome, " +
           "          Sobrenome, " +
           "          NomeCompleto, " +
           "          NomeCracha, " +
           "          Profissao, " +
           "          Cargo, " +
           "          Sexo, " +
           "          DiaNascimento, " +
           "          MesNascimento, " +
           "          AnoNascimento, " +
           "          DDICelular, " +
           "          DDDCelular, " +
           "          TelefoneCelular, " +
           "          SmsCelularPessoal, " +
           "          MmsCelularPessoal, " +
           "          EmailPessoal, " +
           "          AutorizaEnvioEmailPessoal, " +
           "          NomeEnderecoPessoal, " +
           "          NumeroEnderecoPessoal, " +
           "          ComplementoEnderecoPessoal, " +
           "          CepEnderecoPessoal, " +
           "          BairroEnderecoPessoal, " +
           "          CidadeEnderecoPessoal, " +
           "          EstadoEnderecoPessoal, " +
           "          PaisEnderecoPessoal, " +
           "          DDITelefonePessoal, " +
           "          DDDTelefonePessoal, " +
           "          NumeroTelefonePessoal, " +
           "          DDIFaxPessoal, " +
           "          DDDFaxPessoal, " +
           "          NumeroFaxPessoal, " +
           "          RazaoSocial, " +
           "          NomeFantasia, " +
           "          EmpresaCracha, " +
           "          Cnpj, " +
           "          NomeEnderecoComercial, " +
           "          NumeroEnderecoComercial, " +
           "          ComplementoEnderecoComercial, " +
           "          CepEnderecoComercial, " +
           "          BairroEnderecoComercial, " +
           "          CidadeEnderecoComercial, " +
           "          EstadoEnderecoComercial, " +
           "          PaisEnderecoComercial, " +
           "          DDITelefoneComercial, " +
           "          DDDTelefoneComercial, " +
           "          NumeroTelefoneComercial, " +
           "          DDIFaxComercial, " +
           "          DDDFaxComercial, " +
           "          NumeroFaxComercial, " +
           "          PaginaWebComercial, " +
           "          EmailComercial, " +
           "          AutorizaEnvioEmailComercial, " +
           "          Res1, " +
           "          Res2, " +
           "          Res3, " +
           "          Res4, " +
           "          Res5, " +
           "          ObjRef, " +
           "          Departamento, " +
           "          TipoEnderecoPessoal, " +
           "          InscricaoEstadual, " +
           "          TipoEnderecoComercial, " +
           "          Pergunta1, " +
           "          Pergunta1Outro, " +
           "          Pergunta2, " +
           "          Pergunta2Outro, " +
           "          Pergunta3, " +
           "          Pergunta3Outro, " +
           "          Pergunta4, " +
           "          Pergunta4Outro, " +
           "          Pergunta5, " +
           "          Pergunta5Outro, " +
           "          Pergunta6, " +
           "          Pergunta6Outro, " +
           "          Pergunta7, " +
           "          Pergunta7Outro, " +
           "          Pergunta8, " +
           "          Pergunta8Outro, " +
           "          Pergunta9, " +
           "          Pergunta9Outro, " +
           "          Pergunta10, " +
           "          Pergunta10Outro, " +
           "          Pergunta11, " +
           "          Pergunta11Outro, " +
           "          Pergunta12, " +
           "          Pergunta12Outro, " +
           "          Pergunta13, " +
           "          Pergunta13Outro, " +
           "          Pergunta14, " +
           "          Pergunta14Outro, " +
           "          Pergunta15, " +
           "          Pergunta15Outro, " +
           "          Pergunta16, " +
           "          Pergunta16Outro, " +
           "          Pergunta17, " +
           "          Pergunta17Outro, " +
           "          Pergunta18, " +
           "          Pergunta18Outro, " +
           "          Pergunta19, " +
           "          Pergunta19Outro, " +
           "          Pergunta20, " +
           "          Pergunta20Outro " +
           "FROM tblVisitante " +
           "WHERE " + prstCriterio + " LIKE @VALOR ");
            MI_AddParameters("@VALOR", prstAtributo+"%");
            return MI_ExecuteDataSet();
        }


        public CTO_Visitante MS_ObterPorCodigo(string prstCodigo)
        {
            MI_AbrirConexao();

            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql(AP_SelectALL +
            "FROM tblVisitante " +
            "WHERE CodigoBarras = @CodigoBarras limit 1");
            MI_AddParameters("@CodigoBarras", prstCodigo);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Visitante = null;
            else
                ITO_Visitante = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Visitante;
        }

        public void MS_PresenteEvento(string prstCodigo, string stDia)
        {
            MI_AbrirConexao();
            MI_SetSql("Update tblVisitante set " +
                      "PresenteEvento = 'S', " + stDia + "='S'   " +
                      "Where CodigoBarras = @CodigoBarras  ");
            MI_AddParameters("@CodigoBarras", prstCodigo);
            MI_ExecuteQuery();
            MI_FecharConexao();
        }
        
        public CTO_Visitante MS_ObterPorCPF(string prstCPF)
        {
            MI_AbrirConexao();
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql (AP_SelectALL + 
            "FROM tblVisitante " + 
            "WHERE CPF = @CPF  ");
            MI_AddParameters("@CPF", prstCPF);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Visitante = null;
            else
                ITO_Visitante = MI_DataSetToEntidade();
            
            MI_FecharConexao();
            return ITO_Visitante;
        }

        public DataTable MS_ObterPorCPFTabela(string prstCPF)
        {
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql(AP_SelectALL +
            "FROM tblVisitante " +
            "WHERE CPF = @CPF  ");
            MI_AddParameters("@CPF", prstCPF);
            return MI_ExecuteDataSet();
        }

        public CTO_Visitante MS_ObterPorNome(string prstNome)
        {
            MI_AbrirConexao();
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql(AP_SelectALL +
            "FROM tblVisitante           " +
            "WHERE NomeCompleto = @NOME  ");
            MI_AddParameters("@NOME", prstNome);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Visitante = null;
            else
                ITO_Visitante = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Visitante;
        }

        public DataTable MS_ObterPorNomeTabela(string prstNome)
        {
            MI_SetSql(AP_SelectALL +
            "FROM tblVisitante           " +
            "WHERE NomeCompleto = @NOME  ");
            MI_AddParameters("@NOME", prstNome);
            return MI_ExecuteDataSet();
        }

        public CTO_Visitante MS_ObterPorEmail(string prstEmail)
        {
            MI_AbrirConexao();
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql (AP_SelectALL + 
            "FROM tblVisitante " +
            "WHERE EmailLogin = @EmailLogin ");
            MI_AddParameters("@EmailLogin", prstEmail);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Visitante = null;
            else
                ITO_Visitante = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Visitante;
        }

        public DataTable MI_GerarNumeroInscricao(string stTipoForum)
        {
            MI_SetSql("Select ObjRef from tblvisitante where " + stTipoForum + "='S' ");
            return MI_ExecuteDataSet();
        }
        public DataTable MS_ObterRespostaSaida(string prstCodigoBarras)
        {
            MI_SetSql("Select CodigoBarras, PerguntaSaida1, PerguntaSaida2 from tblvisitante where " + 
                      "CodigoBarras = @ID limit 1                                                  ");
            MI_AddParameters("@ID", prstCodigoBarras);
            return MI_ExecuteDataSet();
        }

        public void MS_SalvarPerguntas(string prstCodigoBarras, string prstPergunta1, string prstPergunta2)
        {
            MI_SetSql("UPDATE tblvisitante SET           " +
                      "PerguntaSaida1 = @Pergunta1,      " +
                      "PerguntaSaida2 = @Pergunta2 WHERE " +
                      "CodigoBarras = @ID                ");
            MI_AddParameters("@Pergunta1", prstPergunta1);
            MI_AddParameters("@Pergunta2", prstPergunta2);
            MI_AddParameters("@ID", prstCodigoBarras);
            MI_AbrirConexao();
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public DataTable MS_ObterQuestionario()
        {
            MI_SetSql("Select RealizaPerguntaColetorFeira From questionario  ");
            return MI_ExecuteDataSet();
        }
        public DataTable MS_ObterPorCodigoDat(string prstCodigo)
        {
            //MI_SetSql("SELECT TOP(1) ObjRef FROM tblVisitante " +
            //"WHERE CodigoBarras = @CodigoBarras AND Pergunta20 = '" + MI_ObterDia() + "' ");

            MI_SetSql("SELECT TOP(1) ObjRef FROM tblVisitante " +
            "WHERE CodigoBarras = @CodigoBarras ");

            
            MI_AddParameters("@CodigoBarras", prstCodigo);
            
            return MI_ExecuteDataSet();
        }

        private string MI_ObterDia()
        {
            string stEvento = "";
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));
            dt.Columns.Add("TERMINAL", typeof(string));
            dt.Columns.Add("DIA", typeof(string));
            dt.Columns.Add("FLUXO", typeof(string));
            dt.Columns.Add("EVENTO", typeof(string));

            dt.ReadXml("configuracoes.xml");
            dr = dt.Rows[0];



            return stEvento = dr["EVENTO"].ToString();
        }

        public DataTable MS_ObterCodigoBarrasColetor(string prCodigoBarras)
        {
            MI_SetSql("SELECT TOP(1) * FROM tblCodigoBarrasImpressao WHERE CodigoBarrasImpresso = @CodigoBarras ");

            MI_AddParameters("@CodigoBarras", prCodigoBarras);

            return MI_ExecuteDataSet();            
        }

        public DataTable MS_ObterNomeProduto(string CdProduto)
        {
            MI_SetSql("SELECT TOP(1) dsProduto FROM tblProduto WHERE cdProduto = @cdProduto");
            MI_AddParameters("@cdProduto", CdProduto);

            return MI_ExecuteDataSet();                 
        }
    }
}

