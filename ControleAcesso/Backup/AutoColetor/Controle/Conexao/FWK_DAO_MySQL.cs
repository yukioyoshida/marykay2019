using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace AutoColetor
{
    public class FWK_DAO_MySQL
    {
        private bool AP_EOF = true;
        public SqlDataAdapter adapter = new SqlDataAdapter();
        public SqlDataReader AP_DataReader;
        public String ComandoSQL;
        public List<FWK_DAO_Parametro> Parametros = new List<FWK_DAO_Parametro>();

        public SqlConnection AP_Connection = new FactoryConnection().getConnection();

        protected void MI_AbrirConexao()
        {
            AP_Connection.Open();
        }

        protected void MI_FecharConexao()
        {
            AP_Connection.Close();
        }

        protected void MI_SetSql(String prstSql)
        {
            ComandoSQL = prstSql;
            Parametros.Clear();
        }

        protected void MI_AddParameters(String prstName, Object probValue)
        {
            FWK_DAO_Parametro Parm = new FWK_DAO_Parametro();
            Parm.setNome(prstName);
            Parm.setValor(probValue);
            Parametros.Add(Parm);
        }

        protected void MI_ExecuteNonQuery()
        {
            //MySqlConnection AP_Connection = new FactoryConnection().getMySqlConnection();
            //AP_Connection.Open();
            SqlCommand AP_Command = new SqlCommand(ComandoSQL, AP_Connection);

            foreach (FWK_DAO_Parametro i in Parametros)
                AP_Command.Parameters.AddWithValue(i.getNome(), i.getValor());

            try
            {
                AP_Command.Transaction = AP_Connection.BeginTransaction(System.Data.IsolationLevel.Serializable);
                AP_Command.ExecuteNonQuery();
                AP_Command.Transaction.Commit();
                //AP_Connection.Close();  
            }
            catch (Exception e)
            {
                AP_Command.Transaction.Rollback();
                //AP_Connection.Close();
                throw e;
            }
        }

        protected void MI_ExecuteQuery()
        {
            //MySqlConnection AP_Connection = new FactoryConnection().getMySqlConnection();
            //AP_Connection.Open();
            SqlCommand AP_Command = new SqlCommand(ComandoSQL, AP_Connection);

            foreach (FWK_DAO_Parametro i in Parametros)
                AP_Command.Parameters.AddWithValue(i.getNome(), i.getValor());

            try
            {
                AP_DataReader = AP_Command.ExecuteReader(CommandBehavior.SingleResult);
                adapter.SelectCommand = AP_Command;
                MI_ProximoRegistro();
            }
            catch (Exception e)
            {
                AP_DataReader.Close();
            }
        }

        protected DataTable MI_ExecuteDataSet()
        {
            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;

            //MySqlConnection AP_Connection = new FactoryConnection().getMySqlConnection();
            AP_Connection.Open();
          
            SqlCommand AP_Command = new SqlCommand(ComandoSQL, AP_Connection);

            foreach (FWK_DAO_Parametro i in Parametros)
                AP_Command.Parameters.AddWithValue(i.getNome(), i.getValor());

            try
            {
                //AP_DataReader = AP_Command.ExecuteReader(CommandBehavior.SingleResult);
                adapter.SelectCommand = AP_Command;
                adapter.Fill(table);
                AP_Connection.Close();  
            }
            catch (Exception e)
            {
                AP_DataReader.Close();
            }

            return table;
        }

        protected void MI_ProximoRegistro()
        {
            AP_EOF = !AP_DataReader.Read();
        }


        protected bool MI_EOF()
        {
            return AP_EOF;
        }
    }
}



