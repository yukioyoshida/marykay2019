using System;
using AutoColetor;

public abstract class CAB_CadastrarColetor : ICadastrarColetor
{
    public abstract void MS_Incluir(CTO_Coletor ITO_Coletor, string prProduto);
     public abstract void MS_Alterar(CTO_Coletor ITO_Coletor);
     public abstract void MS_Excluir(CTO_Coletor ITO_Coletor);
     public abstract CTO_Coletor MS_Obter(int pritObjRef);
     public abstract CTO_Coletor MS_Obter(string prstID);
     public abstract CTO_Coletor MS_ObterPeloCodigo(string prstID);
}

