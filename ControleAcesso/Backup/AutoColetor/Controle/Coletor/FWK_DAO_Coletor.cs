using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AutoColetor;

namespace AutoColetor
{
    public class FWK_DAO_Coletor : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                           "          ObjRef, " +
                           "          Coletor, " +
                           "          Codigo, " +
                           "          Fluxo, " +
                           "          DataEntrada, " +
                           "          HoraEntrada, " +
                           "          DataSaida, " +
                           "          HoraSaida, " +
                           "          Permanencia ";

        public FWK_DAO_Coletor()
        {
        }

        public void MS_Incluir(CTO_Coletor ITO_Coletor, string prProduto)
        {
            //int itObjRef = MS_NovoObjRef();
            MI_SetSql ("INSERT INTO tblColetorFeira (" +
                       //"          ObjRef, " +
                       "          Coletor, " +
                       "          Codigo, " +
                       "          Fluxo, " +
                       "          DataEntrada, " +
                       "          HoraEntrada, " +
                       "          DataSaida, " +
                       "          HoraSaida, " +
                       "          Permanencia, " +
                       "          AutorizadoPor, " +
                       "          EntradaConcedida, " +
                       "          ProdutoAutorizado, " +
                       "          Evento) " +
                       " VALUES (" + 
                       //"          @ObjRef, " +
                       "          @Coletor, " +
                       "          @Codigo, " +
                       "          @Fluxo, " +
                       "          CAST(GETDATE() AS DATE), " +
                       "          CAST(GETDATE() AS TIME), " +

                       "          CAST(GETDATE() AS DATE), " +
                       "          CAST(GETDATE() AS TIME), " +

                       //"          @DataSaida, " +
                       //"          @HoraSaida, " +
                       "          @Permanencia, " +
                       "          @AutorizadoPor, " +
                       "          @EntradaConcedida, " +
                       "          @ProdutoAutorizado, " +
                       "          @Evento) ");

            //MI_AddParameters("@ObjRef", itObjRef);
            MI_AddParameters("@Coletor", ITO_Coletor.getColetor());
            MI_AddParameters("@Codigo", ITO_Coletor.getCodigo());
            MI_AddParameters("@Fluxo", ITO_Coletor.getFluxo());
            //MI_AddParameters("@DataEntrada", ITO_Coletor.getDataEntrada());
            //MI_AddParameters("@HoraEntrada", ITO_Coletor.getHoraEntrada());
            //MI_AddParameters("@DataSaida", ITO_Coletor.getDataSaida());
            //MI_AddParameters("@HoraSaida", ITO_Coletor.getHoraSaida());
            
            //MI_AddParameters("@Permanencia", ITO_Coletor.getPermanencia());
            MI_AddParameters("@Permanencia", "");

            MI_AddParameters("@AutorizadoPor", ITO_Coletor.getAutorizadoPor());
            MI_AddParameters("@EntradaConcedida", ITO_Coletor.getEntradaConcedida());
            MI_AddParameters("@ProdutoAutorizado", prProduto);
            MI_AddParameters("@Evento", MI_ObterEvento());

            MI_AbrirConexao();
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        private string MI_ObterEvento()
        {
            string stEvento = "";
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));
            dt.Columns.Add("TERMINAL", typeof(string));
            dt.Columns.Add("DIA", typeof(string));
            dt.Columns.Add("FLUXO", typeof(string));
            dt.Columns.Add("EVENTO", typeof(string));

            dt.ReadXml("configuracoes.xml");
            dr = dt.Rows[0];

            return stEvento = dr["EVENTO"].ToString();
        }

        public void MS_Alterar(CTO_Coletor ITO_Coletor)
        {
            MI_SetSql ("UPDATE tblColetorFeira SET " +
                       "          ObjRef = @ObjRef, " +
                       "          Coletor = @Coletor, " +
                       "          Codigo = @Codigo, " +
                       "          Fluxo = @Fluxo, " +
                       "          DataEntrada = @DataEntrada, " +
                       "          HoraEntrada = @HoraEntrada, " +
                       "          DataSaida = curdate(), " +
                       "          HoraSaida = curtime(), " +
                       "          Permanencia = @Permanencia " + 
                       "WHERE ObjRef = @ObjRef ");
            MI_AddParameters("@ObjRef", ITO_Coletor.getObjRef());
            MI_AddParameters("@Coletor", ITO_Coletor.getColetor());
            MI_AddParameters("@Codigo", ITO_Coletor.getCodigo());
            MI_AddParameters("@Fluxo", ITO_Coletor.getFluxo());
            MI_AddParameters("@DataEntrada", ITO_Coletor.getDataEntrada());
            MI_AddParameters("@HoraEntrada", ITO_Coletor.getHoraEntrada());
            MI_AddParameters("@DataSaida", ITO_Coletor.getDataSaida());
            MI_AddParameters("@HoraSaida", ITO_Coletor.getHoraSaida());
            MI_AddParameters("@Permanencia", ITO_Coletor.getPermanencia());
            MI_AbrirConexao();
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_Excluir(CTO_Coletor ITO_Coletor)
        {
            MI_SetSql ("DELETE FROM tblColetorFeira WHERE ObjRef = @ObjRef");
            MI_AddParameters("@ObjRef", ITO_Coletor.getObjRef());
            
            MI_ExecuteNonQuery();
        }

        public CTO_Coletor MS_Obter(int pritObjRef)
        {
            CTO_Coletor ITO_Coletor = new CTO_Coletor();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          Coletor, " +
                       "          Codigo, " +
                       "          Fluxo, " +
                       "          DataEntrada, " +
                       "          HoraEntrada, " +
                       "          DataSaida, " +
                       "          HoraSaida, " +
                       "          Permanencia " + 
                       "FROM tblColetorFeira " + 
                       "WHERE Objref = @ObjRef ");
            MI_AddParameters("@Objref", pritObjRef);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Coletor = null;
            else
                ITO_Coletor = MI_DataSetToEntidade();
            return ITO_Coletor;
        }

        public CTO_Coletor MI_DataSetToEntidade()
        {
            CTO_Coletor ITO_Coletor = new CTO_Coletor();            
            try
            {
                ITO_Coletor.setObjRef(AP_DataReader.GetInt32(0));
            }
            catch
            {
                ITO_Coletor.setObjRef(0);
            }
            try
            {
                ITO_Coletor.setColetor(AP_DataReader.GetString(1));
            }
            catch
            {
                ITO_Coletor.setColetor("");
            }
            try
            {
                ITO_Coletor.setCodigo(AP_DataReader.GetString(2));
            }
            catch
            {
                ITO_Coletor.setCodigo("");
            }
            try
            {
                ITO_Coletor.setFluxo(AP_DataReader.GetString(3));
            }
            catch
            {
                ITO_Coletor.setFluxo("");
            }
            try
            {
            ITO_Coletor.setDataEntrada(AP_DataReader.GetDateTime(4));
            }
            catch
            {
            }
            try
            {
                ITO_Coletor.setHoraEntrada(AP_DataReader.GetString(5));
            }
            catch
            {
                ITO_Coletor.setHoraEntrada("");
            }
            try
            {
            ITO_Coletor.setDataSaida(AP_DataReader.GetDateTime(6));
            }
            catch
            {
            }
            try
            {
                ITO_Coletor.setHoraSaida(AP_DataReader.GetString(7));
            }
            catch
            {
                ITO_Coletor.setHoraSaida("");
            }
            try
            {
                ITO_Coletor.setPermanencia(AP_DataReader.GetString(8));
            }
            catch
            {
                ITO_Coletor.setPermanencia("");
            }
            return ITO_Coletor;
        }

        public CTO_Coletor MS_ObterTodos()
        {
            CTO_Coletor ITO_Coletor = new CTO_Coletor();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          Coletor, " +
                       "          Codigo, " +
                       "          Fluxo, " +
                       "          DataEntrada, " +
                       "          HoraEntrada, " +
                       "          DataSaida, " +
                       "          HoraSaida, " +
                       "          Permanencia " + 
                       "FROM tblColetorFeira " );
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Coletor = null;
            else
                ITO_Coletor = MI_DataSetToEntidade();
            return ITO_Coletor;
        }

        public CTO_Coletor MS_ObterProximo()
        {
            CTO_Coletor ITO_Coletor = new CTO_Coletor();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_Coletor = null;
            else
                ITO_Coletor = MI_DataSetToEntidade();
                return ITO_Coletor;
        }

        public int MS_NovoObjRef()
        {
            int itObjRef;
            MI_SetSql("Select Max(ObjRef) from tblColetorFeira");
            MI_AbrirConexao();
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 1;
            }
            else
            {
                itObjRef = AP_DataReader.GetInt32(0);
                itObjRef++;
            }
            MI_FecharConexao();
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblColetorFeira");
            return MI_ExecuteDataSet();
        }

    }
}

