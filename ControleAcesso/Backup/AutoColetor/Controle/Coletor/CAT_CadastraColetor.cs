using System;
using System.Data;
using AutoColetor;

public class CAT_CadastrarColetor : CAB_CadastrarColetor
{
     public CAT_CadastrarColetor()
     {
     }
     public override void MS_Incluir(CTO_Coletor ITO_Coletor, string prProduto)
     {
          DAO_Coletor IDAO_Coletor = new DAO_Coletor();
          IDAO_Coletor.MS_Incluir(ITO_Coletor, prProduto);
     }
     public override void MS_Alterar(CTO_Coletor ITO_Coletor)
     {
         DAO_Coletor IDAO_Coletor = new DAO_Coletor();
         IDAO_Coletor.MS_Alterar(ITO_Coletor);
     }
     public override void MS_Excluir(CTO_Coletor ITO_Coletor)
     {
         DAO_Coletor IDAO_Coletor = new DAO_Coletor();
         IDAO_Coletor.MS_Excluir(ITO_Coletor);
     }
     public override CTO_Coletor MS_Obter(int pritObjRef)
     {
           DAO_Coletor IDAO_Coletor = new DAO_Coletor();
           return IDAO_Coletor.MS_Obter(pritObjRef);
     }
     public override CTO_Coletor MS_Obter(string prstID)
     {
          DAO_Coletor IDAO_Coletor = new DAO_Coletor();
          return IDAO_Coletor.MS_Obter(prstID);
     }

     public override CTO_Coletor MS_ObterPeloCodigo(string prstID)
     {
         DAO_Coletor IDAO_Coletor = new DAO_Coletor();
         return IDAO_Coletor.MS_ObterPeloCodigo(prstID);
     }

     public CTO_Coletor MS_ObterPeloCodigoETerminal(string prstID, string stNumeroTerminal)
     {
         DAO_Coletor IDAO_Coletor = new DAO_Coletor();
         return IDAO_Coletor.MS_ObterPeloCodigoETerminal(prstID, stNumeroTerminal);
     }

     public DataTable MS_ObterPeloCodigoChave(string prstID)
     {
         DAO_Coletor IDAO_Coletor = new DAO_Coletor();
         return IDAO_Coletor.MS_ObterPeloCodigoChave(prstID);
     }
}

