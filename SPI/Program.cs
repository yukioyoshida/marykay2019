using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SPI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // OBTER INFORMA��ES
            CSV_VariaveisGlobal Variaveis = new CSV_VariaveisGlobal();
            if (Variaveis.getVersao() != new DAO_Visitante().MS_ObterVersao())
            {
                MessageBox.Show("Sistema desatualizado", "Acesso n�o autorizado");
                Application.ExitThread();
            }
            else
            {
                Variaveis.setSeminario(new DAO_Visitante().MS_ObterSeminario());
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new CLT_Default());
            }
        }
    }
}