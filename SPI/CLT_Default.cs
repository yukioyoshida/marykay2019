﻿using System;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_Default : Form
    {
        public CLT_Default()
        {
            InitializeComponent();
        }

        CSV_VariaveisGlobal CSV_Glogal = new CSV_VariaveisGlobal();

        private void CLT_Default_Load(object sender, EventArgs e)
        {
            CLT_Login ABRIR = new CLT_Login();
            ABRIR.ShowDialog();

            if (ABRIR.AP_Login == true)
            {
                if (!CSV_Glogal.getPrioridade().Equals("5"))
                {
                    MI_IniciarOperacao();
                    Application.ExitThread();
                }
                else
                {
                    CLT_RelatorioProduto abrirRelatorio = new CLT_RelatorioProduto();
                    abrirRelatorio.ShowDialog();
                    CSV_Glogal.MI_LogOutUsuario();
                    Application.ExitThread();
                }
            }
            else
            {
                CSV_Glogal.MI_LogOutUsuario();
                Application.ExitThread();
            }

        }

        private void MI_IniciarOperacao()
        {
            this.Hide();
            CLT_DadosPessoais ABRIR = new CLT_DadosPessoais();
            ABRIR.ShowDialog();
        }
    }
}
