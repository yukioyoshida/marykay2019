using System;
using System.Data;
using SPI;

public interface ICadastrarItensPedido
{
     void MS_Incluir(CTO_ItensPedido ITO_ItensPedido);
     void MS_Alterar(CTO_ItensPedido ITO_ItensPedido);
     void MS_Excluir(CTO_ItensPedido ITO_ItensPedido);
     CTO_ItensPedido MS_Obter(int pritObjRef);
     CTO_ItensPedido MS_Obter(string prstID);
     DataTable MI_ObterItensPorPedido(int idPedido);
}

