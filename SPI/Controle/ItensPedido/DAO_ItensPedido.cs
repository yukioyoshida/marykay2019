using System.Data;
namespace SPI
{
    public class DAO_ItensPedido : FWK_DAO_ItensPedido
    {
        public DAO_ItensPedido()
        {
        }

        public CTO_ItensPedido MS_Obter(string prstID)
        {
            CTO_ItensPedido ITO_ItensPedido = new CTO_ItensPedido();
            MI_SetSql (AP_SelectALL + 
                       "FROM tblItensPedido " + 
                       "WHERE ID = @ID ");
            MI_AddParameters("@ID", prstID);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_ItensPedido = null;
            else
                ITO_ItensPedido = MI_DataSetToEntidade();
            return ITO_ItensPedido;
        }

        public DataTable MI_ObterItensPorPedido(int idPedido)
        {
            MI_SetSql(" SELECT C.* " +
                         " FROM tblpedido AS A, tblItensPedido AS B , tblProduto AS C " +
                         " WHERE " +
                         " A.CDPEDIDO = @IDPEDIDO " +
                         " AND A.DSSITUACAO <> 'CANCELADO' " +
                         " AND B.CDPRODUTO = C.CDPRODUTO " +
                         " AND A.CDPEDIDO = B.CDPEDIDO ");

            MI_AddParameters("@IDPEDIDO", idPedido);

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterInscritosPorPalestra(string stCodigoPalestra)
        {
            MI_SetSql(  "SELECT NOMECOMPLETO AS NOME, DDDTELEFONECOMERCIAL AS DDD, NUMEROTELEFONECOMERCIAL AS TELEFONE, EMAILCOMERCIAL AS EMAIL " +
                        "FROM TBLPRODUTO, TBLITENSPEDIDO AS A INNER JOIN TBLPEDIDO AS B " +
                        "ON B.CDPEDIDO = A.CDPEDIDO AND " +
                        "B.DSSITUACAO <> 'CANCELADO' " +
                        "INNER JOIN TBLVISITANTE AS C " +
                        "ON C.OBJREF = B.CDCLIENTE AND " +
                        "B.CDPEDIDO = A.CDPEDIDO " +
                        "WHERE A.CDPRODUTO = TBLPRODUTO.CDPRODUTO AND " +
                        "A.CDPRODUTO = @Codigo ");
            MI_AddParameters("@Codigo", stCodigoPalestra);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterItensPorPedido(string stCodigo)
        {
            MI_SetSql("SELECT                                                  " +
                      "PRO.cdProduto,                                          " +
                      "PRO.Sigla,                                              " +
                      "PRO.dtPalestra,                                         " +
                      "PRO.dsProduto                                           " +
                      "FROM                                                    " +
                      "tblitenspedido AS ITENS INNER JOIN tblproduto AS PRO ON " +
                      "ITENS.CdProduto = PRO.cdProduto                         " +
                      "WHERE                                                   " +
                      "ITENS.cdPedido = @Codigo                                ");
            MI_AddParameters("@Codigo", stCodigo);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterProdutosCompradosPorCliente(int prCdCliente)
        {
            MI_SetSql("SELECT                           " +
                        "    tblItensPedido.cdProduto,  " +
                        "    tblproduto.Sigla           " +
                        "FROM " +
                        "    tblItensPedido " +
                        "        INNER JOIN tblPedido ON " +
                        "            tblPedido.cdPedido = tblItensPedido.cdPedido " +
                        "        INNER JOIN tblproduto ON " +
                        "            tblproduto.cdProduto = tblItensPedido.cdProduto " +
                        "WHERE " +
                        "            tblPedido.dsSituacao <> 'CANCELADO' AND " +
                        "            tblpedido.cdCliente = @Cliente");

            MI_AddParameters("@Cliente", prCdCliente);
            return MI_ExecuteDataSet();
        }
    }
}

