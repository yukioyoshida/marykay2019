using System.Data;
namespace SPI
{
    public class FWK_DAO_ItensPedido : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                           "          cdItem, " +
                           "          cdPedido, " +
                           "          cdProduto, " +
                           "          dsValor ";

        public FWK_DAO_ItensPedido()
        {
        }

        public void MS_Incluir(CTO_ItensPedido ITO_ItensPedido)
        {
            int itObjRef = MS_NovoObjRef();
            MI_SetSql ("INSERT INTO tblItensPedido (" +
                       "          cdPedido, " +
                       "          cdProduto, " +
                       "          dsValor) " + 
                       " VALUES (" + 
                       "          @cdPedido, " +
                       "          @cdProduto, " +
                       "          @dsValor) ");
            
            MI_AddParameters("@cdPedido", ITO_ItensPedido.getcdPedido());
            MI_AddParameters("@cdProduto", ITO_ItensPedido.getcdProduto());
            MI_AddParameters("@dsValor", ITO_ItensPedido.getdsValor());
            
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_Alterar(CTO_ItensPedido ITO_ItensPedido)
        {
            MI_SetSql ("UPDATE tblItensPedido SET " +
                       "          cdItem = @cdItem, " +
                       "          cdPedido = @cdPedido, " +
                       "          cdProduto = @cdProduto, " +
                       "          dsValor = @dsValor " +
                       "WHERE cdItem = @ObjRef ");
            MI_AddParameters("@cdItem", ITO_ItensPedido.getcdItem());
            MI_AddParameters("@cdPedido", ITO_ItensPedido.getcdPedido());
            MI_AddParameters("@cdProduto", ITO_ItensPedido.getcdProduto());
            MI_AddParameters("@dsValor", ITO_ItensPedido.getdsValor());

            MI_ExecuteNonQuery();
        }

        public void MS_Excluir(CTO_ItensPedido ITO_ItensPedido)
        {
            MI_SetSql("DELETE FROM tblItensPedido WHERE cdItem = @cdItem");
            MI_AddParameters("@ObjRef", ITO_ItensPedido.getcdItem());
            
            MI_ExecuteNonQuery();
        }

        public CTO_ItensPedido MS_Obter(int pritObjRef)
        {
            CTO_ItensPedido ITO_ItensPedido = new CTO_ItensPedido();
            MI_SetSql ("SELECT " + 
                       "          cdItem, " +
                       "          cdPedido, " +
                       "          cdProduto, " +
                       "          dsValor " + 
                       "FROM tblItensPedido " +
                       "WHERE cdItem = @ObjRef ");
            MI_AddParameters("@Objref", pritObjRef);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_ItensPedido = null;
            else
                ITO_ItensPedido = MI_DataSetToEntidade();
            return ITO_ItensPedido;
        }

        public CTO_ItensPedido MI_DataSetToEntidade()
        {
            CTO_ItensPedido ITO_ItensPedido = new CTO_ItensPedido();            
            try
            {
                ITO_ItensPedido.setcdItem(AP_DataReader.GetInt32(0));
            }
            catch
            {
                ITO_ItensPedido.setcdItem(0);
            }
            try
            {
                ITO_ItensPedido.setcdPedido(AP_DataReader.GetInt32(1));
            }
            catch
            {
                ITO_ItensPedido.setcdPedido(0);
            }
            try
            {
                ITO_ItensPedido.setcdProduto(AP_DataReader.GetString(2));
            }
            catch
            {
                ITO_ItensPedido.setcdProduto("");
            }
            try
            {
                ITO_ItensPedido.setdsValor(AP_DataReader.GetString(3));
            }
            catch
            {
                ITO_ItensPedido.setdsValor("");
            }
            return ITO_ItensPedido;
        }

        public CTO_ItensPedido MS_ObterTodos()
        {
            CTO_ItensPedido ITO_ItensPedido = new CTO_ItensPedido();
            MI_SetSql ("SELECT " + 
                       "          cdItem, " +
                       "          cdPedido, " +
                       "          cdProduto, " +
                       "          dsValor " + 
                       "FROM tblItensPedido " );
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_ItensPedido = null;
            else
                ITO_ItensPedido = MI_DataSetToEntidade();
            return ITO_ItensPedido;
        }

        public CTO_ItensPedido MS_ObterProximo()
        {
            CTO_ItensPedido ITO_ItensPedido = new CTO_ItensPedido();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_ItensPedido = null;
            else
                ITO_ItensPedido = MI_DataSetToEntidade();
                return ITO_ItensPedido;
        }

        public int MS_NovoObjRef()
        {
            int itObjRef;
            MI_SetSql("Select Max(cdItem) from tblItensPedido");
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 1;
            }
            else
            {
                itObjRef = AP_DataReader.GetInt32(0);
                itObjRef++;
            }
            MI_FecharConexao();
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblItensPedido");
            return MI_ExecuteDataSet();
        }

    }
}

