using System;
using System.Data;
using SPI;

public abstract class CAB_CadastrarItensPedido : ICadastrarItensPedido
{
     public abstract void MS_Incluir(CTO_ItensPedido ITO_ItensPedido);
     public abstract void MS_Alterar(CTO_ItensPedido ITO_ItensPedido);
     public abstract void MS_Excluir(CTO_ItensPedido ITO_ItensPedido);
     public abstract CTO_ItensPedido MS_Obter(int pritObjRef);
     public abstract CTO_ItensPedido MS_Obter(string prstID);
     public abstract DataTable MI_ObterItensPorPedido(int idPedido);
}

