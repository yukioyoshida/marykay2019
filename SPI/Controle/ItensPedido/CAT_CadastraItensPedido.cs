using System;
using System.Data;
using SPI;

public class CAT_CadastrarItensPedido : CAB_CadastrarItensPedido
{
     public CAT_CadastrarItensPedido()
     {
     }
     public override void MS_Incluir(CTO_ItensPedido ITO_ItensPedido)
     {
          DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
          IDAO_ItensPedido.MS_Incluir(ITO_ItensPedido);
     }
     public override void MS_Alterar(CTO_ItensPedido ITO_ItensPedido)
     {
         DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
         IDAO_ItensPedido.MS_Alterar(ITO_ItensPedido);
     }
     public override void MS_Excluir(CTO_ItensPedido ITO_ItensPedido)
     {
         DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
         IDAO_ItensPedido.MS_Excluir(ITO_ItensPedido);
     }
     public override CTO_ItensPedido MS_Obter(int pritObjRef)
     {
           DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
           return IDAO_ItensPedido.MS_Obter(pritObjRef);
     }
     public override CTO_ItensPedido MS_Obter(string prstID)
     {
          DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
          return IDAO_ItensPedido.MS_Obter(prstID);
     }
     public override DataTable MI_ObterItensPorPedido(int idPedido)
     {
         DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
         return IDAO_ItensPedido.MI_ObterItensPorPedido(idPedido);
     }
     public DataTable MS_ObterInscritosPorPalestra(string stCodigoPalestra)
     {
         DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
         return IDAO_ItensPedido.MS_ObterInscritosPorPalestra(stCodigoPalestra);
     }
    
     public DataTable MS_ObterItensPorPedido(string stCodigo)
     {
         DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
         return IDAO_ItensPedido.MS_ObterItensPorPedido(stCodigo);
     }

     public DataTable MS_ObterProdutosCompradosPorCliente(int prCdCliente)
     {
         DAO_ItensPedido IDAO_ItensPedido = new DAO_ItensPedido();
         return IDAO_ItensPedido.MS_ObterProdutosCompradosPorCliente(prCdCliente);
     }
}

