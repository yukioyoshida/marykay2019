using System;
namespace SPI 
{

    public class CTO_ItensPedido 
    {
          //  Declaracao dos atributos Puros
        int AP_cdItem;
        int AP_cdPedido;
        string AP_cdProduto;
        string AP_dsValor;

        public CTO_ItensPedido () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public int getcdItem()
        {
            return AP_cdItem;
        }
        public int getcdPedido()
        {
            return AP_cdPedido;
        }
        public string getcdProduto()
        {
            return AP_cdProduto;
        }
        public string getdsValor()
        {
            return AP_dsValor;
        }
         //  Declaracao dos Metodos set
        public void setcdItem( int prcdItem)
        {
            AP_cdItem = prcdItem;
        }
        public void setcdPedido( int prcdPedido)
        {
            AP_cdPedido = prcdPedido;
        }
        public void setcdProduto( string prcdProduto)
        {
            AP_cdProduto = prcdProduto;
        }
        public void setdsValor( string prdsValor)
        {
            AP_dsValor = prdsValor;
        }
    }
}

