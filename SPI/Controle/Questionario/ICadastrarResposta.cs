using System;
using System.Data;
using System.Collections.Generic;

using SPI;

public interface ICadastrarResposta
{
    void MS_Incluir(CTO_Resposta ITO_Resposta);
    void MS_Alterar(CTO_Resposta ITO_Resposta);
    void MS_Excluir(CTO_Resposta ITO_Resposta);
    CTO_Resposta MS_Obter(int pritObjRef);
    DataTable MS_ObterRespostas(int pritObjRef, string prEvento);
    CTO_Resposta MS_ObterValue(string prstValue);
}

