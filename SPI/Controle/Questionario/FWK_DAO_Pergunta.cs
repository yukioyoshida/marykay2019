using System.Data;

namespace SPI
{
    public class FWK_DAO_Pergunta : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                           "          ObjRef, " +
                           "          NumeroPergunta, " +
                           "          TituloPortugues, " +
                           "          TituloIngles, " +
                           "          TituloEspanhol, " +
                           "          UnicaMultipla, " +
                           "          Obrigatoria ";

        public FWK_DAO_Pergunta()
        {
        }

        public void MS_Incluir(CTO_Pergunta ITO_Pergunta)
        {
            int itObjRef = MS_NovoObjRef();
            MI_SetSql ("INSERT INTO tblPergunta (" +
                       "          ObjRef, " +
                       "          NumeroPergunta, " +
                       "          TituloPortugues, " +
                       "          TituloIngles, " +
                       "          TituloEspanhol, " +
                       "          UnicaMultipla, " +
                       "          Obrigatoria) " + 
                       " VALUES (" + 
                       "          @ObjRef, " +
                       "          @NumeroPergunta, " +
                       "          @TituloPortugues, " +
                       "          @TituloIngles, " +
                       "          @TituloEspanhol, " +
                       "          @UnicaMultipla, " +
                       "          @Obrigatoria) ");

            MI_AddParameters("@ObjRef", itObjRef);
            MI_AddParameters("@NumeroPergunta", ITO_Pergunta.getNumeroPergunta());
            MI_AddParameters("@TituloPortugues", ITO_Pergunta.getTituloPortugues());
            MI_AddParameters("@TituloIngles", ITO_Pergunta.getTituloIngles());
            MI_AddParameters("@TituloEspanhol", ITO_Pergunta.getTituloEspanhol());
            MI_AddParameters("@UnicaMultipla", ITO_Pergunta.getUnicaMultipla());
            MI_AddParameters("@Obrigatoria", ITO_Pergunta.getObrigatoria());

            MI_ExecuteNonQuery();
        }

        public void MS_Alterar(CTO_Pergunta ITO_Pergunta)
        {
            MI_SetSql ("UPDATE tblPergunta SET " +
                       "          ObjRef = @ObjRef, " +
                       "          NumeroPergunta = @NumeroPergunta, " +
                       "          TituloPortugues = @TituloPortugues, " +
                       "          TituloIngles = @TituloIngles, " +
                       "          TituloEspanhol = @TituloEspanhol, " +
                       "          UnicaMultipla = @UnicaMultipla, " +
                       "          Obrigatoria = @Obrigatoria " + 
                       "WHERE ObjRef = @ObjRef ");
            MI_AddParameters("@ObjRef", ITO_Pergunta.getObjRef());
            MI_AddParameters("@NumeroPergunta", ITO_Pergunta.getNumeroPergunta());
            MI_AddParameters("@TituloPortugues", ITO_Pergunta.getTituloPortugues());
            MI_AddParameters("@TituloIngles", ITO_Pergunta.getTituloIngles());
            MI_AddParameters("@TituloEspanhol", ITO_Pergunta.getTituloEspanhol());
            MI_AddParameters("@UnicaMultipla", ITO_Pergunta.getUnicaMultipla());
            MI_AddParameters("@Obrigatoria", ITO_Pergunta.getObrigatoria());

            MI_ExecuteNonQuery();
        }

        public void MS_Excluir(CTO_Pergunta ITO_Pergunta)
        {
            MI_SetSql ("DELETE FROM tblPergunta WHERE ObjRef = @ObjRef");
            MI_AddParameters("@ObjRef", ITO_Pergunta.getObjRef());
            
            MI_ExecuteNonQuery();
        }

        public CTO_Pergunta MS_Obter(int pritObjRef)
        {
            CTO_Pergunta ITO_Pergunta = new CTO_Pergunta();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          NumeroPergunta, " +
                       "          TituloPortugues, " +
                       "          TituloIngles, " +
                       "          TituloEspanhol, " +
                       "          UnicaMultipla, " +
                       "          Obrigatoria " + 
                       "FROM tblPergunta " + 
                       "WHERE Objref = @ObjRef ");
            MI_AddParameters("@Objref", pritObjRef);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Pergunta = null;
            else
                ITO_Pergunta = MI_DataSetToEntidade();
            return ITO_Pergunta;
        }

        public CTO_Pergunta MI_DataSetToEntidade()
        {
            CTO_Pergunta ITO_Pergunta = new CTO_Pergunta();            
            try
            {
                ITO_Pergunta.setObjRef(AP_DataReader.GetInt32(0));
            }
            catch
            {
                ITO_Pergunta.setObjRef(0);
            }
            try
            {
                ITO_Pergunta.setNumeroPergunta(AP_DataReader.GetString(1));
            }
            catch
            {
                ITO_Pergunta.setNumeroPergunta("");
            }
            try
            {
                ITO_Pergunta.setTituloPortugues(AP_DataReader.GetString(2));
            }
            catch
            {
                ITO_Pergunta.setTituloPortugues("");
            }
            try
            {
                ITO_Pergunta.setTituloIngles(AP_DataReader.GetString(3));
            }
            catch
            {
                ITO_Pergunta.setTituloIngles("");
            }
            try
            {
                ITO_Pergunta.setTituloEspanhol(AP_DataReader.GetString(4));
            }
            catch
            {
                ITO_Pergunta.setTituloEspanhol("");
            }
            try
            {
                ITO_Pergunta.setUnicaMultipla(AP_DataReader.GetString(5));
            }
            catch
            {
                ITO_Pergunta.setUnicaMultipla("");
            }
            try
            {
                ITO_Pergunta.setObrigatoria(AP_DataReader.GetString(6));
            }
            catch
            {
                ITO_Pergunta.setObrigatoria("");
            }
            return ITO_Pergunta;
        }

        public CTO_Pergunta MS_ObterTodos()
        {
            CTO_Pergunta ITO_Pergunta = new CTO_Pergunta();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          NumeroPergunta, " +
                       "          TituloPortugues, " +
                       "          TituloIngles, " +
                       "          TituloEspanhol, " +
                       "          UnicaMultipla, " +
                       "          Obrigatoria " + 
                       "FROM tblPergunta " );
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Pergunta = null;
            else
                ITO_Pergunta = MI_DataSetToEntidade();
            return ITO_Pergunta;
        }

        public CTO_Pergunta MS_ObterProximo()
        {
            CTO_Pergunta ITO_Pergunta = new CTO_Pergunta();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_Pergunta = null;
            else
                ITO_Pergunta = MI_DataSetToEntidade();
                return ITO_Pergunta;
        }

        public int MS_NovoObjRef()
        {
            int itObjRef;
            MI_SetSql("Select Max(ObjRef) from tblPergunta");
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 1;
            }
            else
            {
                itObjRef = AP_DataReader.GetInt32(0);
                itObjRef++;
            }
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblPergunta");
            return MI_ExecuteDataSet();
        }

    }
}

