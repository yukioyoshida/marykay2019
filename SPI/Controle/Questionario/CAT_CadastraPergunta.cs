using System;
using SPI;

public class CAT_CadastrarPergunta : CAB_CadastrarPergunta
{
     public CAT_CadastrarPergunta()
     {
     }
     public override void MS_Incluir(CTO_Pergunta ITO_Pergunta)
     {
          DAO_Pergunta IDAO_Pergunta = new DAO_Pergunta();
          IDAO_Pergunta.MS_Incluir(ITO_Pergunta);
     }
     public override void MS_Alterar(CTO_Pergunta ITO_Pergunta)
     {
         DAO_Pergunta IDAO_Pergunta = new DAO_Pergunta();
         IDAO_Pergunta.MS_Alterar(ITO_Pergunta);
     }
     public override void MS_Excluir(CTO_Pergunta ITO_Pergunta)
     {
         DAO_Pergunta IDAO_Pergunta = new DAO_Pergunta();
         IDAO_Pergunta.MS_Excluir(ITO_Pergunta);
     }
     public override CTO_Pergunta MS_Obter(int pritObjRef)
     {
           DAO_Pergunta IDAO_Pergunta = new DAO_Pergunta();
           return IDAO_Pergunta.MS_Obter(pritObjRef);
     }
     public override CTO_Pergunta MS_ObterPergunta(string prstPergunta, string prEvento)
     {
           DAO_Pergunta IDAO_Pergunta = new DAO_Pergunta();
           return IDAO_Pergunta.MS_ObterPergunta(prstPergunta, prEvento);
     }

     public override string MS_ObterQuantidade(string prEvento)
     {
         DAO_Pergunta IDAO_Pergunta = new DAO_Pergunta();
         return IDAO_Pergunta.MS_ObterQuantidade(prEvento);
     }
}

