using System;
using System.Data;
namespace SPI
{
    public class DAO_Resposta : FWK_DAO_Resposta
    {
        public DAO_Resposta()
        {
        }

        //public CTO_Resposta MS_ObterRespostas(int pritPergunta)
        //{
        //    CTO_Resposta ITO_Resposta = new CTO_Resposta();
        //    MI_SetSql (AP_SelectALL + 
        //    "FROM tblResposta " + 
        //    "WHERE Pergunta = @Pergunta ");
        //    MI_AddParameters("@Pergunta", pritPergunta);
        //    MI_ExecuteQuery();
        //    if (MI_EOF()) 
        //        ITO_Resposta = null;
        //    else
        //        ITO_Resposta = MI_DataSetToEntidade();
        //    return ITO_Resposta;
        //}

        public DataTable MS_ObterRespostas(int pritPergunta, string prEvento)
        {
            MI_SetSql(AP_SelectALL +
                      "FROM tblresposta             " +
                      "WHERE Pergunta = @Pergunta   " +
                      " AND Evento = @Evento        " +
                      " ORDER BY Ordem              ");

            MI_AddParameters("@Pergunta", pritPergunta);
            MI_AddParameters("@Evento", prEvento);

            return MI_ExecuteDataSet();
        }
    }
}

