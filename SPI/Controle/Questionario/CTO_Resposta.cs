using System;
namespace SPI 
{

    public class CTO_Resposta 
    {
          //  Declaracao dos atributos Puros
        int AP_ObjRef;
        int AP_Pergunta;
        string AP_Ordem;
        string AP_TextoPortugues;
        string AP_TextoIngles;
        string AP_TextoEspanhol;
        string AP_Sigla;
        string AP_AdimiteNovoTxtBox;

        public CTO_Resposta () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public int getObjRef()
        {
            return AP_ObjRef;
        }
        public int getPergunta()
        {
            return AP_Pergunta;
        }
        public string getOrdem()
        {
            return AP_Ordem;
        }
        public string getTextoPortugues()
        {
            return AP_TextoPortugues;
        }
        public string getTextoIngles()
        {
            return AP_TextoIngles;
        }
        public string getTextoEspanhol()
        {
            return AP_TextoEspanhol;
        }
        public string getSigla()
        {
            return AP_Sigla;
        }
        public string getAdimiteNovoTxtBox()
        {
            return AP_AdimiteNovoTxtBox;
        }
         //  Declaracao dos Metodos set
        public void setObjRef( int prObjRef)
        {
            AP_ObjRef = prObjRef;
        }
        public void setPergunta( int prPergunta)
        {
            AP_Pergunta = prPergunta;
        }
        public void setOrdem( string prOrdem)
        {
            AP_Ordem = prOrdem;
        }
        public void setTextoPortugues( string prTextoPortugues)
        {
            AP_TextoPortugues = prTextoPortugues;
        }
        public void setTextoIngles( string prTextoIngles)
        {
            AP_TextoIngles = prTextoIngles;
        }
        public void setTextoEspanhol( string prTextoEspanhol)
        {
            AP_TextoEspanhol = prTextoEspanhol;
        }
        public void setSigla( string prSigla)
        {
            AP_Sigla = prSigla;
        }
        public void setAdimiteNovoTxtBox( string prAdimiteNovoTxtBox)
        {
            AP_AdimiteNovoTxtBox = prAdimiteNovoTxtBox;
        }
    }
}

