using System;
using SPI;

public interface ICadastrarPergunta
{
     void MS_Incluir(CTO_Pergunta ITO_Pergunta);
     void MS_Alterar(CTO_Pergunta ITO_Pergunta);
     void MS_Excluir(CTO_Pergunta ITO_Pergunta);
     CTO_Pergunta MS_Obter(int pritObjRef);
     CTO_Pergunta MS_ObterPergunta(string prstCPF, string prEvento);
     string MS_ObterQuantidade(string prEvento);
}

