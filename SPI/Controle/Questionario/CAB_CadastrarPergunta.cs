using System;
using SPI;

public abstract class CAB_CadastrarPergunta : ICadastrarPergunta
{
     public abstract void MS_Incluir(CTO_Pergunta ITO_Pergunta);
     public abstract void MS_Alterar(CTO_Pergunta ITO_Pergunta);
     public abstract void MS_Excluir(CTO_Pergunta ITO_Pergunta);
     public abstract CTO_Pergunta MS_Obter(int pritObjRef);
     public abstract CTO_Pergunta MS_ObterPergunta(string prstCPF, string prEvento);
     public abstract string MS_ObterQuantidade(string prEvento);
}

