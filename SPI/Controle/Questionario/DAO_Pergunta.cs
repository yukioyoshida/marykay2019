using System;
namespace SPI
{
    public class DAO_Pergunta : FWK_DAO_Pergunta
    {
        public DAO_Pergunta()
        {
        }

        public CTO_Pergunta MS_ObterPergunta(string prstPergunta, string prEvento)
        {

            CTO_Pergunta ITO_Pergunta = new CTO_Pergunta();
            MI_SetSql (AP_SelectALL + 
            "FROM tblpergunta                       " +
            "WHERE NumeroPergunta = @NumeroPergunta " +
            " AND Evento = @Evento                  ");
            MI_AddParameters("@NumeroPergunta", prstPergunta);
            MI_AddParameters("@Evento", prEvento);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Pergunta = null;
            else
                ITO_Pergunta = MI_DataSetToEntidade();
            
            MI_FecharConexao();
            
            return ITO_Pergunta;
        }


        public string MS_ObterQuantidade(string prEvento)
        {
            if (prEvento == null) prEvento = "";
            string stQuantidade = "0";
            CTO_Pergunta ITO_Pergunta = new CTO_Pergunta();
            MI_SetSql("Select count(*)Quantidade from tblpergunta   " +
            " WHERE Evento = @Evento                                ");
            MI_AddParameters("@Evento", prEvento);

            MI_ExecuteQuery();
            try
            {
                stQuantidade = Convert.ToString(AP_DataReader.GetString(0));
            }
            catch
            {
                stQuantidade = "0";
            }

            MI_FecharConexao();

            return stQuantidade;
        }
    }
}

