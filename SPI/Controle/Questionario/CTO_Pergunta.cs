using System;

namespace SPI
{

    public class CTO_Pergunta 
    {
          //  Declaracao dos atributos Puros
        int AP_ObjRef;
        string AP_NumeroPergunta;
        string AP_TituloPortugues;
        string AP_TituloIngles;
        string AP_TituloEspanhol;
        string AP_UnicaMultipla;
        string AP_Obrigatoria;

        public CTO_Pergunta () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public int getObjRef()
        {
            return AP_ObjRef;
        }
        public string getNumeroPergunta()
        {
            return AP_NumeroPergunta;
        }
        public string getTituloPortugues()
        {
            return AP_TituloPortugues;
        }
        public string getTituloIngles()
        {
            return AP_TituloIngles;
        }
        public string getTituloEspanhol()
        {
            return AP_TituloEspanhol;
        }
        public string getUnicaMultipla()
        {
            return AP_UnicaMultipla;
        }
        public string getObrigatoria()
        {
            return AP_Obrigatoria;
        }
         //  Declaracao dos Metodos set
        public void setObjRef( int prObjRef)
        {
            AP_ObjRef = prObjRef;
        }
        public void setNumeroPergunta( string prNumeroPergunta)
        {
            AP_NumeroPergunta = prNumeroPergunta;
        }
        public void setTituloPortugues( string prTituloPortugues)
        {
            AP_TituloPortugues = prTituloPortugues;
        }
        public void setTituloIngles( string prTituloIngles)
        {
            AP_TituloIngles = prTituloIngles;
        }
        public void setTituloEspanhol( string prTituloEspanhol)
        {
            AP_TituloEspanhol = prTituloEspanhol;
        }
        public void setUnicaMultipla( string prUnicaMultipla)
        {
            AP_UnicaMultipla = prUnicaMultipla;
        }
        public void setObrigatoria( string prObrigatoria)
        {
            AP_Obrigatoria = prObrigatoria;
        }
    }
}

