using System;
using System.Data;
using System.Collections.Generic;

using SPI;

public abstract class CAB_CadastrarResposta : ICadastrarResposta
{
    public abstract void MS_Incluir(CTO_Resposta ITO_Resposta);
    public abstract void MS_Alterar(CTO_Resposta ITO_Resposta);
    public abstract void MS_Excluir(CTO_Resposta ITO_Resposta);
    public abstract CTO_Resposta MS_Obter(int pritObjRef);
    public abstract DataTable MS_ObterRespostas(int pritObjRef, string prEvento);
    public abstract CTO_Resposta MS_ObterValue(string prstValue);
}

