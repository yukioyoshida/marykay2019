using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace SPI
{
    public class FWK_DAO_Resposta : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                           "          ObjRef, " +
                           "          Pergunta, " +
                           "          Ordem, " +
                           "          TextoPortugues, " +
                           "          TextoIngles, " +
                           "          TextoEspanhol, " +
                           "          Sigla, " +
                           "          AdimiteNovoTxtBox ";

        public FWK_DAO_Resposta()
        {
        }

        public void MS_Incluir(CTO_Resposta ITO_Resposta)
        {
            int itObjRef = MS_NovoObjRef();
            MI_SetSql ("INSERT INTO tblResposta (" +
                       "          ObjRef, " +
                       "          Pergunta, " +
                       "          Ordem, " +
                       "          TextoPortugues, " +
                       "          TextoIngles, " +
                       "          TextoEspanhol, " +
                       "          Sigla, " +
                       "          AdimiteNovoTxtBox) " + 
                       " VALUES (" + 
                       "          @ObjRef, " +
                       "          @Pergunta, " +
                       "          @Ordem, " +
                       "          @TextoPortugues, " +
                       "          @TextoIngles, " +
                       "          @TextoEspanhol, " +
                       "          @Sigla, " +
                       "          @AdimiteNovoTxtBox) ");

            MI_AddParameters("@ObjRef", itObjRef);
            MI_AddParameters("@Pergunta", ITO_Resposta.getPergunta());
            MI_AddParameters("@Ordem", ITO_Resposta.getOrdem());
            MI_AddParameters("@TextoPortugues", ITO_Resposta.getTextoPortugues());
            MI_AddParameters("@TextoIngles", ITO_Resposta.getTextoIngles());
            MI_AddParameters("@TextoEspanhol", ITO_Resposta.getTextoEspanhol());
            MI_AddParameters("@Sigla", ITO_Resposta.getSigla());
            MI_AddParameters("@AdimiteNovoTxtBox", ITO_Resposta.getAdimiteNovoTxtBox());

            MI_ExecuteNonQuery();
        }

        public void MS_Alterar(CTO_Resposta ITO_Resposta)
        {
            MI_SetSql ("UPDATE tblResposta SET " +
                       "          ObjRef = @ObjRef, " +
                       "          Pergunta = @Pergunta, " +
                       "          Ordem = @Ordem, " +
                       "          TextoPortugues = @TextoPortugues, " +
                       "          TextoIngles = @TextoIngles, " +
                       "          TextoEspanhol = @TextoEspanhol, " +
                       "          Sigla = @Sigla, " +
                       "          AdimiteNovoTxtBox = @AdimiteNovoTxtBox " + 
                       "WHERE ObjRef = @ObjRef ");
            MI_AddParameters("@ObjRef", ITO_Resposta.getObjRef());
            MI_AddParameters("@Pergunta", ITO_Resposta.getPergunta());
            MI_AddParameters("@Ordem", ITO_Resposta.getOrdem());
            MI_AddParameters("@TextoPortugues", ITO_Resposta.getTextoPortugues());
            MI_AddParameters("@TextoIngles", ITO_Resposta.getTextoIngles());
            MI_AddParameters("@TextoEspanhol", ITO_Resposta.getTextoEspanhol());
            MI_AddParameters("@Sigla", ITO_Resposta.getSigla());
            MI_AddParameters("@AdimiteNovoTxtBox", ITO_Resposta.getAdimiteNovoTxtBox());

            MI_ExecuteNonQuery();
        }

        public void MS_Excluir(CTO_Resposta ITO_Resposta)
        {
            MI_SetSql ("DELETE FROM tblResposta WHERE ObjRef = @ObjRef");
            MI_AddParameters("@ObjRef", ITO_Resposta.getObjRef());
            
            MI_ExecuteNonQuery();
        }

        public CTO_Resposta MS_ObterValue(string prstValue)
        {
            CTO_Resposta ITO_Resposta = new CTO_Resposta();
            MI_SetSql("SELECT " +
                       "          ObjRef, " +
                       "          Pergunta, " +
                       "          Ordem, " +
                       "          TextoPortugues, " +
                       "          TextoIngles, " +
                       "          TextoEspanhol, " +
                       "          Sigla, " +
                       "          AdimiteNovoTxtBox " +
                       "FROM tblResposta " +
                       "WHERE Sigla = @Sigla ");
            MI_AddParameters("@Sigla", prstValue);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Resposta = null;
            else
                ITO_Resposta = MI_DataSetToEntidade();
            return ITO_Resposta;
        }

        public CTO_Resposta MS_Obter(int pritObjRef)
        {
            CTO_Resposta ITO_Resposta = new CTO_Resposta();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          Pergunta, " +
                       "          Ordem, " +
                       "          TextoPortugues, " +
                       "          TextoIngles, " +
                       "          TextoEspanhol, " +
                       "          Sigla, " +
                       "          AdimiteNovoTxtBox " + 
                       "FROM tblResposta " + 
                       "WHERE Objref = @ObjRef ");
            MI_AddParameters("@Objref", pritObjRef);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Resposta = null;
            else
                ITO_Resposta = MI_DataSetToEntidade();
            return ITO_Resposta;
        }

        public CTO_Resposta MI_DataSetToEntidade()
        {
            CTO_Resposta ITO_Resposta = new CTO_Resposta();            
            try
            {
                ITO_Resposta.setObjRef(AP_DataReader.GetInt32(0));
            }
            catch
            {
                ITO_Resposta.setObjRef(0);
            }
            try
            {
                ITO_Resposta.setPergunta(AP_DataReader.GetInt32(1));
            }
            catch
            {
                ITO_Resposta.setPergunta(0);
            }
            try
            {
                ITO_Resposta.setOrdem(AP_DataReader.GetString(2));
            }
            catch
            {
                ITO_Resposta.setOrdem("");
            }
            try
            {
                ITO_Resposta.setTextoPortugues(AP_DataReader.GetString(3));
            }
            catch
            {
                ITO_Resposta.setTextoPortugues("");
            }
            try
            {
                ITO_Resposta.setTextoIngles(AP_DataReader.GetString(4));
            }
            catch
            {
                ITO_Resposta.setTextoIngles("");
            }
            try
            {
                ITO_Resposta.setTextoEspanhol(AP_DataReader.GetString(5));
            }
            catch
            {
                ITO_Resposta.setTextoEspanhol("");
            }
            try
            {
                ITO_Resposta.setSigla(AP_DataReader.GetString(6));
            }
            catch
            {
                ITO_Resposta.setSigla("");
            }
            try
            {
                ITO_Resposta.setAdimiteNovoTxtBox(AP_DataReader.GetString(7));
            }
            catch
            {
                ITO_Resposta.setAdimiteNovoTxtBox("");
            }
            return ITO_Resposta;
        }

        public CTO_Resposta MS_ObterTodos()
        {
            CTO_Resposta ITO_Resposta = new CTO_Resposta();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          Pergunta, " +
                       "          Ordem, " +
                       "          TextoPortugues, " +
                       "          TextoIngles, " +
                       "          TextoEspanhol, " +
                       "          Sigla, " +
                       "          AdimiteNovoTxtBox " + 
                       "FROM tblResposta " );
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Resposta = null;
            else
                ITO_Resposta = MI_DataSetToEntidade();
            return ITO_Resposta;
        }

        public CTO_Resposta MS_ObterProximo()
        {
            CTO_Resposta ITO_Resposta = new CTO_Resposta();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_Resposta = null;
            else
                ITO_Resposta = MI_DataSetToEntidade();
                return ITO_Resposta;
        }

        public int MS_NovoObjRef()
        {
            int itObjRef;
            MI_SetSql("Select Max(ObjRef) from tblResposta");
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 1;
            }
            else
            {
                itObjRef = AP_DataReader.GetInt32(0);
                itObjRef++;
            }
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblResposta");
            return MI_ExecuteDataSet();
        }

    }
}

