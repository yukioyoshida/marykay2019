using System;
using System.Data;
using System.Collections.Generic;
using SPI;

public class CAT_CadastrarResposta : CAB_CadastrarResposta
{
     public CAT_CadastrarResposta()
     {
     }
     public override void MS_Incluir(CTO_Resposta ITO_Resposta)
     {
          DAO_Resposta IDAO_Resposta = new DAO_Resposta();
          IDAO_Resposta.MS_Incluir(ITO_Resposta);
     }
     public override void MS_Alterar(CTO_Resposta ITO_Resposta)
     {
         DAO_Resposta IDAO_Resposta = new DAO_Resposta();
         IDAO_Resposta.MS_Alterar(ITO_Resposta);
     }
     public override void MS_Excluir(CTO_Resposta ITO_Resposta)
     {
         DAO_Resposta IDAO_Resposta = new DAO_Resposta();
         IDAO_Resposta.MS_Excluir(ITO_Resposta);
     }
     public override CTO_Resposta MS_Obter(int pritObjRef)
     {
           DAO_Resposta IDAO_Resposta = new DAO_Resposta();
           return IDAO_Resposta.MS_Obter(pritObjRef);
     }


    public override CTO_Resposta MS_ObterValue(string prstValue)
    {
        DAO_Resposta IDAO_Resposta = new DAO_Resposta();
        return IDAO_Resposta.MS_ObterValue(prstValue);
    }

    public override DataTable MS_ObterRespostas(int pritObjRef, string prEvento)
     {           
        DAO_Resposta IDAO_Resposta = new DAO_Resposta();
        return IDAO_Resposta.MS_ObterRespostas(pritObjRef, prEvento);
     }
}

