using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using SPI;

namespace SPI
{
    class LinhaTextoImpl : ILinha
    {
        private CTO_Linha ITO_Linha;
        PrintPageEventArgs IEventsArgs;
        private RectangleF AreaDaEtiqueta = new RectangleF();
        private string stTexto = "";
        FontStyle Estilo;

        public void MS_Imprimir()
        {
            if (ITO_Linha.getEstiloFonte().ToUpper() == "BOLD")
            {
                Estilo = FontStyle.Bold;
            }
            else
            {
                Estilo = FontStyle.Regular;
            }

            Font printFont = new Font(ITO_Linha.getNomeFonte(), 
                    ITO_Linha.getTamanhoFonte(), 
                    Estilo);

            stTexto = ITO_Linha.getTexto();
            IEventsArgs.Graphics.DrawString(stTexto, 
                                            printFont, 
                                            Brushes.Black,
                                            ITO_Linha.getPosicaoHorizontalTexto(),
                                            ITO_Linha.getPosicaoVerticalTexto(), 
                                            new StringFormat());
        }

        public void MS_ImprimirSequencial(DataTable dtItens, string prCodigoBarras)
        {
            CTO_Visitante ITO_Visitante = new CAT_CadastrarVisitante().MS_ObterPorCodigo(prCodigoBarras);
            // HTML DO RECIBO
            StringBuilder sbHtml = new StringBuilder();

            sbHtml.Append("<!DOCTYPE html>");
            sbHtml.Append("<html lang='pt-br'>");
            sbHtml.Append("<head>");
            sbHtml.Append(" <meta charset='utf-8'/>");
            sbHtml.Append(" <title>RECIBO</title>");

            sbHtml.Append(" <style>");

            sbHtml.Append("     body{");
            sbHtml.Append("         font-family:Tahoma;");
            sbHtml.Append("     }");

            sbHtml.Append("     .Titles{");
            sbHtml.Append("         font-weight:bold;");
            sbHtml.Append("         font-size:14pt;");
            sbHtml.Append("     }");

            sbHtml.Append("     .Itens{");
            sbHtml.Append("         font-size:12pt;");
            sbHtml.Append("         margin-top:-20px;");
            sbHtml.Append("     }");

            sbHtml.Append(" </style>");

            sbHtml.Append("</head>");
            sbHtml.Append("<body>");

            sbHtml.Append("	<p class='Titles'>"+ ITO_Visitante.AP_Consultora.ToString() + " " + ITO_Visitante.AP_NomedaConsultora.ToString() + "</p>");
            
            sbHtml.Append("	<p class='Titles'>Assinatura:</p>");
            sbHtml.Append("    <p style='margin-top:-56px;margin-left:110px;'><img src='"+ new CSV_VariaveisGlobal().getLocalArmazenamentoFoto() + "\\Assinatura\\" + ""+ ITO_Visitante.AP_CodigoBarras.ToString() + ".jpg' style='width:200px; height:35px;' /></p>");
            sbHtml.Append("    <p style='margin-top:-36px;margin-left:100px;'>_________________________________________</p>");





            Font printFont;
            Int32 prAlturaRegistro = ITO_Linha.getPosicaoVerticalTexto();
            DataRow[] drRegistros;

            drRegistros = dtItens.Select("Correios <> 'S'");

            if (drRegistros.LongLength != 0)
            {
                // ITENS RETIRADOS
                prAlturaRegistro += 25;
                Estilo = FontStyle.Bold;

                printFont = new Font(ITO_Linha.getNomeFonte(),
                            ITO_Linha.getTamanhoFonte(),
                            Estilo);


                stTexto = "Itens retirados";
                sbHtml.Append("	<p class='Titles'>Itens retirados</p>");

                IEventsArgs.Graphics.DrawString(stTexto,
                                                printFont,
                                                Brushes.Black,
                                                ITO_Linha.getPosicaoHorizontalTexto(),
                                                prAlturaRegistro,
                                                new StringFormat());

                prAlturaRegistro += 17;


                foreach (DataRow RowItem in dtItens.Rows)
                {
                    if (RowItem["Correios"].ToString() == "S")
                        continue;

                    if (ITO_Linha.getEstiloFonte().ToUpper() == "BOLD")
                    {
                        Estilo = FontStyle.Bold;
                    }
                    else
                    {
                        Estilo = FontStyle.Regular;
                    }

                    printFont = new Font(ITO_Linha.getNomeFonte(),
                            ITO_Linha.getTamanhoFonte(),
                            Estilo);

                    stTexto = ITO_Linha.getTexto();
                    IEventsArgs.Graphics.DrawString(RowItem["dsReconhecimento1"].ToString() + " - " + RowItem["dsReconhecimento"].ToString(),
                                                    printFont,
                                                    Brushes.Black,
                                                    ITO_Linha.getPosicaoHorizontalTexto(),
                                                    prAlturaRegistro,
                                                    new StringFormat());

                    prAlturaRegistro += 17;
                    
                    sbHtml.Append("	<p class='Itens'>"+ RowItem["dsReconhecimento1"].ToString() + " - " + RowItem["dsReconhecimento"].ToString() + "</p>");
                }
            }


            drRegistros = dtItens.Select("Correios = 'S'");

            if (drRegistros.LongLength != 0)
            {
                // ITENS N�O RETIRADOS
                prAlturaRegistro += 25;
                Estilo = FontStyle.Bold;

                printFont = new Font(ITO_Linha.getNomeFonte(),
                            ITO_Linha.getTamanhoFonte(),
                            Estilo);

                sbHtml.Append("	<p class='Titles'>Itens n�o retirados</p>");

                stTexto = "Itens n�o retirados";
                IEventsArgs.Graphics.DrawString(stTexto,
                                                printFont,
                                                Brushes.Black,
                                                ITO_Linha.getPosicaoHorizontalTexto(),
                                                prAlturaRegistro,
                                                new StringFormat());

                prAlturaRegistro += 17;

                foreach (DataRow RowItem in dtItens.Rows)
                {
                    if (RowItem["Correios"].ToString() != "S")
                        continue;

                    if (ITO_Linha.getEstiloFonte().ToUpper() == "BOLD")
                    {
                        Estilo = FontStyle.Bold;
                    }
                    else
                    {
                        Estilo = FontStyle.Regular;
                    }

                    printFont = new Font(ITO_Linha.getNomeFonte(),
                            ITO_Linha.getTamanhoFonte(),
                            Estilo);

                    stTexto = ITO_Linha.getTexto();
                    IEventsArgs.Graphics.DrawString(RowItem["dsReconhecimento1"].ToString() + " - " + RowItem["dsReconhecimento"].ToString(),
                                                    printFont,
                                                    Brushes.Black,
                                                    ITO_Linha.getPosicaoHorizontalTexto(),
                                                    prAlturaRegistro,
                                                    new StringFormat());

                    prAlturaRegistro += 17;

                    
                    sbHtml.Append("	<p class='Itens'>"+ RowItem["dsReconhecimento1"].ToString() + " - " + RowItem["dsReconhecimento"].ToString() + "</p>");
                }
            }

            sbHtml.Append("</html>");

            if (dtItens.Rows.Count > 0)
            {
                using (StreamWriter w = File.CreateText(new CSV_VariaveisGlobal().getLocalArmazenamentoFoto() + "\\" + ITO_Visitante.AP_Consultora.ToString() + ".html"))
                {
                    w.WriteLine("{0}", sbHtml.ToString());
                }
            }
        }

        public void setEventsArgs(PrintPageEventArgs EV)
        {
            IEventsArgs = EV;
        }
        public void setConteudoLinha( CTO_Linha prITO_Linha)
        {
            ITO_Linha = prITO_Linha;
        }


    }
}
