using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Printing;
using SPI;
using System.Data;
using System.Windows.Forms;
using System.Reflection;


namespace SPI
{
    class CAT_ImprimirEtiqueta
    {
        string AP_Codigo;
        string AP_Produtos;
        string AP_Reconhecimento = string.Empty;
        bool AP_ImprimirCodigoDeBarras = true;
        CTO_Visitante ITO_Visitante = new CTO_Visitante();
        DataTable AP_dtItensDaConsultora = new DataTable();

        public CAT_ImprimirEtiqueta(Int64 pritCodigo, bool prboImprimirCodigo, string stProduto, string stEtiqueta)
        {
            AP_Codigo = pritCodigo.ToString();
            ITO_Visitante = new DAO_Visitante().MS_ObterPorCodigo(pritCodigo.ToString());

            if (stEtiqueta == "1")
            //{ AP_Produtos = ""; }
            //else
            { AP_Produtos = stProduto; }

            AP_ImprimirCodigoDeBarras = prboImprimirCodigo;

            PrintDocument pd = new PrintDocument();
            if (stEtiqueta == "1")
            {
                pd.PrintPage += new PrintPageEventHandler(MI_ImprimirEtiqueta);
            }
            else
            {
                pd.PrintPage += new PrintPageEventHandler(MI_ImprimirEtiquetaCodigoPagamento);
            }
            pd.Print();
        }

        public CAT_ImprimirEtiqueta(Int64 pritCodigo, bool prboImprimirCodigo, string stProduto, string stEtiqueta, string dsReconhecimento)
        {
            AP_Codigo = pritCodigo.ToString();
            AP_Reconhecimento = dsReconhecimento;

            if (stEtiqueta == "1")
            //{ AP_Produtos = ""; }
            //else
            { AP_Produtos = stProduto; }

            AP_ImprimirCodigoDeBarras = prboImprimirCodigo;

            PrintDocument pd = new PrintDocument();
            if (stEtiqueta == "1")
            {
                pd.PrintPage += new PrintPageEventHandler(MI_ImprimirEtiqueta);
            }
            else
            {
                pd.PrintPage += new PrintPageEventHandler(MI_ImprimirEtiquetaCodigoPagamento);
            }
            pd.Print();
        }

        public CAT_ImprimirEtiqueta(Int64 pritCodigo, bool prboImprimirCodigo, DataTable dtItensRetirados)
        {
            AP_Codigo = pritCodigo.ToString();
            AP_ImprimirCodigoDeBarras = prboImprimirCodigo;
            AP_dtItensDaConsultora = dtItensRetirados;

            PrintDocument pd = new PrintDocument();
            pd.PrintPage += new PrintPageEventHandler(MI_ImprimirRecibo);
            
            pd.Print();
        }

        private string MI_DefinirTipoInscricao(string prTipoInscricao)
        {
            string stResult = string.Empty;

            if (("CONVIDADOS".Contains(prTipoInscricao.ToUpper())) || (prTipoInscricao.ToUpper()).Contains("CONVIDADOS"))
            {
                stResult = "CONVIDADOS";
            }
            else
            {
                stResult = "CONSULTORAS";
            }

            return stResult;
        }

        private void MI_ImprimirEtiqueta(object sender, PrintPageEventArgs ev)
        {
            FWK_DAO_XML IWK_DAO_XML = new FWK_DAO_XML();
            IWK_DAO_XML.MS_LerAquivoXML("Etiqueta.xml");
            DataSet dtgTeste = IWK_DAO_XML.MS_GetDataSet();

            CTO_Linha ITO_Linha = new CTO_Linha();

            //thisDataSet.Tables ["lig_det"].Rows [i]["registro"].ToString ())

            for (int i = 0; i <= dtgTeste.Tables[0].Rows.Count - 1; i++)
            {
                ITO_Linha.setObjRef(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["ObjRef"].ToString()));
                ITO_Linha.setAlturaDaEtiqueta(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["AlturaDaEtiqueta"].ToString()));
                ITO_Linha.setAlturaDoCodigoDeBarras(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["AlturaDoCodigoDeBarras"].ToString()));
                ITO_Linha.setLarguraDoCodigoDeBarras(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["LarguraDoCodigoDeBarras"].ToString()));
                ITO_Linha.setPosicaoLateralDaEtiqueta(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["PosicaoLateralDaEtiqueta"].ToString()));
                ITO_Linha.setPosicaoVerticalTexto(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["PosicaoVerticalTexto"].ToString()));
                ITO_Linha.setPosicaoHorizontalTexto(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["PosicaoHorizontalTexto"].ToString()));
                ITO_Linha.setCampo(dtgTeste.Tables[0].Rows[i]["Campo"].ToString());
                ITO_Linha.setNomeFonte(dtgTeste.Tables[0].Rows[i]["NomeFonte"].ToString());
                ITO_Linha.setTamanhoFonte(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["TamanhoFonte"].ToString()));
                ITO_Linha.setEstiloFonte(dtgTeste.Tables[0].Rows[i]["EstiloFonte"].ToString());
                ITO_Linha.setPastaCodBarras(dtgTeste.Tables[0].Rows[i]["PastaCodBarras"].ToString());

                //Faz referencia a class a ser carregada dinamicamente
                Assembly myClass = Assembly.GetAssembly(typeof(CAT_CadastrarVisitante));

                //Cria o objeto a ser invocado
                //Observem que o objeto deve vir acompanhado de seu namespace, que no nosso caso n�o informamos
                //pois esta no mesmo namespace desta classe
                object myObj = myClass.CreateInstance("CAT_CadastrarVisitante");
                object oCampo = "";
                //Invoca o m�todo Escreve da class teste
                if (ITO_Linha.getCampo() != string.Empty)
                {
                    string stNomeCampo;

                    CTO_Visitante ITO_Visitante;
                    ITO_Visitante = (CTO_Visitante)myObj.GetType().InvokeMember("MS_ObterPorCodigo", BindingFlags.InvokeMethod, null, myObj, new object[] { AP_Codigo });

                    if (ITO_Linha.getCampo().ToString().ToUpper() == "PEDIDO")
                    {
                        oCampo = AP_Produtos;
                        ITO_Linha.setTexto("PEDIDO");
                    }
                    else
                    {
                        stNomeCampo = "get" + ITO_Linha.getCampo();
                        oCampo = ITO_Visitante.GetType().InvokeMember(stNomeCampo, BindingFlags.InvokeMethod, null, ITO_Visitante, new object[] { });
                    }
                }

                if (ITO_Linha.getLarguraDoCodigoDeBarras() <= 0)
                {
                    if (ITO_Linha.getTexto() == "PEDIDO")
                    {
                        string stTipoInscricao = MI_DefinirTipoInscricao(ITO_Visitante.getTipodeInscricao());
                        DataTable dtReconhecimentos = new DAO_Visitante().MS_ObterReconhecimentoPorConsultora(ITO_Visitante.getConsultora(), stTipoInscricao);

                        string stProdutos = "";

                        foreach (DataRow Row in dtReconhecimentos.Rows)
                        {
                            if (stTipoInscricao == "CONVIDADOS")
                            {
                                // PERMITE SOMENTE SE FOR OS ITENS DE CONVIDADO
                                // 1 AREA DE ASSENTOS
                                if ((Row["dsReconhecimento"].ToString().ToUpper().Contains("AREA DE ASSENTO:")) || ("AREA DE ASSENTO:".Contains(Row["dsReconhecimento"].ToString().ToUpper())))
                                {
                                    if ((Row["dsNomeCredencial"].ToString().ToUpper() != "") && (Row["dsNomeCredencial"].ToString().ToUpper() != "N�O VAI ESCRITO NA CREDENCIAL"))
                                        stProdutos += Row["cdRoteiro"].ToString() + ";";
                                }

                                // 2 DEFINIDOS PELO CLIENTE
                                if (
                                        (Row["dsReconhecimento"].ToString().Trim() == "Convidado") ||
                                        (Row["dsReconhecimento"].ToString().Trim() == "Atividade Especial Convidado") ||
                                        (Row["dsReconhecimento"].ToString().Trim() == "Coquetel da Realeza - Convidado") ||
                                        (Row["dsReconhecimento"].ToString().Trim() == "C�njuge") ||
                                        (Row["dsReconhecimento"].ToString().Trim() == "Brunch com Alvaro Polanco")
                                   )
                                {
                                    if ((Row["dsNomeCredencial"].ToString().ToUpper() != "") && (Row["dsNomeCredencial"].ToString().ToUpper() != "N�O VAI ESCRITO NA CREDENCIAL"))
                                        stProdutos += Row["cdRoteiro"].ToString() + ";";
                                }
                            }
                            else
                            {
                                if (Row["TipoRoteiro"].ToString().ToUpper() == "I")
                                {
                                    if (Row["dsLocal"].ToString().ToUpper() != "ENTREGA")
                                    {
                                        if (
                                            (Row["dsReconhecimento"].ToString().Trim() == "Convidado") ||
                                            (Row["dsReconhecimento"].ToString().Trim() == "Atividade Especial Convidado") ||
                                            (Row["dsReconhecimento"].ToString().Trim() == "Coquetel da Realeza - Convidado") ||
                                            (Row["dsReconhecimento"].ToString().Trim() == "C�njuge")
                                        )
                                        {
                                            // OS ITENS ACIMA S�O DO CONVIDADO N�O APARECE PARA A CONSULTORA
                                        }
                                        else
                                        {
                                            if ((Row["dsNomeCredencial"].ToString().ToUpper() != "") && (Row["dsNomeCredencial"].ToString().ToUpper() != "N�O VAI ESCRITO NA CREDENCIAL"))
                                                stProdutos += Row["cdRoteiro"].ToString() + ";";
                                        }
                                    }
                                }
                            }
                        }

                        ITO_Linha.setIdentificador(stProdutos);
                        ImageImpl oLinha = new ImageImpl();
                        oLinha.setConteudoLinha(ITO_Linha);
                        oLinha.setEventsArgs(ev);
                        oLinha.MS_Imprimir("1", "BANDEIRA", ITO_Visitante);
                    }
                    else
                    {
                        ITO_Linha.setTexto(oCampo.ToString());
                        LinhaTextoImpl oLinha = new LinhaTextoImpl();
                        oLinha.setConteudoLinha(ITO_Linha);
                        oLinha.setEventsArgs(ev);
                        oLinha.MS_Imprimir();
                    }
                }
                else
                {
                    if (AP_ImprimirCodigoDeBarras)
                    {
                        ITO_Linha.setIdentificador(oCampo.ToString());
                        LinhaBarraImpl oLinha = new LinhaBarraImpl();
                        oLinha.setConteudoLinha(ITO_Linha);
                        oLinha.setEventsArgs(ev);
                        oLinha.MS_Imprimir();
                    }
                }
            }
            ev.HasMorePages = false;
        }

        private void MI_ImprimirEtiquetaCodigoPagamento(object sender, PrintPageEventArgs ev)
        {
            FWK_DAO_XML IWK_DAO_XML = new FWK_DAO_XML();
            IWK_DAO_XML.MS_LerAquivoXML("EtiquetaPagamento.xml");
            DataSet dtgTeste = IWK_DAO_XML.MS_GetDataSet();

            CTO_Linha ITO_Linha = new CTO_Linha();

            //thisDataSet.Tables ["lig_det"].Rows [i]["registro"].ToString ())

            for (int i = 0; i <= dtgTeste.Tables[0].Rows.Count - 1; i++)
            {
                ITO_Linha.setObjRef(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["ObjRef"].ToString()));
                ITO_Linha.setAlturaDaEtiqueta(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["AlturaDaEtiqueta"].ToString()));
                ITO_Linha.setAlturaDoCodigoDeBarras(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["AlturaDoCodigoDeBarras"].ToString()));
                ITO_Linha.setLarguraDoCodigoDeBarras(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["LarguraDoCodigoDeBarras"].ToString()));
                ITO_Linha.setPosicaoLateralDaEtiqueta(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["PosicaoLateralDaEtiqueta"].ToString()));
                ITO_Linha.setPosicaoVerticalTexto(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["PosicaoVerticalTexto"].ToString()));
                ITO_Linha.setPosicaoHorizontalTexto(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["PosicaoHorizontalTexto"].ToString()));
                ITO_Linha.setCampo(dtgTeste.Tables[0].Rows[i]["Campo"].ToString());
                ITO_Linha.setNomeFonte(dtgTeste.Tables[0].Rows[i]["NomeFonte"].ToString());
                ITO_Linha.setTamanhoFonte(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["TamanhoFonte"].ToString()));
                ITO_Linha.setEstiloFonte(dtgTeste.Tables[0].Rows[i]["EstiloFonte"].ToString());
                ITO_Linha.setPastaCodBarras(dtgTeste.Tables[0].Rows[i]["PastaCodBarras"].ToString());

                //Faz referencia a class a ser carregada dinamicamente
                Assembly myClass = Assembly.GetAssembly(typeof(CAT_CadastrarVisitante));

                //Cria o objeto a ser invocado
                //Observem que o objeto deve vir acompanhado de seu namespace, que no nosso caso n�o informamos
                //pois esta no mesmo namespace desta classe
                object myObj = myClass.CreateInstance("CAT_CadastrarVisitante");
                object oCampo = "";
                //Invoca o m�todo Escreve da class teste
                if (ITO_Linha.getCampo() != string.Empty)
                {
                    string stNomeCampo;

                    CTO_Visitante ITO_Visitante;
                    ITO_Visitante = (CTO_Visitante)myObj.GetType().InvokeMember("MS_ObterPorCodigo", BindingFlags.InvokeMethod, null, myObj, new object[] { AP_Codigo });

                    if (ITO_Linha.getCampo().ToString().ToUpper() == "PEDIDO")
                    {
                        oCampo = AP_Produtos;
                    }
                    else
                    {
                        stNomeCampo = "get" + ITO_Linha.getCampo();
                        //ALTERAR
                        oCampo = AP_Produtos;//ITO_Visitante.GetType().InvokeMember(stNomeCampo, BindingFlags.InvokeMethod, null, ITO_Visitante, new object[] { });
                    }
                }

                if (ITO_Linha.getLarguraDoCodigoDeBarras() <= 0)
                {
                    ITO_Linha.setTexto(oCampo.ToString());
                    LinhaTextoImpl oLinha = new LinhaTextoImpl();
                    oLinha.setConteudoLinha(ITO_Linha);
                    oLinha.setEventsArgs(ev);
                    oLinha.MS_Imprimir();
                }
                else
                {
                    if (AP_ImprimirCodigoDeBarras)
                    {
                        ITO_Linha.setIdentificador(oCampo.ToString());
                        LinhaBarraImpl oLinha = new LinhaBarraImpl();
                        oLinha.setConteudoLinha(ITO_Linha);
                        oLinha.setEventsArgs(ev);
                        oLinha.MS_Imprimir();
                    }
                }
            }
            ev.HasMorePages = false;
        }

        private void MI_ImprimirRecibo(object sender, PrintPageEventArgs ev)
        {
            FWK_DAO_XML IWK_DAO_XML = new FWK_DAO_XML();
            IWK_DAO_XML.MS_LerAquivoXML("Recibo.xml");
            DataSet dtgTeste = IWK_DAO_XML.MS_GetDataSet();

            CTO_Linha ITO_Linha = new CTO_Linha();

            for (int i = 0; i <= dtgTeste.Tables[0].Rows.Count - 1; i++)
            {
                ITO_Linha.setObjRef(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["ObjRef"].ToString()));
                ITO_Linha.setAlturaDaEtiqueta(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["AlturaDaEtiqueta"].ToString()));
                ITO_Linha.setAlturaDoCodigoDeBarras(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["AlturaDoCodigoDeBarras"].ToString()));
                ITO_Linha.setLarguraDoCodigoDeBarras(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["LarguraDoCodigoDeBarras"].ToString()));
                ITO_Linha.setPosicaoLateralDaEtiqueta(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["PosicaoLateralDaEtiqueta"].ToString()));
                ITO_Linha.setPosicaoVerticalTexto(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["PosicaoVerticalTexto"].ToString()));
                ITO_Linha.setPosicaoHorizontalTexto(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["PosicaoHorizontalTexto"].ToString()));
                ITO_Linha.setCampo(dtgTeste.Tables[0].Rows[i]["Campo"].ToString());
                ITO_Linha.setNomeFonte(dtgTeste.Tables[0].Rows[i]["NomeFonte"].ToString());
                ITO_Linha.setTamanhoFonte(Convert.ToInt32(dtgTeste.Tables[0].Rows[i]["TamanhoFonte"].ToString()));
                ITO_Linha.setEstiloFonte(dtgTeste.Tables[0].Rows[i]["EstiloFonte"].ToString());
                ITO_Linha.setPastaCodBarras(dtgTeste.Tables[0].Rows[i]["PastaCodBarras"].ToString());

                Assembly myClass = Assembly.GetAssembly(typeof(CAT_CadastrarVisitante));

                //Cria o objeto a ser invocado
                //Observem que o objeto deve vir acompanhado de seu namespace, que no nosso caso n�o informamos
                //pois esta no mesmo namespace desta classe
                object myObj = myClass.CreateInstance("CAT_CadastrarVisitante");
                object oCampo = "";
                //Invoca o m�todo Escreve da class teste
                if (ITO_Linha.getCampo() != string.Empty)
                {
                    string stNomeCampo;

                    CTO_Visitante ITO_Visitante;
                    ITO_Visitante = (CTO_Visitante)myObj.GetType().InvokeMember("MS_ObterPorCodigo", BindingFlags.InvokeMethod, null, myObj, new object[] { AP_Codigo });

                    if (ITO_Linha.getCampo().ToString().ToUpper() == "NOME")
                    {
                        oCampo = ITO_Visitante.getConsultora().ToString() + "  " + ITO_Visitante.getNomedaConsultora();
                    }
                    else if (ITO_Linha.getCampo().ToString().ToUpper() == "ITENS")
                    {
                        oCampo = "";
                    }
                    else
                    {
                        oCampo = "";
                    }
                }

                if (ITO_Linha.getCampo().ToString().ToUpper() == "TEXTO")
                {
                    ITO_Linha.setTexto("Itens recebidos");
                    LinhaTextoImpl oLinha = new LinhaTextoImpl();
                    oLinha.setConteudoLinha(ITO_Linha);
                    oLinha.setEventsArgs(ev);
                    oLinha.MS_Imprimir();
                }
                else if (ITO_Linha.getCampo().ToString().ToUpper() == "TEXTO2")
                {
                    ITO_Linha.setTexto("Assinatura:______________________________");
                    LinhaTextoImpl oLinha = new LinhaTextoImpl();
                    oLinha.setConteudoLinha(ITO_Linha);
                    oLinha.setEventsArgs(ev);
                    oLinha.MS_Imprimir();
                }
                else if (ITO_Linha.getCampo().ToString().ToUpper() == "ASSINATURA")
                {
                    ITO_Linha.setIdentificador(AP_Codigo.ToString());
                    LinhaAssinaturaImpl oLinha = new LinhaAssinaturaImpl();
                    oLinha.setConteudoLinha(ITO_Linha);
                    oLinha.setEventsArgs(ev);
                    oLinha.MS_Imprimir();
                }
                else if (ITO_Linha.getCampo().ToString().ToUpper() == "ITENS")
                {
                    ITO_Linha.setTexto(oCampo.ToString());
                    LinhaTextoImpl oLinha = new LinhaTextoImpl();
                    oLinha.setConteudoLinha(ITO_Linha);
                    oLinha.setEventsArgs(ev);
                    oLinha.MS_ImprimirSequencial(AP_dtItensDaConsultora, AP_Codigo);
                }
                else if (ITO_Linha.getLarguraDoCodigoDeBarras() <= 0)
                {
                    ITO_Linha.setTexto(oCampo.ToString());
                    LinhaTextoImpl oLinha = new LinhaTextoImpl();
                    oLinha.setConteudoLinha(ITO_Linha);
                    oLinha.setEventsArgs(ev);
                    oLinha.MS_Imprimir();
                }
                else
                {
                    if (AP_ImprimirCodigoDeBarras)
                    {
                        ITO_Linha.setIdentificador(oCampo.ToString());
                        LinhaBarraImpl oLinha = new LinhaBarraImpl();
                        oLinha.setConteudoLinha(ITO_Linha);
                        oLinha.setEventsArgs(ev);
                        oLinha.MS_Imprimir();
                    }
                }
            }
            ev.HasMorePages = false;
        }
    }
}
