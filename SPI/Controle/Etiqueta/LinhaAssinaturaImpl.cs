using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Printing;
using System.Data;

namespace SPI
{
    class LinhaAssinaturaImpl : ILinha
    {
        PrintPageEventArgs IEventsArgs;
        private CTO_Linha ITO_Linha;
        string stCodigoPararelo = "";


        public void MS_Imprimir()
        {
            IEventsArgs.Graphics.DrawImage(Image.FromFile(new CSV_VariaveisGlobal().getLocalArmazenamentoFoto() + "\\Assinatura\\" + ITO_Linha.getIdentificador() + ".jpg"),
                ITO_Linha.getPosicaoLateralDaEtiqueta(), 
                ITO_Linha.getAlturaDaEtiqueta(), 
                ITO_Linha.getLarguraDoCodigoDeBarras(),
                ITO_Linha.getAlturaDoCodigoDeBarras());

        }
        public void setEventsArgs(PrintPageEventArgs EV)
        {
            IEventsArgs = EV;
        }
        public void setConteudoLinha(CTO_Linha prITO_Linha)
        {
            ITO_Linha = prITO_Linha;
        }


    }
}
