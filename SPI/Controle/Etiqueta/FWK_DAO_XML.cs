using System;
using System.Data;
using System.IO;

namespace SPI
{
    class FWK_DAO_XML
    {
        public DataSet dtsRegistros = new DataSet();
        public DataTable dtbRegistros = new DataTable("dtbTeste");
       
        
        public void MS_LerAquivoXML (String prstArquivo)
        {
            dtbRegistros.Columns.Add("objref", typeof(string));
            dtbRegistros.Columns.Add("AlturaDaEtiqueta", typeof(string));
            dtbRegistros.Columns.Add("AlturaDoCodigoDeBarras", typeof(string));
            dtbRegistros.Columns.Add("LarguraDoCodigoDeBarras", typeof(string));
            dtbRegistros.Columns.Add("PosicaoLateralDaEtiqueta", typeof(string));
            dtbRegistros.Columns.Add("Texto", typeof(string));
            dtbRegistros.Columns.Add("Campo", typeof(string));
            dtbRegistros.Columns.Add("TipoLinha", typeof(string));
            dtbRegistros.Columns.Add("ImagemDoCodigoDeBarras", typeof(string));
            dtbRegistros.Columns.Add("Identificador", typeof(string));
            dtbRegistros.Columns.Add("PosicaoVerticalTexto", typeof(string));
            dtbRegistros.Columns.Add("PosicaoHorizontalTexto", typeof(string));
            dtbRegistros.Columns.Add("NomeFonte", typeof(string));
            dtbRegistros.Columns.Add("TamanhoFonte", typeof(string));
            dtbRegistros.Columns.Add("EstiloFonte", typeof(string));
            dtbRegistros.Columns.Add("PastaCodBarras", typeof(string));
            dtbRegistros.PrimaryKey = new DataColumn[] { dtbRegistros.Columns["ObjRef"] };

            if (File.Exists(prstArquivo))
            {
                dtbRegistros.ReadXml(prstArquivo);
            }
            dtsRegistros.Tables.Add(dtbRegistros);
        }

        public void MS_EscreverAquivoXML(String prstArquivo)
        {
            dtbRegistros.WriteXml(prstArquivo);
        }

        public DataSet  MS_GetDataSet()
        {
            return dtsRegistros;
        }

        public CTO_Linha MS_Obter(string prstObjRef)
        {
            CTO_Linha ITO_Linha = null;
            DataRow ofoundRow = dtbRegistros.Rows.Find(prstObjRef);
            if (ofoundRow != null)
            {
                ITO_Linha = new CTO_Linha();
                ITO_Linha.setObjRef(Convert.ToInt32(ofoundRow["objref"]));
                ITO_Linha.setCampo(ofoundRow["Campo"].ToString());
                ITO_Linha.setAlturaDaEtiqueta(Convert.ToInt32(ofoundRow["AlturaDaEtiqueta"]));
                ITO_Linha.setAlturaDoCodigoDeBarras(Convert.ToInt32(ofoundRow["AlturaDoCodigoDeBarras"]));
                ITO_Linha.setLarguraDoCodigoDeBarras(Convert.ToInt32(ofoundRow["LarguraDoCodigoDeBarras"]));
                ITO_Linha.setPosicaoLateralDaEtiqueta(Convert.ToInt32(ofoundRow["PosicaoLateralDaEtiqueta"]));
                ITO_Linha.setTexto(ofoundRow["Texto"].ToString());
                ITO_Linha.setCampo(ofoundRow["Campo"].ToString());
                ITO_Linha.setImagemDoCodigoDeBarras(ofoundRow["ImagemDoCodigoDeBarras"].ToString());
                ITO_Linha.setIdentificador(ofoundRow["Identificador"].ToString());
                ITO_Linha.setPosicaoVerticalTexto(Convert.ToInt32(ofoundRow["PosicaoVerticalTexto"]));
                ITO_Linha.setPosicaoHorizontalTexto(Convert.ToInt32(ofoundRow["PosicaoHorizontalTexto"]));
                ITO_Linha.setNomeFonte(ofoundRow["NomeFonte"].ToString());
                ITO_Linha.setTamanhoFonte(Convert.ToInt32(ofoundRow["TamanhoFonte"]));
                ITO_Linha.setPastaCodBarras(ofoundRow["PastaCodBarras"].ToString());
                ITO_Linha.setEstiloFonte(ofoundRow["EstiloFonte"].ToString());

                //ITO_Linha.setObjRef (ofoundRow["ObjRef"].ToString());
            }

            return ITO_Linha;
        }

        public void MS_Alterar(CTO_Linha ITO_Linha)
        {
            DataRow ofoundRow = dtbRegistros.Rows.Find(ITO_Linha.getObjRef());
            ofoundRow.BeginEdit();
            ofoundRow["objref"] = ITO_Linha.getObjRef();
            ofoundRow["Campo"] = ITO_Linha.getCampo();
            ofoundRow["AlturaDaEtiqueta"] = ITO_Linha.getAlturaDaEtiqueta();
            ofoundRow["AlturaDoCodigoDeBarras"] = ITO_Linha.getAlturaDoCodigoDeBarras();
            ofoundRow["LarguraDoCodigoDeBarras"] = ITO_Linha.getLarguraDoCodigoDeBarras();
            ofoundRow["PosicaoLateralDaEtiqueta"] = ITO_Linha.getPosicaoLateralDaEtiqueta();
            ofoundRow["Texto"] = ITO_Linha.getTexto();
            ofoundRow["Campo"] = ITO_Linha.getCampo();
            ofoundRow["ImagemDoCodigoDeBarras"] = ITO_Linha.getImagemDoCodigoDeBarras();
            ofoundRow["Identificador"] = ITO_Linha.getIdentificador();
            ofoundRow["PosicaoVerticalTexto"] = ITO_Linha.getPosicaoVerticalTexto();
            ofoundRow["PosicaoHorizontalTexto"] = ITO_Linha.getPosicaoHorizontalTexto();
            ofoundRow["NomeFonte"] = ITO_Linha.getNomeFonte();
            ofoundRow["TamanhoFonte"] = ITO_Linha.getTamanhoFonte().ToString();
            ofoundRow["EstiloFonte"] = ITO_Linha.getEstiloFonte();
            ofoundRow["PastaCodBarras"] = ITO_Linha.getPastaCodBarras();
            ofoundRow.EndEdit();
            MS_EscreverAquivoXML("Etiqueta.xml");
        }

        public void MS_Incluir(CTO_Linha ITO_Linha)
        {
            dtbRegistros.Rows.Find(ITO_Linha.getObjRef());
            DataRow ofoundRow = dtbRegistros.NewRow();
            ofoundRow.BeginEdit();
            ofoundRow["objref"] = (dtbRegistros.Rows.Count + 1);
            ofoundRow["Campo"] = ITO_Linha.getCampo();
            ofoundRow["AlturaDaEtiqueta"] = ITO_Linha.getAlturaDaEtiqueta();
            ofoundRow["AlturaDoCodigoDeBarras"] = ITO_Linha.getAlturaDoCodigoDeBarras();
            ofoundRow["LarguraDoCodigoDeBarras"] = ITO_Linha.getLarguraDoCodigoDeBarras();
            ofoundRow["PosicaoLateralDaEtiqueta"] = ITO_Linha.getPosicaoLateralDaEtiqueta();
            ofoundRow["Texto"] = ITO_Linha.getTexto();
            ofoundRow["Campo"] = ITO_Linha.getCampo();
            ofoundRow["ImagemDoCodigoDeBarras"] = ITO_Linha.getImagemDoCodigoDeBarras();
            ofoundRow["Identificador"] = ITO_Linha.getIdentificador();
            ofoundRow["PosicaoVerticalTexto"] = ITO_Linha.getPosicaoVerticalTexto();
            ofoundRow["PosicaoHorizontalTexto"] = ITO_Linha.getPosicaoHorizontalTexto();
            ofoundRow["NomeFonte"] = ITO_Linha.getNomeFonte();
            ofoundRow["TamanhoFonte"] = ITO_Linha.getTamanhoFonte().ToString();
            ofoundRow["EstiloFonte"] = ITO_Linha.getEstiloFonte();
            ofoundRow["PastaCodBarras"] = ITO_Linha.getPastaCodBarras();

            ofoundRow.EndEdit();
            dtbRegistros.Rows.Add(ofoundRow);
            MS_EscreverAquivoXML("Etiqueta.xml");
        }
    }
}
