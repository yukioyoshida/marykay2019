using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Printing;
using System.Data;

namespace SPI
{
    class LinhaBarraImpl : ILinha
    {
        PrintPageEventArgs IEventsArgs;
        private CTO_Linha ITO_Linha;
        string stCodigoPararelo = "";

        private ATLCONTROLLib.BarCode ICodigoBarras = new ATLCONTROLLib.BarCode();

        public void MS_Imprimir()
        {
            MI_MontarCodigoDeBarras();
            IEventsArgs.Graphics.DrawImage(Image.FromFile(ITO_Linha.getPastaCodBarras() +
                stCodigoPararelo + ".jpg"),
                ITO_Linha.getPosicaoLateralDaEtiqueta(), 
                ITO_Linha.getAlturaDaEtiqueta(), 
                ITO_Linha.getLarguraDoCodigoDeBarras(),
                ITO_Linha.getAlturaDoCodigoDeBarras());

        }
        public void setEventsArgs(PrintPageEventArgs EV)
        {
            IEventsArgs = EV;
        }
        public void setConteudoLinha(CTO_Linha prITO_Linha)
        {
            ITO_Linha = prITO_Linha;
        }
        public void MI_MontarCodigoDeBarras()
        {
            string prCodigoPararelo = MI_GerarCodigoBarrasPararelo(ITO_Linha.getIdentificador());

            ICodigoBarras.Code128CharSet = ATLCONTROLLib._Code128CharSet.Set_B;
            ICodigoBarras.UPCESystem = ATLCONTROLLib._UPCESystem._1;
            ICodigoBarras.AddCheckDigit = 0;
            ICodigoBarras.DataToEncode = prCodigoPararelo;
            ICodigoBarras.SymbologyID = ATLCONTROLLib._Symbology.INTERLEAVED25;
            ICodigoBarras.AutoSize = true;
            ICodigoBarras.BarHeight = 1;
            ICodigoBarras.ShowText = ATLCONTROLLib._BarcodeShowText.NO;
            ICodigoBarras.TopMarginCM = 0;
            ICodigoBarras.LeftMarginCM = 0;
            ICodigoBarras.SaveBarCode(ITO_Linha.getPastaCodBarras() + prCodigoPararelo + ".jpg");
        }

        private string MI_GerarCodigoBarrasPararelo(string prCodigoOriginal)
        {
            CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

            stCodigoPararelo = prCodigoOriginal;
            int itQuantidadeImpressao = 0;
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();

            /* PASSO 1 - CANCELAR C�DIGOS PARARELOS ANTERIORES */
            if ((CSV_Global.getPrioridade() != "3") && (CSV_Global.getPrioridade() != "4")) // JOIAS N�O PODE MUDAR O C�DIGO DE BARRAS
            {
                IAT_Visitante.MS_CancelarCodigosPararelos(prCodigoOriginal);
            }

            /* PASSO 2 - CONTAR QUANTIDADE DE CREDENCIAIS EMITIDAS */
            DataTable dtQuantidade = IAT_Visitante.MS_ObterQuantidadeImpressoes(prCodigoOriginal);
            try
            {
                itQuantidadeImpressao = Convert.ToInt32(dtQuantidade.Rows[0]["QT"].ToString());
                if ((CSV_Global.getPrioridade() != "3") && (CSV_Global.getPrioridade() != "4")) // JOIAS N�O PODE MUDAR O C�DIGO DE BARRAS
                {
                    itQuantidadeImpressao++;
                }
            }
            catch {
                itQuantidadeImpressao++;
            }
            stCodigoPararelo += "" + itQuantidadeImpressao.ToString("0#");

            /* PASSO 3 - GRAVAR O NOVO C�DIGO */
            if ((CSV_Global.getPrioridade() != "3") && (CSV_Global.getPrioridade() != "4")) // JOIAS N�O PODE MUDAR O C�DIGO DE BARRAS
            {
                IAT_Visitante.MS_GravarCodigoPararelo(prCodigoOriginal, stCodigoPararelo);
            }

            return stCodigoPararelo;
        }
    }
}
