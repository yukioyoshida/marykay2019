using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Printing;

namespace SPI
{
    class ImageImpl
    {
        PrintPageEventArgs IEventsArgs;
        private CTO_Linha ITO_Linha;

        CSV_VariaveisGlobal CSV_Glogal = new CSV_VariaveisGlobal();


        private Image MI_GerarImagem(string prCdRoteiro)
        {
            int positionAltura = 12;

            //Bitmap bmp = new Bitmap(70, 70);
            
            Bitmap bmp = new Bitmap(Image.FromFile("C:\\Fontes\\MaryKay\\marykay2019\\SPI\\Imagens\\imgCredencial.bmp"), 150, 150);


            Graphics h_LinhaFundo = Graphics.FromImage(bmp);
            //h_LinhaFundo.FillRectangle(Brushes.Black, 0, 0, 70, 70);
            h_LinhaFundo.Dispose();

            Graphics h_Linha = Graphics.FromImage(bmp);
            //h_Linha.FillRectangle(Brushes.White, 1, 10, 68, 59);
            h_Linha.Dispose();

            // TIPO RECONHECIMENTO
            string prTipoReconhecimento = new DAO_Visitante().MS_ObterRoteiroPorIdRoteiro(prCdRoteiro).Rows[0]["dsLocal"].ToString().Trim().ToUpper();
            h_Linha = Graphics.FromImage(bmp);
            h_Linha.DrawString(prTipoReconhecimento, new System.Drawing.Font("Lucida Sans Typewriter", 7), Brushes.White, new Point((prTipoReconhecimento == "PALCO" ? 18 : 12), 0));
            h_Linha.Dispose();

            string prTexto = new DAO_Visitante().MS_ObterRoteiroPorIdRoteiro(prCdRoteiro).Rows[0]["dsNomeCredencial"].ToString().Trim();
            if (prTexto.Length <= 0)
            {
                h_Linha = Graphics.FromImage(bmp);
                h_Linha.DrawString(prTexto, new System.Drawing.Font("Lucida Sans Typewriter", 8), Brushes.Black, new Point(0, positionAltura));
                h_Linha.Dispose();
            }
            else
            {
                string stexto = "";
                for (int i = 0; i <= prTexto.Length - 1; i++)
                {
                    stexto += prTexto.Substring(i, 1);
                    if (stexto.Length >= 10)
                    {
                        h_Linha = Graphics.FromImage(bmp);
                        h_Linha.DrawString(stexto, new System.Drawing.Font("Lucida Sans Typewriter", 8), Brushes.Black, new Point(0, positionAltura));
                        h_Linha.Dispose();
                        positionAltura += 10;
                        stexto = "";
                    }
                }

                if (stexto != string.Empty)
                {
                    h_Linha = Graphics.FromImage(bmp);
                    h_Linha.DrawString(stexto, new System.Drawing.Font("Lucida Sans Typewriter", 8), Brushes.Black, new Point(0, positionAltura));
                    h_Linha.Dispose();
                    positionAltura += 10;
                    stexto = "";
                }
            }

            Image img = (Image)bmp;

            return img;
        }

        public void MS_Imprimir(string cdObjeto, string stTipo, CTO_Visitante ITO_Visitante)
        {
            int LarguradaEtiqueta = ITO_Linha.getPosicaoLateralDaEtiqueta();
            int LarguradaEtiqueta2 = ITO_Linha.getPosicaoLateralDaEtiqueta();
            int LarguradaEtiqueta3 = ITO_Linha.getPosicaoLateralDaEtiqueta();
            int LarguradaEtiqueta4 = ITO_Linha.getPosicaoLateralDaEtiqueta();
            int LarguradaEtiqueta5 = ITO_Linha.getPosicaoLateralDaEtiqueta();

            if (stTipo.ToString().ToUpper() == "BANDEIRA")
            {
                string[] produtos = ITO_Linha.getIdentificador().Split(';');
                try
                {
                    foreach (string dsProduto in produtos)
                    {
                        if (LarguradaEtiqueta < 280)
                        {
                            if (MI_PermitirImpressao(dsProduto))
                            {
                                IEventsArgs.Graphics.DrawImage(MI_GerarImagem(dsProduto), LarguradaEtiqueta, ITO_Linha.getAlturaDaEtiqueta(), 70, 70);
                                LarguradaEtiqueta = LarguradaEtiqueta + 80;
                            }
                        }
                        else if (LarguradaEtiqueta2 < 280)
                        {
                            if (MI_PermitirImpressao(dsProduto))
                            {
                                IEventsArgs.Graphics.DrawImage(MI_GerarImagem(dsProduto), LarguradaEtiqueta2, ITO_Linha.getAlturaDaEtiqueta() + 70, 70, 70);
                                LarguradaEtiqueta2 = LarguradaEtiqueta2 + 80;
                            }
                        }
                        else if (LarguradaEtiqueta3 < 280)
                        {
                            if (MI_PermitirImpressao(dsProduto))
                            {
                                IEventsArgs.Graphics.DrawImage(MI_GerarImagem(dsProduto), LarguradaEtiqueta3, ITO_Linha.getAlturaDaEtiqueta() + 140, 70, 70);
                                LarguradaEtiqueta3 = LarguradaEtiqueta3 + 80;
                            }
                        }
                        else if (LarguradaEtiqueta4 < 280)
                        {
                            if (MI_PermitirImpressao(dsProduto))
                            {
                                IEventsArgs.Graphics.DrawImage(MI_GerarImagem(dsProduto), LarguradaEtiqueta4, ITO_Linha.getAlturaDaEtiqueta() + 210, 70, 70);
                                LarguradaEtiqueta4 = LarguradaEtiqueta4 + 80;
                            }
                        }
                        else if (LarguradaEtiqueta5 < 280)
                        {
                            if (MI_PermitirImpressao(dsProduto))
                            {
                                IEventsArgs.Graphics.DrawImage(MI_GerarImagem(dsProduto), LarguradaEtiqueta5, ITO_Linha.getAlturaDaEtiqueta() + 280, 70, 70);
                                LarguradaEtiqueta5 = LarguradaEtiqueta5 + 80;
                            }
                        }
                    }
                }
                catch { }
            }
            else if (stTipo.ToString().ToUpper() == "FOTO")
            {
                try
                {
                    IEventsArgs.Graphics.DrawImage(Image.FromFile(CSV_Glogal.getLocalArmazenamentoFoto() + "\\" + ITO_Linha.getIdentificador() + ".jpg"), ITO_Linha.getPosicaoLateralDaEtiqueta(), ITO_Linha.getAlturaDaEtiqueta(), 240, 170);
                }
                catch {
                    IEventsArgs.Graphics.DrawImage(Image.FromFile("Imagens/categoria/anonimo2.jpg"), ITO_Linha.getPosicaoLateralDaEtiqueta(), ITO_Linha.getAlturaDaEtiqueta(), 240, 170);
                }
            }
            else if (stTipo.ToString().ToUpper() == "CATEGORIA")
            {
                try
                {
                    IEventsArgs.Graphics.DrawImage(Image.FromFile("Imagens/categoria/" + ITO_Linha.getIdentificador() + ".jpg"), ITO_Linha.getPosicaoLateralDaEtiqueta(), ITO_Linha.getAlturaDaEtiqueta(), 384, 41);
                }
                catch { }
            }
        }
        public void setEventsArgs(PrintPageEventArgs EV)
        {
            IEventsArgs = EV;
        }
        public void setConteudoLinha(CTO_Linha prITO_Linha)
        {
            ITO_Linha = prITO_Linha;
        }

        private bool MI_PermitirImpressao(string prCdProduto)
        {
            bool boResult = false;
            return true;

            CAT_CadastrarProduto IAT_Produto = new CAT_CadastrarProduto();
            DataTable dtProduto = IAT_Produto.MS_ObterProdutoPorCodigo(prCdProduto);

            if (dtProduto.Rows[0]["Imprimir"].ToString() == "Aparece na credencial")
            {
                boResult = true;
            }

            return boResult;
        }
    }
}
