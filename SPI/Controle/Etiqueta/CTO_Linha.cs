using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace SPI
{
    class CTO_Linha
    {
        //  Atributos
        private int ObjRef;
        private int AlturaDaEtiqueta;
        private int AlturaDoCodigoDeBarras;
        private int LarguraDoCodigoDeBarras;
        private int PosicaoLateralDaEtiqueta;
        private string Texto;
        private string Campo;
        private int TipoLinha;
        private string ImagemDoCodigoDeBarras;
        private string Identificador;
        private int PosicaoVerticalTexto;
        private int PosicaoHorizontalTexto;
        private string NomeFonte;
        private int TamanhoFonte;
        private string EstiloFonte;
        private string PastaCodBarras;

        //  Setters
        public void setAlturaDaEtiqueta(int pritAlturaDaEtiqueta)
        {
            AlturaDaEtiqueta = pritAlturaDaEtiqueta;
        }
        public void setAlturaDoCodigoDeBarras(int pritAlturaDoCodigoDeBarras)
        {
            AlturaDoCodigoDeBarras = pritAlturaDoCodigoDeBarras;
        }
        public void setLarguraDoCodigoDeBarras(int pritLarguraDoCodigoDeBarras)
        {
            LarguraDoCodigoDeBarras = pritLarguraDoCodigoDeBarras;
        }
        public void setPosicaoLateralDaEtiqueta(int pritPosicaoLateralDaEtiqueta)
        {
            PosicaoLateralDaEtiqueta = pritPosicaoLateralDaEtiqueta;
        }
        public void setTexto(string pritTexto)
        {
            Texto = pritTexto;
        }
        public void setCampo(string pritCampo)
        {
            Campo = pritCampo;
        }
        public void setIdentificador(string pritIdentificador)
        {
            Identificador = pritIdentificador;
        }
        public void setTipoLinha(int pritTipoLinha)
        {
            TipoLinha = pritTipoLinha;
        }
        public void setImagemDoCodigoDeBarras(string prImagemCodBar)
        {
            ImagemDoCodigoDeBarras = prImagemCodBar;
        }
        public void setPosicaoVerticalTexto(int pritPosicaoVertical)
        {
            PosicaoVerticalTexto = pritPosicaoVertical;
        }
        public void setPosicaoHorizontalTexto(int pritPosicaoHorizontal)
        {
            PosicaoHorizontalTexto = pritPosicaoHorizontal;
        }
        public void setObjRef(int pritObjRef)
        {
            ObjRef = pritObjRef;
        }
        public void setNomeFonte(string prstNomeFonte)
        {
            NomeFonte = prstNomeFonte;
        }
        public void setTamanhoFonte(int pritTamanhoFonte)
        {
            TamanhoFonte = pritTamanhoFonte;
        }
        public void setEstiloFonte(string prstEstiloFonte)
        {
            EstiloFonte = prstEstiloFonte;
        }
        public void setPastaCodBarras(string prstPastaCodBarras)
        {
            PastaCodBarras = prstPastaCodBarras;
        }


        //  Getters
        public int getAlturaDaEtiqueta()
        {
            return AlturaDaEtiqueta;
        }
        public int getAlturaDoCodigoDeBarras()
        {
            return AlturaDoCodigoDeBarras;
        }
        public int getLarguraDoCodigoDeBarras()
        {
            return LarguraDoCodigoDeBarras;
        }
        public int getPosicaoLateralDaEtiqueta()
        {
            return PosicaoLateralDaEtiqueta;
        }
        public string getTexto()
        {
            return Texto;
        }
        public string getCampo()
        {
            return Campo;
        }
        public int getTipoLinha()
        {
            return TipoLinha;
        }
        public string getImagemDoCodigoDeBarras()
        {
            return ImagemDoCodigoDeBarras;
        }
        public string getIdentificador()
        {
            return Identificador;
        }
        public int getPosicaoVerticalTexto()
        {
            return PosicaoVerticalTexto;
        }
        public int getPosicaoHorizontalTexto()
        {
            return PosicaoHorizontalTexto;
        }
        public int getObjRef()
        {
            return ObjRef;
        }
        public string getNomeFonte()
        {
            return NomeFonte;
        }
        public int getTamanhoFonte()
        {
            return TamanhoFonte;
        }
        public string getEstiloFonte()
        {
            return EstiloFonte;
        }
        public string getPastaCodBarras()
        {
            return PastaCodBarras;
        }
    }
}
