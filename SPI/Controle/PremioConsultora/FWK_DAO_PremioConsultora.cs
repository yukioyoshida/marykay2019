﻿using System;
using System.Collections.Generic;

namespace SPI
{
    public class FWK_DAO_PremioConsultora : FWK_DAO_MySQL
    {
        public List<CTO_PremioConsultora> MS_DataSetToEntidade()
        {
            List<CTO_PremioConsultora> lITO_PremioConsultora = new List<CTO_PremioConsultora>();
            CTO_PremioConsultora ITO_PremioConsultora;

            while (AP_DataReader.Read())
            {
                ITO_PremioConsultora = new CTO_PremioConsultora();

                ITO_PremioConsultora.ITO_Roteiro = new CTO_Roteiro();
                ITO_PremioConsultora.ITO_PremioConsultoraItem = new CTO_PremioConsultoraItem();

                ITO_PremioConsultora.ObjRef = AP_DataReader["ObjRef"] == DBNull.Value ? 0 : Convert.ToInt32(AP_DataReader["ObjRef"]);
                ITO_PremioConsultora.NrConsultora = AP_DataReader["NrConsultora"] == null ? "" : AP_DataReader["NrConsultora"].ToString();
                ITO_PremioConsultora.dsReconhecimento = AP_DataReader["PREM_RECONHECIMENTO"] == null ? "" : AP_DataReader["PREM_RECONHECIMENTO"].ToString();
                ITO_PremioConsultora.ATIVO = AP_DataReader["ATIVO"] == null ? "" : AP_DataReader["ATIVO"].ToString();
                ITO_PremioConsultora.ITO_PremioConsultoraItem.cdItem = AP_DataReader["cdItem"] == DBNull.Value ? 0 : Convert.ToInt32(AP_DataReader["cdItem"]);
                ITO_PremioConsultora.ITO_PremioConsultoraItem.cdReconhecimento = AP_DataReader["cdReconhecimento"] == DBNull.Value ? 0 : Convert.ToInt32(AP_DataReader["cdReconhecimento"]);
                ITO_PremioConsultora.ITO_PremioConsultoraItem.dsEscolhaPre = AP_DataReader["dsEscolhaPre"] == null ? "" : AP_DataReader["dsEscolhaPre"].ToString();
                ITO_PremioConsultora.ITO_PremioConsultoraItem.dsTrocaLocal = AP_DataReader["dsTrocaLocal"] == null ? "" : AP_DataReader["dsTrocaLocal"].ToString();
                ITO_PremioConsultora.ITO_PremioConsultoraItem.dtTroca = AP_DataReader["dtTroca"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(AP_DataReader["dtTroca"]);
                ITO_PremioConsultora.ITO_PremioConsultoraItem.dsRetirado = AP_DataReader["dsRetirado"] == null ? "" : AP_DataReader["dsRetirado"].ToString();
                ITO_PremioConsultora.ITO_PremioConsultoraItem.dtRetirado = AP_DataReader["dtRetirado"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(AP_DataReader["dtRetirado"]);
                ITO_PremioConsultora.ITO_PremioConsultoraItem.dsObservacoes = AP_DataReader["dsObservacoes"] == null ? "" : AP_DataReader["dsObservacoes"].ToString();
                ITO_PremioConsultora.ITO_PremioConsultoraItem.dsTipo = AP_DataReader["dsTipo"] == null ? "" : AP_DataReader["dsTipo"].ToString();
                ITO_PremioConsultora.ITO_PremioConsultoraItem.Correios = AP_DataReader["Correios"] == null ? "" : AP_DataReader["Correios"].ToString();
                ITO_PremioConsultora.ITO_PremioConsultoraItem.dsCodigoBarrasProduto = AP_DataReader["ITEM_CODIGOBARRAS"] == null ? "" : AP_DataReader["ITEM_CODIGOBARRAS"].ToString();
                ITO_PremioConsultora.ITO_Roteiro.cdRoteiro = AP_DataReader["cdRoteiro"] == DBNull.Value ? 0 : Convert.ToInt32(AP_DataReader["cdRoteiro"]);
                ITO_PremioConsultora.ITO_Roteiro.dsReconhecimento = AP_DataReader["ROT_RECONHECIMENTO"] == null ? "" : AP_DataReader["ROT_RECONHECIMENTO"].ToString();
                ITO_PremioConsultora.ITO_Roteiro.dsLocal = AP_DataReader["ROT_RECONHECIMENTO"] == null ? "" : AP_DataReader["ROT_RECONHECIMENTO"].ToString();
                ITO_PremioConsultora.ITO_Roteiro.dsCodigoBarrasProduto = AP_DataReader["ROT_CODIGOBARRAS"] == null ? "" : AP_DataReader["ROT_CODIGOBARRAS"].ToString();
                ITO_PremioConsultora.ITO_Roteiro.dsQuantidadeEstoque = AP_DataReader["dsQuantidadeEstoque"] == DBNull.Value ? 0 : Convert.ToInt32(AP_DataReader["dsQuantidadeEstoque"]);
                ITO_PremioConsultora.ITO_Roteiro.dsSituacao = AP_DataReader["dsSituacao"] == null ? "" : AP_DataReader["dsSituacao"].ToString();

                lITO_PremioConsultora.Add(ITO_PremioConsultora);
            }
            return lITO_PremioConsultora;
        }
    }
}
