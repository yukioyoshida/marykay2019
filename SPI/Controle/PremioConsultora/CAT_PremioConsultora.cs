﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SPI
{
    public class CAT_PremioConsultora
    {
        public CAT_PremioConsultora()
        {
        }

        public List<CTO_PremioConsultora> MS_ObterPremiacaoPorCodigoConsultora(string stCodigo)
        {
            List<CTO_PremioConsultora> lITO_PremioConsultora = new List<CTO_PremioConsultora>();
            DAO_PremioConsultora dadosConsultora = new DAO_PremioConsultora();

            return lITO_PremioConsultora = dadosConsultora.MS_ObterPremiacaoPorCodigoConsultora(stCodigo);
        }
    }
}
