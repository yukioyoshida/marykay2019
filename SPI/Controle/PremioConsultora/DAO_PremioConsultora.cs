﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SPI
{
    public class DAO_PremioConsultora : FWK_DAO_PremioConsultora
    {
        public DAO_PremioConsultora()
        {
        }

        public List<CTO_PremioConsultora> MS_ObterPremiacaoPorCodigoConsultora(string stCodigo)
        {
            CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();
            string prAcao = "<>";
            string prOr = string.Empty;
            if (CSV_Global.getTipoJoia() == "FAIXA")
            {
                prAcao = "=";
            }
            else
            {
                prOr = "OR ITEM.dsTrocaLocal = ''";
            }

            string sqlQuery =   "SELECT " +
                                "PREM.ObjRef, PREM.NrConsultora, PREM.dsReconhecimento, PREM.ATIVO, ITEM.cdItem, ITEM.cdReconhecimento, ITEM.dsEscolhaPre, ITEM.dsTrocaLocal, ITEM.dtTroca, ITEM.dsRetirado, ITEM.dtRetirado, ITEM.dsObservacoes, ITEM.dsTipo, ITEM.Correios, ITEM.dsCodigoBarrasProduto, ROT.cdRoteiro, ROT.dsReconhecimento, ROT.dsLocal, ROT.dsCodigoBarrasProduto, ROT.dsQuantidadeEstoque, ROT.dsSituacao " +
                                "FROM " +
                                "    tblPremioConsultora PREM " +
                                "        LEFT JOIN tblPremioConsultora_Item ITEM ON " +
                                "            PREM.ObjRef = ITEM.cdReconhecimento " +
                                "        LEFT JOIN tblRoteiro ROT ON " +
                                "            ROT.cdRoteiro = ITEM.dsTrocaLocal " +
                                "WHERE " +
                                "PREM.NrConsultora = @NrConsultora AND (TipoJoia " + prAcao + " 'Faixa' " + prOr + " ) AND PREM.ATIVO = 'S' ";


            var resultado = AP_Connection.Query<CTO_PremioConsultora, CTO_PremioConsultoraItem, CTO_Roteiro, CTO_PremioConsultora>(
                sqlQuery, 
                map: (PREM, ITEM, ROT) => {
                    PREM.ITO_PremioConsultoraItem = ITEM;
                    PREM.ITO_Roteiro = ROT;

                    return PREM;
                },
                splitOn: "cdItem,cdRoteiro", 
                param: new { @NrConsultora = stCodigo });

            return (List<CTO_PremioConsultora>)resultado;
        }
    }
}
