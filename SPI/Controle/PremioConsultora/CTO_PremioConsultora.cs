﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SPI
{
    public class CTO_PremioConsultora
    {
        public CTO_PremioConsultora()
        {
            ITO_Roteiro = new CTO_Roteiro();
            ITO_PremioConsultoraItem = new CTO_PremioConsultoraItem();
        }

        public int ObjRef { get; set; }
        public string NrConsultora { get; set; }
        public string dsReconhecimento { get; set; }
        public string ATIVO { get; set; }
        public CTO_Roteiro ITO_Roteiro { get; set; }
        public CTO_PremioConsultoraItem ITO_PremioConsultoraItem { get; set; }
    }
}
