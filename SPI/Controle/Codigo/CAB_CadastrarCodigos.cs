using System;
using SPI;

public abstract class CAB_CadastrarCodigos : ICadastrarCodigos
{
    public abstract void MS_Incluir(CTO_Codigos ITO_Codigos);
    public abstract void MS_Alterar(CTO_Codigos ITO_Codigos);
    public abstract void MS_Excluir(CTO_Codigos ITO_Codigos);
    public abstract CTO_Codigos MS_Obter(int pritObjRef);
    public abstract CTO_Codigos MS_Obter(string prstID);
    public abstract CTO_Codigos MS_ObterPeloCodFilha(string prstCodFica);
}

