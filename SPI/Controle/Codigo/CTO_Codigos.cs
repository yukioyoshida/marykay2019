using System;
using SPI;
namespace SPI 
{

    public class CTO_Codigos 
    {
          //  Declaracao dos atributos Puros
        int AP_ObjRef;
        string AP_CodFicha;
        string AP_Uso;
        string AP_CodigoBarras;
        string AP_Internacional;

        public CTO_Codigos () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public int getObjRef()
        {
            return AP_ObjRef;
        }
        public string getCodFicha()
        {
            return AP_CodFicha;
        }
        public string getUso()
        {
            return AP_Uso;
        }
        public string getCodigoBarras()
        {
            return AP_CodigoBarras;
        }
        public string getInternacional()
        {
            return AP_Internacional;
        }
         //  Declaracao dos Metodos set
        public void setObjRef( int prObjRef)
        {
            AP_ObjRef = prObjRef;
        }
        public void setCodFicha( string prCodFicha)
        {
            AP_CodFicha = prCodFicha;
        }
        public void setUso( string prUso)
        {
            AP_Uso = prUso;
        }
        public void setCodigoBarras( string prCodigoBarras)
        {
            AP_CodigoBarras = prCodigoBarras;
        }
        public void setInternacional(string prInternacional)
        {
            AP_Internacional = prInternacional;
        }
    }
}

