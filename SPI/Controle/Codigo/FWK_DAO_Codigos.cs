using System.Data;
namespace SPI
{
    public class FWK_DAO_Codigos : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                           "          ObjRef, " +
                           "          CodFicha, " +
                           "          Uso, " +
                           "          CodigoBarras, " +
                           "          Internacional ";

        public FWK_DAO_Codigos()
        {
        }

        public void MS_Incluir(CTO_Codigos ITO_Codigos)
        {
            int itObjRef = MS_NovoObjRef();
            MI_SetSql ("INSERT INTO tblCodigos (" +
                       "          ObjRef, " +
                       "          CodFicha, " +
                       "          Uso, " +
                       "          CodigoBarras, " +
                       "          Internacional) " + 
                       " VALUES (" + 
                       "          @ObjRef, " +
                       "          @CodFicha, " +
                       "          @Uso, " +
                       "          @CodigoBarras, " +
                       "          @Internacional) ");

            MI_AddParameters("@ObjRef", itObjRef);
            MI_AddParameters("@CodFicha", ITO_Codigos.getCodFicha());
            MI_AddParameters("@Uso", ITO_Codigos.getUso());
            MI_AddParameters("@CodigoBarras", ITO_Codigos.getCodigoBarras());
            MI_AddParameters("@Internacional", ITO_Codigos.getInternacional());

            MI_ExecuteNonQuery();
        }

        public void MS_Alterar(CTO_Codigos ITO_Codigos)
        {
            MI_SetSql ("UPDATE tblCodigos SET " +
                       "          ObjRef = @ObjRef, " +
                       "          CodFicha = @CodFicha, " +
                       "          Uso = @Uso, " +
                       "          CodigoBarras = @CodigoBarras, " +
                       "          Internacional = @Internacional " + 
                       "WHERE ObjRef = @ObjRef ");
            MI_AddParameters("@ObjRef", ITO_Codigos.getObjRef());
            MI_AddParameters("@CodFicha", ITO_Codigos.getCodFicha());
            MI_AddParameters("@Uso", ITO_Codigos.getUso());
            MI_AddParameters("@CodigoBarras", ITO_Codigos.getCodigoBarras());
            MI_AddParameters("@Internacional", ITO_Codigos.getInternacional());
            
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_Excluir(CTO_Codigos ITO_Codigos)
        {
            MI_SetSql ("DELETE FROM tblCodigos WHERE ObjRef = @ObjRef");
            MI_AddParameters("@ObjRef", ITO_Codigos.getObjRef());
            
            MI_ExecuteNonQuery();
        }

        public CTO_Codigos MS_Obter(int pritObjRef)
        {
            CTO_Codigos ITO_Codigos = new CTO_Codigos();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          CodFicha, " +
                       "          Uso, " +
                       "          CodigoBarras, " +
                       "          Internacional " + 
                       "FROM tblCodigos " + 
                       "WHERE Objref = @ObjRef ");
            MI_AddParameters("@Objref", pritObjRef);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Codigos = null;
            else
                ITO_Codigos = MI_DataSetToEntidade();
            return ITO_Codigos;
        }

        public CTO_Codigos MI_DataSetToEntidade()
        {
            CTO_Codigos ITO_Codigos = new CTO_Codigos();            
            try
            {
                ITO_Codigos.setObjRef(AP_DataReader.GetInt32(0));
            }
            catch
            {
                ITO_Codigos.setObjRef(0);
            }
            try
            {
                ITO_Codigos.setCodFicha(AP_DataReader.GetString(1));
            }
            catch
            {
                ITO_Codigos.setCodFicha("");
            }
            try
            {
                ITO_Codigos.setUso(AP_DataReader.GetString(2));
            }
            catch
            {
                ITO_Codigos.setUso("");
            }
            try
            {
                ITO_Codigos.setCodigoBarras(AP_DataReader.GetString(3));
            }
            catch
            {
                ITO_Codigos.setCodigoBarras("");
            }
            try
            {
                ITO_Codigos.setInternacional(AP_DataReader.GetString(4));
            }
            catch
            {
                ITO_Codigos.setInternacional("");
            }
            return ITO_Codigos;
        }

        public CTO_Codigos MS_ObterTodos()
        {
            CTO_Codigos ITO_Codigos = new CTO_Codigos();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          CodFicha, " +
                       "          Uso, " +
                       "          CodigoBarras, " +
                       "          Internacional " + 
                       "FROM tblCodigos " );
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Codigos = null;
            else
                ITO_Codigos = MI_DataSetToEntidade();
            return ITO_Codigos;
        }

        public CTO_Codigos MS_ObterProximo()
        {
            CTO_Codigos ITO_Codigos = new CTO_Codigos();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_Codigos = null;
            else
                ITO_Codigos = MI_DataSetToEntidade();
                return ITO_Codigos;
        }

        public int MS_NovoObjRef()
        {
            int itObjRef;
            MI_SetSql("Select Max(ObjRef) from tblCodigos");
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 1;
            }
            else
            {
                itObjRef = AP_DataReader.GetInt32(0);
                itObjRef++;
            }
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblCodigos");
            return MI_ExecuteDataSet();
        }

    }
}

