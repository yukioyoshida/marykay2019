using System;
using SPI;

public interface ICadastrarCodigos
{
    void MS_Incluir(CTO_Codigos ITO_Codigos);
    void MS_Alterar(CTO_Codigos ITO_Codigos);
    void MS_Excluir(CTO_Codigos ITO_Codigos);
    CTO_Codigos MS_Obter(int pritObjRef);
    CTO_Codigos MS_Obter(string prstID);
    CTO_Codigos MS_ObterPeloCodFilha(string prstCodFica);
}

