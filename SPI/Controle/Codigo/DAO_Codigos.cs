namespace SPI
{
    public class DAO_Codigos : FWK_DAO_Codigos
    {
        public DAO_Codigos()
        {
        }

        public CTO_Codigos MS_Obter(string prstID)
        {
            CTO_Codigos ITO_Codigos = new CTO_Codigos();
            MI_SetSql (AP_SelectALL + 
                       "FROM tblCodigos " + 
                       "WHERE ObjRef = @ID ");
            MI_AddParameters("@ID", prstID);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Codigos = null;
            else
                ITO_Codigos = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Codigos;
        }

        public CTO_Codigos MS_ObterPeloCodFilha(string prstCodFica)
        {
            CTO_Codigos ITO_Codigos = new CTO_Codigos();
            MI_SetSql(AP_SelectALL +
                       "FROM tblCodigos " +
                       "WHERE CodFicha = @ID ");
            MI_AddParameters("@ID", prstCodFica);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Codigos = null;
            else
                ITO_Codigos = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Codigos;
        }
    }
}