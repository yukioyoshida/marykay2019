using System;
using SPI;
using SPI;

public class CAT_CadastrarCodigos : CAB_CadastrarCodigos
{
    public CAT_CadastrarCodigos()
    {
    }
    public override void MS_Incluir(CTO_Codigos ITO_Codigos)
    {
         DAO_Codigos IDAO_Codigos = new DAO_Codigos();
         IDAO_Codigos.MS_Incluir(ITO_Codigos);
    }
    public override void MS_Alterar(CTO_Codigos ITO_Codigos)
    {
        DAO_Codigos IDAO_Codigos = new DAO_Codigos();
        IDAO_Codigos.MS_Alterar(ITO_Codigos);
    }
    public override void MS_Excluir(CTO_Codigos ITO_Codigos)
    {
        DAO_Codigos IDAO_Codigos = new DAO_Codigos();
        IDAO_Codigos.MS_Excluir(ITO_Codigos);
    }
    public override CTO_Codigos MS_Obter(int pritObjRef)
    {
          DAO_Codigos IDAO_Codigos = new DAO_Codigos();
          return IDAO_Codigos.MS_Obter(pritObjRef);
    }
    public override CTO_Codigos MS_Obter(string prstID)
    {
         DAO_Codigos IDAO_Codigos = new DAO_Codigos();
         return IDAO_Codigos.MS_Obter(prstID);
    }

     public override CTO_Codigos MS_ObterPeloCodFilha(string prstCodFica)
     {
         DAO_Codigos IDAO_Codigos = new DAO_Codigos();
         return IDAO_Codigos.MS_ObterPeloCodFilha(prstCodFica);
     }
}

