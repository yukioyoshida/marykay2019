using System;
namespace SPI
{

    public class CTO_VariavelGlobal1
    {
        static string AP_NomeEvento = "AVESUI 2012";
        static string AP_Terminal = string.Empty;
        static string AP_Usuario = string.Empty;
        static string AP_NivelAcesso = string.Empty;
        
        public CTO_VariavelGlobal1()
        {
        }

        // M�todos gets
        public string getNomeEvento()
        {
            return AP_NomeEvento;
        }
        public string getUsuario()
        {
            return AP_Usuario;
        }
        public string getNivelAcesso()
        {
            return AP_NivelAcesso;
        }
        public string getTerminal()
        {
            return AP_Terminal;
        }

        // M�todos sets
        public void setUsuario(string prUsuario)
        {
            AP_Usuario = prUsuario;
        }
        public void setNivelAcesso(string prNivelAcesso)
        {
            AP_NivelAcesso = prNivelAcesso;
        }
        public void setTerminal(string prTerminal)
        {
            AP_Terminal = prTerminal;
        }
    }
}


