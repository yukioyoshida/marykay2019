﻿namespace SPI
{
    class CSV_VariaveisGlobal
    {
        //DECLARAÇÃO DE ATRIBUTOS
        static string AP_Usuario = string.Empty;
        static string AP_Prioridade = string.Empty;
        static string AP_NomeEvento = "MARY KAY";

        static string AP_Cadeado = string.Empty;
        static string AP_Terminal = string.Empty;
        static string AP_LocalArmazenamentoFoto = string.Empty;
        static string AP_Seminario = string.Empty;
        static string AP_TipoJoia = string.Empty;
        static string AP_Versao = "6";
        static string AP_CancelaJoia = string.Empty;
        static string AP_CancelaFaixa = string.Empty;

        //METODOS GETs
        public string getUsuario()
        {
            return AP_Usuario;
        }
        public string getPrioridade()
        {
            return AP_Prioridade;
        }
        public string getNumeroTerminal()
        {
            return AP_Terminal;
        }
        public string getLocalArmazenamentoFoto()
        {
            return AP_LocalArmazenamentoFoto;
        }
        public string getNomeEvento()
        {
            return AP_NomeEvento;
        }
        public string getCadeado()
        {
            return AP_Cadeado;
        }
        public string getSemiario()
        {
            return AP_Seminario;
        }
        public string getTipoJoia()
        {
            return AP_TipoJoia;
        }
        public string getVersao()
        {
            return AP_Versao;
        }

        public string getCancelaJoia()
        {
            return AP_CancelaJoia;
        }

        public string getCancelaFaixa()
        {
            return AP_CancelaFaixa;
        }

        //METODOS SETs
        public void setUsuario(string prstUsuario)
        {
            AP_Usuario = prstUsuario;
        }
        public void setPrioridade(string prstPrioridade)
        {
            AP_Prioridade = prstPrioridade;
        }
        public void setNumeroTerminal(string prstNumeroTerminal)
        {
            AP_Terminal = prstNumeroTerminal;
        }
        public void setLocalArmazenamentoFoto(string prstLocalArmazenamentoFoto)
        {
            AP_LocalArmazenamentoFoto = prstLocalArmazenamentoFoto;
        }
        public void setNomeEvento(string prstNomeEvento)
        {
            AP_NomeEvento = prstNomeEvento;
        }
        public void setCadedo(string prstCadeado)
        {
            AP_Cadeado = prstCadeado;
        }
        public void setSeminario(string prstSeminario)
        {
            AP_Seminario = prstSeminario;
        }
        public void setTipoJoia(string prTipoJoia)
        {
            AP_TipoJoia = prTipoJoia;
        }
        public void setVersao(string prVersao)
        {
            AP_Versao = prVersao;
        }

        public void setCancelaFaixa(string prCancelaFaixa)
        {
            AP_CancelaFaixa = prCancelaFaixa;
        }
        public void setCancelaJoia(string prCancelaJoia)
        {
            AP_CancelaJoia = prCancelaJoia;
        }

        //SERVICOS AUXILIARES
        public void MI_LogOutUsuario()
        {
            setUsuario("");
            setPrioridade("");
        }
    }
}