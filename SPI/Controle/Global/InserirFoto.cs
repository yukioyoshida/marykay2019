﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;

namespace SPI
{
    class InserirFoto
    {
        public void MS_SetFoto(PictureBox picture, string prIdRoterio)
        {
            try
            {
                var ms = new MemoryStream(new DAO_Visitante().MS_ObterFotoRoteiro(Convert.ToInt32(prIdRoterio)));
                picture.Image = Image.FromStream(ms);
            }
            catch
            {
                picture = new PictureBox();
            }
        }
    }
}
