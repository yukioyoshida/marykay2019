﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Printing;
using System.Data;
using System.Drawing;

namespace SPI
{
    class CSV_Certificado
    {
        static string AP_Nome = string.Empty;
        static string AP_Curso = string.Empty;

        static bool AP_Congresso = false;
        static bool AP_Curso2 = false;
        static bool AP_Curso3 = false;

        public static Int32 AP_ProdutoImpressao = 0;

        static CTO_Visitante ITO_VisitanteAux;

        static DataTable dt = new DataTable("Certificado");
        static DataRow dr;

        static string stTexto = string.Empty;
        static string stRodape = string.Empty;

        public static void MS_ImprimirCertificado(CTO_Visitante ITO_Visitante)
        {
            //AP_Nome = ITO_Visitante.getNomeCompleto();
            //AP_Curso = ITO_Visitante.getPergunta1();
            //ITO_VisitanteAux = ITO_Visitante;

            //MI_ObterConfiguracoes();
            //MI_ImprimeCertificado();
        }

        private static void MI_ObterConfiguracoes()
        {
            dt.Columns.Clear();
            dt = new DataTable("Certificado");
            dt.Columns.Add("ALTURA", typeof(string));
            dt.Columns.Add("LARGURA", typeof(string));
            dt.Columns.Add("FONT", typeof(string));
            dt.Columns.Add("TAMANHOFONT", typeof(string));
            dt.Columns.Add("ORIENTACAO", typeof(string));
            dt.Columns.Add("IMPRESSORA", typeof(string));

            dt.ReadXml("Certificado.xml");
         }

        private static void MI_ImprimeCertificado()
        {
            dr = dt.Rows[0];
            PrintDocument certificado = new PrintDocument();
            certificado.PrintPage += new PrintPageEventHandler(certificado_PrintPage);

            certificado.PrinterSettings.PrinterName = dr["IMPRESSORA"].ToString();

            if (dr["ORIENTACAO"].ToString().ToUpper() == "PAISAGEM")
                certificado.DefaultPageSettings.Landscape = true;
            else
                certificado.DefaultPageSettings.Landscape = false;

            certificado.OriginAtMargins = true;
            certificado.PrinterSettings.PrintToFile = true;

            certificado.Print();
            
        }

        private static void certificado_PrintPage(object sender, PrintPageEventArgs ev)
        {
            MI_ObterDescricaoCertificado(ITO_VisitanteAux);

            System.Drawing.Pen myPen = new System.Drawing.Pen(System.Drawing.Brushes.Black);

            StringFormat Centralizado = new StringFormat();
            Centralizado.Alignment = System.Drawing.StringAlignment.Center;

            //// CERTIFICAMOS QUE
            //dr = dt.Rows[2];
            ////if (dr["LARGURA"].ToString().ToUpper() == "CENTRO")
            ////{
            //System.Drawing.Font myFonta = new System.Drawing.Font(dr["FONT"].ToString(), Convert.ToInt32(dr["TAMANHOFONT"]));
            ////LINHA DO NOME

            //// Construct a new Rectangle .
            //Rectangle displayRectanglea = new Rectangle(new Point(Convert.ToInt32(dr["LARGURA"]), Convert.ToInt32(dr["ALTURA"])), new Size(950, 100));

            //// Construct 2 new StringFormat objects
            //StringFormat format1a = new StringFormat(StringFormatFlags.NoClip);
            //format1a.Alignment = System.Drawing.StringAlignment.Center;
            
            //StringFormat format2a = new StringFormat(format1a);
            //format2a.Alignment = System.Drawing.StringAlignment.Center;

            //ev.Graphics.DrawString("Certificamos que", myFonta, Brushes.Black, (RectangleF)displayRectanglea, format1a);

            //NOME
            dr = dt.Rows[0];
            //if (dr["LARGURA"].ToString().ToUpper() == "CENTRO")
            //{
                System.Drawing.Font myFont1 = new System.Drawing.Font(dr["FONT"].ToString(), Convert.ToInt32(dr["TAMANHOFONT"]), FontStyle.Bold);
                //LINHA DO NOME

                // Construct a new Rectangle .
                Rectangle displayRectangle = new Rectangle(new Point(Convert.ToInt32(dr["LARGURA"]), Convert.ToInt32(dr["ALTURA"])), new Size(950, 100));

                // Construct 2 new StringFormat objects
                StringFormat format1 = new StringFormat(StringFormatFlags.NoClip);
                format1.Alignment = System.Drawing.StringAlignment.Center;
                StringFormat format2 = new StringFormat(format1);
                format2.Alignment = System.Drawing.StringAlignment.Center;

                ev.Graphics.DrawString(AP_Nome, myFont1, Brushes.Black, (RectangleF)displayRectangle, format1);

                dr = dt.Rows[1];
                System.Drawing.Font myFont3 = new System.Drawing.Font(dr["FONT"].ToString(), Convert.ToInt32(dr["TAMANHOFONT"]));
                
            
                ////LINHA DOS CURSOS
                //int itLinha = 1;
                //int itAumento = 50;

                //int itQuantidadeCursos = MI_ObterCursos(ITO_VisitanteAux);
                //int itCursoImpresso = 0;

                //string stPonto = "";
                
                ////foreach (DataRow drCursos in MI_ObterCursos(ITO_VisitanteAux).Rows)
                //    itCursoImpresso++;
                //    StringFormat format12 = new StringFormat(StringFormatFlags.NoClip);
                //    format12.Alignment = System.Drawing.StringAlignment.Center;
                //    StringFormat format22 = new StringFormat(format12);
                //    format22.Alignment = System.Drawing.StringAlignment.Center;

                //    Rectangle displayRectangle2 = new Rectangle(new Point(Convert.ToInt32(dr["LARGURA"]), Convert.ToInt32(dr["ALTURA"])), new Size(950, 100));

                //    dr = dt.Rows[1];

                //    System.Drawing.Font myFont2 = new System.Drawing.Font(dr["FONT"].ToString(), Convert.ToInt32(dr["TAMANHOFONT"]));

                //    // Construct a new Rectangle.
                //    displayRectangle2 = new Rectangle(new Point(Convert.ToInt32(dr["LARGURA"]), Convert.ToInt32(dr["ALTURA"]) + itAumento), new Size(950, 100));

                //    format12 = new StringFormat(StringFormatFlags.NoClip);
                //    format12.Alignment = System.Drawing.StringAlignment.Center;
                    
                //    format22 = new StringFormat(format12);
                //    format22.Alignment = System.Drawing.StringAlignment.Center;

                //    ev.Graphics.DrawString(stTexto, myFont2, Brushes.Black, (RectangleF)displayRectangle2, format12);
                //    itLinha++;

                //    itAumento = itAumento + 0;
                ////}

                //itAumento = itAumento + 30;
                //StringFormat format13 = new StringFormat(StringFormatFlags.NoClip);
                //format13.Alignment = System.Drawing.StringAlignment.Center;

                //StringFormat format23 = new StringFormat(format13);
                //format23.Alignment = System.Drawing.StringAlignment.Center;

                //Rectangle displayRectangle3 = new Rectangle(new Point(Convert.ToInt32(dr["LARGURA"]), Convert.ToInt32(dr["ALTURA"])), new Size(950, 100));

                //displayRectangle3 = new Rectangle(new Point(Convert.ToInt32(dr["LARGURA"]), Convert.ToInt32(dr["ALTURA"]) + itAumento), new Size(950, 100));

                //// Construct 2 new StringFormat objects
                //format13 = new StringFormat(StringFormatFlags.NoClip);
                //format13.Alignment = System.Drawing.StringAlignment.Center;

                //format23 = new StringFormat(format13);
                //format23.Alignment = System.Drawing.StringAlignment.Center;

                //ev.Graphics.DrawString(stRodape, myFont3, Brushes.Black, (RectangleF)displayRectangle3, format13);

            //}
            //else
            //{
            //    //ev.Graphics.DrawString(AP_Nome, myFont1, Brushes.Black, Convert.ToInt32(dr["LARGURA"]), Convert.ToInt32(dr["ALTURA"]));
            //}

            ev.HasMorePages = false;
        }

        private static void MI_ObterDescricaoCertificado(CTO_Visitante ITO_Visitante)
        {
            //stTexto = string.Empty;
            //stRodape = string.Empty;

            //AP_Congresso = false;
            //AP_Curso2 = false;
            //AP_Curso3 = false;

            //foreach (DataRow Row in MI_ObterProdutosComprados(ITO_Visitante.getObjRef()).Rows)
            //{
            //    if (Row.ItemArray[0].ToString().ToUpper() == "1")
            //        AP_Congresso = true;

            //    if (Row.ItemArray[0].ToString().ToUpper() == "2")
            //        AP_Curso2 = true;

            //    if (Row.ItemArray[0].ToString().ToUpper() == "3")
            //        AP_Curso3 = true;
            //}

            //// CASO 1
            //if ((AP_Congresso == true) && (AP_Curso2 == false) && (AP_Curso3 == false))
            //{
            //    stTexto = "participou do XI Seminário Internacional de Aves e Suínos,";
            //    stRodape = "realizado no período de 02 a 04 de abril de 2012 na qualidade de congressista.";
            //}

            //// CASO 2
            //if ((AP_Curso2 == true) && (AP_Congresso == false) && (AP_Curso3 == false))
            //{
            //    stTexto = "participou do I Seminário Internacional de Biomassa e Bioenergia,";
            //    stRodape = "realizado no período de 02 a 04 de abril de 2012 na qualidade de congressista.";
            //}

            ////CASO 3
            //if (AP_Curso3 == true)
            //{
            //    if (AP_ProdutoImpressao == 1)
            //    {
            //        stTexto = "participou do XI Seminário Internacional de Aves e Suínos,";
            //        stRodape = "realizado no período de 02 a 04 de abril de 2012 na qualidade de congressista.";
            //    }
            //    else if (AP_ProdutoImpressao == 2)
            //    {
            //        stTexto = "participou do I Seminário Internacional de Biomassa e Bioenergia,";
            //        stRodape = "realizado no período de 02 a 04 de abril de 2012 na qualidade de congressista.";
            //    }
            //}
            //// CASO 3
            //if ((AP_Curso3 == true) && (AP_Congresso == false) && (AP_Curso2 == false))
            //{
            //    stTexto = "participou do Curso Prático de Suínos,";
            //    stRodape = "realizado no dia 18 de maio de 2012 na qualidade de congressista.";
            //}


            //// CASO 4
            //if ((AP_Congresso == true) && (AP_Curso2 == true) && (AP_Curso3 == false))
            //{
            //    stTexto = "participou do XI Seminário Internacional de Aves e Suínos, Curso Prático de Aves,";
            //    stRodape = "realizados no período de 17, 18 e 19 de maio de 2012 na qualidade de congressista.";
            //}

            //// CASO 5
            //if ((AP_Congresso == true) && (AP_Curso2 == false) && (AP_Curso3 == true))
            //{
            //    stTexto = "participou do XI Seminário Internacional de Aves e Suínos, Curso Prático de Suínos,";
            //    stRodape = "realizados no período de 17, 18 e 19 de maio de 2012 na qualidade de congressista.";
            //}

            //// CASO 6
            //if ((AP_Congresso == false) && (AP_Curso2 == true) && (AP_Curso3 == true))
            //{
            //    stTexto = "participou do Curso Prático de Aves e Curso Prático de Suínos,";
            //    stRodape = "realizados no período de 17 e 18 de maio de 2012 na qualidade de congressista.";
            //}

            //// CASO 7
            //if ((AP_Congresso == true) && (AP_Curso2 == true) && (AP_Curso3 == true))
            //{
            //    stTexto = "participou do XI Seminário Internacional de Aves e Suínos, Curso Prático de Aves e Curso Prático de Suínos,";
            //    stRodape = "realizados no período de 17, 18 e 19 de maio de 2012 na qualidade de congressista.";
            //}
        }

        private static DataTable MI_ObterProdutosComprados(int prCdCliente)
        {
            CAT_CadastrarItensPedido IAT_Pedido = new CAT_CadastrarItensPedido();
            return IAT_Pedido.MS_ObterProdutosCompradosPorCliente(prCdCliente);
        }
        
        private static int MI_ObterCursos(CTO_Visitante ITO_Visitante)
        {
            return 1;
        }

        private static string MI_ObterDescricao(string prCurso)
        {
            string stRetorno = "";

            //CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            //DataTable dtRegistros = IAT_Visitante.MS_ObterDescricaoCurso(prCurso);

            //if (dtRegistros.Rows.Count > 0)
            //{
            //    DataRow drLinha = dtRegistros.Rows[0];
            //    stRetorno = drLinha.ItemArray[0].ToString();
            //}

            return stRetorno;
        }

    }
}

