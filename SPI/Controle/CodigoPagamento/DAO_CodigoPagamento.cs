using System;

namespace SPI
{
    public class DAO_CodigoPagamento : FWK_DAO_CodigoPagamento
    {
        public DAO_CodigoPagamento()
        {
        }

        public CTO_CodigoPagamento MS_Obter(string prstID)
        {
            CTO_CodigoPagamento ITO_CodigoPagamento = new CTO_CodigoPagamento();
            MI_SetSql (AP_SelectALL + 
                       "FROM tblCodigoPagamento " + 
                       "WHERE itCodigo = @ID ");
            MI_AddParameters("@ID", prstID);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_CodigoPagamento = null;
            else
                ITO_CodigoPagamento = MI_DataSetToEntidade();
            return ITO_CodigoPagamento;
        }

        public string MS_GerarCodigoPagamento()
        {
            Int64 itCodigoPagamento = 0;
            MI_SetSql("Select MAX(itCodigo)Valor From tblCodigoPagamento");
            MI_ExecuteQuery();
            try
            {
                itCodigoPagamento = Convert.ToInt64(AP_DataReader.GetString(0));
                itCodigoPagamento++;
            }
            catch
            {
                itCodigoPagamento = 1000;
            }
            MI_FecharConexao();
            return itCodigoPagamento.ToString();
        }

        public CTO_CodigoPagamento MS_ObterPeloCodigoPedido(string stPedido)
        {
            CTO_CodigoPagamento ITO_CodigoPagamento = new CTO_CodigoPagamento();
            MI_SetSql(AP_SelectALL +
                       "FROM tblCodigoPagamento " +
                       "WHERE CodigoBarras = @ID ");
            MI_AddParameters("@ID", stPedido);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_CodigoPagamento = null;
            else
                ITO_CodigoPagamento = MI_DataSetToEntidade();
            MI_FecharConexao();
            return ITO_CodigoPagamento;
        }

        public CTO_CodigoPagamento MS_ObterPeloCodigoImpressao(string stCodigo)
        {
            CTO_CodigoPagamento ITO_CodigoPagamento = new CTO_CodigoPagamento();
            MI_SetSql(AP_SelectALL +
                       "FROM tblCodigoPagamento " +
                       "WHERE itCodigo = @ID ");
            MI_AddParameters("@ID", stCodigo);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_CodigoPagamento = null;
            else
                ITO_CodigoPagamento = MI_DataSetToEntidade();
            MI_FecharConexao();
            return ITO_CodigoPagamento;
        }

    }
}

