using System;
using SPI;

public interface ICadastrarCodigoPagamento
{
     void MS_Incluir(CTO_CodigoPagamento ITO_CodigoPagamento);
     void MS_Alterar(CTO_CodigoPagamento ITO_CodigoPagamento);
     void MS_Excluir(CTO_CodigoPagamento ITO_CodigoPagamento);
     CTO_CodigoPagamento MS_Obter(int pritObjRef);
     CTO_CodigoPagamento MS_Obter(string prstID);
}

