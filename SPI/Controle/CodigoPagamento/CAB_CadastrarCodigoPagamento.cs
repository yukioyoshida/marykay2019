using System;
using SPI;

public abstract class CAB_CadastrarCodigoPagamento : ICadastrarCodigoPagamento
{
     public abstract void MS_Incluir(CTO_CodigoPagamento ITO_CodigoPagamento);
     public abstract void MS_Alterar(CTO_CodigoPagamento ITO_CodigoPagamento);
     public abstract void MS_Excluir(CTO_CodigoPagamento ITO_CodigoPagamento);
     public abstract CTO_CodigoPagamento MS_Obter(int pritObjRef);
     public abstract CTO_CodigoPagamento MS_Obter(string prstID);
}

