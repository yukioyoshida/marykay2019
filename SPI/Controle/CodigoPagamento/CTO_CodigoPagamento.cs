using System;
namespace SPI 
{

    public class CTO_CodigoPagamento 
    {
          //  Declaracao dos atributos Puros
        string AP_itCodigo;
        string AP_CodigoBarras;

        public CTO_CodigoPagamento () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public string getitCodigo()
        {
            return AP_itCodigo;
        }
        public string getCodigoBarras()
        {
            return AP_CodigoBarras;
        }
         //  Declaracao dos Metodos set
        public void setitCodigo( string pritCodigo)
        {
            AP_itCodigo = pritCodigo;
        }
        public void setCodigoBarras( string prCodigoBarras)
        {
            AP_CodigoBarras = prCodigoBarras;
        }
    }
}

