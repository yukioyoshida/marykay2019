using System;
using SPI;

public class CAT_CadastrarCodigoPagamento : CAB_CadastrarCodigoPagamento
{
     public CAT_CadastrarCodigoPagamento()
     {
     }
     public override void MS_Incluir(CTO_CodigoPagamento ITO_CodigoPagamento)
     {
          DAO_CodigoPagamento IDAO_CodigoPagamento = new DAO_CodigoPagamento();
          IDAO_CodigoPagamento.MS_Incluir(ITO_CodigoPagamento);
     }
     public override void MS_Alterar(CTO_CodigoPagamento ITO_CodigoPagamento)
     {
         DAO_CodigoPagamento IDAO_CodigoPagamento = new DAO_CodigoPagamento();
         IDAO_CodigoPagamento.MS_Alterar(ITO_CodigoPagamento);
     }
     public override void MS_Excluir(CTO_CodigoPagamento ITO_CodigoPagamento)
     {
         DAO_CodigoPagamento IDAO_CodigoPagamento = new DAO_CodigoPagamento();
         IDAO_CodigoPagamento.MS_Excluir(ITO_CodigoPagamento);
     }
     public override CTO_CodigoPagamento MS_Obter(int pritObjRef)
     {
           DAO_CodigoPagamento IDAO_CodigoPagamento = new DAO_CodigoPagamento();
           return IDAO_CodigoPagamento.MS_Obter(pritObjRef);
     }
     public override CTO_CodigoPagamento MS_Obter(string prstID)
     {
          DAO_CodigoPagamento IDAO_CodigoPagamento = new DAO_CodigoPagamento();
          return IDAO_CodigoPagamento.MS_Obter(prstID);
     }
     public string MS_GerarCodigoPagamento()
     {
         DAO_CodigoPagamento IDAO_CodigoPagamento = new DAO_CodigoPagamento();
         return IDAO_CodigoPagamento.MS_GerarCodigoPagamento();
     }
     public CTO_CodigoPagamento MS_ObterPeloCodigoPedido(string stPedido)
     {
         DAO_CodigoPagamento IDAO_CodigoPagamento = new DAO_CodigoPagamento();
         return IDAO_CodigoPagamento.MS_ObterPeloCodigoPedido(stPedido);
     }

     public CTO_CodigoPagamento MS_ObterPeloCodigoImpressao(string stCodigo)
     {
         DAO_CodigoPagamento IDAO_CodigoPagamento = new DAO_CodigoPagamento();
         return IDAO_CodigoPagamento.MS_ObterPeloCodigoImpressao(stCodigo);
     }

}

