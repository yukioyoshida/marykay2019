using System.Data;
namespace SPI
{
    public class FWK_DAO_CodigoPagamento : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                           "          itCodigo, " +
                           "          CodigoBarras ";

        public FWK_DAO_CodigoPagamento()
        {
        }

        public void MS_Incluir(CTO_CodigoPagamento ITO_CodigoPagamento)
        {
            //int itObjRef = MS_NovoObjRef();
            MI_SetSql ("INSERT INTO tblcodigopagamento (" +
                       "          itCodigo, " +
                       "          CodigoBarras) " + 
                       " VALUES (" + 
                       "          @itCodigo, " +
                       "          @CodigoBarras) ");

            MI_AddParameters("@itCodigo", ITO_CodigoPagamento.getitCodigo());
            MI_AddParameters("@CodigoBarras", ITO_CodigoPagamento.getCodigoBarras());
            
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_Alterar(CTO_CodigoPagamento ITO_CodigoPagamento)
        {
            MI_SetSql ("UPDATE tblcodigopagamento SET " +
                       "          itCodigo = @itCodigo, " +
                       "          CodigoBarras = @CodigoBarras " +
                       "WHERE itCodigo = @ObjRef ");
            MI_AddParameters("@itCodigo", ITO_CodigoPagamento.getitCodigo());
            MI_AddParameters("@CodigoBarras", ITO_CodigoPagamento.getCodigoBarras());

            MI_ExecuteNonQuery();
        }

        public void MS_Excluir(CTO_CodigoPagamento ITO_CodigoPagamento)
        {
            MI_SetSql ("DELETE FROM tblcodigopagamento WHERE itCodigo = @ObjRef");
            MI_AddParameters("@ObjRef", ITO_CodigoPagamento.getitCodigo());
            
            MI_ExecuteNonQuery();
        }

        public CTO_CodigoPagamento MS_Obter(int pritObjRef)
        {
            CTO_CodigoPagamento ITO_CodigoPagamento = new CTO_CodigoPagamento();
            MI_SetSql ("SELECT " + 
                       "          itCodigo, " +
                       "          CodigoBarras " + 
                       "FROM tblcodigopagamento " +
                       "WHERE itCodigo = @ObjRef ");
            MI_AddParameters("@Objref", pritObjRef);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_CodigoPagamento = null;
            else
                ITO_CodigoPagamento = MI_DataSetToEntidade();
            return ITO_CodigoPagamento;
        }

        public CTO_CodigoPagamento MI_DataSetToEntidade()
        {
            CTO_CodigoPagamento ITO_CodigoPagamento = new CTO_CodigoPagamento();            
            try
            {
                ITO_CodigoPagamento.setitCodigo(AP_DataReader.GetString(0));
            }
            catch
            {
                ITO_CodigoPagamento.setitCodigo("");
            }
            try
            {
                ITO_CodigoPagamento.setCodigoBarras(AP_DataReader.GetString(1));
            }
            catch
            {
                ITO_CodigoPagamento.setCodigoBarras("");
            }
            return ITO_CodigoPagamento;
        }

        public CTO_CodigoPagamento MS_ObterTodos()
        {
            CTO_CodigoPagamento ITO_CodigoPagamento = new CTO_CodigoPagamento();
            MI_SetSql ("SELECT " + 
                       "          itCodigo, " +
                       "          CodigoBarras " + 
                       "FROM tblcodigopagamento " );
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_CodigoPagamento = null;
            else
                ITO_CodigoPagamento = MI_DataSetToEntidade();
            return ITO_CodigoPagamento;
        }

        public CTO_CodigoPagamento MS_ObterProximo()
        {
            CTO_CodigoPagamento ITO_CodigoPagamento = new CTO_CodigoPagamento();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_CodigoPagamento = null;
            else
                ITO_CodigoPagamento = MI_DataSetToEntidade();
                return ITO_CodigoPagamento;
        }

        public int MS_NovoObjRef()
        {
            int itObjRef;
            MI_SetSql("Select Max(itCodigo) from tblcodigopagamento");
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 1;
            }
            else
            {
                itObjRef = AP_DataReader.GetInt32(0);
                itObjRef++;
            }
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblcodigopagamento");
            return MI_ExecuteDataSet();
        }

    }
}

