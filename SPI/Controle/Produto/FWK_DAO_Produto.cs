using System.Data;
using System;
namespace SPI
{
    public class FWK_DAO_Produto : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                           "          cdProduto, " +
                           "          dsProduto, " +
                           "          dtValor, " +
                           "          dsValor1, " +
                           "          dsValor2, " +
                           "          qtdVagas, " +
                           "          qtdDisponivel, " +
                           "          dtPalestra, " +
                           "          Sigla ";

        public FWK_DAO_Produto()
        {
        }

        public void MS_Incluir(CTO_Produto ITO_Produto)
        {
            int itObjRef = MS_NovoObjRef();
            MI_SetSql ("INSERT INTO tblProduto (" +
                       "          cdProduto, " +
                       "          dsProduto, " +
                       "          Sigla, " +
                       "          Imprimir) " + 
                       " VALUES (" + 
                       "          @cdProduto, " +
                       "          @dsProduto, " +
                       "          @Sigla, " +
                       "          @Imprimir) ");

            MI_AddParameters("@cdProduto", itObjRef.ToString());
            MI_AddParameters("@dsProduto", ITO_Produto.getdsProduto());
            MI_AddParameters("@Sigla", ITO_Produto.getdsProduto());
            MI_AddParameters("@Imprimir", "Aparece na credencial");

            MI_ExecuteNonQuery();
        }

        public void MS_Alterar(CTO_Produto ITO_Produto)
        {
            MI_SetSql ("UPDATE tblProduto SET " +
                       "          cdProduto = @cdProduto, " +
                       "          dsProduto = @dsProduto, " +
                       "          dtValor = @dtValor, " +
                       "          dsValor1 = @dsValor1, " +
                       "          dsValor2 = @dsValor2, " +
                       "          qtdVagas = @qtdVagas, " +
                       "          qtdDisponivel = @qtdDisponivel " +
                       "WHERE cdProduto = @cdProduto ");
            MI_AddParameters("@cdProduto", ITO_Produto.getcdProduto());
            MI_AddParameters("@dsProduto", ITO_Produto.getdsProduto());
            MI_AddParameters("@dtValor", ITO_Produto.getdtValor());
            MI_AddParameters("@dsValor1", ITO_Produto.getdsValor1());
            MI_AddParameters("@dsValor2", ITO_Produto.getdsValor2());
            MI_AddParameters("@qtdVagas", ITO_Produto.getqtdVagas());
            MI_AddParameters("@qtdDisponivel", ITO_Produto.getqtdDisponivel());

            MI_ExecuteNonQuery();
        }

        public void MS_Excluir(CTO_Produto ITO_Produto)
        {
            MI_SetSql("DELETE FROM tblProduto WHERE cdProduto = @ObjRef");
            MI_AddParameters("@ObjRef", ITO_Produto.getcdProduto());
            
            MI_ExecuteNonQuery();
        }

        public CTO_Produto MS_Obter(int pritObjRef)
        {
            CTO_Produto ITO_Produto = new CTO_Produto();
            MI_SetSql ("SELECT " + 
                       "          cdProduto, " +
                       "          dsProduto, " +
                       "          dtValor, " +
                       "          dsValor1, " +
                       "          dsValor2, " +
                       "          qtdVagas, " +
                       "          qtdDisponivel " + 
                       "FROM tblProduto " +
                       "WHERE cdProduto = @ObjRef ");
            MI_AddParameters("@Objref", pritObjRef);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Produto = null;
            else
                ITO_Produto = MI_DataSetToEntidade();
            return ITO_Produto;
        }

        public CTO_Produto MI_DataSetToEntidade()
        {
            CTO_Produto ITO_Produto = new CTO_Produto();            
            try
            {
                ITO_Produto.setcdProduto(AP_DataReader.GetString(0));
            }
            catch
            {
                ITO_Produto.setcdProduto("");
            }
            try
            {
                ITO_Produto.setdsProduto(AP_DataReader.GetString(1));
            }
            catch
            {
                ITO_Produto.setdsProduto("");
            }
            try
            {
            ITO_Produto.setdtValor(AP_DataReader.GetDateTime(2));
            }
            catch
            {
            }
            try
            {
                ITO_Produto.setdsValor1(AP_DataReader.GetString(3));
            }
            catch
            {
                ITO_Produto.setdsValor1("");
            }
            try
            {
                ITO_Produto.setdsValor2(AP_DataReader.GetString(4));
            }
            catch
            {
                ITO_Produto.setdsValor2("");
            }
            try
            {
                ITO_Produto.setqtdVagas(AP_DataReader.GetInt32(5));
            }
            catch
            {
                ITO_Produto.setqtdVagas(0);
            }
            try
            {
                ITO_Produto.setqtdDisponivel(AP_DataReader.GetInt32(6));
            }
            catch
            {
                ITO_Produto.setqtdDisponivel(0);
            }
            return ITO_Produto;
        }

        public CTO_Produto MS_ObterTodos()
        {
            CTO_Produto ITO_Produto = new CTO_Produto();
            MI_SetSql ("SELECT " + 
                       "          cdProduto, " +
                       "          dsProduto, " +
                       "          dtValor, " +
                       "          dsValor1, " +
                       "          dsValor2, " +
                       "          qtdVagas, " +
                       "          qtdDisponivel " + 
                       "FROM tblProduto " );
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Produto = null;
            else
                ITO_Produto = MI_DataSetToEntidade();
            return ITO_Produto;
        }

        public CTO_Produto MS_ObterProximo()
        {
            CTO_Produto ITO_Produto = new CTO_Produto();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_Produto = null;
            else
                ITO_Produto = MI_DataSetToEntidade();
                return ITO_Produto;
        }

        public int MS_NovoObjRef()
        {
            Int32 itObjRef;
            MI_SetSql("Select Max(cdProduto) from tblProduto");
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 1001;
            }
            else
            {
                var teste = AP_DataReader.GetString(0);
                itObjRef = Convert.ToInt32(teste);
                itObjRef++;
            }

            MI_FecharConexao();
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblProduto");
            return MI_ExecuteDataSet();
        }

    }
}

