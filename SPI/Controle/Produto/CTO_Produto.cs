using System;
namespace SPI 
{

    public class CTO_Produto 
    {
          //  Declaracao dos atributos Puros
        string AP_cdProduto;
        string AP_dsProduto;
        DateTime AP_dtValor;
        string AP_dsValor1;
        string AP_dsValor2;
        int AP_qtdVagas;
        int AP_qtdDisponivel;

        public CTO_Produto () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public string getcdProduto()
        {
            return AP_cdProduto;
        }
        public string getdsProduto()
        {
            return AP_dsProduto;
        }
        public DateTime getdtValor()
        {
            return AP_dtValor;
        }
        public string getdsValor1()
        {
            return AP_dsValor1;
        }
        public string getdsValor2()
        {
            return AP_dsValor2;
        }
        public int getqtdVagas()
        {
            return AP_qtdVagas;
        }
        public int getqtdDisponivel()
        {
            return AP_qtdDisponivel;
        }
         //  Declaracao dos Metodos set
        public void setcdProduto( string prcdProduto)
        {
            AP_cdProduto = prcdProduto;
        }
        public void setdsProduto( string prdsProduto)
        {
            AP_dsProduto = prdsProduto;
        }
        public void setdtValor( DateTime prdtValor)
        {
            AP_dtValor = prdtValor;
        }
        public void setdsValor1( string prdsValor1)
        {
            AP_dsValor1 = prdsValor1;
        }
        public void setdsValor2( string prdsValor2)
        {
            AP_dsValor2 = prdsValor2;
        }
        public void setqtdVagas( int prqtdVagas)
        {
            AP_qtdVagas = prqtdVagas;
        }
        public void setqtdDisponivel( int prqtdDisponivel)
        {
            AP_qtdDisponivel = prqtdDisponivel;
        }
    }
}

