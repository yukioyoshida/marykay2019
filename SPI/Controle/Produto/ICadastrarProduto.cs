using System;
using System.Data;
using SPI;

public interface ICadastrarProduto
{
     void MS_Incluir(CTO_Produto ITO_Produto);
     void MS_Alterar(CTO_Produto ITO_Produto);
     void MS_Excluir(CTO_Produto ITO_Produto);
     CTO_Produto MS_Obter(int pritObjRef);
     CTO_Produto MS_Obter(string prstID);
     DataTable MS_ObterTodos();
     DataTable MS_ObterProdutosQtdInscritos();
}

