using System;
using System.Data;
using SPI;

public class CAT_CadastrarProduto : CAB_CadastrarProduto
{
     public CAT_CadastrarProduto()
     {
     }
     public override void MS_Incluir(CTO_Produto ITO_Produto)
     {
          DAO_Produto IDAO_Produto = new DAO_Produto();
          IDAO_Produto.MS_Incluir(ITO_Produto);
     }
     public override void MS_Alterar(CTO_Produto ITO_Produto)
     {
         DAO_Produto IDAO_Produto = new DAO_Produto();
         IDAO_Produto.MS_Alterar(ITO_Produto);
     }
     public override void MS_Excluir(CTO_Produto ITO_Produto)
     {
         DAO_Produto IDAO_Produto = new DAO_Produto();
         IDAO_Produto.MS_Excluir(ITO_Produto);
     }
     public override CTO_Produto MS_Obter(int pritObjRef)
     {
           DAO_Produto IDAO_Produto = new DAO_Produto();
           return IDAO_Produto.MS_Obter(pritObjRef);
     }
     public override CTO_Produto MS_Obter(string prstID)
     {
          DAO_Produto IDAO_Produto = new DAO_Produto();
          return IDAO_Produto.MS_Obter(prstID);
     }
     public override DataTable MS_ObterTodos()
     {
         DAO_Produto IDAO_Produto = new DAO_Produto();
         return IDAO_Produto.MS_ObterTodos();
     }
     public override DataTable MS_ObterProdutosQtdInscritos()
     {
         DAO_Produto IDAO_Produto = new DAO_Produto();
         return IDAO_Produto.MS_ObterProdutosQtdInscritos();
     }

     public CTO_Produto MS_ObterPorSigla(string prstID)
     {
         DAO_Produto IDAO_Produto = new DAO_Produto();
         return IDAO_Produto.MS_ObterPorSigla(prstID);
     }

    public CTO_Produto MS_ObterProdutoPorDescricao(string prstID)
    {
        DAO_Produto IDAO_Produto = new DAO_Produto();
        return IDAO_Produto.MS_ObterProdutoPorDescricao(prstID);
    }

     public DataTable MS_ObterQuantidadeDisponivel(string prstID)
     {
         DAO_Produto IDAO_Produto = new DAO_Produto();
         return IDAO_Produto.MS_ObterQuantidadeDisponivel(prstID);
     }

     public DataTable MS_ObterDisponibilidadeProduto(int itProduto)
     {
         DAO_Produto IDAO_Produto = new DAO_Produto();
         return IDAO_Produto.MS_ObterDisponibilidadeProduto(itProduto);
     }

     public DataTable MS_ObterProdutoPorCodigo(string cdProduto)
     {
         DAO_Produto IDAO_Produto = new DAO_Produto();
         return IDAO_Produto.MS_ObterProdutoPorCodigo(cdProduto);
     }

    public DataTable MS_ObterProdutoPorNome(string prNomeProduto)
    {
        DAO_Produto IDAO_Produto = new DAO_Produto();
        return IDAO_Produto.MS_ObterProdutoPorNome(prNomeProduto);
    }
    

}

