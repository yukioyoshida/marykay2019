using System;
using System.Data;
using SPI;

public abstract class CAB_CadastrarProduto : ICadastrarProduto
{
     public abstract void MS_Incluir(CTO_Produto ITO_Produto);
     public abstract void MS_Alterar(CTO_Produto ITO_Produto);
     public abstract void MS_Excluir(CTO_Produto ITO_Produto);
     public abstract CTO_Produto MS_Obter(int pritObjRef);
     public abstract CTO_Produto MS_Obter(string prstID);
     public abstract DataTable MS_ObterTodos();
     public abstract DataTable MS_ObterProdutosQtdInscritos();
}

