using System.Data;
using SPI;
namespace SPI
{
    public class DAO_Produto : FWK_DAO_Produto
    {
        public DAO_Produto()
        {
        }

        public CTO_Produto MS_Obter(string prstID)
        {
            CTO_Produto ITO_Produto = new CTO_Produto();
            MI_SetSql (AP_SelectALL + 
                       "FROM tblProduto " + 
                       "WHERE ID = @ID ");
            MI_AddParameters("@ID", prstID);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Produto = null;
            else
                ITO_Produto = MI_DataSetToEntidade();
            return ITO_Produto;
        }
        public DataTable MS_ObterTodos()
        {
            MI_SetSql(AP_SelectALL + " FROM tblProduto");

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterProdutosQtdInscritos()
        {
            MI_SetSql("SELECT " +
                      "CDPRODUTO, " +
                      "DSPRODUTO, " +
                      "QtdVagas, " +
                      "(SELECT COUNT(*) FROM TBLITENSPEDIDO AS A INNER JOIN TBLPEDIDO AS B ON B.CDPEDIDO = A.CDPEDIDO AND B.DSSITUACAO <> 'CANCELADO' WHERE A.CDPRODUTO = TBLPRODUTO.CDPRODUTO) AS QTDINSCRITO, " +
                      "QtdDisponivel " +
                      "FROM TBLPRODUTO ");

            return MI_ExecuteDataSet();
        }

        public CTO_Produto MS_ObterPorSigla(string prstID)
        {
            CTO_Produto ITO_Produto = new CTO_Produto();
            MI_SetSql(AP_SelectALL +
                       "FROM tblProduto " +
                       "WHERE Sigla = @Sigla ");
            MI_AddParameters("@Sigla", prstID);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Produto = null;
            else
                ITO_Produto = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Produto;
        }

        public CTO_Produto MS_ObterProdutoPorDescricao(string prstID)
        {
            CTO_Produto ITO_Produto = new CTO_Produto();
            MI_SetSql(AP_SelectALL +
                       "FROM tblProduto " +
                       "WHERE dsProduto = @dsProduto ");
            MI_AddParameters("@dsProduto", prstID);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Produto = null;
            else
                ITO_Produto = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Produto;
        }

        public DataTable MS_ObterQuantidadeDisponivel(string prstID)
        {
            CTO_Produto ITO_Produto = new CTO_Produto();
            MI_SetSql("SELECT                                      " +
	                  "(SELECT qtdVagas FROM tblProduto WHERE tblProduto.cdProduto = tblitenspedido.cdProduto) - count(*) AS DISPONIVEL " +
                      "FROM tblitenspedido INNER JOIN tblPedido ON " +
	                  "tblitenspedido.cdProduto = @Produto AND	" +
	                  "tblPedido.cdPedido = tblitenspedido.cdPedido AND " +
	                  "tblPedido.dsSituacao <> 'CANCELADO' " +
                      "GROUP BY cdProduto                  ");
            MI_AddParameters("@Produto", prstID);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterDisponibilidadeProduto(int itProduto)
        {
            MI_SetSql("SELECT QTDVAGAS FROM TBLPRODUTO WHERE CDPRODUTO = @CodigoProduto");
            MI_AddParameters("@CodigoProduto", itProduto);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterProdutoPorCodigo(string cdProduto)
        {
            MI_SetSql("SELECT * FROM TBLPRODUTO WHERE CDPRODUTO = @CodigoProduto");
            MI_AddParameters("@CodigoProduto", cdProduto);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterProdutoPorNome(string prNomeProduto)
        {
            MI_SetSql("SELECT * FROM TBLPRODUTO WHERE dsProduto = @dsProduto");
            MI_AddParameters("@dsProduto", prNomeProduto);
            return MI_ExecuteDataSet();
        }

    }
}

