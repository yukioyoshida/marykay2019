using System.Data;
namespace SPI
{
    public class FWK_DAO_EmissaoCracha : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                           "          ObjRef, " +
                           "          CodigoBarras, " +
                           "          Data, " +
                           "          Hora, " +
                           "          UsuarioImpressao, " +
                           "          OrigemImpressao ";

        public FWK_DAO_EmissaoCracha()
        {
        }

        public void MS_Incluir(CTO_EmissaoCracha ITO_EmissaoCracha, string prVia)
        {
            int itObjRef = MS_NovoObjRef();
            MI_FecharConexao();

            MI_SetSql ("INSERT INTO TblEmissaoCracha (" +
                       "          ObjRef, " +
                       "          CodigoBarras, " +
                       "          Data, " +
                       "          Hora, " +
                       "          UsuarioImpressao, " +
                       "          OrigemImpressao, " +
                       "          dsMotivo) " +
                       " VALUES (" + 
                       "          @ObjRef, " +
                       "          @CodigoBarras, " +
                       "          CAST(GETDATE() AS DATE), " +
                       "          CAST(GETDATE() AS TIME), " +
                       "          @UsuarioImpressao, " +
                       "          @OrigemImpressao, " +
                       "          @dsMotivo) ");

            MI_AddParameters("@ObjRef", itObjRef);
            MI_AddParameters("@CodigoBarras", ITO_EmissaoCracha.getCodigoBarras());
            MI_AddParameters("@UsuarioImpressao", ITO_EmissaoCracha.getUsuarioImpressao());
            MI_AddParameters("@OrigemImpressao", ITO_EmissaoCracha.getOrigemImpressao());
            MI_AddParameters("@dsMotivo", prVia);

            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_Alterar(CTO_EmissaoCracha ITO_EmissaoCracha)
        {
            MI_SetSql ("UPDATE TblEmissaoCracha SET " +
                       "          ObjRef = @ObjRef, " +
                       "          CodigoBarras = @CodigoBarras, " +
                       "          Data = curdate(), " +
                       "          Hora = curtime(), " +
                       "          UsuarioImpressao = @UsuarioImpressao, " +
                       "          OrigemImpressao = @OrigemImpressao " + 
                       "WHERE ObjRef = @ObjRef ");
            MI_AddParameters("@ObjRef", ITO_EmissaoCracha.getObjRef());
            MI_AddParameters("@CodigoBarras", ITO_EmissaoCracha.getCodigoBarras());
            MI_AddParameters("@Data", ITO_EmissaoCracha.getData());
            MI_AddParameters("@Hora", ITO_EmissaoCracha.getHora());
            MI_AddParameters("@UsuarioImpressao", ITO_EmissaoCracha.getUsuarioImpressao());
            MI_AddParameters("@OrigemImpressao", ITO_EmissaoCracha.getOrigemImpressao());

            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_Excluir(CTO_EmissaoCracha ITO_EmissaoCracha)
        {
            MI_SetSql ("DELETE FROM TblEmissaoCracha WHERE ObjRef = @ObjRef");
            MI_AddParameters("@ObjRef", ITO_EmissaoCracha.getObjRef());
            
            MI_ExecuteNonQuery();
        }

        public CTO_EmissaoCracha MS_Obter(int pritObjRef)
        {
            CTO_EmissaoCracha ITO_EmissaoCracha = new CTO_EmissaoCracha();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          CodigoBarras, " +
                       "          Data, " +
                       "          Hora, " +
                       "          UsuarioImpressao, " +
                       "          OrigemImpressao " + 
                       "FROM TblEmissaoCracha " + 
                       "WHERE Objref = @ObjRef ");
            MI_AddParameters("@Objref", pritObjRef);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_EmissaoCracha = null;
            else
                ITO_EmissaoCracha = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_EmissaoCracha;
        }

        public CTO_EmissaoCracha MI_DataSetToEntidade()
        {
            CTO_EmissaoCracha ITO_EmissaoCracha = new CTO_EmissaoCracha();            
            try
            {
                ITO_EmissaoCracha.setObjRef(AP_DataReader.GetInt32(0));
            }
            catch
            {
                ITO_EmissaoCracha.setObjRef(0);
            }
            try
            {
                ITO_EmissaoCracha.setCodigoBarras(AP_DataReader.GetString(1));
            }
            catch
            {
                ITO_EmissaoCracha.setCodigoBarras("");
            }
            try
            {
            ITO_EmissaoCracha.setData(AP_DataReader.GetDateTime(2));
            }
            catch
            {
            }
            try
            {
                ITO_EmissaoCracha.setHora(AP_DataReader.GetString(3));
            }
            catch
            {
                ITO_EmissaoCracha.setHora("");
            }
            try
            {
                ITO_EmissaoCracha.setUsuarioImpressao(AP_DataReader.GetString(4));
            }
            catch
            {
                ITO_EmissaoCracha.setUsuarioImpressao("");
            }
            try
            {
                ITO_EmissaoCracha.setOrigemImpressao(AP_DataReader.GetString(5));
            }
            catch
            {
                ITO_EmissaoCracha.setOrigemImpressao("");
            }
            return ITO_EmissaoCracha;
        }

        public CTO_EmissaoCracha MS_ObterTodos()
        {
            CTO_EmissaoCracha ITO_EmissaoCracha = new CTO_EmissaoCracha();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          CodigoBarras, " +
                       "          Data, " +
                       "          Hora, " +
                       "          UsuarioImpressao, " +
                       "          OrigemImpressao " + 
                       "FROM TblEmissaoCracha " );
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_EmissaoCracha = null;
            else
                ITO_EmissaoCracha = MI_DataSetToEntidade();
            return ITO_EmissaoCracha;
        }

        public CTO_EmissaoCracha MS_ObterProximo()
        {
            CTO_EmissaoCracha ITO_EmissaoCracha = new CTO_EmissaoCracha();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_EmissaoCracha = null;
            else
                ITO_EmissaoCracha = MI_DataSetToEntidade();
                return ITO_EmissaoCracha;
        }

        public int MS_NovoObjRef()
        {
            int itObjRef;
            MI_SetSql("Select Max(ObjRef) from TblEmissaoCracha");
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 1;
            }
            else
            {
                itObjRef = AP_DataReader.GetInt32(0);
                itObjRef++;
            }
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from TblEmissaoCracha");
            return MI_ExecuteDataSet();
        }

    }
}

