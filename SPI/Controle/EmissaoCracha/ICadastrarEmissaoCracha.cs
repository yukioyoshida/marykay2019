using System;
using System.Data;
using SPI;

public interface ICadastrarEmissaoCracha
{
     void MS_Incluir(CTO_EmissaoCracha ITO_EmissaoCracha, string prVia);
     void MS_Alterar(CTO_EmissaoCracha ITO_EmissaoCracha);
     void MS_Excluir(CTO_EmissaoCracha ITO_EmissaoCracha);
     CTO_EmissaoCracha MS_Obter(int pritObjRef);
     CTO_EmissaoCracha MS_Obter(string prstID);
     DataTable MS_ObterResumo();
     string MS_QuantidadeDuplicados();
     DataTable MS_ObterRanking();
     DataTable MS_ObterPorData(DateTime dtData); 
}

