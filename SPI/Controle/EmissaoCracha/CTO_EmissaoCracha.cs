using System;
namespace SPI 
{

    public class CTO_EmissaoCracha 
    {
          //  Declaracao dos atributos Puros
        int AP_ObjRef;
        string AP_CodigoBarras;
        DateTime AP_Data;
        string AP_Hora;
        string AP_UsuarioImpressao;
        string AP_OrigemImpressao;

        public CTO_EmissaoCracha () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public int getObjRef()
        {
            return AP_ObjRef;
        }
        public string getCodigoBarras()
        {
            return AP_CodigoBarras;
        }
        public DateTime getData()
        {
            return AP_Data;
        }
        public string getHora()
        {
            return AP_Hora;
        }
        public string getUsuarioImpressao()
        {
            return AP_UsuarioImpressao;
        }
        public string getOrigemImpressao()
        {
            return AP_OrigemImpressao;
        }
         //  Declaracao dos Metodos set
        public void setObjRef( int prObjRef)
        {
            AP_ObjRef = prObjRef;
        }
        public void setCodigoBarras( string prCodigoBarras)
        {
            AP_CodigoBarras = prCodigoBarras;
        }
        public void setData( DateTime prData)
        {
            AP_Data = prData;
        }
        public void setHora( string prHora)
        {
            AP_Hora = prHora;
        }
        public void setUsuarioImpressao( string prUsuarioImpressao)
        {
            AP_UsuarioImpressao = prUsuarioImpressao;
        }
        public void setOrigemImpressao( string prOrigemImpressao)
        {
            AP_OrigemImpressao = prOrigemImpressao;
        }
    }
}

