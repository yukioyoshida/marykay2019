using System;
using System.Data;
using SPI;

public class CAT_CadastrarEmissaoCracha : CAB_CadastrarEmissaoCracha
{
     public CAT_CadastrarEmissaoCracha()
     {
     }
     public override void MS_Incluir(CTO_EmissaoCracha ITO_EmissaoCracha, string prVia)
     {
          DAO_EmissaoCracha IDAO_EmissaoCracha = new DAO_EmissaoCracha();
          IDAO_EmissaoCracha.MS_Incluir(ITO_EmissaoCracha, prVia);
     }
     public override void MS_Alterar(CTO_EmissaoCracha ITO_EmissaoCracha)
     {
         DAO_EmissaoCracha IDAO_EmissaoCracha = new DAO_EmissaoCracha();
         IDAO_EmissaoCracha.MS_Alterar(ITO_EmissaoCracha);
     }
     public override void MS_Excluir(CTO_EmissaoCracha ITO_EmissaoCracha)
     {
         DAO_EmissaoCracha IDAO_EmissaoCracha = new DAO_EmissaoCracha();
         IDAO_EmissaoCracha.MS_Excluir(ITO_EmissaoCracha);
     }
     public override CTO_EmissaoCracha MS_Obter(int pritObjRef)
     {
           DAO_EmissaoCracha IDAO_EmissaoCracha = new DAO_EmissaoCracha();
           return IDAO_EmissaoCracha.MS_Obter(pritObjRef);
     }
     public override CTO_EmissaoCracha MS_Obter(string prstID)
     {
          DAO_EmissaoCracha IDAO_EmissaoCracha = new DAO_EmissaoCracha();
          return IDAO_EmissaoCracha.MS_Obter(prstID);
     }

     public override DataTable MS_ObterResumo()
     {
         DAO_EmissaoCracha IDAO_EmissaoCracha = new DAO_EmissaoCracha();
         return IDAO_EmissaoCracha.MS_ObterResumo();
     }
    
     public override string MS_QuantidadeDuplicados()
     {
         DAO_EmissaoCracha IDAO_EmissaoCracha = new DAO_EmissaoCracha();
         return IDAO_EmissaoCracha.MS_QuantidadeDuplicados();
     }

     public override DataTable MS_ObterRanking()
     {
         DAO_EmissaoCracha IDAO_EmissaoCracha = new DAO_EmissaoCracha();
         return IDAO_EmissaoCracha.MS_ObterRanking();
     }
     
     public override DataTable MS_ObterPorData(DateTime dtData)
     {
         DAO_EmissaoCracha IDAO_EmissaoCracha = new DAO_EmissaoCracha();
         return IDAO_EmissaoCracha.MS_ObterPorData(dtData);
     }
}

