using System.Data;
using System;
namespace SPI
{
    public class DAO_EmissaoCracha : FWK_DAO_EmissaoCracha
    {
        public DAO_EmissaoCracha()
        {
        }

        public CTO_EmissaoCracha MS_Obter(string prstID)
        {
            CTO_EmissaoCracha ITO_EmissaoCracha = new CTO_EmissaoCracha();
            MI_SetSql (AP_SelectALL + 
                       "FROM tblEmissaoCracha " + 
                       "WHERE ObjRef = @ID ");
            MI_AddParameters("@ID", prstID);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_EmissaoCracha = null;
            else
                ITO_EmissaoCracha = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_EmissaoCracha;
        }

        public DataTable MS_ObterResumo()
        {
            MI_SetSql("Select count(*)Quantidade, Data from         " +
                      "tblEmissaoCracha group by Data order by Data ");
            return MI_ExecuteDataSet(); 
        }

        public string MS_QuantidadeDuplicados()
        {
            string stQuantidade = string.Empty;
            MI_SetSql("Select count(distinct(CodigoBarras))Quantidade " +
                      "from tblemissaocracha                          ");
            MI_ExecuteQuery();
            try
            {
                if (MI_EOF())
                    stQuantidade = "0";
                else
                    stQuantidade = AP_DataReader.GetString(0);
            }
            catch
            {
                stQuantidade = "0";
            }
            MI_FecharConexao();
            return stQuantidade;
        }

        public DataTable MS_ObterRanking()
        {
            MI_SetSql("Select  count(*)Quantidade, UsuarioImpressao from tblemissaocracha     " + 
                      "group by UsuarioImpressao order by Quantidade desc                     ");
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterPorData(DateTime dtData)
        {
            MI_SetSql(AP_SelectALL +
                       "FROM tblEmissaoCracha " +
                       "WHERE Data = @Data    ");
            MI_AddParameters("@Data", dtData);
            return MI_ExecuteDataSet();
        }
        
    }
}

