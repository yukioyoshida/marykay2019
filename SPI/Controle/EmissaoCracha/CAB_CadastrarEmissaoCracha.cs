using System;
using System.Data; 
using SPI;

public abstract class CAB_CadastrarEmissaoCracha : ICadastrarEmissaoCracha
{
     public abstract void MS_Incluir(CTO_EmissaoCracha ITO_EmissaoCracha, string prVia);
     public abstract void MS_Alterar(CTO_EmissaoCracha ITO_EmissaoCracha);
     public abstract void MS_Excluir(CTO_EmissaoCracha ITO_EmissaoCracha);
     public abstract CTO_EmissaoCracha MS_Obter(int pritObjRef);
     public abstract CTO_EmissaoCracha MS_Obter(string prstID);
     public abstract DataTable MS_ObterResumo();
     public abstract string MS_QuantidadeDuplicados();
     public abstract DataTable MS_ObterRanking();
     public abstract DataTable MS_ObterPorData(DateTime dtData); 
}

