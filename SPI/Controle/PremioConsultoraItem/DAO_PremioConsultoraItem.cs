﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SPI
{
    public class DAO_PremioConsultoraItem : FWK_DAO_PremioConsultoraItem
    {
        public List<CTO_PremioConsultoraItem> MS_Obter(int cdreconhecimento)
        {
            List<CTO_PremioConsultoraItem> dadosPremioConsultoraItem = new List<CTO_PremioConsultoraItem>();


            return dadosPremioConsultoraItem;

        }
        public void MS_Alterar(CTO_PremioConsultoraItem ITO_PremioConsultoraItem)
        {

        }
        public bool MS_CancelarEntregaItem(Int32 cdItem, string stUsuario, string stTerminal)
        {
            bool boResult = false;
            try
            {
                string strSQL = "UPDATE TBLPREMIOCONSULTORA_ITEM SET dsRetirado = 'N', dtRetirado = NULL, UsuarioEntregue = NULL, DSCODIGOBARRASPRODUTO = NULL, dtCancelamentoEntrega = GETDATE(), dsUsuarioCancelamento = @UsuarioCancelamento, dsTerminalCancelamento = @dsTerminal WHERE cdItem = @cdItem";
                var cnn = AP_Connection.Execute(strSQL, new { @cdItem = cdItem, @UsuarioCancelamento = stUsuario, @dsTerminal = stTerminal });
                boResult = true;
            }
            catch(Exception ex)
            {
                throw ex;
                boResult = false;
            }
            return boResult;
        }
        public bool MS_AtualizarCodigoBarrasProduto(String stCodigo, int cdItem)
        {
            bool boReturn = false;
            try
            {
                String strQuery = "UPDATE TBLPREMIOCONSULTORA_ITEM SET DSCODIGOBARRASPRODUTO = @DSCODIGOBARRASPRODUTO WHERE CDITEM = @CDITEM";
                var cnn = AP_Connection.Execute(strQuery, new { @DSCODIGOBARRASPRODUTO = stCodigo, @CDITEM = cdItem });
                boReturn = true;
            }
            catch { boReturn = false; }
            return boReturn;
        }

        public Dictionary<int, int> MS_CalcularRetirados()
        {
            StringBuilder strQuery = new StringBuilder();

            strQuery.AppendLine("SELECT ROT.CDROTEIRO, COUNT(DSTROCALOCAL) RESERVADOS ");
            strQuery.AppendLine("FROM TBLROTEIRO ROT JOIN TBLPREMIOCONSULTORA_ITEM ITEM ");
            strQuery.AppendLine("ON ROT.CDROTEIRO = ITEM.DSTROCALOCAL ");
            strQuery.AppendLine("JOIN TBLPREMIOCONSULTORA PREM ");
            strQuery.AppendLine("ON PREM.OBJREF = ITEM.CDRECONHECIMENTO ");
            strQuery.AppendLine("WHERE ");
            strQuery.AppendLine("ISNULL(RTRIM(LTRIM(DSTROCALOCAL)), '') <> '' ");
            strQuery.AppendLine("AND ATIVO = 'S' ");
            strQuery.AppendLine("AND DSRETIRADO = 'S' ");
            strQuery.AppendLine("AND Correios = 'N' ");
            strQuery.AppendLine("AND TIPOJOIA <> 'FAIXA' ");
            strQuery.AppendLine("GROUP BY ROT.CDROTEIRO");
            //strQuery.AppendLine("AND DSTROCALOCAL = @CDROTEIRO");

            var cnn = AP_Connection.Query(strQuery.ToString()).ToDictionary(row => (int)row.CDROTEIRO, row => (int)row.RESERVADOS);
            Dictionary<int, int> dcQuantidades = cnn;

            return dcQuantidades;
        }

        public Dictionary<int, int> MS_CalcularQuantidadeCorreios()
        {
            StringBuilder strQuery = new StringBuilder();

            strQuery.AppendLine("SELECT ROT.CDROTEIRO, COUNT(DSTROCALOCAL) RESERVADOS ");
            strQuery.AppendLine("FROM TBLROTEIRO ROT JOIN TBLPREMIOCONSULTORA_ITEM ITEM ");
            strQuery.AppendLine("ON ROT.CDROTEIRO = ITEM.DSTROCALOCAL ");
            strQuery.AppendLine("JOIN TBLPREMIOCONSULTORA PREM ");
            strQuery.AppendLine("ON PREM.OBJREF = ITEM.CDRECONHECIMENTO ");
            strQuery.AppendLine("WHERE ");
            strQuery.AppendLine("ISNULL(RTRIM(LTRIM(DSTROCALOCAL)), '') <> '' ");
            strQuery.AppendLine("AND ATIVO = 'S' ");
            strQuery.AppendLine("AND CORREIOS = 'S' ");
            strQuery.AppendLine("AND TIPOJOIA <> 'FAIXA' ");
            strQuery.AppendLine("GROUP BY ROT.CDROTEIRO");
            //strQuery.AppendLine("AND DSTROCALOCAL = @CDROTEIRO");

            var cnn = AP_Connection.Query(strQuery.ToString()).ToDictionary(row => (int)row.CDROTEIRO, row => (int)row.RESERVADOS);
            Dictionary<int, int> dcQuantidades = cnn;

            return dcQuantidades;
        }

        public Dictionary<int, int> MS_CalcularReservados()
        {
            StringBuilder strQuery = new StringBuilder();

            strQuery.AppendLine("SELECT ROT.CDROTEIRO, COUNT(DSTROCALOCAL) RESERVADOS ");
            strQuery.AppendLine("FROM TBLROTEIRO ROT JOIN TBLPREMIOCONSULTORA_ITEM ITEM ");
            strQuery.AppendLine("ON ROT.CDROTEIRO = ITEM.DSTROCALOCAL ");
            strQuery.AppendLine("JOIN TBLPREMIOCONSULTORA PREM ");
            strQuery.AppendLine("ON PREM.OBJREF = ITEM.CDRECONHECIMENTO ");
            strQuery.AppendLine("WHERE ");
            strQuery.AppendLine("ISNULL(RTRIM(LTRIM(DSTROCALOCAL)), '') <> '' ");
            strQuery.AppendLine("AND ATIVO = 'S' ");
            strQuery.AppendLine("AND DSRETIRADO = 'N' ");
            strQuery.AppendLine("AND CORREIOS = 'N' ");
            strQuery.AppendLine("AND TIPOJOIA <> 'FAIXA' ");
            strQuery.AppendLine("GROUP BY ROT.cdRoteiro");
            //strQuery.AppendLine("AND DSTROCALOCAL = @CDROTEIRO");

            var cnn = AP_Connection.Query(strQuery.ToString()).ToDictionary(row => (int)row.CDROTEIRO, row => (int)row.RESERVADOS);
            Dictionary<int, int> dcQuantidades = cnn;

            return dcQuantidades;
        }
    }
}
