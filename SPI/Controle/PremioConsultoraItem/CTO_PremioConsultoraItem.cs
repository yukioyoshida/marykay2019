﻿using System;

namespace SPI
{
    public class CTO_PremioConsultoraItem
    {
        public int cdItem { get; set; }
        public int cdReconhecimento { get; set; }
        public string dsEscolhaPre { get; set; }
        public string dsTrocaLocal { get; set; }
        public DateTime dtTroca { get; set; }
        public string UsuarioTroca { get; set; }
        public string dsRetirado { get; set; }
        public DateTime dtRetirado { get; set; }
        public string UsuarioEntregue { get; set; }
        public string UsuarioInsercao { get; set; }
        public string dsObservacoes { get; set; }
        public string dsTipo { get; set; }
        public string Correios { get; set; }
        public DateTime dtInclusao { get; set; }
        public string dsCodigoBarrasProduto { get; set; }
    }   
}
