﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SPI
{
    public class CAT_PremioConsultoraItem
    {
        public CAT_PremioConsultoraItem()
        {
        }

        public void MS_Alterar(CTO_PremioConsultoraItem ITO_PremioConsultoraItem)
        {

        }
        public bool MS_CancelarEntregaItem(int cdItem, string stUsuarioCancelamento, string stTerminal)
        {
            bool boReturn = false;
            DAO_PremioConsultoraItem dadosPremioConsultoraItem = new DAO_PremioConsultoraItem();

            boReturn = dadosPremioConsultoraItem.MS_CancelarEntregaItem(cdItem, stUsuarioCancelamento, stTerminal);

            return boReturn;
        }

        public bool MS_AtualizarCodigoBarrasProduto(String stCodigoBarras, int cdItem)
        {
            bool boReturn = false;
            DAO_PremioConsultoraItem dadosPremioConsultoraItem = new DAO_PremioConsultoraItem();

            boReturn = dadosPremioConsultoraItem.MS_AtualizarCodigoBarrasProduto(stCodigoBarras, cdItem);

            return boReturn;
        }

        public Dictionary<int, int> MS_CalcularReservados()
        {
            Dictionary<int, int> dcQuantidade = new DAO_PremioConsultoraItem().MS_CalcularReservados();
            return dcQuantidade;
        }
        public Dictionary<int, int> MS_CalcularQuantidadeCorreios()
        {
            Dictionary<int, int> dcQuantidade = new DAO_PremioConsultoraItem().MS_CalcularQuantidadeCorreios();
            return dcQuantidade;
        }

        public  Dictionary<int, int> MS_CalcularRetirados()
        {
            Dictionary<int, int> dcQuantidade = new DAO_PremioConsultoraItem().MS_CalcularRetirados();
            return dcQuantidade;
        }
    }
}
