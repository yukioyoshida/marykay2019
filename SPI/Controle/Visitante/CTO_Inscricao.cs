﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SPI;
namespace SPI
{
    public class CTO_Inscricao
    {
        public string AP_Pedido { get; set; }
        public string AP_IDdoPedido { get; set; }
        public string AP_Consultora { get; set; }
        public string AP_NomedaConsultora { get; set; }
        public string AP_NiveldeCarreira { get; set; }
        public string AP_Unidade { get; set; }
        public string AP_EmaildeContato { get; set; }
        public string AP_Telefone { get; set; }
        public string AP_Cidade { get; set; }
        public string AP_Estado { get; set; }
        public string AP_TipodeInscricao { get; set; }
        public string AP_Status { get; set; }
        public string AP_ValordoPedido { get; set; }
        public string AP_PagocomCartao { get; set; }
        public string AP_BandeiraCC { get; set; }
        public string AP_Parcelas { get; set; }
        public string AP_PagocomSaldo { get; set; }
        public string AP_PagoviaLoterica { get; set; }
        public string AP_DatadoPedido { get; set; }
        public string AP_NomedaNSD { get; set; }
        public string AP_Tipo { get; set; }

        // ESTRUTURA PADRÃO INTERAÇÃO
        public string AP_NumeroTerminal { get; set; }
        public string AP_CodigoBarras { get; set; }
        public Int32 AP_Chave { get; set; }
        public string AP_EmissaoCracha { get; set; }
        public string AP_PresenteDia1 { get; set; }
        public string AP_PresenteDia2 { get; set; }
        public string AP_PresenteDia3 { get; set; }
        public string AP_PresenteDia4 { get; set; }
        public string AP_PresenteDia5 { get; set; }
        public string AP_OrigemInsercao { get; set; }
        public DateTime AP_DataOrigemInsercao { get; set; }
        public string AP_HoraOrigemInsercao { get; set; }
        public string AP_UsuarioOrigemInsercao { get; set; }
        public string AP_OrigemAlteracao { get; set; }
        public DateTime AP_DataOrigemAlteracao { get; set; }
        public string AP_HoraOrigemAlteracao { get; set; }
        public string AP_UsuarioOrigemAlteracao { get; set; }
        public string AP_MetodoInsercao { get; set; }
        public string AP_ParticipouEdicao { get; set; }
        public string AP_OrigemConvite { get; set; }

    }
}
