using System;
using System.Data;
using SPI;

public interface ICadastrarVisitante
{
     void MS_Incluir(CTO_Visitante ITO_Visitante);
     void MS_Alterar(CTO_Visitante ITO_Visitante);
     void MS_Excluir(CTO_Visitante ITO_Visitante);
     CTO_Visitante MS_ObterPorCPF(string prstCPF);
     DataTable MS_ObterPorCPFTabela(string prstCPF);
     CTO_Visitante MS_ObterPorNome(string prstNome);
     DataTable MS_ObterPorNomeTabela(string prstNome);
     CTO_Visitante MS_ObterPorCodigo(string prstCodigo);
     CTO_Visitante MS_ObterPorEmail(string prstEmail);
     DataTable MS_LocalizarRegistroPorFiltro(string prstCriterio, string prstAtributo);
     string MS_GerarCodigoBarras();
     DataTable MS_ObterRegistro(string prstPalavra, string prContendo, string stTipoBusca);
     void MS_ConfirmaImpressao(Int64 prstCodigo);
     DataTable MS_ObterCampos();
     DataTable MS_ObterRegistroGrid(string prstChave);
     CTO_Visitante MS_ObterPorLocalizese(string prstLocalizese);
}

