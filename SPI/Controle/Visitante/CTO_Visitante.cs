using System;
namespace SPI 
{

    public class CTO_Visitante 
    {
        public CTO_Visitante()
        {
        }

        //Declaracao dos atributos Puros
        public string AP_Pedido = string.Empty;
        public string AP_IDdoPedido = string.Empty;
        public string AP_Consultora = string.Empty;
        public string AP_NomedaConsultora = string.Empty;
        public string AP_NiveldeCarreira = string.Empty;
        public string AP_Unidade = string.Empty;
        public string AP_EmaildeContato = string.Empty;
        public string AP_Telefone = string.Empty;
        public string AP_Cidade = string.Empty;
        public string AP_Estado = string.Empty;
        public string AP_TipodeInscricao = string.Empty;
        public string AP_Status = string.Empty;
        public string AP_ValordoPedido = string.Empty;
        public string AP_PagocomCartao = string.Empty;
        public string AP_BandeiraCC = string.Empty;
        public string AP_Parcelas = string.Empty;
        public string AP_PagocomSaldo = string.Empty;
        public string AP_PagoviaLoterica = string.Empty;
        public string AP_DatadoPedido = string.Empty;
        public string AP_NomedaNSD = string.Empty;
        public string AP_Tipo = string.Empty;

        // ESTRUTURA PADR�O INTERA��O
        public string AP_NumeroTerminal = string.Empty;
        public string AP_CodigoBarras = string.Empty;
        public Int32 AP_Chave;
        public string AP_EmissaoCracha = string.Empty;
        public string AP_PresenteDia1 = string.Empty;
        public string AP_PresenteDia2 = string.Empty;
        public string AP_PresenteDia3 = string.Empty;
        public string AP_PresenteDia4 = string.Empty;
        public string AP_PresenteDia5 = string.Empty;
        public string AP_OrigemInsercao = string.Empty;
        public DateTime AP_DataOrigemInsercao;
        public string AP_HoraOrigemInsercao = string.Empty;
        public string AP_UsuarioOrigemInsercao = string.Empty;
        public string AP_OrigemAlteracao = string.Empty;
        public DateTime AP_DataOrigemAlteracao;
        public string AP_HoraOrigemAlteracao = string.Empty;
        public string AP_UsuarioOrigemAlteracao = string.Empty;
        public string AP_MetodoInsercao = string.Empty;
        public string AP_ParticipouEdicao = string.Empty;
        public string AP_OrigemConvite = string.Empty;
        public string AP_CPF = string.Empty;
        public string AP_NomeCracha = string.Empty;
        public string AP_Observacoes = string.Empty;

        public string AP_ValorInscricaoNova = string.Empty;
        public string AP_FormaInscricaoNova = string.Empty;

        #region METODOS GETS
        public string getPedido() {
            return AP_Pedido;
        }
        public string getIDdoPedido() {
            return AP_IDdoPedido;
        }
        public string getConsultora() {
            return AP_Consultora;
        }
        public string getNomedaConsultora() {
            return AP_NomedaConsultora;
        }
        public string getNiveldeCarreira(){
            return AP_NiveldeCarreira;
        }
        public string getUnidade(){
            return AP_Unidade;
        }
        public string getEmaildeContato(){
            return AP_EmaildeContato;
        }
        public string getTelefone(){
            return AP_Telefone;
        }
        public string getCidade(){
            return AP_Cidade;
        }
        public string getEstado(){
            return AP_Estado;
        }
        public string getTipodeInscricao(){
            return AP_TipodeInscricao;
        }
        public string getStatus(){
            return AP_Status;
        }
        public string getValordoPedido(){
            return AP_ValordoPedido;
        }
        public string getPagocomCartao(){
            return AP_PagocomCartao;
        }
        public string getBandeiraCC(){
            return AP_BandeiraCC;
        }
        public string getParcelas(){
            return AP_Parcelas;
        }
        public string getPagocomSaldo(){
            return AP_PagocomSaldo;
        }
        public string getPagoviaLoterica(){
            return AP_PagoviaLoterica;
        }
        public string getDatadoPedido(){
            return AP_DatadoPedido;
        }
        public string getNomedaNSD(){
            return AP_NomedaNSD;
        }
        public string getTipo(){
            return AP_Tipo;
        }
        public string getValorInscricaoNova()
        {
            return AP_ValorInscricaoNova;
        }
        public string getFormaInscricaoNova()
        {
            return AP_FormaInscricaoNova;
        }

        // ESTRUTURA PADR�O INTERA��O
        public string getNumeroTerminal(){
            return AP_NumeroTerminal;
        }
        public string getCodigoBarras(){
            return AP_CodigoBarras;
        }
        public Int32 getChave() {
            return AP_Chave;
        }
        public string getEmissaoCracha(){
            return AP_EmissaoCracha;
        }
        public string getPresenteDia1(){
            return AP_PresenteDia1;
        }
        public string getPresenteDia2(){
            return AP_PresenteDia2;
        }
        public string getPresenteDia3(){
            return AP_PresenteDia3;
        }
        public string getPresenteDia4(){
            return AP_PresenteDia4;
        }
        public string getPresenteDia5(){
            return AP_PresenteDia5;
        }
        public string getOrigemInsercao(){
            return AP_OrigemInsercao;
        }
        public DateTime getDataOrigemInsercao() {
            return AP_DataOrigemInsercao;
        }
        public string getHoraOrigemInsercao(){
            return AP_HoraOrigemInsercao;
        }
        public string getUsuarioOrigemInsercao(){
            return AP_UsuarioOrigemInsercao;
        }
        public string getOrigemAlteracao(){
            return AP_OrigemAlteracao;
        }
        public DateTime getDataOrigemAlteracao() {
            return AP_DataOrigemAlteracao;
        }
        public string getHoraOrigemAlteracao(){
            return AP_HoraOrigemAlteracao;
        }
        public string getUsuarioOrigemAlteracao(){
            return AP_UsuarioOrigemAlteracao;
        }
        public string getMetodoInsercao(){
            return AP_MetodoInsercao;
        }
        public string getParticipouEdicao(){
            return AP_ParticipouEdicao;
        }
        public string getOrigemConvite(){
            return AP_OrigemConvite;
        }
        public string getCPF(){
            return AP_CPF;
        }
        public string getNomeCracha(){
            return AP_NomeCracha;
        }
        public string getObservacoes(){
            return AP_Observacoes;
        }

        #endregion

        #region METODOS SETS

        public void setPedido(string prValue){
            AP_Pedido = prValue;
        }
        public void setIDdoPedido(string prValue){
            AP_IDdoPedido = prValue;
        }
        public void setConsultora(string prValue){
            AP_Consultora = prValue;
        }
        public void setNomedaConsultora(string prValue){
            AP_NomedaConsultora = prValue;
        }
        public void setNiveldeCarreira(string prValue){
            AP_NiveldeCarreira = prValue;
        }
        public void setUnidade(string prValue){
            AP_Unidade = prValue;
        }
        public void setEmaildeContato(string prValue){
            AP_EmaildeContato = prValue;
        }
        public void setTelefone(string prValue){
            AP_Telefone = prValue;
        }
        public void setCidade(string prValue){
            AP_Cidade = prValue;
        }
        public void setEstado(string prValue){
            AP_Estado = prValue;
        }
        public void setTipodeInscricao(string prValue){
            AP_TipodeInscricao = prValue;
        }
        public void setStatus(string prValue){
            AP_Status = prValue;
        }
        public void setValordoPedido(string prValue){
            AP_ValordoPedido = prValue;
        }
        public void setPagocomCartao(string prValue){
            AP_PagocomCartao = prValue;
        }
        public void setBandeiraCC(string prValue){
            AP_BandeiraCC = prValue;
        }
        public void setParcelas(string prValue){
            AP_Parcelas = prValue;
        }
        public void setPagocomSaldo(string prValue){
            AP_PagocomSaldo = prValue;
        }
        public void setPagoviaLoterica(string prValue){
            AP_PagoviaLoterica = prValue;
        }
        public void setDatadoPedido(string prValue){
            AP_DatadoPedido = prValue;
        }
        public void setNomedaNSD(string prValue){
            AP_NomedaNSD = prValue;
        }
        public void setTipo(string prValue){
            AP_Tipo = prValue;
        }

        // ESTRUTURA PADR�O INTERA��O
        public void setNumeroTerminal(string prValue){
            AP_NumeroTerminal = prValue;
        }
        public void setCodigoBarras(string prValue){
            AP_CodigoBarras = prValue;
        }
        public void setChave(Int32 prValue){
            AP_Chave = prValue;
        }
        public void setEmissaoCracha(string prValue){
            AP_EmissaoCracha = prValue;
        }
        public void setPresenteDia1(string prValue){
            AP_PresenteDia1 = prValue;
        }
        public void setPresenteDia2(string prValue){
            AP_PresenteDia2 = prValue;
        }
        public void setPresenteDia3(string prValue){
            AP_PresenteDia3 = prValue;
        }
        public void setPresenteDia4(string prValue){
            AP_PresenteDia4 = prValue;
        }
        public void setPresenteDia5(string prValue){
            AP_PresenteDia5 = prValue;
        }
        public void setOrigemInsercao(string prValue){
            AP_OrigemInsercao = prValue;
        }
        public void setDataOrigemInsercao(DateTime prValue){
            AP_DataOrigemInsercao = prValue;
        }
        public void setHoraOrigemInsercao(string prValue){
            AP_HoraOrigemInsercao = prValue;
        }
        public void setUsuarioOrigemInsercao(string prValue){
            AP_UsuarioOrigemInsercao = prValue;
        }
        public void setOrigemAlteracao(string prValue){
            AP_OrigemAlteracao = prValue;
        }
        public void setDataOrigemAlteracao(DateTime prValue)
        {
            AP_DataOrigemAlteracao = prValue;
        }
        public void setHoraOrigemAlteracao(string prValue){
            AP_HoraOrigemAlteracao = prValue;
        }
        public void setUsuarioOrigemAlteracao(string prValue){
            AP_UsuarioOrigemAlteracao = prValue;
        }
        public void setMetodoInsercao(string prValue){
            AP_MetodoInsercao = prValue;
        }
        public void setParticipouEdicao(string prValue){
            AP_ParticipouEdicao = prValue;
        }
        public void setOrigemConvite(string prValue){
            AP_OrigemConvite = prValue;
        }
        public void setCPF(string prValue){
            AP_CPF = prValue;
        }
        public void setNomeCracha(string prValue){
            AP_NomeCracha = prValue;
        }
        public void setObservacoes(string prValue){
            AP_Observacoes = prValue;
        }
        public void setValorInscricaoNova(string prValue)
        {
            AP_ValorInscricaoNova = prValue;
        }
        public void setFormaInscricaoNova(string prValue)
        {
            AP_FormaInscricaoNova = prValue;
        }

        #endregion

    }
}

