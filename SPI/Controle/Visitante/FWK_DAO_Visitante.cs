using System.Data;
using System;

namespace SPI
{
    public class FWK_DAO_Visitante : FWK_DAO_MySQL
    {
        public string AP_SelectALL = "SELECT  " +
                                      " [Pedido] " +
                                      ",[IDdoPedido] " +
                                      ",[Consultora] " +
                                      ",[NomedaConsultora] " +
                                      ",[NiveldeCarreira] " +
                                      ",[Unidade] " +
                                      ",[EmaildeContato] " +
                                      ",[Telefone] " +
                                      ",[Cidade] " +
                                      ",[Estado] " +
                                      ",[TipodeInscricao] " +
                                      ",[Status] " +
                                      ",[ValordoPedido] " +
                                      ",[PagocomCartao] " +
                                      ",[BandeiraCC] " +
                                      ",[Parcelas] " +
                                      ",[PagocomSaldo] " +
                                      ",[PagoviaLoterica] " +
                                      ",[DatadoPedido] " +
                                      ",[NomedaNSD] " +
                                      ",[Tipo] " +
                                      ",[NumeroTerminal] " +
                                      ",[CodigoBarras] " +
                                      ",[Chave] " +
                                      ",[EmissaoCracha] " +
                                      ",[PresenteDia1] " +
                                      ",[PresenteDia2] " +
                                      ",[PresenteDia3] " +
                                      ",[PresenteDia4] " +
                                      ",[PresenteDia5] " +
                                      ",[OrigemInsercao] " +
                                      ",[DataOrigemInsercao] " +
                                      ",[HoraOrigemInsercao] " +
                                      ",[UsuarioOrigemInsercao] " +
                                      ",[OrigemAlteracao] " +
                                      ",[DataOrigemAlteracao] " +
                                      ",[HoraOrigemAlteracao] " +
                                      ",[UsuarioOrigemAlteracao] " +
                                      ",[MetodoInsercao] " +
                                      ",[ParticipouEdicao] " +
                                      ",[OrigemConvite] " +
                                      ",[CPF] " +
                                      ",[NomeCracha] " +
                                      ",[Observacoes] ";

        public string AP_SelectALLFirst = "SELECT top(1) " +
                                      " [Pedido] " +
                                      ",[IDdoPedido] " +
                                      ",[Consultora] " +
                                      ",[NomedaConsultora] " +
                                      ",[NiveldeCarreira] " +
                                      ",[Unidade] " +
                                      ",[EmaildeContato] " +
                                      ",[Telefone] " +
                                      ",[Cidade] " +
                                      ",[Estado] " +
                                      ",[TipodeInscricao] " +
                                      ",[Status] " +
                                      ",[ValordoPedido] " +
                                      ",[PagocomCartao] " +
                                      ",[BandeiraCC] " +
                                      ",[Parcelas] " +
                                      ",[PagocomSaldo] " +
                                      ",[PagoviaLoterica] " +
                                      ",[DatadoPedido] " +
                                      ",[NomedaNSD] " +
                                      ",[Tipo] " +
                                      ",[NumeroTerminal] " +
                                      ",[CodigoBarras] " +
                                      ",[Chave] " +
                                      ",[EmissaoCracha] " +
                                      ",[PresenteDia1] " +
                                      ",[PresenteDia2] " +
                                      ",[PresenteDia3] " +
                                      ",[PresenteDia4] " +
                                      ",[PresenteDia5] " +
                                      ",[OrigemInsercao] " +
                                      ",[DataOrigemInsercao] " +
                                      ",[HoraOrigemInsercao] " +
                                      ",[UsuarioOrigemInsercao] " +
                                      ",[OrigemAlteracao] " +
                                      ",[DataOrigemAlteracao] " +
                                      ",[HoraOrigemAlteracao] " +
                                      ",[UsuarioOrigemAlteracao] " +
                                      ",[MetodoInsercao] " +
                                      ",[ParticipouEdicao] " +
                                      ",[OrigemConvite] " +
                                      ",[CPF] " +
                                      ",[NomeCracha] " +
                                      ",[Observacoes] ";

        public FWK_DAO_Visitante()
        {
        }

        public void MS_Incluir(CTO_Visitante ITO_Visitante)
        {
            CSV_VariaveisGlobal ISV_Variaveis = new CSV_VariaveisGlobal();

            //int itObjRef = MS_NovoObjRef();
            MI_SetSql ("INSERT INTO [dbo].[tblInscricao] " +
           "([Pedido] " +
           ",[IDdoPedido] " +
           ",[Consultora] " +
           ",[NomedaConsultora] " +
           ",[NiveldeCarreira] " +
           ",[Unidade] " +
           ",[EmaildeContato] " +
           ",[Telefone] " +
           ",[Cidade] " +
           ",[Estado] " +
           ",[TipodeInscricao] " +
           ",[Status] " +
           ",[ValordoPedido] " +
           ",[PagocomCartao] " +
           ",[BandeiraCC] " +
           ",[Parcelas] " +
           ",[PagocomSaldo] " +
           ",[PagoviaLoterica] " +
           ",[DatadoPedido] " +
           ",[NomedaNSD] " +
           ",[Tipo] " +
           ",[NumeroTerminal] " +
           ",[CodigoBarras] " +
           ",[Chave] " +
           ",[EmissaoCracha] " +
           ",[PresenteDia1] " +
           ",[PresenteDia2] " +
           ",[PresenteDia3] " +
           ",[PresenteDia4] " +
           ",[PresenteDia5] " +
           ",[OrigemInsercao] " +
           ",[DataOrigemInsercao] " +
           ",[HoraOrigemInsercao] " +
           ",[UsuarioOrigemInsercao] " +
           ",[OrigemAlteracao] " +
           ",[DataOrigemAlteracao] " +
           ",[HoraOrigemAlteracao] " +
           ",[UsuarioOrigemAlteracao] " +
           ",[MetodoInsercao] " +
           ",[ParticipouEdicao] " +
           ",[OrigemConvite] " +
           ",[CPF] " +
           ",[NomeCracha] " +
           ",[Observacoes] " +
           ",[ValorInscricaoNova] " +
           ",[FormaInscricaoNova]) " +
     "VALUES " +
           "(@Pedido" +
           ",@IDdoPedido" +
           ",@Consultora" +
           ",@NomedaConsultora" +
           ",@NiveldeCarreira" +
           ",@Unidade" +
           ",@EmaildeContato" +
           ",@Telefone" +
           ",@Cidade" +
           ",@Estado" +
           ",@TipodeInscricao" +
           ",@Status" +
           ",@ValordoPedido" +
           ",@PagocomCartao" +
           ",@BandeiraCC" +
           ",@Parcelas" +
           ",@PagocomSaldo" +
           ",@PagoviaLoterica" +
           ",@DatadoPedido" +
           ",@NomedaNSD" +
           ",@Tipo" +
           ",@NumeroTerminal" +
           ",@CodigoBarras" +
           ",@Chave" +
           ",@EmissaoCracha" +
           ",@PresenteDia1" +
           ",@PresenteDia2" +
           ",@PresenteDia3" +
           ",@PresenteDia4" +
           ",@PresenteDia5" +
           ",@OrigemInsercao" +
           ",CAST(GETDATE() AS DATE)" +
           ",CAST(GETDATE() AS TIME)" +
           ",@UsuarioOrigemInsercao" +
           ",@OrigemAlteracao" +
           ",CAST(GETDATE() AS DATE)" +
           ",CAST(GETDATE() AS TIME)" +
           ",@UsuarioOrigemAlteracao" +
           ",@MetodoInsercao" +
           ",@ParticipouEdicao" +
           ",@OrigemConvite" +
           ",@CPF" +
           ",@NomeCracha" +
           ",@Observacoes" +
           ",@ValorInscricaoNova" +
           ",@FormaInscricaoNova)");

            MI_AddParameters("@CodigoBarras", ITO_Visitante.getCodigoBarras());

            MI_AddParameters("@Pedido", ITO_Visitante.getPedido());
            MI_AddParameters("@IDdoPedido", ITO_Visitante.getIDdoPedido());
            MI_AddParameters("@Consultora", ITO_Visitante.getConsultora());
            MI_AddParameters("@NomedaConsultora", ITO_Visitante.getNomedaConsultora());
            MI_AddParameters("@NiveldeCarreira", ITO_Visitante.getNiveldeCarreira());
            MI_AddParameters("@Unidade", ITO_Visitante.getUnidade());
            MI_AddParameters("@EmaildeContato", ITO_Visitante.getEmaildeContato());
            MI_AddParameters("@Telefone", ITO_Visitante.getTelefone());
            MI_AddParameters("@Cidade", ITO_Visitante.getCidade());
            MI_AddParameters("@Estado", ITO_Visitante.getEstado());
            MI_AddParameters("@TipodeInscricao", ITO_Visitante.getTipodeInscricao());
            MI_AddParameters("@Status", ITO_Visitante.getStatus());
            MI_AddParameters("@ValordoPedido", ITO_Visitante.getValordoPedido());
            MI_AddParameters("@PagocomCartao", ITO_Visitante.getPagocomCartao());
            MI_AddParameters("@BandeiraCC", ITO_Visitante.getBandeiraCC());
            MI_AddParameters("@Parcelas", ITO_Visitante.getParcelas());
            MI_AddParameters("@PagocomSaldo", ITO_Visitante.getPagocomSaldo());
            MI_AddParameters("@PagoviaLoterica", ITO_Visitante.getPagoviaLoterica());
            MI_AddParameters("@DatadoPedido", ITO_Visitante.getDatadoPedido());
            MI_AddParameters("@NomedaNSD", ITO_Visitante.getNomedaNSD());
            MI_AddParameters("@Tipo", ITO_Visitante.getTipo());
            MI_AddParameters("@NumeroTerminal", ITO_Visitante.getNumeroTerminal());
            MI_AddParameters("@Chave", ITO_Visitante.getChave());
            MI_AddParameters("@EmissaoCracha", ITO_Visitante.getEmissaoCracha());
            MI_AddParameters("@PresenteDia1", ITO_Visitante.getPresenteDia1());
            MI_AddParameters("@PresenteDia2", ITO_Visitante.getPresenteDia2());
            MI_AddParameters("@PresenteDia3", ITO_Visitante.getPresenteDia3());
            MI_AddParameters("@PresenteDia4", ITO_Visitante.getPresenteDia4());
            MI_AddParameters("@PresenteDia5", ITO_Visitante.getPresenteDia5());
            MI_AddParameters("@OrigemInsercao", ITO_Visitante.getOrigemInsercao());
            //MI_AddParameters("@DataOrigemInsercao", ITO_Visitante.getDataOrigemInsercao());
            //MI_AddParameters("@HoraOrigemInsercao", ITO_Visitante.getHoraOrigemInsercao());
            MI_AddParameters("@UsuarioOrigemInsercao", ITO_Visitante.getUsuarioOrigemInsercao());
            MI_AddParameters("@OrigemAlteracao", ITO_Visitante.getOrigemAlteracao());
            //MI_AddParameters("@DataOrigemAlteracao", ITO_Visitante.getDataOrigemAlteracao());
            //MI_AddParameters("@HoraOrigemAlteracao", ITO_Visitante.getHoraOrigemAlteracao());
            MI_AddParameters("@UsuarioOrigemAlteracao", ITO_Visitante.getUsuarioOrigemAlteracao());
            MI_AddParameters("@MetodoInsercao", ITO_Visitante.getMetodoInsercao());
            MI_AddParameters("@ParticipouEdicao", ITO_Visitante.getParticipouEdicao());
            MI_AddParameters("@OrigemConvite", ITO_Visitante.getOrigemConvite());
            MI_AddParameters("@CPF", ITO_Visitante.getCPF());
            MI_AddParameters("@NomeCracha", ITO_Visitante.getNomeCracha());
            MI_AddParameters("@Observacoes", ITO_Visitante.getObservacoes());

            MI_AddParameters("@ValorInscricaoNova", ITO_Visitante.getValorInscricaoNova());
            MI_AddParameters("@FormaInscricaoNova", ITO_Visitante.getFormaInscricaoNova());

            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_Alterar(CTO_Visitante ITO_Visitante)
        {
            MI_SetSql ("UPDATE tblInscricao SET " +
                            "Pedido = @Pedido, " +
                            "IDdoPedido = @IDdoPedido, " +
                            "Consultora = @Consultora, " +
                            "NomedaConsultora = @NomedaConsultora, " +
                            "NiveldeCarreira = @NiveldeCarreira, " +
                            "Unidade = @Unidade, " +
                            "EmaildeContato = @EmaildeContato, " +
                            "Telefone = @Telefone, " +
                            "Cidade = @Cidade, " +
                            "Estado = @Estado, " +
                            "TipodeInscricao = @TipodeInscricao, " +
                            "Status = @Status, " +
                            "ValordoPedido = @ValordoPedido, " +
                            "PagocomCartao = @PagocomCartao, " +
                            "BandeiraCC = @BandeiraCC, " +
                            "Parcelas = @Parcelas, " +
                            "PagocomSaldo = @PagocomSaldo, " +
                            "PagoviaLoterica = @PagoviaLoterica, " +
                            "DatadoPedido = @DatadoPedido, " +
                            "NomedaNSD = @NomedaNSD, " +
                            "Tipo = @Tipo, " +
                            "NumeroTerminal = @NumeroTerminal, " +
                            "CodigoBarras = @CodigoBarras, " +
                            "Chave = @Chave, " +
                            "EmissaoCracha = @EmissaoCracha, " +
                            "PresenteDia1 = @PresenteDia1, " +
                            "PresenteDia2 = @PresenteDia2, " +
                            "PresenteDia3 = @PresenteDia3, " +
                            "PresenteDia4 = @PresenteDia4, " +
                            "PresenteDia5 = @PresenteDia5, " +
                            "OrigemInsercao = @OrigemInsercao, " +
                            "DataOrigemInsercao = @DataOrigemInsercao, " +
                            "HoraOrigemInsercao = @HoraOrigemInsercao, " +
                            "UsuarioOrigemInsercao = @UsuarioOrigemInsercao, " +
                            "OrigemAlteracao = @OrigemAlteracao, " +
                            "DataOrigemAlteracao = @DataOrigemAlteracao, " +
                            "HoraOrigemAlteracao = @HoraOrigemAlteracao, " +
                            "UsuarioOrigemAlteracao = @UsuarioOrigemAlteracao, " +
                            "MetodoInsercao = @MetodoInsercao, " +
                            "ParticipouEdicao = @ParticipouEdicao, " +
                            "OrigemConvite = @OrigemConvite, " +
                            "CPF = @CPF, " +
                            "NomeCracha = @NomeCracha, " +
                            "Observacoes = @Observacoes " +
                       "WHERE CodigoBarras = @CodigoBarras ");

            MI_AddParameters("@CodigoBarras", ITO_Visitante.getCodigoBarras());

            MI_AddParameters("@Pedido", ITO_Visitante.getPedido());
            MI_AddParameters("@IDdoPedido", ITO_Visitante.getIDdoPedido());
            MI_AddParameters("@Consultora", ITO_Visitante.getConsultora());
            MI_AddParameters("@NomedaConsultora", ITO_Visitante.getNomedaConsultora());
            MI_AddParameters("@NiveldeCarreira", ITO_Visitante.getNiveldeCarreira());
            MI_AddParameters("@Unidade", ITO_Visitante.getUnidade());
            MI_AddParameters("@EmaildeContato", ITO_Visitante.getEmaildeContato());
            MI_AddParameters("@Telefone", ITO_Visitante.getTelefone());
            MI_AddParameters("@Cidade", ITO_Visitante.getCidade());
            MI_AddParameters("@Estado", ITO_Visitante.getEstado());
            MI_AddParameters("@TipodeInscricao", ITO_Visitante.getTipodeInscricao());
            MI_AddParameters("@Status", ITO_Visitante.getStatus());
            MI_AddParameters("@ValordoPedido", ITO_Visitante.getValordoPedido());
            MI_AddParameters("@PagocomCartao", ITO_Visitante.getPagocomCartao());
            MI_AddParameters("@BandeiraCC", ITO_Visitante.getBandeiraCC());
            MI_AddParameters("@Parcelas", ITO_Visitante.getParcelas());
            MI_AddParameters("@PagocomSaldo", ITO_Visitante.getPagocomSaldo());
            MI_AddParameters("@PagoviaLoterica", ITO_Visitante.getPagoviaLoterica());
            MI_AddParameters("@DatadoPedido", ITO_Visitante.getDatadoPedido());
            MI_AddParameters("@NomedaNSD", ITO_Visitante.getNomedaNSD());
            MI_AddParameters("@Tipo", ITO_Visitante.getTipo());
            MI_AddParameters("@NumeroTerminal", ITO_Visitante.getNumeroTerminal());
            MI_AddParameters("@Chave", ITO_Visitante.getChave());
            MI_AddParameters("@EmissaoCracha", ITO_Visitante.getEmissaoCracha());
            MI_AddParameters("@PresenteDia1", ITO_Visitante.getPresenteDia1());
            MI_AddParameters("@PresenteDia2", ITO_Visitante.getPresenteDia2());
            MI_AddParameters("@PresenteDia3", ITO_Visitante.getPresenteDia3());
            MI_AddParameters("@PresenteDia4", ITO_Visitante.getPresenteDia4());
            MI_AddParameters("@PresenteDia5", ITO_Visitante.getPresenteDia5());
            MI_AddParameters("@OrigemInsercao", ITO_Visitante.getOrigemInsercao());
            MI_AddParameters("@DataOrigemInsercao", ITO_Visitante.getDataOrigemInsercao());
            MI_AddParameters("@HoraOrigemInsercao", ITO_Visitante.getHoraOrigemInsercao());
            MI_AddParameters("@UsuarioOrigemInsercao", ITO_Visitante.getUsuarioOrigemInsercao());
            MI_AddParameters("@OrigemAlteracao", ITO_Visitante.getOrigemAlteracao());
            MI_AddParameters("@DataOrigemAlteracao", ITO_Visitante.getDataOrigemAlteracao());
            MI_AddParameters("@HoraOrigemAlteracao", ITO_Visitante.getHoraOrigemAlteracao());
            MI_AddParameters("@UsuarioOrigemAlteracao", ITO_Visitante.getUsuarioOrigemAlteracao());
            MI_AddParameters("@MetodoInsercao", ITO_Visitante.getMetodoInsercao());
            MI_AddParameters("@ParticipouEdicao", ITO_Visitante.getParticipouEdicao());
            MI_AddParameters("@OrigemConvite", ITO_Visitante.getOrigemConvite());
            MI_AddParameters("@CPF", ITO_Visitante.getCPF());
            MI_AddParameters("@NomeCracha", ITO_Visitante.getNomeCracha());
            MI_AddParameters("@Observacoes", ITO_Visitante.getObservacoes());

            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_Excluir(CTO_Visitante ITO_Visitante)
        {
            MI_SetSql ("DELETE FROM tblVisitante WHERE CodigoBarras = @CodigoBarras");
            MI_AddParameters("@CodigoBarras", ITO_Visitante.getCodigoBarras());
            
            MI_ExecuteNonQuery();
            MI_FecharConexao();


        }

        public CTO_Visitante MI_DataSetToEntidade()
        {
            CTO_Visitante ITO_Visitante = new CTO_Visitante();

            ITO_Visitante.setPedido((AP_DataReader["Pedido"] == null ? "" : AP_DataReader["Pedido"].ToString()));
            ITO_Visitante.setIDdoPedido((AP_DataReader["IDdoPedido"] == null ? "" : AP_DataReader["IDdoPedido"].ToString()));
            ITO_Visitante.setConsultora((AP_DataReader["Consultora"] == null ? "" : AP_DataReader["Consultora"].ToString()));
            ITO_Visitante.setNomedaConsultora((AP_DataReader["NomedaConsultora"] == null ? "" : AP_DataReader["NomedaConsultora"].ToString()));
            ITO_Visitante.setNiveldeCarreira((AP_DataReader["NiveldeCarreira"] == null ? "" : AP_DataReader["NiveldeCarreira"].ToString()));
            ITO_Visitante.setUnidade((AP_DataReader["Unidade"] == null ? "" : AP_DataReader["Unidade"].ToString()));
            ITO_Visitante.setEmaildeContato((AP_DataReader["EmaildeContato"] == null ? "" : AP_DataReader["EmaildeContato"].ToString()));
            ITO_Visitante.setTelefone((AP_DataReader["Telefone"] == null ? "" : AP_DataReader["Telefone"].ToString()));
            ITO_Visitante.setCidade((AP_DataReader["Cidade"] == null ? "" : AP_DataReader["Cidade"].ToString()));
            ITO_Visitante.setEstado((AP_DataReader["Estado"] == null ? "" : AP_DataReader["Estado"].ToString()));
            ITO_Visitante.setTipodeInscricao((AP_DataReader["TipodeInscricao"] == null ? "" : AP_DataReader["TipodeInscricao"].ToString()));
            ITO_Visitante.setStatus((AP_DataReader["Status"] == null ? "" : AP_DataReader["Status"].ToString()));
            ITO_Visitante.setValordoPedido((AP_DataReader["ValordoPedido"] == null ? "" : AP_DataReader["ValordoPedido"].ToString()));
            ITO_Visitante.setPagocomCartao((AP_DataReader["PagocomCartao"] == null ? "" : AP_DataReader["PagocomCartao"].ToString()));
            ITO_Visitante.setBandeiraCC((AP_DataReader["BandeiraCC"] == null ? "" : AP_DataReader["BandeiraCC"].ToString()));
            ITO_Visitante.setParcelas((AP_DataReader["Parcelas"] == null ? "" : AP_DataReader["Parcelas"].ToString()));
            ITO_Visitante.setPagocomSaldo((AP_DataReader["PagocomSaldo"] == null ? "" : AP_DataReader["PagocomSaldo"].ToString()));
            ITO_Visitante.setPagoviaLoterica((AP_DataReader["PagoviaLoterica"] == null ? "" : AP_DataReader["PagoviaLoterica"].ToString()));
            ITO_Visitante.setDatadoPedido((AP_DataReader["DatadoPedido"] == null ? "" : AP_DataReader["DatadoPedido"].ToString()));
            ITO_Visitante.setNomedaNSD((AP_DataReader["NomedaNSD"] == null ? "" : AP_DataReader["NomedaNSD"].ToString()));
            ITO_Visitante.setTipo((AP_DataReader["Tipo"] == null ? "" : AP_DataReader["Tipo"].ToString()));
            ITO_Visitante.setNumeroTerminal((AP_DataReader["NumeroTerminal"] == null ? "" : AP_DataReader["NumeroTerminal"].ToString()));
            ITO_Visitante.setCodigoBarras((AP_DataReader["CodigoBarras"] == null ? "" : AP_DataReader["CodigoBarras"].ToString()));
            ITO_Visitante.setChave((AP_DataReader["Chave"] == null ? 0 : Convert.ToInt32(AP_DataReader["Chave"])));
            ITO_Visitante.setEmissaoCracha((AP_DataReader["EmissaoCracha"] == null ? "" : AP_DataReader["EmissaoCracha"].ToString()));
            ITO_Visitante.setPresenteDia1((AP_DataReader["PresenteDia1"] == null ? "" : AP_DataReader["PresenteDia1"].ToString()));
            ITO_Visitante.setPresenteDia2((AP_DataReader["PresenteDia2"] == null ? "" : AP_DataReader["PresenteDia2"].ToString()));
            ITO_Visitante.setPresenteDia3((AP_DataReader["PresenteDia3"] == null ? "" : AP_DataReader["PresenteDia3"].ToString()));
            ITO_Visitante.setPresenteDia4((AP_DataReader["PresenteDia4"] == null ? "" : AP_DataReader["PresenteDia4"].ToString()));
            ITO_Visitante.setPresenteDia5((AP_DataReader["PresenteDia5"] == null ? "" : AP_DataReader["PresenteDia5"].ToString()));
            ITO_Visitante.setOrigemInsercao((AP_DataReader["OrigemInsercao"] == null ? "" : AP_DataReader["OrigemInsercao"].ToString()));
            ITO_Visitante.setDataOrigemInsercao((AP_DataReader["DataOrigemInsercao"] == null ? System.DateTime.Now : Convert.ToDateTime(AP_DataReader["DataOrigemInsercao"])));
            ITO_Visitante.setHoraOrigemInsercao((AP_DataReader["HoraOrigemInsercao"] == null ? "" : AP_DataReader["HoraOrigemInsercao"].ToString()));
            ITO_Visitante.setUsuarioOrigemInsercao((AP_DataReader["UsuarioOrigemInsercao"] == null ? "" : AP_DataReader["UsuarioOrigemInsercao"].ToString()));
            ITO_Visitante.setOrigemAlteracao((AP_DataReader["OrigemAlteracao"] == null ? "" : AP_DataReader["OrigemAlteracao"].ToString()));
            ITO_Visitante.setDataOrigemAlteracao((AP_DataReader["DataOrigemAlteracao"] == null ? System.DateTime.Now : Convert.ToDateTime(AP_DataReader["DataOrigemAlteracao"])));
            ITO_Visitante.setHoraOrigemAlteracao((AP_DataReader["HoraOrigemAlteracao"] == null ? "" : AP_DataReader["HoraOrigemAlteracao"].ToString()));
            ITO_Visitante.setUsuarioOrigemAlteracao((AP_DataReader["UsuarioOrigemAlteracao"] == null ? "" : AP_DataReader["UsuarioOrigemAlteracao"].ToString()));
            ITO_Visitante.setMetodoInsercao((AP_DataReader["MetodoInsercao"] == null ? "" : AP_DataReader["MetodoInsercao"].ToString()));
            ITO_Visitante.setParticipouEdicao((AP_DataReader["ParticipouEdicao"] == null ? "" : AP_DataReader["ParticipouEdicao"].ToString()));
            ITO_Visitante.setOrigemConvite((AP_DataReader["OrigemConvite"] == null ? "" : AP_DataReader["OrigemConvite"].ToString()));
            ITO_Visitante.setCPF((AP_DataReader["CPF"] == null ? "" : AP_DataReader["CPF"].ToString()));
            ITO_Visitante.setNomeCracha((AP_DataReader["NomeCracha"] == null ? "" : AP_DataReader["NomeCracha"].ToString()));
            ITO_Visitante.setObservacoes((AP_DataReader["Observacoes"] == null ? "" : AP_DataReader["Observacoes"].ToString()));

            return ITO_Visitante;
        }

        public CTO_Visitante MS_ObterProximo()
        {
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_Visitante = null;
            else
                ITO_Visitante = MI_DataSetToEntidade();
                return ITO_Visitante;
        }

        public int MS_NovoObjRef()
        {
            int itObjRef;
            MI_SetSql("Select Max(ObjRef) from tblVisitante");
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 1;
            }
            else
            {
                itObjRef = AP_DataReader.GetInt32(0);
                itObjRef++;
            }
            MI_FecharConexao();
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblVisitante");
            return MI_ExecuteDataSet();
        }
    }
}

