using System;
namespace SPI 
{

    public class CTO_Login 
    {
          //  Declaracao dos atributos Puros
        int AP_ObjRef;
        string AP_NomeLogin;
        string AP_SenhaLogin;
        string AP_NomeUsuario;
        string AP_EmailUsuario;
        string AP_CelularUsuario;
        string AP_NivelAcesso;

        public CTO_Login () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public int getObjRef()
        {
            return AP_ObjRef;
        }
        public string getNomeLogin()
        {
            return AP_NomeLogin;
        }
        public string getSenhaLogin()
        {
            return AP_SenhaLogin;
        }
        public string getNomeUsuario()
        {
            return AP_NomeUsuario;
        }
        public string getEmailUsuario()
        {
            return AP_EmailUsuario;
        }
        public string getCelularUsuario()
        {
            return AP_CelularUsuario;
        }
        public string getNivelAcesso()
        {
            return AP_NivelAcesso;
        }
         //  Declaracao dos Metodos set
        public void setObjRef( int prObjRef)
        {
            AP_ObjRef = prObjRef;
        }
        public void setNomeLogin( string prNomeLogin)
        {
            AP_NomeLogin = prNomeLogin;
        }
        public void setSenhaLogin( string prSenhaLogin)
        {
            AP_SenhaLogin = prSenhaLogin;
        }
        public void setNomeUsuario( string prNomeUsuario)
        {
            AP_NomeUsuario = prNomeUsuario;
        }
        public void setEmailUsuario( string prEmailUsuario)
        {
            AP_EmailUsuario = prEmailUsuario;
        }
        public void setCelularUsuario( string prCelularUsuario)
        {
            AP_CelularUsuario = prCelularUsuario;
        }
        public void setNivelAcesso( string prNivelAcesso)
        {
            AP_NivelAcesso = prNivelAcesso;
        }
    }
}

