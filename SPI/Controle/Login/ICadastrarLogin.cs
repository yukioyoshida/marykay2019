using System;
using System.Data;
using SPI;

public interface ICadastrarLogin
{
     void MS_Incluir(CTO_Login ITO_Login);
     void MS_Alterar(CTO_Login ITO_Login);
     void MS_Excluir(CTO_Login ITO_Login);
     CTO_Login MS_Obter(int pritObjRef);
     CTO_Login MS_Obter(string prstID);
     CTO_Login MS_Logar(string prstUsuario, string prstSenha);
     DataTable MS_ObterUsuarios();
}

