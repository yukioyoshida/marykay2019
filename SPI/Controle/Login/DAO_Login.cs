using System.Data; 
using SPI;
namespace SPI
{
    public class DAO_Login : FWK_DAO_Login
    {
        public DAO_Login()
        {
        }

        public void MS_IncluirReconhecimento(CTO_Reconhecimento ITO_Reconhecimento)
        {
            MI_SetSql("");

            MI_AddParameters("@cdUsuario", ITO_Reconhecimento.AP_cdUsuario);
            MI_AddParameters("@Consultora", ITO_Reconhecimento.AP_Consultora);
            MI_AddParameters("@cdRoteiro", ITO_Reconhecimento.AP_cdRoteiro);
            MI_AddParameters("@dsMetodoInsert", ITO_Reconhecimento.AP_dsMetodoInsert);
            MI_AddParameters("@cdInscricao", ITO_Reconhecimento.AP_cdInscricao);

            MI_ExecuteProcedure("SP_IncluirReconhecimento");
        }


        public DataTable MS_ObterUsuarios()
        {
            MI_SetSql("SELECT ObjRef as 'CODIGO',   " +
                      "NomeUsuario as 'Nome',       " +
                      "EmailUsuario as 'Email',     " +
                      "CelularUsuario as 'Celular', " +
                      "NomeLogin as 'Usuario',      " +
                      "SenhaLogin as 'Senha',       " +
                      "NivelAcesso as 'Acesso'      " +
                      "From tblUsuarios             " +
                      "order by NomeUsuario         ");
            return MI_ExecuteDataSet();
        }
        
        public CTO_Login MS_Logar(string prstUsuario, string prstSenha)
        {

            CTO_Login ITO_Login = new CTO_Login();
            MI_SetSql(AP_SelectALL +
                       "FROM tblUsuarios                " +
                       "WHERE NomeLogin = @Usuario and  " +
                       "SenhaLogin = @Senha             ");
            MI_AddParameters("@Usuario", prstUsuario);
            MI_AddParameters("@Senha", prstSenha);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Login = null;
            else
                ITO_Login = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Login;
        }

        
        public CTO_Login MS_Obter(string prstID)
        {
            CTO_Login ITO_Login = new CTO_Login();
            MI_SetSql (AP_SelectALL + 
                       "FROM tblUsuarios " + 
                       "WHERE ObjRef = @ID   ");
            MI_AddParameters("@ID", prstID);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Login = null;
            else
                ITO_Login = MI_DataSetToEntidade();
            
            MI_FecharConexao();
            return ITO_Login;
        }

    }
}

