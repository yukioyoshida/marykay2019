﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SPI
{
    public class CAT_Roteiro
    {
        public CAT_Roteiro()
        {
        }

        public CTO_Roteiro MS_ObterPorCodigoBarras(string stCodigoBarras)
        {
            CTO_Roteiro ITO_Roteiro = new CTO_Roteiro();

            DAO_Roteiro dadosRoteiro = new DAO_Roteiro();
            ITO_Roteiro = dadosRoteiro.MS_ObterPorCodigoBarras(stCodigoBarras);
            return ITO_Roteiro;
        }

        public List<CTO_Roteiro> MS_CarregarProdutos()
        {
            List<CTO_Roteiro> lITO_Roteiro = new List<CTO_Roteiro>();
            return lITO_Roteiro = new DAO_Roteiro().MS_CarregarProdutos();
        }

        public CTO_Roteiro MS_ObterRoteiroPorCodigo(int cdRoteiro)
        {
            CTO_Roteiro ITO_Roteiro = new CTO_Roteiro();
            return ITO_Roteiro = new DAO_Roteiro().MS_ObterRoteiroPorCodigo(cdRoteiro);

        }
    }
}
