﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SPI
{
    public class DAO_Roteiro : FWK_DAO_Roteiro
    {
        public DAO_Roteiro()
        {
        }

        public CTO_Roteiro MS_ObterPorCodigoBarras(string stCodigoBarras)
        {
            CTO_Roteiro ITO_Roteiro = new CTO_Roteiro();

            MI_SetSql("SELECT CDROTEIRO, DSRECONHECIMENTO, DSLOCAL, DSNOMECREDENCIAL, IMGBINARY, DSCODIGOBARRASPRODUTO, DSQUANTIDADEESTOQUE FROM TBLROTEIRO WHERE DSCODIGOBARRASPRODUTO = @DSCODIGOBARRASPRODUTO AND DSSITUACAO = 'ATIVO'");
            MI_AddParameters("@DSCODIGOBARRASPRODUTO", stCodigoBarras);
            MI_ExecuteQuery();

            if (MI_EOF())
                ITO_Roteiro = null;
            else
                ITO_Roteiro = MI_DataSetToEntidade();

            MI_FecharConexao();

            return ITO_Roteiro;
        }

        public List<CTO_Roteiro> MS_CarregarProdutos()
        {
            List<CTO_Roteiro> lITO_Roteiro = new List<CTO_Roteiro>();

            string strQuery = "SELECT CDROTEIRO, DSRECONHECIMENTO FROM TBLROTEIRO WHERE DSSITUACAO = 'ATIVO' AND TIPOJOIA <> 'FAIXA' AND ISNULL(RTRIM(LTRIM(IMGBINARY)), '')<>'' AND inTemControleEstoque = 1";
            var cnn = AP_Connection.Query<CTO_Roteiro>(strQuery);

            foreach(CTO_Roteiro roteiro in cnn)
            {
                lITO_Roteiro.Add(roteiro);
            }
            return lITO_Roteiro;
        }


        public CTO_Roteiro MS_ObterRoteiroPorCodigo(int cdRoteiro)
        {
            CTO_Roteiro ITO_Roteiro = new CTO_Roteiro();
            MI_SetSql("SELECT CDROTEIRO, DSRECONHECIMENTO, DSLOCAL, DSNOMECREDENCIAL, DSCODIGOBARRASPRODUTO, DSQUANTIDADEESTOQUE FROM TBLROTEIRO WHERE CDROTEIRO = @CDROTEIRO AND DSSITUACAO = 'ATIVO'");
            //var cnn = AP_Connection.Query<CTO_Roteiro>(strQuery, new { @CDROTEIRO = cdRoteiro });

            MI_AddParameters("@CDROTEIRO", cdRoteiro);
            MI_ExecuteQuery();

            if (MI_EOF())
                ITO_Roteiro = null;
            else
                ITO_Roteiro = MI_DataSetToEntidade();

            MI_FecharConexao();

            return ITO_Roteiro;

            //var cnn = AP_Connection.Query<CTO_Roteiro>(strQuery, new { @CDROTEIRO = cdRoteiro });
            //ITO_Roteiro = (CTO_Roteiro)cnn;
            //return ITO_Roteiro;
        }
    }
}
