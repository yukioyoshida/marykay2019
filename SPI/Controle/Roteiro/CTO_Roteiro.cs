﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SPI
{
    public class CTO_Roteiro
    {
        public int cdRoteiro { get; set; }
        public string dsReconhecimento { get; set; }
        public string dsLocal { get; set; }         
        public string dsNomeCredencial { get; set; }
        public string dsSituacao { get; set; }
        public DateTime dtCancelamento { get; set; }
        public string cdUsuarioCancelamento { get; set; }
        public string imgBinary { get; set; }
        public string TipoRoteiro { get; set; }
        public string TipoJoia { get; set; }
        public string Dia { get; set; }
        public string dsCodigoBarrasProduto { get; set; }
        public int dsQuantidadeEstoque { get; set; }
    }                                     
}
