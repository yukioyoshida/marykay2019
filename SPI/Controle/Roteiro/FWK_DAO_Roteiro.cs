﻿using System;
namespace SPI
{
    public class FWK_DAO_Roteiro : FWK_DAO_MySQL
    {
        public FWK_DAO_Roteiro()
        {
        }

        public CTO_Roteiro MI_DataSetToEntidade()
        {
            CTO_Roteiro ITO_Roteiro = new CTO_Roteiro();

            ITO_Roteiro.cdRoteiro = AP_DataReader["CDROTEIRO"] == null ? 0 :  Convert.ToInt32(AP_DataReader["CDROTEIRO"]);
            ITO_Roteiro.dsReconhecimento = AP_DataReader["DSRECONHECIMENTO"] == null ? String.Empty : AP_DataReader["DSRECONHECIMENTO"].ToString();
            ITO_Roteiro.dsLocal = AP_DataReader["DSLOCAL"] == null ? String.Empty : AP_DataReader["DSLOCAL"].ToString();
            ITO_Roteiro.dsNomeCredencial= AP_DataReader["DSNOMECREDENCIAL"] == null ? String.Empty : AP_DataReader["DSNOMECREDENCIAL"].ToString();
            //ITO_Roteiro.imgBinary = AP_DataReader["IMGBINARY"] == null ? String.Empty : AP_DataReader["IMGBINARY"].ToString();
            ITO_Roteiro.dsCodigoBarrasProduto = AP_DataReader["DSCODIGOBARRASPRODUTO"] == null ? String.Empty : AP_DataReader["DSCODIGOBARRASPRODUTO"].ToString();
            ITO_Roteiro.dsQuantidadeEstoque = AP_DataReader["DSQUANTIDADEESTOQUE"] == DBNull.Value ? 0 : Convert.ToInt32(AP_DataReader["DSQUANTIDADEESTOQUE"]);


            return ITO_Roteiro;
        }
    }

}
