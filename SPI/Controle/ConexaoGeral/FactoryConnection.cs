using System.Data.SqlClient;
using System.Data;
namespace SPI
{
   public class FactoryConnection
    {
        public SqlConnection getConnection()
        {

            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));

            dt.ReadXml("configuracoes.xml");
            dr = dt.Rows[0];


            //string connectionString = "Server=" + dr.ItemArray[0] + "; Database=" + dr.ItemArray[3] + "; UID=" + dr.ItemArray[1] + ";PWD=" + dr.ItemArray[2] + "";
            string connectionString = "Server=" + dr.ItemArray[0] + "; Database=" + dr.ItemArray[3] + "; UID=" + dr.ItemArray[1] + ";PWD=" + dr.ItemArray[2] + "";
            return new SqlConnection(connectionString);
        }
        //public MySqlConnection getMySqlConnection()
        //{
        //    DataTable dt = new DataTable("CONEXAO");
        //    DataRow dr;

        //    dt.Columns.Add("IP", typeof(string));
        //    dt.Columns.Add("USUARIO", typeof(string));
        //    dt.Columns.Add("SENHA", typeof(string));
        //    dt.Columns.Add("BANCO", typeof(string));

        //    dt.ReadXml("configuracoes.xml");
        //    dr = dt.Rows[0];
            
        //    //string connectionString = "Server="+dr.ItemArray[0]+"; Database="+dr.ItemArray[3]+"; UID="+dr.ItemArray[1]+";PWD="+dr.ItemArray[2]+";Pooling=false;";
        //    string connectionString = "Server = " + dr.ItemArray[0] + "; Port = 3306; Database = " + dr.ItemArray[3] + "; Uid = " + dr.ItemArray[1] + "; Pwd = " + dr.ItemArray[2] + ";";
            

        //    return new MySqlConnection(connectionString);
        //}

    }
}
