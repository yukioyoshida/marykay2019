using System.Data;
namespace ControleRemoto
{
    public class FWK_DAO_Terminal : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                           "          ObjRef, " +
                           "          Terminal, " +
                           "          Status, " +
                           "          Direcao ";

        public FWK_DAO_Terminal()
        {
        }

        public void MS_Incluir(CTO_Terminal ITO_Terminal)
        {
            int itObjRef = MS_NovoObjRef();
            MI_SetSql ("INSERT INTO tblTerminal (" +
                       "          ObjRef, " +
                       "          Terminal, " +
                       "          Status, " +
                       "          Direcao) " + 
                       " VALUES (" + 
                       "          @ObjRef, " +
                       "          @Terminal, " +
                       "          @Status, " +
                       "          @Direcao) ");

            MI_AddParameters("@ObjRef", itObjRef);
            MI_AddParameters("@Terminal", ITO_Terminal.getTerminal());
            MI_AddParameters("@Status", ITO_Terminal.getStatus());
            MI_AddParameters("@Direcao", ITO_Terminal.getDirecao());

            MI_ExecuteNonQuery();
        }

        public void MS_Alterar(CTO_Terminal ITO_Terminal)
        {
            MI_SetSql ("UPDATE tblTerminal SET " +
                       "          ObjRef = @ObjRef, " +
                       "          Terminal = @Terminal, " +
                       "          Status = @Status, " +
                       "          Direcao = @Direcao " + 
                       "WHERE ObjRef = @ObjRef ");
            MI_AddParameters("@ObjRef", ITO_Terminal.getObjRef());
            MI_AddParameters("@Terminal", ITO_Terminal.getTerminal());
            MI_AddParameters("@Status", ITO_Terminal.getStatus());
            MI_AddParameters("@Direcao", ITO_Terminal.getDirecao());

            MI_ExecuteNonQuery();
        }

        public void MS_Excluir(CTO_Terminal ITO_Terminal)
        {
            MI_SetSql ("DELETE FROM tblTerminal WHERE ObjRef = @ObjRef");
            MI_AddParameters("@ObjRef", ITO_Terminal.getObjRef());
            
            MI_ExecuteNonQuery();
        }

        public CTO_Terminal MS_Obter(int pritObjRef)
        {
            CTO_Terminal ITO_Terminal = new CTO_Terminal();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          Terminal, " +
                       "          Status, " +
                       "          Direcao " + 
                       "FROM tblTerminal " + 
                       "WHERE Objref = @ObjRef ");
            MI_AddParameters("@Objref", pritObjRef);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Terminal = null;
            else
                ITO_Terminal = MI_DataSetToEntidade();
            return ITO_Terminal;
        }

        public CTO_Terminal MI_DataSetToEntidade()
        {
            CTO_Terminal ITO_Terminal = new CTO_Terminal();            
            try
            {
                ITO_Terminal.setObjRef(AP_DataReader.GetInt32(0));
            }
            catch
            {
                ITO_Terminal.setObjRef(0);
            }
            try
            {
                ITO_Terminal.setTerminal(AP_DataReader.GetString(1));
            }
            catch
            {
                ITO_Terminal.setTerminal("");
            }
            try
            {
                ITO_Terminal.setStatus(AP_DataReader.GetString(2));
            }
            catch
            {
                ITO_Terminal.setStatus("");
            }
            try
            {
                ITO_Terminal.setDirecao(AP_DataReader.GetString(3));
            }
            catch
            {
                ITO_Terminal.setDirecao("");
            }
            return ITO_Terminal;
        }

        public CTO_Terminal MS_ObterTodos()
        {
            CTO_Terminal ITO_Terminal = new CTO_Terminal();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          Terminal, " +
                       "          Status, " +
                       "          Direcao " + 
                       "FROM tblTerminal " );
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Terminal = null;
            else
                ITO_Terminal = MI_DataSetToEntidade();
            return ITO_Terminal;
        }

        public CTO_Terminal MS_ObterProximo()
        {
            CTO_Terminal ITO_Terminal = new CTO_Terminal();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_Terminal = null;
            else
                ITO_Terminal = MI_DataSetToEntidade();
                return ITO_Terminal;
        }

        public int MS_NovoObjRef()
        {
            int itObjRef;
            MI_SetSql("Select Max(ObjRef) from tblTerminal");
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 1;
            }
            else
            {
                itObjRef = AP_DataReader.GetInt32(0);
                itObjRef++;
            }
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblTerminal");
            return MI_ExecuteDataSet();
        }

    }
}

