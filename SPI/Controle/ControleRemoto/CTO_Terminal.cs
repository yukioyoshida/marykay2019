using System;
namespace ControleRemoto 
{

    public class CTO_Terminal 
    {
          //  Declaracao dos atributos Puros
        int AP_ObjRef;
        string AP_Terminal;
        string AP_Status;
        string AP_Direcao;

        public CTO_Terminal () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public int getObjRef()
        {
            return AP_ObjRef;
        }
        public string getTerminal()
        {
            return AP_Terminal;
        }
        public string getStatus()
        {
            return AP_Status;
        }
        public string getDirecao()
        {
            return AP_Direcao;
        }
         //  Declaracao dos Metodos set
        public void setObjRef( int prObjRef)
        {
            AP_ObjRef = prObjRef;
        }
        public void setTerminal( string prTerminal)
        {
            AP_Terminal = prTerminal;
        }
        public void setStatus( string prStatus)
        {
            AP_Status = prStatus;
        }
        public void setDirecao( string prDirecao)
        {
            AP_Direcao = prDirecao;
        }
    }
}

