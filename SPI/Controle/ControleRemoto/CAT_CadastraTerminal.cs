using System;
using ControleRemoto;

public class CAT_CadastrarTerminal : CAB_CadastrarTerminal
{
     public CAT_CadastrarTerminal()
     {
     }
     public override void MS_Incluir(CTO_Terminal ITO_Terminal)
     {
          DAO_Terminal IDAO_Terminal = new DAO_Terminal();
          IDAO_Terminal.MS_Incluir(ITO_Terminal);
     }
     public override void MS_Alterar(CTO_Terminal ITO_Terminal)
     {
         DAO_Terminal IDAO_Terminal = new DAO_Terminal();
         IDAO_Terminal.MS_Alterar(ITO_Terminal);
     }
     public override void MS_Excluir(CTO_Terminal ITO_Terminal)
     {
         DAO_Terminal IDAO_Terminal = new DAO_Terminal();
         IDAO_Terminal.MS_Excluir(ITO_Terminal);
     }
     public override CTO_Terminal MS_Obter(int pritObjRef)
     {
           DAO_Terminal IDAO_Terminal = new DAO_Terminal();
           return IDAO_Terminal.MS_Obter(pritObjRef);
     }
     public override CTO_Terminal MS_Obter(string prstID)
     {
          DAO_Terminal IDAO_Terminal = new DAO_Terminal();
          return IDAO_Terminal.MS_Obter(prstID);
     }
    
    public override bool MS_VerificaStatus(string stNumeroTerminal)
    {
        DAO_Terminal IDAO_Terminal = new DAO_Terminal();
        return IDAO_Terminal.MS_VerificaStatus(stNumeroTerminal);
    }
    public override void MS_AlterarStatusTerminal(string stNumeroTerminal)
    {
        DAO_Terminal IDAO_Terminal = new DAO_Terminal();
        IDAO_Terminal.MS_AlterarStatusTerminal(stNumeroTerminal);
    }

    public void MS_ExcluirTerminal(string stNumeroTerminal)
    {
        DAO_Terminal IDAO_Terminal = new DAO_Terminal();
        IDAO_Terminal.MS_ExcluirTerminal(stNumeroTerminal);
    }

}

