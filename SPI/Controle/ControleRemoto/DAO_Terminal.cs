namespace ControleRemoto
{
    public class DAO_Terminal : FWK_DAO_Terminal
    {
        public DAO_Terminal()
        {
        }

        public CTO_Terminal MS_Obter(string prstID)
        {
            CTO_Terminal ITO_Terminal = new CTO_Terminal();
            MI_SetSql (AP_SelectALL + 
                       "FROM tblTerminal " + 
                       "WHERE ID = @ID ");
            MI_AddParameters("@ID", prstID);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Terminal = null;
            else
                ITO_Terminal = MI_DataSetToEntidade();
            return ITO_Terminal;
        }

        public void MS_AlterarStatusTerminal(string stNumeroTerminal)
        {
            MI_SetSql("Update tblTerminal set Status = @Status2  " +
                      "Where Terminal = @Terminal and  " +
                      "Status = @Status                ");
            MI_AddParameters("@Terminal", stNumeroTerminal);
            MI_AddParameters("@Status", "C");
            MI_AddParameters("@Status2", "D");
            MI_ExecuteNonQuery(); 
        }

        public bool MS_VerificaStatus(string stNumeroTerminal)
        {
            bool boResult = true;
            MI_SetSql(AP_SelectALL +
                       "FROM tblTerminal                " +
                       "Where Terminal = @Terminal and  " +
                       "Status = @Status or             " +
                       "Status = @Status2               ");
            MI_AddParameters("@Terminal", stNumeroTerminal);
            MI_AddParameters("@Status", "C");
            MI_AddParameters("@Status2", "D");
            MI_ExecuteQuery();
            if (MI_EOF())
                boResult = true;
            else
            {
                boResult = false;
            }
            return boResult;
        }

        public void MS_ExcluirTerminal(string stNumeroTerminal)
        {
            MI_SetSql("Delete from tblTerminal Where " +
                      "Terminal = @ID                ");
            MI_AddParameters("@ID", stNumeroTerminal);
            MI_ExecuteNonQuery();
        }
    }
}

