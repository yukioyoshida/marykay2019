using System;
using ControleRemoto;

public abstract class CAB_CadastrarTerminal : ICadastrarTerminal
{
    public abstract void MS_Incluir(CTO_Terminal ITO_Terminal);
    public abstract void MS_Alterar(CTO_Terminal ITO_Terminal);
    public abstract void MS_Excluir(CTO_Terminal ITO_Terminal);
    public abstract CTO_Terminal MS_Obter(int pritObjRef);
    public abstract CTO_Terminal MS_Obter(string prstID);
    public abstract bool MS_VerificaStatus(string stNumeroTerminal);
    public abstract void MS_AlterarStatusTerminal(string stNumeroTerminal);
}

