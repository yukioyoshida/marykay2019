using System;
using ControleRemoto;

public interface ICadastrarTerminal
{
    void MS_Incluir(CTO_Terminal ITO_Terminal);
    void MS_Alterar(CTO_Terminal ITO_Terminal);
    void MS_Excluir(CTO_Terminal ITO_Terminal);
    CTO_Terminal MS_Obter(int pritObjRef);
    CTO_Terminal MS_Obter(string prstID);
    bool MS_VerificaStatus(string stNumeroTerminal);
    void MS_AlterarStatusTerminal(string stNumeroTerminal);
}

