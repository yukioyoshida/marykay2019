using System.Data;
namespace SPI
{
    public class FWK_DAO_Pedido : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                           "          cdPedido, " +
                           "          cdCliente, " +
                           "          dtPedido, " +
                           "          dsSituacao, " +
                           "          dsFormaPagamento, " +
                           "          dsValorTotal, " +
                           "          dsValorDesconto, " +
                           "          dsTipoPessoa, " +
                           "          dsCpfCnpj, " +
                           "          dsNomeFaturamento, " +
                           "          dsEnderecoFaturamento, " +
                           "          dsCepFaturamento, " +
                           "          dsComplementoFaturamento, " +
                           "          dsBairroFaturamento, " +
                           "          dsCidadeFaturamento, " +
                           "          dsEstadoFaturamento, " +
                           "          dsNumeroFaturamento, " +
                           "          nmCongressista, " +
                           "          dtVencimento, " +
                           "          dtAlteracao, " +
                           "          dsAlteracao, " +
                           "          dsObservacao ";

        public FWK_DAO_Pedido()
        {
        }

        public void MS_Incluir(CTO_Pedido ITO_Pedido)
        {
            int itObjRef = MS_NovoObjRef();
            MI_SetSql ("INSERT INTO tblPedido (" +
                       "          cdPedido, " +
                       "          cdCliente, " +
                       "          dtPedido, " +
                       "          dsSituacao, " +
                       "          dsFormaPagamento, " +
                       "          dsValorTotal, " +
                       "          dsValorDesconto, " +
                       "          dsTipoPessoa, " +
                       "          dsCpfCnpj, " +
                       "          dsNomeFaturamento, " +
                       "          dsEnderecoFaturamento, " +
                       "          dsCepFaturamento, " +
                       "          dsComplementoFaturamento, " +
                       "          dsBairroFaturamento, " +
                       "          dsCidadeFaturamento, " +
                       "          dsEstadoFaturamento, " +
                       "          dsNumeroFaturamento, " +
                       "          nmCongressista, " +
                       "          dtVencimento, " +
                       "          dtAlteracao, " +
                       "          dsAlteracao, " +
                       "          dsObservacao) " + 
                       " VALUES (" + 
                       "          @cdPedido, " +
                       "          @cdCliente, " +
                       "          @dtPedido, " +
                       "          @dsSituacao, " +
                       "          @dsFormaPagamento, " +
                       "          @dsValorTotal, " +
                       "          @dsValorDesconto, " +
                       "          @dsTipoPessoa, " +
                       "          @dsCpfCnpj, " +
                       "          @dsNomeFaturamento, " +
                       "          @dsEnderecoFaturamento, " +
                       "          @dsCepFaturamento, " +
                       "          @dsComplementoFaturamento, " +
                       "          @dsBairroFaturamento, " +
                       "          @dsCidadeFaturamento, " +
                       "          @dsEstadoFaturamento, " +
                       "          @dsNumeroFaturamento, " +
                       "          @nmCongressista, " +
                       "          @dtVencimento, " +
                       "          @dtAlteracao, " +
                       "          @dsAlteracao, " +
                       "          @dsObservacao) ");

            MI_AddParameters("@cdPedido", ITO_Pedido.getcdPedido());
            MI_AddParameters("@cdCliente", ITO_Pedido.getcdCliente());
            MI_AddParameters("@dtPedido", ITO_Pedido.getdtPedido());
            MI_AddParameters("@dsSituacao", ITO_Pedido.getdsSituacao());
            MI_AddParameters("@dsFormaPagamento", ITO_Pedido.getdsFormaPagamento());
            MI_AddParameters("@dsValorTotal", ITO_Pedido.getdsValorTotal());
            MI_AddParameters("@dsValorDesconto", ITO_Pedido.getdsValorDesconto());
            MI_AddParameters("@dsTipoPessoa", ITO_Pedido.getdsTipoPessoa());
            MI_AddParameters("@dsCpfCnpj", ITO_Pedido.getdsCpfCnpj());
            MI_AddParameters("@dsNomeFaturamento", ITO_Pedido.getdsNomeFaturamento());
            MI_AddParameters("@dsEnderecoFaturamento", ITO_Pedido.getdsEnderecoFaturamento());
            MI_AddParameters("@dsCepFaturamento", ITO_Pedido.getdsCepFaturamento());
            MI_AddParameters("@dsComplementoFaturamento", ITO_Pedido.getdsComplementoFaturamento());
            MI_AddParameters("@dsBairroFaturamento", ITO_Pedido.getdsBairroFaturamento());
            MI_AddParameters("@dsCidadeFaturamento", ITO_Pedido.getdsCidadeFaturamento());
            MI_AddParameters("@dsEstadoFaturamento", ITO_Pedido.getdsEstadoFaturamento());
            MI_AddParameters("@dsNumeroFaturamento", ITO_Pedido.getdsNumeroFaturamento());
            MI_AddParameters("@nmCongressista", ITO_Pedido.getnmCongressista());
            MI_AddParameters("@dtVencimento", ITO_Pedido.getdtVencimento());
            MI_AddParameters("@dtAlteracao", ITO_Pedido.getdtAlteracao());
            MI_AddParameters("@dsAlteracao", ITO_Pedido.getdsAlteracao());
            MI_AddParameters("@dsObservacao", ITO_Pedido.getdsObservacao());
            
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_Alterar(CTO_Pedido ITO_Pedido)
        {
            MI_SetSql ("UPDATE tblPedido SET " +
                       "          cdPedido = @cdPedido, " +
                       "          cdCliente = @cdCliente, " +
                       "          dtPedido = @dtPedido, " +
                       "          dsSituacao = @dsSituacao, " +
                       "          dsFormaPagamento = @dsFormaPagamento, " +
                       "          dsValorTotal = @dsValorTotal, " +
                       "          dsValorDesconto = @dsValorDesconto, " +
                       "          dsTipoPessoa = @dsTipoPessoa, " +
                       "          dsCpfCnpj = @dsCpfCnpj, " +
                       "          dsNomeFaturamento = @dsNomeFaturamento, " +
                       "          dsEnderecoFaturamento = @dsEnderecoFaturamento, " +
                       "          dsCepFaturamento = @dsCepFaturamento, " +
                       "          dsComplementoFaturamento = @dsComplementoFaturamento, " +
                       "          dsBairroFaturamento = @dsBairroFaturamento, " +
                       "          dsCidadeFaturamento = @dsCidadeFaturamento, " +
                       "          dsEstadoFaturamento = @dsEstadoFaturamento, " +
                       "          dsNumeroFaturamento = @dsNumeroFaturamento, " +
                       "          nmCongressista = @nmCongressista, " +
                       "          dtVencimento = @dtVencimento, " +
                       "          dtAlteracao = @dtAlteracao, " +
                       "          dsAlteracao = @dsAlteracao, " +
                       "          dsObservacao = @dsObservacao " +
                       "WHERE cdPedido = @cdPedido ");
            MI_AddParameters("@cdPedido", ITO_Pedido.getcdPedido());
            MI_AddParameters("@cdCliente", ITO_Pedido.getcdCliente());
            MI_AddParameters("@dtPedido", ITO_Pedido.getdtPedido());
            MI_AddParameters("@dsSituacao", ITO_Pedido.getdsSituacao());
            MI_AddParameters("@dsFormaPagamento", ITO_Pedido.getdsFormaPagamento());
            MI_AddParameters("@dsValorTotal", ITO_Pedido.getdsValorTotal());
            MI_AddParameters("@dsValorDesconto", ITO_Pedido.getdsValorDesconto());
            MI_AddParameters("@dsTipoPessoa", ITO_Pedido.getdsTipoPessoa());
            MI_AddParameters("@dsCpfCnpj", ITO_Pedido.getdsCpfCnpj());
            MI_AddParameters("@dsNomeFaturamento", ITO_Pedido.getdsNomeFaturamento());
            MI_AddParameters("@dsEnderecoFaturamento", ITO_Pedido.getdsEnderecoFaturamento());
            MI_AddParameters("@dsCepFaturamento", ITO_Pedido.getdsCepFaturamento());
            MI_AddParameters("@dsComplementoFaturamento", ITO_Pedido.getdsComplementoFaturamento());
            MI_AddParameters("@dsBairroFaturamento", ITO_Pedido.getdsBairroFaturamento());
            MI_AddParameters("@dsCidadeFaturamento", ITO_Pedido.getdsCidadeFaturamento());
            MI_AddParameters("@dsEstadoFaturamento", ITO_Pedido.getdsEstadoFaturamento());
            MI_AddParameters("@dsNumeroFaturamento", ITO_Pedido.getdsNumeroFaturamento());
            MI_AddParameters("@nmCongressista", ITO_Pedido.getnmCongressista());
            MI_AddParameters("@dtVencimento", ITO_Pedido.getdtVencimento());
            MI_AddParameters("@dtAlteracao", ITO_Pedido.getdtAlteracao());
            MI_AddParameters("@dsAlteracao", ITO_Pedido.getdsAlteracao());
            MI_AddParameters("@dsObservacao", ITO_Pedido.getdsObservacao());
            
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_Excluir(CTO_Pedido ITO_Pedido)
        {
            MI_SetSql("DELETE FROM tblPedido WHERE cdPedido = @ObjRef");
            MI_AddParameters("@ObjRef", ITO_Pedido.getcdPedido());
            
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public CTO_Pedido MS_Obter(int pritObjRef)
        {
            CTO_Pedido ITO_Pedido = new CTO_Pedido();
            MI_SetSql ("SELECT " + 
                       "          cdPedido, " +
                       "          cdCliente, " +
                       "          dtPedido, " +
                       "          dsSituacao, " +
                       "          dsFormaPagamento, " +
                       "          dsValorTotal, " +
                       "          dsValorDesconto, " +
                       "          dsTipoPessoa, " +
                       "          dsCpfCnpj, " +
                       "          dsNomeFaturamento, " +
                       "          dsEnderecoFaturamento, " +
                       "          dsCepFaturamento, " +
                       "          dsComplementoFaturamento, " +
                       "          dsBairroFaturamento, " +
                       "          dsCidadeFaturamento, " +
                       "          dsEstadoFaturamento, " +
                       "          dsNumeroFaturamento, " +
                       "          nmCongressista, " +
                       "          dtVencimento, " +
                       "          dtAlteracao, " +
                       "          dsAlteracao, " +
                       "          dsObservacao " + 
                       "FROM tblPedido " +
                       "WHERE cdPedido = @ObjRef ");
            MI_AddParameters("@Objref", pritObjRef);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Pedido = null;
            else
                ITO_Pedido = MI_DataSetToEntidade();
            return ITO_Pedido;
        }

        public CTO_Pedido MI_DataSetToEntidade()
        {
            CTO_Pedido ITO_Pedido = new CTO_Pedido();            
            try
            {
                ITO_Pedido.setcdPedido(AP_DataReader.GetInt32(0));
            }
            catch
            {
                ITO_Pedido.setcdPedido(0);
            }
            try
            {
                ITO_Pedido.setcdCliente(AP_DataReader.GetInt32(1));
            }
            catch
            {
                ITO_Pedido.setcdCliente(0);
            }
            try
            {
            ITO_Pedido.setdtPedido(AP_DataReader.GetDateTime(2));
            }
            catch
            {
            }
            try
            {
                ITO_Pedido.setdsSituacao(AP_DataReader.GetString(3));
            }
            catch
            {
                ITO_Pedido.setdsSituacao("");
            }
            try
            {
                ITO_Pedido.setdsFormaPagamento(AP_DataReader.GetString(4));
            }
            catch
            {
                ITO_Pedido.setdsFormaPagamento("");
            }
            try
            {
                ITO_Pedido.setdsValorTotal(AP_DataReader.GetString(5));
            }
            catch
            {
                ITO_Pedido.setdsValorTotal("");
            }
            try
            {
                ITO_Pedido.setdsValorDesconto(AP_DataReader.GetString(6));
            }
            catch
            {
                ITO_Pedido.setdsValorDesconto("");
            }
            try
            {
                ITO_Pedido.setdsTipoPessoa(AP_DataReader.GetString(7));
            }
            catch
            {
                ITO_Pedido.setdsTipoPessoa("");
            }
            try
            {
                ITO_Pedido.setdsCpfCnpj(AP_DataReader.GetString(8));
            }
            catch
            {
                ITO_Pedido.setdsCpfCnpj("");
            }
            try
            {
                ITO_Pedido.setdsNomeFaturamento(AP_DataReader.GetString(9));
            }
            catch
            {
                ITO_Pedido.setdsNomeFaturamento("");
            }
            try
            {
                ITO_Pedido.setdsEnderecoFaturamento(AP_DataReader.GetString(10));
            }
            catch
            {
                ITO_Pedido.setdsEnderecoFaturamento("");
            }
            try
            {
                ITO_Pedido.setdsCepFaturamento(AP_DataReader.GetString(11));
            }
            catch
            {
                ITO_Pedido.setdsCepFaturamento("");
            }
            try
            {
                ITO_Pedido.setdsComplementoFaturamento(AP_DataReader.GetString(12));
            }
            catch
            {
                ITO_Pedido.setdsComplementoFaturamento("");
            }
            try
            {
                ITO_Pedido.setdsBairroFaturamento(AP_DataReader.GetString(13));
            }
            catch
            {
                ITO_Pedido.setdsBairroFaturamento("");
            }
            try
            {
                ITO_Pedido.setdsCidadeFaturamento(AP_DataReader.GetString(14));
            }
            catch
            {
                ITO_Pedido.setdsCidadeFaturamento("");
            }
            try
            {
                ITO_Pedido.setdsEstadoFaturamento(AP_DataReader.GetString(15));
            }
            catch
            {
                ITO_Pedido.setdsEstadoFaturamento("");
            }
            try
            {
                ITO_Pedido.setdsNumeroFaturamento(AP_DataReader.GetString(16));
            }
            catch
            {
                ITO_Pedido.setdsNumeroFaturamento("");
            }
            try
            {
                ITO_Pedido.setnmCongressista(AP_DataReader.GetString(17));
            }
            catch
            {
                ITO_Pedido.setnmCongressista("");
            }
            try
            {
            ITO_Pedido.setdtVencimento(AP_DataReader.GetDateTime(18));
            }
            catch
            {
            }
            try
            {
            ITO_Pedido.setdtAlteracao(AP_DataReader.GetDateTime(19));
            }
            catch
            {
            }
            try
            {
                ITO_Pedido.setdsAlteracao(AP_DataReader.GetString(20));
            }
            catch
            {
                ITO_Pedido.setdsAlteracao("");
            }
            try
            {
                ITO_Pedido.setdsObservacao(AP_DataReader.GetString(21));
            }
            catch
            {
                ITO_Pedido.setdsObservacao("");
            }
            return ITO_Pedido;
        }

        public CTO_Pedido MS_ObterTodos()
        {
            CTO_Pedido ITO_Pedido = new CTO_Pedido();
            MI_SetSql ("SELECT " + 
                       "          cdPedido, " +
                       "          cdCliente, " +
                       "          dtPedido, " +
                       "          dsSituacao, " +
                       "          dsFormaPagamento, " +
                       "          dsValorTotal, " +
                       "          dsValorDesconto, " +
                       "          dsTipoPessoa, " +
                       "          dsCpfCnpj, " +
                       "          dsNomeFaturamento, " +
                       "          dsEnderecoFaturamento, " +
                       "          dsCepFaturamento, " +
                       "          dsComplementoFaturamento, " +
                       "          dsBairroFaturamento, " +
                       "          dsCidadeFaturamento, " +
                       "          dsEstadoFaturamento, " +
                       "          dsNumeroFaturamento, " +
                       "          nmCongressista, " +
                       "          dtVencimento, " +
                       "          dtAlteracao, " +
                       "          dsAlteracao, " +
                       "          dsObservacao " + 
                       "FROM tblPedido " );
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Pedido = null;
            else
                ITO_Pedido = MI_DataSetToEntidade();
            return ITO_Pedido;
        }

        public CTO_Pedido MS_ObterProximo()
        {
            CTO_Pedido ITO_Pedido = new CTO_Pedido();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_Pedido = null;
            else
                ITO_Pedido = MI_DataSetToEntidade();
                return ITO_Pedido;
        }

        public int MS_NovoObjRef()
        {
            int itObjRef;
            MI_SetSql("Select Max(cdPedido) from tblPedido");
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 800001;
            }
            else
            {
                itObjRef = AP_DataReader.GetInt32(0);
                itObjRef++;
            }

            MI_FecharConexao();
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblPedido");
            return MI_ExecuteDataSet();
        }

    }
}

