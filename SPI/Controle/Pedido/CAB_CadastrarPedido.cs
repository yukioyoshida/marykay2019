using System;
using System.Data;
using SPI;

public abstract class CAB_CadastrarPedido : ICadastrarPedido
{
     public abstract void MS_Incluir(CTO_Pedido ITO_Pedido);
     public abstract void MS_Alterar(CTO_Pedido ITO_Pedido);
     public abstract void MS_Excluir(CTO_Pedido ITO_Pedido);
     public abstract CTO_Pedido MS_Obter(int pritObjRef);
     public abstract CTO_Pedido MS_Obter(string prstID);
     public abstract int MS_NovoObjRef();
     public abstract DataTable MI_ObterItensComprados(int idCliente);
     public abstract DataTable MI_ObterTodosPedidos();
     public abstract DataTable MI_ObterPedidos();
}

