using System;
using System.Data;
using SPI;

public interface ICadastrarPedido
{
     void MS_Incluir(CTO_Pedido ITO_Pedido);
     void MS_Alterar(CTO_Pedido ITO_Pedido);
     void MS_Excluir(CTO_Pedido ITO_Pedido);
     CTO_Pedido MS_Obter(int pritObjRef);
     CTO_Pedido MS_Obter(string prstID);
     int MS_NovoObjRef();
     DataTable MI_ObterItensComprados(int idCliente);
     DataTable MI_ObterTodosPedidos();
     DataTable MI_ObterPedidos();
}

