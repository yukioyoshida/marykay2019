using SPI;
using System;
using System.Data;
namespace SPI
{
    public class DAO_Pedido : FWK_DAO_Pedido
    {
        public DAO_Pedido()
        {
        }

        public CTO_Pedido MS_Obter(string prstID)
        {
            CTO_Pedido ITO_Pedido = new CTO_Pedido();
            MI_SetSql (AP_SelectALL + 
                       "FROM tblPedido " + 
                       "WHERE ID = @ID ");
            MI_AddParameters("@ID", prstID);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Pedido = null;
            else
                ITO_Pedido = MI_DataSetToEntidade();
            return ITO_Pedido;
        }

        public DataTable MI_ObterItensComprados(int idCliente)
        {
            MI_SetSql(" SELECT B.*                                  " +
                      "  FROM tblpedido AS A, tblItensPedido AS B   " +
                      "  WHERE                                      " +
	                  "  A.CDCLIENTE = @IDCLIENTE                   " +
                      "  AND                                        " +
	                  "  A.CDPEDIDO = B.CDPEDIDO                    " +
                      "  AND                                        " +
                      "  A.DSSITUACAO <> 'CANCELADO'                ");

            MI_AddParameters("@IDCLIENTE", idCliente);

            return MI_ExecuteDataSet();
        }

        public DataTable MI_ObterTodosPedidos()
        {
            MI_SetSql(AP_SelectALL +
                " FROM tblpedido ");

            return MI_ExecuteDataSet();
        }

        public DataTable MI_ObterPedidos()
        {
            MI_SetSql(" SELECT A.CDPEDIDO, A.DTPEDIDO,  A.DSSITUACAO, A.DSVALORDESCONTO,    " +
                      " A.DTVENCIMENTO, B.NomeCompleto,                                     " +
                      " concat(B.DDDTelefonePessoal, B.NUMEROTELEFONEPESSOAL) AS TELEFONE   " +
                      " FROM tblpedido AS A, tblvisitante AS B                              " +
                      " WHERE                                                               " +
                      " A.DSSITUACAO <> 'CANCELADO'                                         " +
                      " AND A.CDCLIENTE = B.OBJREF                                          ");

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterPedidosGrid(string stPedido)
        {
            MI_SetSql(  "SELECT tblproduto.dsproduto AS 'Itens'                   " + 
                        "FROM tblproduto, tblitenspedido, tblpedido                 "+
                        "WHERE tblproduto.cdproduto = tblitenspedido.cdproduto AND  "+
                        "tblpedido.cdpedido = tblitenspedido.cdpedido and           "+
                        "tblpedido.cdpedido = @Pedido                               ");
            MI_AddParameters("@Pedido", stPedido);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterPedidosPorPessoa(int prItObjRef)
        {
            MI_SetSql("SELECT * FROM tblpedido WHERE dsSituacao <> 'CANCELADO' AND " +
                      "CdCliente = @Cliente                                        ");
            MI_AddParameters("@Cliente", prItObjRef);
            return MI_ExecuteDataSet(); 
        }
        public CTO_Pedido MS_ObterPorNumeroPedido(string prCdPedido)
        {
            CTO_Pedido ITO_Pedido = new CTO_Pedido();
            MI_SetSql(AP_SelectALL +
                       "FROM tblPedido " +
                       "WHERE CdPedido = @ID ");
            MI_AddParameters("@ID", prCdPedido);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Pedido = null;
            else
                ITO_Pedido = MI_DataSetToEntidade();
            MI_FecharConexao();
            return ITO_Pedido;
        }

        public void MS_AlterarDataPagamento(string stPedido, DateTime dtPagamento)
        {
            MI_SetSql("UPDATE TBLPEDIDO SET dtPagamento = curdate() WHERE CDPEDIDO = @Pedido");
            MI_AddParameters("@Pedido", stPedido);
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public DataTable MS_ObterInformacoes(string stFormaPagamento, string stDataPagamento)
        { 
            MI_SetSql("SELECT " +
                        "   (SELECT TBLVISITANTE.NOMECOMPLETO " + "FROM TBLVISITANTE " + "WHERE TBLVISITANTE.OBJREF = TBLPEDIDO.CDCLIENTE AND " + "TBLPEDIDO.DSSITUACAO <> 'CANCELADO') AS 'CLIENTE', " +
                        "   (SELECT TBLVISITANTE.RAZAOSOCIAL " + "FROM TBLVISITANTE " + "WHERE TBLVISITANTE.OBJREF = TBLPEDIDO.CDCLIENTE AND " + "TBLPEDIDO.DSSITUACAO <> 'CANCELADO') AS 'EMPRESA', " + 
                        "   TBLPEDIDO.DSVALORDESCONTO AS VALOR, " + 
                        "   TBLPEDIDO.DTPAGAMENTO AS DATA,      " +
                        "   TBLPEDIDO.DSFORMAPAGAMENTO AS FORMA " +
                        "FROM TBLPEDIDO " +
                        "WHERE " +
                        "   TBLPEDIDO.DSSITUACAO = 'PAGO' AND " +
                        "   TBLPEDIDO.DSFORMAPAGAMENTO = @FormaPagamento AND               " +
                        "   TBLPEDIDO.DTPAGAMENTO = @DtPagamento                           " +
                        "   ORDER BY TBLPEDIDO.DTPAGAMENTO, TBLPEDIDO.DSFORMAPAGAMENTO     ");
            MI_AddParameters("@FormaPagamento", stFormaPagamento);
            MI_AddParameters("@DtPagamento", stDataPagamento);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterInformacoesTodos(string stData )
        {
            MI_SetSql("SELECT " +
                        "(SELECT TBLVISITANTE.NOMECOMPLETO " +
                        "FROM TBLVISITANTE " +
                        "WHERE TBLVISITANTE.OBJREF = TBLPEDIDO.CDCLIENTE AND " +
                        "TBLPEDIDO.DSSITUACAO <> 'CANCELADO') AS 'CLIENTE', " +
                        "(SELECT TBLVISITANTE.RAZAOSOCIAL " +
                        "FROM TBLVISITANTE " +
                        "WHERE TBLVISITANTE.OBJREF = TBLPEDIDO.CDCLIENTE AND " +
                        "TBLPEDIDO.DSSITUACAO <> 'CANCELADO') AS 'EMPRESA', " +
                        "TBLPEDIDO.DSVALORDESCONTO, " +
                        "TBLPEDIDO.DSFORMAPAGAMENTO, " +
                        "TBLPEDIDO.DTPAGAMENTO " +
                        "FROM TBLPEDIDO " +
                        "WHERE TBLPEDIDO.DSSITUACAO = 'PAGO'" +
                        "ORDER BY TBLPEDIDO.DSFORMAPAGAMENTO, TBLPEDIDO.CDCLIENTE ");
            MI_AddParameters("@Data", stData);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterControleCursos()
        {
            MI_SetSql("SELECT TBLPRODUTO.DSPRODUTO,                                 " +
                        " (SELECT COUNT(*)                                          " +
                        " FROM TBLITENSPEDIDO                                       " +
                        " WHERE TBLITENSPEDIDO.CDPRODUTO = TBLPRODUTO.CDPRODUTO     " +
                        " GROUP BY CDPRODUTO) AS 'QTDINSCRITO',                     " +
                        " (SELECT COUNT(TBLITENSPEDIDO.CDPEDIDO)                    " +
                        " FROM TBLITENSPEDIDO, TBLPEDIDO                            " +
                        " WHERE TBLITENSPEDIDO.CDPEDIDO = TBLPEDIDO.CDPEDIDO AND    " +
                        " TBLITENSPEDIDO.CDPRODUTO = TBLPRODUTO.CDPRODUTO AND       " +
                        " TBLPEDIDO.DSSITUACAO = 'PAGO'                             " +
                        " GROUP BY TBLPRODUTO.CDPRODUTO) AS 'PAGOS',                " +
                        " (SELECT COUNT(TBLITENSPEDIDO.CDPEDIDO)                    " +
                        " FROM TBLITENSPEDIDO, TBLPEDIDO                            " +
                        " WHERE TBLITENSPEDIDO.CDPEDIDO = TBLPEDIDO.CDPEDIDO AND    " +
                        " TBLITENSPEDIDO.CDPRODUTO = TBLPRODUTO.CDPRODUTO AND       " +
                        " TBLPEDIDO.DSSITUACAO = 'A RECEBER'                        " +
                        " GROUP BY TBLPRODUTO.CDPRODUTO) AS 'A RECEBER'             " +
                        " FROM TBLPRODUTO                                           " );
            return MI_ExecuteDataSet();
        }
        
        public DataTable MS_ObterFormasPagamento()
        {
            MI_SetSql("SELECT DISTINCT(DSFORMAPAGAMENTO) FROM TBLPEDIDO WHERE DSSITUACAO = 'PAGO' AND DSFORMAPAGAMENTO <> '' ORDER BY DSFORMAPAGAMENTO ");
            return MI_ExecuteDataSet();
        }
        public DataTable MS_ObterDataPagamento(string prFormaPagamento)
        {
            MI_SetSql("SELECT DISTINCT(DTPAGAMENTO) FROM TBLPEDIDO WHERE DSSITUACAO = 'PAGO' AND DSFORMAPAGAMENTO = @Forma ORDER BY DTPAGAMENTO");
            MI_AddParameters("@Forma", prFormaPagamento);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterCodigoProdutoReal(string prColuna)
        {
            //MI_SetSql("SELECT " +
            //            "	tblProduto.cdProduto AS CodigoReal " +
            //            "FROM  " +
            //            "	dicionarioazulbranco " +
            //            "		INNER JOIN tblProduto ON " +
            //            "			tblProduto.dsProduto = dicionarioazulbranco.Descricao " +
            //            "where " +
            //            "	dicionarioazulbranco.coluna = '" + prColuna + "'");

            MI_SetSql("SELECT " +
            "	tblProduto.cdProduto AS CodigoReal " +
            "FROM  " +
            "	dicionarioverdeamarelo " +
            "		INNER JOIN tblProduto ON " +
            "			tblProduto.dsProduto = dicionarioverdeamarelo.Descricao " +
            "where " +
            "	dicionarioverdeamarelo.coluna = '" + prColuna + "'");

            
            return MI_ExecuteDataSet();            
        }

        public DataTable MS_ObterUltimoPedido()
        {
            MI_SetSql("SELECT MAX(cdPedido)Pedido FROM tblPedido");
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterUltimaImportacao()
        {
            MI_SetSql("SELECT MAX(NumeroImportacao)Importacao FROM tblVisitante");
            return MI_ExecuteDataSet();
        }

        public void MS_ExcluirPedidos(Int32 cdUltimoPedido, Int32 prObjRef)
        {
            MI_SetSql("UPDATE " +
                      "    tblPedido " +
                      "SET " +
                      "     dsSituacao = 'CANCELADO', " +
                      "     dsObservacaoImport = 'CANCELADO VIA NOVA IPORTA��O' " +
                      "WHERE " +
                      "     cdCliente = @cdCliente AND  " +
                      "     cdPedido <= @cdPedido ");

            MI_AddParameters("@cdCliente", prObjRef);
            MI_AddParameters("@cdPedido", cdUltimoPedido);

            MI_ExecuteNonQuery();
        }
    }
}

