using System;
using System.Data;
using SPI;

public class CAT_CadastrarPedido : CAB_CadastrarPedido
{
     public CAT_CadastrarPedido()
     {
     }
     public override void MS_Incluir(CTO_Pedido ITO_Pedido)
     {
          DAO_Pedido IDAO_Pedido = new DAO_Pedido();
          IDAO_Pedido.MS_Incluir(ITO_Pedido);
     }
     public override void MS_Alterar(CTO_Pedido ITO_Pedido)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         IDAO_Pedido.MS_Alterar(ITO_Pedido);
     }
     public override void MS_Excluir(CTO_Pedido ITO_Pedido)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         IDAO_Pedido.MS_Excluir(ITO_Pedido);
     }
     public override CTO_Pedido MS_Obter(int pritObjRef)
     {
           DAO_Pedido IDAO_Pedido = new DAO_Pedido();
           return IDAO_Pedido.MS_Obter(pritObjRef);
     }
     public override CTO_Pedido MS_Obter(string prstID)
     {
          DAO_Pedido IDAO_Pedido = new DAO_Pedido();
          return IDAO_Pedido.MS_Obter(prstID);
     }
     public override int MS_NovoObjRef()
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_NovoObjRef();
     }
     public override DataTable MI_ObterItensComprados(int idCliente)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MI_ObterItensComprados(idCliente);
     }
     public override DataTable MI_ObterTodosPedidos()
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MI_ObterTodosPedidos();
     }
     public override DataTable MI_ObterPedidos()
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MI_ObterPedidos();
     }
     public DataTable MS_ObterPedidosGrid(string stPedido)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterPedidosGrid(stPedido);
     }

     public DataTable MS_ObterPedidosPorPessoa(int prItObjRef)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterPedidosPorPessoa(prItObjRef);
     }
    
     public CTO_Pedido MS_ObterPorNumeroPedido(string prCdPedido)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterPorNumeroPedido(prCdPedido);
     }
     public void MS_AlterarDataPagamento(string stPedido, DateTime dtPagamento)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         IDAO_Pedido.MS_AlterarDataPagamento(stPedido, dtPagamento);
     }

     public DataTable MS_ObterInformacoes(string stFormaPagamento, string dtPagamento)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterInformacoes(stFormaPagamento, dtPagamento);
     }
     public DataTable MS_ObterInformacoesTodos(string stData)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterInformacoesTodos(stData);
     }

     public DataTable MS_ObterControleCursos()
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterControleCursos();
     }
    //
     public DataTable MS_ObterFormasPagamento()
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterFormasPagamento();
     }
    
     public DataTable MS_ObterDataPagamento(string prFormaPagamento)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterDataPagamento(prFormaPagamento);
     }

     public DataTable MS_ObterCodigoProdutoReal(string prColuna)
     {
         DAO_Pedido IDAO_Pedido = new DAO_Pedido();
         return IDAO_Pedido.MS_ObterCodigoProdutoReal(prColuna);
     }

    public DataTable MS_ObterUltimoPedido()
    {
        DAO_Pedido IDAO_Pedido = new DAO_Pedido();
        return IDAO_Pedido.MS_ObterUltimoPedido();
    }
    public DataTable MS_ObterUltimaImportacao()
    {
        DAO_Pedido IDAO_Pedido = new DAO_Pedido();
        return IDAO_Pedido.MS_ObterUltimaImportacao();
    }
    

    public void MS_ExcluirPedidos(Int32 cdUltimoPedido, Int32 prObjRef)
    {
        DAO_Pedido IDAO_Pedido = new DAO_Pedido();
        IDAO_Pedido.MS_ExcluirPedidos(cdUltimoPedido, prObjRef);
    }

}

