using System;
namespace SPI 
{

    public class CTO_Pedido 
    {
          //  Declaracao dos atributos Puros
        int AP_cdPedido;
        int AP_cdCliente;
        DateTime AP_dtPedido;
        string AP_dsSituacao;
        string AP_dsFormaPagamento;
        string AP_dsValorTotal;
        string AP_dsValorDesconto;
        string AP_dsTipoPessoa;
        string AP_dsCpfCnpj;
        string AP_dsNomeFaturamento;
        string AP_dsEnderecoFaturamento;
        string AP_dsCepFaturamento;
        string AP_dsComplementoFaturamento;
        string AP_dsBairroFaturamento;
        string AP_dsCidadeFaturamento;
        string AP_dsEstadoFaturamento;
        string AP_dsNumeroFaturamento;
        string AP_nmCongressista;
        DateTime AP_dtVencimento;
        DateTime AP_dtAlteracao;
        string AP_dsAlteracao;
        string AP_dsObservacao;

        public CTO_Pedido () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public int getcdPedido()
        {
            return AP_cdPedido;
        }
        public int getcdCliente()
        {
            return AP_cdCliente;
        }
        public DateTime getdtPedido()
        {
            return AP_dtPedido;
        }
        public string getdsSituacao()
        {
            return AP_dsSituacao;
        }
        public string getdsFormaPagamento()
        {
            return AP_dsFormaPagamento;
        }
        public string getdsValorTotal()
        {
            return AP_dsValorTotal;
        }
        public string getdsValorDesconto()
        {
            return AP_dsValorDesconto;
        }
        public string getdsTipoPessoa()
        {
            return AP_dsTipoPessoa;
        }
        public string getdsCpfCnpj()
        {
            return AP_dsCpfCnpj;
        }
        public string getdsNomeFaturamento()
        {
            return AP_dsNomeFaturamento;
        }
        public string getdsEnderecoFaturamento()
        {
            return AP_dsEnderecoFaturamento;
        }
        public string getdsCepFaturamento()
        {
            return AP_dsCepFaturamento;
        }
        public string getdsComplementoFaturamento()
        {
            return AP_dsComplementoFaturamento;
        }
        public string getdsBairroFaturamento()
        {
            return AP_dsBairroFaturamento;
        }
        public string getdsCidadeFaturamento()
        {
            return AP_dsCidadeFaturamento;
        }
        public string getdsEstadoFaturamento()
        {
            return AP_dsEstadoFaturamento;
        }
        public string getdsNumeroFaturamento()
        {
            return AP_dsNumeroFaturamento;
        }
        public string getnmCongressista()
        {
            return AP_nmCongressista;
        }
        public DateTime getdtVencimento()
        {
            return AP_dtVencimento;
        }
        public DateTime getdtAlteracao()
        {
            return AP_dtAlteracao;
        }
        public string getdsAlteracao()
        {
            return AP_dsAlteracao;
        }
        public string getdsObservacao()
        {
            return AP_dsObservacao;
        }
         //  Declaracao dos Metodos set
        public void setcdPedido( int prcdPedido)
        {
            AP_cdPedido = prcdPedido;
        }
        public void setcdCliente( int prcdCliente)
        {
            AP_cdCliente = prcdCliente;
        }
        public void setdtPedido( DateTime prdtPedido)
        {
            AP_dtPedido = prdtPedido;
        }
        public void setdsSituacao( string prdsSituacao)
        {
            AP_dsSituacao = prdsSituacao;
        }
        public void setdsFormaPagamento( string prdsFormaPagamento)
        {
            AP_dsFormaPagamento = prdsFormaPagamento;
        }
        public void setdsValorTotal( string prdsValorTotal)
        {
            AP_dsValorTotal = prdsValorTotal;
        }
        public void setdsValorDesconto( string prdsValorDesconto)
        {
            AP_dsValorDesconto = prdsValorDesconto;
        }
        public void setdsTipoPessoa( string prdsTipoPessoa)
        {
            AP_dsTipoPessoa = prdsTipoPessoa;
        }
        public void setdsCpfCnpj( string prdsCpfCnpj)
        {
            AP_dsCpfCnpj = prdsCpfCnpj;
        }
        public void setdsNomeFaturamento( string prdsNomeFaturamento)
        {
            AP_dsNomeFaturamento = prdsNomeFaturamento;
        }
        public void setdsEnderecoFaturamento( string prdsEnderecoFaturamento)
        {
            AP_dsEnderecoFaturamento = prdsEnderecoFaturamento;
        }
        public void setdsCepFaturamento( string prdsCepFaturamento)
        {
            AP_dsCepFaturamento = prdsCepFaturamento;
        }
        public void setdsComplementoFaturamento( string prdsComplementoFaturamento)
        {
            AP_dsComplementoFaturamento = prdsComplementoFaturamento;
        }
        public void setdsBairroFaturamento( string prdsBairroFaturamento)
        {
            AP_dsBairroFaturamento = prdsBairroFaturamento;
        }
        public void setdsCidadeFaturamento( string prdsCidadeFaturamento)
        {
            AP_dsCidadeFaturamento = prdsCidadeFaturamento;
        }
        public void setdsEstadoFaturamento( string prdsEstadoFaturamento)
        {
            AP_dsEstadoFaturamento = prdsEstadoFaturamento;
        }
        public void setdsNumeroFaturamento( string prdsNumeroFaturamento)
        {
            AP_dsNumeroFaturamento = prdsNumeroFaturamento;
        }
        public void setnmCongressista( string prnmCongressista)
        {
            AP_nmCongressista = prnmCongressista;
        }
        public void setdtVencimento( DateTime prdtVencimento)
        {
            AP_dtVencimento = prdtVencimento;
        }
        public void setdtAlteracao( DateTime prdtAlteracao)
        {
            AP_dtAlteracao = prdtAlteracao;
        }
        public void setdsAlteracao( string prdsAlteracao)
        {
            AP_dsAlteracao = prdsAlteracao;
        }
        public void setdsObservacao( string prdsObservacao)
        {
            AP_dsObservacao = prdsObservacao;
        }
    }
}

