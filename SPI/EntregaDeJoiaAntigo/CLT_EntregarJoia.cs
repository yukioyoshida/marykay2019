using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_EntregarJoia : Form
    {
        public DataTable dtTodosRegistros;
        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        public string stCodigoBarras = "";
        public string stMetodoBusca = "";
        public string stPalavraChave = "";

        public CLT_EntregarJoia()
        {
            InitializeComponent();
        }

        private void edtPalavra_TextChanged(object sender, EventArgs e)
        {
        }

        private void MI_LocalizarRegistro(string prContendo)
        {
            DataTable dtRegistros = new DataTable();
            CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();
                       
            dtRegistros = IAT_CadastrarVisitante.MS_ObterPorCodigoData(prContendo);

            lblNome1.Text = string.Empty;
            lblNome2.Text = string.Empty;
            lblCodigo.Text = string.Empty;

            dgwReconhecimentos.Rows.Clear();
            gdwEntregas.Rows.Clear();
            
            if (dtRegistros.Rows.Count > 0)
            {
                lblCodigo.Text = dtRegistros.Rows[0]["CodigoBarras"].ToString();

                string stNrConsultora = dtRegistros.Rows[0]["Res1"].ToString();
                DataTable dtConsultora = IAT_CadastrarVisitante.MS_ObterPorCodigoConsultora(stNrConsultora, "");

                if (dtConsultora.Rows.Count == 0)
                {
                    lblNome1.Text = dtRegistros.Rows[0]["NomeCompleto"].ToString();
                }
                else
                {
                    lblNome1.Text = dtConsultora.Rows[0]["NomeCompleto"].ToString();
                    if (dtConsultora.Rows.Count > 1)
                    {
                        lblNome2.Text = dtConsultora.Rows[1]["NomeCompleto"].ToString();
                    }

                    // Carregar as premia��es

                    DataTable dtPremiacoes = IAT_CadastrarVisitante.MS_ObterPremiacoes(stNrConsultora);

                    foreach (DataRow Row in dtPremiacoes.Rows)
                    {
                        if (Row["dsTipo"].ToString() == "RETIRAR")
                        {
                            string stTipoBotao = "RETIRAR";
                            string stImagem = Row["dsFoto"].ToString();

                            if (Row["dsRetirado"].ToString() == "S")
                                stTipoBotao = "RETIRADO";

                            if (Row["dsEscolha"].ToString() != "")
                            {
                                stTipoBotao = "RETIRADO";
                                stImagem = Row["dsEscolha"].ToString();
                            }

                            if (Row["dsRetirado"].ToString() == "E")
                                stTipoBotao = "ESCOLHIDO";

                            gdwEntregas.Rows.Add(
                                Row["ObjRef"].ToString(), 
                                System.Drawing.Image.FromFile("ImagensPremios/" + stImagem + ""), 
                                Row["Descricao"].ToString(), stTipoBotao);
                        }
                        else if (Row["dsTipo"].ToString() == "ESCOLHA")
                        {
                            string stTipoBotao = "ESCOLHER RETIRAR";
                            string stImagem = Row["dsFoto"].ToString();

                            if (Row["dsRetirado"].ToString() == "S")
                                stTipoBotao = "RETIRADO";

                            if (Row["dsEscolha"].ToString() != "")
                            {
                                stTipoBotao = "RETIRADO";
                                stImagem = Row["dsEscolha"].ToString();
                            }

                            if (Row["dsRetirado"].ToString() == "E")
                                stTipoBotao = "ESCOLHIDO";

                            if (stTipoBotao == "ESCOLHER RETIRAR")
                            {
                                gdwEntregas.Rows.Add(
                                    Row["ObjRef"].ToString(),
                                    System.Drawing.Image.FromFile("ImagensPremios/" + stImagem + ""),
                                    Row["Descricao"].ToString(),
                                    stTipoBotao, "SOMENTE ESCOLHER");
                            }
                            else
                            {
                                gdwEntregas.Rows.Add(
                                Row["ObjRef"].ToString(),
                                System.Drawing.Image.FromFile("ImagensPremios/" + stImagem + ""),
                                Row["Descricao"].ToString(),
                                stTipoBotao, "");
                            }
                        }
                        else
                        {
                            dgwReconhecimentos.Rows.Add(Row["Descricao"].ToString());
                        }
                    }

                    foreach (DataGridViewRow Row in dgwReconhecimentos.Rows)
                    {
                        Row.Height = 25;
                    }

                    foreach (DataGridViewRow Row in gdwEntregas.Rows)
                    {
                        Row.Height = 55;
                        if (Row.Cells[3].Value.ToString() == "")
                        {
                            
                        }
                    }

                }
                
            }
            
            //dgwReconhecimentos.DataSource = dtRegistros;
            
            //dgwReconhecimentos.Columns[0].Visible = false;
            //dgwReconhecimentos.Columns[2].Width = 200;
            //dgwReconhecimentos.Columns[3].Width = 200;
            //dgwReconhecimentos.Columns[4].Width = 150;
            //dgwReconhecimentos.Columns[5].Width = 140;

            //dtTodosRegistros = dtRegistros;
        }

        private void dgwRegistros_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            stCodigoBarras = dgwReconhecimentos.CurrentRow.Cells[0].Value.ToString();
            Close();
        }

        private void dgwRegistros_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (dgwReconhecimentos.Rows.Count > 0)
                {
                    stCodigoBarras = dgwReconhecimentos.CurrentRow.Cells[0].Value.ToString();
                    this.Dispose();
                }
            }
        }

        private void CLT_LocalizarRegistro_Load(object sender, EventArgs e)
        {
            CarregaUsuario();
            edtPalavra.Focus();

            CLT_LeituraJoia open = new CLT_LeituraJoia();
            open.ShowDialog();
        }

        private void CarregaUsuario()
        {
            lblUsuario.Text = CSV_Global.getUsuario();
            lblTerminal.Text = CSV_Global.getNumeroTerminal();
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void CLT_LocalizarRegistro_KeyPress(object sender, KeyPressEventArgs e)
        {
            int itTecla = e.KeyChar;
            switch (itTecla.ToString())
            {
                case "13":
                    MI_LocalizarRegistro(edtPalavra.Text.Trim());
                    edtPalavra.Text = string.Empty;
                    break;
                case "27":
                    this.Close();
                    break;
            }
        }

        private void tmrTempo_Tick(object sender, EventArgs e)
        {
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void rbtLocalizese_CheckedChanged(object sender, EventArgs e)
        {
            edtPalavra.Focus();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
        }

        private void rbtCodigo_CheckedChanged_1(object sender, EventArgs e)
        {
            edtPalavra.Focus();
        }

        private void rbtCodigoBarras_CheckedChanged_1(object sender, EventArgs e)
        {
            edtPalavra.Focus();
        }

        private void gdwEntregas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 3)
                {
                    if (gdwEntregas.CurrentCell.Value.ToString() == "RETIRAR")
                    {
                        CLT_RetirarJoia open = new CLT_RetirarJoia();
                        
                        open.AP_stCodigoBarras = lblCodigo.Text;
                        open.AP_stNome1 = lblNome1.Text;
                        open.AP_stNome2 = lblNome2.Text;
                        open.AP_stCdPrimiacao = gdwEntregas.CurrentRow.Cells[0].Value.ToString();

                        open.ShowDialog();

                        MI_LocalizarRegistro(lblCodigo.Text);

                        edtPalavra.Focus();
                    }
                    if (gdwEntregas.CurrentCell.Value.ToString() == "ESCOLHER RETIRAR")
                    {
                        CLT_RetirarEscolhaJoia open = new CLT_RetirarEscolhaJoia();

                        open.AP_stCodigoBarras = lblCodigo.Text;
                        open.AP_stNome1 = lblNome1.Text;
                        open.AP_stNome2 = lblNome2.Text;
                        open.AP_stCdPrimiacao = gdwEntregas.CurrentRow.Cells[0].Value.ToString();

                        open.ShowDialog();

                        MI_LocalizarRegistro(lblCodigo.Text);

                        edtPalavra.Focus();
                    }
                    if (gdwEntregas.CurrentCell.Value.ToString() == "RETIRADO")
                    {
                        MessageBox.Show("Opera��o j� realizada.", "Aten��o!");

                        edtPalavra.Focus();
                    }

                    if (gdwEntregas.CurrentCell.Value.ToString() == "ESCOLHIDO")
                    {
                        MessageBox.Show("Opera��o j� realizada.", "Aten��o!");

                        edtPalavra.Focus();
                    }
                }
                if (e.ColumnIndex == 4)
                {
                    if (gdwEntregas.CurrentCell.Value.ToString() == "SOMENTE ESCOLHER")
                    {
                        CLT_EscolhaJoia open = new CLT_EscolhaJoia();

                        open.AP_stCodigoBarras = lblCodigo.Text;
                        open.AP_stNome1 = lblNome1.Text;
                        open.AP_stNome2 = lblNome2.Text;
                        open.AP_stCdPrimiacao = gdwEntregas.CurrentRow.Cells[0].Value.ToString();

                        open.ShowDialog();

                        MI_LocalizarRegistro(lblCodigo.Text);

                        edtPalavra.Focus();
                    }
                }
            }
            catch { }
        }

        private void btoIncluirNovo_Click(object sender, EventArgs e)
        {
        }
    }
}