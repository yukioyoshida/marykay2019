﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_DetalhesProduto : Form
    {
        public Dictionary<string, string> AP_DetalhesProduto { get; set; }
        
        public CLT_DetalhesProduto()
        {
            InitializeComponent();
        }

        private void MI_ObterDetalhes()
        {
            lblTotalEstoqueDisponivel.Text = AP_DetalhesProduto.Where(x => x.Key == "DISPONIVEL").FirstOrDefault().Value.ToString();
            lblQuantidadeCorreios.Text = AP_DetalhesProduto.Where(x => x.Key == "CORREIOS").FirstOrDefault().Value.ToString();
            lblQuantidadeReservada.Text = AP_DetalhesProduto.Where(x => x.Key == "RESERVADO").FirstOrDefault().Value.ToString();
            lblQuantidadeRetirada.Text = AP_DetalhesProduto.Where(x => x.Key == "RETIRADO").FirstOrDefault().Value.ToString();
            lblDescricaoProduto.Text = AP_DetalhesProduto.Where(x => x.Key == "DESCRICAO").FirstOrDefault().Value.ToString();
            var ms = new MemoryStream(new DAO_Visitante().MS_ObterFotoRoteiro(Convert.ToInt32(AP_DetalhesProduto.Where(x => x.Key == "CDROTEIRO").FirstOrDefault().Value.ToString())));
            picFotoProduto.Image = Image.FromStream(ms);
        }

        private void btVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CLT_DetalhesProduto_Load(object sender, EventArgs e)
        {
            MI_ObterDetalhes();
        }
    }
}
