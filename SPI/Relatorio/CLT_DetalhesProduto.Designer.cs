﻿namespace SPI
{
    partial class CLT_DetalhesProduto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btVoltar = new System.Windows.Forms.Button();
            this.lblQuantidadeCorreios = new System.Windows.Forms.Label();
            this.lblDescricaoProduto = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblQuantidadeRetirada = new System.Windows.Forms.Label();
            this.picFotoProduto = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblQuantidadeReservada = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTotalEstoqueDisponivel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoProduto)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.MistyRose;
            this.panel1.Controls.Add(this.btVoltar);
            this.panel1.Controls.Add(this.lblQuantidadeCorreios);
            this.panel1.Controls.Add(this.lblDescricaoProduto);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lblQuantidadeRetirada);
            this.panel1.Controls.Add(this.picFotoProduto);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.lblQuantidadeReservada);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblTotalEstoqueDisponivel);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1036, 279);
            this.panel1.TabIndex = 4;
            // 
            // btVoltar
            // 
            this.btVoltar.BackColor = System.Drawing.Color.White;
            this.btVoltar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btVoltar.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btVoltar.ForeColor = System.Drawing.Color.Black;
            this.btVoltar.Location = new System.Drawing.Point(880, 222);
            this.btVoltar.Name = "btVoltar";
            this.btVoltar.Size = new System.Drawing.Size(147, 46);
            this.btVoltar.TabIndex = 8;
            this.btVoltar.Text = "Voltar";
            this.btVoltar.UseVisualStyleBackColor = false;
            this.btVoltar.Click += new System.EventHandler(this.btVoltar_Click);
            // 
            // lblQuantidadeCorreios
            // 
            this.lblQuantidadeCorreios.AutoSize = true;
            this.lblQuantidadeCorreios.Font = new System.Drawing.Font("Verdana", 10.2F);
            this.lblQuantidadeCorreios.Location = new System.Drawing.Point(360, 239);
            this.lblQuantidadeCorreios.Name = "lblQuantidadeCorreios";
            this.lblQuantidadeCorreios.Size = new System.Drawing.Size(78, 20);
            this.lblQuantidadeCorreios.TabIndex = 7;
            this.lblQuantidadeCorreios.Text = "[TOTAL]";
            // 
            // lblDescricaoProduto
            // 
            this.lblDescricaoProduto.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescricaoProduto.Location = new System.Drawing.Point(15, 24);
            this.lblDescricaoProduto.Name = "lblDescricaoProduto";
            this.lblDescricaoProduto.Size = new System.Drawing.Size(1012, 58);
            this.lblDescricaoProduto.TabIndex = 2;
            this.lblDescricaoProduto.Text = "[DESCRICAO]";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 10.2F);
            this.label6.Location = new System.Drawing.Point(181, 239);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "Correios:";
            // 
            // lblQuantidadeRetirada
            // 
            this.lblQuantidadeRetirada.AutoSize = true;
            this.lblQuantidadeRetirada.Font = new System.Drawing.Font("Verdana", 10.2F);
            this.lblQuantidadeRetirada.Location = new System.Drawing.Point(360, 187);
            this.lblQuantidadeRetirada.Name = "lblQuantidadeRetirada";
            this.lblQuantidadeRetirada.Size = new System.Drawing.Size(78, 20);
            this.lblQuantidadeRetirada.TabIndex = 5;
            this.lblQuantidadeRetirada.Text = "[TOTAL]";
            // 
            // picFotoProduto
            // 
            this.picFotoProduto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFotoProduto.Location = new System.Drawing.Point(19, 87);
            this.picFotoProduto.Name = "picFotoProduto";
            this.picFotoProduto.Size = new System.Drawing.Size(148, 181);
            this.picFotoProduto.TabIndex = 0;
            this.picFotoProduto.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 10.2F);
            this.label5.Location = new System.Drawing.Point(181, 187);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Retirados:";
            // 
            // lblQuantidadeReservada
            // 
            this.lblQuantidadeReservada.AutoSize = true;
            this.lblQuantidadeReservada.Font = new System.Drawing.Font("Verdana", 10.2F);
            this.lblQuantidadeReservada.Location = new System.Drawing.Point(360, 137);
            this.lblQuantidadeReservada.Name = "lblQuantidadeReservada";
            this.lblQuantidadeReservada.Size = new System.Drawing.Size(78, 20);
            this.lblQuantidadeReservada.TabIndex = 3;
            this.lblQuantidadeReservada.Text = "[TOTAL]";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 10.2F);
            this.label4.Location = new System.Drawing.Point(181, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "Reservados:";
            // 
            // lblTotalEstoqueDisponivel
            // 
            this.lblTotalEstoqueDisponivel.AutoSize = true;
            this.lblTotalEstoqueDisponivel.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalEstoqueDisponivel.Location = new System.Drawing.Point(360, 82);
            this.lblTotalEstoqueDisponivel.Name = "lblTotalEstoqueDisponivel";
            this.lblTotalEstoqueDisponivel.Size = new System.Drawing.Size(78, 20);
            this.lblTotalEstoqueDisponivel.TabIndex = 1;
            this.lblTotalEstoqueDisponivel.Text = "[TOTAL]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(181, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Disponível:";
            // 
            // CLT_DetalhesProduto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1056, 303);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CLT_DetalhesProduto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Detalhes";
            this.Load += new System.EventHandler(this.CLT_DetalhesProduto_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoProduto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblQuantidadeCorreios;
        private System.Windows.Forms.Label lblDescricaoProduto;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblQuantidadeRetirada;
        private System.Windows.Forms.PictureBox picFotoProduto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblQuantidadeReservada;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblTotalEstoqueDisponivel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btVoltar;
    }
}