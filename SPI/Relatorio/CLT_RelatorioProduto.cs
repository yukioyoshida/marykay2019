﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_RelatorioProduto : Form
    {
        public CLT_RelatorioProduto()
        {
            InitializeComponent();
        }
        private void CLT_RelatorioProduto_Load(object sender, EventArgs e)
        {
            MI_PopulaComboProdutos();
            MI_CarregaProdutosDisponiveis();
        }
        private void MI_PopulaComboProdutos()
        {
            List<CTO_Roteiro> lITO_Roteiro = new List<CTO_Roteiro>();
            lITO_Roteiro = new CAT_Roteiro().MS_CarregarProdutos();

            cmbProdutos.DataSource = lITO_Roteiro;
            cmbProdutos.DisplayMember = "DSRECONHECIMENTO";
            cmbProdutos.ValueMember = "CDROTEIRO";
        }

        private void MI_CarregaProdutosDisponiveis()
        {
            List<CTO_Roteiro> lITO_Roteiro = new List<CTO_Roteiro>();
            lITO_Roteiro = new CAT_Roteiro().MS_CarregarProdutos();
            int _positionY = 3;

            Dictionary<int, int> dcQuantidadeReservados = MI_CalcularReservados();
            Dictionary<int, int> dcQuantidadeRetirados = MI_CalcularRetirados();
            Dictionary<int, int> dcQuantidadeCorreios = MI_CalcularQuantidadeCorreios();

            pnlProdutosMain.Controls.Clear();

            foreach (var item in lITO_Roteiro)
            {

                int itQuantidadeProduto = item.dsQuantidadeEstoque;
                int itQuantidadeReservada = dcQuantidadeReservados.Where(x => x.Key == item.cdRoteiro).FirstOrDefault().Value;
                int itQuantidadeRetirada = dcQuantidadeRetirados.Where(x => x.Key == item.cdRoteiro).FirstOrDefault().Value;
                int itQuantidadeCorreios = dcQuantidadeCorreios.Where(x => x.Key == item.cdRoteiro).FirstOrDefault().Value;

                int itQuantidadeDisponivel = itQuantidadeProduto - itQuantidadeReservada - itQuantidadeRetirada - itQuantidadeCorreios;

                Dictionary<string, string> dcValoresTransporte = new Dictionary<string, string>();

                dcValoresTransporte.Add("DISPONIVEL", itQuantidadeDisponivel.ToString());
                dcValoresTransporte.Add("RESERVADO", itQuantidadeReservada.ToString());
                dcValoresTransporte.Add("RETIRADO", itQuantidadeReservada.ToString());
                dcValoresTransporte.Add("CORREIOS", itQuantidadeCorreios.ToString());
                dcValoresTransporte.Add("CDROTEIRO", item.cdRoteiro.ToString());
                dcValoresTransporte.Add("DESCRICAO", item.dsReconhecimento.ToString());
                // 
                // picFotoProduto
                // 
                PictureBox pcProduto = new PictureBox();
                pcProduto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                pcProduto.Location = new System.Drawing.Point(14, 14);
                pcProduto.Name = "picFotoProduto";
                pcProduto.Size = new System.Drawing.Size(112, 126);
                pcProduto.TabIndex = 0;
                pcProduto.TabStop = false;
                var ms = new MemoryStream(new DAO_Visitante().MS_ObterFotoRoteiro(Convert.ToInt32(item.cdRoteiro)));
                pcProduto.Image = Image.FromStream(ms);
                // 
                // lblDescricaoProduto
                // 
                Label lblDescricao = new Label();
                lblDescricao.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                lblDescricao.Location = new System.Drawing.Point(132, 14);
                lblDescricao.Name = "lblDescricaoProduto";
                lblDescricao.Size = new System.Drawing.Size(765, 57);
                lblDescricao.TabIndex = 2;
                lblDescricao.Text = item.dsReconhecimento;
                lblDescricao.AutoSize = false;
                lblDescricao.TextAlign = System.Drawing.ContentAlignment.TopLeft;
                // 
                // label2
                // 
                Label lbl = new Label();
                lbl.AutoSize = true;
                lbl.Location = new System.Drawing.Point(132, 71);
                lbl.Name = "label2";
                lbl.Size = new System.Drawing.Size(147, 29);
                lbl.TabIndex = 0;
                lbl.Text = "Disponível:";
                lbl.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                // 
                // lblTotalEstoqueFisico
                // 
                Label lblQtdEstoque = new Label();
                lblQtdEstoque.AutoSize = true;
                lblQtdEstoque.Location = new System.Drawing.Point(220, 71);
                lblQtdEstoque.Name = String.Format("{0}{1}", "lblTotalEstoqueFisico", item.cdRoteiro.ToString());
                lblQtdEstoque.Size = new System.Drawing.Size(765, 57);
                lblQtdEstoque.TabIndex = 1;
                lblQtdEstoque.Text = itQuantidadeDisponivel <= 1 ? String.Format("{0}{1}", itQuantidadeDisponivel.ToString(), " item.") : String.Format("{0}{1}", itQuantidadeDisponivel.ToString(), " ítens.");
                lblQtdEstoque.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                // 
                // btDetalhes
                // 
                Button btDetalhes = new Button();
                btDetalhes.Location = new System.Drawing.Point(131, 102);
                btDetalhes.Name = "btDetalhes";
                btDetalhes.Size = new System.Drawing.Size(130, 38);
                btDetalhes.TabIndex = 3;
                btDetalhes.Text = "Detalhes";
                btDetalhes.Tag = dcValoresTransporte;
                btDetalhes.UseVisualStyleBackColor = true;
                btDetalhes.BackColor = System.Drawing.Color.White;
                btDetalhes.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                btDetalhes.Click += new System.EventHandler(this.btDetalhes_Click);

                //
                //Painel para adição dos controles criados
                //
                Panel pnl = new Panel();
                pnl.BackColor = System.Drawing.Color.MistyRose;
                pnl.Controls.Add(btDetalhes);
                pnl.Controls.Add(lblDescricao);
                pnl.Controls.Add(pcProduto);
                pnl.Controls.Add(lblQtdEstoque);
                pnl.Controls.Add(lbl);
                pnl.Location = new System.Drawing.Point(3, _positionY);
                pnl.Name = String.Format("{0}{1}", "pnlProduto", item.cdRoteiro.ToString());
                pnl.Size = new System.Drawing.Size(900, 154);
                pnl.TabIndex = 3;

                pnlProdutosMain.Controls.Add(pnl);

                _positionY = _positionY + 160;
            }
        }

        private void btDetalhes_Click(object sender, EventArgs e)
        {
            Button btDetalhes = (Button)sender;

            CLT_DetalhesProduto abrirDetalhes = new CLT_DetalhesProduto();
            abrirDetalhes.AP_DetalhesProduto = (Dictionary<string, string>)btDetalhes.Tag;
            abrirDetalhes.ShowDialog();
        }

        private void MI_ObterInformacoesProduto(int cdRoteiro)
        {
            CTO_Roteiro dadosRoteiro = new CTO_Roteiro();

            dadosRoteiro = new CAT_Roteiro().MS_ObterRoteiroPorCodigo(cdRoteiro);

            if (dadosRoteiro != null)
            {
                PictureBox foto = (PictureBox)pnlConteudo.Controls["picFotoProduto"];

                if (!String.IsNullOrEmpty(dadosRoteiro.cdRoteiro.ToString()))
                {
                    new InserirFoto().MS_SetFoto(foto, dadosRoteiro.cdRoteiro.ToString());
                }

            }

            gpbDetalhes.Visible = true;
            lblDescricaoProduto.Text = dadosRoteiro.dsReconhecimento.ToString();
            lblTotalEstoqueFisico.Text = dadosRoteiro.dsQuantidadeEstoque == 0 ? "0 ítem(ns)" : String.Format("{0}{1}", dadosRoteiro.dsQuantidadeEstoque.ToString(), " Ítem(ns)");
        }



        private string MI_ObterDescricaoFoto(string prCdJoia)
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            return IAT_Visitante.MS_ObterPremioNew(prCdJoia).Rows[0]["dsFoto"].ToString();
        }



        private void btoConsultar_Click(object sender, EventArgs e)
        {
            String valor = cmbProdutos.SelectedValue.ToString();
            MI_ObterInformacoesProduto(Convert.ToInt32(valor));
        }

        private Dictionary<int, int> MI_CalcularReservados()
        {
            Dictionary<int, int> dcQuantidade = new CAT_PremioConsultoraItem().MS_CalcularReservados();
            return dcQuantidade;
        }
        private Dictionary<int, int> MI_CalcularQuantidadeCorreios()
        {
            Dictionary<int, int> dcQuantidade = new CAT_PremioConsultoraItem().MS_CalcularQuantidadeCorreios();
            return dcQuantidade;
        }

        private Dictionary<int, int> MI_CalcularRetirados()
        {
            Dictionary<int, int> dcQuantidade = new CAT_PremioConsultoraItem().MS_CalcularRetirados();
            return dcQuantidade;
        }
    }
}
