﻿namespace SPI
{
    partial class CLT_RelatorioProduto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlConteudo = new System.Windows.Forms.Panel();
            this.gpbDetalhes = new System.Windows.Forms.GroupBox();
            this.pnlProdutosMain = new System.Windows.Forms.Panel();
            this.pnlProduto = new System.Windows.Forms.Panel();
            this.btDetalhes = new System.Windows.Forms.Button();
            this.lblDescricaoProduto = new System.Windows.Forms.Label();
            this.picFotoProduto = new System.Windows.Forms.PictureBox();
            this.lblTotalEstoqueFisico = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gpbEscolha = new System.Windows.Forms.GroupBox();
            this.btoConsultar = new System.Windows.Forms.Button();
            this.cmbProdutos = new System.Windows.Forms.ComboBox();
            this.pnlConteudo.SuspendLayout();
            this.gpbDetalhes.SuspendLayout();
            this.pnlProdutosMain.SuspendLayout();
            this.pnlProduto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoProduto)).BeginInit();
            this.gpbEscolha.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlConteudo
            // 
            this.pnlConteudo.BackColor = System.Drawing.Color.Transparent;
            this.pnlConteudo.Controls.Add(this.gpbDetalhes);
            this.pnlConteudo.Controls.Add(this.gpbEscolha);
            this.pnlConteudo.Location = new System.Drawing.Point(12, 12);
            this.pnlConteudo.Name = "pnlConteudo";
            this.pnlConteudo.Size = new System.Drawing.Size(1285, 757);
            this.pnlConteudo.TabIndex = 0;
            // 
            // gpbDetalhes
            // 
            this.gpbDetalhes.BackColor = System.Drawing.Color.White;
            this.gpbDetalhes.Controls.Add(this.pnlProdutosMain);
            this.gpbDetalhes.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbDetalhes.Location = new System.Drawing.Point(23, 167);
            this.gpbDetalhes.Name = "gpbDetalhes";
            this.gpbDetalhes.Size = new System.Drawing.Size(1241, 557);
            this.gpbDetalhes.TabIndex = 1;
            this.gpbDetalhes.TabStop = false;
            this.gpbDetalhes.Text = "Detalhes";
            // 
            // pnlProdutosMain
            // 
            this.pnlProdutosMain.AutoScroll = true;
            this.pnlProdutosMain.BackColor = System.Drawing.Color.White;
            this.pnlProdutosMain.Controls.Add(this.pnlProduto);
            this.pnlProdutosMain.Location = new System.Drawing.Point(7, 35);
            this.pnlProdutosMain.Name = "pnlProdutosMain";
            this.pnlProdutosMain.Size = new System.Drawing.Size(1228, 516);
            this.pnlProdutosMain.TabIndex = 4;
            // 
            // pnlProduto
            // 
            this.pnlProduto.BackColor = System.Drawing.Color.MistyRose;
            this.pnlProduto.Controls.Add(this.btDetalhes);
            this.pnlProduto.Controls.Add(this.lblDescricaoProduto);
            this.pnlProduto.Controls.Add(this.picFotoProduto);
            this.pnlProduto.Controls.Add(this.lblTotalEstoqueFisico);
            this.pnlProduto.Controls.Add(this.label2);
            this.pnlProduto.Location = new System.Drawing.Point(3, 3);
            this.pnlProduto.Name = "pnlProduto";
            this.pnlProduto.Size = new System.Drawing.Size(900, 183);
            this.pnlProduto.TabIndex = 3;
            // 
            // btDetalhes
            // 
            this.btDetalhes.BackColor = System.Drawing.Color.White;
            this.btDetalhes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btDetalhes.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDetalhes.Location = new System.Drawing.Point(131, 102);
            this.btDetalhes.Name = "btDetalhes";
            this.btDetalhes.Size = new System.Drawing.Size(130, 38);
            this.btDetalhes.TabIndex = 3;
            this.btDetalhes.Text = "Detalhes";
            this.btDetalhes.UseVisualStyleBackColor = false;
            // 
            // lblDescricaoProduto
            // 
            this.lblDescricaoProduto.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescricaoProduto.Location = new System.Drawing.Point(132, 14);
            this.lblDescricaoProduto.Name = "lblDescricaoProduto";
            this.lblDescricaoProduto.Size = new System.Drawing.Size(765, 57);
            this.lblDescricaoProduto.TabIndex = 2;
            this.lblDescricaoProduto.Text = "[DESCRICAO]";
            // 
            // picFotoProduto
            // 
            this.picFotoProduto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFotoProduto.Location = new System.Drawing.Point(14, 14);
            this.picFotoProduto.Name = "picFotoProduto";
            this.picFotoProduto.Size = new System.Drawing.Size(112, 126);
            this.picFotoProduto.TabIndex = 0;
            this.picFotoProduto.TabStop = false;
            // 
            // lblTotalEstoqueFisico
            // 
            this.lblTotalEstoqueFisico.AutoSize = true;
            this.lblTotalEstoqueFisico.Location = new System.Drawing.Point(285, 71);
            this.lblTotalEstoqueFisico.Name = "lblTotalEstoqueFisico";
            this.lblTotalEstoqueFisico.Size = new System.Drawing.Size(110, 29);
            this.lblTotalEstoqueFisico.TabIndex = 1;
            this.lblTotalEstoqueFisico.Text = "[TOTAL]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(132, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 29);
            this.label2.TabIndex = 0;
            this.label2.Text = "Disponível:";
            // 
            // gpbEscolha
            // 
            this.gpbEscolha.Controls.Add(this.btoConsultar);
            this.gpbEscolha.Controls.Add(this.cmbProdutos);
            this.gpbEscolha.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbEscolha.Location = new System.Drawing.Point(23, 24);
            this.gpbEscolha.Name = "gpbEscolha";
            this.gpbEscolha.Size = new System.Drawing.Size(1247, 102);
            this.gpbEscolha.TabIndex = 0;
            this.gpbEscolha.TabStop = false;
            this.gpbEscolha.Text = "Escolha um produto";
            // 
            // btoConsultar
            // 
            this.btoConsultar.Location = new System.Drawing.Point(1086, 44);
            this.btoConsultar.Name = "btoConsultar";
            this.btoConsultar.Size = new System.Drawing.Size(155, 36);
            this.btoConsultar.TabIndex = 1;
            this.btoConsultar.Text = "Consultar";
            this.btoConsultar.UseVisualStyleBackColor = true;
            this.btoConsultar.Click += new System.EventHandler(this.btoConsultar_Click);
            // 
            // cmbProdutos
            // 
            this.cmbProdutos.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbProdutos.FormattingEnabled = true;
            this.cmbProdutos.Location = new System.Drawing.Point(24, 45);
            this.cmbProdutos.Name = "cmbProdutos";
            this.cmbProdutos.Size = new System.Drawing.Size(1056, 33);
            this.cmbProdutos.TabIndex = 0;
            // 
            // CLT_RelatorioProduto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1309, 781);
            this.Controls.Add(this.pnlConteudo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CLT_RelatorioProduto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CLT_RelatorioProduto";
            this.Load += new System.EventHandler(this.CLT_RelatorioProduto_Load);
            this.pnlConteudo.ResumeLayout(false);
            this.gpbDetalhes.ResumeLayout(false);
            this.pnlProdutosMain.ResumeLayout(false);
            this.pnlProduto.ResumeLayout(false);
            this.pnlProduto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoProduto)).EndInit();
            this.gpbEscolha.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlConteudo;
        private System.Windows.Forms.GroupBox gpbEscolha;
        private System.Windows.Forms.ComboBox cmbProdutos;
        private System.Windows.Forms.Button btoConsultar;
        private System.Windows.Forms.GroupBox gpbDetalhes;
        private System.Windows.Forms.Label lblDescricaoProduto;
        private System.Windows.Forms.PictureBox picFotoProduto;
        private System.Windows.Forms.Panel pnlProduto;
        private System.Windows.Forms.Label lblTotalEstoqueFisico;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btDetalhes;
        private System.Windows.Forms.Panel pnlProdutosMain;
    }
}