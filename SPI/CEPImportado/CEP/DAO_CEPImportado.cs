using System;
using System.Data;
namespace CEPGeralImportado
{
    public class DAO_CEPImportado : FWK_DAO_CEPImportado
    {
        public DAO_CEPImportado()
        {
        }

        public DataTable ObterEstados()
        {
            MI_SetSql("Select UF From estados order by UF");
            return MI_ExecuteDataSet();
        }
        public DataTable ObterCidades(string prstEstado)
        {
            MI_SetSql("Select CIDADE From cidades WHERE UF = ?UF order by CIDADE");
            MI_AddParameters("?UF", prstEstado);
            return MI_ExecuteDataSet();
        }
        public DataTable MS_ObterEndereco(string stRua, string stEstado, string stCidade)
        {
            MI_SetSql("Select * From todos WHERE UF = ?UF and " +
                      "CIDADE = ?CIDADE and                   " +
                      "LOGRADOURO like ?LOGRADOURO               " +
                      "order by LOGRADOURO                    ");
            MI_AddParameters("?UF", stEstado);
            MI_AddParameters("?CIDADE", stCidade);
            MI_AddParameters("?LOGRADOURO", stRua + "%");
            return MI_ExecuteDataSet();
        }
    }
}

