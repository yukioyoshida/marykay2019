using System;
using System.Data;
using CEPGeralImportado;

public class CAT_CadastrarCEPImportado : CAB_CadastrarCEPImportado
{
    public CAT_CadastrarCEPImportado()
     {
     }
     public override DataTable ObterEstados()
     {
         DAO_CEPImportado IDAO_CEPImportado = new DAO_CEPImportado();
         return IDAO_CEPImportado.ObterEstados();
     }

    public override DataTable ObterCidades(string prstEstado)
    {
        DAO_CEPImportado IDAO_CEPImportado = new DAO_CEPImportado();
        return IDAO_CEPImportado.ObterCidades(prstEstado);
    }

    public override DataTable MS_ObterEndereco(string stRua, string stEstado, string stCidade)
    {
        DAO_CEPImportado IDAO_CEPImportado = new DAO_CEPImportado();
        return IDAO_CEPImportado.MS_ObterEndereco(stRua, stEstado, stCidade);
    }
}

