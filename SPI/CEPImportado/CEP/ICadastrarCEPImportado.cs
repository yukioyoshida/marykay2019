using System;
using System.Data;
using CEPGeralImportado;

public interface ICadastrarCEPImportado
{
    DataTable ObterEstados();
    DataTable ObterCidades(string prstEstado);
    DataTable MS_ObterEndereco(string stRua, string stEstado, string stCidade);
}

