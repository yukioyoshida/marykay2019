using System;
using System.Data;
using CEPGeralImportado;

public abstract class CAB_CadastrarCEPImportado : ICadastrarCEPImportado
{
    public abstract DataTable ObterEstados();
    public abstract DataTable ObterCidades(string prstEstado);
    public abstract DataTable MS_ObterEndereco(string stRua, string stEstado, string stCidade);
}

