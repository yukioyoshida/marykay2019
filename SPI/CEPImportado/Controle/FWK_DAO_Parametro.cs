using System;
using System.Collections.Generic;
using System.Text;
using CEPGeralImportado;
namespace CEPGeralImportado
{
   public class FWK_DAO_Parametro
    {
        String AP_Nome;
        Object AP_Valor;

        public void setNome(String prstNome)
        {
            AP_Nome = prstNome;
        }
        public String getNome()
        {
            return AP_Nome;
        }

        public void setValor(Object prstValor)
        {
            AP_Valor = prstValor;
        }

        public Object getValor()
        {
            return AP_Valor;
        }
    }
}
