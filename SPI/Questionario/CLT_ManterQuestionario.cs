using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BibliotecaComponentesInteracao;
using System.Reflection;

namespace SPI
{
    public partial class CLT_ManterQuestionario : Form
    {
        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        public string stCodigoBarras = "";

        public string stControle = string.Empty;
        public string stMultipla = string.Empty;
        string stNumeroPerguntaAtual = string.Empty;
        string[] stValue = new string[200];
        string[] stValueOutros = new string[200];
        public string stValidaQuestionario = string.Empty;

        CTO_Visitante ITO_VisitanteDadosCadastrais = null;
     
        public void setVisitante(CTO_Visitante prITO_Visitante)
        {
            ITO_VisitanteDadosCadastrais = prITO_Visitante;
        }

        public CLT_ManterQuestionario()
        {
            InitializeComponent();
        }

        private void MI_CopyCTOToControls(CTO_Visitante ITO_Visitante)
        {
            int i = 0;
          
            //Limpa os vetores
            //------------------------------------
            for (i = 0; i < 199; i++)
                stValue[i] = string.Empty;

            for (i = 0; i < 199; i++)
                stValueOutros[i] = string.Empty;
            //------------------------------------

            //Carrega as text
            //Obs: O outros da pergunta 1 foi armazenado no index 1 para facilitar o entendimento do programador

            string stPergunta = string.Empty;

            for (i = 0; i <= Convert.ToInt32(lblQuantidadeDePerguntas.Text + 1); i++)
            {
                if (i < 9)
                    //stPergunta = "edtPergunta0" + i.ToString();
                    stPergunta = "edtPergunta" + i.ToString();
                else
                    stPergunta = "edtPergunta" + i.ToString();

                foreach (Control objControle in this.groupRespostas.Controls)
                {
                    if ((objControle is TextBox) && (objControle.Name.ToUpper() == stPergunta.ToUpper()))
                    {
                        objControle.Text = Convert.ToString(ITO_Visitante.GetType().InvokeMember("getPergunta" + i.ToString(), BindingFlags.InvokeMethod, null, ITO_Visitante, new object[] { }));
                        stValueOutros[i] = Convert.ToString(ITO_Visitante.GetType().InvokeMember("getPergunta" + i.ToString() + "Outro", BindingFlags.InvokeMethod, null, ITO_Visitante, new object[] { }));
                    }
                }
            }
        }

        private void CLT_ManterQuestionario_Load(object sender, EventArgs e)
        {
            MI_SalvarRegistro();
        }

        private void MI_PreencheAlternativas(string stPergunta)
        {
            string stAlternartivas = string.Empty;

            //Obtem o valor da text
            foreach (Control objControle in this.groupRespostas.Controls)
            {
                if ((objControle is TextBox) && (objControle.Name.ToUpper() == "EDTPERGUNTA" + stPergunta))
                    stAlternartivas = objControle.Text;
            }

            //Atribui os valores

            for (int i = 0; chkAlternativas.Items.Count - 1 >= i; i++)
            {
                if (stAlternartivas.Contains(stValue[i].ToString()))
                    chkAlternativas.SetItemChecked(i, true);
                else
                    chkAlternativas.SetItemChecked(i, false);
            }

            //Atribui o valor do compo outros
            try
            {
                edtOutros.Text = stValueOutros[Convert.ToInt32(stPergunta)].ToString();
            }
            catch
            {
                edtOutros.Text = string.Empty;
            }

        }

        private void chkAlternativas_SelectedIndexChanged(object sender, EventArgs e)
        {
            MI_PreencheRespostas();
        }

        private void chkAlternativas_KeyPress(object sender, KeyPressEventArgs e)
        {
            MI_PreencheRespostas();
        }

        private void MI_PreencheRespostas()
        {
            MI_AtribuiValor("edtPergunta" + stNumeroPerguntaAtual, MI_ObterValueChk());
        }

        private void MI_AtribuiValor(string stPergunta, string stRespostas)
        {
            foreach (Control objControle in this.groupRespostas.Controls)
            {
                if ((objControle is TextBox) && (objControle.Name.ToUpper() == stPergunta.ToUpper()))
                    objControle.Text = stRespostas;
            }
        }

        private string MI_ObterValueChk()
        {
            string stValuee = string.Empty;
            int itIndex = 199;

            itIndex = chkAlternativas.SelectedIndex;

            foreach (Int32 che in chkAlternativas.CheckedIndices) 
                 stValuee += stValue[che] + ";";

            return stValuee;
        }

        private void chkAlternativas_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (stMultipla == "N")
            {
                foreach (Int32 I in chkAlternativas.CheckedIndices)
                {
                    if (I != e.Index)
                        chkAlternativas.SetItemChecked(I, false);
                }
            }
        }

        private void btSalvar_Click(object sender, EventArgs e)
        {
            if (MI_Consistencias())
            {
                MI_SalvarRegistro();
            }
        }

        protected void MI_SalvarRegistro()
        {
            if (MI_Consistencias())
            {
                //  COPIAR PARA O GERADOR
                CTO_Visitante ITO_Visitante = new CTO_Visitante();
                ITO_Visitante = ITO_VisitanteDadosCadastrais;
                CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();

                CAT_CadastrarCodigoPagamento IAT_CodigoPagamento = new CAT_CadastrarCodigoPagamento();
                CTO_CodigoPagamento ITO_CodigoPagamento = new CTO_CodigoPagamento();

                //-----------------------------------------------------------------------------------------------------

                if (ITO_Visitante.getCodigoBarras() != "") // Alteracao
                {
                    ITO_Visitante.setOrigemAlteracao("CREDENCIAMENTO");
                    ITO_Visitante.setDataOrigemAlteracao(DateTime.Today);
                    ITO_Visitante.setHoraOrigemAlteracao(CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5));
                    ITO_Visitante.setUsuarioOrigemAlteracao(CSV_Global.getUsuario());

                    stCodigoBarras = ITO_Visitante.getCodigoBarras(); 

                    IAT_CadastrarVisitante.MS_Alterar(ITO_Visitante);

                    MessageBox.Show("Altera��o realizada com sucesso!","Confirma��o!"); 
                }
                else
                {
                    ITO_Visitante.setNumeroTerminal(CSV_Global.getNumeroTerminal());
                    ITO_Visitante.setCodigoBarras(MI_GerarCodigoBarras(ITO_Visitante.getNumeroTerminal()));
                    ITO_Visitante.setChave(Convert.ToInt32(ITO_Visitante.getCodigoBarras().Substring(3)));
                    ITO_Visitante.setEmissaoCracha("N");
                    ITO_Visitante.setPresenteDia1("N");
                    ITO_Visitante.setPresenteDia2("N");
                    ITO_Visitante.setPresenteDia3("N");
                    ITO_Visitante.setPresenteDia4("N");
                    ITO_Visitante.setPresenteDia5("N");
                    ITO_Visitante.setOrigemInsercao("CREDENCIAMENTO");
                    ITO_Visitante.setDataOrigemInsercao(DateTime.Today);
                    ITO_Visitante.setHoraOrigemInsercao(CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5));
                    ITO_Visitante.setUsuarioOrigemInsercao(CSV_Global.getUsuario());
                    ITO_Visitante.setOrigemAlteracao("CREDENCIAMENTO");
                    ITO_Visitante.setDataOrigemAlteracao(DateTime.Today);
                    ITO_Visitante.setHoraOrigemAlteracao(CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5));
                    ITO_Visitante.setUsuarioOrigemAlteracao("CREDENCIAMENTO");
                    ITO_Visitante.setDataOrigemAlteracao(DateTime.Today);
                    ITO_Visitante.setHoraOrigemAlteracao(CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5));
                    ITO_Visitante.setUsuarioOrigemAlteracao("CREDENCIAMENTO");
                    ITO_Visitante.setMetodoInsercao("CREDENCIAMENTO");
                    ITO_Visitante.setParticipouEdicao(MI_ObterEvento());
                    ITO_Visitante.setOrigemConvite("");

                    stCodigoBarras = ITO_Visitante.getCodigoBarras();
                    IAT_CadastrarVisitante.MS_Incluir(ITO_Visitante);

                    // INSERIR AREA DE ASSENTO
                    if ((ITO_Visitante.getTipodeInscricao().ToUpper().Trim().Contains("CONVIDADOS")) || ("CONVIDADOS").Contains(ITO_Visitante.getTipodeInscricao().ToUpper().Trim()))
                    {
                        // A AREA DE ASSENTO DO CONVIDADO SER� A MESMA DA CONSULTORA
                    }
                    else
                    {
                        CTO_Reconhecimento ITO_Reconhecimento = new CTO_Reconhecimento();
                        ITO_Reconhecimento.AP_cdInscricao = 0;
                        ITO_Reconhecimento.AP_cdRoteiro = 3730; // Area de Assento: Roxo
                        ITO_Reconhecimento.AP_Consultora = ITO_Visitante.getConsultora().ToString();
                        ITO_Reconhecimento.AP_dsMetodoInsert = "AUTOM�TICO AREA DE ASSENTO - CREDENCIAMENTO NOVAS";
                        new DAO_Login().MS_IncluirReconhecimento(ITO_Reconhecimento);


                        ITO_Reconhecimento = new CTO_Reconhecimento();
                        ITO_Reconhecimento.AP_cdInscricao = 0;
                        ITO_Reconhecimento.AP_cdRoteiro = 3933; // Orienta��o Plenaria
                        ITO_Reconhecimento.AP_Consultora = ITO_Visitante.getConsultora().ToString();
                        ITO_Reconhecimento.AP_dsMetodoInsert = "AUTOM�TICO ORIENTA��O PLEN�RIA - CREDENCIAMENTO NOVAS";
                        new DAO_Login().MS_IncluirReconhecimento(ITO_Reconhecimento);
                    }

                    //GERAR C�DIGO DA CONSULTORA
                    CAT_CadastrarVisitante IAT_VisitanteAtualizacao = new CAT_CadastrarVisitante();
                    CTO_Visitante ITO_VisitanteAtualizacao = IAT_VisitanteAtualizacao.MS_ObterPorCodigo(stCodigoBarras);

                    if (ITO_VisitanteAtualizacao.getConsultora() == string.Empty)
                    {
                        ITO_VisitanteAtualizacao.setConsultora("INT" + ITO_VisitanteAtualizacao.getCodigoBarras());
                        IAT_VisitanteAtualizacao.MS_Alterar(ITO_VisitanteAtualizacao);
                    }

                    MessageBox.Show("Cadastro realizado com sucesso!","Confirma��o!"); 

                    //  FIM COPIAR PARA O GERADOR
                }
                Close();
            }
        }

        private string MI_ObterEvento()
        {
            return CSV_Global.getNomeEvento();
        }

        private string MI_GerarCodigoBarras(string stNTerminal)
        {
            string stCodigoBarras = stNTerminal;

            CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();
            stCodigoBarras = stCodigoBarras + IAT_CadastrarVisitante.MS_GerarCodigoBarras();

            return stCodigoBarras;
        }

       

        private bool MI_Consistencias()
        {
            bool boReturn = true;
            return true;

            if (stValidaQuestionario == "VALIDAR")
            {
                if (edtPergunta1.Text == string.Empty)
                {
                    MessageBox.Show("Responda � pergunta n�mero 1", "ATEN��O");
                    return boReturn = false;
                }
                if (edtPergunta2.Text == string.Empty)
                {
                    MessageBox.Show("Responda � pergunta n�mero 2", "ATEN��O");
                    return boReturn = false;
                }
                if (edtPergunta3.Text == string.Empty)
                {
                    MessageBox.Show("Responda � pergunta n�mero 3", "ATEN��O");
                    return boReturn = false;
                }
                if (edtPergunta4.Text == string.Empty)
                {
                    MessageBox.Show("Responda � pergunta n�mero 4", "ATEN��O");
                    return boReturn = false;
                }
                if (edtPergunta5.Text == string.Empty)
                {
                    MessageBox.Show("Responda � pergunta n�mero 5", "ATEN��O");
                    return boReturn = false;
                }

            }

            return boReturn;
        }
                
        private void btVoltar_Click(object sender, EventArgs e)
        {
            stControle = "1";
            Close();
        }

        private void btPerguntaAnterior_Click(object sender, EventArgs e)
        {
            //int itPerguntaAT = 0;
            //int itPerguntaFI = 0;

            //itPerguntaAT = Convert.ToInt32(lblNumeroDaPergunta.Text);
            //itPerguntaFI = Convert.ToInt32(lblQuantidadeDePerguntas.Text);

            //if (itPerguntaAT >= 2)
            //{
            //    itPerguntaAT--;
            //    //MI_CarregaPergunta("0" + itPerguntaAT.ToString());
            //    MI_CarregaPergunta(itPerguntaAT.ToString());
            //    chkAlternativas.Focus();
            //}
        }

        private void btProximaPergunta_Click(object sender, EventArgs e)
        {
            //int itPerguntaAT = 0;
            //int itPerguntaFI = 0;

            //itPerguntaAT = Convert.ToInt32(lblNumeroDaPergunta.Text);
            //itPerguntaFI = Convert.ToInt32(lblQuantidadeDePerguntas.Text);

            //if (itPerguntaAT < itPerguntaFI)
            //{
            //    itPerguntaAT++;
            //    //MI_CarregaPergunta("0" + itPerguntaAT.ToString());
            //    MI_CarregaPergunta(itPerguntaAT.ToString());
            //    chkAlternativas.Focus();
            //}
        }

        private void btPergunta01_Click(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 1)
            //    //MI_CarregaPergunta("01");
            //    MI_CarregaPergunta("1");
            //else
            //    MessageBox.Show("A pergunta 1 n�o existe!", "Aten��o!"); 
        }

        private void btPergunta02_Click(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 2)
            //    //MI_CarregaPergunta("02");
            //    MI_CarregaPergunta("2");
            //else
            //    MessageBox.Show("A pergunta 2 n�o existe!", "Aten��o!"); 
        }

        private void btPergunta03_Click(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 3)
            //    MI_CarregaPergunta("03");
            //    //MI_CarregaPergunta("3");
            //else
            //    MessageBox.Show("A pergunta 3 n�o existe!", "Aten��o!"); 
        }

        private void btPergunta04_Click(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 4)
            //    //MI_CarregaPergunta("04");
            //    MI_CarregaPergunta("4");
            //else
            //    MessageBox.Show("A pergunta 4 n�o existe!", "Aten��o!"); 
        }

        private void btPergunta05_Click(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 5)
            //    //MI_CarregaPergunta("05");
            //    MI_CarregaPergunta("5");
            //else
            //    MessageBox.Show("A pergunta 5 n�o existe!", "Aten��o!"); 
        }

        private void btPergunta06_Click(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 6)
            //    //MI_CarregaPergunta("06");
            //    MI_CarregaPergunta("6");
            //else
            //    MessageBox.Show("A pergunta 6 n�o existe!", "Aten��o!"); 
        }

        private void btPergunta07_Click(object sender, EventArgs e)
        {
        //    if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 7)
        //        //MI_CarregaPergunta("07");
        //        MI_CarregaPergunta("7");
        //    else
        //        MessageBox.Show("A pergunta 7 n�o existe!", "Aten��o!"); 
        }

        private void btPergunta08_Click(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 8)
            //    //MI_CarregaPergunta("08");
            //    MI_CarregaPergunta("8");
            //else
            //    MessageBox.Show("A pergunta 8 n�o existe!", "Aten��o!"); 
        }

        private void btPergunta09_Click(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 9)
            //    //MI_CarregaPergunta("09");
            //    MI_CarregaPergunta("9");
            //else
            //    MessageBox.Show("A pergunta 9 n�o existe!", "Aten��o!"); 
        }

        private void btPergunta010_Click(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 10)
            //    MI_CarregaPergunta("10");
            //else
            //    MessageBox.Show("A pergunta 10 n�o existe!", "Aten��o!"); 
        }

        private void edtPergunta01_TextChanged(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 1)
            //    //MI_PreencheAlternativas("01"); 
            //    MI_PreencheAlternativas("1"); 
        }

        private void edtPergunta02_TextChanged(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 2)
            //    //MI_PreencheAlternativas("02"); 
            //    MI_PreencheAlternativas("2"); 
        }

        private void edtPergunta03_TextChanged(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 3)
            //    //MI_PreencheAlternativas("03");
            //    MI_PreencheAlternativas("3");
        }

        private void edtPergunta04_TextChanged(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 4)
            //    //MI_PreencheAlternativas("04");
            //    MI_PreencheAlternativas("4");
        }

        private void edtPergunta05_TextChanged(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 5)
            //    //MI_PreencheAlternativas("05");
            //    MI_PreencheAlternativas("5");
        }

        private void edtPergunta06_TextChanged(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 6)
            //    //MI_PreencheAlternativas("06");
            //    MI_PreencheAlternativas("6");
        }

        private void edtPergunta07_TextChanged(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 7)
            //    //MI_PreencheAlternativas("07");
            //    MI_PreencheAlternativas("7");
        }

        private void edtPergunta08_TextChanged(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 8)
            //    //MI_PreencheAlternativas("08");
            //    MI_PreencheAlternativas("8");
        }

        private void edtPergunta09_TextChanged(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 9)
            //    //MI_PreencheAlternativas("09");
            //    MI_PreencheAlternativas("9");
        }

        private void edtPergunta10_TextChanged(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(lblQuantidadeDePerguntas.Text) >= 10)
            //    MI_PreencheAlternativas("10");
        }

        private void ChkHabilitarCampos_CheckedChanged(object sender, EventArgs e)
        {
            //if (ChkHabilitarCampos.Checked == true)
            //    MI_ControlaCampos(true);
            //else
            //    MI_ControlaCampos(false);
        }

        private void MI_ControlaCampos(bool boValor)
        {
            //edtPergunta1.ReadOnly = !boValor;
            //edtPergunta2.ReadOnly = !boValor;
            //edtPergunta3.ReadOnly = !boValor;
            //edtPergunta4.ReadOnly = !boValor;
            //edtPergunta5.ReadOnly = !boValor;
            //edtPergunta6.ReadOnly = !boValor;
            //edtPergunta7.ReadOnly = !boValor;
            //edtPergunta8.ReadOnly = !boValor;
            //edtPergunta9.ReadOnly = !boValor;
            //edtPergunta10.ReadOnly = !boValor;
        }
    }
}