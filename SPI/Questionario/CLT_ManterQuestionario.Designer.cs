namespace SPI
{
    partial class CLT_ManterQuestionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CLT_ManterQuestionario));
            this.label1 = new System.Windows.Forms.Label();
            this.group = new System.Windows.Forms.GroupBox();
            this.chkAlternativas = new System.Windows.Forms.CheckedListBox();
            this.btSalvar = new System.Windows.Forms.Button();
            this.btVoltar = new System.Windows.Forms.Button();
            this.edtPergunta1 = new System.Windows.Forms.TextBox();
            this.groupRespostas = new System.Windows.Forms.GroupBox();
            this.ChkHabilitarCampos = new System.Windows.Forms.CheckBox();
            this.btPergunta010 = new System.Windows.Forms.PictureBox();
            this.btPergunta09 = new System.Windows.Forms.PictureBox();
            this.btPergunta08 = new System.Windows.Forms.PictureBox();
            this.btPergunta07 = new System.Windows.Forms.PictureBox();
            this.btPergunta06 = new System.Windows.Forms.PictureBox();
            this.btPergunta05 = new System.Windows.Forms.PictureBox();
            this.btPergunta04 = new System.Windows.Forms.PictureBox();
            this.btPergunta03 = new System.Windows.Forms.PictureBox();
            this.btPergunta02 = new System.Windows.Forms.PictureBox();
            this.btPergunta01 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.edtPergunta2 = new System.Windows.Forms.TextBox();
            this.edtPergunta3 = new System.Windows.Forms.TextBox();
            this.edtPergunta4 = new System.Windows.Forms.TextBox();
            this.edtPergunta5 = new System.Windows.Forms.TextBox();
            this.edtPergunta6 = new System.Windows.Forms.TextBox();
            this.edtPergunta7 = new System.Windows.Forms.TextBox();
            this.edtPergunta8 = new System.Windows.Forms.TextBox();
            this.edtPergunta9 = new System.Windows.Forms.TextBox();
            this.edtPergunta10 = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.edtOutros = new System.Windows.Forms.TextBox();
            this.btPerguntaAnterior = new System.Windows.Forms.Button();
            this.btProximaPergunta = new System.Windows.Forms.Button();
            this.lblNumeroDaPergunta = new System.Windows.Forms.Label();
            this.lblQuantidadeDePerguntas = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.group.SuspendLayout();
            this.groupRespostas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta010)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta01)).BeginInit();
            this.groupBox12.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(367, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Armazenando informa��es, aguarde...";
            // 
            // group
            // 
            this.group.BackColor = System.Drawing.Color.Transparent;
            this.group.Controls.Add(this.chkAlternativas);
            this.group.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.group.Location = new System.Drawing.Point(20, 71);
            this.group.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.group.Name = "group";
            this.group.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.group.Size = new System.Drawing.Size(872, 30);
            this.group.TabIndex = 1;
            this.group.TabStop = false;
            this.group.Visible = false;
            // 
            // chkAlternativas
            // 
            this.chkAlternativas.CheckOnClick = true;
            this.chkAlternativas.ColumnWidth = 300;
            this.chkAlternativas.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlternativas.FormattingEnabled = true;
            this.chkAlternativas.HorizontalExtent = 1;
            this.chkAlternativas.Location = new System.Drawing.Point(19, 20);
            this.chkAlternativas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAlternativas.MultiColumn = true;
            this.chkAlternativas.Name = "chkAlternativas";
            this.chkAlternativas.Size = new System.Drawing.Size(835, 340);
            this.chkAlternativas.TabIndex = 0;
            this.chkAlternativas.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.chkAlternativas_ItemCheck);
            this.chkAlternativas.SelectedIndexChanged += new System.EventHandler(this.chkAlternativas_SelectedIndexChanged);
            this.chkAlternativas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chkAlternativas_KeyPress);
            // 
            // btSalvar
            // 
            this.btSalvar.Location = new System.Drawing.Point(20, 849);
            this.btSalvar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btSalvar.Name = "btSalvar";
            this.btSalvar.Size = new System.Drawing.Size(124, 46);
            this.btSalvar.TabIndex = 14;
            this.btSalvar.Text = "&Salvar";
            this.btSalvar.UseVisualStyleBackColor = true;
            this.btSalvar.Visible = false;
            this.btSalvar.Click += new System.EventHandler(this.btSalvar_Click);
            // 
            // btVoltar
            // 
            this.btVoltar.Location = new System.Drawing.Point(1233, 849);
            this.btVoltar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btVoltar.Name = "btVoltar";
            this.btVoltar.Size = new System.Drawing.Size(124, 46);
            this.btVoltar.TabIndex = 15;
            this.btVoltar.Text = "&Fechar";
            this.btVoltar.UseVisualStyleBackColor = true;
            this.btVoltar.Visible = false;
            this.btVoltar.Click += new System.EventHandler(this.btVoltar_Click);
            // 
            // edtPergunta1
            // 
            this.edtPergunta1.Location = new System.Drawing.Point(17, 46);
            this.edtPergunta1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edtPergunta1.Name = "edtPergunta1";
            this.edtPergunta1.ReadOnly = true;
            this.edtPergunta1.Size = new System.Drawing.Size(379, 22);
            this.edtPergunta1.TabIndex = 4;
            this.edtPergunta1.TextChanged += new System.EventHandler(this.edtPergunta01_TextChanged);
            // 
            // groupRespostas
            // 
            this.groupRespostas.Controls.Add(this.ChkHabilitarCampos);
            this.groupRespostas.Controls.Add(this.btPergunta010);
            this.groupRespostas.Controls.Add(this.btPergunta09);
            this.groupRespostas.Controls.Add(this.btPergunta08);
            this.groupRespostas.Controls.Add(this.btPergunta07);
            this.groupRespostas.Controls.Add(this.btPergunta06);
            this.groupRespostas.Controls.Add(this.btPergunta05);
            this.groupRespostas.Controls.Add(this.btPergunta04);
            this.groupRespostas.Controls.Add(this.btPergunta03);
            this.groupRespostas.Controls.Add(this.btPergunta02);
            this.groupRespostas.Controls.Add(this.btPergunta01);
            this.groupRespostas.Controls.Add(this.label12);
            this.groupRespostas.Controls.Add(this.label11);
            this.groupRespostas.Controls.Add(this.label10);
            this.groupRespostas.Controls.Add(this.label9);
            this.groupRespostas.Controls.Add(this.label8);
            this.groupRespostas.Controls.Add(this.label7);
            this.groupRespostas.Controls.Add(this.label6);
            this.groupRespostas.Controls.Add(this.label5);
            this.groupRespostas.Controls.Add(this.label4);
            this.groupRespostas.Controls.Add(this.label3);
            this.groupRespostas.Controls.Add(this.edtPergunta1);
            this.groupRespostas.Controls.Add(this.edtPergunta2);
            this.groupRespostas.Controls.Add(this.edtPergunta3);
            this.groupRespostas.Controls.Add(this.edtPergunta4);
            this.groupRespostas.Controls.Add(this.edtPergunta5);
            this.groupRespostas.Controls.Add(this.edtPergunta6);
            this.groupRespostas.Controls.Add(this.edtPergunta7);
            this.groupRespostas.Controls.Add(this.edtPergunta8);
            this.groupRespostas.Controls.Add(this.edtPergunta9);
            this.groupRespostas.Controls.Add(this.edtPergunta10);
            this.groupRespostas.Location = new System.Drawing.Point(915, 11);
            this.groupRespostas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupRespostas.Name = "groupRespostas";
            this.groupRespostas.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupRespostas.Size = new System.Drawing.Size(443, 22);
            this.groupRespostas.TabIndex = 11;
            this.groupRespostas.TabStop = false;
            this.groupRespostas.Text = "RESPOSTAS DAS QUEST�ES";
            this.groupRespostas.Visible = false;
            // 
            // ChkHabilitarCampos
            // 
            this.ChkHabilitarCampos.AutoSize = true;
            this.ChkHabilitarCampos.Location = new System.Drawing.Point(304, 0);
            this.ChkHabilitarCampos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ChkHabilitarCampos.Name = "ChkHabilitarCampos";
            this.ChkHabilitarCampos.Size = new System.Drawing.Size(135, 21);
            this.ChkHabilitarCampos.TabIndex = 47;
            this.ChkHabilitarCampos.Text = "Habilitar campos";
            this.ChkHabilitarCampos.UseVisualStyleBackColor = true;
            this.ChkHabilitarCampos.CheckedChanged += new System.EventHandler(this.ChkHabilitarCampos_CheckedChanged);
            // 
            // btPergunta010
            // 
            this.btPergunta010.Image = ((System.Drawing.Image)(resources.GetObject("btPergunta010.Image")));
            this.btPergunta010.Location = new System.Drawing.Point(405, 473);
            this.btPergunta010.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btPergunta010.Name = "btPergunta010";
            this.btPergunta010.Size = new System.Drawing.Size(29, 30);
            this.btPergunta010.TabIndex = 46;
            this.btPergunta010.TabStop = false;
            this.btPergunta010.Click += new System.EventHandler(this.btPergunta010_Click);
            // 
            // btPergunta09
            // 
            this.btPergunta09.Image = ((System.Drawing.Image)(resources.GetObject("btPergunta09.Image")));
            this.btPergunta09.Location = new System.Drawing.Point(405, 425);
            this.btPergunta09.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btPergunta09.Name = "btPergunta09";
            this.btPergunta09.Size = new System.Drawing.Size(29, 30);
            this.btPergunta09.TabIndex = 45;
            this.btPergunta09.TabStop = false;
            this.btPergunta09.Click += new System.EventHandler(this.btPergunta09_Click);
            // 
            // btPergunta08
            // 
            this.btPergunta08.Image = ((System.Drawing.Image)(resources.GetObject("btPergunta08.Image")));
            this.btPergunta08.Location = new System.Drawing.Point(405, 377);
            this.btPergunta08.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btPergunta08.Name = "btPergunta08";
            this.btPergunta08.Size = new System.Drawing.Size(29, 30);
            this.btPergunta08.TabIndex = 44;
            this.btPergunta08.TabStop = false;
            this.btPergunta08.Click += new System.EventHandler(this.btPergunta08_Click);
            // 
            // btPergunta07
            // 
            this.btPergunta07.Image = ((System.Drawing.Image)(resources.GetObject("btPergunta07.Image")));
            this.btPergunta07.Location = new System.Drawing.Point(405, 329);
            this.btPergunta07.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btPergunta07.Name = "btPergunta07";
            this.btPergunta07.Size = new System.Drawing.Size(29, 30);
            this.btPergunta07.TabIndex = 43;
            this.btPergunta07.TabStop = false;
            this.btPergunta07.Click += new System.EventHandler(this.btPergunta07_Click);
            // 
            // btPergunta06
            // 
            this.btPergunta06.Image = ((System.Drawing.Image)(resources.GetObject("btPergunta06.Image")));
            this.btPergunta06.Location = new System.Drawing.Point(405, 281);
            this.btPergunta06.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btPergunta06.Name = "btPergunta06";
            this.btPergunta06.Size = new System.Drawing.Size(29, 30);
            this.btPergunta06.TabIndex = 42;
            this.btPergunta06.TabStop = false;
            this.btPergunta06.Click += new System.EventHandler(this.btPergunta06_Click);
            // 
            // btPergunta05
            // 
            this.btPergunta05.Image = ((System.Drawing.Image)(resources.GetObject("btPergunta05.Image")));
            this.btPergunta05.Location = new System.Drawing.Point(405, 233);
            this.btPergunta05.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btPergunta05.Name = "btPergunta05";
            this.btPergunta05.Size = new System.Drawing.Size(29, 30);
            this.btPergunta05.TabIndex = 41;
            this.btPergunta05.TabStop = false;
            this.btPergunta05.Click += new System.EventHandler(this.btPergunta05_Click);
            // 
            // btPergunta04
            // 
            this.btPergunta04.Image = ((System.Drawing.Image)(resources.GetObject("btPergunta04.Image")));
            this.btPergunta04.Location = new System.Drawing.Point(405, 185);
            this.btPergunta04.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btPergunta04.Name = "btPergunta04";
            this.btPergunta04.Size = new System.Drawing.Size(29, 30);
            this.btPergunta04.TabIndex = 40;
            this.btPergunta04.TabStop = false;
            this.btPergunta04.Click += new System.EventHandler(this.btPergunta04_Click);
            // 
            // btPergunta03
            // 
            this.btPergunta03.Image = ((System.Drawing.Image)(resources.GetObject("btPergunta03.Image")));
            this.btPergunta03.Location = new System.Drawing.Point(405, 137);
            this.btPergunta03.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btPergunta03.Name = "btPergunta03";
            this.btPergunta03.Size = new System.Drawing.Size(29, 30);
            this.btPergunta03.TabIndex = 39;
            this.btPergunta03.TabStop = false;
            this.btPergunta03.Click += new System.EventHandler(this.btPergunta03_Click);
            // 
            // btPergunta02
            // 
            this.btPergunta02.Image = ((System.Drawing.Image)(resources.GetObject("btPergunta02.Image")));
            this.btPergunta02.Location = new System.Drawing.Point(405, 89);
            this.btPergunta02.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btPergunta02.Name = "btPergunta02";
            this.btPergunta02.Size = new System.Drawing.Size(29, 30);
            this.btPergunta02.TabIndex = 38;
            this.btPergunta02.TabStop = false;
            this.btPergunta02.Click += new System.EventHandler(this.btPergunta02_Click);
            // 
            // btPergunta01
            // 
            this.btPergunta01.Image = ((System.Drawing.Image)(resources.GetObject("btPergunta01.Image")));
            this.btPergunta01.Location = new System.Drawing.Point(405, 41);
            this.btPergunta01.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btPergunta01.Name = "btPergunta01";
            this.btPergunta01.Size = new System.Drawing.Size(29, 30);
            this.btPergunta01.TabIndex = 37;
            this.btPergunta01.TabStop = false;
            this.btPergunta01.Click += new System.EventHandler(this.btPergunta01_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(17, 458);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 17);
            this.label12.TabIndex = 26;
            this.label12.Text = "PERGUNTA 10";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(17, 410);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 17);
            this.label11.TabIndex = 25;
            this.label11.Text = "PERGUNTA 9";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(13, 362);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 17);
            this.label10.TabIndex = 24;
            this.label10.Text = "PERGUNTA 8";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(13, 314);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 17);
            this.label9.TabIndex = 23;
            this.label9.Text = "PERGUNTA 7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(17, 266);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 17);
            this.label8.TabIndex = 22;
            this.label8.Text = "PERGUNTA 6";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(17, 218);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 17);
            this.label7.TabIndex = 21;
            this.label7.Text = "PERGUNTA 5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(13, 170);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 17);
            this.label6.TabIndex = 20;
            this.label6.Text = "PERGUNTA 4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(13, 122);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 17);
            this.label5.TabIndex = 19;
            this.label5.Text = "PERGUNTA 3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(13, 74);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "PERGUNTA 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(13, 26);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "PERGUNTA 1";
            // 
            // edtPergunta2
            // 
            this.edtPergunta2.Location = new System.Drawing.Point(17, 94);
            this.edtPergunta2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edtPergunta2.Name = "edtPergunta2";
            this.edtPergunta2.ReadOnly = true;
            this.edtPergunta2.Size = new System.Drawing.Size(379, 22);
            this.edtPergunta2.TabIndex = 5;
            this.edtPergunta2.TextChanged += new System.EventHandler(this.edtPergunta02_TextChanged);
            // 
            // edtPergunta3
            // 
            this.edtPergunta3.Location = new System.Drawing.Point(17, 142);
            this.edtPergunta3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edtPergunta3.Name = "edtPergunta3";
            this.edtPergunta3.ReadOnly = true;
            this.edtPergunta3.Size = new System.Drawing.Size(379, 22);
            this.edtPergunta3.TabIndex = 6;
            this.edtPergunta3.TextChanged += new System.EventHandler(this.edtPergunta03_TextChanged);
            // 
            // edtPergunta4
            // 
            this.edtPergunta4.Location = new System.Drawing.Point(17, 190);
            this.edtPergunta4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edtPergunta4.Name = "edtPergunta4";
            this.edtPergunta4.ReadOnly = true;
            this.edtPergunta4.Size = new System.Drawing.Size(379, 22);
            this.edtPergunta4.TabIndex = 7;
            this.edtPergunta4.TextChanged += new System.EventHandler(this.edtPergunta04_TextChanged);
            // 
            // edtPergunta5
            // 
            this.edtPergunta5.Location = new System.Drawing.Point(17, 238);
            this.edtPergunta5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edtPergunta5.Name = "edtPergunta5";
            this.edtPergunta5.ReadOnly = true;
            this.edtPergunta5.Size = new System.Drawing.Size(379, 22);
            this.edtPergunta5.TabIndex = 8;
            this.edtPergunta5.TextChanged += new System.EventHandler(this.edtPergunta05_TextChanged);
            // 
            // edtPergunta6
            // 
            this.edtPergunta6.Location = new System.Drawing.Point(17, 286);
            this.edtPergunta6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edtPergunta6.Name = "edtPergunta6";
            this.edtPergunta6.ReadOnly = true;
            this.edtPergunta6.Size = new System.Drawing.Size(379, 22);
            this.edtPergunta6.TabIndex = 9;
            this.edtPergunta6.TextChanged += new System.EventHandler(this.edtPergunta06_TextChanged);
            // 
            // edtPergunta7
            // 
            this.edtPergunta7.Location = new System.Drawing.Point(17, 334);
            this.edtPergunta7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edtPergunta7.Name = "edtPergunta7";
            this.edtPergunta7.ReadOnly = true;
            this.edtPergunta7.Size = new System.Drawing.Size(379, 22);
            this.edtPergunta7.TabIndex = 10;
            this.edtPergunta7.TextChanged += new System.EventHandler(this.edtPergunta07_TextChanged);
            // 
            // edtPergunta8
            // 
            this.edtPergunta8.Location = new System.Drawing.Point(17, 382);
            this.edtPergunta8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edtPergunta8.Name = "edtPergunta8";
            this.edtPergunta8.ReadOnly = true;
            this.edtPergunta8.Size = new System.Drawing.Size(379, 22);
            this.edtPergunta8.TabIndex = 11;
            this.edtPergunta8.TextChanged += new System.EventHandler(this.edtPergunta08_TextChanged);
            // 
            // edtPergunta9
            // 
            this.edtPergunta9.Location = new System.Drawing.Point(17, 430);
            this.edtPergunta9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edtPergunta9.Name = "edtPergunta9";
            this.edtPergunta9.ReadOnly = true;
            this.edtPergunta9.Size = new System.Drawing.Size(379, 22);
            this.edtPergunta9.TabIndex = 12;
            this.edtPergunta9.TextChanged += new System.EventHandler(this.edtPergunta09_TextChanged);
            // 
            // edtPergunta10
            // 
            this.edtPergunta10.Location = new System.Drawing.Point(17, 478);
            this.edtPergunta10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edtPergunta10.Name = "edtPergunta10";
            this.edtPergunta10.ReadOnly = true;
            this.edtPergunta10.Size = new System.Drawing.Size(379, 22);
            this.edtPergunta10.TabIndex = 13;
            this.edtPergunta10.TextChanged += new System.EventHandler(this.edtPergunta10_TextChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Location = new System.Drawing.Point(20, 831);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox9.Size = new System.Drawing.Size(1337, 10);
            this.groupBox9.TabIndex = 61;
            this.groupBox9.TabStop = false;
            this.groupBox9.Visible = false;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.edtOutros);
            this.groupBox12.Location = new System.Drawing.Point(20, 460);
            this.groupBox12.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox12.Size = new System.Drawing.Size(872, 57);
            this.groupBox12.TabIndex = 62;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "OUTROS";
            this.groupBox12.Visible = false;
            // 
            // edtOutros
            // 
            this.edtOutros.Location = new System.Drawing.Point(23, 20);
            this.edtOutros.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edtOutros.Name = "edtOutros";
            this.edtOutros.Size = new System.Drawing.Size(835, 22);
            this.edtOutros.TabIndex = 1;
            // 
            // btPerguntaAnterior
            // 
            this.btPerguntaAnterior.Image = ((System.Drawing.Image)(resources.GetObject("btPerguntaAnterior.Image")));
            this.btPerguntaAnterior.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btPerguntaAnterior.Location = new System.Drawing.Point(548, 636);
            this.btPerguntaAnterior.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btPerguntaAnterior.Name = "btPerguntaAnterior";
            this.btPerguntaAnterior.Size = new System.Drawing.Size(137, 96);
            this.btPerguntaAnterior.TabIndex = 2;
            this.btPerguntaAnterior.Text = "&Voltar";
            this.btPerguntaAnterior.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btPerguntaAnterior.UseVisualStyleBackColor = true;
            this.btPerguntaAnterior.Visible = false;
            this.btPerguntaAnterior.Click += new System.EventHandler(this.btPerguntaAnterior_Click);
            // 
            // btProximaPergunta
            // 
            this.btProximaPergunta.Image = ((System.Drawing.Image)(resources.GetObject("btProximaPergunta.Image")));
            this.btProximaPergunta.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btProximaPergunta.Location = new System.Drawing.Point(688, 636);
            this.btProximaPergunta.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btProximaPergunta.Name = "btProximaPergunta";
            this.btProximaPergunta.Size = new System.Drawing.Size(137, 96);
            this.btProximaPergunta.TabIndex = 3;
            this.btProximaPergunta.Text = "&Pr�xima pergunta";
            this.btProximaPergunta.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btProximaPergunta.UseVisualStyleBackColor = true;
            this.btProximaPergunta.Visible = false;
            this.btProximaPergunta.Click += new System.EventHandler(this.btProximaPergunta_Click);
            // 
            // lblNumeroDaPergunta
            // 
            this.lblNumeroDaPergunta.AutoSize = true;
            this.lblNumeroDaPergunta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroDaPergunta.Location = new System.Drawing.Point(548, 619);
            this.lblNumeroDaPergunta.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumeroDaPergunta.Name = "lblNumeroDaPergunta";
            this.lblNumeroDaPergunta.Size = new System.Drawing.Size(26, 17);
            this.lblNumeroDaPergunta.TabIndex = 65;
            this.lblNumeroDaPergunta.Text = "10";
            this.lblNumeroDaPergunta.Visible = false;
            // 
            // lblQuantidadeDePerguntas
            // 
            this.lblQuantidadeDePerguntas.AutoSize = true;
            this.lblQuantidadeDePerguntas.Location = new System.Drawing.Point(615, 619);
            this.lblQuantidadeDePerguntas.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblQuantidadeDePerguntas.Name = "lblQuantidadeDePerguntas";
            this.lblQuantidadeDePerguntas.Size = new System.Drawing.Size(24, 17);
            this.lblQuantidadeDePerguntas.TabIndex = 66;
            this.lblQuantidadeDePerguntas.Text = "10";
            this.lblQuantidadeDePerguntas.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(583, 619);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 17);
            this.label2.TabIndex = 67;
            this.label2.Text = "de";
            this.label2.Visible = false;
            // 
            // CLT_ManterQuestionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1373, 930);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblQuantidadeDePerguntas);
            this.Controls.Add(this.lblNumeroDaPergunta);
            this.Controls.Add(this.btProximaPergunta);
            this.Controls.Add(this.btPerguntaAnterior);
            this.Controls.Add(this.groupBox12);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupRespostas);
            this.Controls.Add(this.btVoltar);
            this.Controls.Add(this.btSalvar);
            this.Controls.Add(this.group);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "CLT_ManterQuestionario";
            this.Text = "SPI - Registro";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CLT_ManterQuestionario_Load);
            this.group.ResumeLayout(false);
            this.groupRespostas.ResumeLayout(false);
            this.groupRespostas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta010)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPergunta01)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox group;
        private System.Windows.Forms.Button btSalvar;
        private System.Windows.Forms.Button btVoltar;
        private System.Windows.Forms.CheckedListBox chkAlternativas;
        private System.Windows.Forms.GroupBox groupRespostas;
        private System.Windows.Forms.TextBox edtPergunta1;
        private System.Windows.Forms.TextBox edtPergunta4;
        private System.Windows.Forms.TextBox edtPergunta3;
        private System.Windows.Forms.TextBox edtPergunta2;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox edtPergunta10;
        private System.Windows.Forms.TextBox edtPergunta9;
        private System.Windows.Forms.TextBox edtPergunta8;
        private System.Windows.Forms.TextBox edtPergunta7;
        private System.Windows.Forms.TextBox edtPergunta6;
        private System.Windows.Forms.TextBox edtPergunta5;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox edtOutros;
        private System.Windows.Forms.Button btPerguntaAnterior;
        private System.Windows.Forms.Button btProximaPergunta;
        private System.Windows.Forms.Label lblNumeroDaPergunta;
        private System.Windows.Forms.Label lblQuantidadeDePerguntas;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox btPergunta010;
        private System.Windows.Forms.PictureBox btPergunta09;
        private System.Windows.Forms.PictureBox btPergunta08;
        private System.Windows.Forms.PictureBox btPergunta07;
        private System.Windows.Forms.PictureBox btPergunta06;
        private System.Windows.Forms.PictureBox btPergunta05;
        private System.Windows.Forms.PictureBox btPergunta04;
        private System.Windows.Forms.PictureBox btPergunta03;
        private System.Windows.Forms.PictureBox btPergunta02;
        private System.Windows.Forms.PictureBox btPergunta01;
        private System.Windows.Forms.CheckBox ChkHabilitarCampos;
    }
}