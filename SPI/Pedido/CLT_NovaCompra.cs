﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace SPI
{
    public partial class CLT_NovaCompra : Form
    {
        public CLT_NovaCompra()
        {
            InitializeComponent();
        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public string AP_Codigo { get; set; }
        public string AP_Nome { get; set; }
        public string AP_Permissao { get; set; }
        public string AP_Produtos { get; set; }

        private void CLT_NovaCompra_Load(object sender, EventArgs e)
        {
            lblCodigo.Text = AP_Codigo;
            lblNome.Text = AP_Nome;
            MI_ObterProdutos();
        }

        private void MI_ObterProdutos()
        {
            CAT_CadastrarProduto IAT_Produto = new CAT_CadastrarProduto();
            DataTable dtProdutos = IAT_Produto.MS_ObterTodos();

            foreach (DataRow Row in dtProdutos.Rows)
            {
                checkedListBox1.Items.Add(Row["dsProduto"].ToString().ToUpper());
            }
        }

        private void btComprar_Click(object sender, EventArgs e)
        {
            MI_FinalizarCompra();
        }

        private void MI_FinalizarCompra()
        {
            if (MI_Consistencias())
            {
                CAT_CadastrarItensPedido IAT_Item = new CAT_CadastrarItensPedido();
                CAT_CadastrarPedido IAT_Pedido = new CAT_CadastrarPedido();
                CTO_Pedido ITO_Pedido = new CTO_Pedido();
                ITO_Pedido.setcdPedido(MI_GerarNovoPedido());
                ITO_Pedido.setcdCliente(MI_ObterIdVisitante());

                ITO_Pedido.setdsSituacao("PAGO");

                ITO_Pedido.setdsTipoPessoa(string.Empty);

                //INICIO Armazena produtos comprados no pedido

                ITO_Pedido.setdsNomeFaturamento(AP_Nome);
                //Fim Armazena produtos comprados no pedido

                ITO_Pedido.setdsValorTotal("0,00");
                ITO_Pedido.setdsValorDesconto("0,00");
                ITO_Pedido.setdtPedido(System.DateTime.Now);
                ITO_Pedido.setdsFormaPagamento("GRATUITO");

                ITO_Pedido.setdtVencimento(System.DateTime.Now);
                ITO_Pedido.setdtAlteracao(DateTime.Parse("1990-01-01"));
                ITO_Pedido.setdsObservacao(edtObservacao.Text.ToUpper());
                ITO_Pedido.setdsCpfCnpj("");
                ITO_Pedido.setdsEnderecoFaturamento("");
                ITO_Pedido.setdsCepFaturamento("");
                ITO_Pedido.setdsComplementoFaturamento("");
                ITO_Pedido.setdsBairroFaturamento("");
                ITO_Pedido.setdsCidadeFaturamento("");
                ITO_Pedido.setdsEstadoFaturamento("");
                ITO_Pedido.setdsNumeroFaturamento("");
                ITO_Pedido.setnmCongressista("");
                ITO_Pedido.setdsAlteracao("");


                //---------Insere o pedido-----------
                IAT_Pedido.MS_Incluir(ITO_Pedido);
                //-----------------------------------

                CTO_ItensPedido ITO_Item = new CTO_ItensPedido();
                CTO_Produto ITO_Produto = new CTO_Produto();
                CAT_CadastrarCodigoPagamento IAT_CodigoPagamento = new CAT_CadastrarCodigoPagamento();
                CTO_CodigoPagamento ITO_CodigoPagamento = new CTO_CodigoPagamento();

                //---------Cadastra o código de pagamento---------

                ITO_CodigoPagamento.setitCodigo(MI_GerarCodigoPagamento(ITO_Pedido.getcdPedido().ToString()));
                ITO_CodigoPagamento.setCodigoBarras(ITO_Pedido.getcdPedido().ToString());
                IAT_CodigoPagamento.MS_Incluir(ITO_CodigoPagamento);
                //------------------------------------------------

                foreach (string produto in checkedListBox1.CheckedItems)
                {
                    ITO_Item.setcdPedido(ITO_Pedido.getcdPedido());
                    ITO_Item.setcdProduto(MI_ObterIdProduto(produto));
                    ITO_Item.setdsValor("0,00");
                    IAT_Item.MS_Incluir(ITO_Item);
                }

                MessageBox.Show("Operação realizada com sucesso!", "ATENÇÃO");
                this.Close();
            }
        }

        public string MI_GerarCodigoPagamento(string stPedido)
        {
            string stPagamento = string.Empty;

            DateTime dtHora = System.DateTime.Now;

            string stHora;

            stHora = dtHora.Millisecond.ToString();

            //stPagamento = "AVS" + stPedido + stHora;
            stPagamento = "2012" + stPedido;

            return stPagamento;
        }

        private string MI_ObterIdProduto(string prSigla)
        {
            string itRetorno = string.Empty;
            CAT_CadastrarProduto IAT_Produto = new CAT_CadastrarProduto();
            CTO_Produto ITO_Produto = IAT_Produto.MS_ObterProdutoPorDescricao(prSigla);

            if (ITO_Produto != null)
            {
                itRetorno = ITO_Produto.getcdProduto();
            }

            return itRetorno;
        }

        private int MI_ObterIdVisitante()
        {
            int itCliente = 0;
            return itCliente;
            //CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            //CTO_Visitante ITO_Visitante = IAT_Visitante.MS_ObterPorCodigo(AP_Codigo);

            //if (ITO_Visitante != null)
            //{
            //    itCliente = ITO_Visitante.getObjRef();
            //}
            //else
            //{
            //    this.Close();
            //}

            return itCliente;
        }

        private int MI_GerarNovoPedido()
        {
            CAT_CadastrarPedido IAT_Pedido = new CAT_CadastrarPedido();
            int idPedido = IAT_Pedido.MS_NovoObjRef();

            return idPedido;
        }

        private bool MI_Consistencias()
        {
            bool boResult = true;
            int itQuantidade = 0;

            foreach (string produto in checkedListBox1.CheckedItems)
            {
                itQuantidade++;
            }

            if (itQuantidade == 0)
            {
                MessageBox.Show("Nenhum produto foi selecionado", "ATENÇÃO");
                return boResult = false;
            }

            return boResult;
        }
    }
}