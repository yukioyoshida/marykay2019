using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class lblTotal : Form
    {
        public lblTotal()
        {
            InitializeComponent();
        }

        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        private void CLT_EmissaoCracha_Load(object sender, EventArgs e)
        {
            MI_CarregaTela();
            CarregaUsuario();
        }

        private void CarregaUsuario()
        {
            lblUsuario.Text = CSV_Global.getUsuario();
            lblTerminal.Text = CSV_Global.getNumeroTerminal();
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void MI_CarregaTela()
        {
            MI_Resumo();
            MI_Ranking();
        }
        
        private void MI_Resumo()
        {
            //PREENCHE A GRID RESUMO
            //-------------------------------------
            DataTable dtRegistro = new DataTable();
            DataTable dtFinal = new DataTable();
            DataRow drLinha;
            DataRow drLinhaInset;

            dtFinal.Columns.Add("DIA/DATA", typeof(string));
            dtFinal.Columns.Add("QUANTIDADE", typeof(string));

            CAT_CadastrarEmissaoCracha IAT_Cracha = new CAT_CadastrarEmissaoCracha();
            dtRegistro = IAT_Cracha.MS_ObterResumo();

            for (int i = 0; dtRegistro.Rows.Count - 1 >= i; i++)
            {
                drLinha = dtRegistro.Rows[i];
                drLinhaInset = dtFinal.NewRow();
                    drLinhaInset["DIA/DATA"] = Convert.ToString(i+1) + "� DIA - " + drLinha["Data"].ToString().Replace("00:00:00","");
                    drLinhaInset["QUANTIDADE"] = drLinha["Quantidade"].ToString();
                drLinhaInset.EndEdit();
                dtFinal.Rows.Add(drLinhaInset[0], drLinhaInset[1]);   
                gdwResumo.DataSource = dtFinal;
            }

            if (gdwResumo.Rows.Count > 0)
            {
                gdwResumo.Columns[0].Width = 110;
                gdwResumo.Columns[1].Width = 100;
            }

            //TOTAIS
            //-------------------------------------

            int itQuantidade = 0;
            int itDuplicados = 0;

            for (int it = 0; dtRegistro.Rows.Count - 1 >= it; it++)
            {
                drLinha = dtRegistro.Rows[it];
                itQuantidade = itQuantidade + Convert.ToInt32(gdwResumo.Rows[it].Cells[1].Value);
            }

            itDuplicados = Convert.ToInt32(IAT_Cracha.MS_QuantidadeDuplicados());
            itDuplicados = itQuantidade - itDuplicados;  

            lblResumoTotal.Text = itQuantidade.ToString();
            lblTotalDuplicados.Text = itDuplicados.ToString(); 
        }

        private void MI_Ranking()
        {
            DataTable dtRegistro = new DataTable();
            DataTable dtFinal = new DataTable();
            DataRow drLinha;
            DataRow drLinhaInset;

            dtFinal.Columns.Add("RANKING", typeof(string));
            dtFinal.Columns.Add("USUARIO", typeof(string));
            dtFinal.Columns.Add("QUANTIDADE", typeof(string));

            CAT_CadastrarEmissaoCracha IAT_Cracha = new CAT_CadastrarEmissaoCracha();
            dtRegistro = IAT_Cracha.MS_ObterRanking();

            for (int i = 0; dtRegistro.Rows.Count - 1 >= i; i++)
            {
                drLinha = dtRegistro.Rows[i];
                drLinhaInset = dtFinal.NewRow();
                drLinhaInset["RANKING"] = Convert.ToString(i + 1) + "� Lugar"; 
                drLinhaInset["USUARIO"] = drLinha["UsuarioImpressao"].ToString().ToUpper(); 
                drLinhaInset["QUANTIDADE"] = drLinha["Quantidade"].ToString();
                drLinhaInset.EndEdit();
                dtFinal.Rows.Add(drLinhaInset[0], drLinhaInset[1], drLinhaInset[2]);
                gdwRanking.DataSource = dtFinal;
            }

            if (gdwRanking.Rows.Count > 0)
            {
                gdwRanking.Columns[0].Width = 100;
                gdwRanking.Columns[1].Width = 270;
                gdwRanking.Columns[2].Width = 100;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void tmrHora_Tick(object sender, EventArgs e)
        {
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void lblTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            int itTecla = e.KeyChar;
            switch (itTecla.ToString())
            {
                case "27":
                    this.Close();
                    break;
            }
        }
    }
}