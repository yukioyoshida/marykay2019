﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace SPI
{
    public partial class Clt_AssinaturaDigital : Form
    {
        Brush brush = new SolidBrush(Color.Black);
        private Bitmap bitmap;
        private Graphics Graf;

        private int x, y, Tam = 5;

        public string AP_CodigoBarras { get; set; }
        public bool AP_AssinaturaProcessada { get; set; }

        private bool down;

        public Clt_AssinaturaDigital()
        {
            InitializeComponent();
        }

        private void Clt_AssinaturaDigital_Load(object sender, EventArgs e)
        {
            bitmap = new Bitmap(pcAssinatura.Width, pcAssinatura.Height);
            Graf = Graphics.FromImage(bitmap);
            Graf.SmoothingMode = SmoothingMode.HighQuality;
        }

        private void pcAssinatura_MouseDown(object sender, MouseEventArgs e)
        {
            down = true;
        }

        private void pcAssinatura_MouseUp(object sender, MouseEventArgs e)
        {
            down = false;
        }

        private void pcAssinatura_MouseMove(object sender, MouseEventArgs e)
        {
            if (down)
            {
                x = MousePosition.X - this.Location.X - 10;
                y = MousePosition.Y - this.Location.Y - 50;

                Graf.FillEllipse(brush, x, y, Tam, Tam);
                pcAssinatura.Image = bitmap;
            }
        }

        private void Clt_AssinaturaDigital_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!AP_AssinaturaProcessada)
                e.Cancel = true;
        }

        private void btnLimparAssinatura_Click(object sender, EventArgs e)
        {
            bitmap = new Bitmap(pcAssinatura.Width, pcAssinatura.Height);
            Graf = Graphics.FromImage(bitmap);
            Graf.SmoothingMode = SmoothingMode.HighQuality;
            pcAssinatura.Image = bitmap;
        }

        private void btnGravaAssinatura_Click(object sender, EventArgs e)
        {
            try
            {
                if (!System.IO.Directory.Exists(new CSV_VariaveisGlobal().getLocalArmazenamentoFoto() + "\\Assinatura\\"))
                {
                    System.IO.Directory.CreateDirectory(new CSV_VariaveisGlobal().getLocalArmazenamentoFoto() + "\\Assinatura\\");
                }

                pcAssinatura.Image.Save(new CSV_VariaveisGlobal().getLocalArmazenamentoFoto() + "\\Assinatura\\" + AP_CodigoBarras + ".jpg");
                AP_AssinaturaProcessada = true;
                this.Close();
            }
            catch
            {
                MessageBox.Show("A Assinatura não foi salva corretamente, tente novamente.", "Atenção",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }
    }
}
