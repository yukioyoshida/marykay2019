﻿namespace SPI
{
    partial class CLT_Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CLT_Login));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.edtNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.edtSenha = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btLogar = new System.Windows.Forms.Button();
            this.btConfiguracoes = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.ImageLocation = "";
            this.pictureBox1.Location = new System.Drawing.Point(-1, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(399, 82);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(-2, 76);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(400, 8);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 14);
            this.label1.TabIndex = 8;
            this.label1.Text = "Usuário:";
            // 
            // edtNome
            // 
            this.edtNome.Location = new System.Drawing.Point(104, 104);
            this.edtNome.Name = "edtNome";
            this.edtNome.Size = new System.Drawing.Size(234, 20);
            this.edtNome.TabIndex = 0;
            this.edtNome.TextChanged += new System.EventHandler(this.edtNome_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(45, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 14);
            this.label2.TabIndex = 10;
            this.label2.Text = "Senha:";
            // 
            // edtSenha
            // 
            this.edtSenha.Location = new System.Drawing.Point(104, 129);
            this.edtSenha.Name = "edtSenha";
            this.edtSenha.PasswordChar = '*';
            this.edtSenha.Size = new System.Drawing.Size(234, 20);
            this.edtSenha.TabIndex = 1;
            this.edtSenha.TextChanged += new System.EventHandler(this.edtSenha_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(12, 164);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(366, 8);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            // 
            // btLogar
            // 
            this.btLogar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btLogar.Image = ((System.Drawing.Image)(resources.GetObject("btLogar.Image")));
            this.btLogar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btLogar.Location = new System.Drawing.Point(104, 178);
            this.btLogar.Name = "btLogar";
            this.btLogar.Size = new System.Drawing.Size(93, 23);
            this.btLogar.TabIndex = 2;
            this.btLogar.Text = "      ENTRAR";
            this.btLogar.UseVisualStyleBackColor = true;
            this.btLogar.Click += new System.EventHandler(this.btLogar_Click);
            // 
            // btConfiguracoes
            // 
            this.btConfiguracoes.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btConfiguracoes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btConfiguracoes.Location = new System.Drawing.Point(203, 178);
            this.btConfiguracoes.Name = "btConfiguracoes";
            this.btConfiguracoes.Size = new System.Drawing.Size(135, 23);
            this.btConfiguracoes.TabIndex = 3;
            this.btConfiguracoes.Text = " CONFIGURAÇÕES";
            this.btConfiguracoes.UseVisualStyleBackColor = true;
            this.btConfiguracoes.Click += new System.EventHandler(this.btConfiguracoes_Click);
            // 
            // CLT_Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(397, 215);
            this.Controls.Add(this.btConfiguracoes);
            this.Controls.Add(this.btLogar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.edtSenha);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edtNome);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.Name = "CLT_Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " Autenticação de usuário";
            this.Load += new System.EventHandler(this.CLT_Login_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CLT_Login_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edtNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edtSenha;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btLogar;
        private System.Windows.Forms.Button btConfiguracoes;
    }
}

