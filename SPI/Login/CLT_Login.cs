﻿using System;
using System.Data;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_Login : Form
    {
        public CLT_Login()
        {
            InitializeComponent();
        }

        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();
        public bool AP_Login = false;

        private void CLT_Login_Load(object sender, EventArgs e)
        {
            CSV_Global.MI_LogOutUsuario();
        }

        private void btLogar_Click(object sender, EventArgs e)
        {
            if (MI_RalizaLogin(edtNome.Text, edtSenha.Text))
            {
                AP_Login = true;
                this.Close();
            }
        }

        private bool MI_RalizaLogin(string prstUsuario, string prstSenha)
        {
            bool boResult = true;

            CAT_CadastrarLogin CAT_CadastrarLogin = new CAT_CadastrarLogin();
            CTO_Login ITO_Login = CAT_CadastrarLogin.MS_Logar(prstUsuario, prstSenha);

            if (ITO_Login != null)
            {
                if ((ITO_Login.getNivelAcesso() == "1") || (ITO_Login.getNivelAcesso() == "2") || (ITO_Login.getNivelAcesso() == "3") || (ITO_Login.getNivelAcesso() == "4") || ITO_Login.getNivelAcesso() == "5")
                {
                    boResult = true;
                    CSV_Global.setUsuario(ITO_Login.getNomeUsuario());
                    CSV_Global.setPrioridade(ITO_Login.getNivelAcesso());
                    CSV_Global.setNumeroTerminal(MI_ObterNumeroTerminal());
                    CSV_Global.setLocalArmazenamentoFoto(MI_ObterLocalArmazenamentoFoto());
                    CSV_Global.setTipoJoia(MI_ObterTipoJoia());
                    CSV_Global.setCancelaFaixa(MI_ObterCancelamento("FAIXA"));
                    CSV_Global.setCancelaJoia(MI_ObterCancelamento("JOIA"));

                    if (ITO_Login.getNivelAcesso() == "3")
                    {
                        CSV_Global.setTipoJoia("JOIA");
                    }
                    if (ITO_Login.getNivelAcesso() == "4")
                    {
                        CSV_Global.setTipoJoia("FAIXA");
                    }
                    if (ITO_Login.getNivelAcesso() == "5")
                    {
                        CSV_Global.setTipoJoia("RELATORIO");
                    }

                }
                else
                {
                    MessageBox.Show("Usuário não possui permissão para acessar o sistema!", "Acesso não permitido");
                    boResult = false;
                }
            }
            else
            {
                MessageBox.Show("Usuário/Senha inválido!", "Acesso não permitido");
                boResult = false;
            }

            return boResult;
        }

        private void CLT_Login_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                e.Handled = true;
                SendKeys.Send("{tab}");
            }
        }

        private void btConfiguracoes_Click(object sender, EventArgs e)
        {
            MI_AbrirConfiguracoes();
        }

        private void MI_AbrirConfiguracoes()
        {
            this.Visible = false;
            CLT_ConexaoServidor ABRIR = new CLT_ConexaoServidor();
            ABRIR.ShowDialog();
            this.Visible = true;
        }


        private string MI_ObterNumeroTerminal()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            string stRetorno = string.Empty;

            try
            {
                dt.Columns.Add("IP", typeof(string));
                dt.Columns.Add("USUARIO", typeof(string));
                dt.Columns.Add("SENHA", typeof(string));
                dt.Columns.Add("BANCO", typeof(string));
                dt.Columns.Add("TERMINAL", typeof(string));
                dt.Columns.Add("ARMAZENAMENTO", typeof(string));

                dt.ReadXml("configuracoes.xml");
                dr = dt.Rows[0];

                stRetorno = Convert.ToString((dr["TERMINAL"]));
            }
            catch
            {
            }

            return stRetorno;
        }

        private string MI_ObterEvento()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            string stRetorno = string.Empty;

            try
            {
                dt.Columns.Add("IP", typeof(string));
                dt.Columns.Add("USUARIO", typeof(string));
                dt.Columns.Add("SENHA", typeof(string));
                dt.Columns.Add("BANCO", typeof(string));
                dt.Columns.Add("TERMINAL", typeof(string));
                dt.Columns.Add("ARMAZENAMENTO", typeof(string));
                dt.Columns.Add("EVENTO", typeof(string));

                dt.ReadXml("configuracoes.xml");
                dr = dt.Rows[0];

                stRetorno = Convert.ToString((dr["EVENTO"]));
            }
            catch
            {
            }

            return stRetorno;
        }

        private string MI_ObterLocalArmazenamentoFoto()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            string stRetorno = string.Empty;

            try
            {
                dt.Columns.Add("IP", typeof(string));
                dt.Columns.Add("USUARIO", typeof(string));
                dt.Columns.Add("SENHA", typeof(string));
                dt.Columns.Add("BANCO", typeof(string));
                dt.Columns.Add("TERMINAL", typeof(string));
                dt.Columns.Add("ARMAZENAMENTO", typeof(string));

                dt.ReadXml("configuracoes.xml");
                dr = dt.Rows[0];

                stRetorno = Convert.ToString((dr["ARMAZENAMENTO"]));
            }
            catch
            {
            }

            return stRetorno;
        }

        public string MI_ObterSeminario()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            string stRetorno = string.Empty;

            try
            {
                dt.Columns.Add("IP", typeof(string));
                dt.Columns.Add("USUARIO", typeof(string));
                dt.Columns.Add("SENHA", typeof(string));
                dt.Columns.Add("BANCO", typeof(string));
                dt.Columns.Add("TERMINAL", typeof(string));
                dt.Columns.Add("ARMAZENAMENTO", typeof(string));
                dt.Columns.Add("SEMINARIO", typeof(string));

                dt.ReadXml("configuracoes.xml");
                dr = dt.Rows[0];

                stRetorno = Convert.ToString((dr["SEMINARIO"]));
            }
            catch
            {
            }

            return stRetorno;
        }

        public string MI_ObterTipoJoia()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            string stRetorno = string.Empty;

            try
            {
                dt.Columns.Add("IP", typeof(string));
                dt.Columns.Add("USUARIO", typeof(string));
                dt.Columns.Add("SENHA", typeof(string));
                dt.Columns.Add("BANCO", typeof(string));
                dt.Columns.Add("TERMINAL", typeof(string));
                dt.Columns.Add("ARMAZENAMENTO", typeof(string));
                dt.Columns.Add("SEMINARIO", typeof(string));
                dt.Columns.Add("TIPOJOIA", typeof(string));

                dt.ReadXml("configuracoes.xml");
                dr = dt.Rows[0];

                stRetorno = Convert.ToString((dr["TIPOJOIA"]));
            }
            catch
            {
            }

            return stRetorno;
        }

        private string MI_ObterCancelamento(string stTipoCancelamento)
        {
            DataTable dt = new DataTable("CANCELARENTREGA");
            DataRow dr;

            string stRetorno = string.Empty;

            try
            {
                dt.Columns.Add("JOIA", typeof(string));
                dt.Columns.Add("FAIXA", typeof(string));

                dt.ReadXml("CancelarEntrega.xml");
                dr = dt.Rows[0];

                if (stTipoCancelamento.Equals("JOIA"))
                    stRetorno = Convert.ToString((dr["JOIA"]));
                else
                    stRetorno = Convert.ToString((dr["FAIXA"]));

            }
            catch
            {
            }

            return stRetorno;
        }

        private void edtSenha_TextChanged(object sender, EventArgs e)
        {
        }

        private void edtNome_TextChanged(object sender, EventArgs e)
        {
        }
    }
}
