using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_Usuarios : Form
    {
        public CLT_Usuarios()
        {
            InitializeComponent();
        }

        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        private void CLT_Usuarios_Load(object sender, EventArgs e)
        {
            MI_CarregaUsuarioNaGrid();
            MI_Controla(false);  
            MI_LimpaTela();
            CarregaUsuario();
        }

        private void CarregaUsuario()
        {
            lblUsuario.Text = CSV_Global.getUsuario();
            lblTerminal.Text = CSV_Global.getNumeroTerminal();
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void MI_Controla(bool boStatus)
        {
            edtNomeCompleto.ReadOnly = !boStatus;
            edtEmail.ReadOnly = !boStatus;
            edtCelular.ReadOnly = !boStatus;
            edtNomeAtendimento.ReadOnly = !boStatus;
            edtSenha.ReadOnly = !boStatus;

            chkAcesso.Enabled = boStatus; 
        }

        private void MI_LimpaTela()
        {
            edtCodigo.Text = string.Empty;

            edtNomeCompleto.Text = string.Empty;
            edtEmail.Text = string.Empty;
            edtCelular.Text = string.Empty;
            edtNomeAtendimento.Text = string.Empty;
            edtSenha.Text = string.Empty;

            chkAcesso.Checked = false;
        }

        private void MI_CarregaUsuarioNaGrid()
        {
            DataTable tbUsuarios = new DataTable();
            CAT_CadastrarLogin IAT_CadastrarLogin = new CAT_CadastrarLogin();
            tbUsuarios = IAT_CadastrarLogin.MS_ObterUsuarios();

            dgwUsuarios.DataSource = tbUsuarios; 
        }

        private void btNovo_Click(object sender, EventArgs e)
        {
            MI_Controla(true);
            MI_LimpaTela();

            edtNomeCompleto.Focus();
        }

        private void btSalvar_Click(object sender, EventArgs e)
        {
            MI_SalvarRegistro(); 
        }
        protected void MI_SalvarRegistro()
        {
            if (MI_Consistencias())
            {
                CAT_CadastrarLogin IAT_CadastrarLogin = new CAT_CadastrarLogin();
                CTO_Login ITO_Login = IAT_CadastrarLogin.MS_Obter(edtCodigo.Text);

                if (ITO_Login != null)
                {
                    ITO_Login.setNomeLogin(edtNomeAtendimento.Text);
                    ITO_Login.setSenhaLogin(edtSenha.Text);
                    ITO_Login.setNomeUsuario(edtNomeCompleto.Text.ToUpper());
                    ITO_Login.setEmailUsuario(edtEmail.Text.ToUpper());
                    ITO_Login.setCelularUsuario(edtCelular.Text);
                    ITO_Login.setNivelAcesso(MI_ObterNivel());

                    IAT_CadastrarLogin.MS_Alterar(ITO_Login);
                    MessageBox.Show("Altera��o realizada com sucesso!", "Confirma��o!");
                }
                else
                {
                    ITO_Login = new CTO_Login();
                    ITO_Login.setNomeLogin(edtNomeAtendimento.Text);
                    ITO_Login.setSenhaLogin(edtSenha.Text);
                    ITO_Login.setNomeUsuario(edtNomeCompleto.Text.ToUpper());
                    ITO_Login.setEmailUsuario(edtEmail.Text.ToUpper());
                    ITO_Login.setCelularUsuario(edtCelular.Text);
                    ITO_Login.setNivelAcesso(MI_ObterNivel());

                    IAT_CadastrarLogin.MS_Incluir(ITO_Login);
                    MessageBox.Show("Usu�rio cadastrado com sucesso!", "Confirma��o!");
                }
                MI_LimpaTela();
                MI_CarregaUsuarioNaGrid();
            }
        }

        private string MI_ObterNivel()
        {
            if (chkAcesso.Checked == true)
            {
                return "1";
            }
            else
            {
                return "2";
            }
        }

        private bool MI_Consistencias()
        {
            bool boResult = true;

            if (edtNomeCompleto.Text == "")
            {
                MessageBox.Show("Informe o nome do usu�rio", "ATEN��O!"); 
                return boResult = false;            
            }
            if (edtNomeAtendimento.Text == "")
            {
                MessageBox.Show("Informe o nome de atendimento", "ATEN��O!");
                return boResult = false;
            }
            if (edtSenha.Text == "")
            {
                MessageBox.Show("Informe a senha de seguran�a", "ATEN��O!");
                return boResult = false;
            }
            return boResult;
        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            MI_LimpaTela();
            MI_Controla(false);  
        }

        private void btAlterar_Click(object sender, EventArgs e)
        {
            if (edtCodigo.Text != null)
            {
                MI_Controla(true);
            }
        }

        private void btExcluir_Click(object sender, EventArgs e)
        {
            if (edtCodigo.Text != "")
            {
                MI_ExluirRegistro(edtCodigo.Text);
            }
        }
        private void MI_ExluirRegistro(string prObjRef)
        {
            CAT_CadastrarLogin IAT_CadastrarLogin = new CAT_CadastrarLogin();
            CTO_Login ITO_Login = IAT_CadastrarLogin.MS_Obter(prObjRef);

            if (ITO_Login != null)
            {
                if (MessageBox.Show("Realmente deseja excluir este usu�rio?","ATEN��O!",MessageBoxButtons.YesNo)==DialogResult.Yes)
                {
                    IAT_CadastrarLogin.MS_Excluir(ITO_Login);
                    MessageBox.Show("Exclus�o realizada com sucesso!", "Confirma��o!");
                    MI_LimpaTela();
                    MI_CarregaUsuarioNaGrid();
                }
            }
        }

        private void dgwUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            MI_CarregarUsuario(dgwUsuarios.CurrentRow.Cells[0].Value.ToString()); 
        }
        private void MI_CarregarUsuario(string prObjRef)
        {
            CAT_CadastrarLogin IAT_CadastrarLogin = new CAT_CadastrarLogin();
            CTO_Login ITO_Login = IAT_CadastrarLogin.MS_Obter(prObjRef);

            if (ITO_Login != null)
            {
                edtCodigo.Text = ITO_Login.getObjRef().ToString();
                edtNomeCompleto.Text = ITO_Login.getNomeUsuario().ToString();
                edtEmail.Text = ITO_Login.getEmailUsuario().ToString();
                edtCelular.Text = ITO_Login.getCelularUsuario().ToString();
                edtNomeAtendimento.Text = ITO_Login.getNomeLogin().ToString();
                edtSenha.Text = ITO_Login.getSenhaLogin().ToString();
                if (ITO_Login.getNivelAcesso().ToString() == "1")
                {
                    chkAcesso.Checked = true;
                }
                else
                {
                    chkAcesso.Checked = false;
                }
                MI_Controla(false); 
            }
        }

        private void trmHora_Tick(object sender, EventArgs e)
        {
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void CLT_Usuarios_KeyPress(object sender, KeyPressEventArgs e)
        {
            int itTecla = e.KeyChar;
            switch (itTecla.ToString())
            {
                case "27":
                    this.Close();
                    break;
            }
        }
    }
}