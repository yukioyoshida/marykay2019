﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class Clt_InsertParticipante : Form
    {
        public bool AP_Result { get; set; }
        public string AP_Consultora { get; set; }
        public string AP_TipoInscricao { get; set; }
        public string AP_TipoInscricaoCategoria { get; set; }

        public string AP_Valor { get; set; }
        public string AP_FormaPagto { get; set; }

        public Clt_InsertParticipante()
        {
            InitializeComponent();
        }

        private void Clt_InsertParticipante_Load(object sender, EventArgs e)
        {
            AP_Result = false;
            MI_LimparTela();
            MI_CarregarTipoInscricao();
        }

        private void MI_CarregarTipoInscricao()
        {
            ddlTipoInscricao.Items.Clear();

            ddlTipoInscricao.Items.Add("");
            foreach (DataRow Row in new DAO_Visitante().MS_ObterTipoInscricao().Rows)
            {
                ddlTipoInscricao.Items.Add(Row["TipoDeInscricao"].ToString());
            }

            ddlTipoInscricao.Text = "";
        }

        private void MI_LimparTela()
        {
            AP_Result = false;
            AP_Consultora = string.Empty;
            AP_TipoInscricao = string.Empty;

            lblNomeCompleto.Text = string.Empty;
            lblCPF.Text = string.Empty;
            lblNSD.Text = string.Empty;

            MI_LimparGrid();
        }

        private void MI_LimparGrid()
        {
            for (int i = gdwInscricao.Rows.Count-1; i >= 0; i--)
                gdwInscricao.Rows.Remove(gdwInscricao.Rows[i]);
        }

        private void btoValidar_Click(object sender, EventArgs e)
        {
            if (edtConsultora.Text.Trim() != string.Empty)
            {
                if (edtConsultora.Text.Length == 6)
                {
                    MI_BuscarConsultora(edtConsultora.Text.Trim());
                }
                else
                {
                    MessageBox.Show("O código da consultora deve conter pelo menos 6 caracteres", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void MI_BuscarConsultora(string prNrConsultora)
        {
            DataTable dtConsultora = new DAO_Visitante().MS_ObterRegistroBDGeral(prNrConsultora);
            if (dtConsultora.Rows.Count > 0)
            {
                AP_Consultora = prNrConsultora;
                lblNomeCompleto.Text = dtConsultora.Rows[0]["nmConsultora"].ToString();
                lblCPF.Text = dtConsultora.Rows[0]["dsCPF"].ToString();
                lblNSD.Text = dtConsultora.Rows[0]["nmNSD"].ToString();

                MI_CarregarInscricoes(prNrConsultora);
            }
            else
            {
                MessageBox.Show("O código informado não foi localizado!", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                MI_LimparTela();
            }
        }

        private void MI_CarregarInscricoes(string prNrConsultora)
        {
            MI_LimparGrid();

            DataTable dtRegistros = new DataTable();
            CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();

            dtRegistros = IAT_CadastrarVisitante.MS_ObterRegistro(prNrConsultora, "", "Consultora");
            gdwInscricao.DataSource = dtRegistros;

            foreach (DataGridViewRow Row in gdwInscricao.Rows)
            {
                Row.Height = 46;

                if ((Row.Cells[3].Value.ToString().ToUpper().Contains("CONVIDADOS")) || ("CONVIDADOS").Contains(Row.Cells[3].Value.ToString().ToUpper()))
                {
                    Row.Cells[4].Value = "CONVIDADO";
                }
                else
                {
                    Row.Cells[4].Value = "CONSULTORA";
                }
            }
        }

        private void btAcessar_Click(object sender, EventArgs e)
        {
            if (gdwInscricao.Rows.Count >= 2)
            {
                MessageBox.Show("É permitido somente 1 acompanhante/convidado", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                AP_Consultora = string.Empty;
                AP_TipoInscricao = string.Empty;
            }
            else
            {
                if (ddlTipoInscricao.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("Selecione o tipo de inscrição", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (edtValor.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("Informe o valor", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (edtFormaPagamento.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("Informe a forma de pagamento", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if ((MI_DefinirTipoInscricao() == "CONVIDADOS") && (gdwInscricao.Rows.Count == 0))
                {
                    MessageBox.Show("Você não pode inserir um acompanhante/convidado antes da consultora", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    bool boResult = true;
                    if (MI_DefinirTipoInscricao() == "CONVIDADOS")
                    {
                        foreach(DataGridViewRow Row in gdwInscricao.Rows)
                        {
                            if (Row.Cells[2].Value.ToString().Trim().ToUpper().Contains("CONVIDADOS"))
                            {
                                boResult = false; // NÃO PODE INSERIR JÁ TEM UM CONVIDDO
                                break;
                            }
                            else if ( ("CONVIDADOS").Contains(Row.Cells[2].Value.ToString().Trim().ToUpper()))
                            {
                                boResult = false; // NÃO PODE INSERIR JÁ TEM UM CONVIDDO
                                break;
                            }
                        }
                    }
                    else
                    {
                        foreach (DataGridViewRow Row in gdwInscricao.Rows)
                        {
                            if (Row.Cells[2].Value.ToString().Trim().ToUpper().Contains("CONVIDADOS"))
                            {
                                boResult = true; // PODE INSERIR É UM CONVIDADO
                            }
                            else if (("CONVIDADOS").Contains(Row.Cells[2].Value.ToString().Trim().ToUpper()))
                            {
                                boResult = false; // PODE INSERIR É UM CONVIDADO
                            }
                            else
                            {
                                boResult = false; // NÃO PODE INSERIR JÁ TEM UMA CONSULTORA
                                break;
                            }
                        }
                    }

                    if (boResult == false)
                    {
                        MessageBox.Show("Inclusão não autorizada.\n"+ MI_DefinirTipoInscricao() + " já cadastrado! ", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        AP_Result = true;
                        AP_TipoInscricao = ddlTipoInscricao.Text;

                        AP_Valor = edtValor.Text;
                        AP_FormaPagto = edtFormaPagamento.Text;

                        this.Close();
                    }
                }
            }
        }

        private string MI_DefinirTipoInscricao()
        {
            string stResult = string.Empty;

            if (("CONVIDADOS".Contains(ddlTipoInscricao.Text.ToUpper())) || (ddlTipoInscricao.Text.ToUpper()).Contains("CONVIDADOS"))
            {
                stResult = "CONVIDADOS";
            }
            else
            {
                stResult = "CONSULTORAS";
            }

            AP_TipoInscricaoCategoria = stResult;
            return stResult;
        }

        private void btoCancelar_Click(object sender, EventArgs e)
        {
            MI_LimparTela();
            this.Close();
        }
    }
}
