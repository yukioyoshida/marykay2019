﻿namespace SPI
{
    partial class CLT_ImprimiCertificadoPorProduto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdBiomassa = new System.Windows.Forms.RadioButton();
            this.rdAvessui = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.btImprimi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rdBiomassa
            // 
            this.rdBiomassa.AutoSize = true;
            this.rdBiomassa.Checked = true;
            this.rdBiomassa.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold);
            this.rdBiomassa.Location = new System.Drawing.Point(75, 82);
            this.rdBiomassa.Name = "rdBiomassa";
            this.rdBiomassa.Size = new System.Drawing.Size(92, 17);
            this.rdBiomassa.TabIndex = 92;
            this.rdBiomassa.TabStop = true;
            this.rdBiomassa.Text = "BIOMASSA";
            this.rdBiomassa.UseVisualStyleBackColor = true;
            // 
            // rdAvessui
            // 
            this.rdAvessui.AutoSize = true;
            this.rdAvessui.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold);
            this.rdAvessui.Location = new System.Drawing.Point(242, 82);
            this.rdAvessui.Name = "rdAvessui";
            this.rdAvessui.Size = new System.Drawing.Size(73, 17);
            this.rdAvessui.TabIndex = 93;
            this.rdAvessui.Text = "AVESUI";
            this.rdAvessui.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(72, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(227, 13);
            this.label6.TabIndex = 94;
            this.label6.Text = "Deseja Imprimir qual certificado?";
            // 
            // btImprimi
            // 
            this.btImprimi.Location = new System.Drawing.Point(123, 185);
            this.btImprimi.Name = "btImprimi";
            this.btImprimi.Size = new System.Drawing.Size(140, 23);
            this.btImprimi.TabIndex = 95;
            this.btImprimi.Text = "IMPRIMIR";
            this.btImprimi.UseVisualStyleBackColor = true;
            this.btImprimi.Click += new System.EventHandler(this.btImprimi_Click);
            // 
            // CLT_ImprimiCertificadoPorProduto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 69);
            this.Controls.Add(this.btImprimi);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.rdAvessui);
            this.Controls.Add(this.rdBiomassa);
            this.Name = "CLT_ImprimiCertificadoPorProduto";
            this.Text = "CLT_ImprimiCertificadoPorProduto";
            this.Load += new System.EventHandler(this.CLT_ImprimiCertificadoPorProduto_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rdBiomassa;
        private System.Windows.Forms.RadioButton rdAvessui;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btImprimi;
    }
}