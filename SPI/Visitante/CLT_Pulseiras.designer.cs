﻿namespace SPI
{
    partial class CLT_Pulseiras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imgCurso1 = new System.Windows.Forms.PictureBox();
            this.imgCurso2 = new System.Windows.Forms.PictureBox();
            this.imgCurso3 = new System.Windows.Forms.PictureBox();
            this.imgCurso6 = new System.Windows.Forms.PictureBox();
            this.imgCurso5 = new System.Windows.Forms.PictureBox();
            this.imgCurso4 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btFechar = new System.Windows.Forms.Button();
            this.imgCurso7 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCategoria = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tmrCategoria = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.imgCurso1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCurso2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCurso3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCurso6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCurso5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCurso4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCurso7)).BeginInit();
            this.SuspendLayout();
            // 
            // imgCurso1
            // 
            this.imgCurso1.BackColor = System.Drawing.Color.Yellow;
            this.imgCurso1.Location = new System.Drawing.Point(447, 184);
            this.imgCurso1.Name = "imgCurso1";
            this.imgCurso1.Size = new System.Drawing.Size(38, 240);
            this.imgCurso1.TabIndex = 0;
            this.imgCurso1.TabStop = false;
            this.imgCurso1.Visible = false;
            // 
            // imgCurso2
            // 
            this.imgCurso2.BackColor = System.Drawing.Color.Yellow;
            this.imgCurso2.Location = new System.Drawing.Point(16, 184);
            this.imgCurso2.Name = "imgCurso2";
            this.imgCurso2.Size = new System.Drawing.Size(38, 240);
            this.imgCurso2.TabIndex = 1;
            this.imgCurso2.TabStop = false;
            this.imgCurso2.Visible = false;
            // 
            // imgCurso3
            // 
            this.imgCurso3.BackColor = System.Drawing.Color.Green;
            this.imgCurso3.Location = new System.Drawing.Point(90, 184);
            this.imgCurso3.Name = "imgCurso3";
            this.imgCurso3.Size = new System.Drawing.Size(38, 240);
            this.imgCurso3.TabIndex = 2;
            this.imgCurso3.TabStop = false;
            this.imgCurso3.Visible = false;
            // 
            // imgCurso6
            // 
            this.imgCurso6.BackColor = System.Drawing.Color.Orange;
            this.imgCurso6.Location = new System.Drawing.Point(302, 184);
            this.imgCurso6.Name = "imgCurso6";
            this.imgCurso6.Size = new System.Drawing.Size(38, 240);
            this.imgCurso6.TabIndex = 5;
            this.imgCurso6.TabStop = false;
            this.imgCurso6.Visible = false;
            // 
            // imgCurso5
            // 
            this.imgCurso5.BackColor = System.Drawing.Color.DeepPink;
            this.imgCurso5.Location = new System.Drawing.Point(230, 184);
            this.imgCurso5.Name = "imgCurso5";
            this.imgCurso5.Size = new System.Drawing.Size(38, 240);
            this.imgCurso5.TabIndex = 4;
            this.imgCurso5.TabStop = false;
            this.imgCurso5.Visible = false;
            // 
            // imgCurso4
            // 
            this.imgCurso4.BackColor = System.Drawing.Color.Yellow;
            this.imgCurso4.Location = new System.Drawing.Point(156, 184);
            this.imgCurso4.Name = "imgCurso4";
            this.imgCurso4.Size = new System.Drawing.Size(38, 240);
            this.imgCurso4.TabIndex = 3;
            this.imgCurso4.TabStop = false;
            this.imgCurso4.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(16, 144);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(501, 8);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 157);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(392, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "ENTREGUE AO VISITANTE AS PULSEIRAS CONFORME ABAIXO";
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(16, 439);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(499, 8);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            // 
            // btFechar
            // 
            this.btFechar.Location = new System.Drawing.Point(227, 453);
            this.btFechar.Name = "btFechar";
            this.btFechar.Size = new System.Drawing.Size(75, 23);
            this.btFechar.TabIndex = 9;
            this.btFechar.Text = "Fechar";
            this.btFechar.UseVisualStyleBackColor = true;
            this.btFechar.Click += new System.EventHandler(this.btFechar_Click);
            // 
            // imgCurso7
            // 
            this.imgCurso7.BackColor = System.Drawing.Color.White;
            this.imgCurso7.Location = new System.Drawing.Point(375, 184);
            this.imgCurso7.Name = "imgCurso7";
            this.imgCurso7.Size = new System.Drawing.Size(38, 240);
            this.imgCurso7.TabIndex = 10;
            this.imgCurso7.TabStop = false;
            this.imgCurso7.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(248, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "COLE A ETIQUETA NA CREDENCIAL DE";
            // 
            // lblCategoria
            // 
            this.lblCategoria.AutoSize = true;
            this.lblCategoria.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategoria.Location = new System.Drawing.Point(115, 64);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(284, 45);
            this.lblCategoria.TabIndex = 12;
            this.lblCategoria.Text = "VERIFICANDO";
            this.lblCategoria.Click += new System.EventHandler(this.lblCategoria_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(15, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(501, 8);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Location = new System.Drawing.Point(14, 23);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(501, 8);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            // 
            // tmrCategoria
            // 
            this.tmrCategoria.Enabled = true;
            this.tmrCategoria.Interval = 1200;
            this.tmrCategoria.Tick += new System.EventHandler(this.tmrCategoria_Tick);
            // 
            // CLT_Pulseiras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 485);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.lblCategoria);
            this.Controls.Add(this.imgCurso7);
            this.Controls.Add(this.btFechar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.imgCurso6);
            this.Controls.Add(this.imgCurso5);
            this.Controls.Add(this.imgCurso4);
            this.Controls.Add(this.imgCurso3);
            this.Controls.Add(this.imgCurso2);
            this.Controls.Add(this.imgCurso1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CLT_Pulseiras";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "INTERAÇÃO";
            this.Load += new System.EventHandler(this.CLT_Pulseiras_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgCurso1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCurso2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCurso3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCurso6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCurso5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCurso4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCurso7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox imgCurso1;
        private System.Windows.Forms.PictureBox imgCurso2;
        private System.Windows.Forms.PictureBox imgCurso3;
        private System.Windows.Forms.PictureBox imgCurso6;
        private System.Windows.Forms.PictureBox imgCurso5;
        private System.Windows.Forms.PictureBox imgCurso4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btFechar;
        private System.Windows.Forms.PictureBox imgCurso7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblCategoria;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Timer tmrCategoria;
    }
}