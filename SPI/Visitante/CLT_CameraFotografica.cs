﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_CameraFotografica : Form
    {
        public CLT_CameraFotografica()
        {
            InitializeComponent();
        }

        static string AP_CodigoVisitante = string.Empty;

        public void setCodigoVisitante(string stCodigoVisitante)
        {
            AP_CodigoVisitante = stCodigoVisitante;
        }

        public string getCodigoVisitante()
        {
            return AP_CodigoVisitante;
        }

        CSV_VariaveisGlobal CSV_Glogal = new CSV_VariaveisGlobal();

        private void CLT_CameraFotografica_Load(object sender, EventArgs e)
        {
            MI_ContraladorVideo(true);
        }

        private void MI_ContraladorVideo(bool boResult)
        {
            Device[] devices = DeviceManager.GetAllDevices();

            foreach (Device d in devices)
            {
                cmbDevices.Items.Add(d);
            }

            if (devices.Length > 0)
            {
                cmbDevices.SelectedIndex = 0;
            }

            if (boResult == true)
            {
                Device da = DeviceManager.GetDevice(cmbDevices.SelectedIndex);
                da.ShowWindow(this.picFoto);
            }
            else
            {
                Device da = DeviceManager.GetDevice(cmbDevices.SelectedIndex);
                da.Stop();
            }
        }

        private void MI_CapturarImagem(string prCodigo)
        {
            Device[] devices = DeviceManager.GetAllDevices();
            Device da = DeviceManager.GetDevice(cmbDevices.SelectedIndex);

            Image bmap = da.CaptureImage(picFoto);
            MI_ContraladorVideo(false);

            bmap.Save(CSV_Glogal.getLocalArmazenamentoFoto() + prCodigo + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);  
        }

        private void btCameraDigital_Click(object sender, EventArgs e)
        {
            MI_CapturarImagem(getCodigoVisitante());
            this.Close();
        }
    }
}
