using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_ConfirmaOperacaoJoia : Form
    {
        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        public string AP_stCodigoBarras { get; set; }
        public string AP_stNome1 { get; set; }
        public string AP_stNome2 { get; set; }
        public string AP_stCdPrimiacao { get; set; }


        public CLT_ConfirmaOperacaoJoia()
        {
            InitializeComponent();
        }

        private void CLT_LocalizarRegistro_Load(object sender, EventArgs e)
        {
            CarregaUsuario();
            CarregarVariaveis();
        }

        private void CarregaUsuario()
        {
            lblUsuario.Text = CSV_Global.getUsuario();
            lblTerminal.Text = CSV_Global.getNumeroTerminal();
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void CarregarVariaveis()
        {
            lblCodigo.Text = AP_stCodigoBarras;
            lblNome1.Text = AP_stNome1;
            lblNome2.Text = AP_stNome2;

            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();

            DataTable dtObterPremiacao = IAT_Visitante.MS_ObterPremioDaConsultora(AP_stCdPrimiacao);
            DataTable dtObterPremio = IAT_Visitante.MS_ObterPremio(dtObterPremiacao.Rows[0]["cdPremio"].ToString());
        }

        private void CLT_LocalizarRegistro_KeyPress(object sender, KeyPressEventArgs e)
        {
            int itTecla = e.KeyChar;
            switch (itTecla.ToString())
            {
                case "27":
                    this.Close();
                    break;
            }
        }

        private void tmrTempo_Tick(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}