﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_SenhaJoia : Form
    {

        public bool AP_Result { get; set; }
        public string AP_Motivo { get; set; }

        public CLT_SenhaJoia()
        {
            InitializeComponent();
        }

        private void CLT_Senha_Load(object sender, EventArgs e)
        {
            AP_Result = false;
        }

        private void MI_VerificarSenha(string stSenha)
        {   
            CSV_VariaveisGlobal Global = new CSV_VariaveisGlobal();
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtSenha = IAT_Visitante.MS_ObterSenha(stSenha);

            if (dtSenha.Rows[0][0].ToString() == edtSenha.Text)
            {
                AP_Result = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Senha Inválida", "ATENÇÃO!");
                AP_Result = false;
                this.Close();
            }
        }

        private void btAcessar_Click(object sender, EventArgs e)
        {
            MI_VerificarSenha(edtSenha.Text.Trim());
        }

        private void btoCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
