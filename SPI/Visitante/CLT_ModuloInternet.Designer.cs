namespace SPI
{
    partial class CLT_ModuloInternet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CLT_ModuloInternet));
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblDataHora = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblNivelAcesso = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.imgPainel = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.edtChave = new System.Windows.Forms.TextBox();
            this.lblMensagem = new System.Windows.Forms.Label();
            this.chkImpressaoAutomatico = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupDados = new System.Windows.Forms.GroupBox();
            this.edtCargo = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.edtEmpresaCracha = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.edtRazaoSocial = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.edtNomeCompleto = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.edtCPF = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.edtCodigo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btImprimirEtiqueta = new System.Windows.Forms.Button();
            this.btEditarDados = new System.Windows.Forms.Button();
            this.groupControle = new System.Windows.Forms.GroupBox();
            this.btCharmarProximo = new System.Windows.Forms.Button();
            this.tmrRelogio = new System.Windows.Forms.Timer(this.components);
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.linha = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lblDirecao = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.lblNumeroTerminal = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lblSair = new System.Windows.Forms.Label();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPainel)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupDados.SuspendLayout();
            this.groupControle.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.linha.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.lblTerminal);
            this.groupBox5.Location = new System.Drawing.Point(678, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(98, 46);
            this.groupBox5.TabIndex = 42;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "TERMINAL";
            // 
            // lblTerminal
            // 
            this.lblTerminal.AutoSize = true;
            this.lblTerminal.BackColor = System.Drawing.Color.Transparent;
            this.lblTerminal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTerminal.ForeColor = System.Drawing.Color.Yellow;
            this.lblTerminal.Location = new System.Drawing.Point(22, 18);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(0, 18);
            this.lblTerminal.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.lblDataHora);
            this.groupBox2.Location = new System.Drawing.Point(786, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(152, 46);
            this.groupBox2.TabIndex = 41;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "DATA - HORA";
            // 
            // lblDataHora
            // 
            this.lblDataHora.AutoSize = true;
            this.lblDataHora.BackColor = System.Drawing.Color.Transparent;
            this.lblDataHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataHora.ForeColor = System.Drawing.Color.Yellow;
            this.lblDataHora.Location = new System.Drawing.Point(6, 18);
            this.lblDataHora.Name = "lblDataHora";
            this.lblDataHora.Size = new System.Drawing.Size(0, 18);
            this.lblDataHora.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.lblNivelAcesso);
            this.groupBox3.Controls.Add(this.lblUsuario);
            this.groupBox3.Location = new System.Drawing.Point(75, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(594, 46);
            this.groupBox3.TabIndex = 40;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "OPERADOR";
            // 
            // lblNivelAcesso
            // 
            this.lblNivelAcesso.AutoSize = true;
            this.lblNivelAcesso.Location = new System.Drawing.Point(706, 20);
            this.lblNivelAcesso.Name = "lblNivelAcesso";
            this.lblNivelAcesso.Size = new System.Drawing.Size(0, 13);
            this.lblNivelAcesso.TabIndex = 1;
            this.lblNivelAcesso.Visible = false;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.BackColor = System.Drawing.Color.Transparent;
            this.lblUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.ForeColor = System.Drawing.Color.Yellow;
            this.lblUsuario.Location = new System.Drawing.Point(17, 18);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(0, 18);
            this.lblUsuario.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Lavender;
            this.label10.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label10.Location = new System.Drawing.Point(364, 95);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(305, 16);
            this.label10.TabIndex = 44;
            this.label10.Text = "MODO DE IMPRESS�O R�PIDA - INTERNET";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Lavender;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Location = new System.Drawing.Point(75, 87);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(863, 34);
            this.pictureBox3.TabIndex = 45;
            this.pictureBox3.TabStop = false;
            // 
            // imgPainel
            // 
            this.imgPainel.BackColor = System.Drawing.Color.LightSteelBlue;
            this.imgPainel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgPainel.Location = new System.Drawing.Point(75, 120);
            this.imgPainel.Name = "imgPainel";
            this.imgPainel.Size = new System.Drawing.Size(863, 315);
            this.imgPainel.TabIndex = 43;
            this.imgPainel.TabStop = false;
            this.imgPainel.Click += new System.EventHandler(this.imgPainel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox1.Controls.Add(this.edtChave);
            this.groupBox1.Location = new System.Drawing.Point(290, 139);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(423, 49);
            this.groupBox1.TabIndex = 51;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PALAVRA CHAVE:";
            // 
            // edtChave
            // 
            this.edtChave.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtChave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtChave.ForeColor = System.Drawing.Color.RoyalBlue;
            this.edtChave.Location = new System.Drawing.Point(6, 16);
            this.edtChave.Name = "edtChave";
            this.edtChave.Size = new System.Drawing.Size(409, 26);
            this.edtChave.TabIndex = 0;
            this.edtChave.TextChanged += new System.EventHandler(this.edtChave_TextChanged);
            this.edtChave.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.edtChave_KeyPress);
            // 
            // lblMensagem
            // 
            this.lblMensagem.AutoSize = true;
            this.lblMensagem.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblMensagem.ForeColor = System.Drawing.Color.Red;
            this.lblMensagem.Location = new System.Drawing.Point(287, 195);
            this.lblMensagem.Name = "lblMensagem";
            this.lblMensagem.Size = new System.Drawing.Size(59, 13);
            this.lblMensagem.TabIndex = 52;
            this.lblMensagem.Text = "Mensagem";
            this.lblMensagem.Click += new System.EventHandler(this.lblMensagem_Click);
            // 
            // chkImpressaoAutomatico
            // 
            this.chkImpressaoAutomatico.AutoSize = true;
            this.chkImpressaoAutomatico.BackColor = System.Drawing.Color.LightSteelBlue;
            this.chkImpressaoAutomatico.Location = new System.Drawing.Point(576, 194);
            this.chkImpressaoAutomatico.Name = "chkImpressaoAutomatico";
            this.chkImpressaoAutomatico.Size = new System.Drawing.Size(129, 17);
            this.chkImpressaoAutomatico.TabIndex = 53;
            this.chkImpressaoAutomatico.Text = "Impress�o autom�tica";
            this.chkImpressaoAutomatico.UseVisualStyleBackColor = false;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox4.Location = new System.Drawing.Point(91, 221);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(832, 8);
            this.groupBox4.TabIndex = 54;
            this.groupBox4.TabStop = false;
            // 
            // groupDados
            // 
            this.groupDados.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupDados.Controls.Add(this.edtCargo);
            this.groupDados.Controls.Add(this.label25);
            this.groupDados.Controls.Add(this.edtEmpresaCracha);
            this.groupDados.Controls.Add(this.label9);
            this.groupDados.Controls.Add(this.edtRazaoSocial);
            this.groupDados.Controls.Add(this.label8);
            this.groupDados.Controls.Add(this.edtNomeCompleto);
            this.groupDados.Controls.Add(this.label6);
            this.groupDados.Controls.Add(this.edtCPF);
            this.groupDados.Controls.Add(this.label5);
            this.groupDados.Controls.Add(this.edtCodigo);
            this.groupDados.Controls.Add(this.label3);
            this.groupDados.Location = new System.Drawing.Point(166, 240);
            this.groupDados.Name = "groupDados";
            this.groupDados.Size = new System.Drawing.Size(547, 184);
            this.groupDados.TabIndex = 55;
            this.groupDados.TabStop = false;
            this.groupDados.Text = "DADOS PESSOAIS";
            // 
            // edtCargo
            // 
            this.edtCargo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtCargo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtCargo.Enabled = false;
            this.edtCargo.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtCargo.Location = new System.Drawing.Point(131, 141);
            this.edtCargo.MaxLength = 30;
            this.edtCargo.Name = "edtCargo";
            this.edtCargo.Size = new System.Drawing.Size(401, 23);
            this.edtCargo.TabIndex = 30;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(17, 146);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(44, 13);
            this.label25.TabIndex = 32;
            this.label25.Text = "Cargo:";
            // 
            // edtEmpresaCracha
            // 
            this.edtEmpresaCracha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtEmpresaCracha.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtEmpresaCracha.Enabled = false;
            this.edtEmpresaCracha.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtEmpresaCracha.Location = new System.Drawing.Point(131, 110);
            this.edtEmpresaCracha.MaxLength = 30;
            this.edtEmpresaCracha.Name = "edtEmpresaCracha";
            this.edtEmpresaCracha.Size = new System.Drawing.Size(401, 23);
            this.edtEmpresaCracha.TabIndex = 28;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(17, 115);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "Empresa Cracha:";
            // 
            // edtRazaoSocial
            // 
            this.edtRazaoSocial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtRazaoSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtRazaoSocial.Enabled = false;
            this.edtRazaoSocial.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtRazaoSocial.Location = new System.Drawing.Point(131, 80);
            this.edtRazaoSocial.MaxLength = 100;
            this.edtRazaoSocial.Name = "edtRazaoSocial";
            this.edtRazaoSocial.Size = new System.Drawing.Size(401, 23);
            this.edtRazaoSocial.TabIndex = 27;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(17, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 29;
            this.label8.Text = "Empresa:";
            // 
            // edtNomeCompleto
            // 
            this.edtNomeCompleto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtNomeCompleto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtNomeCompleto.Enabled = false;
            this.edtNomeCompleto.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtNomeCompleto.Location = new System.Drawing.Point(131, 50);
            this.edtNomeCompleto.MaxLength = 60;
            this.edtNomeCompleto.Name = "edtNomeCompleto";
            this.edtNomeCompleto.Size = new System.Drawing.Size(400, 23);
            this.edtNomeCompleto.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(17, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Nome Completo:";
            // 
            // edtCPF
            // 
            this.edtCPF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtCPF.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtCPF.Enabled = false;
            this.edtCPF.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtCPF.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.edtCPF.Location = new System.Drawing.Point(283, 19);
            this.edtCPF.MaxLength = 11;
            this.edtCPF.Name = "edtCPF";
            this.edtCPF.Size = new System.Drawing.Size(248, 23);
            this.edtCPF.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(247, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "CPF:";
            // 
            // edtCodigo
            // 
            this.edtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtCodigo.Enabled = false;
            this.edtCodigo.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtCodigo.Location = new System.Drawing.Point(131, 19);
            this.edtCodigo.Name = "edtCodigo";
            this.edtCodigo.Size = new System.Drawing.Size(110, 23);
            this.edtCodigo.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(17, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "C�digo:";
            // 
            // btImprimirEtiqueta
            // 
            this.btImprimirEtiqueta.BackColor = System.Drawing.Color.White;
            this.btImprimirEtiqueta.Image = ((System.Drawing.Image)(resources.GetObject("btImprimirEtiqueta.Image")));
            this.btImprimirEtiqueta.Location = new System.Drawing.Point(14, 18);
            this.btImprimirEtiqueta.Name = "btImprimirEtiqueta";
            this.btImprimirEtiqueta.Size = new System.Drawing.Size(86, 72);
            this.btImprimirEtiqueta.TabIndex = 56;
            this.btImprimirEtiqueta.Text = "&Imprimir";
            this.btImprimirEtiqueta.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btImprimirEtiqueta.UseVisualStyleBackColor = false;
            this.btImprimirEtiqueta.Click += new System.EventHandler(this.btImprimirEtiqueta_Click);
            // 
            // btEditarDados
            // 
            this.btEditarDados.BackColor = System.Drawing.Color.White;
            this.btEditarDados.Image = ((System.Drawing.Image)(resources.GetObject("btEditarDados.Image")));
            this.btEditarDados.Location = new System.Drawing.Point(14, 96);
            this.btEditarDados.Name = "btEditarDados";
            this.btEditarDados.Size = new System.Drawing.Size(86, 72);
            this.btEditarDados.TabIndex = 57;
            this.btEditarDados.Text = "&Modificar";
            this.btEditarDados.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btEditarDados.UseVisualStyleBackColor = false;
            this.btEditarDados.Click += new System.EventHandler(this.btEditarDados_Click);
            // 
            // groupControle
            // 
            this.groupControle.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupControle.Controls.Add(this.btEditarDados);
            this.groupControle.Controls.Add(this.btImprimirEtiqueta);
            this.groupControle.Location = new System.Drawing.Point(725, 240);
            this.groupControle.Name = "groupControle";
            this.groupControle.Size = new System.Drawing.Size(112, 184);
            this.groupControle.TabIndex = 58;
            this.groupControle.TabStop = false;
            this.groupControle.Text = "CONTROLE";
            // 
            // btCharmarProximo
            // 
            this.btCharmarProximo.BackColor = System.Drawing.Color.White;
            this.btCharmarProximo.Image = ((System.Drawing.Image)(resources.GetObject("btCharmarProximo.Image")));
            this.btCharmarProximo.Location = new System.Drawing.Point(814, 157);
            this.btCharmarProximo.Name = "btCharmarProximo";
            this.btCharmarProximo.Size = new System.Drawing.Size(68, 58);
            this.btCharmarProximo.TabIndex = 58;
            this.btCharmarProximo.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btCharmarProximo.UseVisualStyleBackColor = false;
            this.btCharmarProximo.Click += new System.EventHandler(this.btCharmarProximo_Click);
            // 
            // tmrRelogio
            // 
            this.tmrRelogio.Enabled = true;
            this.tmrRelogio.Interval = 6000;
            this.tmrRelogio.Tick += new System.EventHandler(this.tmrRelogio_Tick);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Location = new System.Drawing.Point(75, 68);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(863, 8);
            this.groupBox6.TabIndex = 59;
            this.groupBox6.TabStop = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Location = new System.Drawing.Point(0, 373);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(863, 8);
            this.groupBox7.TabIndex = 60;
            this.groupBox7.TabStop = false;
            // 
            // linha
            // 
            this.linha.Controls.Add(this.groupBox9);
            this.linha.Location = new System.Drawing.Point(75, 441);
            this.linha.Name = "linha";
            this.linha.Size = new System.Drawing.Size(863, 8);
            this.linha.TabIndex = 61;
            this.linha.TabStop = false;
            // 
            // groupBox9
            // 
            this.groupBox9.Location = new System.Drawing.Point(0, 373);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(863, 8);
            this.groupBox9.TabIndex = 60;
            this.groupBox9.TabStop = false;
            // 
            // lblDirecao
            // 
            this.lblDirecao.AutoSize = true;
            this.lblDirecao.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblDirecao.ForeColor = System.Drawing.Color.Black;
            this.lblDirecao.Location = new System.Drawing.Point(896, 141);
            this.lblDirecao.Name = "lblDirecao";
            this.lblDirecao.Size = new System.Drawing.Size(16, 13);
            this.lblDirecao.TabIndex = 62;
            this.lblDirecao.Text = "->";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(852, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 61;
            this.label4.Text = "Dire��o";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(784, 141);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(50, 13);
            this.label23.TabIndex = 60;
            this.label23.Text = "Terminal:";
            // 
            // lblNumeroTerminal
            // 
            this.lblNumeroTerminal.AutoSize = true;
            this.lblNumeroTerminal.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblNumeroTerminal.ForeColor = System.Drawing.Color.Black;
            this.lblNumeroTerminal.Location = new System.Drawing.Point(834, 141);
            this.lblNumeroTerminal.Name = "lblNumeroTerminal";
            this.lblNumeroTerminal.Size = new System.Drawing.Size(19, 13);
            this.lblNumeroTerminal.TabIndex = 59;
            this.lblNumeroTerminal.Text = "01";
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox8.Location = new System.Drawing.Point(755, 130);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(175, 8);
            this.groupBox8.TabIndex = 55;
            this.groupBox8.TabStop = false;
            // 
            // lblSair
            // 
            this.lblSair.AutoSize = true;
            this.lblSair.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSair.ForeColor = System.Drawing.Color.White;
            this.lblSair.Location = new System.Drawing.Point(72, 457);
            this.lblSair.Name = "lblSair";
            this.lblSair.Size = new System.Drawing.Size(131, 13);
            this.lblSair.TabIndex = 63;
            this.lblSair.Text = "Para sair tecle (ESC).";
            this.lblSair.Click += new System.EventHandler(this.lblSair_Click);
            // 
            // CLT_ModuloInternet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(1013, 697);
            this.Controls.Add(this.lblSair);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.lblDirecao);
            this.Controls.Add(this.linha);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.groupControle);
            this.Controls.Add(this.lblNumeroTerminal);
            this.Controls.Add(this.groupDados);
            this.Controls.Add(this.btCharmarProximo);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.chkImpressaoAutomatico);
            this.Controls.Add(this.lblMensagem);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.imgPainel);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "CLT_ModuloInternet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CLT_ModuloInternet";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CLT_ModuloInternet_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CLT_ModuloInternet_KeyPress);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CLT_ModuloInternet_KeyDown);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPainel)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupDados.ResumeLayout(false);
            this.groupDados.PerformLayout();
            this.groupControle.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.linha.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblTerminal;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblDataHora;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblNivelAcesso;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox imgPainel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox edtChave;
        private System.Windows.Forms.Label lblMensagem;
        private System.Windows.Forms.CheckBox chkImpressaoAutomatico;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupDados;
        private System.Windows.Forms.TextBox edtCargo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox edtEmpresaCracha;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox edtRazaoSocial;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox edtNomeCompleto;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox edtCPF;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox edtCodigo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btImprimirEtiqueta;
        private System.Windows.Forms.Button btEditarDados;
        private System.Windows.Forms.GroupBox groupControle;
        private System.Windows.Forms.Timer tmrRelogio;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox linha;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button btCharmarProximo;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label lblDirecao;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lblNumeroTerminal;
        private System.Windows.Forms.Label lblSair;
    }
}