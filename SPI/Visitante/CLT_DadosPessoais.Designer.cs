namespace SPI
{
    partial class CLT_DadosPessoais
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CLT_DadosPessoais));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblNivelAcesso = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblDataHora = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btSair = new System.Windows.Forms.Button();
            this.btLocalizar = new System.Windows.Forms.Button();
            this.btCancelar = new System.Windows.Forms.Button();
            this.btSalvar = new System.Windows.Forms.Button();
            this.btAlterar = new System.Windows.Forms.Button();
            this.btNovo = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.lblDirecao = new System.Windows.Forms.Label();
            this.edtConfig = new System.Windows.Forms.Button();
            this.edtChamar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.lblNumeroTerminal = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.tmrRelogio = new System.Windows.Forms.Timer(this.components);
            this.Configurar_impressao = new System.Windows.Forms.ToolStripMenuItem();
            this.Logoff = new System.Windows.Forms.ToolStripMenuItem();
            this.Ferramentas = new System.Windows.Forms.ToolStripMenuItem();
            this.ConfigurarImpressao = new System.Windows.Forms.ToolStripMenuItem();
            this.Logof = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MOpcao = new System.Windows.Forms.ToolStripMenuItem();
            this.mConfigurarEtiqueta = new System.Windows.Forms.ToolStripMenuItem();
            this.mUsuarios = new System.Windows.Forms.ToolStripMenuItem();
            this.mLogoff = new System.Windows.Forms.ToolStripMenuItem();
            this.mFerramentas = new System.Windows.Forms.ToolStripMenuItem();
            this.mImprmirCredencial = new System.Windows.Forms.ToolStripMenuItem();
            this.mLocalizarRegistro = new System.Windows.Forms.ToolStripMenuItem();
            this.administraçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.controleDeEmissãoDeEtiquetasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatoriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importarBancoDeDadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grpDadosPessoais = new System.Windows.Forms.GroupBox();
            this.edtNomeNSD = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.ddlTipoInscricao = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.edtCidade = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.edtTelefone = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.edtEmail = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.edtNiveldeCarreira = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlEntraDePin = new System.Windows.Forms.Panel();
            this.pic6 = new System.Windows.Forms.PictureBox();
            this.pic5 = new System.Windows.Forms.PictureBox();
            this.pic4 = new System.Windows.Forms.PictureBox();
            this.pic3 = new System.Windows.Forms.PictureBox();
            this.pic2 = new System.Windows.Forms.PictureBox();
            this.pic1 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ddlEstado = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.edtObservacoes = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.edtConsultora = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gdwPedidos = new System.Windows.Forms.DataGridView();
            this.Produtos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.edtNomeCracha = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.edtCodigoBarras = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.edtNomeConsultora = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.edtCPF = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btNovaCompra = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlJoias = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.pnlNovaInscricao = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFormaPagamento = new System.Windows.Forms.Label();
            this.lblValor = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.imgCadeado = new System.Windows.Forms.PictureBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btCertificadoLote = new System.Windows.Forms.Button();
            this.btCertificado = new System.Windows.Forms.Button();
            this.lblVersao = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tmrRetirarJoia = new System.Windows.Forms.Timer(this.components);
            this.imprimir2ViaCartãoRealezaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.grpDadosPessoais.SuspendLayout();
            this.pnlEntraDePin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gdwPedidos)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnlJoias.SuspendLayout();
            this.pnlNovaInscricao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCadeado)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.groupBox1.Controls.Add(this.lblNivelAcesso);
            this.groupBox1.Controls.Add(this.lblUsuario);
            this.groupBox1.Location = new System.Drawing.Point(36, 6);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(857, 57);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "OPERADOR";
            // 
            // lblNivelAcesso
            // 
            this.lblNivelAcesso.AutoSize = true;
            this.lblNivelAcesso.Location = new System.Drawing.Point(949, 25);
            this.lblNivelAcesso.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNivelAcesso.Name = "lblNivelAcesso";
            this.lblNivelAcesso.Size = new System.Drawing.Size(0, 17);
            this.lblNivelAcesso.TabIndex = 1;
            this.lblNivelAcesso.Visible = false;
            // 
            // lblUsuario
            // 
            this.lblUsuario.BackColor = System.Drawing.Color.Transparent;
            this.lblUsuario.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblUsuario.Location = new System.Drawing.Point(9, 22);
            this.lblUsuario.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(819, 22);
            this.lblUsuario.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.groupBox2.Controls.Add(this.lblDataHora);
            this.groupBox2.Location = new System.Drawing.Point(1051, 6);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(203, 57);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "DATA - HORA";
            // 
            // lblDataHora
            // 
            this.lblDataHora.BackColor = System.Drawing.Color.Transparent;
            this.lblDataHora.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataHora.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDataHora.Location = new System.Drawing.Point(16, 22);
            this.lblDataHora.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDataHora.Name = "lblDataHora";
            this.lblDataHora.Size = new System.Drawing.Size(164, 22);
            this.lblDataHora.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.groupBox4.Controls.Add(this.btSair);
            this.groupBox4.Controls.Add(this.btLocalizar);
            this.groupBox4.Controls.Add(this.btCancelar);
            this.groupBox4.Controls.Add(this.btSalvar);
            this.groupBox4.Controls.Add(this.btAlterar);
            this.groupBox4.Controls.Add(this.btNovo);
            this.groupBox4.Location = new System.Drawing.Point(901, 70);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(352, 382);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "CONTROLES";
            // 
            // btSair
            // 
            this.btSair.BackColor = System.Drawing.Color.White;
            this.btSair.Image = ((System.Drawing.Image)(resources.GetObject("btSair.Image")));
            this.btSair.Location = new System.Drawing.Point(183, 256);
            this.btSair.Margin = new System.Windows.Forms.Padding(4);
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(147, 112);
            this.btSair.TabIndex = 5;
            this.btSair.Text = "&Logoff";
            this.btSair.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btSair.UseVisualStyleBackColor = false;
            this.btSair.Click += new System.EventHandler(this.btSair_Click);
            // 
            // btLocalizar
            // 
            this.btLocalizar.BackColor = System.Drawing.Color.White;
            this.btLocalizar.Image = ((System.Drawing.Image)(resources.GetObject("btLocalizar.Image")));
            this.btLocalizar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btLocalizar.Location = new System.Drawing.Point(20, 256);
            this.btLocalizar.Margin = new System.Windows.Forms.Padding(4);
            this.btLocalizar.Name = "btLocalizar";
            this.btLocalizar.Size = new System.Drawing.Size(147, 112);
            this.btLocalizar.TabIndex = 4;
            this.btLocalizar.Text = "&Localizar";
            this.btLocalizar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btLocalizar.UseVisualStyleBackColor = false;
            this.btLocalizar.Click += new System.EventHandler(this.btCopiar_Click);
            // 
            // btCancelar
            // 
            this.btCancelar.BackColor = System.Drawing.Color.White;
            this.btCancelar.Enabled = false;
            this.btCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btCancelar.Image")));
            this.btCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btCancelar.Location = new System.Drawing.Point(183, 137);
            this.btCancelar.Margin = new System.Windows.Forms.Padding(4);
            this.btCancelar.Name = "btCancelar";
            this.btCancelar.Size = new System.Drawing.Size(147, 112);
            this.btCancelar.TabIndex = 3;
            this.btCancelar.Text = "&Cancelar";
            this.btCancelar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btCancelar.UseVisualStyleBackColor = false;
            this.btCancelar.Click += new System.EventHandler(this.btCancelar_Click);
            // 
            // btSalvar
            // 
            this.btSalvar.BackColor = System.Drawing.Color.White;
            this.btSalvar.Enabled = false;
            this.btSalvar.Image = ((System.Drawing.Image)(resources.GetObject("btSalvar.Image")));
            this.btSalvar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btSalvar.Location = new System.Drawing.Point(20, 135);
            this.btSalvar.Margin = new System.Windows.Forms.Padding(4);
            this.btSalvar.Name = "btSalvar";
            this.btSalvar.Size = new System.Drawing.Size(147, 113);
            this.btSalvar.TabIndex = 0;
            this.btSalvar.Text = "&Salvar";
            this.btSalvar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btSalvar.UseVisualStyleBackColor = false;
            this.btSalvar.Click += new System.EventHandler(this.btSalvar_Click);
            // 
            // btAlterar
            // 
            this.btAlterar.BackColor = System.Drawing.Color.White;
            this.btAlterar.Image = ((System.Drawing.Image)(resources.GetObject("btAlterar.Image")));
            this.btAlterar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btAlterar.Location = new System.Drawing.Point(183, 20);
            this.btAlterar.Margin = new System.Windows.Forms.Padding(4);
            this.btAlterar.Name = "btAlterar";
            this.btAlterar.Size = new System.Drawing.Size(147, 108);
            this.btAlterar.TabIndex = 1;
            this.btAlterar.Text = "&Modificar";
            this.btAlterar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btAlterar.UseVisualStyleBackColor = false;
            this.btAlterar.Click += new System.EventHandler(this.btAlterar_Click);
            // 
            // btNovo
            // 
            this.btNovo.BackColor = System.Drawing.Color.White;
            this.btNovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btNovo.Image = ((System.Drawing.Image)(resources.GetObject("btNovo.Image")));
            this.btNovo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btNovo.Location = new System.Drawing.Point(19, 20);
            this.btNovo.Margin = new System.Windows.Forms.Padding(4);
            this.btNovo.Name = "btNovo";
            this.btNovo.Size = new System.Drawing.Size(147, 108);
            this.btNovo.TabIndex = 0;
            this.btNovo.Text = "&Adicionar novo";
            this.btNovo.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btNovo.UseVisualStyleBackColor = false;
            this.btNovo.Click += new System.EventHandler(this.btNovo_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.Transparent;
            this.groupBox12.Controls.Add(this.label36);
            this.groupBox12.Controls.Add(this.label35);
            this.groupBox12.Controls.Add(this.lblDirecao);
            this.groupBox12.Controls.Add(this.edtConfig);
            this.groupBox12.Controls.Add(this.edtChamar);
            this.groupBox12.Controls.Add(this.label4);
            this.groupBox12.Controls.Add(this.label23);
            this.groupBox12.Controls.Add(this.lblNumeroTerminal);
            this.groupBox12.Controls.Add(this.label9);
            this.groupBox12.Location = new System.Drawing.Point(1191, 821);
            this.groupBox12.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox12.Size = new System.Drawing.Size(26, 23);
            this.groupBox12.TabIndex = 5;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "CONTROLE REMOTO";
            this.groupBox12.Visible = false;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(165, 121);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(64, 17);
            this.label36.TabIndex = 7;
            this.label36.Text = "CONFIG.";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(44, 121);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(66, 17);
            this.label35.TabIndex = 6;
            this.label35.Text = "CHAMAR";
            // 
            // lblDirecao
            // 
            this.lblDirecao.AutoSize = true;
            this.lblDirecao.BackColor = System.Drawing.Color.Transparent;
            this.lblDirecao.ForeColor = System.Drawing.Color.Black;
            this.lblDirecao.Location = new System.Drawing.Point(172, 23);
            this.lblDirecao.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDirecao.Name = "lblDirecao";
            this.lblDirecao.Size = new System.Drawing.Size(21, 17);
            this.lblDirecao.TabIndex = 5;
            this.lblDirecao.Text = "->";
            // 
            // edtConfig
            // 
            this.edtConfig.BackColor = System.Drawing.Color.White;
            this.edtConfig.ForeColor = System.Drawing.Color.Black;
            this.edtConfig.Image = ((System.Drawing.Image)(resources.GetObject("edtConfig.Image")));
            this.edtConfig.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.edtConfig.Location = new System.Drawing.Point(135, 43);
            this.edtConfig.Margin = new System.Windows.Forms.Padding(4);
            this.edtConfig.Name = "edtConfig";
            this.edtConfig.Size = new System.Drawing.Size(27, 22);
            this.edtConfig.TabIndex = 4;
            this.edtConfig.UseVisualStyleBackColor = false;
            this.edtConfig.Click += new System.EventHandler(this.edtConfig_Click);
            // 
            // edtChamar
            // 
            this.edtChamar.BackColor = System.Drawing.Color.White;
            this.edtChamar.ForeColor = System.Drawing.Color.Black;
            this.edtChamar.Image = ((System.Drawing.Image)(resources.GetObject("edtChamar.Image")));
            this.edtChamar.Location = new System.Drawing.Point(32, 43);
            this.edtChamar.Margin = new System.Windows.Forms.Padding(4);
            this.edtChamar.Name = "edtChamar";
            this.edtChamar.Size = new System.Drawing.Size(95, 74);
            this.edtChamar.TabIndex = 1;
            this.edtChamar.UseVisualStyleBackColor = false;
            this.edtChamar.Click += new System.EventHandler(this.edtChamar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(113, 23);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Direção";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(23, 23);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(67, 17);
            this.label23.TabIndex = 0;
            this.label23.Text = "Terminal:";
            // 
            // lblNumeroTerminal
            // 
            this.lblNumeroTerminal.AutoSize = true;
            this.lblNumeroTerminal.BackColor = System.Drawing.Color.Transparent;
            this.lblNumeroTerminal.ForeColor = System.Drawing.Color.Black;
            this.lblNumeroTerminal.Location = new System.Drawing.Point(89, 23);
            this.lblNumeroTerminal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumeroTerminal.Name = "lblNumeroTerminal";
            this.lblNumeroTerminal.Size = new System.Drawing.Size(24, 17);
            this.lblNumeroTerminal.TabIndex = 2;
            this.lblNumeroTerminal.Text = "01";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(131, 73);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 17);
            this.label9.TabIndex = 12;
            this.label9.Text = "Categoria:";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.groupBox5.Controls.Add(this.lblTerminal);
            this.groupBox5.Location = new System.Drawing.Point(901, 6);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(141, 57);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "TERMINAL";
            // 
            // lblTerminal
            // 
            this.lblTerminal.BackColor = System.Drawing.Color.Transparent;
            this.lblTerminal.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTerminal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTerminal.Location = new System.Drawing.Point(15, 22);
            this.lblTerminal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(115, 22);
            this.lblTerminal.TabIndex = 0;
            // 
            // tmrRelogio
            // 
            this.tmrRelogio.Enabled = true;
            this.tmrRelogio.Interval = 1000;
            this.tmrRelogio.Tick += new System.EventHandler(this.tmrRelogio_Tick);
            // 
            // Configurar_impressao
            // 
            this.Configurar_impressao.Name = "Configurar_impressao";
            this.Configurar_impressao.Size = new System.Drawing.Size(121, 20);
            this.Configurar_impressao.Text = "Configurar impressão";
            // 
            // Logoff
            // 
            this.Logoff.Name = "Logoff";
            this.Logoff.Size = new System.Drawing.Size(50, 20);
            this.Logoff.Text = "Logoff";
            // 
            // Ferramentas
            // 
            this.Ferramentas.Name = "Ferramentas";
            this.Ferramentas.Size = new System.Drawing.Size(80, 20);
            this.Ferramentas.Text = "Ferramentas";
            // 
            // ConfigurarImpressao
            // 
            this.ConfigurarImpressao.Name = "ConfigurarImpressao";
            this.ConfigurarImpressao.Size = new System.Drawing.Size(189, 22);
            this.ConfigurarImpressao.Text = "Configurar Impressão";
            // 
            // Logof
            // 
            this.Logof.Name = "Logof";
            this.Logof.Size = new System.Drawing.Size(116, 22);
            this.Logof.Text = "Logoff";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MOpcao,
            this.mFerramentas,
            this.administraçãoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1369, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MOpcao
            // 
            this.MOpcao.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mConfigurarEtiqueta,
            this.mUsuarios,
            this.mLogoff});
            this.MOpcao.Name = "MOpcao";
            this.MOpcao.Size = new System.Drawing.Size(71, 24);
            this.MOpcao.Text = "Opções";
            // 
            // mConfigurarEtiqueta
            // 
            this.mConfigurarEtiqueta.Name = "mConfigurarEtiqueta";
            this.mConfigurarEtiqueta.Size = new System.Drawing.Size(226, 26);
            this.mConfigurarEtiqueta.Text = "Configurar Impressão";
            this.mConfigurarEtiqueta.Click += new System.EventHandler(this.mConfigurarEtiqueta_Click);
            // 
            // mUsuarios
            // 
            this.mUsuarios.Name = "mUsuarios";
            this.mUsuarios.Size = new System.Drawing.Size(226, 26);
            this.mUsuarios.Text = "Usuários";
            this.mUsuarios.Click += new System.EventHandler(this.mUsuarios_Click);
            // 
            // mLogoff
            // 
            this.mLogoff.Name = "mLogoff";
            this.mLogoff.Size = new System.Drawing.Size(226, 26);
            this.mLogoff.Text = "Logoff";
            this.mLogoff.Click += new System.EventHandler(this.mLogoff_Click);
            // 
            // mFerramentas
            // 
            this.mFerramentas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mImprmirCredencial,
            this.mLocalizarRegistro});
            this.mFerramentas.Name = "mFerramentas";
            this.mFerramentas.Size = new System.Drawing.Size(102, 24);
            this.mFerramentas.Text = "Ferramentas";
            // 
            // mImprmirCredencial
            // 
            this.mImprmirCredencial.BackColor = System.Drawing.Color.Transparent;
            this.mImprmirCredencial.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mImprmirCredencial.Name = "mImprmirCredencial";
            this.mImprmirCredencial.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.mImprmirCredencial.Size = new System.Drawing.Size(237, 26);
            this.mImprmirCredencial.Text = "Imprimir credencial";
            this.mImprmirCredencial.Click += new System.EventHandler(this.mImprmirCredencial_Click);
            // 
            // mLocalizarRegistro
            // 
            this.mLocalizarRegistro.Name = "mLocalizarRegistro";
            this.mLocalizarRegistro.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.mLocalizarRegistro.Size = new System.Drawing.Size(237, 26);
            this.mLocalizarRegistro.Text = "Localizar Registro";
            this.mLocalizarRegistro.Click += new System.EventHandler(this.mLocalizarRegistro_Click);
            // 
            // administraçãoToolStripMenuItem
            // 
            this.administraçãoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.controleDeEmissãoDeEtiquetasToolStripMenuItem,
            this.relatoriosToolStripMenuItem,
            this.importarBancoDeDadosToolStripMenuItem,
            this.imprimir2ViaCartãoRealezaToolStripMenuItem});
            this.administraçãoToolStripMenuItem.Name = "administraçãoToolStripMenuItem";
            this.administraçãoToolStripMenuItem.Size = new System.Drawing.Size(117, 24);
            this.administraçãoToolStripMenuItem.Text = "A&dministração";
            // 
            // controleDeEmissãoDeEtiquetasToolStripMenuItem
            // 
            this.controleDeEmissãoDeEtiquetasToolStripMenuItem.Name = "controleDeEmissãoDeEtiquetasToolStripMenuItem";
            this.controleDeEmissãoDeEtiquetasToolStripMenuItem.Size = new System.Drawing.Size(306, 26);
            this.controleDeEmissãoDeEtiquetasToolStripMenuItem.Text = "Controle de Emissão de Etiquetas";
            this.controleDeEmissãoDeEtiquetasToolStripMenuItem.Click += new System.EventHandler(this.controleDeEmissãoDeEtiquetasToolStripMenuItem_Click);
            // 
            // relatoriosToolStripMenuItem
            // 
            this.relatoriosToolStripMenuItem.Name = "relatoriosToolStripMenuItem";
            this.relatoriosToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F11;
            this.relatoriosToolStripMenuItem.Size = new System.Drawing.Size(306, 26);
            this.relatoriosToolStripMenuItem.Text = "Relatorios";
            this.relatoriosToolStripMenuItem.Click += new System.EventHandler(this.relatoriosToolStripMenuItem_Click);
            // 
            // importarBancoDeDadosToolStripMenuItem
            // 
            this.importarBancoDeDadosToolStripMenuItem.Name = "importarBancoDeDadosToolStripMenuItem";
            this.importarBancoDeDadosToolStripMenuItem.Size = new System.Drawing.Size(306, 26);
            this.importarBancoDeDadosToolStripMenuItem.Text = "Importar banco de dados";
            this.importarBancoDeDadosToolStripMenuItem.Click += new System.EventHandler(this.importarBancoDeDadosToolStripMenuItem_Click);
            // 
            // grpDadosPessoais
            // 
            this.grpDadosPessoais.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.grpDadosPessoais.Controls.Add(this.edtNomeNSD);
            this.grpDadosPessoais.Controls.Add(this.label13);
            this.grpDadosPessoais.Controls.Add(this.ddlTipoInscricao);
            this.grpDadosPessoais.Controls.Add(this.label12);
            this.grpDadosPessoais.Controls.Add(this.edtCidade);
            this.grpDadosPessoais.Controls.Add(this.label11);
            this.grpDadosPessoais.Controls.Add(this.edtTelefone);
            this.grpDadosPessoais.Controls.Add(this.label10);
            this.grpDadosPessoais.Controls.Add(this.edtEmail);
            this.grpDadosPessoais.Controls.Add(this.label8);
            this.grpDadosPessoais.Controls.Add(this.edtNiveldeCarreira);
            this.grpDadosPessoais.Controls.Add(this.label7);
            this.grpDadosPessoais.Controls.Add(this.pnlEntraDePin);
            this.grpDadosPessoais.Controls.Add(this.groupBox3);
            this.grpDadosPessoais.Controls.Add(this.ddlEstado);
            this.grpDadosPessoais.Controls.Add(this.label24);
            this.grpDadosPessoais.Controls.Add(this.edtObservacoes);
            this.grpDadosPessoais.Controls.Add(this.label22);
            this.grpDadosPessoais.Controls.Add(this.edtConsultora);
            this.grpDadosPessoais.Controls.Add(this.label1);
            this.grpDadosPessoais.Controls.Add(this.gdwPedidos);
            this.grpDadosPessoais.Controls.Add(this.edtNomeCracha);
            this.grpDadosPessoais.Controls.Add(this.label21);
            this.grpDadosPessoais.Controls.Add(this.edtCodigoBarras);
            this.grpDadosPessoais.Controls.Add(this.label3);
            this.grpDadosPessoais.Controls.Add(this.edtNomeConsultora);
            this.grpDadosPessoais.Controls.Add(this.label6);
            this.grpDadosPessoais.Controls.Add(this.edtCPF);
            this.grpDadosPessoais.Controls.Add(this.label5);
            this.grpDadosPessoais.Location = new System.Drawing.Point(36, 69);
            this.grpDadosPessoais.Margin = new System.Windows.Forms.Padding(4);
            this.grpDadosPessoais.Name = "grpDadosPessoais";
            this.grpDadosPessoais.Padding = new System.Windows.Forms.Padding(4);
            this.grpDadosPessoais.Size = new System.Drawing.Size(857, 804);
            this.grpDadosPessoais.TabIndex = 0;
            this.grpDadosPessoais.TabStop = false;
            this.grpDadosPessoais.Text = "DADOS PESSOAIS";
            // 
            // edtNomeNSD
            // 
            this.edtNomeNSD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtNomeNSD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtNomeNSD.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtNomeNSD.Location = new System.Drawing.Point(28, 359);
            this.edtNomeNSD.Margin = new System.Windows.Forms.Padding(4);
            this.edtNomeNSD.MaxLength = 100;
            this.edtNomeNSD.Name = "edtNomeNSD";
            this.edtNomeNSD.Size = new System.Drawing.Size(399, 23);
            this.edtNomeNSD.TabIndex = 11;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(25, 340);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(113, 17);
            this.label13.TabIndex = 134;
            this.label13.Text = "Nome da NSD";
            // 
            // ddlTipoInscricao
            // 
            this.ddlTipoInscricao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlTipoInscricao.Enabled = false;
            this.ddlTipoInscricao.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlTipoInscricao.FormattingEnabled = true;
            this.ddlTipoInscricao.Items.AddRange(new object[] {
            "",
            "08073257 - INSCRIÇÃO SEMINÁRIO BRASIL 2018 PROMOCIONAL CONSULTORAS/DIRETORAS - VE" +
                "RDE",
            "08073258 - INSCRIÇÃO SEMINÁRIO BRASIL 2018 PROMOCIONAL CONVIDADOS - VERDE",
            "08073259 - INSCRIÇÃO SEMINÁRIO BRASIL 2018 CONSULTORAS/DIRETORAS - GANHADORAS PRO" +
                "MO 50% DE DESCONTO  – VERDE"});
            this.ddlTipoInscricao.Location = new System.Drawing.Point(28, 300);
            this.ddlTipoInscricao.Margin = new System.Windows.Forms.Padding(4);
            this.ddlTipoInscricao.Name = "ddlTipoInscricao";
            this.ddlTipoInscricao.Size = new System.Drawing.Size(802, 24);
            this.ddlTipoInscricao.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(25, 279);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(149, 17);
            this.label12.TabIndex = 132;
            this.label12.Text = "Tipo de Inscrição:";
            // 
            // edtCidade
            // 
            this.edtCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtCidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtCidade.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtCidade.Location = new System.Drawing.Point(210, 242);
            this.edtCidade.Margin = new System.Windows.Forms.Padding(4);
            this.edtCidade.MaxLength = 100;
            this.edtCidade.Name = "edtCidade";
            this.edtCidade.Size = new System.Drawing.Size(455, 23);
            this.edtCidade.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(207, 221);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 17);
            this.label11.TabIndex = 130;
            this.label11.Text = "Cidade:";
            // 
            // edtTelefone
            // 
            this.edtTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtTelefone.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtTelefone.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtTelefone.Location = new System.Drawing.Point(28, 242);
            this.edtTelefone.Margin = new System.Windows.Forms.Padding(4);
            this.edtTelefone.MaxLength = 100;
            this.edtTelefone.Name = "edtTelefone";
            this.edtTelefone.Size = new System.Drawing.Size(174, 23);
            this.edtTelefone.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(25, 221);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 17);
            this.label10.TabIndex = 128;
            this.label10.Text = "Telefone:";
            // 
            // edtEmail
            // 
            this.edtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtEmail.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtEmail.Location = new System.Drawing.Point(431, 184);
            this.edtEmail.Margin = new System.Windows.Forms.Padding(4);
            this.edtEmail.MaxLength = 100;
            this.edtEmail.Name = "edtEmail";
            this.edtEmail.Size = new System.Drawing.Size(399, 23);
            this.edtEmail.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(428, 163);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 17);
            this.label8.TabIndex = 126;
            this.label8.Text = "E-mail:";
            // 
            // edtNiveldeCarreira
            // 
            this.edtNiveldeCarreira.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtNiveldeCarreira.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtNiveldeCarreira.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtNiveldeCarreira.Location = new System.Drawing.Point(28, 184);
            this.edtNiveldeCarreira.Margin = new System.Windows.Forms.Padding(4);
            this.edtNiveldeCarreira.MaxLength = 100;
            this.edtNiveldeCarreira.Name = "edtNiveldeCarreira";
            this.edtNiveldeCarreira.Size = new System.Drawing.Size(395, 23);
            this.edtNiveldeCarreira.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(25, 163);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(137, 17);
            this.label7.TabIndex = 124;
            this.label7.Text = "Nível de Carreira";
            // 
            // pnlEntraDePin
            // 
            this.pnlEntraDePin.Controls.Add(this.pic6);
            this.pnlEntraDePin.Controls.Add(this.pic5);
            this.pnlEntraDePin.Controls.Add(this.pic4);
            this.pnlEntraDePin.Controls.Add(this.pic3);
            this.pnlEntraDePin.Controls.Add(this.pic2);
            this.pnlEntraDePin.Controls.Add(this.pic1);
            this.pnlEntraDePin.Location = new System.Drawing.Point(27, 670);
            this.pnlEntraDePin.Margin = new System.Windows.Forms.Padding(4);
            this.pnlEntraDePin.Name = "pnlEntraDePin";
            this.pnlEntraDePin.Size = new System.Drawing.Size(803, 125);
            this.pnlEntraDePin.TabIndex = 119;
            this.pnlEntraDePin.Visible = false;
            // 
            // pic6
            // 
            this.pic6.BackColor = System.Drawing.Color.White;
            this.pic6.Location = new System.Drawing.Point(607, 5);
            this.pic6.Margin = new System.Windows.Forms.Padding(4);
            this.pic6.Name = "pic6";
            this.pic6.Size = new System.Drawing.Size(115, 114);
            this.pic6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic6.TabIndex = 5;
            this.pic6.TabStop = false;
            this.pic6.Visible = false;
            // 
            // pic5
            // 
            this.pic5.BackColor = System.Drawing.Color.White;
            this.pic5.Location = new System.Drawing.Point(487, 5);
            this.pic5.Margin = new System.Windows.Forms.Padding(4);
            this.pic5.Name = "pic5";
            this.pic5.Size = new System.Drawing.Size(115, 114);
            this.pic5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic5.TabIndex = 4;
            this.pic5.TabStop = false;
            this.pic5.Visible = false;
            // 
            // pic4
            // 
            this.pic4.BackColor = System.Drawing.Color.White;
            this.pic4.Location = new System.Drawing.Point(367, 5);
            this.pic4.Margin = new System.Windows.Forms.Padding(4);
            this.pic4.Name = "pic4";
            this.pic4.Size = new System.Drawing.Size(115, 114);
            this.pic4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic4.TabIndex = 3;
            this.pic4.TabStop = false;
            this.pic4.Visible = false;
            // 
            // pic3
            // 
            this.pic3.BackColor = System.Drawing.Color.White;
            this.pic3.Location = new System.Drawing.Point(247, 5);
            this.pic3.Margin = new System.Windows.Forms.Padding(4);
            this.pic3.Name = "pic3";
            this.pic3.Size = new System.Drawing.Size(115, 114);
            this.pic3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic3.TabIndex = 2;
            this.pic3.TabStop = false;
            this.pic3.Visible = false;
            // 
            // pic2
            // 
            this.pic2.BackColor = System.Drawing.Color.White;
            this.pic2.Location = new System.Drawing.Point(127, 5);
            this.pic2.Margin = new System.Windows.Forms.Padding(4);
            this.pic2.Name = "pic2";
            this.pic2.Size = new System.Drawing.Size(115, 114);
            this.pic2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic2.TabIndex = 1;
            this.pic2.TabStop = false;
            this.pic2.Visible = false;
            // 
            // pic1
            // 
            this.pic1.BackColor = System.Drawing.Color.White;
            this.pic1.Location = new System.Drawing.Point(7, 5);
            this.pic1.Margin = new System.Windows.Forms.Padding(4);
            this.pic1.Name = "pic1";
            this.pic1.Size = new System.Drawing.Size(115, 114);
            this.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic1.TabIndex = 0;
            this.pic1.TabStop = false;
            this.pic1.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(28, 98);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(800, 8);
            this.groupBox3.TabIndex = 122;
            this.groupBox3.TabStop = false;
            // 
            // ddlEstado
            // 
            this.ddlEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEstado.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlEstado.FormattingEnabled = true;
            this.ddlEstado.Items.AddRange(new object[] {
            "",
            "AC",
            "AL",
            "AM",
            "AP",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MG",
            "MS",
            "MT",
            "PA",
            "PB",
            "PE",
            "PI",
            "PR",
            "RJ",
            "RN",
            "RO",
            "RR",
            "RS",
            "SC",
            "SE",
            "SP",
            "TO",
            "EX"});
            this.ddlEstado.Location = new System.Drawing.Point(673, 242);
            this.ddlEstado.Margin = new System.Windows.Forms.Padding(4);
            this.ddlEstado.Name = "ddlEstado";
            this.ddlEstado.Size = new System.Drawing.Size(157, 24);
            this.ddlEstado.TabIndex = 9;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(670, 221);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(67, 17);
            this.label24.TabIndex = 121;
            this.label24.Text = "Estado:";
            // 
            // edtObservacoes
            // 
            this.edtObservacoes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtObservacoes.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtObservacoes.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtObservacoes.Location = new System.Drawing.Point(431, 359);
            this.edtObservacoes.Margin = new System.Windows.Forms.Padding(4);
            this.edtObservacoes.MaxLength = 100;
            this.edtObservacoes.Name = "edtObservacoes";
            this.edtObservacoes.Size = new System.Drawing.Size(399, 23);
            this.edtObservacoes.TabIndex = 12;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(428, 340);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(113, 17);
            this.label22.TabIndex = 118;
            this.label22.Text = "Observações:";
            // 
            // edtConsultora
            // 
            this.edtConsultora.BackColor = System.Drawing.Color.Pink;
            this.edtConsultora.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtConsultora.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtConsultora.Enabled = false;
            this.edtConsultora.Font = new System.Drawing.Font("Verdana", 16.2F);
            this.edtConsultora.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.edtConsultora.Location = new System.Drawing.Point(295, 51);
            this.edtConsultora.Margin = new System.Windows.Forms.Padding(4);
            this.edtConsultora.MaxLength = 11;
            this.edtConsultora.Name = "edtConsultora";
            this.edtConsultora.Size = new System.Drawing.Size(258, 40);
            this.edtConsultora.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(292, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 17);
            this.label1.TabIndex = 94;
            this.label1.Text = "COD. CONSULTORA";
            // 
            // gdwPedidos
            // 
            this.gdwPedidos.AllowUserToAddRows = false;
            this.gdwPedidos.AllowUserToDeleteRows = false;
            this.gdwPedidos.AllowUserToResizeColumns = false;
            this.gdwPedidos.AllowUserToResizeRows = false;
            this.gdwPedidos.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gdwPedidos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.gdwPedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdwPedidos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Produtos});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gdwPedidos.DefaultCellStyle = dataGridViewCellStyle7;
            this.gdwPedidos.GridColor = System.Drawing.Color.White;
            this.gdwPedidos.Location = new System.Drawing.Point(27, 390);
            this.gdwPedidos.Margin = new System.Windows.Forms.Padding(4);
            this.gdwPedidos.MultiSelect = false;
            this.gdwPedidos.Name = "gdwPedidos";
            this.gdwPedidos.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gdwPedidos.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.gdwPedidos.RowHeadersVisible = false;
            this.gdwPedidos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gdwPedidos.Size = new System.Drawing.Size(803, 272);
            this.gdwPedidos.TabIndex = 13;
            this.gdwPedidos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdwPedidos_CellContentClick);
            // 
            // Produtos
            // 
            this.Produtos.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Produtos.DefaultCellStyle = dataGridViewCellStyle6;
            this.Produtos.HeaderText = "Reconhecimentos";
            this.Produtos.Name = "Produtos";
            this.Produtos.ReadOnly = true;
            // 
            // edtNomeCracha
            // 
            this.edtNomeCracha.BackColor = System.Drawing.SystemColors.Info;
            this.edtNomeCracha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtNomeCracha.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtNomeCracha.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtNomeCracha.Location = new System.Drawing.Point(431, 133);
            this.edtNomeCracha.Margin = new System.Windows.Forms.Padding(4);
            this.edtNomeCracha.MaxLength = 100;
            this.edtNomeCracha.Name = "edtNomeCracha";
            this.edtNomeCracha.Size = new System.Drawing.Size(399, 23);
            this.edtNomeCracha.TabIndex = 4;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(428, 112);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(139, 17);
            this.label21.TabIndex = 6;
            this.label21.Text = "Nome no crachá:";
            // 
            // edtCodigoBarras
            // 
            this.edtCodigoBarras.BackColor = System.Drawing.SystemColors.Info;
            this.edtCodigoBarras.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtCodigoBarras.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtCodigoBarras.Enabled = false;
            this.edtCodigoBarras.Font = new System.Drawing.Font("Verdana", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtCodigoBarras.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.edtCodigoBarras.Location = new System.Drawing.Point(28, 51);
            this.edtCodigoBarras.Margin = new System.Windows.Forms.Padding(4);
            this.edtCodigoBarras.MaxLength = 11;
            this.edtCodigoBarras.Name = "edtCodigoBarras";
            this.edtCodigoBarras.Size = new System.Drawing.Size(259, 40);
            this.edtCodigoBarras.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(25, 30);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Código de Barras:";
            // 
            // edtNomeConsultora
            // 
            this.edtNomeConsultora.BackColor = System.Drawing.SystemColors.Info;
            this.edtNomeConsultora.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtNomeConsultora.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtNomeConsultora.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtNomeConsultora.Location = new System.Drawing.Point(28, 133);
            this.edtNomeConsultora.Margin = new System.Windows.Forms.Padding(4);
            this.edtNomeConsultora.MaxLength = 60;
            this.edtNomeConsultora.Name = "edtNomeConsultora";
            this.edtNomeConsultora.Size = new System.Drawing.Size(395, 23);
            this.edtNomeConsultora.TabIndex = 3;
            this.edtNomeConsultora.TextChanged += new System.EventHandler(this.edtNomeConsultora_TextChanged_1);
            this.edtNomeConsultora.Leave += new System.EventHandler(this.edtNomeCompleto_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(25, 112);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(137, 17);
            this.label6.TabIndex = 4;
            this.label6.Text = "Nome Completo:";
            // 
            // edtCPF
            // 
            this.edtCPF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtCPF.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtCPF.Font = new System.Drawing.Font("Verdana", 16.2F);
            this.edtCPF.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.edtCPF.Location = new System.Drawing.Point(561, 51);
            this.edtCPF.Margin = new System.Windows.Forms.Padding(4);
            this.edtCPF.MaxLength = 11;
            this.edtCPF.Name = "edtCPF";
            this.edtCPF.Size = new System.Drawing.Size(267, 40);
            this.edtCPF.TabIndex = 2;
            this.edtCPF.Leave += new System.EventHandler(this.edtCPF_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(558, 30);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "CPF:";
            // 
            // btNovaCompra
            // 
            this.btNovaCompra.Location = new System.Drawing.Point(901, 764);
            this.btNovaCompra.Margin = new System.Windows.Forms.Padding(4);
            this.btNovaCompra.Name = "btNovaCompra";
            this.btNovaCompra.Size = new System.Drawing.Size(177, 28);
            this.btNovaCompra.TabIndex = 32;
            this.btNovaCompra.Text = "Novo pedido";
            this.btNovaCompra.UseVisualStyleBackColor = true;
            this.btNovaCompra.Visible = false;
            this.btNovaCompra.Click += new System.EventHandler(this.btNovaCompra_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pnlJoias);
            this.panel1.Controls.Add(this.pnlNovaInscricao);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.imgCadeado);
            this.panel1.Controls.Add(this.groupBox6);
            this.panel1.Controls.Add(this.groupBox12);
            this.panel1.Controls.Add(this.lblVersao);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox5);
            this.panel1.Controls.Add(this.grpDadosPessoais);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.btNovaCompra);
            this.panel1.Location = new System.Drawing.Point(41, 42);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1275, 872);
            this.panel1.TabIndex = 1;
            // 
            // pnlJoias
            // 
            this.pnlJoias.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pnlJoias.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlJoias.Controls.Add(this.label14);
            this.pnlJoias.Controls.Add(this.label16);
            this.pnlJoias.Location = new System.Drawing.Point(904, 565);
            this.pnlJoias.Name = "pnlJoias";
            this.pnlJoias.Size = new System.Drawing.Size(349, 100);
            this.pnlJoias.TabIndex = 76;
            this.pnlJoias.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(112, 10);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(122, 17);
            this.label14.TabIndex = 76;
            this.label14.Text = "SALA DE JOIAS";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(19, 45);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(308, 17);
            this.label16.TabIndex = 74;
            this.label16.Text = "SOLICITAR PRESENÇA NA SALA DE PREMIOS";
            // 
            // pnlNovaInscricao
            // 
            this.pnlNovaInscricao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.pnlNovaInscricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNovaInscricao.Controls.Add(this.label2);
            this.pnlNovaInscricao.Controls.Add(this.lblFormaPagamento);
            this.pnlNovaInscricao.Controls.Add(this.lblValor);
            this.pnlNovaInscricao.Location = new System.Drawing.Point(904, 459);
            this.pnlNovaInscricao.Name = "pnlNovaInscricao";
            this.pnlNovaInscricao.Size = new System.Drawing.Size(349, 100);
            this.pnlNovaInscricao.TabIndex = 75;
            this.pnlNovaInscricao.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 10);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 17);
            this.label2.TabIndex = 76;
            this.label2.Text = "NOVA INSCRIÇÃO";
            // 
            // lblFormaPagamento
            // 
            this.lblFormaPagamento.AutoSize = true;
            this.lblFormaPagamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormaPagamento.Location = new System.Drawing.Point(28, 65);
            this.lblFormaPagamento.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFormaPagamento.Name = "lblFormaPagamento";
            this.lblFormaPagamento.Size = new System.Drawing.Size(175, 17);
            this.lblFormaPagamento.TabIndex = 75;
            this.lblFormaPagamento.Text = "FORMA PAGTO: CARTÃO";
            // 
            // lblValor
            // 
            this.lblValor.AutoSize = true;
            this.lblValor.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValor.Location = new System.Drawing.Point(28, 37);
            this.lblValor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(113, 17);
            this.lblValor.TabIndex = 74;
            this.lblValor.Text = "VALOR: R$ 0,00";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(901, 734);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 73;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // imgCadeado
            // 
            this.imgCadeado.Location = new System.Drawing.Point(1225, 805);
            this.imgCadeado.Margin = new System.Windows.Forms.Padding(4);
            this.imgCadeado.Name = "imgCadeado";
            this.imgCadeado.Size = new System.Drawing.Size(41, 39);
            this.imgCadeado.TabIndex = 72;
            this.imgCadeado.TabStop = false;
            this.imgCadeado.Click += new System.EventHandler(this.imgCadeado_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btCertificadoLote);
            this.groupBox6.Controls.Add(this.btCertificado);
            this.groupBox6.Location = new System.Drawing.Point(1225, 739);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox6.Size = new System.Drawing.Size(32, 34);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Visible = false;
            // 
            // btCertificadoLote
            // 
            this.btCertificadoLote.BackColor = System.Drawing.Color.White;
            this.btCertificadoLote.Image = ((System.Drawing.Image)(resources.GetObject("btCertificadoLote.Image")));
            this.btCertificadoLote.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btCertificadoLote.Location = new System.Drawing.Point(183, 18);
            this.btCertificadoLote.Margin = new System.Windows.Forms.Padding(4);
            this.btCertificadoLote.Name = "btCertificadoLote";
            this.btCertificadoLote.Size = new System.Drawing.Size(147, 112);
            this.btCertificadoLote.TabIndex = 6;
            this.btCertificadoLote.Text = "Certificado em Lotes";
            this.btCertificadoLote.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btCertificadoLote.UseVisualStyleBackColor = false;
            this.btCertificadoLote.Visible = false;
            this.btCertificadoLote.Click += new System.EventHandler(this.btCertificadoLote_Click);
            // 
            // btCertificado
            // 
            this.btCertificado.BackColor = System.Drawing.Color.White;
            this.btCertificado.Image = ((System.Drawing.Image)(resources.GetObject("btCertificado.Image")));
            this.btCertificado.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btCertificado.Location = new System.Drawing.Point(20, 18);
            this.btCertificado.Margin = new System.Windows.Forms.Padding(4);
            this.btCertificado.Name = "btCertificado";
            this.btCertificado.Size = new System.Drawing.Size(147, 112);
            this.btCertificado.TabIndex = 5;
            this.btCertificado.Text = "Certificado";
            this.btCertificado.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btCertificado.UseVisualStyleBackColor = false;
            this.btCertificado.Click += new System.EventHandler(this.btCertificado_Click);
            // 
            // lblVersao
            // 
            this.lblVersao.AutoSize = true;
            this.lblVersao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersao.Location = new System.Drawing.Point(901, 805);
            this.lblVersao.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblVersao.Name = "lblVersao";
            this.lblVersao.Size = new System.Drawing.Size(32, 17);
            this.lblVersao.TabIndex = 6;
            this.lblVersao.Text = "V.3";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gray;
            this.panel2.Location = new System.Drawing.Point(83, 69);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1241, 852);
            this.panel2.TabIndex = 2;
            // 
            // tmrRetirarJoia
            // 
            this.tmrRetirarJoia.Interval = 1000;
            this.tmrRetirarJoia.Tick += new System.EventHandler(this.tmrRetirarJoia_Tick);
            // 
            // imprimir2ViaCartãoRealezaToolStripMenuItem
            // 
            this.imprimir2ViaCartãoRealezaToolStripMenuItem.Name = "imprimir2ViaCartãoRealezaToolStripMenuItem";
            this.imprimir2ViaCartãoRealezaToolStripMenuItem.Size = new System.Drawing.Size(306, 26);
            this.imprimir2ViaCartãoRealezaToolStripMenuItem.Text = "Imprimir 2 via cartão realeza";
            this.imprimir2ViaCartãoRealezaToolStripMenuItem.Click += new System.EventHandler(this.imprimir2ViaCartãoRealezaToolStripMenuItem_Click);
            // 
            // CLT_DadosPessoais
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1369, 943);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CLT_DadosPessoais";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CLT_DadosPessoais_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CLT_DadosPessoais_KeyPress);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.grpDadosPessoais.ResumeLayout(false);
            this.grpDadosPessoais.PerformLayout();
            this.pnlEntraDePin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gdwPedidos)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlJoias.ResumeLayout(false);
            this.pnlJoias.PerformLayout();
            this.pnlNovaInscricao.ResumeLayout(false);
            this.pnlNovaInscricao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCadeado)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblDataHora;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblTerminal;
        private System.Windows.Forms.Button btNovo;
        private System.Windows.Forms.Button btSair;
        private System.Windows.Forms.Button btLocalizar;
        private System.Windows.Forms.Button btCancelar;
        private System.Windows.Forms.Button btSalvar;
        private System.Windows.Forms.Button btAlterar;
        private System.Windows.Forms.Timer tmrRelogio;
        private System.Windows.Forms.ToolStripMenuItem Configurar_impressao;
        private System.Windows.Forms.ToolStripMenuItem Logoff;
        private System.Windows.Forms.ToolStripMenuItem Ferramentas;
        private System.Windows.Forms.ToolStripMenuItem ConfigurarImpressao;
        private System.Windows.Forms.ToolStripMenuItem Logof;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MOpcao;
        private System.Windows.Forms.ToolStripMenuItem mConfigurarEtiqueta;
        private System.Windows.Forms.ToolStripMenuItem mLogoff;
        private System.Windows.Forms.ToolStripMenuItem mFerramentas;
        private System.Windows.Forms.ToolStripMenuItem mImprmirCredencial;
        private System.Windows.Forms.Label lblNivelAcesso;
        private System.Windows.Forms.ToolStripMenuItem mLocalizarRegistro;
        private System.Windows.Forms.ToolStripMenuItem mUsuarios;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label lblDirecao;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lblNumeroTerminal;
        private System.Windows.Forms.Button edtConfig;
        private System.Windows.Forms.Button edtChamar;
        private System.Windows.Forms.GroupBox grpDadosPessoais;
        private System.Windows.Forms.TextBox edtNomeConsultora;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox edtCPF;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripMenuItem administraçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem controleDeEmissãoDeEtiquetasToolStripMenuItem;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox edtCodigoBarras;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblVersao;
        private System.Windows.Forms.TextBox edtNomeCracha;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btNovaCompra;
        private System.Windows.Forms.DataGridView gdwPedidos;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btCertificado;
        private System.Windows.Forms.Button btCertificadoLote;
        private System.Windows.Forms.ToolStripMenuItem relatoriosToolStripMenuItem;
        private System.Windows.Forms.PictureBox imgCadeado;
        private System.Windows.Forms.TextBox edtConsultora;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer tmrRetirarJoia;
        private System.Windows.Forms.TextBox edtObservacoes;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ToolStripMenuItem importarBancoDeDadosToolStripMenuItem;
        private System.Windows.Forms.Panel pnlEntraDePin;
        private System.Windows.Forms.PictureBox pic5;
        private System.Windows.Forms.PictureBox pic4;
        private System.Windows.Forms.PictureBox pic3;
        private System.Windows.Forms.PictureBox pic2;
        private System.Windows.Forms.PictureBox pic1;
        private System.Windows.Forms.ComboBox ddlEstado;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox edtEmail;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox edtNiveldeCarreira;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox ddlTipoInscricao;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox edtCidade;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox edtTelefone;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pic6;
        private System.Windows.Forms.TextBox edtNomeNSD;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Produtos;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pnlNovaInscricao;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblFormaPagamento;
        private System.Windows.Forms.Label lblValor;
        private System.Windows.Forms.Panel pnlJoias;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ToolStripMenuItem imprimir2ViaCartãoRealezaToolStripMenuItem;
    }
}