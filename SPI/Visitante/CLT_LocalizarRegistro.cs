using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_LocalizarRegistro : Form
    {
        public DataTable dtTodosRegistros;
        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        public string stCodigoBarras = "";
        public string stMetodoBusca = "";
        public string stPalavraChave = "";
        

        public CLT_LocalizarRegistro()
        {
            InitializeComponent();
        }

        private void edtPalavra_TextChanged(object sender, EventArgs e)
        {
            MI_RealizaBusca();
        }

        private string MI_LocalizaPeloCodigo(string prstCodigo)
        {
            string stCodigoBarras = string.Empty;
            string stContendo = "0";

            CAT_CadastrarCodigos IAT_Codigos = new CAT_CadastrarCodigos();
            CTO_Codigos ITO_Codigos = IAT_Codigos.MS_ObterPeloCodFilha(prstCodigo);

            if (ITO_Codigos.getCodigoBarras() != string.Empty)
            {
                stCodigoBarras = ITO_Codigos.getCodigoBarras();
                
                if (chkContendo.Checked == true)
                    stContendo = "1";

                MI_LocalizarRegistro(stCodigoBarras, stContendo, "CodigoBarras");
            }
            return stCodigoBarras;
        }

        private void MI_RealizaBusca()
        {
            string stContendo = "0";

            if ((edtPalavra.Text.Trim() != null) && (edtPalavra.Text.Length >= 4))
            {
                if (chkContendo.Checked == true)
                    stContendo = "1";

                MI_LocalizarRegistro(edtPalavra.Text, stContendo, MI_TipoBusca());
            }
            else
            {
                try
                {
                    if ((edtPalavra.Text.Length >= 4) && (dtTodosRegistros.Rows.Count != 0))
                        MI_FFF(edtPalavra.Text);
                }
                catch
                {
                    if (chkContendo.Checked == true)
                        stContendo = "1";

                    MI_LocalizarRegistro(edtPalavra.Text, stContendo, MI_TipoBusca());
                }
            }
        }

        private string MI_TipoBusca()
        {
            if (rbtConsultora.Checked == true)
                return "Consultora";

            if (rbtCPF.Checked == true)
                return "CPF";

            if (rbtNome.Checked == true)
                return "NomedaConsultora";

            if (rbtCodigo.Checked == true)
                return "Res1";

            return "NomedaConsultora";
        }

        private void MI_LocalizarRegistro(string prPalavra, string prContendo, string stTipoBusca)
        {
            DataTable dtRegistros = new DataTable();
            CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();

            dtRegistros = IAT_CadastrarVisitante.MS_ObterRegistro(prPalavra, prContendo, stTipoBusca);

            dgwRegistros.DataSource = dtRegistros;
            dtTodosRegistros = dtRegistros;

            foreach (DataGridViewRow Row in dgwRegistros.Rows)
            {
                Row.Height = 46;

                if ((Row.Cells[3].Value.ToString().ToUpper().Contains("CONVIDADOS")) || ("CONVIDADOS").Contains(Row.Cells[3].Value.ToString().ToUpper()))
                {
                    Row.Cells[4].Value = "CONVIDADO";
                }
                else
                {
                    Row.Cells[4].Value = "CONSULTORA";
                }

            }

        }

        private void MI_FFF(string prPalavra)
        {
            DataView dv = new DataView(dtTodosRegistros.Copy());

            dv.RowFilter = MI_TipoBusca() + " Like '%" + prPalavra + "%'";

            dgwRegistros.DataSource = dv.ToTable();
            dgwRegistros.Columns[0].Visible = false;
            dgwRegistros.Columns[2].Width = 500;

            dgwRegistros.Columns[3].Width = 200;
            dgwRegistros.Columns[3].Visible = false;

            dgwRegistros.Columns[4].Width = 150;
            dgwRegistros.Columns[4].Visible = false;

            dgwRegistros.Columns[5].Width = 140;
            dgwRegistros.Columns[5].Visible = false;
        }

        private void dgwRegistros_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            stCodigoBarras = dgwRegistros.CurrentRow.Cells[0].Value.ToString();
            Close();
        }

        private void dgwRegistros_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (dgwRegistros.Rows.Count > 0)
                {
                    stCodigoBarras = dgwRegistros.CurrentRow.Cells[0].Value.ToString();
                    this.Dispose();
                }
            }
        }

        private void CLT_LocalizarRegistro_Load(object sender, EventArgs e)
        {
            if (stMetodoBusca != string.Empty)
                MI_CarregaMetodoBusca(stMetodoBusca);

            CarregaUsuario();
        }

        private void CarregaUsuario()
        {
            lblUsuario.Text = CSV_Global.getUsuario();
            lblTerminal.Text = CSV_Global.getNumeroTerminal();
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void MI_CarregaMetodoBusca(string stMetodoBusca)
        {
            switch (stMetodoBusca)
            { 
                case "CPF":
                    rbtCPF.Checked = true;
                    break;
                case "NOME":
                    rbtNome.Checked = true; 
                    break;
                default:
                    rbtNome.Checked = true;
                    break;
            }

            edtPalavra.Text = stPalavraChave;
            MI_RealizaBusca();
        }

        private void rbtCPF_CheckedChanged(object sender, EventArgs e)
        {
            edtPalavra.Focus();
        }

        private void rbtCNPJ_CheckedChanged(object sender, EventArgs e)
        {
            edtPalavra.Focus();
        }

        private void rbtNome_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtNome.Checked == false)
                return;

            MessageBox.Show("Busca por nome s� pode ser realizada mediante a senha do supervisor.", "Aten��o", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            //VERIFICAR LIBERA��O ATRAVEZ DE SENHA
            Clt_SenhaLiberacaoConteudo OpenSenha = new Clt_SenhaLiberacaoConteudo();
            OpenSenha.ShowDialog();

            if (OpenSenha.AP_Result == false)
            {
                rbtConsultora.Checked = true;
                return;
            }
            else
            {
                edtPalavra.Focus();
            }
        }

        private void rbtEmpresa_CheckedChanged(object sender, EventArgs e)
        {
            edtPalavra.Focus();
        }

        private void CLT_LocalizarRegistro_KeyPress(object sender, KeyPressEventArgs e)
        {
            int itTecla = e.KeyChar;
            switch (itTecla.ToString())
            {
                case "13":
                    e.Handled = true;
                    SendKeys.Send("{tab}");
                    break;
                case "27":
                    this.Close();
                    break;
            }
        }

        private void tmrTempo_Tick(object sender, EventArgs e)
        {
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void rbtLocalizese_CheckedChanged(object sender, EventArgs e)
        {
            edtPalavra.Focus();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void rbtCodigo_CheckedChanged_1(object sender, EventArgs e)
        {
            edtPalavra.Focus();
        }

        private void rbtCodigoBarras_CheckedChanged_1(object sender, EventArgs e)
        {
            edtPalavra.Focus();
        }
    }
}