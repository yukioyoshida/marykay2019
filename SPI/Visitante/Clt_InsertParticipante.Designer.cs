﻿namespace SPI
{
    partial class Clt_InsertParticipante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btoCancelar = new System.Windows.Forms.Button();
            this.btAcessar = new System.Windows.Forms.Button();
            this.edtConsultora = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btoValidar = new System.Windows.Forms.Button();
            this.gdwInscricao = new System.Windows.Forms.DataGridView();
            this.CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONSULTORA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipodeInscricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNSD = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblCPF = new System.Windows.Forms.Label();
            this.lblNomeCompleto = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ddlTipoInscricao = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.edtValor = new System.Windows.Forms.TextBox();
            this.edtFormaPagamento = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdwInscricao)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(41, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(284, 25);
            this.label1.TabIndex = 8;
            this.label1.Text = "Informe o código da consultora:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.btoCancelar);
            this.panel1.Controls.Add(this.btAcessar);
            this.panel1.Location = new System.Drawing.Point(-1, 639);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(947, 121);
            this.panel1.TabIndex = 9;
            // 
            // btoCancelar
            // 
            this.btoCancelar.Location = new System.Drawing.Point(47, 16);
            this.btoCancelar.Margin = new System.Windows.Forms.Padding(4);
            this.btoCancelar.Name = "btoCancelar";
            this.btoCancelar.Size = new System.Drawing.Size(157, 59);
            this.btoCancelar.TabIndex = 1;
            this.btoCancelar.Text = "Cancelar";
            this.btoCancelar.UseVisualStyleBackColor = true;
            this.btoCancelar.Click += new System.EventHandler(this.btoCancelar_Click);
            // 
            // btAcessar
            // 
            this.btAcessar.Location = new System.Drawing.Point(733, 16);
            this.btAcessar.Margin = new System.Windows.Forms.Padding(4);
            this.btAcessar.Name = "btAcessar";
            this.btAcessar.Size = new System.Drawing.Size(157, 59);
            this.btAcessar.TabIndex = 0;
            this.btAcessar.Text = "Inserir participante";
            this.btAcessar.UseVisualStyleBackColor = true;
            this.btAcessar.Click += new System.EventHandler(this.btAcessar_Click);
            // 
            // edtConsultora
            // 
            this.edtConsultora.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtConsultora.Location = new System.Drawing.Point(46, 70);
            this.edtConsultora.Margin = new System.Windows.Forms.Padding(4);
            this.edtConsultora.Name = "edtConsultora";
            this.edtConsultora.Size = new System.Drawing.Size(279, 41);
            this.edtConsultora.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(46, 125);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(857, 8);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // btoValidar
            // 
            this.btoValidar.Location = new System.Drawing.Point(333, 70);
            this.btoValidar.Margin = new System.Windows.Forms.Padding(4);
            this.btoValidar.Name = "btoValidar";
            this.btoValidar.Size = new System.Drawing.Size(157, 41);
            this.btoValidar.TabIndex = 12;
            this.btoValidar.Text = "Validar";
            this.btoValidar.UseVisualStyleBackColor = true;
            this.btoValidar.Click += new System.EventHandler(this.btoValidar_Click);
            // 
            // gdwInscricao
            // 
            this.gdwInscricao.AllowUserToAddRows = false;
            this.gdwInscricao.AllowUserToDeleteRows = false;
            this.gdwInscricao.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.gdwInscricao.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gdwInscricao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdwInscricao.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CODIGO,
            this.CONSULTORA,
            this.NOME,
            this.TipodeInscricao});
            this.gdwInscricao.Cursor = System.Windows.Forms.Cursors.Hand;
            this.gdwInscricao.Location = new System.Drawing.Point(46, 290);
            this.gdwInscricao.Margin = new System.Windows.Forms.Padding(4);
            this.gdwInscricao.MultiSelect = false;
            this.gdwInscricao.Name = "gdwInscricao";
            this.gdwInscricao.ReadOnly = true;
            this.gdwInscricao.Size = new System.Drawing.Size(857, 203);
            this.gdwInscricao.TabIndex = 13;
            // 
            // CODIGO
            // 
            this.CODIGO.DataPropertyName = "CODIGO";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CODIGO.DefaultCellStyle = dataGridViewCellStyle1;
            this.CODIGO.HeaderText = "CODIGO";
            this.CODIGO.Name = "CODIGO";
            this.CODIGO.ReadOnly = true;
            this.CODIGO.Visible = false;
            // 
            // CONSULTORA
            // 
            this.CONSULTORA.DataPropertyName = "CONSULTORA";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CONSULTORA.DefaultCellStyle = dataGridViewCellStyle2;
            this.CONSULTORA.HeaderText = "COD.";
            this.CONSULTORA.Name = "CONSULTORA";
            this.CONSULTORA.ReadOnly = true;
            this.CONSULTORA.Width = 50;
            // 
            // NOME
            // 
            this.NOME.DataPropertyName = "NOME";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.NOME.DefaultCellStyle = dataGridViewCellStyle3;
            this.NOME.HeaderText = "NOME";
            this.NOME.Name = "NOME";
            this.NOME.ReadOnly = true;
            this.NOME.Width = 270;
            // 
            // TipodeInscricao
            // 
            this.TipodeInscricao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TipodeInscricao.DataPropertyName = "TipodeInscricao";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TipodeInscricao.DefaultCellStyle = dataGridViewCellStyle4;
            this.TipodeInscricao.HeaderText = "INSCRICAO";
            this.TipodeInscricao.Name = "TipodeInscricao";
            this.TipodeInscricao.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.lblNSD);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.lblCPF);
            this.panel2.Controls.Add(this.lblNomeCompleto);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(46, 146);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(857, 137);
            this.panel2.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(-1, 98);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(176, 25);
            this.label3.TabIndex = 15;
            this.label3.Text = "NSD:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNSD
            // 
            this.lblNSD.BackColor = System.Drawing.Color.White;
            this.lblNSD.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNSD.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNSD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblNSD.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblNSD.Location = new System.Drawing.Point(183, 92);
            this.lblNSD.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNSD.Name = "lblNSD";
            this.lblNSD.Size = new System.Drawing.Size(659, 31);
            this.lblNSD.TabIndex = 14;
            this.lblNSD.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(-1, 56);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(176, 25);
            this.label4.TabIndex = 13;
            this.label4.Text = "CPF:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCPF
            // 
            this.lblCPF.BackColor = System.Drawing.Color.White;
            this.lblCPF.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCPF.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCPF.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCPF.Location = new System.Drawing.Point(183, 50);
            this.lblCPF.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(659, 31);
            this.lblCPF.TabIndex = 12;
            this.lblCPF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNomeCompleto
            // 
            this.lblNomeCompleto.BackColor = System.Drawing.Color.White;
            this.lblNomeCompleto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNomeCompleto.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeCompleto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblNomeCompleto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblNomeCompleto.Location = new System.Drawing.Point(183, 7);
            this.lblNomeCompleto.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNomeCompleto.Name = "lblNomeCompleto";
            this.lblNomeCompleto.Size = new System.Drawing.Size(659, 31);
            this.lblNomeCompleto.TabIndex = 10;
            this.lblNomeCompleto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(-1, 13);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(176, 25);
            this.label2.TabIndex = 9;
            this.label2.Text = "Nome completo:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ddlTipoInscricao
            // 
            this.ddlTipoInscricao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlTipoInscricao.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlTipoInscricao.FormattingEnabled = true;
            this.ddlTipoInscricao.Items.AddRange(new object[] {
            "",
            "08073257 - INSCRIÇÃO SEMINÁRIO BRASIL 2018 PROMOCIONAL CONSULTORAS/DIRETORAS - VE" +
                "RDE",
            "08073258 - INSCRIÇÃO SEMINÁRIO BRASIL 2018 PROMOCIONAL CONVIDADOS - VERDE",
            "08073259 - INSCRIÇÃO SEMINÁRIO BRASIL 2018 CONSULTORAS/DIRETORAS - GANHADORAS PRO" +
                "MO 50% DE DESCONTO  – VERDE"});
            this.ddlTipoInscricao.Location = new System.Drawing.Point(46, 526);
            this.ddlTipoInscricao.Margin = new System.Windows.Forms.Padding(4);
            this.ddlTipoInscricao.Name = "ddlTipoInscricao";
            this.ddlTipoInscricao.Size = new System.Drawing.Size(857, 24);
            this.ddlTipoInscricao.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(42, 502);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(212, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "Informe o tipo de inscrição:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(42, 569);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 20);
            this.label6.TabIndex = 17;
            this.label6.Text = "Valor:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(242, 569);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 20);
            this.label7.TabIndex = 18;
            this.label7.Text = "Forma Pagamento";
            // 
            // edtValor
            // 
            this.edtValor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtValor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtValor.Font = new System.Drawing.Font("Verdana", 10.8F);
            this.edtValor.Location = new System.Drawing.Point(46, 593);
            this.edtValor.Margin = new System.Windows.Forms.Padding(4);
            this.edtValor.MaxLength = 100;
            this.edtValor.Name = "edtValor";
            this.edtValor.Size = new System.Drawing.Size(174, 29);
            this.edtValor.TabIndex = 19;
            // 
            // edtFormaPagamento
            // 
            this.edtFormaPagamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.edtFormaPagamento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edtFormaPagamento.Font = new System.Drawing.Font("Verdana", 10.8F);
            this.edtFormaPagamento.Location = new System.Drawing.Point(246, 593);
            this.edtFormaPagamento.Margin = new System.Windows.Forms.Padding(4);
            this.edtFormaPagamento.MaxLength = 100;
            this.edtFormaPagamento.Name = "edtFormaPagamento";
            this.edtFormaPagamento.Size = new System.Drawing.Size(288, 29);
            this.edtFormaPagamento.TabIndex = 20;
            // 
            // Clt_InsertParticipante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 726);
            this.Controls.Add(this.edtFormaPagamento);
            this.Controls.Add(this.edtValor);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ddlTipoInscricao);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.gdwInscricao);
            this.Controls.Add(this.btoValidar);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.edtConsultora);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Clt_InsertParticipante";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Incluir novo participante";
            this.Load += new System.EventHandler(this.Clt_InsertParticipante_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gdwInscricao)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btoCancelar;
        private System.Windows.Forms.Button btAcessar;
        private System.Windows.Forms.TextBox edtConsultora;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btoValidar;
        private System.Windows.Forms.DataGridView gdwInscricao;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CONSULTORA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipodeInscricao;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ddlTipoInscricao;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label lblNSD;
        public System.Windows.Forms.Label lblCPF;
        public System.Windows.Forms.Label lblNomeCompleto;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox edtValor;
        private System.Windows.Forms.TextBox edtFormaPagamento;
    }
}