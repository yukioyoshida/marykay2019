﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_Credencial : Form
    {
        string AP_Catetoria = "";
        string AP_Produto = "";

        public bool AP_Congresso { get; set; }
        public bool AP_Curso1 { get; set; }
        public bool AP_Curso2 { get; set; }
        public bool AP_Curso3 { get; set; }
        public string AP_Feira { get; set; }

        public void setPRODUTO(string prstProduto)
        {
            AP_Produto = prstProduto;
        }
        public string getProduto()
        {
            return AP_Produto;
        }

        public void setCATEGORIA(string prstCatetoria)
        {
            AP_Catetoria = prstCatetoria;
        }

        public string getCategoria()
        {
            return AP_Catetoria;
        }

        public CLT_Credencial()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
        }

        private void MI_ExibirCredencial()
        {
            if (AP_Feira == "AVESUI")
                imgCredencial.ImageLocation = "ImagemCredencial\\Visitante_AveSui.jpg";
            else if (AP_Feira == "BIOMASSA")
                imgCredencial.ImageLocation = "ImagemCredencial\\Visitante_BioMassa.jpg";
        }

        private void CLT_Credencial_Load(object sender, EventArgs e)
        {
            MI_ExibirCredencial();
            //MI_ExibirPulseiras();
            //MI_ExibeCategoria();
            //switch (getCategoria())
            //{
            //    case "VISITANTE":
            //        imgCredencial.ImageLocation = "Visitante.JPG";
            //        MI_ExibirPulseira(getProduto());
            //        break;
            //    case "EXPOSITOR":
            //        imgCredencial.ImageLocation = "Expositor.JPG";
            //        MI_ExibirPulseira(getProduto());
            //        break;
            //    case "PALESTRANTE":
            //        imgCredencial.ImageLocation = "Palestrante.JPG";
            //        MI_ExibirPulseira(getProduto());
            //        break;
            //    case "CONGRESSISTA":
            //        imgCredencial.ImageLocation = "Congressista.JPG";
            //        MI_ExibirPulseira(getProduto());
            //        break;
            //    case "SERVIÇOS":
            //        imgCredencial.ImageLocation = "Serviços.JPG";
            //        MI_ExibirPulseira(getProduto());
            //        break;
            //    case "MONTAGEM":
            //        imgCredencial.ImageLocation = "Montagem.JPG";
            //        MI_ExibirPulseira(getProduto());
            //        break;
            //    case "ORGANIZAÇÃO":
            //        imgCredencial.ImageLocation = "Organização.JPG";
            //        MI_ExibirPulseira(getProduto());
            //        break;
            //    case "IMPRENSA":
            //        imgCredencial.ImageLocation = "Imprensa.JPG";
            //        MI_ExibirPulseira(getProduto());
            //        break;
            //    case "STAFF":
            //        imgCredencial.ImageLocation = "Staff.JPG";
            //        MI_ExibirPulseira(getProduto());
            //        break;
            //}
        }

        private void MI_ExibirPulseira(string stProduto)
        {   
            if(stProduto.Contains("AVES"))
            {
                lblAmarelo.Visible = true;
            }
            if(stProduto.Contains("SUINOS"))
            {
                lblVerde.Visible = true;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void MI_ExibirPulseiras()
        {

            //if (AP_Curso1 == true)
            //    imgCurso1.Visible = true;

            if (AP_Curso2 == true)
                lblAmarelo.Visible = true;

            if (AP_Curso3 == true)
                lblVerde.Visible = true;

            //if (AP_Curso4 == true)
            //    imgCurso4.Visible = true;

            //if (AP_Curso5 == true)
            //    imgCurso5.Visible = true;

            //if (AP_Curso6 == true)
            //    imgCurso6.Visible = true;

            //if (AP_Curso7 == true)
            //    imgCurso7.Visible = true;
        }

        public void MI_ExibeCategoria()
        {
            if (AP_Congresso == true)
                imgCredencial.ImageLocation = "Congressista.JPG";
            else
                imgCredencial.ImageLocation = "Visitante.JPG";
        }
    }
}
