﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_ItemImpressao : Form
    {
        public CLT_ItemImpressao()
        {
            InitializeComponent();
        }

        public string AP_Data { get; set; }

        private void btDia4_Click(object sender, EventArgs e)
        {
            AP_Data = "2012-04-04";
            this.Close();
        }

        private void btDia5_Click(object sender, EventArgs e)
        {
            AP_Data = "2012-04-05";
            this.Close();
        }

        private void btDia6_Click(object sender, EventArgs e)
        {
            AP_Data = "2012-04-06";
            this.Close();
        }

        private void CLT_ItemImpressao_Load(object sender, EventArgs e)
        {

        }
    }
}
