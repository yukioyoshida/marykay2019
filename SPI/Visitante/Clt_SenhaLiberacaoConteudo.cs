﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class Clt_SenhaLiberacaoConteudo : Form
    {
        public bool AP_Result { get; set; }

        public Clt_SenhaLiberacaoConteudo()
        {
            InitializeComponent();
        }

        private void Clt_SenhaLiberacaoConteudo_Load(object sender, EventArgs e)
        {
            AP_Result = false;
        }

        private void btoCancelar_Click(object sender, EventArgs e)
        {
            AP_Result = false;
            this.Close();
        }

        private void MI_VerificarSenha(string stSenha)
        {
            CSV_VariaveisGlobal Global = new CSV_VariaveisGlobal();
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtSenha = IAT_Visitante.MS_ObterSenha(stSenha);

            if (dtSenha.Rows[0][0].ToString() == edtSenha.Text)
            {
                AP_Result = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Senha Inválida", "ATENÇÃO!");
                AP_Result = false;
                this.Close();
            }
        }

        private void btAcessar_Click(object sender, EventArgs e)
        {
            MI_VerificarSenha(edtSenha.Text.Trim());
        }
    }
}
