﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI.Visitante
{
    public partial class CLT_AlertaImprimir : Form
    {
        #region Constantes

        private const string MENSAGEM_LABEL = "Cole a etiqueta na Credencial";

        #endregion

        #region Propriedades

        public string Categoria { get; set; }

        #endregion

        public CLT_AlertaImprimir()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CarregaCorCategoria()
        {
            try
            {
                if (Categoria != null)
                {
                    if (Categoria.Equals("ROSA"))
                    {
                        panel1.BackColor = System.Drawing.Color.Pink;
                    }
                    else if (Categoria.Equals("VERDE"))
                    {
                        panel1.BackColor = System.Drawing.Color.Green;
                    }
                    else if (Categoria.Equals("AZUL"))
                    {
                        panel1.BackColor = System.Drawing.Color.Blue;
                    }
                    else if (Categoria.Equals("ROXO"))
                    {
                        panel1.BackColor = System.Drawing.Color.Purple;
                    }
                    else if (Categoria.Equals("AMARELA"))
                    {
                        panel1.BackColor = System.Drawing.Color.Yellow;
                    }
                    else if (Categoria.Equals("CINZA"))
                    {
                        panel1.BackColor = System.Drawing.Color.Gray;
                    }
                    else if (Categoria.Equals("LARANJA"))
                    {
                        panel1.BackColor = System.Drawing.Color.Orange;
                    }
                    if (Categoria.Equals("GOLD"))
                    {
                        panel1.BackColor = System.Drawing.Color.LightYellow;
                    }
                }
            }
            catch (Exception)
            {
                //string msg = "A categoria não foi iniciada, sem informação na propriedade.";
                //MessageBox.Show(msg);
                throw;
            }
        }

        private void CLT_AlertaImprimir_Load(object sender, EventArgs e)
        {
            this.CarregaCorCategoria();

            lblMensagem.Text = MENSAGEM_LABEL;

            lblCategoria.Text = Categoria;
        }
    }
}
