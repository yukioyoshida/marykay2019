namespace SPI
{
    partial class CLT_LocalizarRegistro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.edtPalavra = new System.Windows.Forms.TextBox();
            this.dgwRegistros = new System.Windows.Forms.DataGridView();
            this.chkContendo = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtConsultora = new System.Windows.Forms.RadioButton();
            this.rbtFantasia = new System.Windows.Forms.RadioButton();
            this.rbtCodigo = new System.Windows.Forms.RadioButton();
            this.rbtEmpresa = new System.Windows.Forms.RadioButton();
            this.rbtNome = new System.Windows.Forms.RadioButton();
            this.rbtCNPJ = new System.Windows.Forms.RadioButton();
            this.rbtCPF = new System.Windows.Forms.RadioButton();
            this.rbtCodigoBarras = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblDataHora = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblNivelAcesso = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tmrTempo = new System.Windows.Forms.Timer(this.components);
            this.lblSair = new System.Windows.Forms.Label();
            this.CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONSULTORA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipodeInscricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TIPO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMITIDO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgwRegistros)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label1.Location = new System.Drawing.Point(116, 230);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Palavra chave:";
            // 
            // edtPalavra
            // 
            this.edtPalavra.Location = new System.Drawing.Point(229, 226);
            this.edtPalavra.Margin = new System.Windows.Forms.Padding(4);
            this.edtPalavra.Name = "edtPalavra";
            this.edtPalavra.Size = new System.Drawing.Size(901, 22);
            this.edtPalavra.TabIndex = 1;
            this.edtPalavra.TextChanged += new System.EventHandler(this.edtPalavra_TextChanged);
            // 
            // dgwRegistros
            // 
            this.dgwRegistros.AllowUserToAddRows = false;
            this.dgwRegistros.AllowUserToDeleteRows = false;
            this.dgwRegistros.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dgwRegistros.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgwRegistros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwRegistros.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CODIGO,
            this.CONSULTORA,
            this.NOME,
            this.TipodeInscricao,
            this.TIPO,
            this.EMITIDO});
            this.dgwRegistros.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgwRegistros.Location = new System.Drawing.Point(121, 258);
            this.dgwRegistros.Margin = new System.Windows.Forms.Padding(4);
            this.dgwRegistros.MultiSelect = false;
            this.dgwRegistros.Name = "dgwRegistros";
            this.dgwRegistros.ReadOnly = true;
            this.dgwRegistros.Size = new System.Drawing.Size(1115, 334);
            this.dgwRegistros.TabIndex = 2;
            this.dgwRegistros.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgwRegistros_CellContentClick);
            this.dgwRegistros.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgwRegistros_KeyDown);
            // 
            // chkContendo
            // 
            this.chkContendo.AutoSize = true;
            this.chkContendo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.chkContendo.Location = new System.Drawing.Point(1140, 230);
            this.chkContendo.Margin = new System.Windows.Forms.Padding(4);
            this.chkContendo.Name = "chkContendo";
            this.chkContendo.Size = new System.Drawing.Size(91, 21);
            this.chkContendo.TabIndex = 4;
            this.chkContendo.Text = "Contendo";
            this.chkContendo.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox1.Controls.Add(this.rbtConsultora);
            this.groupBox1.Controls.Add(this.rbtFantasia);
            this.groupBox1.Controls.Add(this.rbtCodigo);
            this.groupBox1.Controls.Add(this.rbtEmpresa);
            this.groupBox1.Controls.Add(this.rbtNome);
            this.groupBox1.Controls.Add(this.rbtCNPJ);
            this.groupBox1.Controls.Add(this.rbtCPF);
            this.groupBox1.Controls.Add(this.rbtCodigoBarras);
            this.groupBox1.Location = new System.Drawing.Point(115, 156);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1121, 63);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "LOCALIZAR REGISTRO POR";
            // 
            // rbtConsultora
            // 
            this.rbtConsultora.AutoSize = true;
            this.rbtConsultora.Checked = true;
            this.rbtConsultora.Location = new System.Drawing.Point(17, 23);
            this.rbtConsultora.Margin = new System.Windows.Forms.Padding(4);
            this.rbtConsultora.Name = "rbtConsultora";
            this.rbtConsultora.Size = new System.Drawing.Size(120, 21);
            this.rbtConsultora.TabIndex = 7;
            this.rbtConsultora.TabStop = true;
            this.rbtConsultora.Text = "&Nr. Consultora";
            this.rbtConsultora.UseVisualStyleBackColor = true;
            this.rbtConsultora.CheckedChanged += new System.EventHandler(this.rbtLocalizese_CheckedChanged);
            // 
            // rbtFantasia
            // 
            this.rbtFantasia.AutoSize = true;
            this.rbtFantasia.Location = new System.Drawing.Point(831, 23);
            this.rbtFantasia.Margin = new System.Windows.Forms.Padding(4);
            this.rbtFantasia.Name = "rbtFantasia";
            this.rbtFantasia.Size = new System.Drawing.Size(124, 21);
            this.rbtFantasia.TabIndex = 6;
            this.rbtFantasia.Text = "Nome &Fantasia";
            this.rbtFantasia.UseVisualStyleBackColor = true;
            this.rbtFantasia.Visible = false;
            // 
            // rbtCodigo
            // 
            this.rbtCodigo.AutoSize = true;
            this.rbtCodigo.Location = new System.Drawing.Point(628, 23);
            this.rbtCodigo.Margin = new System.Windows.Forms.Padding(4);
            this.rbtCodigo.Name = "rbtCodigo";
            this.rbtCodigo.Size = new System.Drawing.Size(73, 21);
            this.rbtCodigo.TabIndex = 5;
            this.rbtCodigo.Text = "C�digo";
            this.rbtCodigo.UseVisualStyleBackColor = true;
            this.rbtCodigo.Visible = false;
            this.rbtCodigo.CheckedChanged += new System.EventHandler(this.rbtCodigo_CheckedChanged_1);
            // 
            // rbtEmpresa
            // 
            this.rbtEmpresa.AutoSize = true;
            this.rbtEmpresa.Location = new System.Drawing.Point(708, 23);
            this.rbtEmpresa.Margin = new System.Windows.Forms.Padding(4);
            this.rbtEmpresa.Name = "rbtEmpresa";
            this.rbtEmpresa.Size = new System.Drawing.Size(112, 21);
            this.rbtEmpresa.TabIndex = 4;
            this.rbtEmpresa.Text = "&Razao Social";
            this.rbtEmpresa.UseVisualStyleBackColor = true;
            this.rbtEmpresa.Visible = false;
            this.rbtEmpresa.CheckedChanged += new System.EventHandler(this.rbtEmpresa_CheckedChanged);
            // 
            // rbtNome
            // 
            this.rbtNome.AutoSize = true;
            this.rbtNome.Location = new System.Drawing.Point(216, 23);
            this.rbtNome.Margin = new System.Windows.Forms.Padding(4);
            this.rbtNome.Name = "rbtNome";
            this.rbtNome.Size = new System.Drawing.Size(66, 21);
            this.rbtNome.TabIndex = 3;
            this.rbtNome.Text = "&Nome";
            this.rbtNome.UseVisualStyleBackColor = true;
            this.rbtNome.CheckedChanged += new System.EventHandler(this.rbtNome_CheckedChanged);
            // 
            // rbtCNPJ
            // 
            this.rbtCNPJ.AutoSize = true;
            this.rbtCNPJ.Location = new System.Drawing.Point(553, 23);
            this.rbtCNPJ.Margin = new System.Windows.Forms.Padding(4);
            this.rbtCNPJ.Name = "rbtCNPJ";
            this.rbtCNPJ.Size = new System.Drawing.Size(64, 21);
            this.rbtCNPJ.TabIndex = 2;
            this.rbtCNPJ.Text = "CNP&J";
            this.rbtCNPJ.UseVisualStyleBackColor = true;
            this.rbtCNPJ.Visible = false;
            this.rbtCNPJ.CheckedChanged += new System.EventHandler(this.rbtCNPJ_CheckedChanged);
            // 
            // rbtCPF
            // 
            this.rbtCPF.AutoSize = true;
            this.rbtCPF.Location = new System.Drawing.Point(147, 23);
            this.rbtCPF.Margin = new System.Windows.Forms.Padding(4);
            this.rbtCPF.Name = "rbtCPF";
            this.rbtCPF.Size = new System.Drawing.Size(55, 21);
            this.rbtCPF.TabIndex = 1;
            this.rbtCPF.Text = "C&PF";
            this.rbtCPF.UseVisualStyleBackColor = true;
            this.rbtCPF.CheckedChanged += new System.EventHandler(this.rbtCPF_CheckedChanged);
            // 
            // rbtCodigoBarras
            // 
            this.rbtCodigoBarras.AutoSize = true;
            this.rbtCodigoBarras.Location = new System.Drawing.Point(972, 23);
            this.rbtCodigoBarras.Margin = new System.Windows.Forms.Padding(4);
            this.rbtCodigoBarras.Name = "rbtCodigoBarras";
            this.rbtCodigoBarras.Size = new System.Drawing.Size(139, 21);
            this.rbtCodigoBarras.TabIndex = 0;
            this.rbtCodigoBarras.Text = "&C�digo de Barras";
            this.rbtCodigoBarras.UseVisualStyleBackColor = true;
            this.rbtCodigoBarras.Visible = false;
            this.rbtCodigoBarras.CheckedChanged += new System.EventHandler(this.rbtCodigoBarras_CheckedChanged_1);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Lavender;
            this.label10.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label10.Location = new System.Drawing.Point(571, 118);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(222, 20);
            this.label10.TabIndex = 38;
            this.label10.Text = "LOCALIZAR REGISTRO";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Lavender;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Location = new System.Drawing.Point(101, 107);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(1150, 41);
            this.pictureBox3.TabIndex = 39;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(101, 148);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1150, 457);
            this.pictureBox1.TabIndex = 37;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Location = new System.Drawing.Point(105, 86);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox7.Size = new System.Drawing.Size(1151, 10);
            this.groupBox7.TabIndex = 64;
            this.groupBox7.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.lblTerminal);
            this.groupBox5.Location = new System.Drawing.Point(911, 22);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(131, 57);
            this.groupBox5.TabIndex = 63;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "TERMINAL";
            // 
            // lblTerminal
            // 
            this.lblTerminal.AutoSize = true;
            this.lblTerminal.BackColor = System.Drawing.Color.Transparent;
            this.lblTerminal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTerminal.ForeColor = System.Drawing.Color.Yellow;
            this.lblTerminal.Location = new System.Drawing.Point(29, 22);
            this.lblTerminal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(0, 24);
            this.lblTerminal.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.lblDataHora);
            this.groupBox2.Location = new System.Drawing.Point(1053, 22);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(203, 57);
            this.groupBox2.TabIndex = 62;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "DATA - HORA";
            // 
            // lblDataHora
            // 
            this.lblDataHora.AutoSize = true;
            this.lblDataHora.BackColor = System.Drawing.Color.Transparent;
            this.lblDataHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataHora.ForeColor = System.Drawing.Color.Yellow;
            this.lblDataHora.Location = new System.Drawing.Point(8, 22);
            this.lblDataHora.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDataHora.Name = "lblDataHora";
            this.lblDataHora.Size = new System.Drawing.Size(0, 24);
            this.lblDataHora.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.lblNivelAcesso);
            this.groupBox3.Controls.Add(this.lblUsuario);
            this.groupBox3.Location = new System.Drawing.Point(105, 22);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(791, 57);
            this.groupBox3.TabIndex = 61;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "OPERADOR";
            // 
            // lblNivelAcesso
            // 
            this.lblNivelAcesso.AutoSize = true;
            this.lblNivelAcesso.Location = new System.Drawing.Point(941, 25);
            this.lblNivelAcesso.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNivelAcesso.Name = "lblNivelAcesso";
            this.lblNivelAcesso.Size = new System.Drawing.Size(0, 17);
            this.lblNivelAcesso.TabIndex = 1;
            this.lblNivelAcesso.Visible = false;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.BackColor = System.Drawing.Color.Transparent;
            this.lblUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.ForeColor = System.Drawing.Color.Yellow;
            this.lblUsuario.Location = new System.Drawing.Point(23, 22);
            this.lblUsuario.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(0, 24);
            this.lblUsuario.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Location = new System.Drawing.Point(101, 613);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(1151, 10);
            this.groupBox4.TabIndex = 65;
            this.groupBox4.TabStop = false;
            // 
            // tmrTempo
            // 
            this.tmrTempo.Enabled = true;
            this.tmrTempo.Interval = 6000;
            this.tmrTempo.Tick += new System.EventHandler(this.tmrTempo_Tick);
            // 
            // lblSair
            // 
            this.lblSair.AutoSize = true;
            this.lblSair.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSair.ForeColor = System.Drawing.Color.White;
            this.lblSair.Location = new System.Drawing.Point(97, 633);
            this.lblSair.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSair.Name = "lblSair";
            this.lblSair.Size = new System.Drawing.Size(156, 17);
            this.lblSair.TabIndex = 66;
            this.lblSair.Text = "Para sair tecle (ESC).";
            // 
            // CODIGO
            // 
            this.CODIGO.DataPropertyName = "CODIGO";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CODIGO.DefaultCellStyle = dataGridViewCellStyle1;
            this.CODIGO.HeaderText = "CODIGO";
            this.CODIGO.Name = "CODIGO";
            this.CODIGO.ReadOnly = true;
            this.CODIGO.Visible = false;
            // 
            // CONSULTORA
            // 
            this.CONSULTORA.DataPropertyName = "CONSULTORA";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CONSULTORA.DefaultCellStyle = dataGridViewCellStyle2;
            this.CONSULTORA.HeaderText = "COD.";
            this.CONSULTORA.Name = "CONSULTORA";
            this.CONSULTORA.ReadOnly = true;
            this.CONSULTORA.Width = 50;
            // 
            // NOME
            // 
            this.NOME.DataPropertyName = "NOME";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.NOME.DefaultCellStyle = dataGridViewCellStyle3;
            this.NOME.HeaderText = "NOME";
            this.NOME.Name = "NOME";
            this.NOME.ReadOnly = true;
            this.NOME.Width = 270;
            // 
            // TipodeInscricao
            // 
            this.TipodeInscricao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TipodeInscricao.DataPropertyName = "TipodeInscricao";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TipodeInscricao.DefaultCellStyle = dataGridViewCellStyle4;
            this.TipodeInscricao.HeaderText = "INSCRICAO";
            this.TipodeInscricao.Name = "TipodeInscricao";
            this.TipodeInscricao.ReadOnly = true;
            // 
            // TIPO
            // 
            this.TIPO.DataPropertyName = "TIPO";
            this.TIPO.HeaderText = "TIPO";
            this.TIPO.Name = "TIPO";
            this.TIPO.ReadOnly = true;
            // 
            // EMITIDO
            // 
            this.EMITIDO.DataPropertyName = "EMITIDO";
            this.EMITIDO.HeaderText = "EMITIDO";
            this.EMITIDO.Name = "EMITIDO";
            this.EMITIDO.ReadOnly = true;
            this.EMITIDO.Width = 60;
            // 
            // CLT_LocalizarRegistro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(1361, 900);
            this.Controls.Add(this.lblSair);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.dgwRegistros);
            this.Controls.Add(this.chkContendo);
            this.Controls.Add(this.edtPalavra);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CLT_LocalizarRegistro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Localizar Registro";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CLT_LocalizarRegistro_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CLT_LocalizarRegistro_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.dgwRegistros)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edtPalavra;
        private System.Windows.Forms.DataGridView dgwRegistros;
        private System.Windows.Forms.CheckBox chkContendo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbtEmpresa;
        private System.Windows.Forms.RadioButton rbtNome;
        private System.Windows.Forms.RadioButton rbtCNPJ;
        private System.Windows.Forms.RadioButton rbtCPF;
        private System.Windows.Forms.RadioButton rbtCodigoBarras;
        private System.Windows.Forms.RadioButton rbtFantasia;
        private System.Windows.Forms.RadioButton rbtCodigo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblTerminal;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblDataHora;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblNivelAcesso;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Timer tmrTempo;
        private System.Windows.Forms.Label lblSair;
        private System.Windows.Forms.RadioButton rbtConsultora;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CONSULTORA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipodeInscricao;
        private System.Windows.Forms.DataGridViewTextBoxColumn TIPO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMITIDO;
    }
}