using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CEPGeralImportado; 

namespace SPI
{
    public partial class CLT_LocalizarEndereco : Form
    {
        DataTable dtTodosRegistros;
        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        public bool boResult = false;
        public string stCEP = string.Empty;
        public string stBairro = string.Empty;
        public string stTipoEndereco = string.Empty;
        public string stLogradouro = string.Empty;
        public string stCidade = string.Empty;
        public string stUF = string.Empty;
        public string stDDD = string.Empty;
        public string stNumero = string.Empty;

        public CLT_LocalizarEndereco()
        {
            InitializeComponent();
        }

        private void CLT_LocalizarEndereco_Load(object sender, EventArgs e)
        {
            CarregaUsuario();
            MI_CarregaEstados();
        }

        private void CarregaUsuario()
        {
            lblUsuario.Text = CSV_Global.getUsuario();
            lblTerminal.Text = CSV_Global.getNumeroTerminal();
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void MI_CarregaEstados()
        {
            DataTable dtEstados = new DataTable();
            DataRow drLinha;

            CAT_CadastrarCEPImportado IAT_CEPImportado = new CAT_CadastrarCEPImportado();

            dtEstados = IAT_CEPImportado.ObterEstados();

            for (int i = 0; i <= dtEstados.Rows.Count - 1; i++)
            {
                drLinha = dtEstados.Rows[i];
                cmbEstado.Items.Add(drLinha.ItemArray[0].ToString());  
            }
        }

        private void MI_CarregaCidades(string stEstado)
        {
            DataTable dtCidades = new DataTable();
            DataRow drLinha;

            cmbCidade.Items.Clear();

            CAT_CadastrarCEPImportado IAT_CEPImportado = new CAT_CadastrarCEPImportado();

            dtCidades = IAT_CEPImportado.ObterCidades(stEstado);

            for (int i = 0; i <= dtCidades.Rows.Count - 1; i++)
            {
                drLinha = dtCidades.Rows[i];
                cmbCidade.Items.Add(drLinha.ItemArray[0].ToString());
            }

            cmbCidade.Text = "";
        }

        private void cmbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            MI_CarregaCidades(cmbEstado.Text); 
        }

        private void CLT_LocalizarEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            int itTecla = e.KeyChar;
            switch (itTecla.ToString())
            {
                case "13":
                    e.Handled = true;
                    SendKeys.Send("{tab}");
                    break;
                case "27":
                    this.Close();
                    break;
            }
        }

        private void edtRua_TextChanged(object sender, EventArgs e)
        {
            MI_ObterEndereco();
        }

        private void MI_ObterEndereco()
        {
            if ((edtRua.Text.Trim() != null) && (edtRua.Text.Length == 4))
            {
                MI_LocalizarRegistro(edtRua.Text, cmbEstado.Text, cmbCidade.Text);
            }
            else
            {
                try
                {
                    if (edtRua.Text.Length >= 4 && dtTodosRegistros.Rows.Count != 0)
                        MI_FFF(edtRua.Text);
                }
                catch
                {
                    MI_LocalizarRegistro(edtRua.Text, cmbEstado.Text, cmbCidade.Text);
                }
            }
        }

        private void MI_LocalizarRegistro(string stRua, string stEstado, string stCidade)
        {
            DataTable dtRegistros = new DataTable();
            CAT_CadastrarCEPImportado IAT_CadastrarCEPImportado = new CAT_CadastrarCEPImportado();

            dtRegistros = IAT_CadastrarCEPImportado.MS_ObterEndereco(stRua, stEstado, stCidade);

            dtRegistros = MI_ObterNovaTabela(dtRegistros);

            gdwResultado.DataSource = dtRegistros;
            gdwResultado.Columns[0].Width = 70;
            gdwResultado.Columns[2].Width = 210;
            gdwResultado.Columns[3].Width = 150;
            gdwResultado.Columns[4].Width = 30;
            gdwResultado.Columns[5].Width = 33;
            gdwResultado.Columns[6].Visible = false;
            gdwResultado.Columns[7].Visible = false;
            gdwResultado.Columns[8].Visible = false;
            gdwResultado.Columns[9].Visible = false;
            gdwResultado.Columns[10].Visible = false;

            dtTodosRegistros = dtRegistros;

            if (edtNumero.Text.Trim() != string.Empty)
                MI_RealizaFiltroParidade(edtNumero.Text);
        }

        private DataTable MI_ObterNovaTabela(DataTable dtRegistros)
        {
            DataTable dtPreparo = new DataTable();
            DataRow drLinha;
            DataRow drLinhaInsert;

            dtPreparo.Columns.Add("CEP", typeof(string));
            dtPreparo.Columns.Add("BAIRRO", typeof(string));
            dtPreparo.Columns.Add("LOGRADOURO", typeof(string));
            dtPreparo.Columns.Add("CIDADE", typeof(string));
            dtPreparo.Columns.Add("UF", typeof(string));
            dtPreparo.Columns.Add("DDD", typeof(string));
            dtPreparo.Columns.Add("TIPO", typeof(string));
            dtPreparo.Columns.Add("NUMEROINI", typeof(string));
            dtPreparo.Columns.Add("NUMEROFIM", typeof(string));
            dtPreparo.Columns.Add("PARIDADE", typeof(string));
            dtPreparo.Columns.Add("LOGRADOUROORIGINAL", typeof(string));

            for (int i = 0; i <= dtRegistros.Rows.Count - 1; i++)
            {
                drLinha = dtRegistros.Rows[i];

                drLinhaInsert = dtPreparo.NewRow();
                    drLinhaInsert["CEP"] = drLinha["CEP"].ToString();
                    drLinhaInsert["BAIRRO"] = drLinha["BAIRRO"].ToString();
                    drLinhaInsert["LOGRADOURO"] = drLinha["TIPO"].ToString() + " " + drLinha["LOGRADOURO"].ToString();
                    drLinhaInsert["CIDADE"] = drLinha["CIDADE"].ToString();
                    drLinhaInsert["UF"] = drLinha["UF"].ToString();
                    drLinhaInsert["DDD"] = drLinha["DDD"].ToString();
                    drLinhaInsert["TIPO"] = drLinha["TIPO"].ToString();
                    drLinhaInsert["NUMEROINI"] = drLinha["NUMEROINI"].ToString();
                    drLinhaInsert["NUMEROFIM"] = drLinha["NUMEROFIM"].ToString();
                    drLinhaInsert["PARIDADE"] = drLinha["PARIDADE"].ToString();
                    drLinhaInsert["LOGRADOUROORIGINAL"] = drLinha["LOGRADOURO"].ToString();
                drLinhaInsert.EndEdit();

                dtPreparo.Rows.Add(drLinhaInsert[0], drLinhaInsert[1], drLinhaInsert[2], drLinhaInsert[3], drLinhaInsert[4], drLinhaInsert[5], drLinhaInsert[6], drLinhaInsert[7], drLinhaInsert[8], drLinhaInsert[9], drLinhaInsert[10]); 
            }

            return dtPreparo;
        }

        private void MI_FFF(string prPalavra)
        {
            DataView dv = new DataView(dtTodosRegistros.Copy());

            dv.RowFilter = "LOGRADOURO Like '%" + prPalavra + "%'";

            gdwResultado.DataSource = dv.ToTable();
            gdwResultado.Columns[0].Width = 70;
            gdwResultado.Columns[2].Width = 210;
            gdwResultado.Columns[3].Width = 150;
            gdwResultado.Columns[4].Width = 30;
            gdwResultado.Columns[5].Width = 33;
            gdwResultado.Columns[6].Visible = false;
            gdwResultado.Columns[7].Visible = false;
            gdwResultado.Columns[8].Visible = false;
            gdwResultado.Columns[9].Visible = false;
            gdwResultado.Columns[10].Visible = false;

            if (edtNumero.Text.Trim() != string.Empty)
                MI_RealizaFiltroParidade(edtNumero.Text);
        }

        private void MI_RealizaFiltroParidade(string stNumero)
        {
            string stParidade = string.Empty;

            //Verifica se o n�mero � PAR OU IMPAR
            try
            {
                if (Convert.ToInt32(stNumero) % 2 != 0)
                    stParidade = "I";
                else
                    stParidade = "P";
            }
            catch
            {
                return;
            }
            //===================================

            //REALIZA LIMPEZA INICIAL
            int i = gdwResultado.Rows.Count - 1;
            
            while (i >= 0)
            {
                if ((gdwResultado.Rows[i].Cells[9].Value.ToString() != "A") && (gdwResultado.Rows[i].Cells[9].Value.ToString() != string.Empty))
                {
                    if (gdwResultado.Rows[i].Cells[9].Value.ToString() != stParidade)
                    {
                          gdwResultado.Rows.Remove(gdwResultado.Rows[i]);
                    }
                }
                i = i - 1;
            }

            i = gdwResultado.Rows.Count - 1;

            while (i >= 0)
            {
                if (gdwResultado.Rows[i].Cells[9].Value.ToString() != string.Empty)
                {
                    try
                    {
                        if ((Convert.ToInt32(stNumero) >= Convert.ToInt32(gdwResultado.Rows[i].Cells[7].Value.ToString())) && (Convert.ToInt32(stNumero) <= Convert.ToInt32(gdwResultado.Rows[i].Cells[8].Value.ToString())))
                        { }
                        else
                        {
                            gdwResultado.Rows.Remove(gdwResultado.Rows[i]);
                        }
                    }
                    catch
                    {
                    }
                }
                i = i - 1;
            }
            
        }

        private void gdwResultado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            MI_ObterDados();
        }

        private void MI_ObterDados()
        {
            boResult = true;
            stCEP = gdwResultado.CurrentRow.Cells[0].Value.ToString();
            stBairro = gdwResultado.CurrentRow.Cells[1].Value.ToString();
            stLogradouro = gdwResultado.CurrentRow.Cells[10].Value.ToString();
            stCidade = gdwResultado.CurrentRow.Cells[3].Value.ToString();
            stUF = gdwResultado.CurrentRow.Cells[4].Value.ToString();
            stDDD = gdwResultado.CurrentRow.Cells[5].Value.ToString();
            stTipoEndereco = gdwResultado.CurrentRow.Cells[6].Value.ToString();
            stNumero = edtNumero.Text.Trim();
            Close();
        }

        private void edtNumero_TextChanged(object sender, EventArgs e)
        {
            if (edtNumero.Text.Trim() != string.Empty)
                MI_ObterEndereco();
            else
                MI_ObterEndereco();
        }

        private void gdwResultado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MI_ObterDados();
            }
        }

        private void cmbEstado_KeyPress(object sender, KeyPressEventArgs e)
        {
            MI_CarregaCidades(cmbEstado.Text); 
        }

        private void cmbEstado_DragEnter(object sender, DragEventArgs e)
        {
            MessageBox.Show("TESTE","");
        }

        private void tmrTempo_Tick(object sender, EventArgs e)
        {
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }
    }
}