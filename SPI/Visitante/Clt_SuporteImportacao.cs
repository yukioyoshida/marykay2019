﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI.Visitante
{
    public partial class Clt_SuporteImportacao : Form
    {
        public Clt_SuporteImportacao()
        {
            InitializeComponent();
        }

        private void MI_InserirItem(string prCodigoBarras, string prColuna)
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            //CTO_Visitante ITO_Visitante = IAT_Visitante.MS_ObterPorCodigo(prCodigoBarras);

            DataTable dtVisitante = IAT_Visitante.MS_ObterPorCodigoData(prCodigoBarras);

            CAT_CadastrarPedido IAT_Pedido = new CAT_CadastrarPedido();
            CTO_Pedido ITO_Pedido = new CTO_Pedido();

            ITO_Pedido.setcdPedido(MI_GerarNovoPedido());
            ITO_Pedido.setcdCliente(Convert.ToInt32(dtVisitante.Rows[0]["ObjRef"]));

            ITO_Pedido.setdsSituacao("PAGO");

            ITO_Pedido.setdsTipoPessoa(string.Empty);

            //INICIO Armazena produtos comprados no pedido

            ITO_Pedido.setdsNomeFaturamento(dtVisitante.Rows[0]["NomeCompleto"].ToString());

            //Fim Armazena produtos comprados no pedido

            ITO_Pedido.setdsValorTotal("0,00");
            ITO_Pedido.setdsValorDesconto("0,00");
            ITO_Pedido.setdtPedido(System.DateTime.Now);
            ITO_Pedido.setdsFormaPagamento("GRATUITO");

            ITO_Pedido.setdtVencimento(System.DateTime.Now);
            ITO_Pedido.setdsObservacao("");

            //---------Insere o pedido-----------
            IAT_Pedido.MS_Incluir(ITO_Pedido);

            // Insere o Item

            // ObterCodigoReal

            DataTable dtCodigoReal = IAT_Pedido.MS_ObterCodigoProdutoReal(prColuna);

            CAT_CadastrarItensPedido IAT_Item = new CAT_CadastrarItensPedido();
            CTO_ItensPedido ITO_Item = new CTO_ItensPedido();
            CTO_Produto ITO_Produto = new CTO_Produto();
            CAT_CadastrarCodigoPagamento IAT_CodigoPagamento = new CAT_CadastrarCodigoPagamento();
            CTO_CodigoPagamento ITO_CodigoPagamento = new CTO_CodigoPagamento();

            //---------Cadastra o código de pagamento---------

            ITO_CodigoPagamento.setitCodigo(MI_GerarCodigoPagamento(ITO_Pedido.getcdPedido().ToString()));
            ITO_CodigoPagamento.setCodigoBarras(ITO_Pedido.getcdPedido().ToString());
            IAT_CodigoPagamento.MS_Incluir(ITO_CodigoPagamento);

            ITO_Item.setcdPedido(ITO_Pedido.getcdPedido());
            ITO_Item.setcdProduto(dtCodigoReal.Rows[0]["CodigoReal"].ToString());
            ITO_Item.setdsValor("0,00");
            IAT_Item.MS_Incluir(ITO_Item);
        }

        public string MI_GerarCodigoPagamento(string stPedido)
        {
            string stPagamento = string.Empty;

            DateTime dtHora = System.DateTime.Now;

            string stHora;

            stHora = dtHora.Millisecond.ToString();

            //stPagamento = "AVS" + stPedido + stHora;
            stPagamento = "2012" + stPedido;

            return stPagamento;
        }

        private int MI_GerarNovoPedido()
        {
            CAT_CadastrarPedido IAT_Pedido = new CAT_CadastrarPedido();
            int idPedido = IAT_Pedido.MS_NovoObjRef();

            return idPedido;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i <= 15000; i++)
            {
                CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
                DataTable dtRegistro = IAT_Visitante.MS_ObterProximoImportacao();

                if (dtRegistro.Rows.Count == 0)
                {
                    MessageBox.Show("Operação finalizada!");
                    break;
                }
                else
                {
                    #region AZUL E BRANCO
                    //if (dtRegistro.Rows[0]["AJ"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AJ");

                    //if (dtRegistro.Rows[0]["AK"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AK");

                    //if (dtRegistro.Rows[0]["AL"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AL");

                    //if (dtRegistro.Rows[0]["AM"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AM");

                    //if (dtRegistro.Rows[0]["AN"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AN");

                    //if (dtRegistro.Rows[0]["AO"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AO");

                    //if (dtRegistro.Rows[0]["AP"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AP");

                    //if (dtRegistro.Rows[0]["AQ"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AQ");

                    //if (dtRegistro.Rows[0]["AR"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AR");

                    //if (dtRegistro.Rows[0]["AS"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AS");

                    //if (dtRegistro.Rows[0]["AT"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AT");

                    //if (dtRegistro.Rows[0]["AU"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AU");

                    //if (dtRegistro.Rows[0]["AV"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AV");

                    //if (dtRegistro.Rows[0]["AW"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AW");

                    //if (dtRegistro.Rows[0]["AX"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AX");

                    //if (dtRegistro.Rows[0]["AY"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AY");

                    //if (dtRegistro.Rows[0]["AZ"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AZ");

                    //if (dtRegistro.Rows[0]["BA"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BA");

                    //if (dtRegistro.Rows[0]["BB"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BB");

                    //if (dtRegistro.Rows[0]["BC"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BC");

                    //if (dtRegistro.Rows[0]["BD"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BD");

                    //if (dtRegistro.Rows[0]["BE"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BE");

                    //if (dtRegistro.Rows[0]["BF"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BF");

                    //if (dtRegistro.Rows[0]["BG"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BG");

                    //if (dtRegistro.Rows[0]["BH"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BH");

                    //if (dtRegistro.Rows[0]["BI"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BI");

                    //if (dtRegistro.Rows[0]["BJ"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BJ");

                    //if (dtRegistro.Rows[0]["BK"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BK");

                    //if (dtRegistro.Rows[0]["BL"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BL");

                    //if (dtRegistro.Rows[0]["BM"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BM");

                    //if (dtRegistro.Rows[0]["BN"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BN");

                    //if (dtRegistro.Rows[0]["BO"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BO");

                    //if (dtRegistro.Rows[0]["BP"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BP");

                    //if (dtRegistro.Rows[0]["BQ"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BQ");

                    //if (dtRegistro.Rows[0]["BR"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BR");

                    //if (dtRegistro.Rows[0]["BS"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BS");

                    //if (dtRegistro.Rows[0]["BT"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BT");

                    //if (dtRegistro.Rows[0]["BU"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BU");

                    //if (dtRegistro.Rows[0]["BV"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BV");

                    //if (dtRegistro.Rows[0]["BW"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BW");

                    //if (dtRegistro.Rows[0]["BX"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BX");

                    //if (dtRegistro.Rows[0]["BY"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BY");

                    //if (dtRegistro.Rows[0]["BZ"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BZ");

                    //if (dtRegistro.Rows[0]["CA"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CA");

                    //if (dtRegistro.Rows[0]["CB"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CB");

                    //if (dtRegistro.Rows[0]["CC"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CC");

                    //if (dtRegistro.Rows[0]["CD"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CD");

                    //if (dtRegistro.Rows[0]["CE"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CE");

                    //if (dtRegistro.Rows[0]["CF"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CF");

                    //if (dtRegistro.Rows[0]["CG"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CG");

                    //if (dtRegistro.Rows[0]["CH"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CH");

                    //if (dtRegistro.Rows[0]["CI"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CI");

                    //if (dtRegistro.Rows[0]["CJ"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CJ");

                    //if (dtRegistro.Rows[0]["CK"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CK");

                    //if (dtRegistro.Rows[0]["CL"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CL");

                    //if (dtRegistro.Rows[0]["CM"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CM");

                    //if (dtRegistro.Rows[0]["CN"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CN");

                    //if (dtRegistro.Rows[0]["CO"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CO");

                    //if (dtRegistro.Rows[0]["CP"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CP");

                    //if (dtRegistro.Rows[0]["CQ"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CQ");

                    //if (dtRegistro.Rows[0]["CR"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CR");

                    //if (dtRegistro.Rows[0]["CS"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CS");

                    //if (dtRegistro.Rows[0]["CT"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CT");

                    //if (dtRegistro.Rows[0]["CU"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CU");

                    //if (dtRegistro.Rows[0]["CV"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CV");

                    //if (dtRegistro.Rows[0]["CW"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CW");

                    //if (dtRegistro.Rows[0]["CX"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CX");

                    //if (dtRegistro.Rows[0]["CY"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CY");

                    //if (dtRegistro.Rows[0]["CZ"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CZ");

                    //if (dtRegistro.Rows[0]["DA"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DA");

                    //if (dtRegistro.Rows[0]["DB"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DB");

                    //if (dtRegistro.Rows[0]["DC"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DC");

                    //if (dtRegistro.Rows[0]["DD"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DD");

                    //if (dtRegistro.Rows[0]["DE"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DE");

                    //if (dtRegistro.Rows[0]["DF"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DF");

                    //if (dtRegistro.Rows[0]["DG"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DG");

                    //if (dtRegistro.Rows[0]["DH"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DH");

                    //if (dtRegistro.Rows[0]["DI"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DI");

                    //if (dtRegistro.Rows[0]["DJ"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DJ");

                    //if (dtRegistro.Rows[0]["DK"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DK");

                    //if (dtRegistro.Rows[0]["DL"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DL");

                    //if (dtRegistro.Rows[0]["DM"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DM");

                    //if (dtRegistro.Rows[0]["DN"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DN");

                    //if (dtRegistro.Rows[0]["DO"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DO");

                    //if (dtRegistro.Rows[0]["DP"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DP");

                    //if (dtRegistro.Rows[0]["DQ"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DQ");

                    //if (dtRegistro.Rows[0]["DR"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DR");

                    //if (dtRegistro.Rows[0]["DS"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DS");

                    //if (dtRegistro.Rows[0]["DY"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DY");

                    //if (dtRegistro.Rows[0]["DZ"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DZ");

                    //if (dtRegistro.Rows[0]["EA"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EA");

                    //if (dtRegistro.Rows[0]["EB"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EB");

                    //if (dtRegistro.Rows[0]["EC"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EC");

                    //if (dtRegistro.Rows[0]["ED"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "ED");

                    //if (dtRegistro.Rows[0]["EE"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EE");

                    //if (dtRegistro.Rows[0]["EF"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EF");

                    //if (dtRegistro.Rows[0]["EG"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EG");

                    //if (dtRegistro.Rows[0]["EH"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EH");

                    //if (dtRegistro.Rows[0]["EK"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EK");

                    //if (dtRegistro.Rows[0]["EL"].ToString().Trim().ToUpper() == "X")
                    //    MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EL");
                    #endregion

                    #region VERMELHO E AMARELO

                    if (dtRegistro.Rows[0]["AJ"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AJ");

                    if (dtRegistro.Rows[0]["AK"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AK");

                    if (dtRegistro.Rows[0]["AL"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AL");

                    if (dtRegistro.Rows[0]["AM"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AM");

                    if (dtRegistro.Rows[0]["AN"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AN");

                    if (dtRegistro.Rows[0]["AO"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AO");

                    if (dtRegistro.Rows[0]["AU"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AU");

                    if (dtRegistro.Rows[0]["AV"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AV");

                    if (dtRegistro.Rows[0]["AW"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AW");

                    if (dtRegistro.Rows[0]["AX"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AX");

                    if (dtRegistro.Rows[0]["AY"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AY");

                    if (dtRegistro.Rows[0]["AZ"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "AZ");

                    if (dtRegistro.Rows[0]["BA"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BA");

                    if (dtRegistro.Rows[0]["BB"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BB");

                    if (dtRegistro.Rows[0]["BC"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BC");

                    if (dtRegistro.Rows[0]["BD"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BD");

                    if (dtRegistro.Rows[0]["BE"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BE");

                    if (dtRegistro.Rows[0]["BF"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BF");

                    if (dtRegistro.Rows[0]["BG"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BG");

                    if (dtRegistro.Rows[0]["BH"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BH");

                    if (dtRegistro.Rows[0]["BI"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BI");

                    if (dtRegistro.Rows[0]["BJ"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BJ");

                    if (dtRegistro.Rows[0]["BK"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BK");

                    if (dtRegistro.Rows[0]["BL"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BL");

                    if (dtRegistro.Rows[0]["BM"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BM");

                    if (dtRegistro.Rows[0]["BN"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BN");

                    if (dtRegistro.Rows[0]["BO"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BO");

                    if (dtRegistro.Rows[0]["BP"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BP");

                    if (dtRegistro.Rows[0]["BQ"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BQ");

                    if (dtRegistro.Rows[0]["BR"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BR");

                    if (dtRegistro.Rows[0]["BS"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BS");

                    if (dtRegistro.Rows[0]["BT"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BT");

                    if (dtRegistro.Rows[0]["BU"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BU");

                    if (dtRegistro.Rows[0]["BV"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BV");

                    if (dtRegistro.Rows[0]["BW"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BW");

                    if (dtRegistro.Rows[0]["BX"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BX");

                    if (dtRegistro.Rows[0]["BY"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BY");

                    if (dtRegistro.Rows[0]["BZ"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "BZ");

                    if (dtRegistro.Rows[0]["CA"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CA");

                    if (dtRegistro.Rows[0]["CB"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CB");

                    if (dtRegistro.Rows[0]["CC"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CC");

                    if (dtRegistro.Rows[0]["CD"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CD");

                    if (dtRegistro.Rows[0]["CE"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CE");

                    if (dtRegistro.Rows[0]["CF"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CF");

                    if (dtRegistro.Rows[0]["CG"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CG");

                    if (dtRegistro.Rows[0]["CH"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CH");

                    if (dtRegistro.Rows[0]["CI"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CI");

                    if (dtRegistro.Rows[0]["CJ"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CJ");

                    if (dtRegistro.Rows[0]["CK"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CK");

                    if (dtRegistro.Rows[0]["CL"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CL");

                    if (dtRegistro.Rows[0]["CM"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CM");

                    if (dtRegistro.Rows[0]["CN"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CN");

                    if (dtRegistro.Rows[0]["CO"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CO");

                    if (dtRegistro.Rows[0]["CP"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CP");

                    if (dtRegistro.Rows[0]["CQ"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CQ");

                    if (dtRegistro.Rows[0]["CR"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CR");

                    if (dtRegistro.Rows[0]["CS"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CS");

                    if (dtRegistro.Rows[0]["CT"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CT");

                    if (dtRegistro.Rows[0]["CU"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CU");

                    if (dtRegistro.Rows[0]["CV"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CV");

                    if (dtRegistro.Rows[0]["CW"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CW");

                    if (dtRegistro.Rows[0]["CX"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CX");

                    if (dtRegistro.Rows[0]["CY"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CY");

                    if (dtRegistro.Rows[0]["CZ"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "CZ");

                    if (dtRegistro.Rows[0]["DA"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DA");

                    if (dtRegistro.Rows[0]["DB"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DB");

                    if (dtRegistro.Rows[0]["DC"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DC");

                    if (dtRegistro.Rows[0]["DD"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DD");

                    if (dtRegistro.Rows[0]["DE"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DE");

                    if (dtRegistro.Rows[0]["DF"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DF");

                    if (dtRegistro.Rows[0]["DG"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DG");

                    if (dtRegistro.Rows[0]["DH"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DH");

                    if (dtRegistro.Rows[0]["DI"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DI");

                    if (dtRegistro.Rows[0]["DJ"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DJ");

                    if (dtRegistro.Rows[0]["DK"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DK");

                    if (dtRegistro.Rows[0]["DL"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DL");

                    if (dtRegistro.Rows[0]["DM"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "DM");

                    if (dtRegistro.Rows[0]["ED"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "ED");

                    if (dtRegistro.Rows[0]["EE"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EE");

                    if (dtRegistro.Rows[0]["EF"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EF");

                    if (dtRegistro.Rows[0]["EG"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EG");

                    if (dtRegistro.Rows[0]["EH"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EH");

                    if (dtRegistro.Rows[0]["EI"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EI");

                    if (dtRegistro.Rows[0]["EJ"].ToString().Trim().ToUpper() == "X")
                        MI_InserirItem(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString(), "EJ");

                    #endregion


                    IAT_Visitante.MI_ConfirmarImportacao(dtRegistro.Rows[0]["CODIGOBARRAS"].ToString());
                }
            }
        }
    }
}
