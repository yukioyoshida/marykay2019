﻿namespace SPI
{
    partial class CLT_Credencial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.imgCredencial = new System.Windows.Forms.PictureBox();
            this.lblAmarelo = new System.Windows.Forms.Label();
            this.lblVerde = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgCredencial)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(102, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(236, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cole a etiqueta nesta credencial.";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(183, 448);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // imgCredencial
            // 
            this.imgCredencial.ImageLocation = "";
            this.imgCredencial.Location = new System.Drawing.Point(69, 19);
            this.imgCredencial.Name = "imgCredencial";
            this.imgCredencial.Size = new System.Drawing.Size(303, 380);
            this.imgCredencial.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgCredencial.TabIndex = 3;
            this.imgCredencial.TabStop = false;
            // 
            // lblAmarelo
            // 
            this.lblAmarelo.BackColor = System.Drawing.Color.Yellow;
            this.lblAmarelo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmarelo.Location = new System.Drawing.Point(12, 387);
            this.lblAmarelo.Name = "lblAmarelo";
            this.lblAmarelo.Size = new System.Drawing.Size(416, 23);
            this.lblAmarelo.TabIndex = 8;
            this.lblAmarelo.Text = "ENTREGUE A PULSEIRA AMARELA";
            this.lblAmarelo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblAmarelo.Visible = false;
            // 
            // lblVerde
            // 
            this.lblVerde.BackColor = System.Drawing.Color.Green;
            this.lblVerde.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVerde.Location = new System.Drawing.Point(12, 414);
            this.lblVerde.Name = "lblVerde";
            this.lblVerde.Size = new System.Drawing.Size(416, 23);
            this.lblVerde.TabIndex = 11;
            this.lblVerde.Text = "ENTREGUE A PULSEIRA VERDE";
            this.lblVerde.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblVerde.Visible = false;
            // 
            // CLT_Credencial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 483);
            this.Controls.Add(this.lblVerde);
            this.Controls.Add(this.lblAmarelo);
            this.Controls.Add(this.imgCredencial);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CLT_Credencial";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Credencial";
            this.Load += new System.EventHandler(this.CLT_Credencial_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgCredencial)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.PictureBox imgCredencial;
        private System.Windows.Forms.Label lblAmarelo;
        private System.Windows.Forms.Label lblVerde;
    }
}