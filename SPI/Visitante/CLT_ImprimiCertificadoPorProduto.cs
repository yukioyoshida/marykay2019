﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_ImprimiCertificadoPorProduto : Form
    {
        public CLT_ImprimiCertificadoPorProduto()
        {
            InitializeComponent();
        }


        CTO_Visitante ITO_Visitante = null;

        public void setVisitante(CTO_Visitante prITO_Visitante)
        {
            ITO_Visitante= prITO_Visitante;
        }

        private void btImprimi_Click(object sender, EventArgs e)
        {
            MI_Continuar();
        }

        private void MI_Continuar()
        {
            if (rdAvessui.Checked || rdBiomassa.Checked)
            {
                if (rdAvessui.Checked)
                    CSV_Certificado.AP_ProdutoImpressao = 1;
                else
                    CSV_Certificado.AP_ProdutoImpressao = 2;

                CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
                CSV_Certificado.MS_ImprimirCertificado(ITO_Visitante);
                IAT_Visitante.MS_Alterar(ITO_Visitante);

                this.Close();
            }
            else
            {
                MessageBox.Show("Selecione um dos certificados.", "Atenção");
            }
        }

        private void CLT_ImprimiCertificadoPorProduto_Load(object sender, EventArgs e)
        {
            MI_Continuar();
        }
    }
}
