﻿namespace SPI
{
    partial class CLT_ImpressaoLotes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.edtLimite = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btIniciar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // edtLimite
            // 
            this.edtLimite.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtLimite.Location = new System.Drawing.Point(180, 72);
            this.edtLimite.Name = "edtLimite";
            this.edtLimite.Size = new System.Drawing.Size(100, 31);
            this.edtLimite.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(385, 57);
            this.label1.TabIndex = 1;
            this.label1.Text = "Por favor, defina a quantidade de certificados a serem impressos por vez.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btIniciar
            // 
            this.btIniciar.BackColor = System.Drawing.SystemColors.Control;
            this.btIniciar.Location = new System.Drawing.Point(180, 120);
            this.btIniciar.Name = "btIniciar";
            this.btIniciar.Size = new System.Drawing.Size(99, 31);
            this.btIniciar.TabIndex = 2;
            this.btIniciar.Text = "Iniciar Impressão";
            this.btIniciar.UseVisualStyleBackColor = false;
            this.btIniciar.Click += new System.EventHandler(this.button1_Click);
            // 
            // CLT_ImpressaoLotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(460, 174);
            this.Controls.Add(this.btIniciar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.edtLimite);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CLT_ImpressaoLotes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Impressão em Lotes";
            this.Load += new System.EventHandler(this.CLT_ImpressaoLotes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edtLimite;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btIniciar;
    }
}