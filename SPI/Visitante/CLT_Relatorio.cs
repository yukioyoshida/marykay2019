﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace SPI
{
    public partial class CLT_Relatorio : Form
    {
        public CLT_Relatorio()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string stDataPagamento = string.Empty;
            //try
            //{
                //stDataPagamento = Convert.ToDateTime(edtDataPagamento.Text).ToString("yyyy-MM-dd");
                MI_GerarRelatorioNovo(cmbPagamento.Text, stDataPagamento);

            //}
            //catch
            //{
            //    MessageBox.Show("Formato de data incorreto. \n Utilize o modelo DD/MM/AAAA.", "Atenção.");
            //    edtDataPagamento.Text = string.Empty;
            //    edtDataPagamento.Focus();
            //}
        }


        private void MI_GerarRelatorio(string stFormaPagamento, string stDataPagamento)
        {
            string stRelatorioDinamico = string.Empty;
            CAT_CadastrarPedido IAT_Pedido = new CAT_CadastrarPedido();
            if (cmbPagamento.Text != "TODOS")
            {
                DataTable dtPedido = IAT_Pedido.MS_ObterInformacoes(stFormaPagamento,"");
                //StringBuilder stRelatorio = new StringBuilder();
                decimal dcValorTotal = 0;

                string stNomeArquivo = System.DateTime.Now.ToString().Replace(":", "");
                stNomeArquivo = stNomeArquivo.Replace(" ", "");
                stNomeArquivo = stNomeArquivo.Replace("/", "");
                stNomeArquivo = stNomeArquivo.Replace("-", "");
                stNomeArquivo = stNomeArquivo.Replace(".", "");
                stNomeArquivo = stNomeArquivo.Trim();

                StreamWriter stRelatorio = new StreamWriter("c:\\Relatorios\\" + stNomeArquivo + ".html");

                stRelatorio.Write("<table width='100%' border='0' cellpadding='0' cellspacing='0' bordercolor='#666666'>");
                stRelatorio.Write("<tr>");
                stRelatorio.Write("<td bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>FORMA DE PAGAMENTO: " + cmbPagamento.Text + "</strong></font></td>");

                stRelatorio.Write("<td bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>DATA: " + edtDataPagamento.Text + "</strong></font></td>");
                stRelatorio.Write("</tr>");
                stRelatorio.Write("<tr>");
                stRelatorio.Write("<td width='30%' bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>NOME</strong></font></td>");
                stRelatorio.Write("<td width='7%' bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>EMPRESA</strong></font></td>");
                stRelatorio.Write("<td width='7%' bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>VALOR</strong></font></td>");
                stRelatorio.Write("<td width='7%' bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>DATA PAGAMENTO</strong></font></td>");
                stRelatorio.Write("</tr>");

                foreach (DataRow Row in dtPedido.Rows)
                {
                    stRelatorio.Write("<tr>");
                    stRelatorio.Write("<td bgcolor='#FFFFFF'><div align='left'>" + Row.ItemArray[0].ToString() + "</div></td>");
                    stRelatorio.Write("<td width='50%' bgcolor='#FFFFFF' align='left'><font color='#333333'>" + Row.ItemArray[1].ToString() + "</font></td>");
                    stRelatorio.Write("<td width='13%' bgcolor='#FFFFFF' align='left'><font color='#333333'>" + Row.ItemArray[2].ToString() + "</font></td>");
                    stRelatorio.Write("<td width='13%' bgcolor='#FFFFFF' align='left'><font color='#333333'>" + Row.ItemArray[3].ToString() + "</font></td>");
                    dcValorTotal += Convert.ToDecimal(Row.ItemArray[2].ToString());
                    stRelatorio.Write("</tr>");
                }

                stRelatorio.Write("<tr>");
                stRelatorio.Write("<td>&nbsp;</td>");
                stRelatorio.Write("</tr>");
                stRelatorio.Write("<tr>");
                stRelatorio.Write("<td colspan='2' bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>Valor Total: </strong></font></td>");
                stRelatorio.Write("<td bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>" + dcValorTotal.ToString() + "</strong></font></td>");
                stRelatorio.Write("</tr>");
                stRelatorio.Write("</table>");

                stRelatorio.Close();

                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo.FileName = "c:\\Relatorios\\" + stNomeArquivo + ".html";
                proc.Start();
            }
            else
            {
                DataTable dtPedido = IAT_Pedido.MS_ObterInformacoesTodos(stDataPagamento);
                
                decimal dcValorTotal = 0;

                string stNomeArquivo = System.DateTime.Now.ToString().Replace(":", "");
                stNomeArquivo = stNomeArquivo.Replace(" ", "");
                stNomeArquivo = stNomeArquivo.Replace("/", "");
                stNomeArquivo = stNomeArquivo.Replace("-", "");
                stNomeArquivo = stNomeArquivo.Replace(".", "");
                stNomeArquivo = stNomeArquivo.Trim();

                StreamWriter stRelatorio = new StreamWriter("c:\\Relatorios\\" + stNomeArquivo + ".html");

                stRelatorio.Write("<table width='100%' border='0' cellpadding='0' cellspacing='0' bordercolor='#666666'>");
                stRelatorio.Write("<tr>");
                stRelatorio.Write("<td bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>FORMA DE PAGAMENTO: " + cmbPagamento.Text + "</strong></font></td>");

                stRelatorio.Write("<td bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>DATA: " + edtDataPagamento.Text + "</strong></font></td>");
                stRelatorio.Write("</tr>");
                stRelatorio.Write("<tr>");
                stRelatorio.Write("<td width='30%' bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>NOME</strong></font></td>");
                stRelatorio.Write("<td width='7%' bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>EMPRESA</strong></font></td>");
                stRelatorio.Write("<td width='7%' bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>VALOR</strong></font></td>");
                stRelatorio.Write("<td width='7%' bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>FORMA PAGAMENTO</strong></font></td>");
                stRelatorio.Write("<td width='7%' bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>DATA PAGAMENTO</strong></font></td>");
                stRelatorio.Write("</tr>");

                foreach (DataRow Row in dtPedido.Rows)
                {
                    stRelatorio.Write("<tr>");
                    stRelatorio.Write("<td bgcolor='#FFFFFF'><div align='left'>" + Row.ItemArray[0].ToString() + "</div></td>");
                    stRelatorio.Write("<td width='50%' bgcolor='#FFFFFF' align='left'><font color='#333333'>" + Row.ItemArray[1].ToString() + "</font></td>");
                    stRelatorio.Write("<td width='13%' bgcolor='#FFFFFF' align='left'><font color='#333333'>" + Row.ItemArray[2].ToString() + "</font></td>");
                    stRelatorio.Write("<td width='13%' bgcolor='#FFFFFF' align='left'><font color='#333333'>" + Row.ItemArray[3].ToString() + "</font></td>");
                    stRelatorio.Write("<td width='13%' bgcolor='#FFFFFF' align='left'><font color='#333333'>" + Row.ItemArray[4].ToString() + "</font></td>");
                    dcValorTotal += Convert.ToDecimal(Row.ItemArray[2].ToString());
                    stRelatorio.Write("</tr>");
                }

                stRelatorio.Write("<tr>");
                stRelatorio.Write("<td>&nbsp;</td>");
                stRelatorio.Write("</tr>");
                stRelatorio.Write("<tr>");
                stRelatorio.Write("<td colspan='2' bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>Valor Total: </strong></font></td>");
                stRelatorio.Write("<td bgcolor='#FFFFFF' align='left'><font color='#333333'><strong>" + dcValorTotal.ToString() + "</strong></font></td>");
                stRelatorio.Write("</tr>");
                stRelatorio.Write("</table>");

                stRelatorio.Close();

                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo.FileName = "c:\\Relatorios\\" + stNomeArquivo + ".html";
                proc.Start();
            }
        }


        private void MI_GerarRelatorioNovo(string stFormaPagamento, string stDataPagamento)
        {
            string stRelatorioDinamico = string.Empty;
            CAT_CadastrarPedido IAT_Pedido = new CAT_CadastrarPedido();
            if (cmbPagamento.Text != "TODOS")
            {
                //StringBuilder stRelatorio = new StringBuilder();
                decimal dcValorTotal = 0;

                string stNomeArquivo = System.DateTime.Now.ToString().Replace(":", "");
                stNomeArquivo = stNomeArquivo.Replace(" ", "");
                stNomeArquivo = stNomeArquivo.Replace("/", "");
                stNomeArquivo = stNomeArquivo.Replace("-", "");
                stNomeArquivo = stNomeArquivo.Replace(".", "");
                stNomeArquivo = stNomeArquivo.Trim();

                StreamWriter stRelatorio = new StreamWriter("c:\\Relatorios\\" + stNomeArquivo + ".html");

                stRelatorio.Write("<html>");
                stRelatorio.Write("<head>");
                stRelatorio.Write("<title>Relatorio</title>");
                stRelatorio.Write("</head>");
                stRelatorio.Write("<body>");
                stRelatorio.Write("<font face='Arial'>");

                DataTable dtFormasDePagamento = IAT_Pedido.MS_ObterFormasPagamento();

                string stData = string.Empty;
                string stDataAux = string.Empty;
                decimal dcValorTotalDia = 0;
                decimal dcTotalCategoria = 0;

                foreach (DataRow RowForma in dtFormasDePagamento.Rows)
                {
                    stRelatorio.Write("<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                    stRelatorio.Write("  <tr>");
                    stRelatorio.Write("    <td colspan='5'><p><strong><font size='-3'>Pago em " + TirarAcentos(RowForma.ItemArray[0].ToString()).ToString().ToLower() + "</font></strong></p></td>");
                    stRelatorio.Write("  </tr>");

                    foreach (DataRow drDataRowDataPagamento in IAT_Pedido.MS_ObterDataPagamento(RowForma.ItemArray[0].ToString()).Rows)
                    {
                        stDataAux = Convert.ToDateTime(drDataRowDataPagamento.ItemArray[0]).Day.ToString() + "/";
                        stDataAux += Convert.ToDateTime(drDataRowDataPagamento.ItemArray[0]).Month.ToString();

                        stRelatorio.Write("  <tr>");
                        stRelatorio.Write("    <td width='2%'>&nbsp;</td>");
                        stRelatorio.Write("    <td width='2%'><font size='-3'><i>" + stDataAux + "</i></font></td>");
                        stRelatorio.Write("    <td width='30%'>&nbsp;</td>");
                        stRelatorio.Write("    <td width='30%'>&nbsp;</td>");
                        stRelatorio.Write("    <td width='10%'>&nbsp;</td>");
                        stRelatorio.Write("  </tr>");

                        foreach (DataRow Row in IAT_Pedido.MS_ObterInformacoes(RowForma.ItemArray[0].ToString(), Convert.ToDateTime(drDataRowDataPagamento.ItemArray[0]).ToString("yyyy-MM-dd")).Rows)
                        {
                            stRelatorio.Write("  <tr>");
                            stRelatorio.Write("    <td>&nbsp;</td>");
                            stRelatorio.Write("    <td>&nbsp;</td>");
                            stRelatorio.Write("    <td><font size='-3'>" + TirarAcentos(Row["CLIENTE"].ToString()) + "</font></td>");
                            stRelatorio.Write("    <td><font size='-3'>" + TirarAcentos(Row["EMPRESA"].ToString()) + "</font></td>");
                            stRelatorio.Write("    <td><font size='-3'>R$ " + TirarAcentos(Row["VALOR"].ToString()) + "</font></td>");
                            dcValorTotalDia += Convert.ToDecimal(Row["VALOR"]);
                            stRelatorio.Write("  </tr>");
                        }

                        stRelatorio.Write("  <tr>");
                        stRelatorio.Write("    <td>&nbsp;</td>");
                        stRelatorio.Write("    <td colspan='3'><font size='-3'>Valor Total do dia:</font></td>");
                        stRelatorio.Write("    <td><font size='-3'> R$ " + dcValorTotalDia.ToString("##0.00") + " </font></td>");
                        stRelatorio.Write("  </tr>");
                        stRelatorio.Write("  <tr>");
                        stRelatorio.Write("    <td>&nbsp;</td>");
                        stRelatorio.Write("    <td>&nbsp;</td>");
                        stRelatorio.Write("    <td>&nbsp;</td>");
                        stRelatorio.Write("    <td>&nbsp;</td>");
                        stRelatorio.Write("    <td>&nbsp;</td>");
                        stRelatorio.Write("  </tr>");

                        dcTotalCategoria += dcValorTotalDia;
                        dcValorTotalDia = 0;
                    }

                    stRelatorio.Write("  <tr>");
                    stRelatorio.Write("    <td colspan='4'><font size='-3'>Valor Total da Categoria: </font> </td>");
                    stRelatorio.Write("    <td><font size='-3'> R$ " + dcTotalCategoria.ToString("##0.00") + "</font></td>");
                    stRelatorio.Write("  </tr>");
                    stRelatorio.Write("</table>");

                    stRelatorio.Write("<BR />");
                    stRelatorio.Write("<HR />");

                    dcTotalCategoria = 0;
                }

                stRelatorio.Close();

                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo.FileName = "c:\\Relatorios\\" + stNomeArquivo + ".html";
                proc.Start();
            }
        }

        public string TirarAcentos(string texto)
        {
            string textor = "";

            for (int i = 0; i < texto.Length; i++)
            {
                if (texto[i].ToString() == "ã") textor += "a";
                else if (texto[i].ToString() == "á") textor += "a";
                else if (texto[i].ToString() == "à") textor += "a";
                else if (texto[i].ToString() == "â") textor += "a";
                else if (texto[i].ToString() == "ä") textor += "a";
                else if (texto[i].ToString() == "é") textor += "e";
                else if (texto[i].ToString() == "è") textor += "e";
                else if (texto[i].ToString() == "ê") textor += "e";
                else if (texto[i].ToString() == "ë") textor += "e";
                else if (texto[i].ToString() == "í") textor += "i";
                else if (texto[i].ToString() == "ì") textor += "i";
                else if (texto[i].ToString() == "ï") textor += "i";
                else if (texto[i].ToString() == "õ") textor += "o";
                else if (texto[i].ToString() == "ó") textor += "o";
                else if (texto[i].ToString() == "ò") textor += "o";
                else if (texto[i].ToString() == "ö") textor += "o";
                else if (texto[i].ToString() == "ú") textor += "u";
                else if (texto[i].ToString() == "ù") textor += "u";
                else if (texto[i].ToString() == "ü") textor += "u";
                else if (texto[i].ToString() == "ç") textor += "c";
                else if (texto[i].ToString() == "Ã") textor += "A";
                else if (texto[i].ToString() == "Á") textor += "A";
                else if (texto[i].ToString() == "À") textor += "A";
                else if (texto[i].ToString() == "Â") textor += "A";
                else if (texto[i].ToString() == "Ä") textor += "A";
                else if (texto[i].ToString() == "É") textor += "E";
                else if (texto[i].ToString() == "È") textor += "E";
                else if (texto[i].ToString() == "Ê") textor += "E";
                else if (texto[i].ToString() == "Ë") textor += "E";
                else if (texto[i].ToString() == "Í") textor += "I";
                else if (texto[i].ToString() == "Ì") textor += "I";
                else if (texto[i].ToString() == "Ï") textor += "I";
                else if (texto[i].ToString() == "Õ") textor += "O";
                else if (texto[i].ToString() == "Ó") textor += "O";
                else if (texto[i].ToString() == "Ò") textor += "O";
                else if (texto[i].ToString() == "Ö") textor += "O";
                else if (texto[i].ToString() == "Ú") textor += "U";
                else if (texto[i].ToString() == "Ù") textor += "U";
                else if (texto[i].ToString() == "Ü") textor += "U";
                else if (texto[i].ToString() == "Ç") textor += "C";
                else textor += texto[i];
            }
            return textor.ToUpper();
        }


        private void CLT_Relatorio_Load(object sender, EventArgs e)
        {
            MI_CarregaControleCursos();
            MI_CarregaFormaPagamento();
        }

        private void MI_CarregaFormaPagamento()
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtCategorias = IAT_Visitante.MS_ObterFormaPagamento();

            cmbPagamento.Items.Add("");
            cmbPagamento.Items.Add("TODOS");

            foreach (DataRow Row in dtCategorias.Rows)
                cmbPagamento.Items.Add(Row["FormaPagamento"].ToString());

            cmbPagamento.Text = "";
        }

        private void MI_CarregaControleCursos()
        {
            CAT_CadastrarPedido IAT_Pedido = new CAT_CadastrarPedido();
            DataTable dtPedidos = IAT_Pedido.MS_ObterControleCursos();

            gdwCursos.DataSource = dtPedidos;
        }
    }
}
