using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BibliotecaComponentesInteracao;
using FuncoesGlobaisWeb;
using ControleRemoto;
using SPI.Visitante;
using System.Drawing.Imaging;

namespace SPI
{
    public partial class CLT_DadosPessoais : Form
    {
        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        public string stUsuario = string.Empty;
        public string stNivelAcesso = string.Empty;

        //AUXILIO NO MODO INTERNET
        public string stCodigoInternet = string.Empty;
        public string stInternetHabilitada = "N";

        public void setUsuario(string prstUsuario)
        {
            stUsuario = prstUsuario;
        }

        public void setNivelAcesso(string prNivelAcesso)
        {
            stNivelAcesso = prNivelAcesso;
        }

        CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();
        CTO_Visitante ITO_Visitante = new CTO_Visitante();

        public CLT_DadosPessoais()
        {
            InitializeComponent();
            btCertificado.Visible = false;
        }

        CSV_VariaveisGlobal CSV_Glogal = new CSV_VariaveisGlobal();

        private void CLT_DadosPessoais_Load(object sender, EventArgs e)
        {
            lblVersao.Text = "Vers�o: " + new CSV_VariaveisGlobal().getVersao() + " Semin�rio: " + new CSV_VariaveisGlobal().getSemiario();
            MI_DeterminaImagem();
            CarregaUsuario();
            MI_ObterNumeroTerminalControle();
            MI_CarregarTipoInscricao();
   
            MI_ControTela(true);

            if (stInternetHabilitada == "S")
            {
                MI_PreparaTelaInternet();
            }
        }

        private void MI_CarregarTipoInscricao()
        {
            ddlTipoInscricao.Items.Clear();

            ddlTipoInscricao.Items.Add("");
            foreach (DataRow Row in new DAO_Visitante().MS_ObterTipoInscricao().Rows)
            {
                ddlTipoInscricao.Items.Add(Row["TipoDeInscricao"].ToString());
            }

            ddlTipoInscricao.Text = "";
        }

        private void MI_DeterminaImagem()
        {
            imgCadeado.ImageLocation = "cadeado1.jpg";
        }

        private void CarregaUsuario()
        {
            lblUsuario.Text = CSV_Global.getUsuario();
            lblNivelAcesso.Text = CSV_Global.getPrioridade();
            lblTerminal.Text = CSV_Global.getNumeroTerminal();

            if (CSV_Global.getPrioridade().Equals("1"))
            {
                btNovo.Visible = false;
            }

            if ((CSV_Global.getPrioridade().Equals("3")) || (CSV_Global.getPrioridade().Equals("4")))
            {
                panel1.Visible = false;

                CLT_LeituraJoia open = new CLT_LeituraJoia();
                open.ShowDialog();

                MI_LOGOFF();
            }

            if(CSV_Global.getPrioridade().Equals("5"))
            {
                CLT_RelatorioProduto abrirRelatorio = new CLT_RelatorioProduto();
                abrirRelatorio.ShowDialog();
                MI_LOGOFF();
            }
        }

        private void MI_PreparaTelaInternet()
        {
            menuStrip1.Enabled = false;
            edtCodigoBarras.Text = stCodigoInternet;
            CarregaTela();
            MI_ModificarDados();

            btSair.Enabled = false;
            edtChamar.Enabled = false;
            edtConfig.Enabled = false;
        }

        private bool MI_ValidaCPF()
        {
            bool boResult = true;
            //FactoryComponente oFactoryComponente = new FactoryComponente();
            //oFactoryComponente.setDiretorioAplicacao("");
            //oFactoryComponente.setIdioma(1);
            //IComponente oComponente = null;

            try
            {
                if (edtCPF.Text != string.Empty)
                {
                    //    oComponente = oFactoryComponente.get("CPF");
                    //    oComponente.setValue(edtCPF.Text);
                    //    if (!oComponente.MS_Validator())
                    //    {
                    //        MessageBox.Show(oComponente.getMessagemDeErro(), "ATEN��O!");
                    //        edtCPF.Focus();
                    //        return boResult = false;
                    //    }
                    return boResult = CSV_FuncoesGlobais.MS_ValidarCPF(edtCPF.Text);
                }
            }
            catch
            {
                MessageBox.Show("CPF Inv�lido!", "ATEN��O!");
                edtCPF.Focus();
                return boResult = false;
            }
            return boResult;
        }

        private bool MI_Consistencias()
        {
            bool boResult = true;

            try
            {
                if (edtNomeConsultora.Text == string.Empty)
                {
                    MessageBox.Show("Preencha o Nome completo", "ATEN��O!");
                    edtNomeConsultora.Focus();
                    return boResult = false;
                }
                if (edtNomeCracha.Text == string.Empty)
                {
                    MessageBox.Show("Preencha o nome crach�", "ATEN��O!");
                    edtNomeCracha.Focus();
                    return boResult = false;
                }

                if (edtEmail.Text.Trim() != String.Empty)
                {
                    if (ValidaEmail(edtEmail.Text) == false)
                    {
                        MessageBox.Show("E-mail inv�lido", "ATEN��O!");
                        edtEmail.Focus();
                        return boResult = false;
                    }
                }

                if (ddlTipoInscricao.Text == string.Empty)
                {
                    MessageBox.Show("Selecione o tipo de Inscri��o", "ATEN��O!");
                    ddlTipoInscricao.Focus();
                    return boResult = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            

            return boResult;
        }

        private void btSalvar_Click(object sender, EventArgs e)
        {
            MI_TransferirParaFormularioPerguntas();
        }

        protected void MI_TransferirParaFormularioPerguntas()
        {
            if (MI_Consistencias())
            {
                //  Verificar se � uma alteracao
                CTO_Visitante ITO_Visitante = new CTO_Visitante();
                ITO_Visitante = IAT_CadastrarVisitante.MS_ObterPorCodigo(edtCodigoBarras.Text);

                if (ITO_Visitante == null)
                {
                    //Cria uma nova instancia para receber os dados dos controle.
                    ITO_Visitante = new CTO_Visitante();
                }

                CLT_ManterQuestionario ABRIR = new CLT_ManterQuestionario();
                ABRIR.setVisitante(MI_CopyControlsToCTO(ITO_Visitante));

                if (imgCadeado.ImageLocation == "cadeado1.jpg")
                {
                    ABRIR.stValidaQuestionario = "VALIDAR";
                }
                else
                {
                    ABRIR.stValidaQuestionario = string.Empty;
                }
                ABRIR.ShowDialog();


                if (stInternetHabilitada == "S")
                {
                    this.Close();
                    stInternetHabilitada = "N";
                }

                edtCodigoBarras.Text = ABRIR.stCodigoBarras.ToString();

                if (ABRIR.stControle == "1")
                { }
                else
                {
                    CarregaTela();

                    if (edtCodigoBarras.Text != "")
                    {
                        MI_ControTela(true);
                        MI_ControleBotoes(true);
                    }
                }
            }
        }

        private CTO_Visitante MI_CopyControlsToCTO(CTO_Visitante ITO_Visitante)
        {
            //DADOS PESSOAIS
            //=======================================================================================
            ITO_Visitante.setCPF(edtCPF.Text.Trim().ToUpper());
            ITO_Visitante.setConsultora(edtConsultora.Text.Trim().ToUpper());

            ITO_Visitante.setNomedaConsultora(edtNomeConsultora.Text.ToUpper());
            ITO_Visitante.setNomeCracha(edtNomeCracha.Text.ToUpper());

            ITO_Visitante.setNiveldeCarreira(edtNiveldeCarreira.Text.Trim().ToUpper());
            ITO_Visitante.setEmaildeContato(edtEmail.Text.Trim().ToUpper());

            ITO_Visitante.setTelefone(edtTelefone.Text.Trim().ToUpper());
            ITO_Visitante.setCidade(edtCidade.Text.Trim().ToUpper());
            ITO_Visitante.setEstado(ddlEstado.Text);

            ITO_Visitante.setTipodeInscricao(ddlTipoInscricao.Text);

            ITO_Visitante.setNomedaNSD(edtNomeNSD.Text.Trim().ToUpper());
            ITO_Visitante.setObservacoes(edtObservacoes.Text.Trim().ToUpper());

            ITO_Visitante.setValorInscricaoNova(lblValor.Tag.ToString());
            ITO_Visitante.setFormaInscricaoNova(lblFormaPagamento.Tag.ToString());

            return ITO_Visitante;
        }

        private void MI_CopyCTOToControls(CTO_Visitante ITO_Visitante)
        {
            //DADOS PESSOAIS
            //=======================================================================================
            edtCodigoBarras.Text = ITO_Visitante.getCodigoBarras();

            edtCPF.Text = ITO_Visitante.getCPF();
            edtConsultora.Text = ITO_Visitante.getConsultora();

            edtNomeConsultora.Text = ITO_Visitante.getNomedaConsultora();
            edtNomeCracha.Text = ITO_Visitante.getNomeCracha();

            edtNiveldeCarreira.Text = ITO_Visitante.getNiveldeCarreira();
            edtEmail.Text = ITO_Visitante.getEmaildeContato();

            edtTelefone.Text = ITO_Visitante.getTelefone();
            edtCidade.Text = ITO_Visitante.getCidade();
            ddlEstado.Text = ITO_Visitante.getEstado();

            ddlTipoInscricao.Text = ITO_Visitante.getTipodeInscricao();

            edtNomeNSD.Text = ITO_Visitante.getNomedaNSD();
            edtObservacoes.Text = ITO_Visitante.getObservacoes();

            MI_ObterPedidos(ITO_Visitante);
        }

        private void CarregaTela()
        {
            CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();
            CTO_Visitante ITO_Visitante = IAT_CadastrarVisitante.MS_ObterPorCodigo(edtCodigoBarras.Text);

            if (ITO_Visitante != null)
            {
                MI_CopyCTOToControls(ITO_Visitante);
                MI_ControTela(true);
                MI_ControleBotoes(true);
            }
        }

        private void MI_LimparProdutos()
        {
            gdwPedidos.Rows.Clear();
            for(int i = 1; i <= 6; i++)
            {
                PictureBox pic = (PictureBox)pnlEntraDePin.Controls["pic" + i];
                pic.Image = null;
            }
        }

        private void MI_ObterPedidos(CTO_Visitante ITO_Visitante)
        {
            string stTipoInscricao = MI_DefinirTipoInscricao(ddlTipoInscricao.Text);
            DataTable dtReconhecimentos = new DAO_Visitante().MS_ObterReconhecimentoPorConsultora(ITO_Visitante.getConsultora(), stTipoInscricao);

            int idPicEntrega = 1;
            foreach (DataRow Row in dtReconhecimentos.Rows)
            {
                if (stTipoInscricao == "CONVIDADOS")
                {
                    // PERMITE SOMENTE SE FOR OS ITENS DE CONVIDADO
                    // 1 AREA DE ASSENTOS
                    if ((Row["dsReconhecimento"].ToString().ToUpper().Contains("AREA DE ASSENTO:")) || ("AREA DE ASSENTO:".Contains(Row["dsReconhecimento"].ToString().ToUpper())))
                    {
                        gdwPedidos.Rows.Add(
                            Row["dsReconhecimento"].ToString()
                            );
                    }

                    // 2 DEFINIDOS PELO CLIENTE
                    if (
                            (Row["dsReconhecimento"].ToString().Trim() == "Convidado") ||
                            (Row["dsReconhecimento"].ToString().Trim() == "Atividade Especial Convidado") ||
                            (Row["dsReconhecimento"].ToString().Trim() == "Coquetel da Realeza - Convidado") ||
                            (Row["dsReconhecimento"].ToString().Trim() == "C�njuge") ||
                            (Row["dsReconhecimento"].ToString().Trim() == "Brunch com Alvaro Polanco")
                       )
                    {
                        gdwPedidos.Rows.Add(
                            Row["dsReconhecimento"].ToString()
                            );
                    }
                }
                else
                {
                    if (Row["TipoRoteiro"].ToString().ToUpper() == "I")
                    {
                        if (Row["dsLocal"].ToString().ToUpper() == "ENTREGA")
                        {
                            MI_DemostrarEntrega(Row["cdRoteiro"].ToString(), idPicEntrega.ToString());
                            idPicEntrega = idPicEntrega + 1;
                        }
                        else
                        {
                            if (
                                (Row["dsReconhecimento"].ToString().Trim() == "Convidado") ||
                                (Row["dsReconhecimento"].ToString().Trim() == "Atividade Especial Convidado") ||
                                (Row["dsReconhecimento"].ToString().Trim() == "Coquetel da Realeza - Convidado") ||
                                (Row["dsReconhecimento"].ToString().Trim() == "C�njuge")
                            )
                            {
                                // OS ITENS ACIMA S�O DO CONVIDADO N�O APARECE PARA A CONSULTORA
                            }
                            else
                            {
                                gdwPedidos.Rows.Add(
                                    Row["dsReconhecimento"].ToString()
                                );
                            }
                                
                        }
                    }
                }
            }

            // VERIFICAR SE POSSUI JOIAS
            pnlJoias.Visible = false;
            if (stTipoInscricao != "CONVIDADOS")
            {
                if (new DAO_Visitante().MS_ObterPremiacoes(edtConsultora.Text).Rows.Count>0)
                {
                    pnlJoias.Visible = true;
                }
            }

        }

        private void MI_DemostrarEntrega(string prCdRoteiro, string prIdPic)
        {
            PictureBox pic = (PictureBox)pnlEntraDePin.Controls["pic"+ prIdPic];
            new InserirFoto().MS_SetFoto(pic, prCdRoteiro);
            pic.Visible = true;
        }

        private string MI_DefinirTipoInscricao(string prTipoInscricao)
        {
            string stResult = string.Empty;

            if (("CONVIDADOS".Contains(prTipoInscricao.ToUpper())) || (prTipoInscricao.ToUpper()).Contains("CONVIDADOS"))
            {
                stResult = "CONVIDADOS";
            }
            else
            {
                stResult = "CONSULTORAS";
            }

            return stResult;
        }

        private string MI_ObterProdutos(string prCdPedido)
        {
            string stRetorno = string.Empty;

            CAT_CadastrarItensPedido IAT_ItensPedido = new CAT_CadastrarItensPedido();
            DataTable dtItens = IAT_ItensPedido.MS_ObterItensPorPedido(prCdPedido);

            foreach (DataRow Row in dtItens.Rows)
            {
                stRetorno += Row["Sigla"].ToString() + "; ";
            }

            return stRetorno;
        }

        private void btNovo_Click(object sender, EventArgs e)
        {
            Clt_InsertParticipante open = new Clt_InsertParticipante();
            open.ShowDialog();

            if (open.AP_Result==true)
            {
                MI_LimpaTela();
                MI_ControleBotoes(false);
                MI_ControTela(false);
                edtCPF.Focus();

                edtConsultora.Text = open.AP_Consultora;
                ddlTipoInscricao.Text = open.AP_TipoInscricao;

                pnlNovaInscricao.Visible = true;

                lblValor.Tag = open.AP_Valor;
                lblFormaPagamento.Tag = open.AP_FormaPagto;

                lblValor.Text = "VALOR: " + lblValor.Tag;
                lblFormaPagamento.Text = "FORMA PAGTO: " + lblFormaPagamento.Tag;

                if (open.AP_TipoInscricaoCategoria == "CONSULTORAS")
                {
                    edtCPF.Text = open.lblCPF.Text;
                    edtNomeCracha.Text = open.lblNomeCompleto.Text;
                    edtNomeConsultora.Text = open.lblNomeCompleto.Text;
                    edtNomeNSD.Text = open.lblNSD.Text;
                }

                ddlTipoInscricao.Enabled = false;
            }
        
        }

        private void MI_ControleBotoes(bool boStatus)
        {
            btNovo.Enabled = boStatus;
            btAlterar.Enabled = boStatus;
            btLocalizar.Enabled = boStatus;
            btSalvar.Enabled = !boStatus;
            btCancelar.Enabled = !boStatus;
        }

        private void MI_ControTela(bool boStatus)
        {
            foreach (Control objControle in this.grpDadosPessoais.Controls)
            {
                //TEXT BOX
                if (objControle is TextBox)
                {
                    TextBox ObjTextBox = new TextBox();
                    ObjTextBox = (TextBox)objControle;
                    ObjTextBox.ReadOnly = boStatus;
                }
                if (objControle is ComboBox)
                    objControle.Enabled = !boStatus;

                if (objControle is CheckBox)
                    objControle.Enabled = !boStatus;

                // Painel entrega de PIN
                pnlEntraDePin.Visible = boStatus;

            }

            if (boStatus == true)
            {
                imgCadeado.ImageLocation = "cadeado1.jpg";
            }
        }

        private void MI_LimpaTela()
        {
            foreach (Control objControle in this.grpDadosPessoais.Controls)
            {
                if ((objControle is TextBox) || (objControle is ComboBox))
                    objControle.Text = string.Empty;

                if (objControle is CheckBox)
                {
                    CheckBox chk = (CheckBox)objControle;
                    chk.Checked = false;
                }

            }

            MI_LimparProdutos();
            pnlJoias.Visible = false;
            pnlNovaInscricao.Visible = false;

            lblValor.Tag = "";
            lblFormaPagamento.Tag = "";

            lblValor.Text = "VALOR:";
            lblFormaPagamento.Text = "FORMA PAGTO:";
          }

        private void tmrRelogio_Tick(object sender, EventArgs e)
        {
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void btCopiar_Click(object sender, EventArgs e)
        {
            MI_LocalizarRegistro();
        }

        private void MI_CadastroSimplificado()
        {
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente deseja deslogar?", "ATEN��O!", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                MI_LOGOFF();
            }
        }

        private void MI_LocalizarRegistro()
        {
            CLT_LocalizarRegistro ABRIR = new CLT_LocalizarRegistro();
            ABRIR.ShowDialog();
            MI_LimpaTela();
            if (ABRIR.stCodigoBarras.ToString().Trim() != string.Empty)
                edtCodigoBarras.Text = ABRIR.stCodigoBarras.ToString();

            CarregaTela();
        }

        private void MI_ObterRegistrosEncotrados(string stTipoBusca, string stPalavraChave)
        {
            string stCodigoObtido = string.Empty;

            CLT_LocalizarRegistro ABRIR = new CLT_LocalizarRegistro();

            ABRIR.stMetodoBusca = stTipoBusca;
            ABRIR.stPalavraChave = stPalavraChave;
            ABRIR.ShowDialog();

            stCodigoObtido = ABRIR.stCodigoBarras.ToString();

            if (stCodigoObtido != string.Empty)
            {
                if (stCodigoObtido.Trim() != string.Empty)
                    edtCodigoBarras.Text = stCodigoObtido;

                CarregaTela();
            }
        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente deseja cancelar?", "Aten��o!", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                MI_LimpaTela();
                MI_ControTela(true);
                MI_ControleBotoes(true);
                CarregaTela();

                if (stInternetHabilitada == "S")
                {
                    this.Close();
                    stInternetHabilitada = "N";
                }
            }
        }

        private void btAlterar_Click(object sender, EventArgs e)
        {
            MI_ModificarDados();
        }

        private void MI_ModificarDados()
        {
            if (edtCodigoBarras.Text.Trim() != "")
            {
                MI_ControleBotoes(false);
                MI_ControTela(false);

                ddlTipoInscricao.Enabled = false;
            }
        }

        private void MI_ObterPeloCPF(string prCpf)
        {
            bool boResult = false;
            DataTable dtRegistros = new DataTable();
            DataRow drLinha;

            CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();
            dtRegistros = IAT_CadastrarVisitante.MS_ObterPorCPFTabela(prCpf);

            if (dtRegistros.Rows.Count > 0)
            {
                for (int i = 0; dtRegistros.Rows.Count - i >= i; i++)
                {
                    drLinha = dtRegistros.Rows[i];
                    edtCPF.Text = drLinha["CPF"].ToString();
                    if (drLinha["CodigoBarras"].ToString() == edtCodigoBarras.Text)
                        boResult = true;
                }
                if (boResult == false)
                {
                    if (MessageBox.Show("CPF J� cadastrado. Deseja vizualizar o(s) registro(s) localizado(s)?", "ATEN��O!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        MI_ObterRegistrosEncotrados("CPF", edtCPF.Text);
                        MI_ModificarDados();
                    }
                    else
                        edtCPF.Text = "";
                }
            }
        }

        private void MI_ObterPeloNome(string prNOME)
        {
            bool boResult = false;
            DataTable dtRegistros = new DataTable();
            DataRow drLinha;

            CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();
            dtRegistros = IAT_CadastrarVisitante.MS_ObterPorNomeTabela(prNOME);

            if (dtRegistros.Rows.Count > 0)
            {
                for (int i = 0; dtRegistros.Rows.Count - i >= i; i++)
                {
                    drLinha = dtRegistros.Rows[i];
                    if (drLinha["CodigoBarras"].ToString() == edtCodigoBarras.Text)
                        boResult = true;
                }
                if (boResult == false)
                {
                    if (MessageBox.Show("Nome J� cadastrado. Deseja vizualizar o(s) registro(s) localizado(s)?", "ATEN��O!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        MI_ObterRegistrosEncotrados("NOME", edtNomeConsultora.Text);
                        MI_ModificarDados();
                    }
                }
            }
        }

        private void MI_LOGOFF()
        {
            this.Close();
            CLT_Default ABRIR = new CLT_Default();
            ABRIR.ShowDialog();
        }

        private void mLogoff_Click(object sender, EventArgs e)
        {
            MI_LOGOFF();
        }

        private void mSimplificado_Click(object sender, EventArgs e)
        {
        }

        private void MI_CopiarDados()
        {
        }

        private void mCopiarDados_Click(object sender, EventArgs e)
        {
            MI_CopiarDados();
            MI_ControleBotoes(false);
        }

        string stProdutos = string.Empty;
        string stImpressao = string.Empty;
        string stNumeroPedido = string.Empty;
        string stCancelaImpressao = string.Empty;

        private void mImprmirCredencial_Click(object sender, EventArgs e)
        {
            if (edtNomeConsultora.ReadOnly == true)
            {
                CSV_Global.setPrioridade("1");
                if (MI_ConsultarEmissaoConsultora(edtConsultora.Text) == false)
                {
                    return;
                }

                CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();
                CTO_Visitante ITO_Visitante = IAT_CadastrarVisitante.MS_ObterPorCodigo(edtCodigoBarras.Text);

                if (ITO_Visitante != null)
                {
                    if (ITO_Visitante.getEmissaoCracha().ToString() == "S")
                    {
                        //VERIFICAR LIBERA��O ATRAVEZ DE SENHA
                        CLT_Senha OpenSenha = new CLT_Senha();
                        OpenSenha.ShowDialog();

                        if (OpenSenha.AP_Result == false)
                            return;
                        //-----------------------------------

                        MI_RegistraImpressaoCracha(edtCodigoBarras.Text, lblUsuario.Text, "COMPLETO", OpenSenha.AP_Motivo);
                        MI_ImprimirCredencial(ITO_Visitante);
                    }
                    else
                    {
                        new Clt_AlertaDevolucao().ShowDialog();

                        MI_RegistraImpressaoCracha(edtCodigoBarras.Text, lblUsuario.Text, "COMPLETO", "Primeira Impress�o");
                        MI_ImprimirCredencial(ITO_Visitante);
                    }

                }
            }

            stImpressao = string.Empty;
        }

        private bool MI_ConsultarEmissaoConsultora(string prNrConsultora)
        {
            bool boResult = false;

            if (MI_DefinirTipoInscricao(ddlTipoInscricao.Text) == "CONVIDADOS")
            {
                foreach (DataRow Row in new DAO_Visitante().MS_ObterRegistro(prNrConsultora, "", "Consultora").Rows)
                {
                    if ((MI_DefinirTipoInscricao(Row["TipodeInscricao"].ToString().Trim()) == "CONSULTORAS") && (Row["EMITIDO"].ToString().ToUpper().Trim() == "S"))
                    {
                        boResult = true;
                    }
                }
            }
            else
            {
                boResult = true;
            }

            if (boResult == false)
            {
                MessageBox.Show("A emiss�o da credencial do acompanhante/convidado s� pode ser realizada ap�s a emiss�o da credencial da consultora", "Aten��o", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return boResult;
        }


        private void MI_ObterItensImpressao(int prObjRefCliente, string stData)
        {
            CAT_CadastrarItensPedido IAT_ItensPedido = new CAT_CadastrarItensPedido();
            CAT_CadastrarPedido IAT_Pedido = new CAT_CadastrarPedido();
            DataTable DtPedido = IAT_Pedido.MS_ObterPedidosPorPessoa(prObjRefCliente);

            foreach (DataRow Row in DtPedido.Rows)
            {
                DataTable dtItens = IAT_ItensPedido.MS_ObterItensPorPedido(Row["CdPedido"].ToString());
                if (Row["dsSituacao"].ToString().ToUpper() == "PAGO")
                {
                    foreach (DataRow Rowr in dtItens.Rows)
                    {
                        stProdutos += Rowr["cdProduto"].ToString() + ";";
                    }
                }
                else
                {
                    if ((CSV_Global.getPrioridade().ToString() == "1") || (CSV_Global.getPrioridade().ToString() == "2"))
                    {
                        if (MessageBox.Show("O pedido de n�mero " + Row["CdPedido"].ToString() + " n�o encontra-se pago. Deseja imprimir mesmo assim?", "ATEN��O", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            foreach (DataRow Rowr in dtItens.Rows)
                            {
                                stProdutos += Rowr["cdProduto"].ToString() + ";";
                            }
                            stCancelaImpressao = string.Empty;
                        }
                        else
                        {
                            stCancelaImpressao = "1";
                        }
                    }
                }
            }
        }

        private void MI_GerarCodigoImpressao(string NumeroPedido)
        {
            CAT_CadastrarCodigoPagamento IAT_CodigoPagamento = new CAT_CadastrarCodigoPagamento();
            CTO_CodigoPagamento ITO_CodigoPagamento = new CTO_CodigoPagamento();

            ITO_CodigoPagamento.setitCodigo(MI_GerarCodigoPagamento(NumeroPedido));
            ITO_CodigoPagamento.setCodigoBarras(NumeroPedido);
            IAT_CodigoPagamento.MS_Incluir(ITO_CodigoPagamento);
        }

        public string MI_GerarCodigoPagamento(string stPedido)
        {
            string stPagamento = string.Empty;

            DateTime dtHora = System.DateTime.Now;

            string stHora;

            stHora = dtHora.Second.ToString() + dtHora.Millisecond.ToString();

            //stPagamento = stPedido + stHora;
            stPagamento = "2012" + stPedido;
            return stPagamento;
        }

        private void MI_ImprimirCredencial(CTO_Visitante ITO_Visitante)
        {
            bool boImprimirCodigo = true;
            CAT_ImprimirEtiqueta IAT_ImprimirEtiqueta = new CAT_ImprimirEtiqueta(Convert.ToInt64(ITO_Visitante.getCodigoBarras()), boImprimirCodigo, stProdutos, "1");
            new CAT_CadastrarVisitante().MS_ConfirmaImpressao(Convert.ToInt64(ITO_Visitante.getCodigoBarras()));

            CLT_AlertaImprimir impr = new CLT_AlertaImprimir();
            impr.Categoria = MI_ObterAreaAssento();
            impr.ShowDialog();
        }

        private string MI_ObterAreaAssento()
        {
            string stResult = "";

            foreach(DataGridViewRow Row in gdwPedidos.Rows)
            {
                if ((Row.Cells[0].Value.ToString().ToUpper().Contains("AREA DE ASSENTO:")) || ("AREA DE ASSENTO:".Contains(Row.Cells[0].Value.ToString().ToUpper())))
                {
                    stResult = Row.Cells[0].Value.ToString().ToUpper();

                    if ((stResult.Contains("CINZA")) || ("CINZA".Contains(Row.Cells[0].Value.ToString().ToUpper())))
                    {
                        stResult = "CINZA";
                    }
                    else if ((stResult.Contains("GOLD")) || ("GOLD".Contains(Row.Cells[0].Value.ToString().ToUpper())))
                    {
                        stResult = "GOLD";
                    }
                    else if ((stResult.Contains("LARANJA")) || ("LARANJA".Contains(Row.Cells[0].Value.ToString().ToUpper())))
                    {
                        stResult = "LARANJA";
                    }
                    else if ((stResult.Contains("ROXO")) || ("LARANJA".Contains(Row.Cells[0].Value.ToString().ToUpper())))
                    {
                        stResult = "ROXO";
                    }
                }
            }

            return stResult;
        }

        private void MI_RegistraImpressaoCracha(string stCodigoBarras, string stNomeUsuario, string stOrigem, string prVia)
        {
            CAT_CadastrarEmissaoCracha IAT_EmissaoCracha = new CAT_CadastrarEmissaoCracha();
            CTO_EmissaoCracha ITO_Emissao = new CTO_EmissaoCracha();

            ITO_Emissao.setCodigoBarras(stCodigoBarras);
            ITO_Emissao.setUsuarioImpressao(stNomeUsuario);
            ITO_Emissao.setOrigemImpressao(stOrigem);

            IAT_EmissaoCracha.MS_Incluir(ITO_Emissao, prVia);
        }

        private void mConfigurarEtiqueta_Click(object sender, EventArgs e)
        {
            if (stNivelAcesso == "1")
            {
                CLT_ConfigurarImpressao ABRIR = new CLT_ConfigurarImpressao();
                ABRIR.ShowDialog();
            }
            else
            {
                MessageBox.Show("Usu�rio n�o autorizado", "SPI - Controle de acesso");
            }
        }

        private void mLocalizarRegistro_Click(object sender, EventArgs e)
        {
            MI_LocalizarRegistro();
        }

        private void mUsuarios_Click(object sender, EventArgs e)
        {
            if (lblNivelAcesso.Text == "1")
            {
                CLT_Usuarios ABRIR = new CLT_Usuarios();
                ABRIR.ShowDialog();
            }
            else
            {
                MessageBox.Show("Usu�rio n�o autorizado", "SPI - Controle de acesso");
            }
        }

        private void edtChamar_Click(object sender, EventArgs e)
        {
            MI_ChamarProximo(lblNumeroTerminal.Text);
        }

        private void MI_ChamarProximo(string stNumeroTerminal)
        {
            CAT_CadastrarTerminal IAT_CadastrarTerminal = new CAT_CadastrarTerminal();

            if (MI_VerificaStatusTerminal(stNumeroTerminal).Equals(true))
            {
                IAT_CadastrarTerminal.MS_Incluir(MI_ObterValores());
            }
            else
            {
                IAT_CadastrarTerminal.MS_AlterarStatusTerminal(stNumeroTerminal);
            }
        }
        private bool MI_VerificaStatusTerminal(string stNumeroTerminal)
        {
            bool boResult = true;

            CAT_CadastrarTerminal IAT_CadasatrarTerminal = new CAT_CadastrarTerminal();
            boResult = IAT_CadasatrarTerminal.MS_VerificaStatus(stNumeroTerminal);

            return boResult;
        }

        private void MI_AbrirConfiguracoes()
        {
            CLT_Configuracoes ABRIR = new CLT_Configuracoes();
            ABRIR.ShowDialog();
            MI_ObterNumeroTerminalControle();
        }

        private CTO_Terminal MI_ObterValores()
        {
            CTO_Terminal ITO_Terminal = new CTO_Terminal();
            ITO_Terminal.setTerminal(lblNumeroTerminal.Text);
            ITO_Terminal.setStatus("D");
            if (lblDirecao.Text.Equals("->"))
                ITO_Terminal.setDirecao("D");
            else
                ITO_Terminal.setDirecao("E");
            return ITO_Terminal;
        }
        private void MI_ObterNumeroTerminalControle()
        {
            DataTable dtXML = new DataTable("TERMINAL");
            DataRow dr;

            dtXML.Columns.Add("NUMERO", typeof(string));
            dtXML.Columns.Add("DIRECAO", typeof(string));
            dtXML.ReadXml("terminal.xml");

            dr = dtXML.Rows[0];
            lblNumeroTerminal.Text = dr.ItemArray[0].ToString();

            if (dr.ItemArray[1].ToString().Equals("D"))
                lblDirecao.Text = "->";
            else
                lblDirecao.Text = "<-";
        }

        private void CLT_DadosPessoais_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                e.Handled = true;
                SendKeys.Send("{tab}");
            }
        }

        private void edtNomeConsultora_TextChanged_1(object sender, EventArgs e)
        {
            string stNome = "";

            string urString = edtNomeConsultora.Text;
            string[] pieces = urString.Split(new char[] { ' ' });

            try
            {
                int it = pieces.Length;
                stNome = pieces[0].ToString().ToUpper();
                if (it > 1)
                    edtNomeCracha.Text = pieces[0].ToString().ToUpper() + " " + pieces[it - 1].ToString().ToUpper();
            }
            catch { }
        }

        private void edtCPF_Leave(object sender, EventArgs e)
        {
            if (edtCPF.Text.Trim() != string.Empty)
            {
                if (MI_ValidaCPF())
                {
                    MI_ObterPeloCPF(edtCPF.Text);
                }
            }
        }

        private void edtNomeCompleto_Leave(object sender, EventArgs e)
        {
            if (edtNomeConsultora.Text.Trim() != string.Empty)
                MI_ObterPeloNome(edtNomeConsultora.Text);
        }

        public bool ValidaEmail(string emailInformado)
        {
            //Verifica se tem @ e . no e-mail
            if (!emailInformado.Contains("@") || !emailInformado.Contains("."))
                return false;

            //Divide em antes e depois do @
            string[] campos = emailInformado.Split('@');

            //se tiver mais que 1 arroba, n�o � valido
            if (campos.Length != 2)
                return false;

            //se for menor que 4 caracteres, t� errado
            if (campos[0].Length < 2)
                return false;

            //Agora eu pego depois do arroba e divido os pontos
            if (!campos[1].Contains("."))
                return false;
            campos = campos[1].Split('.');

            //se for menor que 4, � falso
            if (campos[0].Length < 2)
                return false;

            //if (campos[1].Length < 3)
            //    return false;
            //se chegou aqui, pode ser verdadeiro
            return true;
        }

        private void edtConfig_Click(object sender, EventArgs e)
        {
            MI_AbrirConfiguracoes();
        }

        private void label31_Click(object sender, EventArgs e)
        { }
        private void controleDeEmiss�oDeEtiquetasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lblNivelAcesso.Text == "2")
            {
                lblTotal ABRIR = new lblTotal();
                ABRIR.ShowDialog();
            }
            else
            {
                MessageBox.Show("Usu�rio n�o autorizado", "SPI - Controle de acesso");
            }
        }

        private void atendimentoInternetToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void btNovaCompra_Click(object sender, EventArgs e)
        {
            string stItens = string.Empty;

            foreach (DataGridViewRow oRow in gdwPedidos.Rows)
            {
                stItens += oRow.Cells[6].Value.ToString();
            }
            if ((edtCodigoBarras.Text != string.Empty) && (edtNomeConsultora.ReadOnly == true))
            {
                CLT_NovaCompra ABRIR = new CLT_NovaCompra();
                ABRIR.AP_Produtos = stItens;
                ABRIR.AP_Nome = edtNomeConsultora.Text.ToUpper();
                ABRIR.AP_Codigo = edtCodigoBarras.Text.ToUpper();
                ABRIR.AP_Permissao = CSV_Global.getPrioridade();
                ABRIR.ShowDialog();

                CarregaTela();
            }
        }

        private void gdwPedidos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void btCapturarImagem_Click(object sender, EventArgs e)
        {
            MI_CapturarFoto();
        }

        private void MI_CapturarFoto()
        {
            if (edtCodigoBarras.Text.Trim() != string.Empty)
            {
                CLT_CameraFotografica ABRIR = new CLT_CameraFotografica();
                ABRIR.setCodigoVisitante(edtCodigoBarras.Text);
                ABRIR.ShowDialog();

                //picFoto.ImageLocation = CSV_Glogal.getLocalArmazenamentoFoto() + ABRIR.getCodigoVisitante() + ".jpg";
            }
        }

        private void buscarPorC�digoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CLT_LocalizarPorCodigo ABRIR = new CLT_LocalizarPorCodigo();
            ABRIR.ShowDialog();
            edtCodigoBarras.Text = ABRIR.AP_CodigoCartao;
            CarregaTela();
        }

        private void buscarPorC�digoImpress�oToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CLT_CodigoImpressao ABRIR = new CLT_CodigoImpressao();
            ABRIR.ShowDialog();
            edtCodigoBarras.Text = ABRIR.AP_CodigoImpressao;
            CarregaTela();
        }

        private void btCertificado_Click(object sender, EventArgs e)
        {
            if (MI_ConsistenciasCertificado())
            {
                if ((edtCodigoBarras.Text.Trim() != string.Empty) && (edtNomeConsultora.ReadOnly == true))
                {
                    CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
                    CTO_Visitante ITO_Visitante = IAT_Visitante.MS_ObterPorCodigo(edtCodigoBarras.Text.Trim());
                    if (ITO_Visitante != null)
                    {

                        bool bProduto3 = false;
                        foreach (DataGridViewRow Row in gdwPedidos.Rows)
                        {
                            if (Row.Cells[0].Value.ToString().ToUpper() != "3")
                            {
                                bProduto3 = true;
                            }
                        }

                        if (bProduto3)
                        {
                            CLT_ImprimiCertificadoPorProduto ABRIR = new CLT_ImprimiCertificadoPorProduto();
                            ABRIR.setVisitante(ITO_Visitante);
                            ABRIR.ShowDialog();
                        }
                        else
                        {
                            CSV_Certificado.MS_ImprimirCertificado(ITO_Visitante);
                            IAT_Visitante.MS_Alterar(ITO_Visitante);
                        }
                    }
                }
            }
        }

        private bool MI_ConsistenciasCertificado()
        {
            return true;
        }

        private DataTable MI_ObterProdutosComprados(int prCdCliente)
        {
            CAT_CadastrarItensPedido IAT_Pedido = new CAT_CadastrarItensPedido();
            return IAT_Pedido.MS_ObterProdutosCompradosPorCliente(prCdCliente);
        }

        private void btCertificadoLote_Click(object sender, EventArgs e)
        {
            CLT_ImpressaoLotes ABRIR = new CLT_ImpressaoLotes();
            ABRIR.ShowDialog();
        }

        private void relatoriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (CSV_Global.getPrioridade() == "1")
            //{
            //    CLT_Relatorio ABRIR = new CLT_Relatorio();
            //    ABRIR.ShowDialog();
            //}
            //else
            //{
            //    MessageBox.Show("Voc� n�o possui privilegios para acessar este item.", "Aten��o");
            //}

        }

        private void tmrRetirarJoia_Tick(object sender, EventArgs e)
        {
        }

        private void edtQuestao11_TextChanged(object sender, EventArgs e)
        {
        }

        private void imgCadeado_Click(object sender, EventArgs e)
        {
        }

        private void importarBancoDeDadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int positionAltura = 12;

            Bitmap bmp = new Bitmap(70, 70);

            Graphics h_LinhaFundo = Graphics.FromImage(bmp);
            h_LinhaFundo.FillRectangle(Brushes.Black, 0, 0, 70, 70);
            h_LinhaFundo.Dispose();

            Graphics h_Linha = Graphics.FromImage(bmp);
            h_Linha.FillRectangle(Brushes.White, 1,10, 68, 59);
            h_Linha.Dispose();

            // TIPO RECONHECIMENTO
            string prTipoReconhecimento = "PLATEIA";
            h_Linha = Graphics.FromImage(bmp);
            h_Linha.DrawString(prTipoReconhecimento, new System.Drawing.Font("Lucida Sans Typewriter", 7), Brushes.White, new Point((prTipoReconhecimento == "PALCO" ? 18 : 12), 0));
            h_Linha.Dispose();

            string prTexto = "PAULO MANOEL RODRIGUES JUNIOR";
            if (prTexto.Length <= 0)
            {
                h_Linha = Graphics.FromImage(bmp);
                h_Linha.DrawString(prTexto, new System.Drawing.Font("Lucida Sans Typewriter", 8), Brushes.Black, new Point(0, positionAltura));
                h_Linha.Dispose();
            }
            else
            {
                string stexto = "";
                for (int i = 0; i <= prTexto.Length-1; i++)
                {
                    stexto += prTexto.Substring(i, 1);
                    if (stexto.Length >= 10)
                    {
                        h_Linha = Graphics.FromImage(bmp);
                        h_Linha.DrawString(stexto, new System.Drawing.Font("Lucida Sans Typewriter", 8), Brushes.Black, new Point(0, positionAltura));
                        h_Linha.Dispose();
                        positionAltura += 10;
                        stexto = "";
                    }
                }

                if (stexto != string.Empty)
                {
                    h_Linha = Graphics.FromImage(bmp);
                    h_Linha.DrawString(stexto, new System.Drawing.Font("Lucida Sans Typewriter", 8), Brushes.Black, new Point(0, positionAltura));
                    h_Linha.Dispose();
                    positionAltura += 10;
                    stexto = "";
                }
            }

            try
            {
                Image img = (Image)bmp;
                pic1.Image = img;
                pic1.Visible = true;
            }
            catch
            { }
        }

        private void imprimir2ViaCart�oRealezaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (edtNomeConsultora.ReadOnly == true)
            {
                if (edtCodigoBarras.Text.Trim() != string.Empty)
                {
                    CSV_Global.setPrioridade("3");
                    MI_ImprimirCartaoRealeza(edtCodigoBarras.Text, "");
                }


            }

        }

        private void MI_ImprimirCartaoRealeza(string prCodigoBarras, string dsReconhecimento)
        {
            MI_ImprimirCredencial(Convert.ToInt64(prCodigoBarras), "", "", dsReconhecimento);
        }
        private void MI_ImprimirCredencial(Int64 itCodigo, string stProdutos, string stEvento, string dsReconhecimento)
        {
            bool boImprimirCodigo = true;
            CAT_ImprimirEtiqueta IAT_ImprimirEtiqueta = new CAT_ImprimirEtiqueta(itCodigo, boImprimirCodigo, stProdutos, "1", dsReconhecimento);
        }
    }
}