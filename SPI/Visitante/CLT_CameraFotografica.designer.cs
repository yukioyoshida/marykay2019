﻿namespace SPI
{
    partial class CLT_CameraFotografica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CLT_CameraFotografica));
            this.picFoto = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btCameraDigital = new System.Windows.Forms.Button();
            this.cmbDevices = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.picFoto)).BeginInit();
            this.SuspendLayout();
            // 
            // picFoto
            // 
            this.picFoto.BackColor = System.Drawing.Color.Black;
            this.picFoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFoto.Image = ((System.Drawing.Image)(resources.GetObject("picFoto.Image")));
            this.picFoto.Location = new System.Drawing.Point(0, 0);
            this.picFoto.Name = "picFoto";
            this.picFoto.Size = new System.Drawing.Size(338, 237);
            this.picFoto.TabIndex = 0;
            this.picFoto.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(-19, 232);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(391, 8);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // btCameraDigital
            // 
            this.btCameraDigital.BackColor = System.Drawing.Color.White;
            this.btCameraDigital.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btCameraDigital.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCameraDigital.ForeColor = System.Drawing.Color.Black;
            this.btCameraDigital.Image = ((System.Drawing.Image)(resources.GetObject("btCameraDigital.Image")));
            this.btCameraDigital.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btCameraDigital.Location = new System.Drawing.Point(98, 245);
            this.btCameraDigital.Name = "btCameraDigital";
            this.btCameraDigital.Size = new System.Drawing.Size(147, 38);
            this.btCameraDigital.TabIndex = 2;
            this.btCameraDigital.Text = "Capturar imagem";
            this.btCameraDigital.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btCameraDigital.UseVisualStyleBackColor = false;
            this.btCameraDigital.Click += new System.EventHandler(this.btCameraDigital_Click);
            // 
            // cmbDevices
            // 
            this.cmbDevices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDevices.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDevices.FormattingEnabled = true;
            this.cmbDevices.Location = new System.Drawing.Point(44, 21);
            this.cmbDevices.Name = "cmbDevices";
            this.cmbDevices.Size = new System.Drawing.Size(256, 20);
            this.cmbDevices.TabIndex = 3;
            this.cmbDevices.Visible = false;
            // 
            // CLT_CameraFotografica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(337, 288);
            this.Controls.Add(this.picFoto);
            this.Controls.Add(this.cmbDevices);
            this.Controls.Add(this.btCameraDigital);
            this.Controls.Add(this.groupBox1);
            this.ForeColor = System.Drawing.Color.DimGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CLT_CameraFotografica";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " Câmera Fotográfica";
            this.Load += new System.EventHandler(this.CLT_CameraFotografica_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picFoto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picFoto;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btCameraDigital;
        private System.Windows.Forms.ComboBox cmbDevices;

    }
}