using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ControleRemoto;

namespace SPI
{
    public partial class CLT_ModuloInternet : Form
    {
        public CLT_ModuloInternet()
        {
            InitializeComponent();
        }

        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        private void CLT_ModuloInternet_Load(object sender, EventArgs e)
        {
            CarregaUsuario();
            MI_ReiniciaTela();
            MI_ObterNumeroTerminalControle();
        }

        private void CarregaUsuario()
        {
            lblUsuario.Text = CSV_Global.getUsuario();
            lblTerminal.Text = CSV_Global.getNumeroTerminal();
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void MI_ReiniciaTela()
        {
            imgPainel.Size = new Size(imgPainel.Size.Width, 114);
            lblSair.Location = new Point(lblSair.Location.X, 240);   
            linha.Location = new Point(linha.Location.X, 232);   
            groupDados.Visible = false;
            groupControle.Visible = false;
            lblMensagem.Text = string.Empty;

            edtCodigo.Text = string.Empty;
        }

        private void MI_IniciaTelaTela()
        {
            imgPainel.Size = new Size(imgPainel.Size.Width, 315);
            lblSair.Location = new Point(lblSair.Location.X, 450);
            linha.Location = new Point(linha.Location.X, 441);
            groupDados.Visible = true;
            groupControle.Visible = true;
        }
        
        private void edtChave_KeyPress(object sender, KeyPressEventArgs e)
        {
            int itTecla = e.KeyChar;
            string stChave = "";
            if (itTecla.ToString() == "13")
            {
                try
                {   
                    if (edtChave.Text.Trim() != string.Empty)
                    {
                        if ((edtChave.Text.Length == 6) && (edtChave.Text.Substring(0, 1) == "0"))
                        {
                            MI_LocarlizarRegistro(edtChave.Text.Substring(1));
                        }
                        else
                        {
                            MI_LocarlizarRegistro(edtChave.Text);
                        }
                    }
                }
                catch
                { }
            }
        }

        private void MI_LocarlizarRegistro(string stChave)
        {
            DataTable dtRegistro = new DataTable();

            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            dtRegistro = IAT_Visitante.MS_ObterRegistroGrid(stChave);

            if (dtRegistro.Rows.Count > 0)
            {
                MI_RealizaBusca(dtRegistro);
            }
            else
            {
                lblMensagem.Text = "REGISTRO N�O ENCONTRADO!";
                MI_ReiniciaTela();
            }
        }

        private void MI_RealizaBusca(DataTable dtRegistros)
        {
            if ((edtChave.Text.Length == 6) && (edtChave.Text.Substring(0, 1) == "0"))
            {
                if (dtRegistros.Rows.Count > 1)
                {
                    MI_ObterDadoSelecionado(edtChave.Text.Substring(1));
                }
                else
                {
                    MI_ObterDadosDataTable(dtRegistros);
                }
            }
            else
            {
                if (dtRegistros.Rows.Count > 1)
                    MI_ObterDadoSelecionado(edtChave.Text);
                else
                    MI_ObterDadosDataTable(dtRegistros);   
            }
        }

        private void MI_ObterDadosDataTable(DataTable dtRegistros)
        {
            DataRow drLinha;
            drLinha = dtRegistros.Rows[0];

            edtCodigo.Text = drLinha["CodigoBarras"].ToString();
            edtCPF.Text = drLinha["CPF"].ToString();
            edtNomeCompleto.Text = drLinha["NomeCompleto"].ToString();
            edtRazaoSocial.Text = drLinha["RazaoSocial"].ToString();
            edtEmpresaCracha.Text = drLinha["EmpresaCracha"].ToString();
            edtCargo.Text = drLinha["Cargo"].ToString();

            edtChave.Text = string.Empty;

            MI_IniciaTelaTela();
            if (chkImpressaoAutomatico.Checked == true)
                MI_ImprimirCredencial();
        }

        private void MI_ObterDadoSelecionado(string stChave)
        {
            string stCodigoSelecionado = string.Empty;

            CLT_LocalizarRegistroInternet ABRIR = new CLT_LocalizarRegistroInternet();
            ABRIR.stChave = stChave;
            ABRIR.ShowDialog();
            stCodigoSelecionado = ABRIR.stCodigoBarras;

            if (stCodigoSelecionado != string.Empty)
            { 
                CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
                CTO_Visitante ITO_Visitante = IAT_Visitante.MS_ObterPorCodigo(stCodigoSelecionado);
                if (ITO_Visitante != null)
                {
                    MI_CarregaTelaCTO(ITO_Visitante);
                }
            }
        }

        private void MI_CarregaTelaCTO(CTO_Visitante ITO_Visitante)
        {
            //edtCodigo.Text = ITO_Visitante.getCodigoBarras();
            //edtCPF.Text = ITO_Visitante.getCPF();
            //edtNomeCompleto.Text = ITO_Visitante.getNomeCompleto();
            //edtRazaoSocial.Text = ITO_Visitante.getRazaoSocial();
            //edtEmpresaCracha.Text = ITO_Visitante.getNomeFantasia();
            //edtCargo.Text = ITO_Visitante.getCargo();

            //edtChave.Text = string.Empty;

            //MI_IniciaTelaTela();
            //if (chkImpressaoAutomatico.Checked == true)
            //    MI_ImprimirCredencial();
        }

        private void MI_ImprimirCredencial()
        {
            if (edtCodigo.Text != string.Empty)
            {
                CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();
                CTO_Visitante ITO_Visitante = IAT_CadastrarVisitante.MS_ObterPorCodigo(edtCodigo.Text);

                if (ITO_Visitante != null)
                    if (ITO_Visitante.getEmissaoCracha().ToString() == "S")
                    {
                        if (MessageBox.Show("A Credencial deste participante j� foi impressa.\n Deseja imprimir novamente?", "ATEN��O!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            MI_RegistraImpressaoCracha(edtCodigo.Text, lblUsuario.Text, "INTERNET");
                            MI_ImprimirCredencial(Convert.ToInt64(ITO_Visitante.getCodigoBarras()));
                        }
                    }
                    else
                    {
                        MI_RegistraImpressaoCracha(edtCodigo.Text, lblUsuario.Text, "INTERNET");
                        IAT_CadastrarVisitante.MS_ConfirmaImpressao(Convert.ToInt64(ITO_Visitante.getCodigoBarras()));
                        MI_ImprimirCredencial(Convert.ToInt64(ITO_Visitante.getCodigoBarras()));
                    }
            }
        }

        private void MI_RegistraImpressaoCracha(string stCodigoBarras, string stNomeUsuario, string stOrigem)
        {
            CAT_CadastrarEmissaoCracha IAT_EmissaoCracha = new CAT_CadastrarEmissaoCracha();
            CTO_EmissaoCracha ITO_Emissao = new CTO_EmissaoCracha();

            ITO_Emissao.setCodigoBarras(stCodigoBarras);
            ITO_Emissao.setUsuarioImpressao(stNomeUsuario);
            ITO_Emissao.setOrigemImpressao(stOrigem);

            IAT_EmissaoCracha.MS_Incluir(ITO_Emissao, "MODULO INTERNET");
        }

        private void MI_ImprimirCredencial(Int64 itCodigo)
        {
            //bool boImprimirCodigo = true;
            //CAT_ImprimirEtiqueta IAT_ImprimirEtiqueta = new CAT_ImprimirEtiqueta(itCodigo, boImprimirCodigo, "1");
        }

        private void btImprimirEtiqueta_Click(object sender, EventArgs e)
        {
            MI_ImprimirCredencial();
        }

        private void tmrRelogio_Tick(object sender, EventArgs e)
        {
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void btEditarDados_Click(object sender, EventArgs e)
        {
            MI_EditarRegistro();
        }

        private void MI_EditarRegistro()
        {
            CLT_DadosPessoais ABRIR = new CLT_DadosPessoais();
            ABRIR.stInternetHabilitada = "S";
            ABRIR.stCodigoInternet = edtCodigo.Text;
            ABRIR.ShowDialog();

            this.Close();
            //CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            //CTO_Visitante ITO_Visitante = IAT_Visitante.MS_ObterPorCodigo(edtCodigo.Text);
            //if (ITO_Visitante != null)
            //{
            //    MI_CarregaTelaCTO(ITO_Visitante);
            //}
            
        }

        private void btCharmarProximo_Click(object sender, EventArgs e)
        {
            MI_ChamarProximo(lblNumeroTerminal.Text); 
        }

        private void MI_ChamarProximo(string stNumeroTerminal)
        {
            CAT_CadastrarTerminal IAT_CadastrarTerminal = new CAT_CadastrarTerminal();

            if (MI_VerificaStatusTerminal(stNumeroTerminal).Equals(true))
            {
                IAT_CadastrarTerminal.MS_Incluir(MI_ObterValores());
            }
            else
            {
                IAT_CadastrarTerminal.MS_AlterarStatusTerminal(stNumeroTerminal);
            }
        }

        private CTO_Terminal MI_ObterValores()
        {
            CTO_Terminal ITO_Terminal = new CTO_Terminal();
            ITO_Terminal.setTerminal(lblNumeroTerminal.Text);
            ITO_Terminal.setStatus("D");
            if (lblDirecao.Text.Equals("->"))
                ITO_Terminal.setDirecao("D");
            else
                ITO_Terminal.setDirecao("E");
            return ITO_Terminal;
        }

        private bool MI_VerificaStatusTerminal(string stNumeroTerminal)
        {
            bool boResult = true;

            CAT_CadastrarTerminal IAT_CadasatrarTerminal = new CAT_CadastrarTerminal();
            boResult = IAT_CadasatrarTerminal.MS_VerificaStatus(stNumeroTerminal);

            return boResult;
        }

        private void MI_ObterNumeroTerminalControle()
        {
            DataTable dtXML = new DataTable("TERMINAL");
            DataRow dr;

            dtXML.Columns.Add("NUMERO", typeof(string));
            dtXML.Columns.Add("DIRECAO", typeof(string));
            dtXML.ReadXml("terminal.xml");

            dr = dtXML.Rows[0];
            lblNumeroTerminal.Text = dr.ItemArray[0].ToString();

            if (dr.ItemArray[1].ToString().Equals("D"))
                lblDirecao.Text = "->";
            else
                lblDirecao.Text = "<-";
        }

        private void CLT_ModuloInternet_KeyPress(object sender, KeyPressEventArgs e)
        {
            int itTecla = e.KeyChar;
            switch (itTecla.ToString())
            {
                case "27":
                    this.Close();
                    break;
            }
        }

        private void CLT_ModuloInternet_KeyDown(object sender, KeyEventArgs e)
        {
            string stTecla = e.KeyCode.ToString();
            switch (stTecla)
            {
                case "F4":
                    MI_ImprimirCredencial();
                    break;
            }
        }

        private void edtChave_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblSair_Click(object sender, EventArgs e)
        {

        }

        private void lblMensagem_Click(object sender, EventArgs e)
        {

        }

        private void imgPainel_Click(object sender, EventArgs e)
        {

        }
    }
}