using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_RetirarJoia : Form
    {
        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        public string AP_stCodigoBarras { get; set; }
        public string AP_stNome1 { get; set; }
        public string AP_stNome2 { get; set; }
        public string AP_stCdPrimiacao { get; set; }


        public CLT_RetirarJoia()
        {
            InitializeComponent();
        }

        private void CLT_LocalizarRegistro_Load(object sender, EventArgs e)
        {
            CarregaUsuario();
            CarregarVariaveis();
        }

        private void CarregaUsuario()
        {
            lblUsuario.Text = CSV_Global.getUsuario();
            lblTerminal.Text = CSV_Global.getNumeroTerminal();
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void CarregarVariaveis()
        {
            lblCodigo.Text = AP_stCodigoBarras;
            lblNome1.Text = AP_stNome1;
            lblNome2.Text = AP_stNome2;

            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();

            DataTable dtObterPremiacao = IAT_Visitante.MS_ObterPremioDaConsultora(AP_stCdPrimiacao);
            DataTable dtObterPremio = IAT_Visitante.MS_ObterPremio(dtObterPremiacao.Rows[0]["cdPremio"].ToString());

            imgProduto.Image = System.Drawing.Image.FromFile("ImagensPremios/" + dtObterPremio.Rows[0]["dsFoto"].ToString() + "");
            lblNomeProduto.Text = dtObterPremio.Rows[0]["dsFoto"].ToString().Replace("_", " ").Replace(".jpg", "").Replace("semImagem.jpg", "");


            // Textos

            lblTitulo.Text = lblTitulo.Text.Replace("[NOME]", MI_ObterNomeRegistrado(AP_stCodigoBarras));
            lblTitulo.Text = lblTitulo.Text.Replace("[CORTE]", dtObterPremio.Rows[0]["Vinculado"].ToString());

            lblAceite.Text = lblAceite.Text.Replace("[NOME]", MI_ObterNomeRegistrado(AP_stCodigoBarras));
            lblAceite.Text = lblAceite.Text.Replace("[NRCONSULTORA]", dtObterPremiacao.Rows[0]["NrConsultora"].ToString());
            lblAceite.Text = lblAceite.Text.Replace("[DIAMES]", System.DateTime.Now.Day.ToString() + "/" + System.DateTime.Now.Month.ToString());


        }

        private string MI_ObterNomeRegistrado(string prCodigo)
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            CTO_Visitante ITO_Visitante = IAT_Visitante.MS_ObterPorCodigo(prCodigo);

            return ITO_Visitante.getNomedaConsultora();
        }

        private void CLT_LocalizarRegistro_KeyPress(object sender, KeyPressEventArgs e)
        {
            int itTecla = e.KeyChar;
            switch (itTecla.ToString())
            {
                case "27":
                    this.Close();
                    break;
            }
        }

        private void btoNaoAceito_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btoAceito_Click(object sender, EventArgs e)
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            IAT_Visitante.MS_ConfirmarRetiradaPremio(AP_stCdPrimiacao, lblUsuario.Text);

            CLT_ConfirmaOperacaoJoia open = new CLT_ConfirmaOperacaoJoia();

            open.AP_stCodigoBarras = AP_stCodigoBarras;
            open.AP_stNome1 = AP_stNome1;
            open.AP_stNome2 = AP_stNome2;
            open.AP_stCdPrimiacao = AP_stCdPrimiacao;

            open.ShowDialog();

            this.Close();
        }



    }
}