using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_LocalizarRegistroInternet : Form
    {
        public CLT_LocalizarRegistroInternet()
        {
            InitializeComponent();
        }

        public string stChave = string.Empty;
        public string stCodigoBarras = string.Empty;

        private void CLT_LocalizarRegistroInternet_Load(object sender, EventArgs e)
        {
            MI_LocalizarRegistros();
        }

        private void MI_LocalizarRegistros()
        {
            DataTable dtRegistro = new DataTable();
            DataTable dtFinal = new DataTable();
            DataRow drLinha;
            DataRow drLinhaInsert;

            dtFinal.Columns.Add("CODIGO", typeof(string));
            dtFinal.Columns.Add("CPF", typeof(string));
            dtFinal.Columns.Add("NOME", typeof(string));
            dtFinal.Columns.Add("EMPRESA", typeof(string));
            dtFinal.Columns.Add("CARGO", typeof(string));
            
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            dtRegistro = IAT_Visitante.MS_ObterRegistroGrid(stChave);

            for (int i = 0; dtRegistro.Rows.Count - 1 >= i; i++)
            {
                drLinha = dtRegistro.Rows[i];
                drLinhaInsert = dtFinal.NewRow();
                    drLinhaInsert["CODIGO"] = drLinha["CodigoBarras"].ToString();
                    drLinhaInsert["CPF"] = drLinha["CPF"].ToString();
                    drLinhaInsert["NOME"] = drLinha["NomeCompleto"].ToString();
                    drLinhaInsert["EMPRESA"] = drLinha["RazaoSocial"].ToString();
                    drLinhaInsert["CARGO"] = drLinha["Cargo"].ToString();
                    dtFinal.Rows.Add(drLinhaInsert[0], drLinhaInsert[1], drLinhaInsert[2], drLinhaInsert[3], drLinhaInsert[4]); 
                drLinhaInsert.EndEdit(); 
            }

            gdwRegistros.DataSource = dtFinal;

            gdwRegistros.Columns[0].Visible = false;
            gdwRegistros.Columns[1].Width = 100;
            gdwRegistros.Columns[2].Width = 219;
            gdwRegistros.Columns[3].Width = 200;
            gdwRegistros.Columns[4].Width = 100;
        }

        private void gdwRegistros_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            stCodigoBarras = gdwRegistros.CurrentRow.Cells[0].Value.ToString();
            Close();
        }

        private void gdwRegistros_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (gdwRegistros.Rows.Count > 0)
                {
                    stCodigoBarras = gdwRegistros.CurrentRow.Cells[0].Value.ToString();
                    Close();
                }
            }
        }

        private void CLT_LocalizarRegistroInternet_KeyPress(object sender, KeyPressEventArgs e)
        {
            int itTecla = e.KeyChar;
            switch (itTecla.ToString())
            {
                case "13":
                    e.Handled = true;
                    SendKeys.Send("{tab}");
                    break;
                case "27":
                    this.Close();
                    break;
            }
        }
    }
}