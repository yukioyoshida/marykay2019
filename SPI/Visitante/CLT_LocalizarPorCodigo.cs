﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_LocalizarPorCodigo : Form
    {
        public string AP_CodigoCartao;

        public DataTable dtTodosRegistros;
        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        public string stCodigoBarras = "";
        public string stMetodoBusca = "";
        public string stPalavraChave = "";

        public CLT_LocalizarPorCodigo()
        {
            InitializeComponent();
        }

        private void CLT_LocalizarPorCodigo_Load(object sender, EventArgs e)
        {

        }

        private string MI_LocalizaPeloCodigoCartao(string prstCodigo)
        {
            string stCodigoBarras = string.Empty;
            string stContendo = "0";
            
            //if (prstCodigo.Length > 6)
            //{
                CAT_CadastrarVisitante IAT_Codigos = new CAT_CadastrarVisitante();
                CTO_Visitante ITO_Visitante = IAT_Codigos.MS_ObterPorCodigo(prstCodigo);

                if (ITO_Visitante != null)
                {
                    if (ITO_Visitante.getCodigoBarras() != string.Empty)
                    {
                        AP_CodigoCartao = ITO_Visitante.getCodigoBarras();
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Registro não encontrado", "");
                    }
                }
                else
                {
                    MessageBox.Show("Código inexistente", "");
                }
            //}
            
            return stCodigoBarras;
        }

        

        private void edtPalavra_TextChanged(object sender, EventArgs e)
        {
            //MI_LocalizaPeloCodigoCartao(edtPalavra.Text);
            //Close();
        }

        private void edtPalavra_Leave(object sender, EventArgs e)
        {
            //MI_LocalizaPeloCodigoCartao(edtPalavra.Text);
            //Close();
        }

        private void edtPalavra_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                MI_LocalizaPeloCodigoCartao(edtPalavra.Text);
            }
        }
    }
}
