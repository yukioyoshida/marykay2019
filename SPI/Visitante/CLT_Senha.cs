﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_Senha : Form
    {

        public bool AP_Result { get; set; }
        public string AP_Motivo { get; set; }

        public CLT_Senha()
        {
            InitializeComponent();
        }

        private void CLT_Senha_Load(object sender, EventArgs e)
        {
            AP_Result = false;
        }

        private void MI_VerificarSenha(string stSenha)
        {   
            CSV_VariaveisGlobal Global = new CSV_VariaveisGlobal();
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtSenha = IAT_Visitante.MS_ObterSenha(stSenha);

            if (dtSenha.Rows[0][0].ToString() == edtSenha.Text)
            {
                AP_Result = true;
                AP_Motivo = "";

                if (rbtPerda.Checked == true)
                {
                    AP_Motivo = "Perda de credencial";
                }
                if (rbtPrimeiraImpressao.Checked == true)
                {
                    AP_Motivo = "Erro na 1ª impressão";
                }

                this.Close();
            }
            else
            {
                MessageBox.Show("Senha Inválida", "ATENÇÃO!");
                AP_Result = false;
                this.Close();
            }
        }

        private void btAcessar_Click(object sender, EventArgs e)
        {
            if (rbtPerda.Checked == false && rbtPrimeiraImpressao.Checked == false)
            {
                MessageBox.Show("Informe o motivo!", "Atenção");
                return;
            }
            else
            {
                MI_VerificarSenha(edtSenha.Text.Trim());
            }
        }

        private void btoCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
