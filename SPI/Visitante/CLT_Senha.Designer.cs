﻿namespace SPI
{
    partial class CLT_Senha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.edtSenha = new System.Windows.Forms.TextBox();
            this.btAcessar = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtPrimeiraImpressao = new System.Windows.Forms.RadioButton();
            this.rbtPerda = new System.Windows.Forms.RadioButton();
            this.btoCancelar = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 201);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Senha do supervisor";
            // 
            // edtSenha
            // 
            this.edtSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtSenha.Location = new System.Drawing.Point(183, 198);
            this.edtSenha.Name = "edtSenha";
            this.edtSenha.PasswordChar = '*';
            this.edtSenha.Size = new System.Drawing.Size(338, 26);
            this.edtSenha.TabIndex = 1;
            // 
            // btAcessar
            // 
            this.btAcessar.Location = new System.Drawing.Point(417, 13);
            this.btAcessar.Name = "btAcessar";
            this.btAcessar.Size = new System.Drawing.Size(118, 48);
            this.btAcessar.TabIndex = 2;
            this.btAcessar.Text = "Liberar";
            this.btAcessar.UseVisualStyleBackColor = true;
            this.btAcessar.Click += new System.EventHandler(this.btAcessar_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.btoCancelar);
            this.panel1.Controls.Add(this.btAcessar);
            this.panel1.Location = new System.Drawing.Point(-14, 290);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(573, 98);
            this.panel1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(268, 24);
            this.label2.TabIndex = 4;
            this.label2.Text = "Esta credencial já foi impressa.";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(526, 56);
            this.label3.TabIndex = 5;
            this.label3.Text = "Para reimprimir, por favor selecione uma das opções abaixo:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtPerda);
            this.groupBox1.Controls.Add(this.rbtPrimeiraImpressao);
            this.groupBox1.Location = new System.Drawing.Point(17, 89);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(504, 94);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // rbtPrimeiraImpressao
            // 
            this.rbtPrimeiraImpressao.AutoSize = true;
            this.rbtPrimeiraImpressao.Location = new System.Drawing.Point(17, 19);
            this.rbtPrimeiraImpressao.Name = "rbtPrimeiraImpressao";
            this.rbtPrimeiraImpressao.Size = new System.Drawing.Size(122, 17);
            this.rbtPrimeiraImpressao.TabIndex = 0;
            this.rbtPrimeiraImpressao.TabStop = true;
            this.rbtPrimeiraImpressao.Text = "Erro na 1ª impressão";
            this.rbtPrimeiraImpressao.UseVisualStyleBackColor = true;
            // 
            // rbtPerda
            // 
            this.rbtPerda.AutoSize = true;
            this.rbtPerda.Location = new System.Drawing.Point(17, 56);
            this.rbtPerda.Name = "rbtPerda";
            this.rbtPerda.Size = new System.Drawing.Size(120, 17);
            this.rbtPerda.TabIndex = 1;
            this.rbtPerda.TabStop = true;
            this.rbtPerda.Text = "Perda de credencial";
            this.rbtPerda.UseVisualStyleBackColor = true;
            // 
            // btoCancelar
            // 
            this.btoCancelar.Location = new System.Drawing.Point(35, 13);
            this.btoCancelar.Name = "btoCancelar";
            this.btoCancelar.Size = new System.Drawing.Size(118, 48);
            this.btoCancelar.TabIndex = 7;
            this.btoCancelar.Text = "Cancelar";
            this.btoCancelar.UseVisualStyleBackColor = true;
            this.btoCancelar.Click += new System.EventHandler(this.btoCancelar_Click);
            // 
            // CLT_Senha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 363);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.edtSenha);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CLT_Senha";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Liberação";
            this.Load += new System.EventHandler(this.CLT_Senha_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edtSenha;
        private System.Windows.Forms.Button btAcessar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbtPerda;
        private System.Windows.Forms.RadioButton rbtPrimeiraImpressao;
        private System.Windows.Forms.Button btoCancelar;
    }
}