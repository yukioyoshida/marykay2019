﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_ImpressaoLotes : Form
    {
        public CLT_ImpressaoLotes()
        {
            InitializeComponent();
        }

        private void CLT_ImpressaoLotes_Load(object sender, EventArgs e)
        {

        }

        private bool MI_ConsistenciasCertificado(DataTable dtRegistros)
        {

            //if (dtRegistros.Rows.Count == 0)
            //{
            //    MessageBox.Show("Itens não encontrados.", "Atenção");
            //    return false;
            //}

            //foreach (DataGridViewRow Row in dtRegistros.Rows)
            //{
            //    if (Row.Cells[1].Value.ToString().ToUpper() != "PAGO")
            //    {
            //        MessageBox.Show("Cliente com pendencia de pagamento", "Atenção");
            //        return false;
            //    }
            //}

            return true;
        }

        private void MI_GerarCertificado(int stLimite)
        {
            //CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            //DataTable dtRegistros = IAT_Visitante.MS_ObterTodosParticipantesCertificados(stLimite);

            //if (dtRegistros.Rows.Count == 0)
            //{
            //    MessageBox.Show("Não há registros para imprimir");
            //    return;
            //}

            //foreach (DataRow Row in dtRegistros.Rows)
            //{
            //    if (MI_ConsistenciasCertificado(dtRegistros))
            //    {
            //        CAT_CadastrarVisitante IAT_Visitante2 = new CAT_CadastrarVisitante();
            //        CTO_Visitante ITO_Visitante = IAT_Visitante2.MS_ObterPorCodigo(Row.ItemArray[2].ToString());
            //        if (ITO_Visitante != null)
            //        {
            //            CAT_CadastrarPedido IAT_Pedido = new CAT_CadastrarPedido();
            //            DataTable DtPedido = IAT_Pedido.MS_ObterPedidosPorPessoa(ITO_Visitante.getObjRef());

            //            if (DtPedido.Rows.Count == 0)
            //            {   }
            //            else
            //            {
            //                CSV_Certificado.MS_ImprimirCertificado(ITO_Visitante);
            //                //ITO_Visitante.setPergunta20("S");
            //                //IAT_Visitante.MS_Alterar(ITO_Visitante);
            //            }
            //        }
            //    }
            //}

            ////ROTINA PARA MARCAR IMPRESSO
            //if (MessageBox.Show("Os certificados foram impressos corretamente?", "ATENÇÃO", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //{
            //    foreach (DataRow Row in dtRegistros.Rows)
            //    {
            //        if (MI_ConsistenciasCertificado(dtRegistros))
            //        {
            //            CAT_CadastrarVisitante IAT_Visitante2 = new CAT_CadastrarVisitante();
            //            CTO_Visitante ITO_Visitante = IAT_Visitante2.MS_ObterPorCodigo(Row.ItemArray[2].ToString());
            //            if (ITO_Visitante != null)
            //            {
            //                CAT_CadastrarPedido IAT_Pedido = new CAT_CadastrarPedido();
            //                DataTable DtPedido = IAT_Pedido.MS_ObterPedidosPorPessoa(ITO_Visitante.getObjRef());

            //                if (DtPedido.Rows.Count == 0)
            //                { }
            //                else
            //                {
            //                    //CSV_Certificado.MS_ImprimirCertificado(ITO_Visitante);
            //                    ITO_Visitante.setPergunta20("S");
            //                    IAT_Visitante.MS_Alterar(ITO_Visitante);
            //                }
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("Clique em Iniciar impressão para reimprimir", "ATENÇÃO");
            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (edtLimite.Text == string.Empty)
            {
                MessageBox.Show("Digite um limite de impressões!", "Atenção.");
                edtLimite.Text = string.Empty;
                edtLimite.Focus();
                return;
            }

            try
            {
                Convert.ToInt32(edtLimite.Text);
                MI_GerarCertificado(Convert.ToInt32(edtLimite.Text));
            }
            catch
            {
                MessageBox.Show("Entre apenas com números.", "Atenção!");
                edtLimite.Text = string.Empty;
                edtLimite.Focus();
                return;
            }
        }
    }
}
