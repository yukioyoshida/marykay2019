﻿namespace SPI
{
    partial class CLT_ItemImpressao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btDia4 = new System.Windows.Forms.Button();
            this.btDia5 = new System.Windows.Forms.Button();
            this.btDia6 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btDia4
            // 
            this.btDia4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDia4.Location = new System.Drawing.Point(37, 52);
            this.btDia4.Name = "btDia4";
            this.btDia4.Size = new System.Drawing.Size(219, 54);
            this.btDia4.TabIndex = 0;
            this.btDia4.Text = "04 / 04 / 2012";
            this.btDia4.UseVisualStyleBackColor = true;
            this.btDia4.Click += new System.EventHandler(this.btDia4_Click);
            // 
            // btDia5
            // 
            this.btDia5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDia5.Location = new System.Drawing.Point(37, 127);
            this.btDia5.Name = "btDia5";
            this.btDia5.Size = new System.Drawing.Size(219, 54);
            this.btDia5.TabIndex = 1;
            this.btDia5.Text = "05 / 04 / 2012";
            this.btDia5.UseVisualStyleBackColor = true;
            this.btDia5.Click += new System.EventHandler(this.btDia5_Click);
            // 
            // btDia6
            // 
            this.btDia6.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDia6.Location = new System.Drawing.Point(37, 204);
            this.btDia6.Name = "btDia6";
            this.btDia6.Size = new System.Drawing.Size(219, 54);
            this.btDia6.TabIndex = 2;
            this.btDia6.Text = "06 / 04 / 2012";
            this.btDia6.UseVisualStyleBackColor = true;
            this.btDia6.Click += new System.EventHandler(this.btDia6_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 14);
            this.label1.TabIndex = 3;
            this.label1.Text = "Selecione o dia que deseja Imprimir";
            // 
            // CLT_ItemImpressao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 274);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btDia6);
            this.Controls.Add(this.btDia5);
            this.Controls.Add(this.btDia4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CLT_ItemImpressao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " Selecione";
            this.Load += new System.EventHandler(this.CLT_ItemImpressao_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btDia4;
        private System.Windows.Forms.Button btDia5;
        private System.Windows.Forms.Button btDia6;
        private System.Windows.Forms.Label label1;
    }
}