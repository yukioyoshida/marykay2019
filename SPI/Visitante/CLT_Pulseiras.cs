﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_Pulseiras : Form
    {
        public CLT_Pulseiras()
        {
            InitializeComponent();
        }

        public bool AP_Congresso { get; set; }
        public bool AP_Curso1 { get; set; }
        public bool AP_Curso2 { get; set; }
        public bool AP_Curso3 { get; set; }
        public bool AP_Curso4 { get; set; }
        public bool AP_Curso5 { get; set; }
        public bool AP_Curso6 { get; set; }
        public bool AP_Curso7 { get; set; }

        public string AP_Categoria { get; set; }

        private void CLT_Pulseiras_Load(object sender, EventArgs e)
        {
            MI_ExibirPulseiras();
            MI_ExibeCategoria();
        }

        public void MI_ExibirPulseiras(){

            //if (AP_Curso1 == true)
            //    imgCurso1.Visible = true;

            if (AP_Curso2 == true)
                imgCurso2.Visible = true;

            if (AP_Curso3 == true)
                imgCurso3.Visible = true;

            //if (AP_Curso4 == true)
            //    imgCurso4.Visible = true;

            //if (AP_Curso5 == true)
            //    imgCurso5.Visible = true;

            //if (AP_Curso6 == true)
            //    imgCurso6.Visible = true;

            //if (AP_Curso7 == true)
            //    imgCurso7.Visible = true;
        }

        public void MI_ExibeCategoria()
        {
            if (AP_Congresso == true)
                lblCategoria.Text = "CONGRESSISTA";
            else
                lblCategoria.Text = "VISITANTE";
        }

        private void btFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tmrCategoria_Tick(object sender, EventArgs e)
        {
            lblCategoria.Visible = !lblCategoria.Visible;
        }

        private void lblCategoria_Click(object sender, EventArgs e)
        {

        }

    }
}
