using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ControleRemoto
{
    public partial class CLT_Configuracoes : Form
    {
        public CLT_Configuracoes()
        {
            InitializeComponent();
        }

        private void CLT_Configuracoes_Load(object sender, EventArgs e)
        {
            MI_CarregaDirecao();
            MI_CarregaNumeroTerminal();
            MI_CarregaConfiguracoes();
        }

        private void MI_CarregaDirecao()
        {
            cmbDirecao.Items.Add("<-");
            cmbDirecao.Items.Add("->");   
        }
        private void MI_CarregaNumeroTerminal()
        {
            for (int i = 1; i < 31; i++)
            {
                if (i<10)
                    cmbNumeros.Items.Add("0" + i.ToString());  
                else
                    cmbNumeros.Items.Add(i.ToString());  
            }
            MI_ObterNumeroTerminal(); 
        }

        private void MI_ObterNumeroTerminal()
        {
            DataTable dtXML = new DataTable("TERMINAL");
            DataRow dr;

            dtXML.Columns.Add("NUMERO", typeof(string));
            dtXML.Columns.Add("DIRECAO", typeof(string));
            dtXML.ReadXml("terminal.xml");

            dr = dtXML.Rows[0];
            cmbNumeros.Text = dr.ItemArray[0].ToString();
            if (dr.ItemArray[1].ToString().Equals("D"))
                cmbDirecao.Text = "->";
            else
                cmbDirecao.Text = "<-";
        }

        private void MI_CarregaConfiguracoes()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            try
            {
                dt.Columns.Add("IP", typeof(string));
                dt.Columns.Add("USUARIO", typeof(string));
                dt.Columns.Add("SENHA", typeof(string));
                dt.Columns.Add("BANCO", typeof(string));
                dt.Columns.Add("TERMINAL", typeof(string));

                dt.ReadXml("ConfiguracoesControle.xml");

                dr = dt.Rows[0];

                edtIP.Text = Convert.ToString((dr.ItemArray[0]));
                edtUser.Text = Convert.ToString((dr.ItemArray[1]));
                edtPass.Text = Convert.ToString((dr.ItemArray[2]));
                edtDatabase.Text = Convert.ToString((dr.ItemArray[3]));
            }
            catch
            {
                MessageBox.Show("Arquivo de configura��o n�o encontrado", "ATEN��O!");
            }
        }

        private void MI_AlteraXML()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));


            dr = dt.NewRow();

            dr[0] = edtIP.Text;
            dr[1] = edtUser.Text;
            dr[2] = edtPass.Text;
            dr[3] = edtDatabase.Text;

            dr.EndEdit();

            dt.Rows.Add(dr[0], dr[1], dr[2], dr[3]);

            dt.WriteXml("ConfiguracoesControle.xml");
            MessageBox.Show("Configura��o realizada com sucesso.", "Confirma��o");
        }

        private void MI_AlteraNumeroTermianalXML()
        {
            DataTable dt = new DataTable("TERMINAL");
            DataRow dr;

            dt.Columns.Add("NUMERO", typeof(string));
            dt.Columns.Add("DIRECAO", typeof(string));
            dr = dt.NewRow();

            dr[0] = cmbNumeros.Text;
            if (cmbDirecao.Text.Equals("->"))
                dr[1] = "D";
            else
                dr[1] = "E";
            
            dr.EndEdit();

            dt.Rows.Add(dr[0], dr[1]);
            dt.WriteXml("TERMINAL.xml");
        }

        private void btSalvar_Click(object sender, EventArgs e)
        {
            MI_AlteraNumeroTermianalXML();
            MI_AlteraXML();
            Close();
        }
    }
}