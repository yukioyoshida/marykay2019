﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_ConexaoServidor : Form
    {
        public CLT_ConexaoServidor()
        {
            InitializeComponent();
        }

        private void CLT_ConexaoServidor_Load(object sender, EventArgs e)
        {
            MI_CarregaConfiguracoes();
        }

        private void CLT_ConexaoServidor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                e.Handled = true;
                SendKeys.Send("{tab}");
            }
        }

        private void MI_CarregaConfiguracoes()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            try
            {
                dt.Columns.Add("IP", typeof(string));
                dt.Columns.Add("USUARIO", typeof(string));
                dt.Columns.Add("SENHA", typeof(string));
                dt.Columns.Add("BANCO", typeof(string));
                dt.Columns.Add("TERMINAL", typeof(string));
                dt.Columns.Add("ARMAZENAMENTO", typeof(string));
                dt.Columns.Add("SEMINARIO", typeof(string));
                dt.Columns.Add("TIPOJOIA", typeof(string));

                dt.ReadXml("configuracoes.xml");

                dr = dt.Rows[0];

                edtIP.Text = Convert.ToString((dr["IP"]));
                edtUsuario.Text = Convert.ToString((dr["USUARIO"]));
                edtSenha.Text = Convert.ToString((dr["SENHA"]));
                edtDatabase.Text = Convert.ToString((dr["BANCO"]));
                edtTerminal.Text = Convert.ToString((dr["TERMINAL"]));
                edtCaminhoFoto.Text = Convert.ToString((dr["ARMAZENAMENTO"]));
            }
            catch
            {
                MessageBox.Show("Arquivo de configuração não encontrado", "ATENÇÃO!");
            }
        }

        private void MI_AlteraXML()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));
            dt.Columns.Add("TERMINAL", typeof(string));
            dt.Columns.Add("ARMAZENAMENTO", typeof(string));
            dt.Columns.Add("SEMINARIO", typeof(string));

            dr = dt.NewRow();

            dr[0] = edtIP.Text;
            dr[1] = edtUsuario.Text;
            dr[2] = edtSenha.Text;
            dr[3] = edtDatabase.Text;
            dr[4] = edtTerminal.Text;
            dr[5] = edtCaminhoFoto.Text;
            dr[6] = new DAO_Visitante().MS_ObterSeminario();

            dr.EndEdit();

            dt.Rows.Add(dr[0], dr[1], dr[2], dr[3], dr[4], dr[5], dr[6]);

            dt.WriteXml("configuracoes.xml");
            MessageBox.Show("Configuração realizada com sucesso.", "Confirmação");
        }

        private void btSalvar_Click(object sender, EventArgs e)
        {
            if (MI_Consistencias())
            {
                MI_AlteraXML();
                this.Close();
            }
        }

        private bool MI_Consistencias()
        {
            bool boResult = true;

            if (edtIP.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Informe o Endereço IP", "Atenção!");
                return boResult = false;
            }
            if (edtUsuario.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Informe o usuário", "Atenção!");
                return boResult = false;
            }
            if (edtSenha.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Informe a senha", "Atenção!");
                return boResult = false;
            }
            if (edtDatabase.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Informe o nome do banco de dados", "Atenção!");
                return boResult = false;
            }
            if (edtTerminal.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Informe o número do terminal", "Atenção!");
                return boResult = false;
            }

            return boResult;
        }
    }
}
