using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_Conexao : Form
    {
        public CLT_Conexao()
        {
            InitializeComponent();
        }

        private void CLT_Conexao_Load(object sender, EventArgs e)
        {
            MI_CarregaConfiguracoes();
        }

        private void MI_CarregaConfiguracoes()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            try
            {
                dt.Columns.Add("IP", typeof(string));
                dt.Columns.Add("USUARIO", typeof(string));
                dt.Columns.Add("SENHA", typeof(string));
                dt.Columns.Add("BANCO", typeof(string));
                dt.Columns.Add("TERMINAL", typeof(string));

                dt.ReadXml("configuracoes.xml");

                dr = dt.Rows[0];

                edtIP.Text = Convert.ToString((dr.ItemArray[0]));
                edtUsuario.Text = Convert.ToString((dr.ItemArray[1]));
                edtSenha.Text = Convert.ToString((dr.ItemArray[2]));
                edtBase.Text = Convert.ToString((dr.ItemArray[3]));
                edtNumeroTerminal.Text = Convert.ToString((dr.ItemArray[4]));
            }
            catch
            {
                MessageBox.Show("Arquivo de configura��o n�o encontrado","ATEN��O!");  
            }
        }

        private void btSalvar_Click(object sender, EventArgs e)
        {
            if (MI_Consistencias())
            {
                MI_AlteraXML();
                Close();
            }
        }
        private bool MI_Consistencias()
        {
            bool boReturn = true;

            if (edtIP.Text == string.Empty)
            {
                MessageBox.Show("Informe o IP!", "ATEN��O!"); 
                boReturn = false;
                return boReturn;
            }
            if (edtUsuario.Text == string.Empty)
            {
                MessageBox.Show("Informe o usu�rio!", "ATEN��O!");
                boReturn = false;
                return boReturn;
            }
            if (edtSenha.Text == string.Empty)
            {
                MessageBox.Show("Informe a senha!", "ATEN��O!");
                boReturn = false;
                return boReturn;
            }
            if (edtBase.Text == string.Empty)
            {
                MessageBox.Show("Informe o Banco de dados!", "ATEN��O!");
                boReturn = false;
                return boReturn;
            }
            if (edtNumeroTerminal.Text == string.Empty)
            {
                MessageBox.Show("Informe o n�mero do terminal!", "ATEN��O!");
                boReturn = false;
                return boReturn;
            }

            return boReturn; 
        }

        private void MI_AlteraXML()
        {
            DataTable dt = new DataTable("CONEXAO");
            DataRow dr;

            dt.Columns.Add("IP", typeof(string));
            dt.Columns.Add("USUARIO", typeof(string));
            dt.Columns.Add("SENHA", typeof(string));
            dt.Columns.Add("BANCO", typeof(string));
            dt.Columns.Add("TERMINAL", typeof(string));
            

            dr = dt.NewRow();

            dr[0] = edtIP.Text;
            dr[1] = edtUsuario.Text;
            dr[2] = edtSenha.Text;
            dr[3] = edtBase.Text;
            dr[4] = edtNumeroTerminal.Text;

            dr.EndEdit();

            dt.Rows.Add(dr[0], dr[1], dr[2], dr[3], dr[4]);

            dt.WriteXml("configuracoes.xml");
            MessageBox.Show("Configura��o realizada com sucesso.", "Confirma��o");
        }
    }
}