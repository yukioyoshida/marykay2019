﻿namespace SPI
{
    partial class Clt_AssinaturaDigital
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pcAssinatura = new System.Windows.Forms.PictureBox();
            this.btnLimparAssinatura = new System.Windows.Forms.Button();
            this.btnGravaAssinatura = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pcAssinatura)).BeginInit();
            this.SuspendLayout();
            // 
            // pcAssinatura
            // 
            this.pcAssinatura.BackColor = System.Drawing.Color.White;
            this.pcAssinatura.Location = new System.Drawing.Point(7, 7);
            this.pcAssinatura.Margin = new System.Windows.Forms.Padding(4);
            this.pcAssinatura.Name = "pcAssinatura";
            this.pcAssinatura.Size = new System.Drawing.Size(1439, 459);
            this.pcAssinatura.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcAssinatura.TabIndex = 1;
            this.pcAssinatura.TabStop = false;
            this.pcAssinatura.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pcAssinatura_MouseDown);
            this.pcAssinatura.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pcAssinatura_MouseMove);
            this.pcAssinatura.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pcAssinatura_MouseUp);
            // 
            // btnLimparAssinatura
            // 
            this.btnLimparAssinatura.BackColor = System.Drawing.Color.Red;
            this.btnLimparAssinatura.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimparAssinatura.Location = new System.Drawing.Point(7, 499);
            this.btnLimparAssinatura.Margin = new System.Windows.Forms.Padding(4);
            this.btnLimparAssinatura.Name = "btnLimparAssinatura";
            this.btnLimparAssinatura.Size = new System.Drawing.Size(442, 61);
            this.btnLimparAssinatura.TabIndex = 4;
            this.btnLimparAssinatura.Text = "Limpar assinatura";
            this.btnLimparAssinatura.UseVisualStyleBackColor = false;
            this.btnLimparAssinatura.Click += new System.EventHandler(this.btnLimparAssinatura_Click);
            // 
            // btnGravaAssinatura
            // 
            this.btnGravaAssinatura.BackColor = System.Drawing.Color.Green;
            this.btnGravaAssinatura.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGravaAssinatura.Location = new System.Drawing.Point(1004, 501);
            this.btnGravaAssinatura.Margin = new System.Windows.Forms.Padding(4);
            this.btnGravaAssinatura.Name = "btnGravaAssinatura";
            this.btnGravaAssinatura.Size = new System.Drawing.Size(442, 57);
            this.btnGravaAssinatura.TabIndex = 3;
            this.btnGravaAssinatura.Text = "Gravar assinatura";
            this.btnGravaAssinatura.UseVisualStyleBackColor = false;
            this.btnGravaAssinatura.Click += new System.EventHandler(this.btnGravaAssinatura_Click);
            // 
            // Clt_AssinaturaDigital
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1459, 585);
            this.ControlBox = false;
            this.Controls.Add(this.btnLimparAssinatura);
            this.Controls.Add(this.btnGravaAssinatura);
            this.Controls.Add(this.pcAssinatura);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Clt_AssinaturaDigital";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Assinatura";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Clt_AssinaturaDigital_FormClosing);
            this.Load += new System.EventHandler(this.Clt_AssinaturaDigital_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pcAssinatura)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pcAssinatura;
        private System.Windows.Forms.Button btnLimparAssinatura;
        private System.Windows.Forms.Button btnGravaAssinatura;
    }
}