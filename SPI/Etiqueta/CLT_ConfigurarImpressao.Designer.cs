namespace SPI
{
    partial class CLT_ConfigurarImpressao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CLT_ConfigurarImpressao));
            this.fraEtiqueta = new System.Windows.Forms.GroupBox();
            this.lblLinha4 = new System.Windows.Forms.Label();
            this.lblLinha3 = new System.Windows.Forms.Label();
            this.lblLinha5 = new System.Windows.Forms.Label();
            this.lblLinha2 = new System.Windows.Forms.Label();
            this.lblLinha1 = new System.Windows.Forms.Label();
            this.imgCodigoBarras = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btExcluir = new System.Windows.Forms.Button();
            this.btAlterar = new System.Windows.Forms.Button();
            this.cmdSalvar = new System.Windows.Forms.Button();
            this.cmdInserir = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.edtLargura = new System.Windows.Forms.TextBox();
            this.edtAltura = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cmbCampo = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbtNegrito = new System.Windows.Forms.RadioButton();
            this.rbtNormal = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.edtFont = new System.Windows.Forms.TextBox();
            this.edtTamanho = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.edtAlturaCodigoBarrasY = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.edtLarguraCodigoBarrasY = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.edtAlturaCodigoBarras = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.edtLarguraCodigoBarras = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dgwConfiguracoes = new System.Windows.Forms.DataGridView();
            this.btSalvarCodigoBarras = new System.Windows.Forms.Button();
            this.edtPastaCodigoBarras = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.fraPicote = new System.Windows.Forms.GroupBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.fraEtiqueta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCodigoBarras)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwConfiguracoes)).BeginInit();
            this.fraPicote.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // fraEtiqueta
            // 
            this.fraEtiqueta.BackColor = System.Drawing.Color.White;
            this.fraEtiqueta.Controls.Add(this.lblLinha4);
            this.fraEtiqueta.Controls.Add(this.lblLinha3);
            this.fraEtiqueta.Controls.Add(this.lblLinha5);
            this.fraEtiqueta.Controls.Add(this.lblLinha2);
            this.fraEtiqueta.Controls.Add(this.lblLinha1);
            this.fraEtiqueta.Controls.Add(this.imgCodigoBarras);
            this.fraEtiqueta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fraEtiqueta.Location = new System.Drawing.Point(12, 19);
            this.fraEtiqueta.Name = "fraEtiqueta";
            this.fraEtiqueta.Size = new System.Drawing.Size(273, 127);
            this.fraEtiqueta.TabIndex = 0;
            this.fraEtiqueta.TabStop = false;
            // 
            // lblLinha4
            // 
            this.lblLinha4.AutoSize = true;
            this.lblLinha4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinha4.Location = new System.Drawing.Point(17, 75);
            this.lblLinha4.Name = "lblLinha4";
            this.lblLinha4.Size = new System.Drawing.Size(0, 16);
            this.lblLinha4.TabIndex = 5;
            // 
            // lblLinha3
            // 
            this.lblLinha3.AutoSize = true;
            this.lblLinha3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinha3.Location = new System.Drawing.Point(17, 69);
            this.lblLinha3.Name = "lblLinha3";
            this.lblLinha3.Size = new System.Drawing.Size(0, 16);
            this.lblLinha3.TabIndex = 4;
            // 
            // lblLinha5
            // 
            this.lblLinha5.AutoSize = true;
            this.lblLinha5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinha5.Location = new System.Drawing.Point(16, 98);
            this.lblLinha5.Name = "lblLinha5";
            this.lblLinha5.Size = new System.Drawing.Size(0, 16);
            this.lblLinha5.TabIndex = 3;
            // 
            // lblLinha2
            // 
            this.lblLinha2.AutoSize = true;
            this.lblLinha2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinha2.Location = new System.Drawing.Point(16, 43);
            this.lblLinha2.Name = "lblLinha2";
            this.lblLinha2.Size = new System.Drawing.Size(0, 16);
            this.lblLinha2.TabIndex = 2;
            // 
            // lblLinha1
            // 
            this.lblLinha1.AutoSize = true;
            this.lblLinha1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinha1.Location = new System.Drawing.Point(16, 16);
            this.lblLinha1.Name = "lblLinha1";
            this.lblLinha1.Size = new System.Drawing.Size(0, 18);
            this.lblLinha1.TabIndex = 1;
            // 
            // imgCodigoBarras
            // 
            this.imgCodigoBarras.Image = ((System.Drawing.Image)(resources.GetObject("imgCodigoBarras.Image")));
            this.imgCodigoBarras.Location = new System.Drawing.Point(156, 88);
            this.imgCodigoBarras.Name = "imgCodigoBarras";
            this.imgCodigoBarras.Size = new System.Drawing.Size(90, 33);
            this.imgCodigoBarras.TabIndex = 0;
            this.imgCodigoBarras.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.btExcluir);
            this.groupBox2.Controls.Add(this.btAlterar);
            this.groupBox2.Controls.Add(this.cmdSalvar);
            this.groupBox2.Controls.Add(this.cmdInserir);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(12, 259);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(364, 204);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Configuração da Etiqueta";
            // 
            // btExcluir
            // 
            this.btExcluir.Location = new System.Drawing.Point(283, 175);
            this.btExcluir.Name = "btExcluir";
            this.btExcluir.Size = new System.Drawing.Size(75, 23);
            this.btExcluir.TabIndex = 14;
            this.btExcluir.Text = "Excluir";
            this.btExcluir.UseVisualStyleBackColor = true;
            this.btExcluir.Click += new System.EventHandler(this.btExcluir_Click);
            // 
            // btAlterar
            // 
            this.btAlterar.Location = new System.Drawing.Point(191, 175);
            this.btAlterar.Name = "btAlterar";
            this.btAlterar.Size = new System.Drawing.Size(75, 23);
            this.btAlterar.TabIndex = 13;
            this.btAlterar.Text = "Alterar";
            this.btAlterar.UseVisualStyleBackColor = true;
            this.btAlterar.Click += new System.EventHandler(this.btAlterar_Click);
            // 
            // cmdSalvar
            // 
            this.cmdSalvar.Location = new System.Drawing.Point(99, 175);
            this.cmdSalvar.Name = "cmdSalvar";
            this.cmdSalvar.Size = new System.Drawing.Size(75, 23);
            this.cmdSalvar.TabIndex = 12;
            this.cmdSalvar.Text = "Salvar";
            this.cmdSalvar.UseVisualStyleBackColor = true;
            this.cmdSalvar.Click += new System.EventHandler(this.cmdSalvar_Click);
            // 
            // cmdInserir
            // 
            this.cmdInserir.Location = new System.Drawing.Point(6, 175);
            this.cmdInserir.Name = "cmdInserir";
            this.cmdInserir.Size = new System.Drawing.Size(75, 23);
            this.cmdInserir.TabIndex = 11;
            this.cmdInserir.Text = "Inserir";
            this.cmdInserir.UseVisualStyleBackColor = true;
            this.cmdInserir.Click += new System.EventHandler(this.cmdInserir_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.edtLargura);
            this.groupBox5.Controls.Add(this.edtAltura);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.ForeColor = System.Drawing.Color.Black;
            this.groupBox5.Location = new System.Drawing.Point(217, 75);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(141, 93);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Posicionamento";
            // 
            // edtLargura
            // 
            this.edtLargura.Location = new System.Drawing.Point(69, 19);
            this.edtLargura.Name = "edtLargura";
            this.edtLargura.Size = new System.Drawing.Size(62, 20);
            this.edtLargura.TabIndex = 3;
            this.edtLargura.TextChanged += new System.EventHandler(this.edtLargura_TextChanged);
            // 
            // edtAltura
            // 
            this.edtAltura.Location = new System.Drawing.Point(69, 42);
            this.edtAltura.Name = "edtAltura";
            this.edtAltura.Size = new System.Drawing.Size(62, 20);
            this.edtAltura.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "<--> Largura";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(29, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Altura";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cmbCampo);
            this.groupBox4.ForeColor = System.Drawing.Color.Black;
            this.groupBox4.Location = new System.Drawing.Point(6, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(352, 50);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Nome do Campo:";
            // 
            // cmbCampo
            // 
            this.cmbCampo.FormattingEnabled = true;
            this.cmbCampo.Location = new System.Drawing.Point(11, 19);
            this.cmbCampo.Name = "cmbCampo";
            this.cmbCampo.Size = new System.Drawing.Size(331, 21);
            this.cmbCampo.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbtNegrito);
            this.groupBox3.Controls.Add(this.rbtNormal);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.edtFont);
            this.groupBox3.Controls.Add(this.edtTamanho);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(6, 75);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 93);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Font";
            // 
            // rbtNegrito
            // 
            this.rbtNegrito.AutoSize = true;
            this.rbtNegrito.Location = new System.Drawing.Point(133, 65);
            this.rbtNegrito.Name = "rbtNegrito";
            this.rbtNegrito.Size = new System.Drawing.Size(59, 17);
            this.rbtNegrito.TabIndex = 8;
            this.rbtNegrito.TabStop = true;
            this.rbtNegrito.Text = "Negrito";
            this.rbtNegrito.UseVisualStyleBackColor = true;
            // 
            // rbtNormal
            // 
            this.rbtNormal.AutoSize = true;
            this.rbtNormal.Location = new System.Drawing.Point(69, 65);
            this.rbtNormal.Name = "rbtNormal";
            this.rbtNormal.Size = new System.Drawing.Size(58, 17);
            this.rbtNormal.TabIndex = 7;
            this.rbtNormal.TabStop = true;
            this.rbtNormal.Text = "Normal";
            this.rbtNormal.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(28, 67);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Estilo:";
            // 
            // edtFont
            // 
            this.edtFont.Location = new System.Drawing.Point(69, 19);
            this.edtFont.Name = "edtFont";
            this.edtFont.Size = new System.Drawing.Size(123, 20);
            this.edtFont.TabIndex = 3;
            // 
            // edtTamanho
            // 
            this.edtTamanho.Location = new System.Drawing.Point(69, 42);
            this.edtTamanho.Name = "edtTamanho";
            this.edtTamanho.Size = new System.Drawing.Size(123, 20);
            this.edtTamanho.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Font:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 45);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Tamanho:";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.groupBox9);
            this.groupBox6.Controls.Add(this.groupBox8);
            this.groupBox6.Controls.Add(this.dgwConfiguracoes);
            this.groupBox6.Controls.Add(this.btSalvarCodigoBarras);
            this.groupBox6.Controls.Add(this.edtPastaCodigoBarras);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Location = new System.Drawing.Point(12, 469);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(364, 155);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Código de Barras";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.edtAlturaCodigoBarrasY);
            this.groupBox9.Controls.Add(this.label1);
            this.groupBox9.Controls.Add(this.edtLarguraCodigoBarrasY);
            this.groupBox9.Controls.Add(this.label2);
            this.groupBox9.Location = new System.Drawing.Point(194, 45);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(164, 75);
            this.groupBox9.TabIndex = 12;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Horizontal";
            // 
            // edtAlturaCodigoBarrasY
            // 
            this.edtAlturaCodigoBarrasY.Location = new System.Drawing.Point(71, 42);
            this.edtAlturaCodigoBarrasY.Name = "edtAlturaCodigoBarrasY";
            this.edtAlturaCodigoBarrasY.Size = new System.Drawing.Size(83, 20);
            this.edtAlturaCodigoBarrasY.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Altura";
            // 
            // edtLarguraCodigoBarrasY
            // 
            this.edtLarguraCodigoBarrasY.Location = new System.Drawing.Point(71, 19);
            this.edtLarguraCodigoBarrasY.Name = "edtLarguraCodigoBarrasY";
            this.edtLarguraCodigoBarrasY.Size = new System.Drawing.Size(83, 20);
            this.edtLarguraCodigoBarrasY.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "<--> Largura";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.edtAlturaCodigoBarras);
            this.groupBox8.Controls.Add(this.label12);
            this.groupBox8.Controls.Add(this.edtLarguraCodigoBarras);
            this.groupBox8.Controls.Add(this.label11);
            this.groupBox8.Location = new System.Drawing.Point(12, 45);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(166, 75);
            this.groupBox8.TabIndex = 11;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Horizontal";
            // 
            // edtAlturaCodigoBarras
            // 
            this.edtAlturaCodigoBarras.Location = new System.Drawing.Point(71, 42);
            this.edtAlturaCodigoBarras.Name = "edtAlturaCodigoBarras";
            this.edtAlturaCodigoBarras.Size = new System.Drawing.Size(84, 20);
            this.edtAlturaCodigoBarras.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(32, 45);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Altura";
            // 
            // edtLarguraCodigoBarras
            // 
            this.edtLarguraCodigoBarras.Location = new System.Drawing.Point(71, 19);
            this.edtLarguraCodigoBarras.Name = "edtLarguraCodigoBarras";
            this.edtLarguraCodigoBarras.Size = new System.Drawing.Size(84, 20);
            this.edtLarguraCodigoBarras.TabIndex = 7;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(2, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "<--> Largura";
            // 
            // dgwConfiguracoes
            // 
            this.dgwConfiguracoes.AllowUserToAddRows = false;
            this.dgwConfiguracoes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwConfiguracoes.Location = new System.Drawing.Point(12, 130);
            this.dgwConfiguracoes.Name = "dgwConfiguracoes";
            this.dgwConfiguracoes.ReadOnly = true;
            this.dgwConfiguracoes.Size = new System.Drawing.Size(265, 14);
            this.dgwConfiguracoes.TabIndex = 11;
            this.dgwConfiguracoes.Visible = false;
            // 
            // btSalvarCodigoBarras
            // 
            this.btSalvarCodigoBarras.Location = new System.Drawing.Point(283, 126);
            this.btSalvarCodigoBarras.Name = "btSalvarCodigoBarras";
            this.btSalvarCodigoBarras.Size = new System.Drawing.Size(75, 23);
            this.btSalvarCodigoBarras.TabIndex = 10;
            this.btSalvarCodigoBarras.Text = "Salvar";
            this.btSalvarCodigoBarras.UseVisualStyleBackColor = true;
            this.btSalvarCodigoBarras.Click += new System.EventHandler(this.btSalvarCodigoBarras_Click);
            // 
            // edtPastaCodigoBarras
            // 
            this.edtPastaCodigoBarras.Location = new System.Drawing.Point(54, 19);
            this.edtPastaCodigoBarras.Name = "edtPastaCodigoBarras";
            this.edtPastaCodigoBarras.Size = new System.Drawing.Size(294, 20);
            this.edtPastaCodigoBarras.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Pasta";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(252, 213);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Alterar Etiqueta";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // fraPicote
            // 
            this.fraPicote.BackColor = System.Drawing.Color.White;
            this.fraPicote.Controls.Add(this.pictureBox5);
            this.fraPicote.Location = new System.Drawing.Point(289, 19);
            this.fraPicote.Name = "fraPicote";
            this.fraPicote.Size = new System.Drawing.Size(46, 127);
            this.fraPicote.TabIndex = 14;
            this.fraPicote.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(11, 18);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(24, 95);
            this.pictureBox5.TabIndex = 11;
            this.pictureBox5.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(4, 202);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(354, 10);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Transparent;
            this.groupBox7.Controls.Add(this.fraEtiqueta);
            this.groupBox7.Controls.Add(this.button1);
            this.groupBox7.Controls.Add(this.groupBox1);
            this.groupBox7.Controls.Add(this.fraPicote);
            this.groupBox7.Location = new System.Drawing.Point(12, 12);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(364, 241);
            this.groupBox7.TabIndex = 16;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Visualização";
            // 
            // CLT_ConfigurarImpressao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(383, 629);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CLT_ConfigurarImpressao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " SPI - Configurar Impressão";
            this.Load += new System.EventHandler(this.CLT_ConfigurarImpressao_Load);
            this.fraEtiqueta.ResumeLayout(false);
            this.fraEtiqueta.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCodigoBarras)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwConfiguracoes)).EndInit();
            this.fraPicote.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox fraEtiqueta;
        private System.Windows.Forms.PictureBox imgCodigoBarras;
        private System.Windows.Forms.Label lblLinha1;
        private System.Windows.Forms.Label lblLinha5;
        private System.Windows.Forms.Label lblLinha2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbCampo;
        private System.Windows.Forms.TextBox edtFont;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox edtTamanho;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton rbtNegrito;
        private System.Windows.Forms.RadioButton rbtNormal;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox edtLargura;
        private System.Windows.Forms.TextBox edtAltura;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button cmdInserir;
        private System.Windows.Forms.Button btSalvarCodigoBarras;
        private System.Windows.Forms.TextBox edtLarguraCodigoBarras;
        private System.Windows.Forms.TextBox edtAlturaCodigoBarras;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox edtPastaCodigoBarras;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button cmdSalvar;
        private System.Windows.Forms.DataGridView dgwConfiguracoes;
        private System.Windows.Forms.Label lblLinha3;
        private System.Windows.Forms.Label lblLinha4;
        private System.Windows.Forms.Button btAlterar;
        private System.Windows.Forms.Button btExcluir;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox fraPicote;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox edtAlturaCodigoBarrasY;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edtLarguraCodigoBarrasY;
        private System.Windows.Forms.Label label2;
    }
}