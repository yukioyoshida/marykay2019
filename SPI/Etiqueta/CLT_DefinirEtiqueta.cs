using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_DefinirEtiqueta : Form
    {
        public CLT_DefinirEtiqueta()
        {
            InitializeComponent();
        }

        public int itNumeroEtiqueta = 0;

        private void btSalvar_Click(object sender, EventArgs e)
        {
            itNumeroEtiqueta = MI_ObterNumeroEtiqueta();
            this.Close();
        }
        private int MI_ObterNumeroEtiqueta()
        {
            if (rbt1.Checked == true) 
            {
                return 1;
            }
            if (rbt2.Checked == true)
            {
                return 2;
            }
            if (rbt3.Checked == true)
            {
                return 3;
            }
            if (rbt4.Checked == true)
            {
                return 4;
            }
            if (rbt5.Checked == true)
            {
                return 5;
            }
            return 0;
        }
    }
}