using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_ConfigurarImpressao : Form
    {
        Int32 AP_ObjRef;
        Int32 AP_Manutencao = 1;

        public int itNumeroEtiqueta = 0;

        FWK_DAO_XML IWK_DAO_XML = new FWK_DAO_XML();

        public CLT_ConfigurarImpressao()
        {
            InitializeComponent();
        }

        private int getManutencao()
        {
            return AP_Manutencao;
        }

        private void setManutencao(int pritOpcao)
        {
            AP_Manutencao = pritOpcao;
        }

        private void CLT_ConfigurarImpressao_Load(object sender, EventArgs e)
        {
            MI_CarregaComboCampos();

            IWK_DAO_XML.MS_LerAquivoXML("Etiqueta.xml");
            dgwConfiguracoes.DataSource = IWK_DAO_XML.MS_GetDataSet();
            dgwConfiguracoes.DataMember = "dtbTeste";

            MI_SimulaEtiqueta();

            MI_Controle(false); 
        }

        private void MI_CarregaComboCampos()
        {
            DataTable dtColunas = new DataTable();
            DataRow drLinha;

            CAT_CadastrarVisitante IAT_CadastrarVisitnate = new CAT_CadastrarVisitante();
            dtColunas = IAT_CadastrarVisitnate.MS_ObterCampos();  

            for (int i = 0; dtColunas.Rows.Count - 1 >= i; i++)
            {
                drLinha = dtColunas.Rows[i];
                cmbCampo.Items.Add(drLinha.ItemArray[0].ToString());
            }
            cmbCampo.Items.Add("VALOR FIXO");
        }

        private void cmdInserir_Click(object sender, EventArgs e)
        {
            if (dgwConfiguracoes.Rows.Count <= 5)
            {
                setManutencao(1);
                MI_LimparCampos();
                MI_Controle(true);
            }
            else
            {
                MessageBox.Show("A capacidade de linhas permitida foi alcan�ada!", "ATEN��O!");  
            }
        }

        private void cmdSalvar_Click(object sender, EventArgs e)
        {
            MI_Salvar();
            MI_SimulaEtiqueta();
        }

        private void MI_Salvar()
        {
            if (getManutencao() == 1)
                IWK_DAO_XML.MS_Incluir(MI_CopyControlsToCTO());
            else
            {
                CTO_Linha ITO_Linha = MI_CopyControlsToCTO();
                ITO_Linha.setObjRef(AP_ObjRef);
                IWK_DAO_XML.MS_Alterar(ITO_Linha);
            }
        }

        private CTO_Linha MI_CopyControlsToCTO()
        {
            CTO_Linha ITO_Linha = new CTO_Linha();
            ITO_Linha.setCampo(cmbCampo.Text);
            ITO_Linha.setPosicaoHorizontalTexto(Convert.ToInt32(edtLargura.Text));
            ITO_Linha.setPosicaoVerticalTexto(Convert.ToInt32(edtAltura.Text));
            ITO_Linha.setAlturaDaEtiqueta(Convert.ToInt32(0));
            ITO_Linha.setAlturaDoCodigoDeBarras(0);
            ITO_Linha.setLarguraDoCodigoDeBarras(0);
            ITO_Linha.setPosicaoLateralDaEtiqueta(Convert.ToInt32(0));
            ITO_Linha.setNomeFonte(edtFont.Text);
            ITO_Linha.setTamanhoFonte(Convert.ToInt32(edtTamanho.Text));
            ITO_Linha.setEstiloFonte(MI_ObterEstilo());
            ITO_Linha.setPastaCodBarras("");
            return ITO_Linha;
        }

        private void MI_SalvarCodigoBarras()
        {
            if (getManutencao() == 1)
                IWK_DAO_XML.MS_Incluir(MI_CopyControlsToCTOCodigodeBarras());
            else
            {
                CTO_Linha ITO_Linha = MI_CopyControlsToCTOCodigodeBarras();
                ITO_Linha.setObjRef(AP_ObjRef);
                IWK_DAO_XML.MS_Alterar(ITO_Linha);
            }
        }

        private CTO_Linha MI_CopyControlsToCTOCodigodeBarras()
        {
            CTO_Linha ITO_Linha = new CTO_Linha();
            ITO_Linha.setCampo(cmbCampo.Text);
            ITO_Linha.setPosicaoHorizontalTexto(0);
            ITO_Linha.setPosicaoVerticalTexto(0);
            ITO_Linha.setAlturaDaEtiqueta(Convert.ToInt32(0));
            ITO_Linha.setAlturaDoCodigoDeBarras(Convert.ToInt32(edtAlturaCodigoBarras.Text));
            ITO_Linha.setLarguraDoCodigoDeBarras(Convert.ToInt32(edtLarguraCodigoBarras.Text));
            ITO_Linha.setPosicaoLateralDaEtiqueta(Convert.ToInt32(0));
            ITO_Linha.setNomeFonte(edtFont.Text);
            ITO_Linha.setTamanhoFonte(10);
            ITO_Linha.setEstiloFonte(MI_ObterEstilo());
            ITO_Linha.setPastaCodBarras("C:\\SPI\\bin\\CodBarras\\");
            return ITO_Linha;
        }

        private string MI_ObterEstilo()
        {
            if (rbtNegrito.Checked == true)
            {
                return "BOLD";
            }
            else
            {
                return "NORMAL";
            }
        }

        private void MI_LimparCampos()
        {
            cmbCampo.Text = "";
            edtFont.Text = "";
            edtTamanho.Text = "";
            rbtNegrito.Checked = false;
            rbtNormal.Checked = true;
            edtLargura.Text = "";
            edtAltura.Text = "";
            
            edtLarguraCodigoBarras.Text = "";
            edtAlturaCodigoBarras.Text = "";
            edtPastaCodigoBarras.Text = "";

            edtLarguraCodigoBarrasY.Text = "";
            edtAlturaCodigoBarrasY.Text = ""; 
        }

        private void MI_Controle(bool stStatus)
        {
            cmbCampo.Enabled = stStatus;
            edtFont.ReadOnly = !stStatus;
            edtTamanho.ReadOnly = !stStatus;
            rbtNegrito.Enabled = stStatus;
            rbtNormal.Enabled = stStatus; 
            edtLargura.ReadOnly = !stStatus;
            edtAltura.ReadOnly = !stStatus;
        }

        private void btAlterar_Click(object sender, EventArgs e)
        {
            MI_AbrirFormularioLinhas(); 
        }

        private void MI_Carrega(string stObjRef)
        {
            setManutencao(2);

            CTO_Linha ITO_Linha = IWK_DAO_XML.MS_Obter(stObjRef);
            if (ITO_Linha != null)
            {
                MI_CopyCTOToCOntrols(ITO_Linha);
            }
            else
            {
                MessageBox.Show("Est� linha n�o est� sendo utilizada!", "ATEN��O!");
                MI_AbrirFormularioLinhas();
            }
        }

        private void MI_AbrirFormularioLinhas()
        {
            Int32 itLinha = 0;
            CLT_SelecionarLinha ABRIR = new CLT_SelecionarLinha();
            ABRIR.ShowDialog();

            if (ABRIR.stLinha != "")
            {
                itLinha = Convert.ToInt32(ABRIR.stLinha);
                itLinha++;

                MI_Carrega(itLinha.ToString());

                if (AP_Manutencao == 2)
                {
                    MI_Controle(true);
                }
            }
        }

        private void MI_CopyCTOToCOntrols(CTO_Linha IET_Linha)
        {
            MI_LimparCampos(); 

            if (IET_Linha != null)
            {
                if ((IET_Linha.getObjRef().ToString() != "6") && (IET_Linha.getObjRef().ToString() != "7"))
                {
                    AP_ObjRef = IET_Linha.getObjRef();
                    cmbCampo.Text = IET_Linha.getCampo();
                    edtLargura.Text = IET_Linha.getPosicaoHorizontalTexto().ToString();
                    edtAltura.Text = IET_Linha.getPosicaoVerticalTexto().ToString();
                    edtFont.Text = IET_Linha.getNomeFonte();
                    edtTamanho.Text = IET_Linha.getTamanhoFonte().ToString();
                    if (IET_Linha.getEstiloFonte() == "BOLD")
                    {
                        rbtNegrito.Checked = true;
                        rbtNormal.Checked = false;
                    }
                    else
                    {
                        rbtNegrito.Checked = false;
                        rbtNormal.Checked = true;
                    }
                }
                else
                {
                    AP_ObjRef = IET_Linha.getObjRef();
                    edtAlturaCodigoBarras.Text = IET_Linha.getAlturaDoCodigoDeBarras().ToString();
                    edtLarguraCodigoBarras.Text = IET_Linha.getLarguraDoCodigoDeBarras().ToString();
                    edtPastaCodigoBarras.Text = IET_Linha.getPastaCodBarras();
                }
            }
        }

        private void btExcluir_Click(object sender, EventArgs e)
        {
            //if (getManutencao() == 1)
            //{
            //}
            //else
            //{
            //    setManutencao(2);
            //    dgwConfiguracoes.Rows.Remove(dgwConfiguracoes.Rows[AP_ObjRef-1]);
            //    MI_Salvar();
            //}
        }

        private void MI_SimulaEtiqueta()
        {
            Int32 itLinha = dgwConfiguracoes.Rows.Count - 1; 
            try
            {
                if (itLinha >= 0)
                {
                    if (!MI_CasoCodigoBarras(1))
                    {
                        lblLinha1.Location = new Point(MI_ObterPosicaoY(1), lblLinha1.Location.Y);
                        lblLinha1.Location = new Point(MI_ObterPosicaoX(1), lblLinha1.Location.X);
                        lblLinha1.Font = new Font(MI_ObterFont(1), MI_ObterTamanho(1), MI_ObterEstilo(1));
                        lblLinha1.Text = MI_ObterSiglaCampo(1);
                    }
                    else
                    {
                        imgCodigoBarras.Location = new Point(MI_ObterPosicaoCodigoBarrasY(1), imgCodigoBarras.Location.Y);
                        imgCodigoBarras.Location = new Point(MI_ObterPosicaoCodigoBarrasY(1), imgCodigoBarras.Location.X);
                    }
                }

                if (itLinha >= 1)
                {
                    if (!MI_CasoCodigoBarras(2))
                    {
                        lblLinha2.Location = new Point(MI_ObterPosicaoY(2), lblLinha2.Location.Y);
                        lblLinha2.Location = new Point(MI_ObterPosicaoX(2), lblLinha2.Location.X);
                        lblLinha2.Font = new Font(MI_ObterFont(2), MI_ObterTamanho(2), MI_ObterEstilo(2));
                        lblLinha2.Text = MI_ObterSiglaCampo(2);
                    }
                    else
                    {
                        imgCodigoBarras.Location = new Point(MI_ObterPosicaoCodigoBarrasY(2), imgCodigoBarras.Location.Y);
                        imgCodigoBarras.Location = new Point(MI_ObterPosicaoCodigoBarrasX(2), imgCodigoBarras.Location.X);
                    }
                }

                if (itLinha >= 2)
                {
                    if (!MI_CasoCodigoBarras(3))
                    {
                        lblLinha3.Location = new Point(MI_ObterPosicaoY(3), lblLinha3.Location.Y);
                        lblLinha3.Location = new Point(MI_ObterPosicaoX(3), lblLinha3.Location.X);
                        lblLinha3.Font = new Font(MI_ObterFont(3), MI_ObterTamanho(3), MI_ObterEstilo(3));
                        lblLinha3.Text = MI_ObterSiglaCampo(3);
                    }
                    else
                    {
                        imgCodigoBarras.Location = new Point(MI_ObterPosicaoCodigoBarrasY(3), imgCodigoBarras.Location.Y);
                        imgCodigoBarras.Location = new Point(MI_ObterPosicaoCodigoBarrasX(3), imgCodigoBarras.Location.X);
                    }
                }

                if (itLinha >= 3)
                {
                    if (!MI_CasoCodigoBarras(4))
                    {
                        lblLinha4.Location = new Point(MI_ObterPosicaoY(4), lblLinha4.Location.Y);
                        lblLinha4.Location = new Point(MI_ObterPosicaoX(4), lblLinha4.Location.X);
                        lblLinha4.Font = new Font(MI_ObterFont(4), MI_ObterTamanho(4), MI_ObterEstilo(4));
                        lblLinha4.Text = "INFORMA��ES";
                        lblLinha4.Text = MI_ObterSiglaCampo(4);
                    }
                    else
                    {
                        imgCodigoBarras.Location = new Point(MI_ObterPosicaoCodigoBarrasY(4), imgCodigoBarras.Location.Y);
                        imgCodigoBarras.Location = new Point(MI_ObterPosicaoCodigoBarrasX(4), imgCodigoBarras.Location.X);
                    }
                }

                if (itLinha >= 4)
                {
                    if (!MI_CasoCodigoBarras(5))
                    {
                        lblLinha5.Location = new Point(MI_ObterPosicaoY(5), lblLinha5.Location.Y);
                        lblLinha5.Location = new Point(MI_ObterPosicaoX(5), lblLinha5.Location.X);
                        lblLinha5.Font = new Font(MI_ObterFont(5), MI_ObterTamanho(5), MI_ObterEstilo(5));
                        lblLinha5.Text = MI_ObterSiglaCampo(5);
                    }
                    else
                    {
                        imgCodigoBarras.Location = new Point(MI_ObterPosicaoCodigoBarrasY(5), imgCodigoBarras.Location.Y);
                        imgCodigoBarras.Location = new Point(MI_ObterPosicaoCodigoBarrasX(5), imgCodigoBarras.Location.X);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Erro ao tentar montar a etiqueta, verifique as configura��es","ATEN��O!");  
            }
        }

        private Int32 MI_ObterPosicaoY(Int32 itLinha)
        {
            return Convert.ToInt32(dgwConfiguracoes.Rows[itLinha-1].Cells["PosicaoVerticalTexto"].Value); 
        }

        private Int32 MI_ObterPosicaoX(Int32 itLinha)
        {
            return Convert.ToInt32(dgwConfiguracoes.Rows[itLinha-1].Cells["PosicaoHorizontalTexto"].Value); 
        }

        private Int32 MI_ObterPosicaoCodigoBarrasY(Int32 itLinha)
        {
            return Convert.ToInt32(dgwConfiguracoes.Rows[itLinha - 1].Cells["AlturaDoCodigoDeBarras"].Value);
        }

        private Int32 MI_ObterPosicaoCodigoBarrasX(Int32 itLinha)
        {
            Int32 itValor = Convert.ToInt32(dgwConfiguracoes.Rows[itLinha - 1].Cells["LarguraDoCodigoDeBarras"].Value);
            return itValor - 106;
        }

        private string MI_ObterFont(Int32 itLinha)
        {
            return Convert.ToString(dgwConfiguracoes.Rows[itLinha - 1].Cells["NomeFonte"].Value); 
        }

        private Int32 MI_ObterTamanho(Int32 itLinha)
        {
            return Convert.ToInt32(dgwConfiguracoes.Rows[itLinha - 1].Cells["TamanhoFonte"].Value);
        }

        private FontStyle MI_ObterEstilo(Int32 itLinha)
        {
            string stTipo = Convert.ToString(dgwConfiguracoes.Rows[itLinha - 1].Cells["EstiloFonte"].Value);
            if (stTipo == "BOLD")
            {
                return FontStyle.Bold;
            }
            else
            {
                return FontStyle.Regular;
            }
        }

        private string MI_ObterSiglaCampo(Int32 itLinha)
        {
            string stNomeCampo = "";
            return stNomeCampo; 
            //Assembly myClass = Assembly.GetAssembly(typeof(CAT_CadastrarVisitante));
            
            //CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();
            //CTO_Visitante ITO_Visitante = IAT_CadastrarVisitante.MS_Obter1();
            //if (ITO_Visitante != null)
            //{
            //    stNomeCampo = "get" + dgwConfiguracoes.Rows[itLinha - 1].Cells["Campo"].Value;
            //    return Convert.ToString(ITO_Visitante.GetType().InvokeMember(stNomeCampo, BindingFlags.InvokeMethod, null, ITO_Visitante, new object[] { }));
            //}
            //else
            //{
            //    return Convert.ToString(dgwConfiguracoes.Rows[itLinha - 1].Cells["Campo"].Value);            
            //}
        }

        private bool MI_CasoCodigoBarras(Int32 itLinha)
        {
            bool boResult = false;
            string stValor = "";
            stValor =  Convert.ToString(dgwConfiguracoes.Rows[itLinha - 1].Cells["PastaCodBarras"].Value);

            if (stValor != "")
            {
                boResult = true;
            }
            return boResult;
        }

        private void MI_AlterarEtiqueta()
        {
            int itNumeroEtiqueta = 0;

            CLT_DefinirEtiqueta ABRIR = new CLT_DefinirEtiqueta();
            ABRIR.ShowDialog();
            itNumeroEtiqueta = ABRIR.itNumeroEtiqueta;

            if (itNumeroEtiqueta != 0)
            {
                fraEtiqueta.Size = new Size(MI_ObterLargura(itNumeroEtiqueta), MI_ObterAltura(itNumeroEtiqueta));
            }

            if (itNumeroEtiqueta == 4)
            {
                fraPicote.Visible = true;
            }
            else
            {
                fraPicote.Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MI_AlterarEtiqueta();
        }

        private int MI_ObterLargura(int itNumeroEtiqueta)
        {
            switch (itNumeroEtiqueta)
            {
                case 1:
                    return 320;
                    break;
                case 2:
                    return 340;
                    break;
                case 3:
                    return 320;
                    break;
                case 4:
                    return 273;
                    break;
                case 5:
                    return 273;
                    break;
            }
            return 0;
        }

        private int MI_ObterAltura(int itNumeroEtiqueta)
        {
            switch (itNumeroEtiqueta)
            {
                case 1:
                    return 126;
                    break;
                case 2:
                    return 176;
                    break;
                case 3:
                    return 94;
                    break;
                case 4:
                    return 127;
                    break;
                case 5:
                    return 142;
                    break;
            }
            return 0;
        }

        private void edtLargura_TextChanged(object sender, EventArgs e)
        {

        }

        private void btSalvarCodigoBarras_Click(object sender, EventArgs e)
        {
            MI_SalvarCodigoBarras();
            MI_SimulaEtiqueta();
        }
    }
}