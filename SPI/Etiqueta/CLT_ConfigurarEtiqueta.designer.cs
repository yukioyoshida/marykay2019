namespace SPI
{
    partial class CLT_ConfigurarEtiqueta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.dtgTeste = new System.Windows.Forms.DataGridView();
            this.button4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.edtPosicaoHorizontalDoTexto = new System.Windows.Forms.TextBox();
            this.edtPosicaoVerticalDoTexto = new System.Windows.Forms.TextBox();
            this.btoIncluir = new System.Windows.Forms.Button();
            this.btoEditar = new System.Windows.Forms.Button();
            this.Barras = new System.Windows.Forms.TabControl();
            this.Texto = new System.Windows.Forms.TabPage();
            this.lbxEstiloFonte = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.edtTamanhoFonte = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.edtNomeFonte = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbbCampo = new System.Windows.Forms.ComboBox();
            this.Campo = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.edtPastaCodBarras = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.edtPosicaoLateralDaEtiqueta = new System.Windows.Forms.TextBox();
            this.edtLarguraDoCodigoDeBarras = new System.Windows.Forms.TextBox();
            this.edtAlturaDoCodigoDeBarras = new System.Windows.Forms.TextBox();
            this.edtAlturaDaEtiqueta = new System.Windows.Forms.TextBox();
            this.edtPosicaoDoCodigoDeBarras = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTeste)).BeginInit();
            this.Barras.SuspendLayout();
            this.Texto.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Imprimir";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dtgTeste
            // 
            this.dtgTeste.AllowUserToAddRows = false;
            this.dtgTeste.AllowUserToDeleteRows = false;
            this.dtgTeste.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgTeste.Location = new System.Drawing.Point(12, 313);
            this.dtgTeste.Name = "dtgTeste";
            this.dtgTeste.ReadOnly = true;
            this.dtgTeste.ShowEditingIcon = false;
            this.dtgTeste.Size = new System.Drawing.Size(548, 154);
            this.dtgTeste.TabIndex = 1;
            this.dtgTeste.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgTeste_CellContentClick);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(194, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 8;
            this.button4.Text = "Salvar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Posi��o Lateral:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(259, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Posi��o Altura:";
            // 
            // edtPosicaoHorizontalDoTexto
            // 
            this.edtPosicaoHorizontalDoTexto.Location = new System.Drawing.Point(153, 48);
            this.edtPosicaoHorizontalDoTexto.Name = "edtPosicaoHorizontalDoTexto";
            this.edtPosicaoHorizontalDoTexto.Size = new System.Drawing.Size(50, 20);
            this.edtPosicaoHorizontalDoTexto.TabIndex = 2;
            // 
            // edtPosicaoVerticalDoTexto
            // 
            this.edtPosicaoVerticalDoTexto.Location = new System.Drawing.Point(343, 45);
            this.edtPosicaoVerticalDoTexto.Name = "edtPosicaoVerticalDoTexto";
            this.edtPosicaoVerticalDoTexto.Size = new System.Drawing.Size(50, 20);
            this.edtPosicaoVerticalDoTexto.TabIndex = 3;
            // 
            // btoIncluir
            // 
            this.btoIncluir.Location = new System.Drawing.Point(101, 12);
            this.btoIncluir.Name = "btoIncluir";
            this.btoIncluir.Size = new System.Drawing.Size(75, 23);
            this.btoIncluir.TabIndex = 17;
            this.btoIncluir.Text = "Incluir";
            this.btoIncluir.UseVisualStyleBackColor = true;
            this.btoIncluir.Click += new System.EventHandler(this.btoIncluir_Click);
            // 
            // btoEditar
            // 
            this.btoEditar.Location = new System.Drawing.Point(12, 284);
            this.btoEditar.Name = "btoEditar";
            this.btoEditar.Size = new System.Drawing.Size(75, 23);
            this.btoEditar.TabIndex = 18;
            this.btoEditar.Text = "Editar";
            this.btoEditar.UseVisualStyleBackColor = true;
            this.btoEditar.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // Barras
            // 
            this.Barras.Controls.Add(this.Texto);
            this.Barras.Controls.Add(this.tabPage2);
            this.Barras.Location = new System.Drawing.Point(12, 66);
            this.Barras.Name = "Barras";
            this.Barras.SelectedIndex = 0;
            this.Barras.Size = new System.Drawing.Size(548, 212);
            this.Barras.TabIndex = 19;
            // 
            // Texto
            // 
            this.Texto.Controls.Add(this.lbxEstiloFonte);
            this.Texto.Controls.Add(this.label8);
            this.Texto.Controls.Add(this.edtTamanhoFonte);
            this.Texto.Controls.Add(this.label7);
            this.Texto.Controls.Add(this.edtNomeFonte);
            this.Texto.Controls.Add(this.label6);
            this.Texto.Controls.Add(this.cbbCampo);
            this.Texto.Controls.Add(this.Campo);
            this.Texto.Controls.Add(this.edtPosicaoVerticalDoTexto);
            this.Texto.Controls.Add(this.label1);
            this.Texto.Controls.Add(this.label4);
            this.Texto.Controls.Add(this.edtPosicaoHorizontalDoTexto);
            this.Texto.Location = new System.Drawing.Point(4, 22);
            this.Texto.Name = "Texto";
            this.Texto.Padding = new System.Windows.Forms.Padding(3);
            this.Texto.Size = new System.Drawing.Size(540, 186);
            this.Texto.TabIndex = 0;
            this.Texto.Text = "Texto";
            this.Texto.UseVisualStyleBackColor = true;
            this.Texto.Click += new System.EventHandler(this.Texto_Click);
            // 
            // lbxEstiloFonte
            // 
            this.lbxEstiloFonte.FormattingEnabled = true;
            this.lbxEstiloFonte.Items.AddRange(new object[] {
            "NORMAL",
            "BOLD"});
            this.lbxEstiloFonte.Location = new System.Drawing.Point(153, 117);
            this.lbxEstiloFonte.Name = "lbxEstiloFonte";
            this.lbxEstiloFonte.Size = new System.Drawing.Size(100, 43);
            this.lbxEstiloFonte.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(112, 117);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Estilo:";
            // 
            // edtTamanhoFonte
            // 
            this.edtTamanhoFonte.Location = new System.Drawing.Point(343, 77);
            this.edtTamanhoFonte.Name = "edtTamanhoFonte";
            this.edtTamanhoFonte.Size = new System.Drawing.Size(100, 20);
            this.edtTamanhoFonte.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(282, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Tamanho:";
            // 
            // edtNomeFonte
            // 
            this.edtNomeFonte.Location = new System.Drawing.Point(153, 80);
            this.edtNomeFonte.Name = "edtNomeFonte";
            this.edtNomeFonte.Size = new System.Drawing.Size(100, 20);
            this.edtNomeFonte.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(110, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Fonte:";
            // 
            // cbbCampo
            // 
            this.cbbCampo.FormattingEnabled = true;
            this.cbbCampo.Items.AddRange(new object[] {
            "CodigoBarras"});
            this.cbbCampo.Location = new System.Drawing.Point(153, 15);
            this.cbbCampo.Name = "cbbCampo";
            this.cbbCampo.Size = new System.Drawing.Size(298, 21);
            this.cbbCampo.TabIndex = 1;
            // 
            // Campo
            // 
            this.Campo.AutoSize = true;
            this.Campo.Location = new System.Drawing.Point(104, 18);
            this.Campo.Name = "Campo";
            this.Campo.Size = new System.Drawing.Size(43, 13);
            this.Campo.TabIndex = 17;
            this.Campo.Text = "Campo:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.edtPastaCodBarras);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.edtPosicaoLateralDaEtiqueta);
            this.tabPage2.Controls.Add(this.edtLarguraDoCodigoDeBarras);
            this.tabPage2.Controls.Add(this.edtAlturaDoCodigoDeBarras);
            this.tabPage2.Controls.Add(this.edtAlturaDaEtiqueta);
            this.tabPage2.Controls.Add(this.edtPosicaoDoCodigoDeBarras);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(540, 186);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Barras";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // edtPastaCodBarras
            // 
            this.edtPastaCodBarras.Location = new System.Drawing.Point(178, 120);
            this.edtPastaCodBarras.Name = "edtPastaCodBarras";
            this.edtPastaCodBarras.Size = new System.Drawing.Size(355, 20);
            this.edtPastaCodBarras.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 123);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Pasta:";
            // 
            // edtPosicaoLateralDaEtiqueta
            // 
            this.edtPosicaoLateralDaEtiqueta.Location = new System.Drawing.Point(178, 94);
            this.edtPosicaoLateralDaEtiqueta.Name = "edtPosicaoLateralDaEtiqueta";
            this.edtPosicaoLateralDaEtiqueta.Size = new System.Drawing.Size(100, 20);
            this.edtPosicaoLateralDaEtiqueta.TabIndex = 10;
            // 
            // edtLarguraDoCodigoDeBarras
            // 
            this.edtLarguraDoCodigoDeBarras.Location = new System.Drawing.Point(179, 68);
            this.edtLarguraDoCodigoDeBarras.Name = "edtLarguraDoCodigoDeBarras";
            this.edtLarguraDoCodigoDeBarras.Size = new System.Drawing.Size(100, 20);
            this.edtLarguraDoCodigoDeBarras.TabIndex = 9;
            // 
            // edtAlturaDoCodigoDeBarras
            // 
            this.edtAlturaDoCodigoDeBarras.Location = new System.Drawing.Point(178, 42);
            this.edtAlturaDoCodigoDeBarras.Name = "edtAlturaDoCodigoDeBarras";
            this.edtAlturaDoCodigoDeBarras.Size = new System.Drawing.Size(100, 20);
            this.edtAlturaDoCodigoDeBarras.TabIndex = 8;
            // 
            // edtAlturaDaEtiqueta
            // 
            this.edtAlturaDaEtiqueta.Location = new System.Drawing.Point(179, 16);
            this.edtAlturaDaEtiqueta.Name = "edtAlturaDaEtiqueta";
            this.edtAlturaDaEtiqueta.Size = new System.Drawing.Size(100, 20);
            this.edtAlturaDaEtiqueta.TabIndex = 7;
            // 
            // edtPosicaoDoCodigoDeBarras
            // 
            this.edtPosicaoDoCodigoDeBarras.AutoSize = true;
            this.edtPosicaoDoCodigoDeBarras.Location = new System.Drawing.Point(9, 97);
            this.edtPosicaoDoCodigoDeBarras.Name = "edtPosicaoDoCodigoDeBarras";
            this.edtPosicaoDoCodigoDeBarras.Size = new System.Drawing.Size(140, 13);
            this.edtPosicaoDoCodigoDeBarras.TabIndex = 3;
            this.edtPosicaoDoCodigoDeBarras.Text = "Posi��o Lateral da Etiqueta:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(145, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Largura do C�digo de Barras:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Altura do C�digo de Barras:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Altura da Etiqueta:";
            // 
            // CLT_ConfigurarEtiqueta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 469);
            this.Controls.Add(this.Barras);
            this.Controls.Add(this.btoEditar);
            this.Controls.Add(this.btoIncluir);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.dtgTeste);
            this.Controls.Add(this.button1);
            this.Name = "CLT_ConfigurarEtiqueta";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dtgTeste)).EndInit();
            this.Barras.ResumeLayout(false);
            this.Texto.ResumeLayout(false);
            this.Texto.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dtgTeste;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox edtPosicaoHorizontalDoTexto;
        private System.Windows.Forms.TextBox edtPosicaoVerticalDoTexto;
        private System.Windows.Forms.Button btoIncluir;
        private System.Windows.Forms.Button btoEditar;
        private System.Windows.Forms.TabControl Barras;
        private System.Windows.Forms.TabPage Texto;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox cbbCampo;
        private System.Windows.Forms.Label Campo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label edtPosicaoDoCodigoDeBarras;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox edtPosicaoLateralDaEtiqueta;
        private System.Windows.Forms.TextBox edtLarguraDoCodigoDeBarras;
        private System.Windows.Forms.TextBox edtAlturaDoCodigoDeBarras;
        private System.Windows.Forms.TextBox edtAlturaDaEtiqueta;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox edtTamanhoFonte;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox edtNomeFonte;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox edtPastaCodBarras;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox lbxEstiloFonte;
    }
}

