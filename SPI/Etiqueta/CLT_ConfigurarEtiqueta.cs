using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Reflection;
using SPI;

namespace SPI
{
    public partial class CLT_ConfigurarEtiqueta : Form
    {
        Int32 AP_ObjRef; 
        Int32 AP_Manutencao = 1;

        FWK_DAO_XML IWK_DAO_XML = new FWK_DAO_XML();

        public CLT_ConfigurarEtiqueta()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            IWK_DAO_XML.MS_LerAquivoXML("Etiqueta.xml");
            dtgTeste.DataSource = IWK_DAO_XML.MS_GetDataSet();
            dtgTeste.DataMember = "dtbTeste";

            MI_LimparCampos();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //CLT_ImprimirCracha ILT_Imprimir = new CLT_ImprimirCracha();
            //ILT_Imprimir.MS_Imprimir();

            PrintDocument pd = new PrintDocument();
            pd.PrintPage += new PrintPageEventHandler(MI_Imprimir);
            pd.Print();
        }


        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            IWK_DAO_XML.MS_EscreverAquivoXML("Etiqueta.xml");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (getManutencao() == 1)
                IWK_DAO_XML.MS_Incluir(MI_CopyControlsToCTO());
            else
            {
                CTO_Linha ITO_Linha = MI_CopyControlsToCTO();
                ITO_Linha.setObjRef(AP_ObjRef);
                IWK_DAO_XML.MS_Alterar(ITO_Linha);
            }
        }

        private void MI_CopyCTOToCOntrols(CTO_Linha IET_Linha)
        {
            if (IET_Linha != null)
            {
                AP_ObjRef = IET_Linha.getObjRef();
                cbbCampo.Text = IET_Linha.getCampo();
                edtPosicaoHorizontalDoTexto.Text = IET_Linha.getPosicaoHorizontalTexto().ToString();
                edtPosicaoVerticalDoTexto.Text = IET_Linha.getPosicaoVerticalTexto().ToString();
                edtAlturaDaEtiqueta.Text = IET_Linha.getAlturaDaEtiqueta().ToString();
                edtAlturaDoCodigoDeBarras.Text = IET_Linha.getAlturaDoCodigoDeBarras().ToString();
                edtLarguraDoCodigoDeBarras.Text = IET_Linha.getLarguraDoCodigoDeBarras().ToString();
                edtPosicaoLateralDaEtiqueta.Text = IET_Linha.getPosicaoLateralDaEtiqueta().ToString();
                edtNomeFonte.Text = IET_Linha.getNomeFonte();
                edtTamanhoFonte.Text = IET_Linha.getTamanhoFonte().ToString();
                if (IET_Linha.getEstiloFonte() == "BOLD")
                    lbxEstiloFonte.Text = "BOLD";
                else
                    lbxEstiloFonte.Text = "BOLD";

                edtPastaCodBarras.Text = IET_Linha.getPastaCodBarras();
            }
        }

        private CTO_Linha  MI_CopyControlsToCTO()
        {
            CTO_Linha ITO_Linha = new CTO_Linha();
            ITO_Linha.setCampo(cbbCampo.Text);
            ITO_Linha.setPosicaoHorizontalTexto(Convert.ToInt32(edtPosicaoHorizontalDoTexto.Text));
            ITO_Linha.setPosicaoVerticalTexto(Convert.ToInt32(edtPosicaoVerticalDoTexto.Text));
            ITO_Linha.setAlturaDaEtiqueta(Convert.ToInt32(edtAlturaDaEtiqueta.Text));
            ITO_Linha.setAlturaDoCodigoDeBarras(Convert.ToInt32(edtAlturaDoCodigoDeBarras.Text));
            ITO_Linha.setLarguraDoCodigoDeBarras(Convert.ToInt32(edtLarguraDoCodigoDeBarras.Text));
            ITO_Linha.setPosicaoLateralDaEtiqueta(Convert.ToInt32(edtPosicaoLateralDaEtiqueta.Text));
            ITO_Linha.setNomeFonte(edtNomeFonte.Text);
            ITO_Linha.setTamanhoFonte(Convert.ToInt32(edtTamanhoFonte.Text));
            ITO_Linha.setEstiloFonte(lbxEstiloFonte.Text);
            ITO_Linha.setPastaCodBarras(edtPastaCodBarras.Text);
            return ITO_Linha;
        }

        private int getManutencao()
        {
            return AP_Manutencao;
        }

        private void setManutencao(int pritOpcao)
        {
            AP_Manutencao = pritOpcao;
        }

        private void btoIncluir_Click(object sender, EventArgs e)
        {
            setManutencao(1);
            MI_LimparCampos();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            setManutencao(2);
            CTO_Linha ITO_Linha = IWK_DAO_XML.MS_Obter(dtgTeste.CurrentRow.Cells["ObjRef"].Value.ToString());
            MI_CopyCTOToCOntrols(ITO_Linha);
        }

        private void MI_Imprimir(object sender, PrintPageEventArgs ev)
        {
        }

        private void Texto_Click(object sender, EventArgs e)
        {

        }

        private void MI_LimparCampos()
        {
            edtPosicaoHorizontalDoTexto.Text = "0";
            edtPosicaoVerticalDoTexto.Text = "0";
            edtAlturaDaEtiqueta.Text = "0";
            edtAlturaDoCodigoDeBarras.Text = "0";
            edtLarguraDoCodigoDeBarras.Text = "0";
            edtPosicaoLateralDaEtiqueta.Text = "0";
            edtTamanhoFonte.Text = "0";
            edtPastaCodBarras.Text = "";
            cbbCampo.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {
        }

        private void Alterar_Click(object sender, EventArgs e)
        {
        }

        private void dtgTeste_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}