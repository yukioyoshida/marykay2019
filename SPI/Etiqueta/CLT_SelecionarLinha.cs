using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_SelecionarLinha : Form
    {
        public string stLinha = string.Empty;  

        public CLT_SelecionarLinha()
        {
            InitializeComponent();
        }

        private void btVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            stLinha = cmbLinhas.SelectedIndex.ToString();
            this.Close();
        }
    }
}