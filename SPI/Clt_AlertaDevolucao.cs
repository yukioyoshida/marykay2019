﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class Clt_AlertaDevolucao : Form
    {
        public Clt_AlertaDevolucao()
        {
            InitializeComponent();
        }

        private void tmrPiscar_Tick(object sender, EventArgs e)
        {
            lblAtencao.Visible = !lblAtencao.Visible;
        }

        private void btAcessar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
