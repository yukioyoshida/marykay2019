using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_LeituraJoia : Form
    {
        public DataTable dtTodosRegistros;
        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        public string stCodigoBarras = "";
        public string stMetodoBusca = "";
        public string stPalavraChave = "";

        private bool AP_TrocaDeProduto { get; set; }

        public CLT_LeituraJoia()
        {
            InitializeComponent();
        }

        private void CarregaBackground()
        {
            this.BackgroundImage = Properties.Resources.BackgroundMaryKay;
        }

        private void CLT_LocalizarRegistro_Load(object sender, EventArgs e)
        {
            System.Threading.Thread ts = new System.Threading.Thread(CarregaBackground);
            ts.Start();

            CarregaUsuario();
            edtPalavra.Focus();
            MI_PosicionarPainel();

            if (CSV_Global.getTipoJoia().ToString().Equals("FAIXA") && !CSV_Global.getCancelaFaixa().ToUpper().Equals("S"))
                btoCancelarEntrega.Visible = false;

            if (CSV_Global.getTipoJoia().ToString().Equals("JOIA") && !CSV_Global.getCancelaJoia().ToUpper().Equals("S"))
                btoCancelarEntrega.Visible = false;

            lblCortes.Text = "Primeira Linha" + Environment.NewLine + "Segunda Linha";

            MI_Limpar();
        }

        private void MI_Limpar()
        {
            lblCodigo.Text = string.Empty;

            lblTexto1.Visible = false;
            lblTexto2.Visible = false;

            lblCortes.Text = string.Empty;
            lblConsultora.Text = string.Empty;

            btoContinuar.Visible = false;
            btoFinalizar.Visible = false;
        }

        private void MI_Exibir()
        {
            lblTexto1.Visible = true;
            lblTexto2.Visible = true;
            btoContinuar.Visible = true;
            btoFinalizar.Visible = true;
        }

        private bool MI_VerificarCredencialCancelada(string prCodigoBarras)
        {
            bool boResult = false;
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();

            foreach (DataRow Row in IAT_Visitante.MS_ObterCodigoCredencial(prCodigoBarras).Rows)
            {
                if (Row["Situacao"].ToString() == "ATIVO")
                {
                    return boResult = true;
                }
            }

            return boResult;
        }

        private void MI_ExibirMensagem(string prMensagem)
        {
            Clt_MensagemBox oPEN = new Clt_MensagemBox();
            oPEN.AP_Mensagem = prMensagem;
            oPEN.ShowDialog();
        }
        private void MI_MensagemSucesso(String stMensagsem)
        {
            Clt_Mensagem abrirMensagem = new Clt_Mensagem();
            abrirMensagem.AP_Mensagem = stMensagsem;
            abrirMensagem.ShowDialog();
        }

        private void MI_LocalizarRegistro(string prContendo)
        {
            DataTable dtRegistros = new DataTable();
            CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();

            if (MI_VerificarCredencialCancelada(prContendo) == false)
            {
                MI_ExibirMensagem("Esta credencial foi cancelada ou n�o foi encontrado itens vinculados");
                return;
            }
            else
            {
                try { prContendo = prContendo.Substring(0, 10); }
                catch { }
            }

            dtRegistros = IAT_CadastrarVisitante.MS_ObterPorCodigoDataConsultora(prContendo);

            MI_Limpar();

            if (dtRegistros.Rows.Count > 0)
            {
                // VERIFICAR SE A CONSULTORA ESTA NO SEMINARIO
                if ((dtRegistros.Rows[0]["TipodeInscricao"].ToString().ToUpper().Contains(new CSV_VariaveisGlobal().getSemiario().ToString().ToUpper())) || (new CSV_VariaveisGlobal().getSemiario().ToString().ToUpper().Contains(dtRegistros.Rows[0]["TipodeInscricao"].ToString().ToUpper())))
                {
                    // CONTINUA ESTA NO SEMINARIO
                }
                else
                {
                    MI_ExibirMensagem("Esta credencial n�o esta inscrita para este semin�rio");
                    return;
                }

                lblCodigo.Text = dtRegistros.Rows[0]["CodigoBarras"].ToString();

                stCodigoBarras = dtRegistros.Rows[0]["CodigoBarras"].ToString();

                string stNrConsultora = dtRegistros.Rows[0]["Consultora"].ToString();

                DataTable dtPremiacoes = IAT_CadastrarVisitante.MS_ObterPremiacoes(stNrConsultora);

                if (dtPremiacoes.Rows.Count == 0)
                {
                    MI_Limpar();
                }
                else
                {
                    MI_Exibir();

                    lblConsultora.Text = dtRegistros.Rows[0]["NomedaConsultora"].ToString();

                    foreach (DataRow Row in dtPremiacoes.Rows)
                    {
                        // LISTAR OS RECONHECIMENTOS
                        lblCortes.Text += Row["dsReconhecimento"].ToString() + Environment.NewLine;
                    }

                    // Posicionar Texto 2

                    lblTexto2.Location = new Point(lblTexto2.Location.X, lblCortes.Location.Y + lblCortes.Size.Height + 25);

                    if (new CSV_VariaveisGlobal().getPrioridade() == "3")
                    {
                        if (MI_VerificarBrindeExtra(stNrConsultora))
                        {
                            Clt_BrindeAdicional open = new Clt_BrindeAdicional();
                            open.ShowDialog();
                        }
                    }

                }
            }
        }

        private bool MI_VerificarBrindeExtra(string prConsultora)
        {
            bool boResult = false;

            if (prConsultora == "002302") return true;
            if (prConsultora == "007579") return true;
            if (prConsultora == "009440") return true;
            if (prConsultora == "005857") return true;
            if (prConsultora == "010576") return true;

            if (prConsultora == "014496") return true;
            if (prConsultora == "010465") return true;
            if (prConsultora == "005017") return true;
            if (prConsultora == "010284") return true;

            if (prConsultora == "000556") return true;
            if (prConsultora == "001912") return true;
            if (prConsultora == "007468") return true;
            if (prConsultora == "007636") return true;
            if (prConsultora == "012098") return true;
            if (prConsultora == "004593") return true;
            if (prConsultora == "006472") return true;
            if (prConsultora == "000124") return true;
            if (prConsultora == "000297") return true;
            if (prConsultora == "001324") return true;

            return boResult;
        }

        private void MI_PosicionarPainel()
        {
            pnlPainelTexto.Location = new Point((this.Size.Width / 2) - (pnlPainelTexto.Size.Width / 2), pnlPainelTexto.Location.Y + 100);
            btoRealizarEntrega.Location = new Point(Screen.PrimaryScreen.Bounds.Width - 50, btoRealizarEntrega.Location.Y);
        }

        private void CarregaUsuario()
        {
            lblUsuario.Text = CSV_Global.getUsuario();
            lblTerminal.Text = CSV_Global.getNumeroTerminal();
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void CLT_LocalizarRegistro_KeyPress(object sender, KeyPressEventArgs e)
        {
            int itTecla = e.KeyChar;
            switch (itTecla.ToString())
            {
                case "13":

                    if (!AP_TrocaDeProduto)
                    {
                        MI_LocalizarRegistro(edtPalavra.Text.Trim());
                        edtPalavra.Text = string.Empty;
                    }
                    else
                    {
                        MI_LocalizaRegistroParaCancelamento(edtPalavra.Text.Trim());
                        edtPalavra.Text = string.Empty;
                    }
                    break;
                case "27":
                    this.Close();
                    break;
            }
        }



        private void MI_LocalizaRegistroParaCancelamento(string prContendo)
        {
            DataTable dtRegistros = new DataTable();
            CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();

            if (MI_VerificarCredencialCancelada(prContendo) == false)
            {
                MI_ExibirMensagem("Esta credencial foi cancelada ou n�o foi encontrado itens vinculados");
                return;
            }
            else
            {
                try { prContendo = prContendo.Substring(0, 10); }
                catch { }
            }

            dtRegistros = IAT_CadastrarVisitante.MS_ObterPorCodigoDataConsultora(prContendo);

            MI_Limpar();

            string stNrConsultora = dtRegistros.Rows[0]["Consultora"].ToString();
            String stNomeConsultora = dtRegistros.Rows[0]["NomedaConsultora"].ToString();

            CLT_CancelamentoProduto abrirCancelamento = new CLT_CancelamentoProduto();
            abrirCancelamento.AP_CodigoBarras = prContendo;
            abrirCancelamento.AP_NumeroConsultora = stNrConsultora;
            abrirCancelamento.AP_NomeConsultora = stNomeConsultora;
            abrirCancelamento.ShowDialog();

            if (abrirCancelamento.AP_ExibeMensagemSucesso)
            {
                MI_MensagemSucesso("Cancelamento de entrega realizado com sucesso.");
            }
            AP_TrocaDeProduto = false;
            btoRealizarEntrega.Visible = false;
            btoCancelarEntrega.Visible = true;
            lblMensagemInicial.Text = "POR FAVOR, FA�A A LEITURA DA CREDENCIAL";
        }

        private void tmrTempo_Tick(object sender, EventArgs e)
        {
            lblDataHora.Text = System.DateTime.Today.ToString().Replace("00:00:00", "") + " - " + CSV_FuncoesGlobais.MS_GetTime().ToString().Substring(0, 5);
        }

        private void rbtCodigo_CheckedChanged_1(object sender, EventArgs e)
        {
            edtPalavra.Focus();
        }

        private void rbtCodigoBarras_CheckedChanged_1(object sender, EventArgs e)
        {
            edtPalavra.Focus();
        }

        private void btoIncluirNovo_Click(object sender, EventArgs e)
        {
        }

        private void btoContinuar_Click(object sender, EventArgs e)
        {
            MI_Continuar();
        }

        private void MI_Continuar()
        {
            if (lblCodigo.Text.Trim() != string.Empty)
            {
                CLT_Reconhecimento open = new CLT_Reconhecimento();
                open.AP_CodigoBarras = lblCodigo.Text;
                open.AP_NomeConsultora = lblConsultora.Text;

                open.ShowDialog();

                MI_AnalisarSeTemItemPendente(open.boResultSair);
            }
        }

        private void MI_AnalisarSeTemItemPendente(bool boSair)
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtItem = IAT_Visitante.MS_ObterTodosOsItens(stCodigoBarras);

            bool boTudoEntregue = true;
            foreach (DataRow Row in dtItem.Rows)
            {
                if (Row["dsRetirado"].ToString() == "N")
                    boTudoEntregue = false;
            }

            if ((boTudoEntregue == true) || (boSair == true))
            {
                MI_Finalizar();
            }
            else
            {
                MI_Continuar();
            }
        }

        private void MI_Finalizar()
        {
            Ctl_Finalizacao Open = new Ctl_Finalizacao();
            Open.AP_NomeConsultora = lblConsultora.Text;
            Open.AP_CodigoBarras = stCodigoBarras;
            Open.ShowDialog();

            MI_Limpar();
        }

        private void btoFinalizar_Click(object sender, EventArgs e)
        {
            MI_Finalizar();
        }

        private void lblConsultora_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btoTrocarJoia_Click(object sender, EventArgs e)
        {

        }

        private void btoCancelarEntrega_Click(object sender, EventArgs e)
        {
            CLT_SenhaCancelamento abrirCancelamento = new CLT_SenhaCancelamento();
            abrirCancelamento.ShowDialog();

            if (abrirCancelamento.AP_Result)
            {
                AP_TrocaDeProduto = true;
                lblMensagemInicial.Text = "LEIA O C�DIGO DE BARRAS PARA CANCELAR A ENTREGA";
                btoCancelarEntrega.Visible = false;
                btoRealizarEntrega.Visible = true;
            }
            else
            {
                AP_TrocaDeProduto = false;
                lblMensagemInicial.Text = "POR FAVOR, FA�A A LEITURA DA CREDENCIAL";
                btoCancelarEntrega.Visible = true;
                btoRealizarEntrega.Visible = false;
            }
        }

        private void btoRealizarEntrega_Click(object sender, EventArgs e)
        {
            AP_TrocaDeProduto = false;
            lblMensagemInicial.Text = "POR FAVOR, FA�A A LEITURA DA CREDENCIAL";
            btoCancelarEntrega.Visible = true;
            btoRealizarEntrega.Visible = false;
        }
    }
}