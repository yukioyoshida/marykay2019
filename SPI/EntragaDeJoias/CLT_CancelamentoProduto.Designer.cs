﻿namespace SPI
{
    partial class CLT_CancelamentoProduto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlPainelTexto = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlReconhecimento = new System.Windows.Forms.Panel();
            this.lblConsultora = new System.Windows.Forms.Label();
            this.pnlPainelTexto.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlPainelTexto
            // 
            this.pnlPainelTexto.Controls.Add(this.lblConsultora);
            this.pnlPainelTexto.Controls.Add(this.label1);
            this.pnlPainelTexto.Controls.Add(this.pnlReconhecimento);
            this.pnlPainelTexto.Location = new System.Drawing.Point(44, 55);
            this.pnlPainelTexto.Name = "pnlPainelTexto";
            this.pnlPainelTexto.Size = new System.Drawing.Size(1022, 467);
            this.pnlPainelTexto.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1016, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "SELECIONE O ÍTEM QUE DESEJA CANCELAR";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlReconhecimento
            // 
            this.pnlReconhecimento.Location = new System.Drawing.Point(3, 91);
            this.pnlReconhecimento.Name = "pnlReconhecimento";
            this.pnlReconhecimento.Size = new System.Drawing.Size(1016, 232);
            this.pnlReconhecimento.TabIndex = 0;
            // 
            // lblConsultora
            // 
            this.lblConsultora.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblConsultora.BackColor = System.Drawing.Color.Transparent;
            this.lblConsultora.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsultora.ForeColor = System.Drawing.Color.Black;
            this.lblConsultora.Location = new System.Drawing.Point(43, 354);
            this.lblConsultora.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblConsultora.Name = "lblConsultora";
            this.lblConsultora.Size = new System.Drawing.Size(932, 65);
            this.lblConsultora.TabIndex = 83;
            this.lblConsultora.Text = "[NOME]";
            this.lblConsultora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CLT_CancelamentoProduto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1102, 853);
            this.Controls.Add(this.pnlPainelTexto);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CLT_CancelamentoProduto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CLT_CancelamentoProduto";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CLT_CancelamentoProduto_Load);
            this.pnlPainelTexto.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlPainelTexto;
        private System.Windows.Forms.Panel pnlReconhecimento;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblConsultora;
    }
}