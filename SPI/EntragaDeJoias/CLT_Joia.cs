using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_Joia : Form
    {
        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        public string AP_Reconhecimento { get; set; }
        public string AP_CodigoBarras { get; set; }
        public string AP_NrConsultora { get; set; }
        public string AP_NomeConsultora { get; set; }
        public string AP_CdReconhecimento { get; set; }
        public string AP_NomeReconhecimento { get; set; }

        public string AP_CdItemJoia { get; set; }

        public CLT_Joia()
        {
            InitializeComponent();
        }

        private void CarregaBackground()
        {
            this.BackgroundImage = Properties.Resources.BackgroundMaryKay;
        }

        private void CLT_LocalizarRegistro_Load(object sender, EventArgs e)
        {
            System.Threading.Thread ts = new System.Threading.Thread(CarregaBackground);
            ts.Start();

            MI_PosicionarPainel();
            lblConsultora.Text = AP_NomeConsultora;
            lblCorteSelecionada.Text = lblCorteSelecionada.Text.Replace("[lblReconhecimento]", AP_NomeReconhecimento);
            AP_Reconhecimento = AP_NomeReconhecimento;
            lblCorteSelecionada.Visible = true;
            MI_CarregarPremios();
        }
       

        private void MI_PosicionarPainel()
        {
            pnlPainelTexto.Location = new Point((this.Size.Width / 2) - (pnlPainelTexto.Size.Width / 2), pnlPainelTexto.Location.Y); 
        }

        private void MI_LimparPremios()
        {
            AP_CdItemJoia = "";

            for(int i=1; i<=10; i++)
            {
                PictureBox img = (PictureBox)pnlPremios.Controls["img" + i.ToString()];
                img.Visible = false;

                Button bto = (Button)pnlPremios.Controls["bto" + i.ToString()];
                bto.Visible = false;

                Label lblR_ = (Label)pnlPremios.Controls["lblR_" + i.ToString()];
                lblR_.Visible = false;
            }

        }

        private void MI_CarregarPremios()
        {
            MI_LimparPremios();
            
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();

            // PREMIOS JOIA
            DataTable dtPremios = IAT_Visitante.MS_ObterPremiosDaConsultora(AP_CdReconhecimento, "PREMIO");

            bool boJoiaEscolhida = false;

            if (dtPremios.Rows.Count > 0)
            {
                DataRow Row = dtPremios.Rows[0];

                AP_CdItemJoia = Row["cdItem"].ToString();

                if (Row["dsTrocaLocal"].ToString().ToUpper() != string.Empty)
                {
                    new InserirFoto().MS_SetFoto(img1, Row["dsTrocaLocal"].ToString().ToUpper());
                    img1.Visible = true;
                    bto1.Visible = true;

                    //FAIXA PARA RETIRADO
                    if (Row["dsRetirado"].ToString().ToUpper() == "S")
                    {
                        lblR_1.Visible = true;
                        bto1.Visible = false;
                    }

                    boJoiaEscolhida = true;
                    bto1.Text = "RETIRAR";
                    bto1.Tag = Row["dsTrocaLocal"].ToString().ToUpper();

                    // ICONE HELP
                    PictureBox imgHelp = (PictureBox)pnlPremios.Controls["imgAjudaPremio1"];

                    imgHelp.Tag = new DAO_Visitante().MS_ObterRoteiroPorIdRoteiro(bto1.Tag.ToString()).Rows[0]["dsReconhecimento"].ToString();
                    imgHelp.Visible = true;

                    if (CSV_Global.getTipoJoia() != "FAIXA")
                    {
                        btoCorreios1.Visible = true;
                        btoCorreios1.Tag = Row["dsTrocaLocal"].ToString().ToUpper();
                    }
                }


                // N�O ESCOLHEU O ITEM Habilitar todos os itens para escolha
                if (boJoiaEscolhida == false)
                {
                    lblEscolheuOuNaoEscolheu.Text = "Identificamos que voc� ainda n�o escolheu o seu pr�mio. Voc� pode escolher entre as op��es abaixo.";

                    // OBTER OS ITENS PARA ESCOLHA
                    DataTable dtPremiosEscolha = new DAO_Visitante().MS_ObterTodosOsPremio();
                    int itEscolha = 1;
                    foreach(DataRow RowPremio in dtPremiosEscolha.Rows)
                    {
                        if (MI_ObterPermissaoDisponibilizacao(RowPremio["dsReconhecimento"].ToString()))
                        {
                            PictureBox img = (PictureBox)pnlPremios.Controls["img" + itEscolha.ToString()];
                            new InserirFoto().MS_SetFoto(img, RowPremio["cdRoteiro"].ToString());
                            img.Visible = true;

                            Button bto = (Button)pnlPremios.Controls["bto" + itEscolha.ToString()];
                            bto.Visible = true;
                            bto.Text = "ESCOLHER";
                            bto.Tag = RowPremio["cdRoteiro"].ToString();

                            // ICONE HELP
                            PictureBox imgHelp = (PictureBox)pnlPremios.Controls["imgAjudaPremio" + itEscolha.ToString()];

                            imgHelp.Tag = new DAO_Visitante().MS_ObterRoteiroPorIdRoteiro(bto.Tag.ToString()).Rows[0]["dsReconhecimento"].ToString();
                            imgHelp.Visible = true;


                            itEscolha++;
                        }
                    }
                }
                else
                {
                    lblEscolheuOuNaoEscolheu.Text = "Voc� j� escolheu o seu pr�mio abaixo:";
                }
            }

            // PREMIOS MIMO
            //DataTable dtPremiosMimo = IAT_Visitante.MS_ObterPremiosDaConsultora(AP_CdReconhecimento, "MIMO");
            //foreach (DataRow RowMimo in dtPremiosMimo.Rows)
            //{
            //    img5.Visible = true;
            //    img5.ImageLocation = "ImagensPremios/" + RowMimo["dsTrocaLocal"].ToString() + ".png";

            //    lblR_5.Visible = false;
            //    bto5.Visible = true;

            //    if (RowMimo["dsRetirado"].ToString() == "S")
            //    {
            //        lblR_5.Visible = true;
            //        bto5.Visible = false;
            //    }

            //    bto5.Tag = RowMimo["cdItem"].ToString();

            //    lblEscolheuOuNaoEscolheu.Text = "Veja abaixo o premio que voc� ir� retirar";
            //}

            //MI_RealinharProdutos();
        }

        private bool MI_ObterPermissaoDisponibilizacao(string prNomePremio)
        {
            bool boResult = false;
            AP_Reconhecimento = AP_Reconhecimento.Trim();
            switch (prNomePremio)
            {
                case "Corrente em ouro ros� 18k de 46cm com pingente de quartzo rosa 5x5mm":
                    if ((AP_Reconhecimento == "Corte de VP Consultoras") ||
                        (AP_Reconhecimento == "Corte de VP Diretoras") ||
                        (AP_Reconhecimento == "Corte de Inicia��o") ||
                        (AP_Reconhecimento == "Corte das Unidades - Circulo dos 500 mil pontos")
                        )
                    {
                        boResult = true;
                    }
                    break;
                case "Rel�gio Vivara em a�o prateado e dourado":
                    if ((AP_Reconhecimento == "Corte de VP Consultoras") ||
                    (AP_Reconhecimento == "Corte de VP Diretoras") ||
                    (AP_Reconhecimento == "Corte de Inicia��o") ||
                    (AP_Reconhecimento == "Corte das Unidades - Circulo dos 500 mil pontos")
                    )
                    {
                        boResult = true;
                    }
                    break;
                case "Rel�gio Vivara em a�o prateado e ros�":
                    if ((AP_Reconhecimento == "Corte de VP Consultoras") ||
                    (AP_Reconhecimento == "Corte de VP Diretoras") ||
                    (AP_Reconhecimento == "Corte de Inicia��o") ||
                    (AP_Reconhecimento == "Corte das Unidades - Circulo dos 500 mil pontos")
                    )
                    {
                        boResult = true;
                    }
                    break;
                case "Broche de abelha tamanho m�dio em ouro amarelo 18k, com efeitos brilhantes e olhos de esmeralda":
                    if (AP_Reconhecimento == "Corte de Inicia��o")
                    {
                        boResult = true;
                    }
                    break;
                case "Rel�gio Tommy Hilfiger em a�o cor chumbo":
                    if (AP_Reconhecimento == "Corte das Unidades - Circulo dos 600 mil pontos")
                    {
                        boResult = true;
                    }
                    break;
                case "Rel�gio Vivara Ros� Beg�nia com Brilhantes":
                    if (AP_Reconhecimento == "Corte das Unidades - Circulo dos 600 mil pontos")
                    {
                        boResult = true;
                    }
                    break;
                case "Corrente em ouro ros� 18k de 46cm com pingente de quartzo rosa 6x6mm com diamante":
                    if (AP_Reconhecimento == "Corte das Unidades - Circulo dos 600 mil pontos")
                    {
                        boResult = true;
                    }
                    break;
                default:
                    boResult = false;
                    break;
            }


            return boResult;
        }

        private string MI_ObterDescricaoJoia(string prCdJoia)
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            return IAT_Visitante.MS_ObterPremioNew(prCdJoia).Rows[0]["Descricao"].ToString();
        }

        
        private void btoAcao_Click(object sender, EventArgs e)
        {
            Button btoAcao = (Button)sender;
            if (btoAcao.Text == "ESCOLHER")
                MI_Escolher(btoAcao.Tag.ToString(), "ESCOLHER");
            else
                MI_Escolher(btoAcao.Tag.ToString(), "RETIRAR");
        }

        private void btoCorreio_Click(object sender, EventArgs e)
        {
            Button btoAcao = (Button)sender;
            MI_Escolher(btoAcao.Tag.ToString(), "RETIRAR_CORREIOS");
        }

        private void MI_Escolher(string prJoia, string prAcao)
        {
            if (prAcao == "ESCOLHER")
            {
                Clt_ConfirmaEscolha open = new Clt_ConfirmaEscolha();
                open.AP_Reconhecimento = AP_NomeReconhecimento;
                open.AP_Premio = prJoia;
                open.ShowDialog();

                if (open.AP_boResult == true)
                {
                    CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
                    IAT_Visitante.MS_EscolherJoiaLocal(AP_CdItemJoia, prJoia);
                }

                MI_CarregarPremios();
            }
            else if (prAcao == "RETIRAR_CORREIOS")
            {
                CLT_RetirarJoiaNew Open = new CLT_RetirarJoiaNew();

                Open.AP_CodigoBarras = AP_CodigoBarras;
                Open.AP_NomeConsultora = AP_NomeConsultora;
                Open.AP_NrConsultora = MI_ObterNumeroConsultora(AP_CodigoBarras);
                Open.AP_Premio = prJoia;
                Open.AP_CdItemJoia = AP_CdItemJoia;
                Open.AP_Correio = "S";

                Open.ShowDialog();

                this.Close();
            }
            else
            {
                CLT_RetirarJoiaNew Open = new CLT_RetirarJoiaNew();

                Open.AP_CodigoBarras = AP_CodigoBarras;
                Open.AP_NomeConsultora = AP_NomeConsultora;
                Open.AP_NrConsultora = MI_ObterNumeroConsultora(AP_CodigoBarras);
                Open.AP_Premio = prJoia;
                Open.AP_CdItemJoia = AP_CdItemJoia;
                Open.AP_Correio = "N";

                Open.ShowDialog();

                this.Close();
            }
        }

        private string MI_ObterNumeroConsultora(string prCodigoBarras)
        {
            string stResult = string.Empty;

            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            CTO_Visitante ITO_Visitante = IAT_Visitante.MS_ObterPorCodigo(prCodigoBarras);

            try
            {
                stResult = ITO_Visitante.getConsultora().ToString();
            }
            catch { }

            return stResult;
        }

        private void lblConsultora_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MI_RealinharProdutos()
        {

            //// ITEM 1 - N�o � necess�rio alinhar

            /* ITEM 2
            Zerar posicionamento*/
            img2.Location = new Point(img1.Location.X, img1.Location.Y);
            lblR_2.Location = new Point(lblR_1.Location.X, lblR_1.Location.Y);
            bto2.Location = new Point(bto1.Location.X, bto1.Location.Y);
            /* Posiciona o ITEM 2 */
            if (img1.Visible == true)
            {
                img2.Location = new Point(img1.Location.X + 180, img2.Location.Y);
                lblR_2.Location = new Point(lblR_1.Location.X + 180, lblR_2.Location.Y);
                bto2.Location = new Point(bto1.Location.X + 180, bto2.Location.Y);
            }

            /* ITEM 3
            Zerar posicionamento*/
            img3.Location = new Point(img1.Location.X, img1.Location.Y);
            lblR_3.Location = new Point(lblR_1.Location.X, lblR_1.Location.Y);
            bto3.Location = new Point(bto1.Location.X, bto1.Location.Y);
            /* Posiciona o ITEM 3 */
            if (img1.Visible == true)
            {
                img3.Location = new Point(img1.Location.X + 180, img3.Location.Y);
                lblR_3.Location = new Point(lblR_1.Location.X + 180, lblR_3.Location.Y);
                bto3.Location = new Point(bto1.Location.X + 180, bto3.Location.Y);
            }
            if (img2.Visible == true)
            {
                img3.Location = new Point(img2.Location.X + 180, img3.Location.Y);
                lblR_3.Location = new Point(lblR_2.Location.X + 180, lblR_3.Location.Y);
                bto3.Location = new Point(bto2.Location.X + 180, bto3.Location.Y);
            }

            /* ITEM 4
            Zerar posicionamento*/
            img4.Location = new Point(img1.Location.X, img1.Location.Y);
            lblR_4.Location = new Point(lblR_1.Location.X, lblR_1.Location.Y);
            bto4.Location = new Point(bto1.Location.X, bto1.Location.Y);
            /* Posiciona o ITEM 4 */
            if (img1.Visible == true)
            {
                img4.Location = new Point(img1.Location.X + 180, img3.Location.Y);
                lblR_4.Location = new Point(lblR_1.Location.X + 180, lblR_3.Location.Y);
                bto4.Location = new Point(bto1.Location.X + 180, bto3.Location.Y);
            }
            if (img2.Visible == true)
            {
                img4.Location = new Point(img2.Location.X + 180, img3.Location.Y);
                lblR_4.Location = new Point(lblR_2.Location.X + 180, lblR_3.Location.Y);
                bto4.Location = new Point(bto2.Location.X + 180, bto3.Location.Y);
            }
            if (img3.Visible == true)
            {
                img4.Location = new Point(img3.Location.X + 180, img3.Location.Y);
                lblR_4.Location = new Point(lblR_3.Location.X + 180, lblR_3.Location.Y);
                bto4.Location = new Point(bto3.Location.X + 180, bto3.Location.Y);
            }

            /* ITEM 4
            Zerar posicionamento*/
            img5.Location = new Point(img1.Location.X, img1.Location.Y);
            lblR_5.Location = new Point(lblR_1.Location.X, lblR_1.Location.Y);
            bto5.Location = new Point(bto1.Location.X, bto1.Location.Y);

        }

        private void Help_Click(object sender, EventArgs e)
        {
            PictureBox bto = (PictureBox)sender;
            string img = bto.Name.ToString().Replace("imgAjudaPremio", "");
            PictureBox pic = (PictureBox)pnlPremios.Controls["img" + img.ToString()];

            EntragaDeJoias.Clt_DetalheJoia open = new EntragaDeJoias.Clt_DetalheJoia();
            open.ImgFoto = pic;
            open.AP_Descricao = bto.Tag.ToString();

            open.ShowDialog();
        }
    }   
}
 