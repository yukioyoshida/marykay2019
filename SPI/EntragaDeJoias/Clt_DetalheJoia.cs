﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPI.EntragaDeJoias
{
    public partial class Clt_DetalheJoia : Form
    {
        public PictureBox ImgFoto;
        public string AP_Descricao;
        public Clt_DetalheJoia()
        {
            InitializeComponent();
        }

        private void btoFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Clt_DetalheJoia_Load(object sender, EventArgs e)
        {
            picImagem.Image = ImgFoto.Image;
            lblDescricao.Text = AP_Descricao;
        }
    }
}
