﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class Clt_ConfirmaEscolha : Form
    {
        public string   AP_Reconhecimento { get; set; }
        public string   AP_Premio { get; set; }
        public bool     AP_boResult { get; set; } 

        public Clt_ConfirmaEscolha()
        {
            InitializeComponent();
        }

        private void Clt_ConfirmaEscolha_Load(object sender, EventArgs e)
        {
            AP_boResult = false;
            lblReconhecimento.Text = AP_Reconhecimento;
            MI_ExibirImagem();
        }

        private void MI_ExibirImagem()
        {

            //imgPremio.ImageLocation = "ImagensPremios/" + AP_Premio + ".png";

            PictureBox image = (PictureBox)imgPremio;
            new InserirFoto().MS_SetFoto(image, AP_Premio);
        }

        private void btoNAO_Click(object sender, EventArgs e)
        {
            AP_boResult = false;
            this.Close();
        }

        private void btoSIM_Click(object sender, EventArgs e)
        {
            AP_boResult = true;
            this.Close();
        }
    }
}
