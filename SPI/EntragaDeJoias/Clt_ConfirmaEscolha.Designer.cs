﻿namespace SPI
{
    partial class Clt_ConfirmaEscolha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCorteSelecionada = new System.Windows.Forms.Label();
            this.imgPremio = new System.Windows.Forms.PictureBox();
            this.btoNAO = new System.Windows.Forms.Button();
            this.btoSIM = new System.Windows.Forms.Button();
            this.lblReconhecimento = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCorteSelecionada
            // 
            this.lblCorteSelecionada.AutoSize = true;
            this.lblCorteSelecionada.BackColor = System.Drawing.Color.Transparent;
            this.lblCorteSelecionada.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorteSelecionada.ForeColor = System.Drawing.Color.Gray;
            this.lblCorteSelecionada.Location = new System.Drawing.Point(67, 39);
            this.lblCorteSelecionada.Name = "lblCorteSelecionada";
            this.lblCorteSelecionada.Size = new System.Drawing.Size(450, 29);
            this.lblCorteSelecionada.TabIndex = 71;
            this.lblCorteSelecionada.Text = "Você confirma a escolha do item abaixo?";
            // 
            // imgPremio
            // 
            this.imgPremio.Location = new System.Drawing.Point(163, 85);
            this.imgPremio.Name = "imgPremio";
            this.imgPremio.Size = new System.Drawing.Size(265, 265);
            this.imgPremio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPremio.TabIndex = 72;
            this.imgPremio.TabStop = false;
            // 
            // btoNAO
            // 
            this.btoNAO.BackColor = System.Drawing.Color.White;
            this.btoNAO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btoNAO.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoNAO.Location = new System.Drawing.Point(72, 396);
            this.btoNAO.Name = "btoNAO";
            this.btoNAO.Size = new System.Drawing.Size(158, 50);
            this.btoNAO.TabIndex = 73;
            this.btoNAO.TabStop = false;
            this.btoNAO.Text = "NÃO";
            this.btoNAO.UseVisualStyleBackColor = false;
            this.btoNAO.Click += new System.EventHandler(this.btoNAO_Click);
            // 
            // btoSIM
            // 
            this.btoSIM.BackColor = System.Drawing.Color.White;
            this.btoSIM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btoSIM.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoSIM.Location = new System.Drawing.Point(342, 396);
            this.btoSIM.Name = "btoSIM";
            this.btoSIM.Size = new System.Drawing.Size(158, 50);
            this.btoSIM.TabIndex = 74;
            this.btoSIM.TabStop = false;
            this.btoSIM.Text = "SIM";
            this.btoSIM.UseVisualStyleBackColor = false;
            this.btoSIM.Click += new System.EventHandler(this.btoSIM_Click);
            // 
            // lblReconhecimento
            // 
            this.lblReconhecimento.AutoSize = true;
            this.lblReconhecimento.BackColor = System.Drawing.Color.Transparent;
            this.lblReconhecimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReconhecimento.ForeColor = System.Drawing.Color.Gray;
            this.lblReconhecimento.Location = new System.Drawing.Point(67, 6);
            this.lblReconhecimento.Name = "lblReconhecimento";
            this.lblReconhecimento.Size = new System.Drawing.Size(221, 29);
            this.lblReconhecimento.TabIndex = 75;
            this.lblReconhecimento.Text = "lblReconhecimento";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(605, 471);
            this.panel1.TabIndex = 76;
            // 
            // Clt_ConfirmaEscolha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(608, 475);
            this.Controls.Add(this.lblReconhecimento);
            this.Controls.Add(this.btoSIM);
            this.Controls.Add(this.btoNAO);
            this.Controls.Add(this.imgPremio);
            this.Controls.Add(this.lblCorteSelecionada);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Clt_ConfirmaEscolha";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Clt_ConfirmaEscolha";
            this.Load += new System.EventHandler(this.Clt_ConfirmaEscolha_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCorteSelecionada;
        private System.Windows.Forms.PictureBox imgPremio;
        private System.Windows.Forms.Button btoNAO;
        private System.Windows.Forms.Button btoSIM;
        private System.Windows.Forms.Label lblReconhecimento;
        private System.Windows.Forms.Panel panel1;
    }
}