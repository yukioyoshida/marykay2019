namespace SPI
{
    partial class CLT_Joia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tmrTempo = new System.Windows.Forms.Timer(this.components);
            this.lblConsultora = new System.Windows.Forms.Label();
            this.pnlPainelTexto = new System.Windows.Forms.Panel();
            this.pnlPremios = new System.Windows.Forms.Panel();
            this.imgAjudaPremio10 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio9 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio8 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio7 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio6 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio5 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio4 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio3 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio2 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio1 = new System.Windows.Forms.PictureBox();
            this.btoCorreios1 = new System.Windows.Forms.Button();
            this.lblR_10 = new System.Windows.Forms.Label();
            this.bto10 = new System.Windows.Forms.Button();
            this.img10 = new System.Windows.Forms.PictureBox();
            this.lblR_9 = new System.Windows.Forms.Label();
            this.bto9 = new System.Windows.Forms.Button();
            this.img9 = new System.Windows.Forms.PictureBox();
            this.lblR_8 = new System.Windows.Forms.Label();
            this.bto8 = new System.Windows.Forms.Button();
            this.img8 = new System.Windows.Forms.PictureBox();
            this.lblR_7 = new System.Windows.Forms.Label();
            this.bto7 = new System.Windows.Forms.Button();
            this.img7 = new System.Windows.Forms.PictureBox();
            this.lblR_6 = new System.Windows.Forms.Label();
            this.bto6 = new System.Windows.Forms.Button();
            this.img6 = new System.Windows.Forms.PictureBox();
            this.lblR_4 = new System.Windows.Forms.Label();
            this.bto4 = new System.Windows.Forms.Button();
            this.img4 = new System.Windows.Forms.PictureBox();
            this.lblR_5 = new System.Windows.Forms.Label();
            this.bto5 = new System.Windows.Forms.Button();
            this.img5 = new System.Windows.Forms.PictureBox();
            this.lblEscolheuOuNaoEscolheu = new System.Windows.Forms.Label();
            this.lblR_3 = new System.Windows.Forms.Label();
            this.lblR_2 = new System.Windows.Forms.Label();
            this.lblR_1 = new System.Windows.Forms.Label();
            this.lblCorteSelecionada = new System.Windows.Forms.Label();
            this.bto3 = new System.Windows.Forms.Button();
            this.bto2 = new System.Windows.Forms.Button();
            this.bto1 = new System.Windows.Forms.Button();
            this.img3 = new System.Windows.Forms.PictureBox();
            this.img2 = new System.Windows.Forms.PictureBox();
            this.img1 = new System.Windows.Forms.PictureBox();
            this.pnlPainelTexto.SuspendLayout();
            this.pnlPremios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img1)).BeginInit();
            this.SuspendLayout();
            // 
            // tmrTempo
            // 
            this.tmrTempo.Enabled = true;
            this.tmrTempo.Interval = 6000;
            // 
            // lblConsultora
            // 
            this.lblConsultora.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblConsultora.BackColor = System.Drawing.Color.Transparent;
            this.lblConsultora.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsultora.ForeColor = System.Drawing.Color.Black;
            this.lblConsultora.Location = new System.Drawing.Point(33, 632);
            this.lblConsultora.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblConsultora.Name = "lblConsultora";
            this.lblConsultora.Size = new System.Drawing.Size(1090, 65);
            this.lblConsultora.TabIndex = 67;
            this.lblConsultora.Text = "[NOME]";
            this.lblConsultora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblConsultora.Click += new System.EventHandler(this.lblConsultora_Click);
            // 
            // pnlPainelTexto
            // 
            this.pnlPainelTexto.BackColor = System.Drawing.Color.Transparent;
            this.pnlPainelTexto.Controls.Add(this.pnlPremios);
            this.pnlPainelTexto.Location = new System.Drawing.Point(16, 52);
            this.pnlPainelTexto.Margin = new System.Windows.Forms.Padding(4);
            this.pnlPainelTexto.Name = "pnlPainelTexto";
            this.pnlPainelTexto.Size = new System.Drawing.Size(1138, 631);
            this.pnlPainelTexto.TabIndex = 71;
            // 
            // pnlPremios
            // 
            this.pnlPremios.BackColor = System.Drawing.Color.Transparent;
            this.pnlPremios.Controls.Add(this.imgAjudaPremio10);
            this.pnlPremios.Controls.Add(this.imgAjudaPremio9);
            this.pnlPremios.Controls.Add(this.imgAjudaPremio8);
            this.pnlPremios.Controls.Add(this.imgAjudaPremio7);
            this.pnlPremios.Controls.Add(this.imgAjudaPremio6);
            this.pnlPremios.Controls.Add(this.imgAjudaPremio5);
            this.pnlPremios.Controls.Add(this.imgAjudaPremio4);
            this.pnlPremios.Controls.Add(this.imgAjudaPremio3);
            this.pnlPremios.Controls.Add(this.imgAjudaPremio2);
            this.pnlPremios.Controls.Add(this.imgAjudaPremio1);
            this.pnlPremios.Controls.Add(this.btoCorreios1);
            this.pnlPremios.Controls.Add(this.lblR_10);
            this.pnlPremios.Controls.Add(this.bto10);
            this.pnlPremios.Controls.Add(this.img10);
            this.pnlPremios.Controls.Add(this.lblR_9);
            this.pnlPremios.Controls.Add(this.bto9);
            this.pnlPremios.Controls.Add(this.img9);
            this.pnlPremios.Controls.Add(this.lblR_8);
            this.pnlPremios.Controls.Add(this.bto8);
            this.pnlPremios.Controls.Add(this.img8);
            this.pnlPremios.Controls.Add(this.lblR_7);
            this.pnlPremios.Controls.Add(this.bto7);
            this.pnlPremios.Controls.Add(this.img7);
            this.pnlPremios.Controls.Add(this.lblR_6);
            this.pnlPremios.Controls.Add(this.bto6);
            this.pnlPremios.Controls.Add(this.img6);
            this.pnlPremios.Controls.Add(this.lblR_4);
            this.pnlPremios.Controls.Add(this.bto4);
            this.pnlPremios.Controls.Add(this.img4);
            this.pnlPremios.Controls.Add(this.lblR_5);
            this.pnlPremios.Controls.Add(this.bto5);
            this.pnlPremios.Controls.Add(this.img5);
            this.pnlPremios.Controls.Add(this.lblEscolheuOuNaoEscolheu);
            this.pnlPremios.Controls.Add(this.lblR_3);
            this.pnlPremios.Controls.Add(this.lblR_2);
            this.pnlPremios.Controls.Add(this.lblR_1);
            this.pnlPremios.Controls.Add(this.lblCorteSelecionada);
            this.pnlPremios.Controls.Add(this.bto3);
            this.pnlPremios.Controls.Add(this.bto2);
            this.pnlPremios.Controls.Add(this.bto1);
            this.pnlPremios.Controls.Add(this.img3);
            this.pnlPremios.Controls.Add(this.img2);
            this.pnlPremios.Controls.Add(this.img1);
            this.pnlPremios.Location = new System.Drawing.Point(44, 16);
            this.pnlPremios.Margin = new System.Windows.Forms.Padding(4);
            this.pnlPremios.Name = "pnlPremios";
            this.pnlPremios.Size = new System.Drawing.Size(1076, 646);
            this.pnlPremios.TabIndex = 72;
            // 
            // imgAjudaPremio10
            // 
            this.imgAjudaPremio10.Image = global::SPI.Properties.Resources.help2;
            this.imgAjudaPremio10.Location = new System.Drawing.Point(1033, 362);
            this.imgAjudaPremio10.Name = "imgAjudaPremio10";
            this.imgAjudaPremio10.Size = new System.Drawing.Size(30, 30);
            this.imgAjudaPremio10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio10.TabIndex = 111;
            this.imgAjudaPremio10.TabStop = false;
            this.imgAjudaPremio10.Visible = false;
            this.imgAjudaPremio10.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio9
            // 
            this.imgAjudaPremio9.Image = global::SPI.Properties.Resources.help2;
            this.imgAjudaPremio9.Location = new System.Drawing.Point(813, 362);
            this.imgAjudaPremio9.Name = "imgAjudaPremio9";
            this.imgAjudaPremio9.Size = new System.Drawing.Size(30, 30);
            this.imgAjudaPremio9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio9.TabIndex = 110;
            this.imgAjudaPremio9.TabStop = false;
            this.imgAjudaPremio9.Visible = false;
            this.imgAjudaPremio9.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio8
            // 
            this.imgAjudaPremio8.Image = global::SPI.Properties.Resources.help2;
            this.imgAjudaPremio8.Location = new System.Drawing.Point(596, 362);
            this.imgAjudaPremio8.Name = "imgAjudaPremio8";
            this.imgAjudaPremio8.Size = new System.Drawing.Size(30, 30);
            this.imgAjudaPremio8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio8.TabIndex = 109;
            this.imgAjudaPremio8.TabStop = false;
            this.imgAjudaPremio8.Visible = false;
            this.imgAjudaPremio8.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio7
            // 
            this.imgAjudaPremio7.Image = global::SPI.Properties.Resources.help2;
            this.imgAjudaPremio7.Location = new System.Drawing.Point(380, 362);
            this.imgAjudaPremio7.Name = "imgAjudaPremio7";
            this.imgAjudaPremio7.Size = new System.Drawing.Size(30, 30);
            this.imgAjudaPremio7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio7.TabIndex = 108;
            this.imgAjudaPremio7.TabStop = false;
            this.imgAjudaPremio7.Visible = false;
            this.imgAjudaPremio7.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio6
            // 
            this.imgAjudaPremio6.Image = global::SPI.Properties.Resources.help2;
            this.imgAjudaPremio6.Location = new System.Drawing.Point(165, 362);
            this.imgAjudaPremio6.Name = "imgAjudaPremio6";
            this.imgAjudaPremio6.Size = new System.Drawing.Size(30, 30);
            this.imgAjudaPremio6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio6.TabIndex = 107;
            this.imgAjudaPremio6.TabStop = false;
            this.imgAjudaPremio6.Visible = false;
            this.imgAjudaPremio6.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio5
            // 
            this.imgAjudaPremio5.Image = global::SPI.Properties.Resources.help2;
            this.imgAjudaPremio5.Location = new System.Drawing.Point(1033, 156);
            this.imgAjudaPremio5.Name = "imgAjudaPremio5";
            this.imgAjudaPremio5.Size = new System.Drawing.Size(30, 30);
            this.imgAjudaPremio5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio5.TabIndex = 106;
            this.imgAjudaPremio5.TabStop = false;
            this.imgAjudaPremio5.Visible = false;
            this.imgAjudaPremio5.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio4
            // 
            this.imgAjudaPremio4.Image = global::SPI.Properties.Resources.help2;
            this.imgAjudaPremio4.Location = new System.Drawing.Point(813, 156);
            this.imgAjudaPremio4.Name = "imgAjudaPremio4";
            this.imgAjudaPremio4.Size = new System.Drawing.Size(30, 30);
            this.imgAjudaPremio4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio4.TabIndex = 105;
            this.imgAjudaPremio4.TabStop = false;
            this.imgAjudaPremio4.Visible = false;
            this.imgAjudaPremio4.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio3
            // 
            this.imgAjudaPremio3.Image = global::SPI.Properties.Resources.help2;
            this.imgAjudaPremio3.Location = new System.Drawing.Point(596, 156);
            this.imgAjudaPremio3.Name = "imgAjudaPremio3";
            this.imgAjudaPremio3.Size = new System.Drawing.Size(30, 30);
            this.imgAjudaPremio3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio3.TabIndex = 104;
            this.imgAjudaPremio3.TabStop = false;
            this.imgAjudaPremio3.Visible = false;
            this.imgAjudaPremio3.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio2
            // 
            this.imgAjudaPremio2.Image = global::SPI.Properties.Resources.help2;
            this.imgAjudaPremio2.Location = new System.Drawing.Point(381, 156);
            this.imgAjudaPremio2.Name = "imgAjudaPremio2";
            this.imgAjudaPremio2.Size = new System.Drawing.Size(30, 30);
            this.imgAjudaPremio2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio2.TabIndex = 103;
            this.imgAjudaPremio2.TabStop = false;
            this.imgAjudaPremio2.Visible = false;
            this.imgAjudaPremio2.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio1
            // 
            this.imgAjudaPremio1.Image = global::SPI.Properties.Resources.help2;
            this.imgAjudaPremio1.Location = new System.Drawing.Point(166, 156);
            this.imgAjudaPremio1.Name = "imgAjudaPremio1";
            this.imgAjudaPremio1.Size = new System.Drawing.Size(30, 30);
            this.imgAjudaPremio1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio1.TabIndex = 102;
            this.imgAjudaPremio1.TabStop = false;
            this.imgAjudaPremio1.Visible = false;
            this.imgAjudaPremio1.Click += new System.EventHandler(this.Help_Click);
            // 
            // btoCorreios1
            // 
            this.btoCorreios1.Location = new System.Drawing.Point(25, 391);
            this.btoCorreios1.Margin = new System.Windows.Forms.Padding(4);
            this.btoCorreios1.Name = "btoCorreios1";
            this.btoCorreios1.Size = new System.Drawing.Size(170, 44);
            this.btoCorreios1.TabIndex = 101;
            this.btoCorreios1.TabStop = false;
            this.btoCorreios1.Text = "ENVIO PELOS CORREIOS";
            this.btoCorreios1.UseVisualStyleBackColor = true;
            this.btoCorreios1.Visible = false;
            this.btoCorreios1.Click += new System.EventHandler(this.btoCorreio_Click);
            // 
            // lblR_10
            // 
            this.lblR_10.BackColor = System.Drawing.Color.Black;
            this.lblR_10.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblR_10.ForeColor = System.Drawing.Color.Red;
            this.lblR_10.Location = new System.Drawing.Point(893, 494);
            this.lblR_10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblR_10.Name = "lblR_10";
            this.lblR_10.Size = new System.Drawing.Size(170, 28);
            this.lblR_10.TabIndex = 100;
            this.lblR_10.Text = "RETIRADO";
            this.lblR_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblR_10.Visible = false;
            // 
            // bto10
            // 
            this.bto10.Location = new System.Drawing.Point(893, 521);
            this.bto10.Margin = new System.Windows.Forms.Padding(4);
            this.bto10.Name = "bto10";
            this.bto10.Size = new System.Drawing.Size(170, 44);
            this.bto10.TabIndex = 99;
            this.bto10.TabStop = false;
            this.bto10.Text = "RETIRAR";
            this.bto10.UseVisualStyleBackColor = true;
            this.bto10.Visible = false;
            this.bto10.Click += new System.EventHandler(this.btoAcao_Click);
            // 
            // img10
            // 
            this.img10.Location = new System.Drawing.Point(893, 362);
            this.img10.Margin = new System.Windows.Forms.Padding(4);
            this.img10.Name = "img10";
            this.img10.Size = new System.Drawing.Size(170, 160);
            this.img10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img10.TabIndex = 98;
            this.img10.TabStop = false;
            // 
            // lblR_9
            // 
            this.lblR_9.BackColor = System.Drawing.Color.Black;
            this.lblR_9.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblR_9.ForeColor = System.Drawing.Color.Red;
            this.lblR_9.Location = new System.Drawing.Point(673, 494);
            this.lblR_9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblR_9.Name = "lblR_9";
            this.lblR_9.Size = new System.Drawing.Size(170, 28);
            this.lblR_9.TabIndex = 97;
            this.lblR_9.Text = "RETIRADO";
            this.lblR_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblR_9.Visible = false;
            // 
            // bto9
            // 
            this.bto9.Location = new System.Drawing.Point(673, 521);
            this.bto9.Margin = new System.Windows.Forms.Padding(4);
            this.bto9.Name = "bto9";
            this.bto9.Size = new System.Drawing.Size(170, 44);
            this.bto9.TabIndex = 96;
            this.bto9.TabStop = false;
            this.bto9.Text = "RETIRAR";
            this.bto9.UseVisualStyleBackColor = true;
            this.bto9.Visible = false;
            this.bto9.Click += new System.EventHandler(this.btoAcao_Click);
            // 
            // img9
            // 
            this.img9.Location = new System.Drawing.Point(673, 362);
            this.img9.Margin = new System.Windows.Forms.Padding(4);
            this.img9.Name = "img9";
            this.img9.Size = new System.Drawing.Size(170, 160);
            this.img9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img9.TabIndex = 95;
            this.img9.TabStop = false;
            // 
            // lblR_8
            // 
            this.lblR_8.BackColor = System.Drawing.Color.Black;
            this.lblR_8.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblR_8.ForeColor = System.Drawing.Color.Red;
            this.lblR_8.Location = new System.Drawing.Point(456, 494);
            this.lblR_8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblR_8.Name = "lblR_8";
            this.lblR_8.Size = new System.Drawing.Size(170, 28);
            this.lblR_8.TabIndex = 94;
            this.lblR_8.Text = "RETIRADO";
            this.lblR_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblR_8.Visible = false;
            // 
            // bto8
            // 
            this.bto8.Location = new System.Drawing.Point(456, 521);
            this.bto8.Margin = new System.Windows.Forms.Padding(4);
            this.bto8.Name = "bto8";
            this.bto8.Size = new System.Drawing.Size(170, 44);
            this.bto8.TabIndex = 93;
            this.bto8.TabStop = false;
            this.bto8.Text = "RETIRAR";
            this.bto8.UseVisualStyleBackColor = true;
            this.bto8.Visible = false;
            this.bto8.Click += new System.EventHandler(this.btoAcao_Click);
            // 
            // img8
            // 
            this.img8.Location = new System.Drawing.Point(456, 362);
            this.img8.Margin = new System.Windows.Forms.Padding(4);
            this.img8.Name = "img8";
            this.img8.Size = new System.Drawing.Size(170, 160);
            this.img8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img8.TabIndex = 92;
            this.img8.TabStop = false;
            // 
            // lblR_7
            // 
            this.lblR_7.BackColor = System.Drawing.Color.Black;
            this.lblR_7.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblR_7.ForeColor = System.Drawing.Color.Red;
            this.lblR_7.Location = new System.Drawing.Point(241, 494);
            this.lblR_7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblR_7.Name = "lblR_7";
            this.lblR_7.Size = new System.Drawing.Size(170, 28);
            this.lblR_7.TabIndex = 91;
            this.lblR_7.Text = "RETIRADO";
            this.lblR_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblR_7.Visible = false;
            // 
            // bto7
            // 
            this.bto7.Location = new System.Drawing.Point(241, 521);
            this.bto7.Margin = new System.Windows.Forms.Padding(4);
            this.bto7.Name = "bto7";
            this.bto7.Size = new System.Drawing.Size(170, 44);
            this.bto7.TabIndex = 90;
            this.bto7.TabStop = false;
            this.bto7.Text = "RETIRAR";
            this.bto7.UseVisualStyleBackColor = true;
            this.bto7.Visible = false;
            this.bto7.Click += new System.EventHandler(this.btoAcao_Click);
            // 
            // img7
            // 
            this.img7.Location = new System.Drawing.Point(241, 362);
            this.img7.Margin = new System.Windows.Forms.Padding(4);
            this.img7.Name = "img7";
            this.img7.Size = new System.Drawing.Size(170, 160);
            this.img7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img7.TabIndex = 89;
            this.img7.TabStop = false;
            // 
            // lblR_6
            // 
            this.lblR_6.BackColor = System.Drawing.Color.Black;
            this.lblR_6.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblR_6.ForeColor = System.Drawing.Color.Red;
            this.lblR_6.Location = new System.Drawing.Point(25, 494);
            this.lblR_6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblR_6.Name = "lblR_6";
            this.lblR_6.Size = new System.Drawing.Size(170, 28);
            this.lblR_6.TabIndex = 88;
            this.lblR_6.Text = "RETIRADO";
            this.lblR_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblR_6.Visible = false;
            // 
            // bto6
            // 
            this.bto6.Location = new System.Drawing.Point(25, 521);
            this.bto6.Margin = new System.Windows.Forms.Padding(4);
            this.bto6.Name = "bto6";
            this.bto6.Size = new System.Drawing.Size(170, 44);
            this.bto6.TabIndex = 87;
            this.bto6.TabStop = false;
            this.bto6.Text = "RETIRAR";
            this.bto6.UseVisualStyleBackColor = true;
            this.bto6.Visible = false;
            this.bto6.Click += new System.EventHandler(this.btoAcao_Click);
            // 
            // img6
            // 
            this.img6.Location = new System.Drawing.Point(25, 362);
            this.img6.Margin = new System.Windows.Forms.Padding(4);
            this.img6.Name = "img6";
            this.img6.Size = new System.Drawing.Size(170, 160);
            this.img6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img6.TabIndex = 86;
            this.img6.TabStop = false;
            // 
            // lblR_4
            // 
            this.lblR_4.BackColor = System.Drawing.Color.Black;
            this.lblR_4.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblR_4.ForeColor = System.Drawing.Color.Red;
            this.lblR_4.Location = new System.Drawing.Point(674, 288);
            this.lblR_4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblR_4.Name = "lblR_4";
            this.lblR_4.Size = new System.Drawing.Size(169, 28);
            this.lblR_4.TabIndex = 85;
            this.lblR_4.Text = "RETIRADO";
            this.lblR_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblR_4.Visible = false;
            // 
            // bto4
            // 
            this.bto4.Location = new System.Drawing.Point(673, 316);
            this.bto4.Margin = new System.Windows.Forms.Padding(4);
            this.bto4.Name = "bto4";
            this.bto4.Size = new System.Drawing.Size(170, 44);
            this.bto4.TabIndex = 84;
            this.bto4.TabStop = false;
            this.bto4.Text = "RETIRAR";
            this.bto4.UseVisualStyleBackColor = true;
            this.bto4.Click += new System.EventHandler(this.btoAcao_Click);
            // 
            // img4
            // 
            this.img4.Location = new System.Drawing.Point(673, 156);
            this.img4.Margin = new System.Windows.Forms.Padding(4);
            this.img4.Name = "img4";
            this.img4.Size = new System.Drawing.Size(170, 160);
            this.img4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img4.TabIndex = 83;
            this.img4.TabStop = false;
            // 
            // lblR_5
            // 
            this.lblR_5.BackColor = System.Drawing.Color.Black;
            this.lblR_5.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblR_5.ForeColor = System.Drawing.Color.Red;
            this.lblR_5.Location = new System.Drawing.Point(894, 288);
            this.lblR_5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblR_5.Name = "lblR_5";
            this.lblR_5.Size = new System.Drawing.Size(169, 28);
            this.lblR_5.TabIndex = 82;
            this.lblR_5.Text = "RETIRADO";
            this.lblR_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblR_5.Visible = false;
            // 
            // bto5
            // 
            this.bto5.Location = new System.Drawing.Point(893, 316);
            this.bto5.Margin = new System.Windows.Forms.Padding(4);
            this.bto5.Name = "bto5";
            this.bto5.Size = new System.Drawing.Size(170, 44);
            this.bto5.TabIndex = 81;
            this.bto5.TabStop = false;
            this.bto5.Text = "RETIRAR";
            this.bto5.UseVisualStyleBackColor = true;
            this.bto5.Click += new System.EventHandler(this.btoAcao_Click);
            // 
            // img5
            // 
            this.img5.Location = new System.Drawing.Point(893, 156);
            this.img5.Margin = new System.Windows.Forms.Padding(4);
            this.img5.Name = "img5";
            this.img5.Size = new System.Drawing.Size(170, 160);
            this.img5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img5.TabIndex = 80;
            this.img5.TabStop = false;
            // 
            // lblEscolheuOuNaoEscolheu
            // 
            this.lblEscolheuOuNaoEscolheu.BackColor = System.Drawing.Color.Transparent;
            this.lblEscolheuOuNaoEscolheu.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.lblEscolheuOuNaoEscolheu.ForeColor = System.Drawing.Color.Black;
            this.lblEscolheuOuNaoEscolheu.Location = new System.Drawing.Point(23, 70);
            this.lblEscolheuOuNaoEscolheu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEscolheuOuNaoEscolheu.Name = "lblEscolheuOuNaoEscolheu";
            this.lblEscolheuOuNaoEscolheu.Size = new System.Drawing.Size(1049, 82);
            this.lblEscolheuOuNaoEscolheu.TabIndex = 74;
            // 
            // lblR_3
            // 
            this.lblR_3.BackColor = System.Drawing.Color.Black;
            this.lblR_3.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblR_3.ForeColor = System.Drawing.Color.Red;
            this.lblR_3.Location = new System.Drawing.Point(457, 288);
            this.lblR_3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblR_3.Name = "lblR_3";
            this.lblR_3.Size = new System.Drawing.Size(169, 28);
            this.lblR_3.TabIndex = 73;
            this.lblR_3.Text = "RETIRADO";
            this.lblR_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblR_3.Visible = false;
            // 
            // lblR_2
            // 
            this.lblR_2.BackColor = System.Drawing.Color.Black;
            this.lblR_2.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblR_2.ForeColor = System.Drawing.Color.Red;
            this.lblR_2.Location = new System.Drawing.Point(242, 288);
            this.lblR_2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblR_2.Name = "lblR_2";
            this.lblR_2.Size = new System.Drawing.Size(169, 28);
            this.lblR_2.TabIndex = 72;
            this.lblR_2.Text = "RETIRADO";
            this.lblR_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblR_2.Visible = false;
            // 
            // lblR_1
            // 
            this.lblR_1.BackColor = System.Drawing.Color.Black;
            this.lblR_1.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblR_1.ForeColor = System.Drawing.Color.Red;
            this.lblR_1.Location = new System.Drawing.Point(25, 288);
            this.lblR_1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblR_1.Name = "lblR_1";
            this.lblR_1.Size = new System.Drawing.Size(170, 28);
            this.lblR_1.TabIndex = 71;
            this.lblR_1.Text = "RETIRADO";
            this.lblR_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblR_1.Visible = false;
            // 
            // lblCorteSelecionada
            // 
            this.lblCorteSelecionada.AutoSize = true;
            this.lblCorteSelecionada.BackColor = System.Drawing.Color.Transparent;
            this.lblCorteSelecionada.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.lblCorteSelecionada.ForeColor = System.Drawing.Color.Black;
            this.lblCorteSelecionada.Location = new System.Drawing.Point(23, 25);
            this.lblCorteSelecionada.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCorteSelecionada.Name = "lblCorteSelecionada";
            this.lblCorteSelecionada.Size = new System.Drawing.Size(261, 31);
            this.lblCorteSelecionada.TabIndex = 70;
            this.lblCorteSelecionada.Text = "[lblReconhecimento]";
            // 
            // bto3
            // 
            this.bto3.Location = new System.Drawing.Point(456, 316);
            this.bto3.Margin = new System.Windows.Forms.Padding(4);
            this.bto3.Name = "bto3";
            this.bto3.Size = new System.Drawing.Size(170, 44);
            this.bto3.TabIndex = 7;
            this.bto3.TabStop = false;
            this.bto3.Text = "RETIRAR";
            this.bto3.UseVisualStyleBackColor = true;
            this.bto3.Click += new System.EventHandler(this.btoAcao_Click);
            // 
            // bto2
            // 
            this.bto2.Location = new System.Drawing.Point(241, 316);
            this.bto2.Margin = new System.Windows.Forms.Padding(4);
            this.bto2.Name = "bto2";
            this.bto2.Size = new System.Drawing.Size(170, 44);
            this.bto2.TabIndex = 6;
            this.bto2.TabStop = false;
            this.bto2.Text = "RETIRAR";
            this.bto2.UseVisualStyleBackColor = true;
            this.bto2.Click += new System.EventHandler(this.btoAcao_Click);
            // 
            // bto1
            // 
            this.bto1.Location = new System.Drawing.Point(25, 316);
            this.bto1.Margin = new System.Windows.Forms.Padding(4);
            this.bto1.Name = "bto1";
            this.bto1.Size = new System.Drawing.Size(170, 44);
            this.bto1.TabIndex = 5;
            this.bto1.TabStop = false;
            this.bto1.Text = "RETIRAR";
            this.bto1.UseVisualStyleBackColor = true;
            this.bto1.Click += new System.EventHandler(this.btoAcao_Click);
            // 
            // img3
            // 
            this.img3.Location = new System.Drawing.Point(456, 156);
            this.img3.Margin = new System.Windows.Forms.Padding(4);
            this.img3.Name = "img3";
            this.img3.Size = new System.Drawing.Size(170, 160);
            this.img3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img3.TabIndex = 2;
            this.img3.TabStop = false;
            // 
            // img2
            // 
            this.img2.Location = new System.Drawing.Point(241, 156);
            this.img2.Margin = new System.Windows.Forms.Padding(4);
            this.img2.Name = "img2";
            this.img2.Size = new System.Drawing.Size(170, 160);
            this.img2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img2.TabIndex = 1;
            this.img2.TabStop = false;
            // 
            // img1
            // 
            this.img1.Location = new System.Drawing.Point(25, 156);
            this.img1.Margin = new System.Windows.Forms.Padding(4);
            this.img1.Name = "img1";
            this.img1.Size = new System.Drawing.Size(170, 160);
            this.img1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img1.TabIndex = 0;
            this.img1.TabStop = false;
            // 
            // CLT_Joia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1167, 900);
            this.Controls.Add(this.lblConsultora);
            this.Controls.Add(this.pnlPainelTexto);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CLT_Joia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Localizar Registro";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CLT_LocalizarRegistro_Load);
            this.pnlPainelTexto.ResumeLayout(false);
            this.pnlPremios.ResumeLayout(false);
            this.pnlPremios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer tmrTempo;
        private System.Windows.Forms.Label lblConsultora;
        private System.Windows.Forms.Panel pnlPainelTexto;
        private System.Windows.Forms.Panel pnlPremios;
        private System.Windows.Forms.Label lblR_3;
        private System.Windows.Forms.Label lblR_2;
        private System.Windows.Forms.Label lblR_1;
        private System.Windows.Forms.Label lblCorteSelecionada;
        private System.Windows.Forms.Button bto3;
        private System.Windows.Forms.Button bto2;
        private System.Windows.Forms.Button bto1;
        private System.Windows.Forms.PictureBox img3;
        private System.Windows.Forms.PictureBox img2;
        private System.Windows.Forms.PictureBox img1;
        private System.Windows.Forms.Label lblEscolheuOuNaoEscolheu;
        private System.Windows.Forms.Label lblR_5;
        private System.Windows.Forms.Button bto5;
        private System.Windows.Forms.PictureBox img5;
        private System.Windows.Forms.Label lblR_4;
        private System.Windows.Forms.Button bto4;
        private System.Windows.Forms.PictureBox img4;
        private System.Windows.Forms.Label lblR_10;
        private System.Windows.Forms.Button bto10;
        private System.Windows.Forms.PictureBox img10;
        private System.Windows.Forms.Label lblR_9;
        private System.Windows.Forms.Button bto9;
        private System.Windows.Forms.PictureBox img9;
        private System.Windows.Forms.Label lblR_8;
        private System.Windows.Forms.Button bto8;
        private System.Windows.Forms.PictureBox img8;
        private System.Windows.Forms.Label lblR_7;
        private System.Windows.Forms.Button bto7;
        private System.Windows.Forms.PictureBox img7;
        private System.Windows.Forms.Label lblR_6;
        private System.Windows.Forms.Button bto6;
        private System.Windows.Forms.PictureBox img6;
        private System.Windows.Forms.Button btoCorreios1;
        private System.Windows.Forms.PictureBox imgAjudaPremio10;
        private System.Windows.Forms.PictureBox imgAjudaPremio9;
        private System.Windows.Forms.PictureBox imgAjudaPremio8;
        private System.Windows.Forms.PictureBox imgAjudaPremio7;
        private System.Windows.Forms.PictureBox imgAjudaPremio6;
        private System.Windows.Forms.PictureBox imgAjudaPremio5;
        private System.Windows.Forms.PictureBox imgAjudaPremio4;
        private System.Windows.Forms.PictureBox imgAjudaPremio3;
        private System.Windows.Forms.PictureBox imgAjudaPremio2;
        private System.Windows.Forms.PictureBox imgAjudaPremio1;
    }
}