using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_Reconhecimento : Form
    {
        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        public bool boResultSair = false;
        public string AP_CodigoBarras { get; set; }
        public string AP_NrConsultora { get; set; }
        public string AP_NomeConsultora { get; set; }

        public CLT_Reconhecimento()
        {
            InitializeComponent();
        }

        private void CarregaBackground()
        {
            this.BackgroundImage = Properties.Resources.BackgroundMaryKay;
        }

        private void CLT_LocalizarRegistro_Load(object sender, EventArgs e)
        {
            System.Threading.Thread ts = new System.Threading.Thread(CarregaBackground);
            ts.Start();

            MI_PosicionarPainel();

            lblConsultora.Text = AP_NomeConsultora;
            MI_CarregarReconhecimentos();

            btoFinalizar.Location = new Point(pnlReconhecimento.Location.X, btoFinalizar.Location.Y);
        }

        private void MI_PosicionarPainel()
        {
            pnlPainelTexto.Location = new Point((this.Size.Width / 2) - (pnlPainelTexto.Size.Width / 2), pnlPainelTexto.Location.Y); 
        }

        private void MI_CarregarReconhecimentos()
        {
            pnlReconhecimento.Controls.Clear();

            CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();
            DataTable dtPremiacoes = IAT_CadastrarVisitante.MS_ObterReconhecimentos(AP_CodigoBarras);


            if ((dtPremiacoes.Rows.Count == 1) && (MI_VerificarSeRetirado(Convert.ToInt32(dtPremiacoes.Rows[0]["ObjRef"].ToString())) != "RETIRADO"))
            {
                if (prVerificaProdutoTop3(Convert.ToInt32(dtPremiacoes.Rows[0]["ObjRef"].ToString())))
                {
                    int it_Y = 20;
                    foreach (DataRow Row in dtPremiacoes.Rows)
                    {
                        MI_MontarReconhecimentoButton(it_Y.ToString(), Row, it_Y);
                        it_Y += 55;
                    }
                }
                else
                {
                    CLT_Joia open = new CLT_Joia();
                    open.AP_CodigoBarras = AP_CodigoBarras;
                    open.AP_CdReconhecimento = dtPremiacoes.Rows[0]["ObjRef"].ToString();
                    open.AP_NomeReconhecimento = dtPremiacoes.Rows[0]["dsReconhecimento"].ToString();
                    open.AP_NomeConsultora = AP_NomeConsultora;
                    open.ShowDialog();

                    this.Close();
                }
            }
            else
            {
                int it_Y = 20;
                foreach (DataRow Row in dtPremiacoes.Rows)
                {
                    MI_MontarReconhecimentoButton(it_Y.ToString(), Row, it_Y);
                    it_Y += 55;
                }
            }
        }

        private void MI_MontarReconhecimentoButton(string prID, DataRow Row, int it_Y)
        {
            Button objControle = new Button();

            objControle.BackColor = System.Drawing.Color.White;
            objControle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            objControle.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            objControle.Location = new System.Drawing.Point(30, it_Y);
            objControle.Size = new System.Drawing.Size(600, 50);
            objControle.TabIndex = 0;
            objControle.TabStop = false;
            objControle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            objControle.UseVisualStyleBackColor = false;

            objControle.Text = Row["dsReconhecimento"].ToString();
            objControle.Tag = Row["ObjRef"].ToString();

            objControle.Click += new System.EventHandler(this.Reconhecimento_Click);

            string prSituacao = MI_VerificarSeRetirado(Convert.ToInt32(Row["ObjRef"].ToString()));

            if (prSituacao == "RETIRADO")
            {
                objControle.Enabled = false;
                objControle.BackColor = System.Drawing.Color.LightGray;
                objControle.Text = Row["dsReconhecimento"].ToString() + " ("+ prSituacao + ")";
            }
            else
            {
                if (prVerificaProdutoTop3(Convert.ToInt32(Row["ObjRef"].ToString())))
                {
                    objControle.Enabled = false;
                    objControle.BackColor = System.Drawing.Color.LightGray;
                    objControle.Text = Row["dsReconhecimento"].ToString() + " (" + "RETIRAR NO PALCO" + ")";
                }
                else
                {
                    objControle.Enabled = true;
                    objControle.BackColor = System.Drawing.Color.White;
                    objControle.Text = Row["dsReconhecimento"].ToString() + " (" + prSituacao + ")";
                }
            }

            pnlReconhecimento.Controls.Add(objControle);
        }

        private void Reconhecimento_Click(object sender, EventArgs e)
        {
            Button btReconhecimento = (Button)sender;
            string stCorteSelecionada = btReconhecimento.Tag.ToString();

            MI_Andamento(stCorteSelecionada, btReconhecimento.Text);
        }

        private string MI_VerificarSeRetirado(Int32 prCdReconhecimento)
        {
            bool boResult = true;
            string prTexto = string.Empty;

            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtPremio = new DataTable();

            dtPremio = IAT_Visitante.MS_ObterPremiosDaConsultora(prCdReconhecimento.ToString(), "PREMIO");
            foreach (DataRow Row in dtPremio.Rows)
            {
                if (Row["dsRetirado"].ToString().ToUpper().Trim() == "N")
                {
                    boResult = false;
                }
            }

            dtPremio = IAT_Visitante.MS_ObterPremiosDaConsultora(prCdReconhecimento.ToString(), "NUMERAL");
            foreach (DataRow Row in dtPremio.Rows)
            {
                if (Row["dsRetirado"].ToString().ToUpper().Trim() == "N")
                {
                    boResult = false;
                }
            }

            dtPremio = IAT_Visitante.MS_ObterPremiosDaConsultora(prCdReconhecimento.ToString(), "Charm");
            foreach (DataRow Row in dtPremio.Rows)
            {
                if (Row["dsRetirado"].ToString().ToUpper().Trim() == "N")
                {
                    boResult = false;
                }
            }

            dtPremio = IAT_Visitante.MS_ObterPremiosDaConsultora(prCdReconhecimento.ToString(), "Faixa");
            foreach (DataRow Row in dtPremio.Rows)
            {
                if (Row["dsRetirado"].ToString().ToUpper().Trim() == "N")
                {
                    boResult = false;
                }
            }

            if (boResult == false)
            {
                prTexto = "retirar pr�mio";

                // VERIFICAR SE PRECISA ESCOLHER PREMIO
                dtPremio = IAT_Visitante.MS_ObterPremiosDaConsultora(prCdReconhecimento.ToString(), "PREMIO");
                foreach (DataRow Row in dtPremio.Rows)
                {
                    if (Row["dsTrocaLocal"].ToString().Trim() == "")
                    {
                        prTexto = "escolher pr�mio";
                    }
                }

            }

            if (boResult == true)
            {
                prTexto = "RETIRADO";
            }

            return prTexto;
        }

        
        private bool prVerificaProdutoTop3(Int32 prCdReconhecimento)
        {
            bool boResult = false;

            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtPremio = new DataTable();

            dtPremio = IAT_Visitante.MS_ObterPremiosDaConsultora(prCdReconhecimento.ToString(), "MIMO");
            foreach (DataRow Row in dtPremio.Rows)
            {
                if (Row["dsEscolhaPre"].ToString().ToUpper().Trim() == "27")
                {
                    boResult = true;
                }
            }

            return boResult;
        }

        private void MI_Andamento(string prCorteSelecionada, string prNomeCorte)
        {
            CLT_Joia open = new CLT_Joia();
            open.AP_CodigoBarras = AP_CodigoBarras;
            open.AP_CdReconhecimento = prCorteSelecionada;
            open.AP_NomeReconhecimento = prNomeCorte.Replace("(RETIRADO)", "").Replace("(escolher pr�mio)", "").Replace("(retirar pr�mio)", "");
            open.AP_NomeConsultora = AP_NomeConsultora;
            open.ShowDialog();

            MI_CarregarReconhecimentos();

            if (MI_VerificarSeTemAlgumItemPareRetirar() == false)
                this.Close();
        }

        private bool MI_VerificarSeTemAlgumItemPareRetirar()
        {
            bool boResult = false;
            foreach(Control objControle in pnlReconhecimento.Controls)
            {
                if (objControle is Button)
                {
                    if (objControle.Enabled == true)
                        boResult = true;
                }
            }

            return boResult;
        }


        private void lblConsultora_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btoFinalizar_Click(object sender, EventArgs e)
        {
            boResultSair = true;
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}