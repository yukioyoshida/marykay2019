﻿namespace SPI
{
    partial class CLT_LerCodigoProduto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblConsultora = new System.Windows.Forms.Label();
            this.lblCodigoBarrasProduto = new System.Windows.Forms.Label();
            this.edtPalavra = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblConsultora
            // 
            this.lblConsultora.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblConsultora.BackColor = System.Drawing.Color.Transparent;
            this.lblConsultora.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsultora.ForeColor = System.Drawing.Color.Black;
            this.lblConsultora.Location = new System.Drawing.Point(4, 38);
            this.lblConsultora.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblConsultora.Name = "lblConsultora";
            this.lblConsultora.Size = new System.Drawing.Size(1119, 65);
            this.lblConsultora.TabIndex = 68;
            this.lblConsultora.Text = "Leia o código de barras do produto";
            this.lblConsultora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCodigoBarrasProduto
            // 
            this.lblCodigoBarrasProduto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCodigoBarrasProduto.BackColor = System.Drawing.Color.Transparent;
            this.lblCodigoBarrasProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoBarrasProduto.ForeColor = System.Drawing.Color.Black;
            this.lblCodigoBarrasProduto.Location = new System.Drawing.Point(2, 171);
            this.lblCodigoBarrasProduto.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodigoBarrasProduto.Name = "lblCodigoBarrasProduto";
            this.lblCodigoBarrasProduto.Size = new System.Drawing.Size(1119, 65);
            this.lblCodigoBarrasProduto.TabIndex = 69;
            this.lblCodigoBarrasProduto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // edtPalavra
            // 
            this.edtPalavra.Location = new System.Drawing.Point(-10, -30);
            this.edtPalavra.Name = "edtPalavra";
            this.edtPalavra.Size = new System.Drawing.Size(64, 22);
            this.edtPalavra.TabIndex = 70;
            this.edtPalavra.TextChanged += new System.EventHandler(this.edtPalavra_TextChanged);
            // 
            // CLT_LerCodigoProduto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(1123, 346);
            this.Controls.Add(this.edtPalavra);
            this.Controls.Add(this.lblCodigoBarrasProduto);
            this.Controls.Add(this.lblConsultora);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CLT_LerCodigoProduto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CLT_LerCodigoProduo";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CLT_LerCodigoProduto_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblConsultora;
        private System.Windows.Forms.Label lblCodigoBarrasProduto;
        private System.Windows.Forms.TextBox edtPalavra;
    }
}