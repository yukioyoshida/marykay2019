﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_LerCodigoProduto : Form
    {
        public bool AP_CancelamentoEntrega { get; set; }
        public string AP_NrConsultora { get; set; }
        public string AP_codJoia { get; set; }
        public bool CodigoValido { get; set; }

        public CLT_LerCodigoProduto()
        {
            InitializeComponent();
        }

        private void CLT_LerCodigoProduto_Load(object sender, EventArgs eventArgs)
        {
            edtPalavra.Focus();
        }

        private void MI_LocalizarRegistro(string stCodigoBarras)
        {
            CTO_Roteiro ITO_Roteiro = new CTO_Roteiro();
            List<CTO_PremioConsultora> lITO_PremioConsultora = new List<CTO_PremioConsultora>();
            ITO_Roteiro = new CAT_Roteiro().MS_ObterPorCodigoBarras(stCodigoBarras);

            if (ITO_Roteiro != null)
            {
                lITO_PremioConsultora = new CAT_PremioConsultora().MS_ObterPremiacaoPorCodigoConsultora(AP_NrConsultora);

                if (lITO_PremioConsultora.Count > 0)
                {
                    var premioConsultora = lITO_PremioConsultora.Where(x => x.ITO_Roteiro.dsCodigoBarrasProduto == stCodigoBarras && x.ITO_PremioConsultoraItem.cdItem == Convert.ToInt32(AP_codJoia)).FirstOrDefault();

                    if (premioConsultora == null)
                    {
                        CodigoValido = false;
                        lblCodigoBarrasProduto.Text = "Este código de barras não pertence a esse produto";
                        edtPalavra.Text = "";
                        edtPalavra.Focus();
                    }
                    else
                    {
                        if (AP_CancelamentoEntrega)
                            CodigoValido = true;
                        else
                            CodigoValido = new CAT_PremioConsultoraItem().MS_AtualizarCodigoBarrasProduto(stCodigoBarras, Convert.ToInt32(AP_codJoia));

                        this.Close();
                    }
                }
            }
            else
            {
                lblCodigoBarrasProduto.Text = "Produto não localizado, tente novamente";
                edtPalavra.Text = "";
                edtPalavra.Focus();
            }
        }

        private void edtPalavra_TextChanged(object sender, EventArgs e)
        {
            if (edtPalavra.Text.Trim() != null && edtPalavra.Text.Length >= 6)
                MI_LocalizarRegistro(edtPalavra.Text.Trim());
        }

        private void CLT_LerCodigoProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
        }
    }
}
