﻿namespace SPI
{
    partial class Clt_TrocaDeJoia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Clt_TrocaDeJoia));
            this.lblCorteSelecionada = new System.Windows.Forms.Label();
            this.btoNAO = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.imgAjudaPremio10 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio9 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio8 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio7 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio6 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio5 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio4 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio3 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio2 = new System.Windows.Forms.PictureBox();
            this.imgAjudaPremio1 = new System.Windows.Forms.PictureBox();
            this.imgPremio10 = new System.Windows.Forms.PictureBox();
            this.imgPremio9 = new System.Windows.Forms.PictureBox();
            this.imgPremio8 = new System.Windows.Forms.PictureBox();
            this.imgPremio7 = new System.Windows.Forms.PictureBox();
            this.imgPremio6 = new System.Windows.Forms.PictureBox();
            this.imgPremio5 = new System.Windows.Forms.PictureBox();
            this.imgPremio4 = new System.Windows.Forms.PictureBox();
            this.imgPremio3 = new System.Windows.Forms.PictureBox();
            this.imgPremio2 = new System.Windows.Forms.PictureBox();
            this.imgPremio1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCorteSelecionada
            // 
            this.lblCorteSelecionada.AutoSize = true;
            this.lblCorteSelecionada.BackColor = System.Drawing.Color.Transparent;
            this.lblCorteSelecionada.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorteSelecionada.ForeColor = System.Drawing.Color.Gray;
            this.lblCorteSelecionada.Location = new System.Drawing.Point(21, 68);
            this.lblCorteSelecionada.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCorteSelecionada.Name = "lblCorteSelecionada";
            this.lblCorteSelecionada.Size = new System.Drawing.Size(187, 36);
            this.lblCorteSelecionada.TabIndex = 71;
            this.lblCorteSelecionada.Text = "Troca de joia";
            // 
            // btoNAO
            // 
            this.btoNAO.BackColor = System.Drawing.Color.White;
            this.btoNAO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btoNAO.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoNAO.Location = new System.Drawing.Point(899, 599);
            this.btoNAO.Margin = new System.Windows.Forms.Padding(4);
            this.btoNAO.Name = "btoNAO";
            this.btoNAO.Size = new System.Drawing.Size(211, 62);
            this.btoNAO.TabIndex = 73;
            this.btoNAO.TabStop = false;
            this.btoNAO.Text = "CANCELAR";
            this.btoNAO.UseVisualStyleBackColor = false;
            this.btoNAO.Click += new System.EventHandler(this.btoNAO_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.imgAjudaPremio10);
            this.panel1.Controls.Add(this.imgAjudaPremio9);
            this.panel1.Controls.Add(this.imgAjudaPremio8);
            this.panel1.Controls.Add(this.imgAjudaPremio7);
            this.panel1.Controls.Add(this.imgAjudaPremio6);
            this.panel1.Controls.Add(this.imgAjudaPremio5);
            this.panel1.Controls.Add(this.imgAjudaPremio4);
            this.panel1.Controls.Add(this.imgAjudaPremio3);
            this.panel1.Controls.Add(this.imgAjudaPremio2);
            this.panel1.Controls.Add(this.imgAjudaPremio1);
            this.panel1.Controls.Add(this.imgPremio10);
            this.panel1.Controls.Add(this.imgPremio9);
            this.panel1.Controls.Add(this.imgPremio8);
            this.panel1.Controls.Add(this.imgPremio7);
            this.panel1.Controls.Add(this.imgPremio6);
            this.panel1.Controls.Add(this.imgPremio5);
            this.panel1.Controls.Add(this.imgPremio4);
            this.panel1.Controls.Add(this.imgPremio3);
            this.panel1.Controls.Add(this.lblCorteSelecionada);
            this.panel1.Controls.Add(this.imgPremio2);
            this.panel1.Controls.Add(this.imgPremio1);
            this.panel1.Controls.Add(this.btoNAO);
            this.panel1.Location = new System.Drawing.Point(1, -7);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1151, 685);
            this.panel1.TabIndex = 76;
            // 
            // imgAjudaPremio10
            // 
            this.imgAjudaPremio10.Image = ((System.Drawing.Image)(resources.GetObject("imgAjudaPremio10.Image")));
            this.imgAjudaPremio10.Location = new System.Drawing.Point(1081, 340);
            this.imgAjudaPremio10.Name = "imgAjudaPremio10";
            this.imgAjudaPremio10.Size = new System.Drawing.Size(35, 35);
            this.imgAjudaPremio10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio10.TabIndex = 93;
            this.imgAjudaPremio10.TabStop = false;
            this.imgAjudaPremio10.Visible = false;
            this.imgAjudaPremio10.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio9
            // 
            this.imgAjudaPremio9.Image = ((System.Drawing.Image)(resources.GetObject("imgAjudaPremio9.Image")));
            this.imgAjudaPremio9.Location = new System.Drawing.Point(862, 340);
            this.imgAjudaPremio9.Name = "imgAjudaPremio9";
            this.imgAjudaPremio9.Size = new System.Drawing.Size(35, 35);
            this.imgAjudaPremio9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio9.TabIndex = 92;
            this.imgAjudaPremio9.TabStop = false;
            this.imgAjudaPremio9.Visible = false;
            this.imgAjudaPremio9.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio8
            // 
            this.imgAjudaPremio8.Image = ((System.Drawing.Image)(resources.GetObject("imgAjudaPremio8.Image")));
            this.imgAjudaPremio8.Location = new System.Drawing.Point(640, 340);
            this.imgAjudaPremio8.Name = "imgAjudaPremio8";
            this.imgAjudaPremio8.Size = new System.Drawing.Size(35, 35);
            this.imgAjudaPremio8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio8.TabIndex = 91;
            this.imgAjudaPremio8.TabStop = false;
            this.imgAjudaPremio8.Visible = false;
            this.imgAjudaPremio8.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio7
            // 
            this.imgAjudaPremio7.Image = ((System.Drawing.Image)(resources.GetObject("imgAjudaPremio7.Image")));
            this.imgAjudaPremio7.Location = new System.Drawing.Point(417, 340);
            this.imgAjudaPremio7.Name = "imgAjudaPremio7";
            this.imgAjudaPremio7.Size = new System.Drawing.Size(35, 35);
            this.imgAjudaPremio7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio7.TabIndex = 90;
            this.imgAjudaPremio7.TabStop = false;
            this.imgAjudaPremio7.Visible = false;
            this.imgAjudaPremio7.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio6
            // 
            this.imgAjudaPremio6.Image = ((System.Drawing.Image)(resources.GetObject("imgAjudaPremio6.Image")));
            this.imgAjudaPremio6.Location = new System.Drawing.Point(193, 340);
            this.imgAjudaPremio6.Name = "imgAjudaPremio6";
            this.imgAjudaPremio6.Size = new System.Drawing.Size(35, 35);
            this.imgAjudaPremio6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio6.TabIndex = 89;
            this.imgAjudaPremio6.TabStop = false;
            this.imgAjudaPremio6.Visible = false;
            this.imgAjudaPremio6.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio5
            // 
            this.imgAjudaPremio5.Image = ((System.Drawing.Image)(resources.GetObject("imgAjudaPremio5.Image")));
            this.imgAjudaPremio5.Location = new System.Drawing.Point(1081, 122);
            this.imgAjudaPremio5.Name = "imgAjudaPremio5";
            this.imgAjudaPremio5.Size = new System.Drawing.Size(35, 35);
            this.imgAjudaPremio5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio5.TabIndex = 88;
            this.imgAjudaPremio5.TabStop = false;
            this.imgAjudaPremio5.Visible = false;
            this.imgAjudaPremio5.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio4
            // 
            this.imgAjudaPremio4.Image = ((System.Drawing.Image)(resources.GetObject("imgAjudaPremio4.Image")));
            this.imgAjudaPremio4.Location = new System.Drawing.Point(862, 122);
            this.imgAjudaPremio4.Name = "imgAjudaPremio4";
            this.imgAjudaPremio4.Size = new System.Drawing.Size(35, 35);
            this.imgAjudaPremio4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio4.TabIndex = 87;
            this.imgAjudaPremio4.TabStop = false;
            this.imgAjudaPremio4.Visible = false;
            this.imgAjudaPremio4.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio3
            // 
            this.imgAjudaPremio3.Image = ((System.Drawing.Image)(resources.GetObject("imgAjudaPremio3.Image")));
            this.imgAjudaPremio3.Location = new System.Drawing.Point(640, 122);
            this.imgAjudaPremio3.Name = "imgAjudaPremio3";
            this.imgAjudaPremio3.Size = new System.Drawing.Size(35, 35);
            this.imgAjudaPremio3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio3.TabIndex = 86;
            this.imgAjudaPremio3.TabStop = false;
            this.imgAjudaPremio3.Visible = false;
            this.imgAjudaPremio3.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio2
            // 
            this.imgAjudaPremio2.Image = ((System.Drawing.Image)(resources.GetObject("imgAjudaPremio2.Image")));
            this.imgAjudaPremio2.Location = new System.Drawing.Point(417, 122);
            this.imgAjudaPremio2.Name = "imgAjudaPremio2";
            this.imgAjudaPremio2.Size = new System.Drawing.Size(35, 35);
            this.imgAjudaPremio2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio2.TabIndex = 85;
            this.imgAjudaPremio2.TabStop = false;
            this.imgAjudaPremio2.Visible = false;
            this.imgAjudaPremio2.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgAjudaPremio1
            // 
            this.imgAjudaPremio1.Image = global::SPI.Properties.Resources.help2;
            this.imgAjudaPremio1.Location = new System.Drawing.Point(193, 122);
            this.imgAjudaPremio1.Name = "imgAjudaPremio1";
            this.imgAjudaPremio1.Size = new System.Drawing.Size(35, 35);
            this.imgAjudaPremio1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAjudaPremio1.TabIndex = 84;
            this.imgAjudaPremio1.TabStop = false;
            this.imgAjudaPremio1.Click += new System.EventHandler(this.Help_Click);
            // 
            // imgPremio10
            // 
            this.imgPremio10.Location = new System.Drawing.Point(916, 340);
            this.imgPremio10.Margin = new System.Windows.Forms.Padding(4);
            this.imgPremio10.Name = "imgPremio10";
            this.imgPremio10.Size = new System.Drawing.Size(200, 200);
            this.imgPremio10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPremio10.TabIndex = 83;
            this.imgPremio10.TabStop = false;
            this.imgPremio10.Tag = "10";
            this.imgPremio10.Click += new System.EventHandler(this.Trocar_Click);
            // 
            // imgPremio9
            // 
            this.imgPremio9.Location = new System.Drawing.Point(697, 340);
            this.imgPremio9.Margin = new System.Windows.Forms.Padding(4);
            this.imgPremio9.Name = "imgPremio9";
            this.imgPremio9.Size = new System.Drawing.Size(200, 200);
            this.imgPremio9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPremio9.TabIndex = 82;
            this.imgPremio9.TabStop = false;
            this.imgPremio9.Tag = "10";
            this.imgPremio9.Click += new System.EventHandler(this.Trocar_Click);
            // 
            // imgPremio8
            // 
            this.imgPremio8.Location = new System.Drawing.Point(475, 340);
            this.imgPremio8.Margin = new System.Windows.Forms.Padding(4);
            this.imgPremio8.Name = "imgPremio8";
            this.imgPremio8.Size = new System.Drawing.Size(200, 200);
            this.imgPremio8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPremio8.TabIndex = 81;
            this.imgPremio8.TabStop = false;
            this.imgPremio8.Tag = "11";
            this.imgPremio8.Click += new System.EventHandler(this.Trocar_Click);
            // 
            // imgPremio7
            // 
            this.imgPremio7.Location = new System.Drawing.Point(252, 340);
            this.imgPremio7.Margin = new System.Windows.Forms.Padding(4);
            this.imgPremio7.Name = "imgPremio7";
            this.imgPremio7.Size = new System.Drawing.Size(200, 200);
            this.imgPremio7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPremio7.TabIndex = 80;
            this.imgPremio7.TabStop = false;
            this.imgPremio7.Tag = "40";
            this.imgPremio7.Click += new System.EventHandler(this.Trocar_Click);
            // 
            // imgPremio6
            // 
            this.imgPremio6.Location = new System.Drawing.Point(28, 340);
            this.imgPremio6.Margin = new System.Windows.Forms.Padding(4);
            this.imgPremio6.Name = "imgPremio6";
            this.imgPremio6.Size = new System.Drawing.Size(200, 200);
            this.imgPremio6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPremio6.TabIndex = 79;
            this.imgPremio6.TabStop = false;
            this.imgPremio6.Tag = "9";
            this.imgPremio6.Click += new System.EventHandler(this.Trocar_Click);
            // 
            // imgPremio5
            // 
            this.imgPremio5.Location = new System.Drawing.Point(916, 122);
            this.imgPremio5.Margin = new System.Windows.Forms.Padding(4);
            this.imgPremio5.Name = "imgPremio5";
            this.imgPremio5.Size = new System.Drawing.Size(200, 200);
            this.imgPremio5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPremio5.TabIndex = 78;
            this.imgPremio5.TabStop = false;
            this.imgPremio5.Tag = "10";
            this.imgPremio5.Click += new System.EventHandler(this.Trocar_Click);
            // 
            // imgPremio4
            // 
            this.imgPremio4.Location = new System.Drawing.Point(697, 122);
            this.imgPremio4.Margin = new System.Windows.Forms.Padding(4);
            this.imgPremio4.Name = "imgPremio4";
            this.imgPremio4.Size = new System.Drawing.Size(200, 200);
            this.imgPremio4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPremio4.TabIndex = 77;
            this.imgPremio4.TabStop = false;
            this.imgPremio4.Tag = "10";
            this.imgPremio4.Click += new System.EventHandler(this.Trocar_Click);
            // 
            // imgPremio3
            // 
            this.imgPremio3.Location = new System.Drawing.Point(475, 122);
            this.imgPremio3.Margin = new System.Windows.Forms.Padding(4);
            this.imgPremio3.Name = "imgPremio3";
            this.imgPremio3.Size = new System.Drawing.Size(200, 200);
            this.imgPremio3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPremio3.TabIndex = 76;
            this.imgPremio3.TabStop = false;
            this.imgPremio3.Tag = "11";
            this.imgPremio3.Click += new System.EventHandler(this.Trocar_Click);
            // 
            // imgPremio2
            // 
            this.imgPremio2.Location = new System.Drawing.Point(252, 122);
            this.imgPremio2.Margin = new System.Windows.Forms.Padding(4);
            this.imgPremio2.Name = "imgPremio2";
            this.imgPremio2.Size = new System.Drawing.Size(200, 200);
            this.imgPremio2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPremio2.TabIndex = 75;
            this.imgPremio2.TabStop = false;
            this.imgPremio2.Tag = "40";
            this.imgPremio2.Click += new System.EventHandler(this.Trocar_Click);
            // 
            // imgPremio1
            // 
            this.imgPremio1.Location = new System.Drawing.Point(28, 122);
            this.imgPremio1.Margin = new System.Windows.Forms.Padding(4);
            this.imgPremio1.Name = "imgPremio1";
            this.imgPremio1.Size = new System.Drawing.Size(200, 200);
            this.imgPremio1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPremio1.TabIndex = 72;
            this.imgPremio1.TabStop = false;
            this.imgPremio1.Tag = "9";
            this.imgPremio1.Click += new System.EventHandler(this.Trocar_Click);
            // 
            // Clt_TrocaDeJoia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1167, 691);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Clt_TrocaDeJoia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Clt_ConfirmaEscolha";
            this.Load += new System.EventHandler(this.Clt_ConfirmaEscolha_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAjudaPremio1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblCorteSelecionada;
        private System.Windows.Forms.PictureBox imgPremio1;
        private System.Windows.Forms.Button btoNAO;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox imgPremio2;
        private System.Windows.Forms.PictureBox imgPremio3;
        private System.Windows.Forms.PictureBox imgPremio4;
        private System.Windows.Forms.PictureBox imgPremio10;
        private System.Windows.Forms.PictureBox imgPremio9;
        private System.Windows.Forms.PictureBox imgPremio8;
        private System.Windows.Forms.PictureBox imgPremio7;
        private System.Windows.Forms.PictureBox imgPremio6;
        private System.Windows.Forms.PictureBox imgPremio5;
        private System.Windows.Forms.PictureBox imgAjudaPremio1;
        private System.Windows.Forms.PictureBox imgAjudaPremio10;
        private System.Windows.Forms.PictureBox imgAjudaPremio9;
        private System.Windows.Forms.PictureBox imgAjudaPremio8;
        private System.Windows.Forms.PictureBox imgAjudaPremio7;
        private System.Windows.Forms.PictureBox imgAjudaPremio6;
        private System.Windows.Forms.PictureBox imgAjudaPremio5;
        private System.Windows.Forms.PictureBox imgAjudaPremio4;
        private System.Windows.Forms.PictureBox imgAjudaPremio3;
        private System.Windows.Forms.PictureBox imgAjudaPremio2;
    }
}