namespace SPI
{
    partial class CLT_LeituraJoia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CLT_LeituraJoia));
            this.edtPalavra = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblDataHora = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblNivelAcesso = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.tmrTempo = new System.Windows.Forms.Timer(this.components);
            this.lblSair = new System.Windows.Forms.Label();
            this.lblConsultora = new System.Windows.Forms.Label();
            this.lblTexto1 = new System.Windows.Forms.Label();
            this.lblTexto2 = new System.Windows.Forms.Label();
            this.lblCortes = new System.Windows.Forms.Label();
            this.pnlPainelTexto = new System.Windows.Forms.Panel();
            this.btoFinalizar = new System.Windows.Forms.Button();
            this.btoContinuar = new System.Windows.Forms.Button();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.lblMensagemInicial = new System.Windows.Forms.Label();
            this.btoCancelarEntrega = new System.Windows.Forms.PictureBox();
            this.btoRealizarEntrega = new System.Windows.Forms.PictureBox();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.pnlPainelTexto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btoCancelarEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoRealizarEntrega)).BeginInit();
            this.SuspendLayout();
            // 
            // edtPalavra
            // 
            this.edtPalavra.BackColor = System.Drawing.Color.WhiteSmoke;
            this.edtPalavra.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edtPalavra.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtPalavra.ForeColor = System.Drawing.Color.Aquamarine;
            this.edtPalavra.Location = new System.Drawing.Point(-13, -12);
            this.edtPalavra.Margin = new System.Windows.Forms.Padding(4);
            this.edtPalavra.Name = "edtPalavra";
            this.edtPalavra.Size = new System.Drawing.Size(1, 34);
            this.edtPalavra.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.lblTerminal);
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(821, 832);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(131, 57);
            this.groupBox5.TabIndex = 63;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "TERMINAL";
            this.groupBox5.Visible = false;
            // 
            // lblTerminal
            // 
            this.lblTerminal.AutoSize = true;
            this.lblTerminal.BackColor = System.Drawing.Color.Transparent;
            this.lblTerminal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTerminal.ForeColor = System.Drawing.Color.Yellow;
            this.lblTerminal.Location = new System.Drawing.Point(29, 22);
            this.lblTerminal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(0, 24);
            this.lblTerminal.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.lblDataHora);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(964, 832);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(203, 57);
            this.groupBox2.TabIndex = 62;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "DATA - HORA";
            this.groupBox2.Visible = false;
            // 
            // lblDataHora
            // 
            this.lblDataHora.AutoSize = true;
            this.lblDataHora.BackColor = System.Drawing.Color.Transparent;
            this.lblDataHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataHora.ForeColor = System.Drawing.Color.Yellow;
            this.lblDataHora.Location = new System.Drawing.Point(8, 22);
            this.lblDataHora.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDataHora.Name = "lblDataHora";
            this.lblDataHora.Size = new System.Drawing.Size(0, 24);
            this.lblDataHora.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.lblNivelAcesso);
            this.groupBox3.Controls.Add(this.lblUsuario);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(16, 832);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(791, 57);
            this.groupBox3.TabIndex = 61;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "OPERADOR";
            this.groupBox3.Visible = false;
            // 
            // lblNivelAcesso
            // 
            this.lblNivelAcesso.AutoSize = true;
            this.lblNivelAcesso.Location = new System.Drawing.Point(941, 25);
            this.lblNivelAcesso.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNivelAcesso.Name = "lblNivelAcesso";
            this.lblNivelAcesso.Size = new System.Drawing.Size(0, 17);
            this.lblNivelAcesso.TabIndex = 1;
            this.lblNivelAcesso.Visible = false;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.BackColor = System.Drawing.Color.Transparent;
            this.lblUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.ForeColor = System.Drawing.Color.Yellow;
            this.lblUsuario.Location = new System.Drawing.Point(23, 22);
            this.lblUsuario.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(0, 24);
            this.lblUsuario.TabIndex = 0;
            // 
            // tmrTempo
            // 
            this.tmrTempo.Enabled = true;
            this.tmrTempo.Interval = 6000;
            this.tmrTempo.Tick += new System.EventHandler(this.tmrTempo_Tick);
            // 
            // lblSair
            // 
            this.lblSair.AutoSize = true;
            this.lblSair.BackColor = System.Drawing.Color.Transparent;
            this.lblSair.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSair.ForeColor = System.Drawing.Color.White;
            this.lblSair.Location = new System.Drawing.Point(1171, 873);
            this.lblSair.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSair.Name = "lblSair";
            this.lblSair.Size = new System.Drawing.Size(156, 17);
            this.lblSair.TabIndex = 66;
            this.lblSair.Text = "Para sair tecle (ESC).";
            // 
            // lblConsultora
            // 
            this.lblConsultora.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblConsultora.BackColor = System.Drawing.Color.Transparent;
            this.lblConsultora.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsultora.ForeColor = System.Drawing.Color.Black;
            this.lblConsultora.Location = new System.Drawing.Point(33, 631);
            this.lblConsultora.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblConsultora.Name = "lblConsultora";
            this.lblConsultora.Size = new System.Drawing.Size(1043, 65);
            this.lblConsultora.TabIndex = 67;
            this.lblConsultora.Text = "[NOME]";
            this.lblConsultora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblConsultora.Click += new System.EventHandler(this.lblConsultora_Click);
            // 
            // lblTexto1
            // 
            this.lblTexto1.BackColor = System.Drawing.Color.Transparent;
            this.lblTexto1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTexto1.ForeColor = System.Drawing.Color.Black;
            this.lblTexto1.Location = new System.Drawing.Point(25, 6);
            this.lblTexto1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTexto1.Name = "lblTexto1";
            this.lblTexto1.Size = new System.Drawing.Size(1050, 65);
            this.lblTexto1.TabIndex = 68;
            this.lblTexto1.Text = "Parab�ns pela sua dedica��o e por suas conquistas no ano de 2018. Voc� � ganhador" +
    "a do(s) seguinte(s) Reconhecimento(s):";
            // 
            // lblTexto2
            // 
            this.lblTexto2.BackColor = System.Drawing.Color.Transparent;
            this.lblTexto2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTexto2.ForeColor = System.Drawing.Color.Black;
            this.lblTexto2.Location = new System.Drawing.Point(20, 215);
            this.lblTexto2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTexto2.Name = "lblTexto2";
            this.lblTexto2.Size = new System.Drawing.Size(1055, 116);
            this.lblTexto2.TabIndex = 69;
            this.lblTexto2.Text = "Voc� receber� os pr�mios escolhidos e indicados nas pr�ximas telas. Caso n�o tenh" +
    "a feito a escolha dos seus pr�mios, poder� escolher entre as op��es dispon�veis." +
    "";
            // 
            // lblCortes
            // 
            this.lblCortes.AutoSize = true;
            this.lblCortes.BackColor = System.Drawing.Color.Transparent;
            this.lblCortes.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCortes.ForeColor = System.Drawing.Color.Black;
            this.lblCortes.Location = new System.Drawing.Point(60, 78);
            this.lblCortes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCortes.Name = "lblCortes";
            this.lblCortes.Size = new System.Drawing.Size(259, 29);
            this.lblCortes.TabIndex = 70;
            this.lblCortes.Text = "[RECONHECIMENTO]";
            // 
            // pnlPainelTexto
            // 
            this.pnlPainelTexto.BackColor = System.Drawing.Color.Transparent;
            this.pnlPainelTexto.Controls.Add(this.btoFinalizar);
            this.pnlPainelTexto.Controls.Add(this.btoContinuar);
            this.pnlPainelTexto.Controls.Add(this.lblTexto1);
            this.pnlPainelTexto.Controls.Add(this.lblTexto2);
            this.pnlPainelTexto.Controls.Add(this.lblCortes);
            this.pnlPainelTexto.Location = new System.Drawing.Point(16, 69);
            this.pnlPainelTexto.Margin = new System.Windows.Forms.Padding(4);
            this.pnlPainelTexto.Name = "pnlPainelTexto";
            this.pnlPainelTexto.Size = new System.Drawing.Size(1079, 575);
            this.pnlPainelTexto.TabIndex = 71;
            // 
            // btoFinalizar
            // 
            this.btoFinalizar.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoFinalizar.Location = new System.Drawing.Point(25, 451);
            this.btoFinalizar.Margin = new System.Windows.Forms.Padding(4);
            this.btoFinalizar.Name = "btoFinalizar";
            this.btoFinalizar.Size = new System.Drawing.Size(264, 74);
            this.btoFinalizar.TabIndex = 74;
            this.btoFinalizar.TabStop = false;
            this.btoFinalizar.Text = "FINALIZAR";
            this.btoFinalizar.UseVisualStyleBackColor = true;
            this.btoFinalizar.Visible = false;
            this.btoFinalizar.Click += new System.EventHandler(this.btoFinalizar_Click);
            // 
            // btoContinuar
            // 
            this.btoContinuar.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoContinuar.Location = new System.Drawing.Point(796, 451);
            this.btoContinuar.Margin = new System.Windows.Forms.Padding(4);
            this.btoContinuar.Name = "btoContinuar";
            this.btoContinuar.Size = new System.Drawing.Size(264, 74);
            this.btoContinuar.TabIndex = 73;
            this.btoContinuar.TabStop = false;
            this.btoContinuar.Text = "CONTINUAR";
            this.btoContinuar.UseVisualStyleBackColor = true;
            this.btoContinuar.Click += new System.EventHandler(this.btoContinuar_Click);
            // 
            // lblCodigo
            // 
            this.lblCodigo.Location = new System.Drawing.Point(1140, 832);
            this.lblCodigo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(205, 22);
            this.lblCodigo.TabIndex = 72;
            this.lblCodigo.Visible = false;
            // 
            // lblMensagemInicial
            // 
            this.lblMensagemInicial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMensagemInicial.BackColor = System.Drawing.Color.Transparent;
            this.lblMensagemInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensagemInicial.ForeColor = System.Drawing.Color.Black;
            this.lblMensagemInicial.Location = new System.Drawing.Point(33, 9);
            this.lblMensagemInicial.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMensagemInicial.Name = "lblMensagemInicial";
            this.lblMensagemInicial.Size = new System.Drawing.Size(994, 56);
            this.lblMensagemInicial.TabIndex = 73;
            this.lblMensagemInicial.Text = "POR FAVOR, FA�A A LEITURA DA CREDENCIAL";
            this.lblMensagemInicial.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btoCancelarEntrega
            // 
            this.btoCancelarEntrega.AccessibleRole = System.Windows.Forms.AccessibleRole.Sound;
            this.btoCancelarEntrega.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btoCancelarEntrega.BackColor = System.Drawing.Color.Transparent;
            this.btoCancelarEntrega.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoCancelarEntrega.Image = ((System.Drawing.Image)(resources.GetObject("btoCancelarEntrega.Image")));
            this.btoCancelarEntrega.Location = new System.Drawing.Point(16, 799);
            this.btoCancelarEntrega.Margin = new System.Windows.Forms.Padding(4);
            this.btoCancelarEntrega.Name = "btoCancelarEntrega";
            this.btoCancelarEntrega.Size = new System.Drawing.Size(35, 35);
            this.btoCancelarEntrega.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btoCancelarEntrega.TabIndex = 74;
            this.btoCancelarEntrega.TabStop = false;
            this.btoCancelarEntrega.Click += new System.EventHandler(this.btoCancelarEntrega_Click);
            // 
            // btoRealizarEntrega
            // 
            this.btoRealizarEntrega.AccessibleRole = System.Windows.Forms.AccessibleRole.Sound;
            this.btoRealizarEntrega.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btoRealizarEntrega.BackColor = System.Drawing.Color.Transparent;
            this.btoRealizarEntrega.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoRealizarEntrega.Image = ((System.Drawing.Image)(resources.GetObject("btoRealizarEntrega.Image")));
            this.btoRealizarEntrega.Location = new System.Drawing.Point(1053, 802);
            this.btoRealizarEntrega.Margin = new System.Windows.Forms.Padding(4);
            this.btoRealizarEntrega.Name = "btoRealizarEntrega";
            this.btoRealizarEntrega.Size = new System.Drawing.Size(32, 32);
            this.btoRealizarEntrega.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btoRealizarEntrega.TabIndex = 75;
            this.btoRealizarEntrega.TabStop = false;
            this.btoRealizarEntrega.Visible = false;
            this.btoRealizarEntrega.Click += new System.EventHandler(this.btoRealizarEntrega_Click);
            // 
            // CLT_LeituraJoia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1120, 900);
            this.Controls.Add(this.btoRealizarEntrega);
            this.Controls.Add(this.btoCancelarEntrega);
            this.Controls.Add(this.lblMensagemInicial);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.pnlPainelTexto);
            this.Controls.Add(this.lblConsultora);
            this.Controls.Add(this.edtPalavra);
            this.Controls.Add(this.lblSair);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CLT_LeituraJoia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Localizar Registro";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CLT_LocalizarRegistro_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CLT_LocalizarRegistro_KeyPress);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.pnlPainelTexto.ResumeLayout(false);
            this.pnlPainelTexto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btoCancelarEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoRealizarEntrega)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edtPalavra;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblTerminal;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblDataHora;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblNivelAcesso;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Timer tmrTempo;
        private System.Windows.Forms.Label lblSair;
        private System.Windows.Forms.Label lblConsultora;
        private System.Windows.Forms.Label lblTexto1;
        private System.Windows.Forms.Label lblTexto2;
        private System.Windows.Forms.Label lblCortes;
        private System.Windows.Forms.Panel pnlPainelTexto;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Button btoContinuar;
        private System.Windows.Forms.Button btoFinalizar;
        private System.Windows.Forms.Label lblMensagemInicial;
        private System.Windows.Forms.PictureBox btoCancelarEntrega;
        private System.Windows.Forms.PictureBox btoRealizarEntrega;
    }
}