﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_ConfirmaCancelamento : Form
    {
        public CTO_PremioConsultora AP_PremioConsultora { get; set; }
        public string AP_NomeCOnsultora { get; set; }
        public bool AP_EntregaCancelada { get; set; }

        public CLT_ConfirmaCancelamento()
        {
            InitializeComponent();
        }

        private void btoNao_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CLT_ConfirmaCancelamento_Load(object sender, EventArgs e)
        {
            System.Threading.Thread ts = new System.Threading.Thread(MI_Background);
            ts.Start();

            MI_PosicionarPainel();
            CarregaProdutoCancelamento(AP_PremioConsultora);
        }

        private void MI_Background()
        {
            this.BackgroundImage = Properties.Resources.BackgroundMaryKay;
        }
        private void MI_PosicionarPainel()
        {
            pnlPainelTexto.Location = new Point((this.Size.Width / 2) - (pnlPainelTexto.Size.Width / 2), pnlPainelTexto.Location.Y);
        }

        private void CarregaProdutoCancelamento(CTO_PremioConsultora ITO_PremioConsultora)
        {
            lblConsultora.Text = AP_NomeCOnsultora;
            lblProduto.Text = ITO_PremioConsultora.ITO_Roteiro.dsReconhecimento.ToString();
            MI_ExibirImagem(ITO_PremioConsultora.ITO_Roteiro.cdRoteiro.ToString(), ITO_PremioConsultora.ITO_PremioConsultoraItem.cdItem.ToString());
        }
        private void MI_ExibirImagem(string idRoteiro, string idItemPremio)
        {
            PictureBox foto = (PictureBox)pnlPainelTexto.Controls["imgPremio"];

            if (!String.IsNullOrEmpty(idRoteiro))
            {
                new InserirFoto().MS_SetFoto(foto, idRoteiro);
            }
            else
            {
                CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
                imgPremio.ImageLocation = "ImagensPremios/" + MI_ObterDescricaoFoto(IAT_Visitante.MS_ObterItemPremio(idItemPremio).Rows[0]["dsTrocaLocal"].ToString());
            }
        }
        private string MI_ObterDescricaoFoto(string prCdJoia)
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            return IAT_Visitante.MS_ObterPremioNew(prCdJoia).Rows[0]["dsFoto"].ToString();
        }

        private void GravarCancelamento(CTO_PremioConsultora ITO_PremioConsultora)
        {
            bool boConfirmaCancelamento = false;

            boConfirmaCancelamento = new CAT_PremioConsultoraItem().MS_CancelarEntregaItem(ITO_PremioConsultora.ITO_PremioConsultoraItem.cdItem, new CSV_VariaveisGlobal().getUsuario().ToString(), new CSV_VariaveisGlobal().getNumeroTerminal().ToString());

            if (boConfirmaCancelamento)
            {
                AP_EntregaCancelada = true;
                this.Close();
            }
        }

        private void btoContinuar_Click(object sender, EventArgs e)
        {
            CTO_PremioConsultora ITO_PremioConsultora = new CTO_PremioConsultora();
            ITO_PremioConsultora = (CTO_PremioConsultora)AP_PremioConsultora;

            CLT_LerCodigoProduto abrirCodigoProduto = new CLT_LerCodigoProduto();
            abrirCodigoProduto.AP_codJoia = ITO_PremioConsultora.ITO_PremioConsultoraItem.cdItem.ToString();
            abrirCodigoProduto.AP_NrConsultora = ITO_PremioConsultora.NrConsultora;
            abrirCodigoProduto.AP_CancelamentoEntrega = true;
            abrirCodigoProduto.ShowDialog();

            if (abrirCodigoProduto.CodigoValido)
            {
                GravarCancelamento(AP_PremioConsultora);
            }
        }
    }
}
