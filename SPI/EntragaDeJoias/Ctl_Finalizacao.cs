using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class Ctl_Finalizacao : Form
    {
        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        public string AP_NomeConsultora { get; set; }
        public string AP_CodigoBarras { get; set; }

        public Ctl_Finalizacao()
        {
            InitializeComponent();
        }

        private void MI_Background()
        {
            this.BackgroundImage = Properties.Resources.FundoJoias1;
        }

        private void CLT_LocalizarRegistro_Load(object sender, EventArgs e)
        {
            System.Threading.Thread ts = new System.Threading.Thread(MI_Background);
            ts.Start();

            MI_PosicionarPainel();

            lblConsultora.Text = AP_NomeConsultora;

            MI_AnalisaReconhecimentoParaConviteImprimirReciboEntrega();
        }

        private void MI_AnalisaReconhecimentoParaConviteImprimirReciboEntrega()
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtReconhecimento = IAT_Visitante.MS_ObterReconhecimentos(AP_CodigoBarras);

            MI_ImprimirRecibo();
        }

        private void MI_AnalizarConviteConjuge(string prCodigoBarras)
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            CTO_Visitante ITO_Visitante = IAT_Visitante.MS_ObterPorCodigo(prCodigoBarras);

            if (ITO_Visitante != null)
            {
                string stConsultora = ITO_Visitante.getConsultora();
                if (stConsultora != string.Empty)
                {
                    DataTable dtConjuge = IAT_Visitante.MS_ObterPorCodigoConsultoraConjuge(stConsultora, "");
                    if (dtConjuge.Rows.Count > 0)
                        picConviteConjuge.Visible = true;
                }
            }

        }

        private void MI_ImprimirRecibo()
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtItem = IAT_Visitante.MS_ObterTodosOsItens(AP_CodigoBarras);

            if (CSV_Global.getTipoJoia() != "FAIXA")
            {
                if (dtItem.Rows.Count > 0)
                {
                    // GERAR ASSINATURA
                    Clt_AssinaturaDigital open = new Clt_AssinaturaDigital();
                    open.AP_CodigoBarras = AP_CodigoBarras;
                    open.ShowDialog();

                    //IMPRIMIR RECIBO
                    CAT_ImprimirEtiqueta Imprimir = new CAT_ImprimirEtiqueta(Convert.ToInt64(AP_CodigoBarras), true, dtItem);
                }
            }

        }


        private void MI_PosicionarPainel()
        {
            pnlPainelTexto.Location = new Point((this.Size.Width / 2) - (pnlPainelTexto.Size.Width / 2), pnlPainelTexto.Location.Y); 
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}