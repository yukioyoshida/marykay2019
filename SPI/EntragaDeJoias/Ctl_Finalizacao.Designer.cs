namespace SPI
{
    partial class Ctl_Finalizacao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblConsultora = new System.Windows.Forms.Label();
            this.lblTexto1 = new System.Windows.Forms.Label();
            this.pnlPainelTexto = new System.Windows.Forms.Panel();
            this.pnlReconhecimento = new System.Windows.Forms.Panel();
            this.lblConvite = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.picConviteConjuge = new System.Windows.Forms.PictureBox();
            this.picConvite = new System.Windows.Forms.PictureBox();
            this.pnlPainelTexto.SuspendLayout();
            this.pnlReconhecimento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picConviteConjuge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picConvite)).BeginInit();
            this.SuspendLayout();
            // 
            // lblConsultora
            // 
            this.lblConsultora.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblConsultora.BackColor = System.Drawing.Color.Transparent;
            this.lblConsultora.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsultora.ForeColor = System.Drawing.Color.Black;
            this.lblConsultora.Location = new System.Drawing.Point(33, 631);
            this.lblConsultora.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblConsultora.Name = "lblConsultora";
            this.lblConsultora.Size = new System.Drawing.Size(1284, 65);
            this.lblConsultora.TabIndex = 67;
            this.lblConsultora.Text = "[NOME]";
            this.lblConsultora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTexto1
            // 
            this.lblTexto1.AutoSize = true;
            this.lblTexto1.BackColor = System.Drawing.Color.Transparent;
            this.lblTexto1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTexto1.ForeColor = System.Drawing.Color.Black;
            this.lblTexto1.Location = new System.Drawing.Point(517, 13);
            this.lblTexto1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTexto1.Name = "lblTexto1";
            this.lblTexto1.Size = new System.Drawing.Size(200, 31);
            this.lblTexto1.TabIndex = 68;
            this.lblTexto1.Text = "Muito obrigado!";
            // 
            // pnlPainelTexto
            // 
            this.pnlPainelTexto.BackColor = System.Drawing.Color.Transparent;
            this.pnlPainelTexto.Controls.Add(this.pnlReconhecimento);
            this.pnlPainelTexto.Location = new System.Drawing.Point(16, 52);
            this.pnlPainelTexto.Margin = new System.Windows.Forms.Padding(4);
            this.pnlPainelTexto.Name = "pnlPainelTexto";
            this.pnlPainelTexto.Size = new System.Drawing.Size(1329, 576);
            this.pnlPainelTexto.TabIndex = 71;
            // 
            // pnlReconhecimento
            // 
            this.pnlReconhecimento.BackColor = System.Drawing.Color.Transparent;
            this.pnlReconhecimento.Controls.Add(this.picConviteConjuge);
            this.pnlReconhecimento.Controls.Add(this.lblConvite);
            this.pnlReconhecimento.Controls.Add(this.picConvite);
            this.pnlReconhecimento.Controls.Add(this.label1);
            this.pnlReconhecimento.Controls.Add(this.lblTexto1);
            this.pnlReconhecimento.Location = new System.Drawing.Point(28, 22);
            this.pnlReconhecimento.Margin = new System.Windows.Forms.Padding(4);
            this.pnlReconhecimento.Name = "pnlReconhecimento";
            this.pnlReconhecimento.Size = new System.Drawing.Size(1257, 535);
            this.pnlReconhecimento.TabIndex = 71;
            // 
            // lblConvite
            // 
            this.lblConvite.BackColor = System.Drawing.Color.Transparent;
            this.lblConvite.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConvite.ForeColor = System.Drawing.Color.Black;
            this.lblConvite.Location = new System.Drawing.Point(4, 123);
            this.lblConvite.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblConvite.Name = "lblConvite";
            this.lblConvite.Size = new System.Drawing.Size(1249, 75);
            this.lblConvite.TabIndex = 71;
            this.lblConvite.Text = "Voc� � uma das ganhadoras das Cortes do Viva Mary Kay 2016. Voc� receber� o(s) co" +
    "nvite(s) abaixo do Coquetel da Realeza.";
            this.lblConvite.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblConvite.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(369, 59);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(491, 31);
            this.label1.TabIndex = 69;
            this.label1.Text = "Desejamos que tenha um �timo evento.";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 8000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // picConviteConjuge
            // 
            this.picConviteConjuge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.picConviteConjuge.Image = global::SPI.Properties.Resources.foto_convite_coquetel_da_realeza___conjuge;
            this.picConviteConjuge.Location = new System.Drawing.Point(247, 374);
            this.picConviteConjuge.Margin = new System.Windows.Forms.Padding(4);
            this.picConviteConjuge.Name = "picConviteConjuge";
            this.picConviteConjuge.Size = new System.Drawing.Size(771, 114);
            this.picConviteConjuge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picConviteConjuge.TabIndex = 72;
            this.picConviteConjuge.TabStop = false;
            this.picConviteConjuge.Visible = false;
            // 
            // picConvite
            // 
            this.picConvite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.picConvite.Image = global::SPI.Properties.Resources.foto_convite_coquetel_da_realeza1;
            this.picConvite.Location = new System.Drawing.Point(247, 248);
            this.picConvite.Margin = new System.Windows.Forms.Padding(4);
            this.picConvite.Name = "picConvite";
            this.picConvite.Size = new System.Drawing.Size(771, 114);
            this.picConvite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picConvite.TabIndex = 70;
            this.picConvite.TabStop = false;
            this.picConvite.Visible = false;
            // 
            // Ctl_Finalizacao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1361, 900);
            this.Controls.Add(this.pnlPainelTexto);
            this.Controls.Add(this.lblConsultora);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Ctl_Finalizacao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Localizar Registro";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CLT_LocalizarRegistro_Load);
            this.pnlPainelTexto.ResumeLayout(false);
            this.pnlReconhecimento.ResumeLayout(false);
            this.pnlReconhecimento.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picConviteConjuge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picConvite)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblConsultora;
        private System.Windows.Forms.Label lblTexto1;
        private System.Windows.Forms.Panel pnlPainelTexto;
        private System.Windows.Forms.Panel pnlReconhecimento;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox picConvite;
        private System.Windows.Forms.Label lblConvite;
        private System.Windows.Forms.PictureBox picConviteConjuge;
    }
}