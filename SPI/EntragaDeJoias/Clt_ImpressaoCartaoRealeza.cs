﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class Clt_ImpressaoCartaoRealeza : Form
    {
        public string   AP_Reconhecimento { get; set; }
        public bool     AP_boResult { get; set; }

        public Clt_ImpressaoCartaoRealeza()
        {
            InitializeComponent();
        }

        private void Clt_ConfirmaEscolha_Load(object sender, EventArgs e)
        {
            AP_boResult = false;
            lblReconhecimento.Text = AP_Reconhecimento;
        }

        private void btoNAO_Click(object sender, EventArgs e)
        {
            AP_boResult = false;
            this.Close();
        }

        private void btoSIM_Click(object sender, EventArgs e)
        {
            AP_boResult = true;
            this.Close();
        }
    }
}
