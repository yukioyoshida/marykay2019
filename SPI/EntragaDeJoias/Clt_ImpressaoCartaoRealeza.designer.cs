﻿namespace SPI
{
    partial class Clt_ImpressaoCartaoRealeza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCorteSelecionada = new System.Windows.Forms.Label();
            this.btoNAO = new System.Windows.Forms.Button();
            this.btoSIM = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblReconhecimento = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCorteSelecionada
            // 
            this.lblCorteSelecionada.AutoSize = true;
            this.lblCorteSelecionada.BackColor = System.Drawing.Color.Transparent;
            this.lblCorteSelecionada.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorteSelecionada.ForeColor = System.Drawing.Color.Gray;
            this.lblCorteSelecionada.Location = new System.Drawing.Point(8, 98);
            this.lblCorteSelecionada.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCorteSelecionada.Name = "lblCorteSelecionada";
            this.lblCorteSelecionada.Size = new System.Drawing.Size(616, 32);
            this.lblCorteSelecionada.TabIndex = 71;
            this.lblCorteSelecionada.Text = "A etiqueta do cartão foi impresso com sucesso?";
            // 
            // btoNAO
            // 
            this.btoNAO.BackColor = System.Drawing.Color.White;
            this.btoNAO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btoNAO.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoNAO.Location = new System.Drawing.Point(89, 249);
            this.btoNAO.Margin = new System.Windows.Forms.Padding(4);
            this.btoNAO.Name = "btoNAO";
            this.btoNAO.Size = new System.Drawing.Size(211, 62);
            this.btoNAO.TabIndex = 73;
            this.btoNAO.TabStop = false;
            this.btoNAO.Text = "NÃO";
            this.btoNAO.UseVisualStyleBackColor = false;
            this.btoNAO.Click += new System.EventHandler(this.btoNAO_Click);
            // 
            // btoSIM
            // 
            this.btoSIM.BackColor = System.Drawing.Color.White;
            this.btoSIM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btoSIM.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoSIM.Location = new System.Drawing.Point(449, 249);
            this.btoSIM.Margin = new System.Windows.Forms.Padding(4);
            this.btoSIM.Name = "btoSIM";
            this.btoSIM.Size = new System.Drawing.Size(211, 62);
            this.btoSIM.TabIndex = 74;
            this.btoSIM.TabStop = false;
            this.btoSIM.Text = "SIM";
            this.btoSIM.UseVisualStyleBackColor = false;
            this.btoSIM.Click += new System.EventHandler(this.btoSIM_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btoSIM);
            this.panel1.Controls.Add(this.btoNAO);
            this.panel1.Controls.Add(this.lblReconhecimento);
            this.panel1.Controls.Add(this.lblCorteSelecionada);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(806, 359);
            this.panel1.TabIndex = 76;
            // 
            // lblReconhecimento
            // 
            this.lblReconhecimento.AutoSize = true;
            this.lblReconhecimento.BackColor = System.Drawing.Color.Transparent;
            this.lblReconhecimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReconhecimento.ForeColor = System.Drawing.Color.Gray;
            this.lblReconhecimento.Location = new System.Drawing.Point(11, 49);
            this.lblReconhecimento.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblReconhecimento.Name = "lblReconhecimento";
            this.lblReconhecimento.Size = new System.Drawing.Size(269, 36);
            this.lblReconhecimento.TabIndex = 75;
            this.lblReconhecimento.Text = "lblReconhecimento";
            // 
            // Clt_ImpressaoCartaoRealeza
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(811, 365);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Clt_ImpressaoCartaoRealeza";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Clt_ConfirmaEscolha";
            this.Load += new System.EventHandler(this.Clt_ConfirmaEscolha_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblCorteSelecionada;
        private System.Windows.Forms.Button btoNAO;
        private System.Windows.Forms.Button btoSIM;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblReconhecimento;
    }
}