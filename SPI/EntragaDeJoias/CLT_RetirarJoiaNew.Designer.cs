namespace SPI
{
    partial class CLT_RetirarJoiaNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CLT_RetirarJoiaNew));
            this.tmrTempo = new System.Windows.Forms.Timer(this.components);
            this.lblConsultora = new System.Windows.Forms.Label();
            this.lblTexto1 = new System.Windows.Forms.Label();
            this.pnlPainelTexto = new System.Windows.Forms.Panel();
            this.chkItemAcima = new System.Windows.Forms.PictureBox();
            this.chkLiConcordo = new System.Windows.Forms.PictureBox();
            this.btoContinuar = new System.Windows.Forms.Button();
            this.btoObservacoes = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTextoGeral = new System.Windows.Forms.Label();
            this.imgPremio = new System.Windows.Forms.PictureBox();
            this.btoTrocarJoia = new System.Windows.Forms.PictureBox();
            this.pnlPainelTexto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkItemAcima)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLiConcordo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoTrocarJoia)).BeginInit();
            this.SuspendLayout();
            // 
            // tmrTempo
            // 
            this.tmrTempo.Enabled = true;
            this.tmrTempo.Interval = 6000;
            // 
            // lblConsultora
            // 
            this.lblConsultora.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblConsultora.BackColor = System.Drawing.Color.Transparent;
            this.lblConsultora.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsultora.ForeColor = System.Drawing.Color.Black;
            this.lblConsultora.Location = new System.Drawing.Point(33, 631);
            this.lblConsultora.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblConsultora.Name = "lblConsultora";
            this.lblConsultora.Size = new System.Drawing.Size(1043, 65);
            this.lblConsultora.TabIndex = 67;
            this.lblConsultora.Text = "[NOME]";
            this.lblConsultora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblConsultora.Click += new System.EventHandler(this.lblConsultora_Click);
            // 
            // lblTexto1
            // 
            this.lblTexto1.AutoSize = true;
            this.lblTexto1.BackColor = System.Drawing.Color.Transparent;
            this.lblTexto1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.lblTexto1.ForeColor = System.Drawing.Color.Black;
            this.lblTexto1.Location = new System.Drawing.Point(20, 15);
            this.lblTexto1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTexto1.Name = "lblTexto1";
            this.lblTexto1.Size = new System.Drawing.Size(779, 31);
            this.lblTexto1.TabIndex = 68;
            this.lblTexto1.Text = "Voc� retirou o pr�mio abaixo. Por favor confirme o recebimento:";
            // 
            // pnlPainelTexto
            // 
            this.pnlPainelTexto.BackColor = System.Drawing.Color.Transparent;
            this.pnlPainelTexto.Controls.Add(this.chkItemAcima);
            this.pnlPainelTexto.Controls.Add(this.chkLiConcordo);
            this.pnlPainelTexto.Controls.Add(this.btoContinuar);
            this.pnlPainelTexto.Controls.Add(this.btoObservacoes);
            this.pnlPainelTexto.Controls.Add(this.label3);
            this.pnlPainelTexto.Controls.Add(this.label2);
            this.pnlPainelTexto.Controls.Add(this.lblTextoGeral);
            this.pnlPainelTexto.Controls.Add(this.imgPremio);
            this.pnlPainelTexto.Controls.Add(this.lblTexto1);
            this.pnlPainelTexto.Location = new System.Drawing.Point(16, 52);
            this.pnlPainelTexto.Margin = new System.Windows.Forms.Padding(4);
            this.pnlPainelTexto.Name = "pnlPainelTexto";
            this.pnlPainelTexto.Size = new System.Drawing.Size(1091, 576);
            this.pnlPainelTexto.TabIndex = 71;
            // 
            // chkItemAcima
            // 
            this.chkItemAcima.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkItemAcima.Location = new System.Drawing.Point(71, 486);
            this.chkItemAcima.Margin = new System.Windows.Forms.Padding(4);
            this.chkItemAcima.Name = "chkItemAcima";
            this.chkItemAcima.Size = new System.Drawing.Size(68, 62);
            this.chkItemAcima.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.chkItemAcima.TabIndex = 80;
            this.chkItemAcima.TabStop = false;
            this.chkItemAcima.Click += new System.EventHandler(this.chkItemAcima_Click);
            // 
            // chkLiConcordo
            // 
            this.chkLiConcordo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkLiConcordo.Location = new System.Drawing.Point(71, 417);
            this.chkLiConcordo.Margin = new System.Windows.Forms.Padding(4);
            this.chkLiConcordo.Name = "chkLiConcordo";
            this.chkLiConcordo.Size = new System.Drawing.Size(67, 62);
            this.chkLiConcordo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.chkLiConcordo.TabIndex = 79;
            this.chkLiConcordo.TabStop = false;
            this.chkLiConcordo.Click += new System.EventHandler(this.chkLiConcordo_Click);
            // 
            // btoContinuar
            // 
            this.btoContinuar.BackColor = System.Drawing.Color.White;
            this.btoContinuar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btoContinuar.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoContinuar.Location = new System.Drawing.Point(859, 495);
            this.btoContinuar.Margin = new System.Windows.Forms.Padding(4);
            this.btoContinuar.Name = "btoContinuar";
            this.btoContinuar.Size = new System.Drawing.Size(211, 62);
            this.btoContinuar.TabIndex = 78;
            this.btoContinuar.TabStop = false;
            this.btoContinuar.Text = "Continuar";
            this.btoContinuar.UseVisualStyleBackColor = false;
            this.btoContinuar.Click += new System.EventHandler(this.btoContinuar_Click);
            // 
            // btoObservacoes
            // 
            this.btoObservacoes.BackColor = System.Drawing.Color.White;
            this.btoObservacoes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btoObservacoes.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoObservacoes.Location = new System.Drawing.Point(640, 495);
            this.btoObservacoes.Margin = new System.Windows.Forms.Padding(4);
            this.btoObservacoes.Name = "btoObservacoes";
            this.btoObservacoes.Size = new System.Drawing.Size(211, 62);
            this.btoObservacoes.TabIndex = 77;
            this.btoObservacoes.TabStop = false;
            this.btoObservacoes.Text = "Observa��es";
            this.btoObservacoes.UseVisualStyleBackColor = false;
            this.btoObservacoes.Click += new System.EventHandler(this.btoObservacoes_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(145, 501);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(294, 29);
            this.label3.TabIndex = 76;
            this.label3.Text = "Recebi o(s) item(s) acima.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(145, 437);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(558, 29);
            this.label2.TabIndex = 75;
            this.label2.Text = "Li, entendi e concordo com os termos e condi��es.";
            // 
            // lblTextoGeral
            // 
            this.lblTextoGeral.BackColor = System.Drawing.Color.Transparent;
            this.lblTextoGeral.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextoGeral.ForeColor = System.Drawing.Color.Black;
            this.lblTextoGeral.Location = new System.Drawing.Point(43, 281);
            this.lblTextoGeral.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTextoGeral.Name = "lblTextoGeral";
            this.lblTextoGeral.Size = new System.Drawing.Size(1044, 134);
            this.lblTextoGeral.TabIndex = 74;
            this.lblTextoGeral.Text = resources.GetString("lblTextoGeral.Text");
            // 
            // imgPremio
            // 
            this.imgPremio.Location = new System.Drawing.Point(51, 75);
            this.imgPremio.Margin = new System.Windows.Forms.Padding(4);
            this.imgPremio.Name = "imgPremio";
            this.imgPremio.Size = new System.Drawing.Size(205, 187);
            this.imgPremio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPremio.TabIndex = 73;
            this.imgPremio.TabStop = false;
            // 
            // btoTrocarJoia
            // 
            this.btoTrocarJoia.AccessibleRole = System.Windows.Forms.AccessibleRole.Sound;
            this.btoTrocarJoia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btoTrocarJoia.BackColor = System.Drawing.Color.Transparent;
            this.btoTrocarJoia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoTrocarJoia.Image = global::SPI.Properties.Resources.refresh_joia;
            this.btoTrocarJoia.Location = new System.Drawing.Point(33, 827);
            this.btoTrocarJoia.Margin = new System.Windows.Forms.Padding(4);
            this.btoTrocarJoia.Name = "btoTrocarJoia";
            this.btoTrocarJoia.Size = new System.Drawing.Size(38, 32);
            this.btoTrocarJoia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btoTrocarJoia.TabIndex = 72;
            this.btoTrocarJoia.TabStop = false;
            this.btoTrocarJoia.Click += new System.EventHandler(this.btoTrocarJoia_Click);
            // 
            // CLT_RetirarJoiaNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1120, 900);
            this.Controls.Add(this.btoTrocarJoia);
            this.Controls.Add(this.pnlPainelTexto);
            this.Controls.Add(this.lblConsultora);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CLT_RetirarJoiaNew";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Localizar Registro";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CLT_LocalizarRegistro_Load);
            this.pnlPainelTexto.ResumeLayout(false);
            this.pnlPainelTexto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkItemAcima)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLiConcordo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoTrocarJoia)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer tmrTempo;
        private System.Windows.Forms.Label lblConsultora;
        private System.Windows.Forms.Label lblTexto1;
        private System.Windows.Forms.Panel pnlPainelTexto;
        private System.Windows.Forms.PictureBox imgPremio;
        private System.Windows.Forms.Label lblTextoGeral;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btoContinuar;
        private System.Windows.Forms.Button btoObservacoes;
        private System.Windows.Forms.PictureBox chkLiConcordo;
        private System.Windows.Forms.PictureBox chkItemAcima;
        private System.Windows.Forms.PictureBox btoTrocarJoia;
    }
}