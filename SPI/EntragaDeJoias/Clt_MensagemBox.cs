﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class Clt_MensagemBox : Form
    {
        public string   AP_Mensagem { get; set; }

        public Clt_MensagemBox()
        {
            InitializeComponent();
        }

        private void Clt_ConfirmaEscolha_Load(object sender, EventArgs e)
        {
            lblMensagem.Text = AP_Mensagem;
        }

        private void btoOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
