﻿namespace SPI
{
    partial class CLT_ConfirmaCancelamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTexto1 = new System.Windows.Forms.Label();
            this.imgPremio = new System.Windows.Forms.PictureBox();
            this.btoContinuar = new System.Windows.Forms.Button();
            this.btoNao = new System.Windows.Forms.Button();
            this.lblProduto = new System.Windows.Forms.Label();
            this.lblConsultora = new System.Windows.Forms.Label();
            this.pnlPainelTexto = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio)).BeginInit();
            this.pnlPainelTexto.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTexto1
            // 
            this.lblTexto1.BackColor = System.Drawing.Color.Transparent;
            this.lblTexto1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.lblTexto1.ForeColor = System.Drawing.Color.Black;
            this.lblTexto1.Location = new System.Drawing.Point(12, 16);
            this.lblTexto1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTexto1.Name = "lblTexto1";
            this.lblTexto1.Size = new System.Drawing.Size(948, 31);
            this.lblTexto1.TabIndex = 69;
            this.lblTexto1.Text = "Você tem certeza que deseja cancelar o ítem abaixo?";
            this.lblTexto1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // imgPremio
            // 
            this.imgPremio.Location = new System.Drawing.Point(404, 82);
            this.imgPremio.Margin = new System.Windows.Forms.Padding(4);
            this.imgPremio.Name = "imgPremio";
            this.imgPremio.Size = new System.Drawing.Size(205, 187);
            this.imgPremio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPremio.TabIndex = 74;
            this.imgPremio.TabStop = false;
            // 
            // btoContinuar
            // 
            this.btoContinuar.BackColor = System.Drawing.Color.White;
            this.btoContinuar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btoContinuar.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoContinuar.Location = new System.Drawing.Point(749, 445);
            this.btoContinuar.Margin = new System.Windows.Forms.Padding(4);
            this.btoContinuar.Name = "btoContinuar";
            this.btoContinuar.Size = new System.Drawing.Size(211, 62);
            this.btoContinuar.TabIndex = 80;
            this.btoContinuar.TabStop = false;
            this.btoContinuar.Text = "SIM";
            this.btoContinuar.UseVisualStyleBackColor = false;
            this.btoContinuar.Click += new System.EventHandler(this.btoContinuar_Click);
            // 
            // btoNao
            // 
            this.btoNao.BackColor = System.Drawing.Color.White;
            this.btoNao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btoNao.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoNao.Location = new System.Drawing.Point(12, 445);
            this.btoNao.Margin = new System.Windows.Forms.Padding(4);
            this.btoNao.Name = "btoNao";
            this.btoNao.Size = new System.Drawing.Size(211, 62);
            this.btoNao.TabIndex = 79;
            this.btoNao.TabStop = false;
            this.btoNao.Text = "NÃO";
            this.btoNao.UseVisualStyleBackColor = false;
            this.btoNao.Click += new System.EventHandler(this.btoNao_Click);
            // 
            // lblProduto
            // 
            this.lblProduto.BackColor = System.Drawing.Color.Transparent;
            this.lblProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.lblProduto.ForeColor = System.Drawing.Color.Black;
            this.lblProduto.Location = new System.Drawing.Point(12, 286);
            this.lblProduto.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProduto.Name = "lblProduto";
            this.lblProduto.Size = new System.Drawing.Size(948, 31);
            this.lblProduto.TabIndex = 81;
            this.lblProduto.Text = "[PRODUTO]";
            this.lblProduto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblConsultora
            // 
            this.lblConsultora.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblConsultora.BackColor = System.Drawing.Color.Transparent;
            this.lblConsultora.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsultora.ForeColor = System.Drawing.Color.Black;
            this.lblConsultora.Location = new System.Drawing.Point(18, 362);
            this.lblConsultora.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblConsultora.Name = "lblConsultora";
            this.lblConsultora.Size = new System.Drawing.Size(932, 65);
            this.lblConsultora.TabIndex = 82;
            this.lblConsultora.Text = "[NOME]";
            this.lblConsultora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlPainelTexto
            // 
            this.pnlPainelTexto.Controls.Add(this.imgPremio);
            this.pnlPainelTexto.Controls.Add(this.lblTexto1);
            this.pnlPainelTexto.Controls.Add(this.btoContinuar);
            this.pnlPainelTexto.Controls.Add(this.lblConsultora);
            this.pnlPainelTexto.Controls.Add(this.btoNao);
            this.pnlPainelTexto.Controls.Add(this.lblProduto);
            this.pnlPainelTexto.Location = new System.Drawing.Point(56, 65);
            this.pnlPainelTexto.Name = "pnlPainelTexto";
            this.pnlPainelTexto.Size = new System.Drawing.Size(973, 527);
            this.pnlPainelTexto.TabIndex = 83;
            // 
            // CLT_ConfirmaCancelamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1102, 853);
            this.Controls.Add(this.pnlPainelTexto);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CLT_ConfirmaCancelamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Confirmação de cancelamento";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CLT_ConfirmaCancelamento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgPremio)).EndInit();
            this.pnlPainelTexto.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTexto1;
        private System.Windows.Forms.PictureBox imgPremio;
        private System.Windows.Forms.Button btoContinuar;
        private System.Windows.Forms.Button btoNao;
        private System.Windows.Forms.Label lblProduto;
        private System.Windows.Forms.Label lblConsultora;
        private System.Windows.Forms.Panel pnlPainelTexto;
    }
}