﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class Clt_TrocaDeJoia : Form
    {
        public string   AP_Reconhecimento { get; set; }
        public string   AP_PremioConsultora { get; set; }
        public string   AP_cdPremio { get; set; }
        public string   AP_Premio { get; set; }
        public bool     AP_boResult { get; set; } 

        public Clt_TrocaDeJoia()
        {
            InitializeComponent();
        }

        private void Clt_ConfirmaEscolha_Load(object sender, EventArgs e)
        {
            AP_boResult = false;
            AP_PremioConsultora = new DAO_Visitante().MS_ObterItemPremio(AP_cdPremio).Rows[0]["cdReconhecimento"].ToString();
            AP_Reconhecimento = new DAO_Visitante().MS_ObterPrimioConsultoraPorIdPremio(AP_PremioConsultora).Rows[0]["dsReconhecimento"].ToString();

            // OBTER OS ITENS PARA ESCOLHA
            DataTable dtPremiosEscolha = new DAO_Visitante().MS_ObterTodosOsPremio();
            int itEscolha = 1;
            foreach (DataRow RowPremio in dtPremiosEscolha.Rows)
            {
                if (MI_ObterPermissaoDisponibilizacao(RowPremio["dsReconhecimento"].ToString()))
                {
                    PictureBox img = (PictureBox)panel1.Controls["imgPremio" + itEscolha.ToString()];
                    new InserirFoto().MS_SetFoto(img, RowPremio["cdRoteiro"].ToString());
                    img.Visible = true;
                    img.Tag = RowPremio["cdRoteiro"].ToString().ToString();

                    // ICONE HELP
                    PictureBox imgHelp = (PictureBox)panel1.Controls["imgAjudaPremio" + itEscolha.ToString()];
                    imgHelp.Tag = RowPremio["dsReconhecimento"].ToString();
                    imgHelp.Visible = true;

                    itEscolha++;
                }
            }
        }

        private bool MI_ObterPermissaoDisponibilizacao(string prNomePremio)
        {
            bool boResult = false;
            AP_Reconhecimento = AP_Reconhecimento.Trim();

            switch (prNomePremio)
            {
                case "Corrente em ouro rosé 18k de 46cm com pingente de quartzo rosa 5x5mm":
                    if ((AP_Reconhecimento == "Corte de VP Consultoras") || 
                        (AP_Reconhecimento == "Corte de VP Diretoras") || 
                        (AP_Reconhecimento == "Corte de Iniciação") ||
                        (AP_Reconhecimento == "Corte das Unidades - Circulo dos 500 mil pontos")
                        )
                    {
                        boResult = true;
                    }
                    break;
                case "Relógio Vivara em aço prateado e dourado":
                    if ((AP_Reconhecimento == "Corte de VP Consultoras") ||
                    (AP_Reconhecimento == "Corte de VP Diretoras") ||
                    (AP_Reconhecimento == "Corte de Iniciação") ||
                    (AP_Reconhecimento == "Corte das Unidades - Circulo dos 500 mil pontos")
                    )
                    {
                        boResult = true;
                    }
                    break;
                case "Relógio Vivara em aço prateado e rosé":
                    if ((AP_Reconhecimento == "Corte de VP Consultoras") ||
                    (AP_Reconhecimento == "Corte de VP Diretoras") ||
                    (AP_Reconhecimento == "Corte de Iniciação") ||
                    (AP_Reconhecimento == "Corte das Unidades - Circulo dos 500 mil pontos")
                    )
                    {
                        boResult = true;
                    }
                    break;
                case "Broche de abelha tamanho médio em ouro amarelo 18k, com efeitos brilhantes e olhos de esmeralda":
                    if (AP_Reconhecimento == "Corte de Iniciação")
                    {
                        boResult = true;
                    }
                    break;
                case "Relógio Tommy Hilfiger em aço cor chumbo":
                    if (AP_Reconhecimento == "Corte das Unidades - Circulo dos 600 mil pontos")
                    {
                        boResult = true;
                    }
                    break;
                case "Relógio Vivara Rosé Begônia com Brilhantes":
                    if (AP_Reconhecimento == "Corte das Unidades - Circulo dos 600 mil pontos")
                    {
                        boResult = true;
                    }
                    break;
                case "Corrente em ouro rosé 18k de 46cm com pingente de quartzo rosa 6x6mm com diamante":
                    if (AP_Reconhecimento == "Corte das Unidades - Circulo dos 600 mil pontos")
                    {
                        boResult = true;
                    }
                    break;
                default:
                    boResult = false;
                    break;
            }


            return boResult;
        }

        private void btoNAO_Click(object sender, EventArgs e)
        {
            AP_boResult = false;
            this.Close();
        }

        private void Trocar_Click(object sender, EventArgs e)
        {
            PictureBox bot = (PictureBox)sender;
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            IAT_Visitante.MS_TrocarJoia(AP_cdPremio, bot.Tag.ToString());
            AP_Premio = bot.Tag.ToString();
            this.Close();
        }

        private void Help_Click(object sender, EventArgs e)
        {
            PictureBox bto = (PictureBox)sender;
            string img = bto.Name.ToString().Replace("imgAjudaPremio", "");
            PictureBox pic = (PictureBox)panel1.Controls["imgPremio" + img.ToString()];

            EntragaDeJoias.Clt_DetalheJoia open = new EntragaDeJoias.Clt_DetalheJoia();
            open.ImgFoto = pic;
            open.AP_Descricao = bto.Tag.ToString();

            open.ShowDialog();
        }
    }
}
