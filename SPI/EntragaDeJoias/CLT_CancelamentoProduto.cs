﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_CancelamentoProduto : Form
    {
        public string AP_CodigoBarras { get; set; }
        public string AP_NumeroConsultora { get; set; }
        public string AP_NomeConsultora { get; set; }
        public bool AP_ExibeMensagemSucesso { get; set; }

        public CLT_CancelamentoProduto()
        {
            InitializeComponent();
        }
        private void CLT_CancelamentoProduto_Load(object sender, EventArgs e)
        {
            MI_Background();
            MI_PosicionarPainel();
            CarregarReconhecimentosRetirados();
        }
        private void MI_Background()
        {
            this.BackgroundImage = Properties.Resources.BackgroundMaryKay;
        }
        private void MI_PosicionarPainel()
        {
            pnlPainelTexto.Location = new Point((this.Size.Width / 2) - (pnlPainelTexto.Size.Width / 2), pnlPainelTexto.Location.Y);
        }

        private void CarregarReconhecimentosRetirados()
        {
            pnlReconhecimento.Controls.Clear();

            List<CTO_PremioConsultora> lITO_PremioConsultora = new List<CTO_PremioConsultora>();
            lITO_PremioConsultora = new CAT_PremioConsultora().MS_ObterPremiacaoPorCodigoConsultora(AP_NumeroConsultora);

            if (lITO_PremioConsultora.Count > 0)
            {

                lblConsultora.Text = AP_NomeConsultora;
                bool boSemItens = true;

                int it_Y = 20;
                foreach (CTO_PremioConsultora Row in lITO_PremioConsultora)
                {   
                    if (MI_VerificarSeRetirado(Convert.ToInt32(Row.ObjRef)))
                    {
                        MI_MontarReconhecimentoButton(it_Y.ToString(), Row, it_Y);
                        it_Y += 55;
                        boSemItens = false;
                    }
                }

                if (boSemItens)
                { 
                    lblConsultora.Text = "Não há ítens para cancelar";
                    AP_ExibeMensagemSucesso = false;
                    Timer oTimer = new Timer();
                    oTimer.Interval = 5000;
                    oTimer.Tick += new EventHandler(oTimer_Tick);
                    oTimer.Start();
                }
            }
            else
            { 
                lblConsultora.Text = "Não há ítens para cancelar";

                AP_ExibeMensagemSucesso = false;
                Timer oTimer = new Timer();
                oTimer.Interval = 5000;
                oTimer.Tick += new EventHandler(oTimer_Tick);
                oTimer.Start();
            }
        }

        private void oTimer_Tick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MI_MontarReconhecimentoButton(string prID, CTO_PremioConsultora Row, int it_Y)
        {
            Button objControle = new Button();

            objControle.BackColor = System.Drawing.Color.White;
            objControle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            objControle.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            objControle.Location = new System.Drawing.Point(30, it_Y);
            objControle.Size = new System.Drawing.Size(700, 50);
            objControle.TabIndex = 0;
            objControle.TabStop = false;
            objControle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            objControle.UseVisualStyleBackColor = false;

            objControle.Text = Row.dsReconhecimento.ToString();// Row["dsReconhecimento"].ToString();
            objControle.Tag = Row; //Row.ObjRef.ToString();//Row["ObjRef"].ToString();

            objControle.Click += new System.EventHandler(this.Reconhecimento_Click);

            pnlReconhecimento.Controls.Add(objControle);
        }
        private bool MI_VerificarSeRetirado(Int32 prCdReconhecimento)
        {
            bool boResult = true;
            string prTexto = string.Empty;

            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtPremio = new DataTable();

            dtPremio = IAT_Visitante.MS_ObterPremiosDaConsultora(prCdReconhecimento.ToString(), "PREMIO");
            foreach (DataRow Row in dtPremio.Rows)
            {
                if (Row["dsRetirado"].ToString().ToUpper().Trim() == "N")
                {
                    boResult = false;
                }
            }

            dtPremio = IAT_Visitante.MS_ObterPremiosDaConsultora(prCdReconhecimento.ToString(), "NUMERAL");
            foreach (DataRow Row in dtPremio.Rows)
            {
                if (Row["dsRetirado"].ToString().ToUpper().Trim() == "N")
                {
                    boResult = false;
                }
            }

            dtPremio = IAT_Visitante.MS_ObterPremiosDaConsultora(prCdReconhecimento.ToString(), "Charm");
            foreach (DataRow Row in dtPremio.Rows)
            {
                if (Row["dsRetirado"].ToString().ToUpper().Trim() == "N")
                {
                    boResult = false;
                }
            }

            dtPremio = IAT_Visitante.MS_ObterPremiosDaConsultora(prCdReconhecimento.ToString(), "Faixa");
            foreach (DataRow Row in dtPremio.Rows)
            {
                if (Row["dsRetirado"].ToString().ToUpper().Trim() == "N")
                {
                    boResult = false;
                }
            }

            if (boResult == false)
            {
                prTexto = "retirar prêmio";

                // VERIFICAR SE PRECISA ESCOLHER PREMIO
                dtPremio = IAT_Visitante.MS_ObterPremiosDaConsultora(prCdReconhecimento.ToString(), "PREMIO");
                foreach (DataRow Row in dtPremio.Rows)
                {
                    if (Row["dsTrocaLocal"].ToString().Trim() == "")
                    {
                        prTexto = "escolher prêmio";
                    }
                }
            }

            return boResult;
        }
        private void Reconhecimento_Click(object sender, EventArgs e)
        {
            Button btReconhecimento = (Button)sender;
            CLT_ConfirmaCancelamento abrirConfirmacaoCancelamento = new CLT_ConfirmaCancelamento();
            abrirConfirmacaoCancelamento.AP_PremioConsultora = (CTO_PremioConsultora)btReconhecimento.Tag;
            abrirConfirmacaoCancelamento.AP_NomeCOnsultora = AP_NomeConsultora;
            abrirConfirmacaoCancelamento.ShowDialog();

            if (abrirConfirmacaoCancelamento.AP_EntregaCancelada)
                AP_ExibeMensagemSucesso = true;

            this.Close();
        }

    }
}
