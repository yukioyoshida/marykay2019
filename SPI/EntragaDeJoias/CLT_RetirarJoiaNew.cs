using System;
using System.Drawing;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_RetirarJoiaNew : Form
    {
        CSV_VariaveisGlobal CSV_Global = new CSV_VariaveisGlobal();

        public string AP_CodigoBarras { get; set; }
        public string AP_NrConsultora { get; set; }
        public string AP_NomeConsultora { get; set; }
        public string AP_CdItemJoia { get; set; }
        public string AP_Premio { get; set; }
        public string AP_Correio { get; set; }

        public CLT_RetirarJoiaNew()
        {
            InitializeComponent();
        }

        private void MI_Background()
        {
            this.BackgroundImage = Properties.Resources.BackgroundMaryKay;
        }

        private void CLT_LocalizarRegistro_Load(object sender, EventArgs e)
        {
            System.Threading.Thread ts = new System.Threading.Thread(MI_Background);
            ts.Start();

            MI_PosicionarPainel();
            MI_CarregarTela();
        }

        private void MI_PosicionarPainel()
        {
            pnlPainelTexto.Location = new Point((this.Size.Width / 2) - (pnlPainelTexto.Size.Width / 2), pnlPainelTexto.Location.Y);
        }

        private void MI_CarregarTela()
        {
            MI_ExibirImagem();

            lblTextoGeral.Text = lblTextoGeral.Text.Replace("[NOME_CONSULTORA]", AP_NomeConsultora);
            lblTextoGeral.Text = lblTextoGeral.Text.Replace("[NrConsultora]", AP_NrConsultora);
            lblTextoGeral.Text = lblTextoGeral.Text.Replace("[DATA]", System.DateTime.Now.ToShortDateString());

            chkLiConcordo.ImageLocation = "ImagemGeral/ckOf.png";
            chkItemAcima.ImageLocation = "ImagemGeral/ckOf.png";

            lblConsultora.Text = AP_NomeConsultora;

            if (CSV_Global.getTipoJoia() == "FAIXA")
            {
                btoTrocarJoia.Visible = false;
            }
            else
            {
                btoTrocarJoia.Visible = true;
            }

        }

        private void MI_ExibirImagem()
        {
            PictureBox foto = (PictureBox)pnlPainelTexto.Controls["imgPremio"];

            if (!String.IsNullOrEmpty(AP_Premio))
            {
                new InserirFoto().MS_SetFoto(foto, AP_Premio);
            }
            else
            {
                CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
                imgPremio.ImageLocation = "ImagensPremios/" + MI_ObterDescricaoFoto(IAT_Visitante.MS_ObterItemPremio(AP_CdItemJoia).Rows[0]["dsTrocaLocal"].ToString());
            }
        }

        private string MI_ObterDescricaoFoto(string prCdJoia)
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            return IAT_Visitante.MS_ObterPremioNew(prCdJoia).Rows[0]["dsFoto"].ToString();
        }

        private void chkLiConcordo_Click(object sender, EventArgs e)
        {
            if (chkLiConcordo.ImageLocation == "ImagemGeral/ckOf.png")
            {
                chkLiConcordo.ImageLocation = "ImagemGeral/ckOn.png";
            }
            else
            {
                chkLiConcordo.ImageLocation = "ImagemGeral/ckOf.png";
            }
        }

        private void chkItemAcima_Click(object sender, EventArgs e)
        {
            if (chkItemAcima.ImageLocation == "ImagemGeral/ckOf.png")
            {
                chkItemAcima.ImageLocation = "ImagemGeral/ckOn.png";
            }
            else
            {
                chkItemAcima.ImageLocation = "ImagemGeral/ckOf.png";
            }
        }

        private void btoObservacoes_Click(object sender, EventArgs e)
        {
            Clt_ObservacoesNew Open = new Clt_ObservacoesNew();

            Open.AP_CodigoBarras = AP_CodigoBarras;
            Open.AP_NomeConsultora = AP_NomeConsultora;
            Open.AP_NrConsultora = AP_NrConsultora;
            Open.AP_Premio = AP_Premio;
            Open.AP_CdItemJoia = AP_CdItemJoia;

            Open.ShowDialog();
        }

        private bool MI_Consistencias()
        {
            bool boResult = true;

            if (chkLiConcordo.ImageLocation == "ImagemGeral/ckOf.png")
            {
                return boResult = false;
            }
            if (chkItemAcima.ImageLocation == "ImagemGeral/ckOf.png")
            {
                return boResult = false;
            }

            return boResult;
        }

        private void btoContinuar_Click(object sender, EventArgs e)
        {
            if (MI_Consistencias())
            {
                if (CSV_Global.getTipoJoia() != "FAIXA")
                {
                    CLT_LerCodigoProduto abrir = new CLT_LerCodigoProduto();
                    abrir.AP_NrConsultora = AP_NrConsultora;
                    abrir.AP_codJoia = AP_CdItemJoia;
                    abrir.AP_CancelamentoEntrega = false;
                    abrir.ShowDialog();
                    //this.Close();

                    if (abrir.CodigoValido)
                    {
                        CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
                        IAT_Visitante.MS_ConfirmarRetiradaPremioNew(AP_CdItemJoia, AP_Correio);
                        this.Close();
                    }
                }
                else
                {
                    CLT_LerCodigoProduto abrir = new CLT_LerCodigoProduto();
                    abrir.AP_NrConsultora = AP_NrConsultora;
                    abrir.AP_codJoia = AP_CdItemJoia;
                    abrir.AP_CancelamentoEntrega = false;
                    abrir.ShowDialog();

                    if (abrir.CodigoValido)
                    {
                        CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
                        IAT_Visitante.MS_ConfirmarRetiradaPremioNew(AP_CdItemJoia, AP_Correio);

                        string cdRoteiro = new DAO_Visitante().MS_ObterItemPremio(AP_CdItemJoia).Rows[0]["dsTrocaLocal"].ToString();
                        string dsRoteiro = new DAO_Visitante().MS_ObterRoteiroPorIdRoteiro(cdRoteiro).Rows[0]["dsReconhecimento"].ToString();

                        Clt_ImpressaoCartaoRealeza open = new Clt_ImpressaoCartaoRealeza();
                        open.AP_boResult = false;

                        while (open.AP_boResult == false)
                        {
                            MI_ImprimirCartaoRealeza(AP_CodigoBarras, dsRoteiro);
                            open.ShowDialog();

                            if (open.AP_boResult == false)
                            {
                                MessageBox.Show("Um novo arquivo sera enviado para a impressora", "Aten��o");
                            }
                        }

                        this.Close();
                    }
                }
            }
        }

        private void MI_ImprimirCartaoRealeza(string prCodigoBarras, string dsReconhecimento)
        {
            MI_ImprimirCredencial(Convert.ToInt64(prCodigoBarras), "", "", dsReconhecimento);
        }

        private void MI_ImprimirCredencial(Int64 itCodigo, string stProdutos, string stEvento, string dsReconhecimento)
        {
            bool boImprimirCodigo = true;
            CAT_ImprimirEtiqueta IAT_ImprimirEtiqueta = new CAT_ImprimirEtiqueta(itCodigo, boImprimirCodigo, stProdutos, "1", dsReconhecimento);
        }

        private void lblConsultora_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btoTrocarJoia_Click(object sender, EventArgs e)
        {
            MI_TrocarJoia();
        }

        private void MI_TrocarJoia()
        {
            CLT_SenhaJoia open = new CLT_SenhaJoia();
            open.ShowDialog();

            if (open.AP_Result)
            {
                Clt_TrocaDeJoia openTrocal = new Clt_TrocaDeJoia();
                openTrocal.AP_cdPremio = AP_CdItemJoia;
                openTrocal.AP_Premio = AP_Premio;
                openTrocal.AP_Reconhecimento = AP_CdItemJoia;
                openTrocal.ShowDialog();

                AP_Premio = openTrocal.AP_Premio;

                MI_CarregarTela();
            }
        }

    }
}

