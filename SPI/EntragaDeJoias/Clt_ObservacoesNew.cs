﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPI
{
    public partial class Clt_ObservacoesNew : Form
    {
        public string AP_CodigoBarras { get; set; }
        public string AP_NrConsultora { get; set; }
        public string AP_NomeConsultora { get; set; }
        public string AP_CdItemJoia { get; set; }
        public string AP_Premio { get; set; }

        public Clt_ObservacoesNew()
        {
            InitializeComponent();
        }

        private void Clt_ConfirmaEscolha_Load(object sender, EventArgs e)
        {
            MI_ObterObservacoes();
        }

        private void MI_ObterObservacoes()
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtReconhecimento = IAT_Visitante.MS_ObterReconhecimentosEspecifico(AP_CdItemJoia);
            edtObservacoes.Text = dtReconhecimento.Rows[0]["dsObservacoes"].ToString();
        }

        private void MI_SalvarObservacoes()
        {
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            IAT_Visitante.MS_SalvarObservacoes(AP_CdItemJoia, edtObservacoes.Text.Trim());
            this.Close();
        }

        private void btoNAO_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btoSIM_Click(object sender, EventArgs e)
        {
            MI_SalvarObservacoes();
            this.Close();
        }
    }
}
