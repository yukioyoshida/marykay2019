﻿using System;
using System.Data;
using System.Windows.Forms;

namespace SPI
{
    public partial class CLT_SenhaCancelamento : Form
    {
        public bool AP_Result { get; set; }

        public CLT_SenhaCancelamento()
        {
            InitializeComponent();
        }

        private void btnProsseguir_Click(object sender, EventArgs e)
        {
            MI_VerificarSenha(edtSenha.Text);
        }
        private void MI_VerificarSenha(string stSenha)
        {
            CSV_VariaveisGlobal Global = new CSV_VariaveisGlobal();
            CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
            DataTable dtSenha = IAT_Visitante.MS_ObterSenha(stSenha);

            if (dtSenha.Rows[0][0].ToString() == edtSenha.Text)
            {
                AP_Result = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Senha Inválida", "ATENÇÃO!");
                AP_Result = false;
                this.Close();
            }
        }
    }
}
