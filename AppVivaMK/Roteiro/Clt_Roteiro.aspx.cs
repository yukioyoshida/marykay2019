﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class credenciamento_Clt_Roteiro : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Session["oUsuario"] == null) || (Session["oSeminario"] == null))
        {
            Response.Redirect("~/autentication/Clt_Autentication.aspx", true);
        }

        if ((Request["onmRoteiro"] == null) || (Request["onmRoteiro"] == null))
        {
            Response.Redirect("~/autentication/Clt_Autentication.aspx", true);
        }

        if ((Request["oRoteiro"] == null) || (Request["oRoteiro"] == null))
        {
            Response.Redirect("~/autentication/Clt_Autentication.aspx", true);
        }

        if (!IsPostBack)
        {
            MI_CarregarColetor();
            MI_SetAtributoPadrao();
            MI_CarregarTela("");
        }
    }

    private void MI_CarregarColetor()
    {
        ddlColetor.Items.Clear();
        ddlColetor.Items.Add("SELECIONE");

        foreach (DataRow Row in new CAT_Relatorios().MS_ObterColetor(Session["oSeminario"].ToString()).Rows)
        {
            ddlColetor.Items.Add(Row["Coletor"].ToString());
        }
    }

    private void MI_SetAtributoPadrao()
    {
        this.Title = new Global().AP_Title;
        lblSeminario.Text = Session["oSeminario"].ToString();
        lblNomeRoteiro.Text = Request["onmRoteiro"].ToString();
    }

    private bool MI_Consistencias()
    {
        bool boResult = true;
        return boResult;
    }

    protected void btoFiltrar_Click(object sender, EventArgs e)
    {
        if (MI_Consistencias())
        {
            MI_CarregarTela(edtChave.Text.Trim());
        }
    }

    private void MI_CarregarTela(string prChaveBucas)
    {
        DataTable dtPessoas = new CAT_Relatorios().MS_ObterPessoasNoRoteiro(prChaveBucas, Session["oSeminario"].ToString(), Request["oRoteiro"].ToString(), ddlColetor.Text);

        gdwParticipantes.DataSource = dtPessoas;
        gdwParticipantes.DataBind();

        int itQuantidadeRegistros = 0;
        int ITQuantidadeRetirou = 0;
        int itQuantidadePresentesScaner = 0;

        foreach(GridViewRow Row in gdwParticipantes.Rows)
        {
            if ((Row.Cells[3].Text.ToString().ToUpper().Contains("CONVIDADOS")) || (("CONVIDADOS").Contains(Row.Cells[3].Text.ToString().ToUpper())))
            {
                Row.Cells[3].Text = "CONVIDADO";
            }
            else
            {
                Row.Cells[3].Text = "CONSULTORA";
            }

            if (Row.Cells[3].Text == "CONVIDADO")
            {
                Row.Visible = false;
                // PERMITE SOMENTE SE FOR OS ITENS DE CONVIDADO
                // 1 AREA DE ASSENTOS
                if ((lblNomeRoteiro.Text.ToString().ToUpper().Contains("AREA DE ASSENTO:")) || ("AREA DE ASSENTO:".Contains(lblNomeRoteiro.Text.ToString().ToUpper())))
                {
                    Row.Visible = true;
                }

                // 2 DEFINIDOS PELO CLIENTE
                if (
                        (lblNomeRoteiro.Text.ToString().Trim() == "Convidado") ||
                        (lblNomeRoteiro.Text.ToString().Trim() == "Atividade Especial Convidado") ||
                        (lblNomeRoteiro.Text.ToString().Trim() == "Coquetel da Realeza - Convidado") ||
                        (lblNomeRoteiro.Text.ToString().Trim() == "Cônjuge") ||
                        (lblNomeRoteiro.Text.ToString().Trim() == "Brunch com Alvaro Polanco")
                   )
                {
                    Row.Visible = true;
                }
            }
            else
            {
                Row.Visible = true;
                if (
                    (lblNomeRoteiro.Text.ToString().Trim() == "Convidado") ||
                    (lblNomeRoteiro.Text.ToString().Trim() == "Atividade Especial Convidado") ||
                    (lblNomeRoteiro.Text.ToString().Trim() == "Coquetel da Realeza - Convidado") ||
                    (lblNomeRoteiro.Text.ToString().Trim() == "Cônjuge")
                )
                {
                    Row.Visible = false;
                }
            }

            if (Row.Visible == true)
            {
                itQuantidadeRegistros++;

                if (Row.Cells[4].Text.ToString().ToUpper() == "S")
                    ITQuantidadeRetirou++;

                try
                {
                    Convert.ToDateTime(Row.Cells[5].Text.ToString().ToUpper());
                    itQuantidadePresentesScaner++;
                }
                catch
                { }
                    
            }

        }

        lblQuantidadeLocalizado.Text = "Registros encontrados: " + itQuantidadeRegistros.ToString();
        lblQuantidadeQueRetirou.Text = "Retirou a credencial: " + ITQuantidadeRetirou.ToString() + " | Não retirou a credencial: " + (itQuantidadeRegistros - ITQuantidadeRetirou).ToString();
        QuantidadePresentesNoScaner.Text = "Quantidade de presentes: " + itQuantidadePresentesScaner.ToString();
    }



    private void MI_CarregarAvancado(string prAvancado)
    {
        pnlSeparadoPorCorte.Visible = true;

        DataTable dtReconhecimentoAvancado = new CAT_Relatorios().MS_ObterPessoasNoRoteiroAvancado(prAvancado, Session["oSeminario"].ToString(), Request["oRoteiro"].ToString(), ddlColetor.Text.ToString());

        gdwDetalhado.DataSource = dtReconhecimentoAvancado;
        gdwDetalhado.DataBind();

        foreach (GridViewRow Row in gdwDetalhado.Rows)
        {
            if ((Row.Cells[6].Text.ToString().ToUpper().Contains("CONVIDADOS")) || (("CONVIDADOS").Contains(Row.Cells[6].Text.ToString().ToUpper())))
            {
                Row.Cells[6].Text = "CONVIDADO";
            }
            else
            {
                Row.Cells[6].Text = "CONSULTORA";
            }

            if (Row.Cells[6].Text == "CONVIDADO")
            {
                Row.Visible = false;
                // PERMITE SOMENTE SE FOR OS ITENS DE CONVIDADO
                // 1 AREA DE ASSENTOS
                if ((lblNomeRoteiro.Text.ToString().ToUpper().Contains("AREA DE ASSENTO:")) || ("AREA DE ASSENTO:".Contains(lblNomeRoteiro.Text.ToString().ToUpper())))
                {
                    Row.Visible = true;
                }

                // 2 DEFINIDOS PELO CLIENTE
                if (
                        (lblNomeRoteiro.Text.ToString().Trim() == "Convidado") ||
                        (lblNomeRoteiro.Text.ToString().Trim() == "Atividade Especial Convidado") ||
                        (lblNomeRoteiro.Text.ToString().Trim() == "Coquetel da Realeza - Convidado") ||
                        (lblNomeRoteiro.Text.ToString().Trim() == "Cônjuge") ||
                        (lblNomeRoteiro.Text.ToString().Trim() == "Brunch com Alvaro Polanco")
                   )
                {
                    Row.Visible = true;
                }
            }
            else
            {
                Row.Visible = true;
                if (
                    (lblNomeRoteiro.Text.ToString().Trim() == "Convidado") ||
                    (lblNomeRoteiro.Text.ToString().Trim() == "Atividade Especial Convidado") ||
                    (lblNomeRoteiro.Text.ToString().Trim() == "Coquetel da Realeza - Convidado") ||
                    (lblNomeRoteiro.Text.ToString().Trim() == "Cônjuge")
                )
                {
                    Row.Visible = false;
                }
            }

            if (Row.Cells[5].Text.ToString() != "0")
                Row.Cells[5].Text = "S";
            else
                Row.Cells[5].Text = "N";

        }
    }

    protected void btoMisGoGive_Click(object sender, EventArgs e)
    {
        string prRoteiro = "'3800','3801','3802','3803','3804','3805','3806','3807','3808','3809','3810','3811'";
        MI_CarregarAvancado(prRoteiro);
    }



    protected void btoTop20Unidades_Click(object sender, EventArgs e)
    {
        string prRoteiro = "'3823','3824','3825'";
        MI_CarregarAvancado(prRoteiro);
    }

    protected void btoTop20Iniciacao_Click(object sender, EventArgs e)
    {
        string prRoteiro = "'3826'";
        MI_CarregarAvancado(prRoteiro);
    }

    protected void btoTop20VPConsultoras_Click(object sender, EventArgs e)
    {
        string prRoteiro = "'3827'";
        MI_CarregarAvancado(prRoteiro);
        
    }

    protected void btoTop20VPDiretoras_Click(object sender, EventArgs e)
    {
        string prRoteiro = "'3828'";
        MI_CarregarAvancado(prRoteiro);
    }

    protected void btoTop3_Click(object sender, EventArgs e)
    {
        string prRoteiro = "'3829','3830','3831','3832'";
        MI_CarregarAvancado(prRoteiro);
    }

    protected void ddlColetor_SelectedIndexChanged(object sender, EventArgs e)
    {
        MI_CarregarAvancado("0");
        MI_CarregarTela("");
    }
}