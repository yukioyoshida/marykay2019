﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Clt_Roteiro.aspx.cs" Inherits="credenciamento_Clt_Roteiro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <link href="../layout/Style.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="container-aplication">

            <div style="text-align:right;">
                <img src="../image/exit.png" onclick="MI_Return();" style="cursor:pointer; width:30px; margin-top:5px; margin-right:10px;" />
            </div>

            <div style="width:100%; margin-top:-40px; text-align:center;" > 
                <p style="margin-left:5px;padding-top:10px; font-weight:bold;">Seminário</p>
                <p style="margin-left:5px;margin-top:-10px;"><asp:Label ID="lblSeminario" runat="server" Text=""></asp:Label></p>
            </div>
            
            <div class="LinhaDivisoria"></div>
            
            <p><asp:Label ID="lblNomeRoteiro" runat="server" Text=""></asp:Label></p>

            <asp:Panel runat="server" ID="pnlRelatorio" CssClass="pnlMenuRelatorio"> 
                
                <p style="font-size:8pt;margin-bottom:0px;"><span style="text-align:left; font-size:8pt;">
                    <asp:Label ID="lblQuantidadeLocalizado0" runat="server" Text=""></asp:Label>
                    </span><span style="text-align:left; font-size:8pt;">
                    <asp:Label ID="lblQuantidadeQueRetirou0" runat="server" Text=""></asp:Label>
                    </span>
                </p>

                <span style="text-align:left; font-size:8pt;">
                <asp:Label ID="lblQuantidadeLocalizado" runat="server" Text=""></asp:Label><br />
                <asp:Label ID="lblQuantidadeQueRetirou" runat="server" Text=""></asp:Label><br />
                </span>

                <p style="font-size:8pt;margin-bottom:0px;">
                    DEFINA O SCANNER</p>
                <asp:DropDownList ID="ddlColetor" runat="server" Width="300px"  CssClass="DropDown" AutoPostBack="True" OnSelectedIndexChanged="ddlColetor_SelectedIndexChanged"></asp:DropDownList>

                <p style="font-size:8pt;margin-bottom:0px;">Filtros avançados</p>

                <asp:Button runat="server" ID="btoMisGoGive" Text="Miss Go Give" Width="300px" CssClass="Button" OnClick="btoMisGoGive_Click" /><br /><br />

                <asp:Button runat="server" ID="btoTop20Unidades" Text="TOP 20 Corte das Unidades" Width="300px" CssClass="Button" OnClick="btoTop20Unidades_Click" /><br /><br />
                <asp:Button runat="server" ID="btoTop20Iniciacao" Text="TOP 20 Corte de Iniciação" Width="300px" CssClass="Button" OnClick="btoTop20Iniciacao_Click" /><br /><br />
                <asp:Button runat="server" ID="btoTop20VPConsultoras" Text="TOP 20 Corte VP Consultoras" Width="300px" CssClass="Button" OnClick="btoTop20VPConsultoras_Click" /><br /><br />
                <asp:Button runat="server" ID="btoTop20VPDiretoras" Text="TOP 20 Corte VP Diretoras" Width="300px" CssClass="Button" OnClick="btoTop20VPDiretoras_Click" /><br /><br />

                <asp:Button runat="server" ID="btoTop3" Text="TOP 3" Width="300px" CssClass="Button" OnClick="btoTop3_Click" /><br /><br />

                <br />
                <br />
                <asp:Panel ID="pnlSeparadoPorCorte" runat="server" Width="100%" Visible="false">

                    <span style="text-align:left; font-size:8pt;"><asp:Label ID="lblPorSelecao" Text="" runat="server"></asp:Label></span>
                    <asp:GridView runat="server" ID="gdwDetalhado" AutoGenerateColumns="False" Width="100%" style="font-size: xx-small">
                        <Columns>
                            <asp:BoundField DataField="Consultora" HeaderText="Consultora" >
                            <ItemStyle Width="50px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Tipo" HeaderText="Tipo" >
                            <FooterStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="300px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Nome" HeaderText="Nome" FooterText="Nome" Visible="true" >
                                <ItemStyle Width="70px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Credenciamento" HeaderText="Credenciamento" FooterText="Credenciamento" Visible="true" >
                                <ItemStyle Width="50px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Scanner" HeaderText="Scanner" FooterText="Scanner" Visible="true" >
                                <ItemStyle Width="50px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Faixa" HeaderText="Faixa" FooterText="Faixa" Visible="true" >
                                <ItemStyle Width="50px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TipodeInscricao" HeaderText="TipodeInscricao" FooterText="TipodeInscricao" Visible="true" >
                                <ItemStyle Width="50px" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    <hr />
                </asp:Panel>

                <br />
                <br />
                                <p style="font-size:8pt;margin-bottom:0px;">cod.consultora/cpf/nome</p>
                
                <div style="text-align:center;">
                    <asp:TextBox runat="server" ID="edtChave" Text="" Width="100%"></asp:TextBox>
                </div>
                <div style="margin-top:5px;">
                    <asp:Button runat="server" ID="btoFiltrar" Text="Filtrar" OnClick="btoFiltrar_Click" Width="100%" CssClass="Button" />
                </div>
                <br />
                <br />
                <p style="text-align:right;"><asp:Label ID="QuantidadePresentesNoScaner" Text="" runat="server"></asp:Label></p>
                <asp:GridView ID="gdwParticipantes" runat="server" AutoGenerateColumns="False" style="font-size: xx-small" Width="100%">
                    <Columns>
                        <asp:BoundField DataField="Consultora" HeaderText="Consultora">
                        <ItemStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CPF" HeaderText="CPF">
                        <ItemStyle Width="60px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Nome" HeaderText="Nome">
                        <FooterStyle HorizontalAlign="Left" />
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="300px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TipodeInscricao" FooterText="Inscricao" HeaderText="Inscricao" Visible="true">
                        <ItemStyle Width="70px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Retirou" FooterText="Retirou" HeaderText="Retirou" Visible="true">
                        <ItemStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Scanner" FooterText="Scanner" HeaderText="Scanner" Visible="true">
                        <ItemStyle Width="50px" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                <br />
                
            </asp:Panel>

        </div>
    </form>
</body>
</html>

<script>
    function MI_Return() {
        window.location = '../menu/Clt_Menu.aspx';
    }

</script>
