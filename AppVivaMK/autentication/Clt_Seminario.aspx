﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Clt_Seminario.aspx.cs" Inherits="autentication_Clt_Seminario" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <link href="../layout/Style.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="container-aplication">

            <div style="width:100%; text-align:center;" > 
                <p style="margin-left:5px;padding-top:10px; font-weight:bold;">Olá</p>
                <p style="margin-left:5px;margin-top:-10px;"><asp:Label ID="lblNomeUsuario" runat="server" Text="Paulo Rodrigues"></asp:Label></p>
            </div>
            
            <div class="LinhaDivisoria"></div>
            
            <p>Selecione o seminário desejado</p>

            <asp:DropDownList ID="ddlSeminario" runat="server" CssClass="DropDown">
                <asp:ListItem Value="0" Text="SELECIONE"></asp:ListItem>
                <asp:ListItem Value="AMARELO" Text="AMARELO"></asp:ListItem>
                <asp:ListItem Value="AZUL" Text="AZUL"></asp:ListItem>
                <asp:ListItem Value="VERDE" Text="VERDE"></asp:ListItem>
            </asp:DropDownList>

            <p><asp:Button ID="btoContinuar" runat="server" Text="CONTINUAR" CssClass="Button" OnClick="btoContinuar_Click"></asp:Button></p>

        </div>
    </form>
</body>
</html>
