﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class autentication_Clt_Seminario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["oUsuario"] == null)
        {
            Response.Redirect("Clt_Autentication.aspx", false);
        }
        MI_SetAtributoPadrao();
        MI_CarregarTela();
    }

    private void MI_SetAtributoPadrao()
    {
        this.Title = new Global().AP_Title;
    }

    private void MI_CarregarTela()
    {
        lblNomeUsuario.Text = Session["oUsuario"].ToString();
    }

    private bool MI_Consistencias()
    {
        bool boResult = true;
        if (ddlSeminario.SelectedValue.ToString()=="0")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Selecione o seminário!')", true);
            return boResult = false;
        }

        return boResult;
    }

    protected void btoContinuar_Click(object sender, EventArgs e)
    {
        if (MI_Consistencias())
        {
            Session.Add("oSeminario", ddlSeminario.SelectedValue.ToString());
            Response.Redirect("~/menu/Clt_Menu.aspx", false);
        }
    }
}