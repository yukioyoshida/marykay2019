﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class autentication_Clt_Autentication : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MI_LimparSession();
            MI_SetAtributoPadrao();
        }
    }

    private void MI_LimparSession()
    {
        Session.RemoveAll();
    }

    private void MI_SetAtributoPadrao()
    {
        this.Title = new Global().AP_Title;
    }

    private bool MI_Consistencias()
    {
        bool boResult = true;

        if (edtUsuario.Text.Trim() == String.Empty)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Informe seu usuário!')", true);
            return boResult = false;
        }

        if (edtUsuario.Text.Trim() == String.Empty)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Informe sua senha de segurança!')", true);
            return boResult = false;
        }

        DataTable dtRegistro = new CAT_Relatorios().MS_RelizarLogin(edtUsuario.Text.Trim(), edtSenha.Text.Trim());
        if (dtRegistro.Rows.Count == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Usuário ou senhá inválido!')", true);
            edtUsuario.Text = string.Empty;
            edtSenha.Text = string.Empty;
            return boResult = false;
        }
        else
        {
            Session.Add("oUsuario", dtRegistro.Rows[0]["NomeUsuario"].ToString());
        }

        return boResult;

    }

    protected void btoLogar_Click(object sender, EventArgs e)
    {
        if (MI_Consistencias())
        {
            Response.Redirect("Clt_Seminario.aspx", false);
        }
    }
}