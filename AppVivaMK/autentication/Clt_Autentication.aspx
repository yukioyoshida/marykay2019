﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Clt_Autentication.aspx.cs" Inherits="autentication_Clt_Autentication" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <link href="../layout/Style.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        
        <div id="container-aplication">

            <img src="../image/logo_vivamk2018_final.png" style="max-width:80%;" />

            <div class="LinhaDivisoria" style="margin-top:20px;"></div>

            <p>Login</p>

            <div id="container-controles" style="width:80%; margin:auto;">
                
                <div style="text-align:left;">
                    <span class="rotulo">Usuário:</span>
                </div>
                <div>
                    <asp:TextBox ID="edtUsuario" runat="server" Text="" Width="100%"></asp:TextBox>
                </div>

                <div style="text-align:left;margin-top:10px;">
                    <span class="rotulo">Senha:</span>
                </div>
                <div>
                    <asp:TextBox ID="edtSenha" runat="server" type="password" Text="" Width="100%"></asp:TextBox>
                </div>

                <div style="margin-top:20px;">
                    <asp:Button ID="btoLogar" runat="server" Text="ENTRAR" CssClass="Button" OnClick="btoLogar_Click"></asp:Button>
                </div>

            </div>

        </div>

        <div id="container-rodape" style="display:none;">

            © 2018 Interação | Produtos para atendimento ao público de eventos

        </div>

    </form>
</body>
</html>
