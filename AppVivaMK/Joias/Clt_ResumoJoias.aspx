﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Clt_ResumoJoias.aspx.cs" Inherits="credenciamento_Clt_ResumoJoias" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <link href="../layout/Style.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
<div id="container-aplication">

            <div style="text-align:right;">
                <img src="../image/exit.png" onclick="MI_Return();" style="cursor:pointer; width:30px; margin-top:5px; margin-right:10px;" />
            </div>

            <div style="width:100%; margin-top:-40px; text-align:center;" > 
                <p style="margin-left:5px;padding-top:10px; font-weight:bold;">Seminário</p>
                <p style="margin-left:5px;margin-top:-10px;"><asp:Label ID="lblSeminario" runat="server" Text=""></asp:Label></p>
            </div>
            
            <div class="LinhaDivisoria"></div>
            
            <p>Joias</p>

            <asp:Panel runat="server" ID="pnlRelatorio" CssClass="pnlMenuRelatorio">
                <asp:GridView ID="gdwCredenciamento" runat="server" style="font-size: x-small" Width="100%" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField DataField="TIPO" HeaderText="TIPO" />
                        <asp:BoundField DataField="INSCRITAS" HeaderText="INSCRITAS" />
                        <asp:BoundField DataField="RETIROU" HeaderText="RETIROU" />
                    </Columns>
                </asp:GridView>
                <br />
                <br />

                <br />
                <p>Detalhes</p>

                <p>Selecione o tipo de entrega</p>
                <p>
                    <asp:Button runat="server" Text="Premio" ID="btoPremio" CssClass="Button" Width="100px" OnClick="btoPremio_Click" />
                    &nbsp;&nbsp;
                    <asp:Button runat="server" Text="Numeral" ID="btoNumeral" CssClass="Button" Width="100px" OnClick="btoNumeral_Click" />
                    <br />
                    <br />
                    <asp:Button runat="server" Text="Charm" ID="btoCharm" CssClass="Button" Width="100px" OnClick="btoCharm_Click" />
                    &nbsp;&nbsp;
                    <asp:Button runat="server" Text="Faixa" ID="btoFaixa" CssClass="Button" Width="100px" OnClick="btoFaixa_Click" />
                    <br />
                    <br />
                </p>

                <asp:GridView ID="gdwJoias" runat="server" style="font-size: x-small" Width="100%" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField DataField="Nome" HeaderText="Nome" />
                        <asp:BoundField DataField="Inscritas" HeaderText="Inscritas" />
                        <asp:BoundField DataField="Retirou" HeaderText="Retirou" />
                    </Columns>
                </asp:GridView>
                     <br />
            </asp:Panel>

        </div>
    </form>
</body>
</html>
<script>
    function MI_Return() {
        window.location = '../menu/Clt_Menu.aspx';
    }

</script>