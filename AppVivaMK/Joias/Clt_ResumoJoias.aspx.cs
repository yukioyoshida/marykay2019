﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class credenciamento_Clt_ResumoJoias : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Session["oUsuario"] == null) || (Session["oSeminario"] == null))
        {
            Response.Redirect("~/autentication/Clt_Autentication.aspx", true);
        }
        if (!IsPostBack)
        {
            MI_SetAtributoPadrao();
            MI_CarregarGrid();
        }
    }

    private void MI_SetAtributoPadrao()
    {
        this.Title = new Global().AP_Title;
        lblSeminario.Text = Session["oSeminario"].ToString();
    }

    private void MI_CarregarGrid()
    {
        DataTable dtCredenciamento = new DataTable();
        dtCredenciamento.Columns.Add("TIPO", typeof(string));
        dtCredenciamento.Columns.Add("INSCRITAS", typeof(string));
        dtCredenciamento.Columns.Add("RETIROU", typeof(string));

        gdwCredenciamento.DataSource = new CAT_Relatorios().MS_ObterQuantidadesJoia(Session["oSeminario"].ToString());
        gdwCredenciamento.DataBind();

    }

    private void MI_CarregarDetalhes(string prTipoPremio)
    {
        gdwJoias.DataSource = new CAT_Relatorios().MS_ObterQuantidadePorTipoPremio(prTipoPremio, Session["oSeminario"].ToString());
        gdwJoias.DataBind();
    }

    protected void btoPremio_Click(object sender, EventArgs e)
    {
        MI_CarregarDetalhes("Premio");
    }

    protected void btoNumeral_Click(object sender, EventArgs e)
    {
        MI_CarregarDetalhes("Numeral");
    }

    protected void btoCharm_Click(object sender, EventArgs e)
    {
        MI_CarregarDetalhes("Charm");
    }

    protected void btoFaixa_Click(object sender, EventArgs e)
    {
        MI_CarregarDetalhes("Faixa");
    }
}