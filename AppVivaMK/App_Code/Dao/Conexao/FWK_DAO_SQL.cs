using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Interacao
{
    public class FWK_DAO_SQL
    {
        private string AP_SqlResult = string.Empty;

        private bool AP_EOF = true;
        public SqlDataAdapter adapter = new SqlDataAdapter();
        public SqlDataReader AP_DataReader;
        public string ComandoSQL;
        public List<FWK_DAO_Parametro> Parametros = new List<FWK_DAO_Parametro>();

        public SqlConnection AP_Connection = new FactoryConnection().getConnection();

        private void MI_AbrirConexao()
        {
            AP_Connection.Open();
        }

        protected void MI_FecharConexao()
        {
            AP_Connection.Close();
        }

        protected void MI_SetSql(String prstSql)
        {
            ComandoSQL = prstSql;
            Parametros.Clear();
        }

        protected void MI_AddParameters(String prstName, Object probValue)
        {
            FWK_DAO_Parametro Parm = new FWK_DAO_Parametro();
            Parm.setNome(prstName);
            Parm.setValor(probValue);
            Parametros.Add(Parm);
        }

        protected void MI_ExecuteNonQuery()
        {
            AP_Connection.Open();
            SqlCommand AP_Command = new SqlCommand(ComandoSQL, AP_Connection);

            foreach (FWK_DAO_Parametro i in Parametros)
            {
                AP_Command.Parameters.AddWithValue(i.getNome(), i.getValor());
            }

            try
            {
                AP_Command.Transaction = AP_Connection.BeginTransaction(IsolationLevel.Serializable);
                AP_Command.ExecuteNonQuery();
                AP_Command.Transaction.Commit();
                AP_Connection.Close();
            }
            catch (Exception e)
            {
                AP_Command.Transaction.Rollback();
                throw e;
            }
        }

        protected void MI_ExecuteQuery()
        {
            AP_Connection.Open();
            SqlCommand AP_Command = new SqlCommand(ComandoSQL, AP_Connection);

            foreach (FWK_DAO_Parametro i in Parametros)
            {
                AP_Command.Parameters.AddWithValue(i.getNome(), i.getValor());
            }

            try
            {
                AP_DataReader = AP_Command.ExecuteReader(CommandBehavior.SingleResult);
                adapter.SelectCommand = AP_Command;
                MI_ProximoRegistro();
            }
            catch (Exception e)
            {
                AP_DataReader.Close();
            }
        }

        protected DataTable MI_ExecuteDataSet()
        {
            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;

            AP_Connection.Open();

            SqlCommand AP_Command = new SqlCommand(ComandoSQL, AP_Connection);

            foreach (FWK_DAO_Parametro i in Parametros)
            {
                AP_Command.Parameters.AddWithValue(i.getNome(), i.getValor());
            }

            try
            {
                adapter.SelectCommand = AP_Command;
                adapter.Fill(table);
                AP_Connection.Close();
            }
            catch (Exception e)
            {
                AP_DataReader.Close();
            }

            return table;
        }

        protected DataTable MI_ExecuteProcedimento(string prNameProcedimento)
        {
            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;

            AP_Connection.Open();
            SqlCommand AP_Command = new SqlCommand();
            AP_Command.Connection = AP_Connection;
            AP_Command.CommandText = prNameProcedimento;
            AP_Command.CommandType = CommandType.StoredProcedure;

            foreach (FWK_DAO_Parametro i in Parametros)
            {
                AP_Command.Parameters.AddWithValue(i.getNome(), i.getValor());
            }

            try
            {
                adapter.SelectCommand = AP_Command;
                adapter.Fill(table);
                AP_Connection.Close();
            }
            catch (Exception e)
            {
                AP_DataReader.Close();
            }

            return table;
        }

        protected void MI_ProximoRegistro()
        {
            AP_EOF = !AP_DataReader.Read();
        }

        protected bool MI_EOF()
        {
            return AP_EOF;
        }
    }
}



