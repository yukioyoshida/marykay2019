﻿using System.Data;

namespace Interacao
{
    public class FWK_DAO_Relatorios : FWK_DAO_SQL
    {
        public FWK_DAO_Relatorios()
        {  
        }

        public DataTable Rel_PorParametro(string campo)
        {
            string query = "SELECT                                                                                                                         " +
                           "     CASE v." + campo + " WHEN '' THEN 'CONSULTORA' ELSE v." + campo + " END AS Campo,                                                                                                 " +
                           "     Presentes = (SELECT COUNT(*) FROM tblVisitante v2 WHERE v2.PresenteEvento = 'S' AND v2." + campo + " = v." + campo + "),  " +
                           "     NPresentes = (SELECT COUNT(*) FROM tblVisitante v2 WHERE v2.PresenteEvento = 'N' AND v2." + campo + " = v." + campo + "), " +
                           "     Total = ''                                                                                                                " +
                           " FROM                                                                                                                          " +
                           "     tblVisitante v                                                                                                            " +
                           " GROUP BY                                                                                                                      " +
                           "     v." + campo;

            MI_SetSql(query);

            return MI_ExecuteDataSet();
        }

        public DataTable Rel_PorProduto(string produto)
        {
            string query = "SELECT                                                                                                                                      " +
                           "    Campo = p.dsProduto,                                                                                                                    " +
                           "    Presentes = (SELECT COUNT(DISTINCT(vis.ObjRef)) FROM tblitenspedido item INNER JOIN tblpedido ped ON item.cdPedido = ped.cdPedido       " +
                           "              INNER JOIN tblvisitante vis ON ped.cdCliente = vis.ObjRef                                                                     " +
                           "              WHERE ped.dsSituacao = 'PAGO' AND vis.PresenteEvento = 'S' AND item.cdProduto = p.cdProduto),                                 " +
                           "    NPresentes = (SELECT COUNT(DISTINCT(vis.ObjRef)) FROM tblitenspedido item INNER JOIN tblpedido ped ON item.cdPedido = ped.cdPedido      " +
                           "              INNER JOIN tblvisitante vis ON ped.cdCliente = vis.ObjRef                                                                     " +
                           "              WHERE ped.dsSituacao = 'PAGO' AND vis.PresenteEvento = 'N' AND item.cdProduto = p.cdProduto),                                 " +
                           "    Total = '',                                                                                                                             " +
                           "    p.cdProduto                                                                                                                             " +
                           " FROM                                                                                                                                       " +
                           "    tblProduto p                                                                                                                            " +
                           " WHERE                                                                                                                                      " +
                           "    Cancelado is null AND p.VinculoRelatorio = " + produto;

            MI_SetSql(query);

            return MI_ExecuteDataSet();
        }

        public DataTable Rel_NivelCarreira()
        {
            string query = "WITH NivelCarreira (Nivel) " +
                            "AS  " +
                            "( " +
                            "    SELECT DISTINCT(Res4) FROM tblvisitante " +
                            ") " +
                            "SELECT " +
                            "	IIF (Nivel = '', 'SEM INFORMACAO', Nivel) AS Campo, " +
                            "	Presentes = (SELECT COUNT(*) FROM tblvisitante WHERE tblvisitante.PresenteEvento = 'S' AND tblvisitante.Res4 = NivelCarreira.Nivel), " +
                            "	NPresentes = (SELECT COUNT(*) FROM tblvisitante WHERE tblvisitante.PresenteEvento = 'N' AND tblvisitante.Res4 = NivelCarreira.Nivel), " +
                            "	Total = '', " +
                            "	cdProduto = '' " +
                            "FROM  " +
                            "	NivelCarreira " +
                            "--WHERE " +
                            "	--LEN(Nivel)>0 ";

            MI_SetSql(query);

            return MI_ExecuteDataSet();
        }


        public DataTable Rel_Joias()
        {
            string query = " SELECT                                                                                                                       " +
                            "    Campo = l.Descricao,                                                                                                     " +
                            "    Retirados = (SELECT COUNT(*) FROM tblPremioConsultora_Item p WHERE p.dsTrocaLocal = l.cdPremio AND p.dsRetirado = 'S'),  " +
                            "    NRetirados = (SELECT COUNT(*) FROM tblPremioConsultora_Item p WHERE p.dsTrocaLocal = l.cdPremio AND p.dsRetirado = 'N'), " +
                            "    Total = '' ,                                                                                                             " +
                            "    l.cdPremio                                                                                                               " +
                            " FROM                                                                                                                        " +
                            "    tbllistapremiacoes l                                                                                                     " +
                            " WHERE                                                                                                                       " +
                            "    l.dsTipo = 'JOIA'";

            MI_SetSql(query);

            return MI_ExecuteDataSet();
        }

        public DataTable Rel_JoiasEntrega()
        {
            string query = " SELECT"+
                           "    Campo = l.Descricao,                                                                                                                                             " +
                           "    Preevento = (SELECT COUNT(*) FROM tblPremioConsultora_Item p WHERE p.dsEscolhaPre = l.cdPremio),                                                                 " +
                           "    Evento = (SELECT COUNT(*) FROM tblPremioConsultora_Item p WHERE p.dsTrocaLocal = l.cdPremio AND p.dsEscolhaPre = ''),                                            " +
                           "    Entregue = (SELECT COUNT(*) FROM tblPremioConsultora_Item p WHERE p.dsTrocaLocal = l.cdPremio AND p.dsRetirado = 'S'),                                           " +
                           "    Trocas = (SELECT COUNT(*) FROM tblPremioConsultora_Item p WHERE p.dsEscolhaPre <> '' AND p.dsTrocaLocal = l.cdPremio AND p.dsEscolhaPre<> p.dsTrocaLocal),       " +
	                       "    Total = '',                                                                                                                                                      " +
                           "    l.cdPremio                                                                                                                                                       " +
                           " FROM                                                                                                                                                                " +
                           "    tbllistapremiacoes l                                                                                                                                             " +
                           " WHERE                                                                                                                                                               " +
                           "    l.dsTipo = 'JOIA'";

            MI_SetSql(query);

            return MI_ExecuteDataSet();
        }

        public DataTable Rel_JoiasTroca()
        {
            string query = " SELECT                                                                                                                                                              " +
                           "    Campo = l.Descricao,                                                                                                                                             " +
                           "    EventoTroca = (SELECT COUNT(*) FROM tblPremioConsultora_Item p WHERE p.dsEscolhaPre <> '' AND p.dsTrocaLocal = l.cdPremio AND p.dsEscolhaPre<> p.dsTrocaLocal),  " +
                           "    Total = '',                                                                                                                                                      " +
                           "    l.cdPremio                                                                                                                                                       " +
                           " FROM                                                                                                                                                                " +
                           "    tbllistapremiacoes l                                                                                                                                             " +
                           " WHERE                                                                                                                                                               " +
                           "    l.dsTipo = 'JOIA'";

            MI_SetSql(query);

            return MI_ExecuteDataSet();
        }

        public DataTable Rel_JoiasEstoque()
        {
            string query = " SELECT" +
                           "    Campo = l.Descricao,                                                                                                                                " +
                           "    Preevento = (SELECT COUNT(*) FROM tblPremioConsultora_Item p WHERE p.dsEscolhaPre = l.cdPremio),                                                    " +
                           "    Estoque = CAST(l.Estoque AS INT) - (SELECT COUNT(*) FROM tblPremioConsultora_Item p WHERE p.dsTrocaLocal = l.cdPremio AND p.dsRetirado = 'S'),      " +
                           "    Entregue = (SELECT COUNT(*) FROM tblPremioConsultora_Item p WHERE p.dsTrocaLocal = l.cdPremio AND p.dsRetirado = 'S'),                              " +
                           "    Total = '',                                                                                                                                         " +
                           "    l.cdPremio                                                                                                                                          " +
                           " FROM                                                                                                                                                   " +
                           "    tbllistapremiacoes l                                                                                                                                " +
                           " WHERE                                                                                                                                                  " +
                           "    l.dsTipo = 'JOIA'";

            MI_SetSql(query);

            return MI_ExecuteDataSet();
        }

        public DataTable Rel_Coletor(string coletor)
        {
            string query = "SELECT   " +
                           "	col.Coletor,  " +
                           "	convert(varchar(10),col.dataEntrada,103) + ' ' + col.HoraEntrada as DataEntrada,  " +
                           "	vis.CodigoBarras,         " +
                           "	vis.Res1 as CodigoConsultoria,  " +
                           "	vis.NomeCompleto   " +
                           "FROM tblcoletorFeira col  " +
                           "INNER JOIN tblvisitante vis on vis.CodigoBarras = col.codigo   " +
                           "where EntradaConcedida = 'S' AND Coletor = '" + coletor + "'  " +
                           "order by col.coletor, col.DataEntrada";

            MI_SetSql(query);

            return MI_ExecuteDataSet();
        }

        

        public DataTable Rel_ColetorDistinct()
        {
            string query = "SELECT 'SELECIONE' AS Coletor " + 
                           "UNION ALL  " + 
                           "SELECT DISTINCT col.Coletor " + 
                           "FROM tblcoletorFeira col  ";

            MI_SetSql(query);

            return MI_ExecuteDataSet();
        }

        public DataTable Expo1(string Res4)
        {
            MI_SetSql("select " + 
                      "@Res4 as NivelCarreira, " + 
                      "CodigoBarras, " + 
                      "Res1 as CodigoConsultoria, " + 
                      "NomeCompleto, " + 
                      "CPF, " + 
                      "Res4 as NivelCarreira, " + 
                      "EmailPessoal as Email, " + 
                      "NumeroTelefonePessoal as Telefone, " + 
                      "PresenteEvento  " + 
                      "from tblvisitante  " + 
                      "where Res4 = @Res4");

            MI_AddParameters("@Res4", Res4);
            return MI_ExecuteDataSet();
        }

        public DataTable Expo2(string Res5)
        {
            if (Res5 == "CONSULTORA")
            {
                Res5 = string.Empty;
            }

            MI_SetSql("select " +
                      "Pergunta15 as TipoInscricao, " +
                      "CodigoBarras, " +
                      "Res1 as CodigoConsultoria, " +
                      "NomeCompleto, " +
                      "CPF, " +
                      "Res4 as NivelCarreira, " +
                      "EmailPessoal as Email, " +
                      "NumeroTelefonePessoal as Telefone, " + 
                      "PresenteEvento  " +
                      "from tblvisitante  " +
                      "where Res5 = @Res5");

            MI_AddParameters("@Res5", Res5);
            return MI_ExecuteDataSet();
        }
        public DataTable Expo3(string CategoriaRegistro)
        {
            if (CategoriaRegistro.ToUpper().Contains("CINZA"))
                CategoriaRegistro = "CINZA";

            if (CategoriaRegistro.ToUpper().Contains("ROXO"))
                CategoriaRegistro = "ROXO";

            if (CategoriaRegistro.ToUpper().Contains("LARANJA"))
                CategoriaRegistro = "LARANJA";

            if (CategoriaRegistro.ToUpper().Contains("GOLD"))
                CategoriaRegistro = "GOLD";


            MI_SetSql("select  " +
                      "@CategoriaRegistro as AreaAssento, " +
                      "CodigoBarras, " +
                      "Res1 as CodigoConsultoria, " +
                      "NomeCompleto, " +
                      "CPF, " +
                      "Res4 as NivelCarreira, " +
                      "EmailPessoal as Email, " +
                      "NumeroTelefonePessoal as Telefone, " + 
                      "PresenteEvento  " +
                      "from tblvisitante  " +
                      "where CategoriaRegistro = @CategoriaRegistro");

            MI_AddParameters("@CategoriaRegistro", CategoriaRegistro);
            return MI_ExecuteDataSet();
        }

        public DataTable Expo4(string Res2)
        {
            MI_SetSql("select " +
                      "@Res2 as AreaNacional, " +
                      "CodigoBarras, " +
                      "Res1 as CodigoConsultoria, " +
                      "NomeCompleto, " +
                      "CPF, " +
                      "Res4 as NivelCarreira, " +
                      "EmailPessoal as Email, " +
                      "NumeroTelefonePessoal as Telefone, " + 
                      "PresenteEvento  " +
                      "from tblvisitante  " +
                      "where Res2 = @Res2");

            MI_AddParameters("@Res2", Res2);
            return MI_ExecuteDataSet();
        }
        public DataTable ExpoUnico(string cdProduto, string Categoria)
        {
            MI_SetSql("select " +
                      "      @Categoria as Categoria,  " +
                      "      CodigoBarras, " +
                      "      Res1 as CodigoConsultoria, " +
                      "      NomeCompleto, " +
                      "      CPF, " +
                      "      Res4 as NivelCarreira, " +
                      "      EmailPessoal as Email, " +
                      "      NumeroTelefonePessoal as Telefone, " + 
                      "      PresenteEvento  " +
                      "  from tblvisitante vis  " +
                      "  where exists(select * from tblpedido ped " +
                      "               inner join tblitenspedido item on ped.cdPedido = item.cdPedido " +
                      "               where ped.dsSituacao = 'PAGO' " +
                      "               and ped.cdCliente = vis.ObjRef " +
                      "               and item.cdProduto = @cdProduto)");

            MI_AddParameters("@Categoria", Categoria);
            MI_AddParameters("@cdProduto", cdProduto);
            return MI_ExecuteDataSet();
        }

        public DataTable ExpoJoia(string cdPremio, string Categoria)
        {
            MI_SetSql("select  " +
                      "      @Categoria as Categoria,  " +
                      "      CodigoBarras, " +
                      "      Res1 as CodigoConsultoria, " +
                      "      NomeCompleto, " +
                      "      CPF, " +
                      "      Res4 as NivelCarreira, " +
                      "      EmailPessoal as Email, " +
                      "      NumeroTelefonePessoal as Telefone, " + 
                      "      PresenteEvento  " +
                      "  from tblvisitante vis  " +
                      "  where exists(select * from tblpremioConsultora pre " +
                      "           inner join tblPremioConsultora_Item item on pre.objref = item.cdReconhecimento " +
                      "           where pre.CodigoBarras = vis.CodigoBarras " +
                      "           and item.dsRetirado in('S','N') " +
                      "           and item.dsTrocaLocal = @cdPremio " +
                      "           and item.dsTipo = 'JOIA') ");

            MI_AddParameters("@Categoria", Categoria);
            MI_AddParameters("@cdPremio", cdPremio);
            return MI_ExecuteDataSet();
        }
        public DataTable ExpoJoiaEntrega(string cdPremio, string Categoria)
        {
            MI_SetSql("select distinct       " +
                      "	    @Categoria as Categoria, " + 
                      "	    pre.dsReconhecimento as Premio,        " + 
                      "	    vis.CodigoBarras,        " + 
                      "	    Res1 as CodigoConsultoria,    " +     
                      "	    vis.NomeCompleto,        " +
                      "     vis.CPF, " +
                      "     vis.Res4 as NivelCarreira, " +
                      "     vis.EmailPessoal as Email, " +
                      "     vis.NumeroTelefonePessoal as Telefone, " + 
                      "	    vis.PresenteEvento     " + 
                      "from tblvisitante vis   " + 
                      "inner join tblPremioConsultora pre on pre.CodigoBarras = vis.CodigoBarras   " + 
                      "where  " + 
                      "   exists(select * from tblPremioConsultora_Item item  " + 
                      "	      where pre.ObjRef = item.cdReconhecimento " +
                      "		  and item.dsEscolhaPre = @cdPremio " + 
                      "		  and item.dsTipo = 'JOIA') " + 
                      "or exists(select * from tblPremioConsultora_Item item  " + 
                      "		  where pre.ObjRef = item.cdReconhecimento " + 
                      "		  and item.dsEscolhaPre = '' " +
                      "		  and item.dsTrocaLocal = @cdPremio " + 
                      "		  and item.dsTipo = 'JOIA') " + 
                      "or exists(select * from tblPremioConsultora_Item item  " + 
                      "		  where pre.ObjRef = item.cdReconhecimento " + 
                      "		  and item.dsRetirado = 'S' " +
                      "		  and item.dsTrocaLocal = @cdPremio " + 
                      "		  and item.dsTipo = 'JOIA') " + 
                      "or exists(select * from tblPremioConsultora_Item item " + 
                      "		  where pre.ObjRef = item.cdReconhecimento " + 
                      "		  and item.dsEscolhaPre <> '' " + 
                      "		  and item.dsEscolhaPre <> item.dsTrocaLocal " +
                      "		  and item.dsTrocaLocal = @cdPremio " + 
                      "		  and item.dsTipo = 'JOIA')");

            MI_AddParameters("@Categoria", Categoria);
            MI_AddParameters("@cdPremio", cdPremio);
            return MI_ExecuteDataSet();
        }
        public DataTable ExpoJoiaTroca(string cdPremio, string Categoria)
        {
            MI_SetSql("select " +
                      " 	@Categoria as Categoria, " +
                      "	    vis.CodigoBarras,     " +
                      "	    Res1 as CodigoConsultoria, " +
                      "	    vis.NomeCompleto,        " +
                      "     vis.CPF, " +
                      "     vis.Res4 as NivelCarreira, " +
                      "     vis.EmailPessoal as Email, " +
                      "     vis.NumeroTelefonePessoal as Telefone, " + 
                      "	    vis.PresenteEvento     " +
                      "from tblvisitante vis   " +
                      "where  " +
                      "   exists(select * from tblPremioConsultora pre  " +
                      "		  inner join tblPremioConsultora_Item item on pre.ObjRef = item.cdReconhecimento " +
                      "	      where pre.CodigoBarras = vis.CodigoBarras   " +
                      "		  and item.dsEscolhaPre <> '' " +
                      "		  and item.dsEscolhaPre <> item.dsTrocaLocal " +
                      "		  and item.dsTrocaLocal = @cdPremio " +
                      "		  and item.dsTipo = 'JOIA')");

            MI_AddParameters("@Categoria", Categoria);
            MI_AddParameters("@cdPremio", cdPremio);
            return MI_ExecuteDataSet();
        }
        public DataTable ExpoJoiaEstoque(string cdPremio, string Categoria)
        {
            MI_SetSql(  "select distinct       " +
                        "	@Categoria as Categoria,  " + 
                        "	pre.dsReconhecimento as Premio,  " + 
                        "	vis.CodigoBarras,         " + 
                        "	Res1 as CodigoConsultoria,      " +    
                        "	vis.NomeCompleto,         " +
                        "   vis.CPF, " +
                        "   vis.Res4 as NivelCarreira, " +
                        "   vis.EmailPessoal as Email,  " +
                        "   vis.NumeroTelefonePessoal as Telefone, " + 
                        "	vis.PresenteEvento      " + 
                        "from tblvisitante vis   " + 
                        "inner join tblPremioConsultora pre on pre.CodigoBarras = vis.CodigoBarras   " + 
                        "where   " + 
                        "   exists(select * from tblPremioConsultora_Item item   " + 
                        "		  where pre.ObjRef = item.cdReconhecimento  " +
                        "	      and item.dsEscolhaPre = @cdPremio  " + 
                        "		  and item.dsTipo = 'JOIA')  " + 
                        "or exists(select * from tblPremioConsultora_Item item   " + 
                        "	      where pre.ObjRef = item.cdReconhecimento  " +
                        "		  and item.dsTrocaLocal = @cdPremio  " + 
                        "		  and item.dsTipo = 'JOIA'  " + 
                        "		  and item.dsRetirado = 'S')");

            MI_AddParameters("@Categoria", Categoria);
            MI_AddParameters("@cdPremio", cdPremio);
            return MI_ExecuteDataSet();
        }

        public DataTable ExpoColetor(string Coletor)
        {
            string query = "SELECT   " +
                           "	col.Coletor,  " +
                           "	convert(varchar(10),col.dataEntrada,103) + ' ' + col.HoraEntrada as DataEntrada,  " +
                           "	vis.CodigoBarras,         " +
                           "	vis.Res1 as CodigoConsultoria,  " +
                           "	vis.NomeCompleto,   " +
                           "    vis.CPF, " +
                           "    vis.Res4 as NivelCarreira, " +
                           "    vis.EmailPessoal as Email, " +
                           "    vis.NumeroTelefonePessoal as Telefone  " + 
                           "FROM tblcoletorFeira col  " +
                           "INNER JOIN tblvisitante vis on vis.CodigoBarras = col.codigo   " +
                           "where EntradaConcedida = 'S'  " +
                           "and col.Coletor = @Coletor " +
                           "order by col.coletor, col.DataEntrada";


            MI_SetSql(query);
            MI_AddParameters("@Coletor", Coletor);

            return MI_ExecuteDataSet();
        }

        public DataTable ObterDadosPorConsultoraOuNome(string prChave)
        {
            string query = "SELECT " +
                "	tblcoletorfeira.Coletor AS Coletor, " +
                "	'DataEntrada' = convert(varchar(10), tblcoletorfeira.DataEntrada, 103) + ' ' + SUBSTRING(tblcoletorfeira.HoraEntrada, 1,8), " +
                "	tblvisitante.CodigoBarras AS CodigoBarras, " +
                "	Res1 AS CodigoConsultoria, " +
                "	NomeCompleto AS NomeCompleto  " +
                "FROM  " +
                "	tblvisitante " +
                "		INNER JOIN tblcoletorfeira ON " +
                "			tblcoletorfeira.Codigo = tblvisitante.CodigoBarras " +
                "WHERE " +
                "	tblcoletorfeira.EntradaConcedida = 'S' AND  " +
                "	(tblvisitante.NomeCompleto LIKE '"+ prChave +"%' OR Res1 LIKE '"+ prChave +"%') " +
                "ORDER BY " +
                "	'DataEntrada' DESC";


            MI_SetSql(query);

            return MI_ExecuteDataSet();
        }

    }
}