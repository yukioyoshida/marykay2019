﻿using System.Data;
using Interacao;

public class CAT_Relatorios : CAB_Relatorios
{
    public CAT_Relatorios()
    {
    }

    public DataTable MS_ObterColetor(string prSeminario)
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.MS_ObterColetor(prSeminario);
    }

    public DataTable MS_RelizarLogin(string prUsuario, string prSenha)
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.MS_RelizarLogin(prUsuario, prSenha);
    }

    public DataTable MS_ObterRoteiro()
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.MS_ObterRoteiro();
    }

    public DataTable MS_ObterQuantidadePorTipoPremio(string prTipo, string prSeminario)
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.MS_ObterQuantidadePorTipoPremio(prTipo, prSeminario);
    }

    public DataTable MS_ObterPessoasNoRoteiro(string prChave, string prSeminario, string prCdRoteiro, string prColetor)
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.MS_ObterPessoasNoRoteiro(prChave, prSeminario, prCdRoteiro, prColetor);
    }

    public DataTable MS_ObterPessoasNoRoteiroAvancado(string prChave, string prSeminario, string prCdRoteiro, string prColetor)
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.MS_ObterPessoasNoRoteiroAvancado(prChave, prSeminario, prCdRoteiro, prColetor);
    }


    public DataTable MS_ObterInscricaoPorChave(string prSeminario, string prChave)
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.MS_ObterInscricaoPorChave(prSeminario, prChave);
    }

    public DataTable MS_ObterLeituras(string prCodigoBarras)
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.MS_ObterLeituras(prCodigoBarras);
    }

    public DataTable MS_ObterCredenciamento(string prSeminario)
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.MS_ObterCredenciamento(prSeminario);
    }

    public DataTable MS_ObterCredenciamentoRoteiro(string prSeminario)
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.MS_ObterCredenciamentoRoteiro(prSeminario);
    }

    public DataTable MS_ObterQuantidadesJoia(string prSeminario)
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.MS_ObterQuantidadesJoia(prSeminario);
    }


    public override DataTable Rel_PorParametro(string campo)
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.Rel_PorParametro(campo);
    }
    public override DataTable Rel_PorProduto(string produto)
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.Rel_PorProduto(produto);
    }
    public override DataTable Rel_Joias()
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.Rel_Joias();
    }

    public override DataTable Rel_JoiasEstoque()
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.Rel_JoiasEstoque();
    }

    public override DataTable Rel_JoiasTroca()
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.Rel_JoiasTroca();
    }

    public override DataTable Rel_JoiasEntrega()
    {
        DAO_Relatorios IDAO_Relatorios = new DAO_Relatorios();
        return IDAO_Relatorios.Rel_JoiasEntrega();
    }
}