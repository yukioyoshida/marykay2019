﻿using System.Data;

public interface IRelatorios
{
    DataTable Rel_PorParametro(string campo);
    DataTable Rel_PorProduto(string produto);
    DataTable Rel_Joias();
    DataTable Rel_JoiasEstoque();
    DataTable Rel_JoiasTroca();
    DataTable Rel_JoiasEntrega();
}