﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;

namespace Interacao
{
    public class DAO_Relatorios : FWK_DAO_Relatorios
    {
        public DataTable MS_ObterColetor(string prSeminario)
        {
            MI_SetSql("SELECT DISTINCT(Coletor) FROM tblcoletorfeira WHERE Evento = '"+ prSeminario + "' ORDER BY Coletor");

            return MI_ExecuteDataSet();
        }

        public DataTable MS_RelizarLogin(string prUsuario, string prSenha)
        {
            MI_AddParameters("@Usuario", prUsuario);
            MI_AddParameters("@Senha", prSenha);

            return MI_ExecuteProcedimento("SP_RealizarLogin");
        }

        public DataTable MS_ObterRoteiro()
        {
            return MI_ExecuteProcedimento("SP_ObterRoteiro");
        }

        public DataTable MS_ObterInscricaoPorChave(string prSeminario, string prChave)
        {
            MI_AddParameters("@Seminario", prSeminario);
            MI_AddParameters("@Chave", prChave);

            return MI_ExecuteProcedimento("SP_ObterInscricao");
        }

        public DataTable MS_ObterLeituras(string prCodigoBarras)
        {
            MI_AddParameters("@CodigoBarras", prCodigoBarras);

            return MI_ExecuteProcedimento("SP_ObterLeituras");
        }

        public DataTable MS_ObterCredenciamento(string prSeminario)
        {
            MI_AddParameters("@Seminario", prSeminario);

            return MI_ExecuteProcedimento("SP_Credenciamento");
        }

        public DataTable MS_ObterCredenciamentoRoteiro(string prSeminario)
        {
            MI_AddParameters("@Seminario", prSeminario);

            return MI_ExecuteProcedimento("SP_CredenciamentoRoteiro");
        }

        public DataTable MS_ObterQuantidadesJoia(string prSeminario)
        {
            MI_AddParameters("@Seminario", prSeminario);

            return MI_ExecuteProcedimento("SP_RelQuantidadeJoias");
        }

        public DataTable MS_ObterQuantidadePorTipoPremio(string prTipo, string prSeminario)
        {
            MI_AddParameters("@Tipo", prTipo);
            MI_AddParameters("@Seminario", prSeminario);

            return MI_ExecuteProcedimento("SP_RelJoiasPorTipo");
        }

        public DataTable MS_ObterPessoasNoRoteiro(string prChave, string prSeminario, string prCdRoteiro, string prColetor)
        {
            MI_AddParameters("@Seminario", prSeminario);
            MI_AddParameters("@cdRoteiro", prCdRoteiro);
            MI_AddParameters("@Chave", prChave);
            MI_AddParameters("@Coletor", prColetor);

            return MI_ExecuteProcedimento("SP_RelPessoasRoteiro");
        }

        public DataTable MS_ObterPessoasNoRoteiroAvancado(string prChave, string prSeminario, string prCdRoteiro, string prColetor)
        {
            MI_SetSql("DECLARE @Seminario VARCHAR(MAX) " +
            "DECLARE @cdRoteiro VARCHAR(MAX) " +
            "SET @cdRoteiro = "+ prCdRoteiro + " " +
            "SET @Seminario = '"+ prSeminario + "' " +
            "SELECT " +
            "	DISTINCT " +
            "	INC.Consultora, " +
            "	tblRoteiro.dsReconhecimento AS Tipo, " +
            "	INC.NomeDaConsultora AS Nome, " +
            "	INC.EmissaoCracha AS Credenciamento, " +
            //"	'Scanner' = (SELECT TOP(1) SUBSTRING(CAST(HoraSaida AS varchar(MAX)), 0,9) FROM tblcoletorfeira WHERE Codigo = INC.CodigoBarras AND ProdutoAutorizado = (SELECT TOP(1) dsReconhecimento FROM tblRoteiro WHERE tblRoteiro.cdRoteiro = @cdRoteiro) ORDER BY ObjRef ASC), " +
            "	'Scanner' = (SELECT TOP(1) SUBSTRING(CAST(HoraSaida AS varchar(MAX)), 0,9) FROM tblcoletorfeira WHERE Codigo = INC.CodigoBarras AND Coletor = @Coletor ORDER BY ObjRef ASC), " +
            "	'Faixa' =  " +
            "	(  " +
            "		SELECT   " +
            "			COUNT(*)  " +
            "		FROM  " +
            "			tblPremioConsultora   " +
            "				INNER JOIN tblPremioConsultora_Item ON  " +
            "					tblPremioConsultora_Item.cdReconhecimento = tblPremioConsultora.ObjRef  " +
            "				INNER JOIN tblRoteiro ON " +
            "					tblRoteiro.cdRoteiro = tblPremioConsultora_Item.dsTrocaLocal " +
            "		WHERE  " +
            "			tblPremioConsultora.NrConsultora = INC.Consultora AND  " +
            "			tblPremioConsultora.ATIVO = 'S' AND  " +
            "			tblPremioConsultora_Item.dsRetirado = 'S' AND  " +
            "			tblRoteiro.TipoJoia = 'Faixa' " +
            "	),  " +
            "   INC.TipodeInscricao " +
            "FROM   " +
            "	tblInscricao AS INC  " +
            "		INNER JOIN tblReconhecimento AS REC ON  " +
            "			INC.Consultora = REC.Consultora  " +
            "		INNER JOIN tblReconhecimento AS BLA ON  " +
            "			INC.Consultora = BLA.Consultora  " +
            "			INNER JOIN tblRoteiro ON  " +
            "				tblRoteiro.cdRoteiro = BLA.cdRoteiro " +
            "WHERE  " +
            "	REC.dsSituacao = 'ATIVO' AND  " +
            "   BLA.dsSituacao = 'ATIVO' AND  " +
            "	REC.cdRoteiro = @cdRoteiro AND  " +
            "	INC.TipodeInscricao LIKE CONCAT('%',@Seminario,'%') AND  " +
            "	BLA.cdRoteiro IN ("+ prChave + ") ORDER BY INC.NomeDaConsultora");

            MI_AddParameters("@Coletor", prColetor);

            return MI_ExecuteDataSet();
        }
    }
}