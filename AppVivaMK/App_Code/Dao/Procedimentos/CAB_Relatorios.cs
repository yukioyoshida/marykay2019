﻿using System.Data;

public abstract class CAB_Relatorios : IRelatorios
{
    public abstract DataTable Rel_PorParametro(string campo);
    public abstract DataTable Rel_PorProduto(string produto);
    public abstract DataTable Rel_Joias();
    public abstract DataTable Rel_JoiasEstoque();
    public abstract DataTable Rel_JoiasTroca();
    public abstract DataTable Rel_JoiasEntrega();
}