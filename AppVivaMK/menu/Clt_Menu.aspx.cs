﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class menu_Clt_Menu : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        // do the bartman
        MI_InserirMenu();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["oUsuario"] == null)
        {
            Response.Redirect("~/autentication/Clt_Autentication.aspx", true);
        }

        MI_SetAtributoPadrao();
        
    }

    private void MI_SetAtributoPadrao()
    {
        this.Title = new Global().AP_Title;
        lblSeminario.Text = Session["oSeminario"].ToString();
    }

    private void MI_InserirMenu()
    {
        pnlRelatorio.Controls.Clear();

        Button btoAcao = new Button();
        foreach (DataRow Row in new CAT_Relatorios().MS_ObterRoteiro().Rows)
        {
            //if (Row["Dia"].ToString() == prDia)
            //{
                btoAcao = new Button();
                btoAcao.CommandName = Row["cdRoteiro"].ToString() + "_" + Row["Dia"].ToString();
                btoAcao.Text = Row["dsReconhecimento"].ToString();
                btoAcao.CssClass = "ButtonMenuRelatorio";
                btoAcao.Click += new EventHandler(Unnamed_Click);
                btoAcao.Visible = false;
                pnlRelatorio.Controls.Add(btoAcao);
            //}
        }
    }

    public void Unnamed_Click(object sender, EventArgs e)
    {
        Button btoAcao = (Button)sender;
        if (btoAcao.CommandName == "rastrear")
        {
            Response.Redirect("~/credenciamento/Clt_Search.aspx", true);
        }
        else if (btoAcao.CommandName == "credenciamento")
        {
            Response.Redirect("~/credenciamento/Clt_ResumoCredenciamento.aspx", true);
        }
        else if (btoAcao.CommandName == "joias")
        {
            Response.Redirect("~/Joias/Clt_ResumoJoias.aspx", true);
        }
        else
        {
            string[] command = btoAcao.CommandName.ToString().Split('_');

            Response.Redirect("~/Roteiro/Clt_Roteiro.aspx?oRoteiro=" + command[0] + "&onmRoteiro=" + btoAcao.Text.ToString(), true);
        }

    }

    private void MI_ExibirBotoes(string prDia)
    {
        foreach(Control objControle in pnlRelatorio.Controls)
        {
            Button bto = (Button)objControle;

            string[] CommandName = bto.CommandName.Split('_');
            if (CommandName[1].ToString() == prDia)
                bto.Visible = true;
            else
                bto.Visible = false;

        }
        
    }

    protected void btoDia1_Click(object sender, EventArgs e)
    {
        MI_ExibirBotoes("1");
    }

    protected void btoDia2_Click(object sender, EventArgs e)
    {
        MI_ExibirBotoes("2");
    }
}