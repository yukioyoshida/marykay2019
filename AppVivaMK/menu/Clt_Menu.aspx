﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Clt_Menu.aspx.cs" Inherits="menu_Clt_Menu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <link href="../layout/Style.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="container-aplication">

            <div style="text-align:right;">
                <img src="../image/exit.png" onclick="MI_Return();" style="cursor:pointer; width:30px; margin-top:5px; margin-right:10px;" />
            </div>

            <div style="width:100%; margin-top:-40px; text-align:center;" > 
                <p style="margin-left:5px;padding-top:10px; font-weight:bold;">Seminário</p>
                <p style="margin-left:5px;margin-top:-10px;"><asp:Label ID="lblSeminario" runat="server" Text="AMARELO"></asp:Label></p>
            </div>
            
            <div class="LinhaDivisoria"></div>
            
            <p>Selecione o Item desejado</p>

            <asp:Panel runat="server" ID="pnlRelatorioFixo" CssClass="pnlMenuRelatorio"> 
                
                <asp:Button runat="server" CommandName="rastrear" Text="Rastrear participante" CssClass="ButtonMenuRelatorio" OnClick="Unnamed_Click" />
                <asp:Button runat="server" CommandName="credenciamento" Text="Credenciamento" CssClass="ButtonMenuRelatorio" OnClick="Unnamed_Click" />
                <asp:Button runat="server" CommandName="joias" Text="Sala de jóias" CssClass="ButtonMenuRelatorio" OnClick="Unnamed_Click" />

            </asp:Panel>

            <div class="LinhaDivisoria" style="padding-top:10px;"></div>

            <p>Selecione o dia</p>
            <p>
                <asp:Button runat="server" Text="DIA 1" ID="btoDia1" CssClass="Button" OnClick="btoDia1_Click"  />
                &nbsp;&nbsp;
                <asp:Button runat="server" Text="DIA 2" ID="btoDia2" CssClass="Button" OnClick="btoDia2_Click" />
            </p>

            <asp:Panel runat="server" ID="pnlRelatorio" CssClass="pnlMenuRelatorio"> 
                


            </asp:Panel>



        </div>
    </form>
</body>
</html>

<script>
    function MI_Return() {
        window.location = '../autentication/Clt_Seminario.aspx';
    }

</script>
