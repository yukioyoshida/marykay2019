﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class credenciamento_Clt_ResumoCredenciamento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Session["oUsuario"] == null) || (Session["oSeminario"] == null))
        {
            Response.Redirect("~/autentication/Clt_Autentication.aspx", true);
        }
        if (!IsPostBack)
        {
            MI_SetAtributoPadrao();
            MI_CarregarGrid();
        }
    }

    private void MI_SetAtributoPadrao()
    {
        this.Title = new Global().AP_Title;
        lblSeminario.Text = Session["oSeminario"].ToString();
    }

    private void MI_CarregarGrid()
    {
        DataTable dtCredenciamento = new DataTable();
        dtCredenciamento.Columns.Add("Tipo", typeof(string));
        dtCredenciamento.Columns.Add("Inscritos", typeof(string));
        dtCredenciamento.Columns.Add("Presentes", typeof(string));
        dtCredenciamento.Columns.Add("NaoPresentes", typeof(string));
        dtCredenciamento.Columns.Add("Novas", typeof(string));
        dtCredenciamento.Columns.Add("Total", typeof(string));

        int itTotalInscricao = 0;
        int itPresentes = 0;
        int itNaoTotalInscricao = 0;
        int itNovas = 0;
        int itTotalTotal = 0;
        int itTotal = 0;
        foreach (DataRow Row in new CAT_Relatorios().MS_ObterCredenciamento(Session["oSeminario"].ToString()).Rows)
        {
            DataRow RowInsert = dtCredenciamento.NewRow();
            RowInsert["Tipo"] = Row["Tipo"].ToString();
            RowInsert["Inscritos"] = Row["Inscritos"].ToString();
            RowInsert["Presentes"] = Row["Presentes"].ToString();
            RowInsert["NaoPresentes"] = Row["NaoPresentes"].ToString();
            RowInsert["Novas"] = Row["Novas"].ToString();
            itTotal = Convert.ToInt32(Row["Presentes"]) + Convert.ToInt32(Row["Novas"]);
            RowInsert["Total"] = itTotal.ToString();
            RowInsert.EndEdit();
            dtCredenciamento.Rows.Add(RowInsert);

            itTotalInscricao += Convert.ToInt32(Row["Inscritos"].ToString());
            itPresentes += Convert.ToInt32(Row["Presentes"].ToString());
            itNaoTotalInscricao += Convert.ToInt32(Row["NaoPresentes"].ToString());
            itNovas += Convert.ToInt32(Row["Novas"].ToString());
            itTotalTotal += itTotal;
        }

        // TOTAL
        DataRow RowInsertTotals = dtCredenciamento.NewRow();
        RowInsertTotals["Tipo"] = "TOTAL";
        RowInsertTotals["Inscritos"] = itTotalInscricao.ToString();
        RowInsertTotals["Presentes"] = itPresentes.ToString();
        RowInsertTotals["NaoPresentes"] = itNaoTotalInscricao.ToString();
        RowInsertTotals["Novas"] = itNovas.ToString();
        RowInsertTotals["Total"] = itTotalTotal.ToString();
        RowInsertTotals.EndEdit();
        dtCredenciamento.Rows.Add(RowInsertTotals);

        gdwCredenciamento.DataSource = dtCredenciamento;
        gdwCredenciamento.DataBind();


    }

    protected void btoDia1_Click(object sender, EventArgs e)
    {
        MI_CarregarDetalhes("1");
    }

    protected void btoDia2_Click(object sender, EventArgs e)
    {
        MI_CarregarDetalhes("2");
    }

    private void MI_CarregarDetalhes(string prDia)
    {
        int itTotal = 0;
        DataTable dtCredenciamentoRoteiro = new DataTable();
        dtCredenciamentoRoteiro.Columns.Add("Tipo", typeof(string));
        dtCredenciamentoRoteiro.Columns.Add("Inscritos", typeof(string));
        dtCredenciamentoRoteiro.Columns.Add("Presentes", typeof(string));
        dtCredenciamentoRoteiro.Columns.Add("NaoPresentes", typeof(string));
        dtCredenciamentoRoteiro.Columns.Add("Novas", typeof(string));
        dtCredenciamentoRoteiro.Columns.Add("Total", typeof(string));

        foreach (DataRow Row in new CAT_Relatorios().MS_ObterCredenciamentoRoteiro(Session["oSeminario"].ToString()).Rows)
        {
            if (Row["Dia"].ToString() == prDia)
            {
                DataRow RowInsert = dtCredenciamentoRoteiro.NewRow();
                RowInsert["Tipo"] = Row["Tipo"].ToString();
                RowInsert["Inscritos"] = Row["Inscritos"].ToString();
                RowInsert["Presentes"] = Row["Presentes"].ToString();
                RowInsert["NaoPresentes"] = Row["NaoPresentes"].ToString();
                RowInsert["Novas"] = Row["Novas"].ToString();
                itTotal = Convert.ToInt32(Row["Presentes"]) + Convert.ToInt32(Row["Novas"]);
                RowInsert["Total"] = itTotal.ToString();
                RowInsert.EndEdit();
                dtCredenciamentoRoteiro.Rows.Add(RowInsert);
            }
        }

        gdwCredenciamentoRoteiro.DataSource = dtCredenciamentoRoteiro;
        gdwCredenciamentoRoteiro.DataBind();
    }
}