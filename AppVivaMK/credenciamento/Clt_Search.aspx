﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Clt_Search.aspx.cs" Inherits="credenciamento_Clt_Search" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <link href="../layout/Style.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="container-aplication">

            <div style="text-align:right;">
                <img src="../image/exit.png" onclick="MI_Return();" style="cursor:pointer; width:30px; margin-top:5px; margin-right:10px;" />
            </div>

            <div style="width:100%; margin-top:-40px; text-align:center;" > 
                <p style="margin-left:5px;padding-top:10px; font-weight:bold;">Seminário</p>
                <p style="margin-left:5px;margin-top:-10px;"><asp:Label ID="lblSeminario" runat="server" Text=""></asp:Label></p>
            </div>
            
            <div class="LinhaDivisoria"></div>
            
            <p>Rastrear participante</p>

            <asp:Panel runat="server" ID="pnlRelatorio" CssClass="pnlMenuRelatorio"> 
                
                <p style="font-size:8pt;margin-bottom:0px;">cod.consultora/cpf/nome</p>
                
                <div>
                    <asp:TextBox runat="server" ID="edtChave" Text="" Width="100%"></asp:TextBox>
                </div>
                <div style="margin-top:5px;">
                    <asp:Button runat="server" ID="btoFiltrar" Text="Iniciar Busca" OnClick="btoFiltrar_Click" Width="100%" CssClass="Button" />
                </div>
                <br />
                <asp:GridView runat="server" ID="gdwParticipantes" AutoGenerateColumns="False" Width="100%" style="font-size: xx-small" OnRowCommand="gdwParticipantes_RowCommand">
                    <Columns>
                        <asp:BoundField DataField="CodigoBarras" HeaderText="" Visible="true" >
                        <ItemStyle Width="1px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Consultora" HeaderText="Cod" >
                        <ItemStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CPF" HeaderText="CPF" >
                        <ItemStyle Width="60px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NomedaConsultora" HeaderText="Nome" >
                        <FooterStyle HorizontalAlign="Left" />
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:ButtonField ButtonType="Image" CommandName="Selecionar" Text="Button" ImageUrl="~/image/search.png" >
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="20px" />
                        </asp:ButtonField>
                    </Columns>
                </asp:GridView>
                <br />
                <asp:Label ID="lblOcorrencias" runat="server" Text=""></asp:Label>
                <asp:GridView runat="server" ID="gdwHistorico" AutoGenerateColumns="False" Width="100%" style="font-size: x-small">
                    <Columns>
                        <asp:BoundField DataField="Data" HeaderText="Data" />
                        <asp:BoundField DataField="Coletor" HeaderText="Coletor" />
                        <asp:BoundField DataField="Produto" HeaderText="Produto" />
                    </Columns>
                </asp:GridView>

            </asp:Panel>

        </div>
    </form>
</body>
</html>

<script>
    function MI_Return() {
        window.location = '../menu/Clt_Menu.aspx';
    }

</script>
