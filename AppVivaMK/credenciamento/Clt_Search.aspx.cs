﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class credenciamento_Clt_Search : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Session["oUsuario"] == null) || (Session["oSeminario"] == null))
        {
            Response.Redirect("~/autentication/Clt_Autentication.aspx", true);
        }
        if (!IsPostBack)
        {
            MI_SetAtributoPadrao();
        }
    }

    private void MI_SetAtributoPadrao()
    {
        this.Title = new Global().AP_Title;
        lblSeminario.Text = Session["oSeminario"].ToString();
    }

    private bool MI_Consistencias()
    {
        bool boResult = true;

        if (edtChave.Text.Trim() == string.Empty)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Entre com a chave de busca')", true);
            return boResult = false;
        }

        if (edtChave.Text.Trim().Length<=2)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Digite pelo menos 3 caracteres')", true);
            return boResult = false;
        }

        return boResult;
    }

    protected void btoFiltrar_Click(object sender, EventArgs e)
    {
        if (MI_Consistencias())
        {
            DataTable dtPessoas = new CAT_Relatorios().MS_ObterInscricaoPorChave(Session["oSeminario"].ToString(), edtChave.Text);

            gdwParticipantes.DataSource = dtPessoas;
            gdwParticipantes.DataBind();

            gdwHistorico.Visible = false;
        }
    }

    protected void gdwParticipantes_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        gdwHistorico.Visible = false;
        int index = Convert.ToInt32(e.CommandArgument);
        string stCodigoBarras = gdwParticipantes.Rows[index].Cells[0].Text.ToString();

        DataTable dtLeituras = new CAT_Relatorios().MS_ObterLeituras(stCodigoBarras);

        DataTable dtHistorico = new DataTable();
        dtHistorico.Columns.Add("Coletor", typeof(string));
        dtHistorico.Columns.Add("Data", typeof(string));
        dtHistorico.Columns.Add("Produto", typeof(string));

        foreach(DataRow Row in dtLeituras.Rows)
        {
            DataRow drInsert = dtHistorico.NewRow();
            drInsert["Coletor"] = Row["Coletor"].ToString();
            drInsert["Data"] = Convert.ToDateTime(Row["DataEntrada"]).ToShortDateString() + " " + Convert.ToDateTime(Row["HoraEntrada"]).ToShortTimeString();
            drInsert["Produto"] = Row["ProdutoAutorizado"].ToString();
            drInsert.EndEdit();
            dtHistorico.Rows.Add(drInsert);
            gdwHistorico.Visible = true;
        }

        gdwHistorico.DataSource = dtHistorico;
        gdwHistorico.DataBind();

        lblOcorrencias.Text = "Ocorrências encontradas: " + gdwHistorico.Rows.Count.ToString();

    }
}