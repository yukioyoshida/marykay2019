﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SPI;
using System.IO;
using OfficeOpenXml;

namespace Databasemanager
{
    public partial class ctl_AreaAssento : UserControl
    {
        public string AP_Seminario = "SELECIONE";
        public int AP_nuInscritos = 0;
        public ctl_AreaAssento()
        {
            InitializeComponent();
        }

        private void ctl_AreaAssento_Load(object sender, EventArgs e)
        {
            if (AP_Seminario == "SELECIONE")
            {
                Clt_SelectSeminario open = new Clt_SelectSeminario();
                open.ShowDialog();

                AP_Seminario = open.AP_RESULTADO;

                if (AP_Seminario == "SELECIONE")
                {
                    this.Controls.Clear();
                    return;
                }
            }

            grpAreaAssento.Visible = true;

            AP_nuInscritos = Convert.ToInt32(new DAO_Login().MS_ObterQuantidadeInscritosPorSeminario(AP_Seminario));
            lblNumeroInscritos.Text = "Nº Inscritos seminário " + AP_Seminario + ":" + AP_nuInscritos.ToString();

            MI_CarregarGridConfiguracao();
            MI_CarregarQuantidadeSemAreaAssento();
        }

        private void MI_CarregarQuantidadeSemAreaAssento()
        {
            string stRoteiros = string.Empty;
            foreach (DataGridViewRow row in gdwConfiguracao.Rows)
                stRoteiros += "," + row.Cells[0].Value.ToString();

            lblInscritosSemAssento.Text = "Nº Inscritos sem área de assento: " + new DAO_Login().MS_ObterQuantidadeInscritosSemReconhecimentoX(AP_Seminario, stRoteiros);
        }

        private void MI_CarregarGridConfiguracao()
        {
            gdwConfiguracao.Rows.Clear();

            DataTable dtConfiguracao = new DAO_Login().MS_ObterConfAreaAssento(AP_Seminario);
            if (dtConfiguracao.Rows.Count == 0) // GERAR AREA ASSENTO
            {
                int itPrioridade = 1;
                foreach(DataRow Row in new DAO_Login().MS_ObterRoteiro("I").Rows)
                {
                    if (Row["dsReconhecimento"].ToString().ToUpper().Contains("DE ASSENTO"))
                    {
                        gdwConfiguracao.Rows.Add(
                            Row["cdRoteiro"].ToString(),
                            Row["dsReconhecimento"].ToString(),
                            "0",
                            itPrioridade.ToString(),
                            "S"
                            );

                        itPrioridade++;
                    }
                }

                try
                {
                    int itQuantidadePorAssento = AP_nuInscritos / gdwConfiguracao.Rows.Count;
                    int itQuantidadePorAssentoDiferenca = AP_nuInscritos % gdwConfiguracao.Rows.Count;

                    foreach(DataGridViewRow Row in gdwConfiguracao.Rows)
                    {
                        Row.Cells[2].Value = itQuantidadePorAssento.ToString();
                    }

                    gdwConfiguracao.Rows[gdwConfiguracao.Rows.Count - 1].Cells[2].Value = Convert.ToString(Convert.ToInt32(gdwConfiguracao.Rows[0].Cells[2].Value) + itQuantidadePorAssentoDiferenca);
                }
                catch
                {

                }
            }
            else
            {
                foreach(DataRow Row in dtConfiguracao.Rows)
                {
                    gdwConfiguracao.Rows.Add(
                            Row["cdRoteiro"].ToString(),
                            Row["dsReconhecimento"].ToString(),
                            Row["nuQuantidade"].ToString(),
                            Row["nuPrioridade"].ToString(),
                            Row["dsGerador"].ToString()
                            );
                }
            }

            MI_ExibirSituacao();

        }

        private void MI_ExibirSituacao()
        {
            gdwResultado.Rows.Clear();

            foreach(DataGridViewRow Row in gdwConfiguracao.Rows)
            {
                gdwResultado.Rows.Add(
                        Row.Cells[0].Value.ToString(),
                        Row.Cells[1].Value.ToString(),
                        "Qtd. desejado: " + Row.Cells[2].Value.ToString() + " | Qtd. obtido: " + MI_ObterQuantidadePorRoteiro(Row.Cells[0].Value.ToString())
                        );
            }

            foreach (DataGridViewRow Row in gdwResultado.Rows)
                Row.Height = 31;
        }

        private string MI_ObterQuantidadePorRoteiro(string prCdRoteiro)
        {
            return new DAO_Login().MS_ObterQuantidadePorRoteiroSeminario(AP_Seminario, prCdRoteiro);
        }

        private bool MI_Consistencias()
        {
            bool boResult = true;

            return boResult;
        }

        private void btoGerarAreaAssento_Click(object sender, EventArgs e)
        {
            if (MI_Consistencias())
            {
                progress.Value = 0;
                gropImportacao.Visible = true;
                this.Invoke(new MethodInvoker(MI_IniciarImportacao));
            }
        }

        private void MI_IniciarImportacao()
        {
            // EXCLUIR CONFIGURAÇÕES
            new DAO_Login().MS_ExcluirConfiguracoesAssento(AP_Seminario);

            // INCLUIR NOVA CONFIGURAÇÃO
            foreach (DataGridViewRow Row in gdwConfiguracao.Rows)
            {
                CTO_AreaAssento ITO_AreaAssento = new CTO_AreaAssento();
                ITO_AreaAssento.AP_cdRoteiro = Convert.ToInt32(Row.Cells[0].Value);
                ITO_AreaAssento.AP_dsSeminario = AP_Seminario;
                ITO_AreaAssento.AP_nuQuantidade = Convert.ToInt32(Row.Cells[2].Value);
                ITO_AreaAssento.AP_nuPrioridade = Convert.ToInt32(Row.Cells[3].Value);
                ITO_AreaAssento.AP_dsGerador = Row.Cells[4].Value.ToString();

                new DAO_Login().MS_IncluirConfAreaAssento(ITO_AreaAssento);
            }

            // EXCLUIR ROTEIRO ATUAL

            foreach (DataGridViewRow Row in gdwConfiguracao.Rows)
            {
                if (Row.Cells[4].Value.ToString() == "S")
                {
                    new DAO_Login().MI_ExcluirReconhecimentoAreaAssento(Convert.ToInt32(Row.Cells[0].Value), AP_Seminario);
                }
            }

            // GERAR AS AREAS DE ASSENTO
            string stRoteiros = string.Empty;
            foreach (DataGridViewRow row in gdwConfiguracao.Rows)
                stRoteiros += "," + row.Cells[0].Value.ToString();

            
            // OBTER TODOS OS ACOMPANHANTES E DEIXA NA MEMORIA
            DataTable dtAcompanhenteSemRoteirosX = new DAO_Login().MS_ObterSomenteAcompanhantePorSeminarioQueNaoPossuemReconhecimentoX(AP_Seminario, stRoteiros);

            DataTable dtConfiguracao = new DAO_Login().MS_ObterConfAreaAssento(AP_Seminario);
            dtConfiguracao.DefaultView.Sort = "nuPrioridade ASC";
            foreach (DataRow Row in dtConfiguracao.Rows)
            {
                if (Row["dsGerador"].ToString().ToUpper() == "N")
                    continue;

                progress.Maximum = Convert.ToInt32(Row["nuQuantidade"].ToString());
                progress.Value = 0;
                progress.Visible = true;

                int itQuantidadeObtida = Convert.ToInt32(MI_ObterQuantidadePorRoteiro(Row["cdRoteiro"].ToString()));
                while (itQuantidadeObtida < Convert.ToInt32(Row["nuQuantidade"].ToString()))
                {
                    // DEFINIAR QUANTAS CONSULTORAS BUSCAR
                    int TOP = 1;
                    try
                    {
                        TOP = Convert.ToInt32(Row["nuQuantidade"].ToString()) - itQuantidadeObtida;
                        TOP = TOP / 3;
                        if (TOP <= 0)
                            TOP = 1;
                    }
                    catch { }

                    // IMPORTAR A CONSULTORA
                    // OBTER AS CONSULTORAS SEM ASSENTO DEFINIDO
                    DataTable dtConsultoraSemRoteirosX = new DAO_Login().MS_ObterSomenteConsultorasPorSeminarioQueNaoPossuemReconhecimentoX(AP_Seminario, stRoteiros, TOP);
                    if (dtConsultoraSemRoteirosX.Rows.Count == 0)
                        break;

                    foreach (DataRow RowConsultora in dtConsultoraSemRoteirosX.Rows)
                    {
                        CTO_Reconhecimento ITO_Reconhecimento = new CTO_Reconhecimento();
                        ITO_Reconhecimento.AP_cdInscricao = Convert.ToInt32(RowConsultora["cdInscricao"].ToString());
                        ITO_Reconhecimento.AP_cdRoteiro = Convert.ToInt32(Row["cdRoteiro"].ToString());
                        ITO_Reconhecimento.AP_Consultora = RowConsultora["Consultora"].ToString();
                        ITO_Reconhecimento.AP_dsMetodoInsert = "AUTOMÁTICO AREA DE ASSENTO";
                        new DAO_Login().MS_IncluirReconhecimento(ITO_Reconhecimento);
                        itQuantidadeObtida++;

                        // IMPORTAR ACOMPANHANTES CASO EXISTIR
                        foreach (DataRow RowAcompanhante in dtAcompanhenteSemRoteirosX.Select("Consultora = '"+ RowConsultora["Consultora"].ToString() + "'"))
                        {
                            CTO_Reconhecimento ITO_ReconhecimentoAcompanhante = new CTO_Reconhecimento();
                            ITO_ReconhecimentoAcompanhante.AP_cdInscricao = Convert.ToInt32(RowAcompanhante["cdInscricao"].ToString());
                            ITO_ReconhecimentoAcompanhante.AP_cdRoteiro = Convert.ToInt32(Row["cdRoteiro"].ToString());
                            ITO_ReconhecimentoAcompanhante.AP_Consultora = RowAcompanhante["Consultora"].ToString();
                            ITO_ReconhecimentoAcompanhante.AP_dsMetodoInsert = "AUTOMÁTICO AREA DE ASSENTO - ACOMPANHANTE";
                            new DAO_Login().MS_IncluirReconhecimento(ITO_ReconhecimentoAcompanhante);
                            itQuantidadeObtida++;
                        }
                    }

                    progress.Value = itQuantidadeObtida;
                }
            }

            MessageBox.Show("Operação realizada com sucesso", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            progress.Value = 0;
            progress.Visible = false;
            gropImportacao.Visible = false;
            MI_ExibirSituacao();
            MI_CarregarQuantidadeSemAreaAssento(); ;
        }

        private void gdwResultado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex==3)
                {
                    this.Controls.Clear();
                    this.Controls.Add(new ctl_editarAssento(AP_Seminario, gdwResultado.CurrentRow.Cells[0].Value.ToString(), gdwResultado.CurrentRow.Cells[1].Value.ToString()));
                }
            }
            catch { }
        }

        private void btoExportarParaExcel_Click(object sender, EventArgs e)
        {
            progress.Value = 0;
            this.Invoke(new MethodInvoker(MI_GerarExcel));
        }

        private void MI_GerarExcel()
        {
            if (!System.IO.Directory.Exists("C:\\Database Manager\\Gerador"))
            {
                System.IO.Directory.CreateDirectory("C:\\Database Manager\\Gerador");
            }

            DataTable dtPlanilhao = new DAO_Login().MS_ObterPlanilhao(AP_Seminario);

            // INSERIR ROTEIRO
            foreach (DataRow Row in new DAO_Login().MS_ObterRoteiro("I").Rows)
            {
                if (Row["dsReconhecimento"].ToString().Contains("Area de Assento"))
                {
                    try
                    {
                        dtPlanilhao.Columns.Add(Row["dsReconhecimento"].ToString());
                    }
                    catch
                    {
                        dtPlanilhao.Columns.Add(Row["dsReconhecimento"].ToString() + "_1");
                    }
                }
            }

            DataTable dtTodosReconhecimentos = new DAO_Login().MS_ObterTodosOsReconhecimentos();

            // VERIFICAR OS RECONHECIMENTOS
            int itQuantidade = 0;
            foreach (DataRow Row in dtPlanilhao.Rows)
            {
                string prNrConsultora = Row["Consultora"].ToString();

                for (int i = 19; i <= dtPlanilhao.Columns.Count - 1; i++)
                {
                    string prNomeReconhecimento = "";
                    prNomeReconhecimento = dtPlanilhao.Columns[i].ToString();
                    Row[i] = MI_ObterSePossuiReconhecimento(dtTodosReconhecimentos, prNrConsultora, prNomeReconhecimento);
                }


            }

            FileInfo caminhoNomeArquivo = new FileInfo(@"C:\\Database Manager\\Gerador\" + AP_Seminario + "_" + System.DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".xlsx");
            ExcelPackage arquivoExcel = new ExcelPackage(caminhoNomeArquivo);

            // CRIANDO (ADD) uma planilha neste arquivo e obtendo a referência para meu código operá-la.
            ExcelWorksheet planilha = arquivoExcel.Workbook.Worksheets.Add("AreaAssento" + AP_Seminario);

            for (int i = 1; i < dtPlanilhao.Columns.Count + 1; i++)
            {
                planilha.Cells[1, i].Value = dtPlanilhao.Columns[i - 1].ColumnName.ToString();
            }


            for (int i = 0; i < dtPlanilhao.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dtPlanilhao.Columns.Count; j++)
                {
                    planilha.Cells[i + 2, j + 1].Value = dtPlanilhao.Rows[i][j].ToString();
                }
            }

            arquivoExcel.Save();
            arquivoExcel.Dispose();

            System.Diagnostics.Process.Start(caminhoNomeArquivo.FullName.ToString());
        }

        private string MI_ObterSePossuiReconhecimento(DataTable dtReconhecimentos, string prConsultora, string prReconhecimento)
        {
            string stResult = string.Empty;

            DataRow[] dataRows = dtReconhecimentos.Select("Consultora='" + prConsultora + "' AND dsReconhecimento='" + prReconhecimento + "'");
            foreach (DataRow row in dataRows)
            {
                return stResult = "X";
            }

            return stResult;
        }
    }
}
