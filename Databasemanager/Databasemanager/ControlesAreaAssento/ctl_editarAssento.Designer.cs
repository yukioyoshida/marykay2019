﻿namespace Databasemanager
{
    partial class ctl_editarAssento
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpAreaAssento = new System.Windows.Forms.GroupBox();
            this.btoDel = new System.Windows.Forms.PictureBox();
            this.btoEdit = new System.Windows.Forms.PictureBox();
            this.btoAdd = new System.Windows.Forms.PictureBox();
            this.btoVoltar = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btoFiltrar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.edtChave = new System.Windows.Forms.TextBox();
            this.lblQuantidadeRegistros = new System.Windows.Forms.Label();
            this.gdwTable = new System.Windows.Forms.DataGridView();
            this.Reconhecimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataImportacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomeCredencial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdInscricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpAreaAssento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btoDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoVoltar)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdwTable)).BeginInit();
            this.SuspendLayout();
            // 
            // grpAreaAssento
            // 
            this.grpAreaAssento.Controls.Add(this.btoDel);
            this.grpAreaAssento.Controls.Add(this.btoEdit);
            this.grpAreaAssento.Controls.Add(this.btoAdd);
            this.grpAreaAssento.Controls.Add(this.btoVoltar);
            this.grpAreaAssento.Controls.Add(this.groupBox1);
            this.grpAreaAssento.Controls.Add(this.lblQuantidadeRegistros);
            this.grpAreaAssento.Controls.Add(this.gdwTable);
            this.grpAreaAssento.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpAreaAssento.Location = new System.Drawing.Point(16, 16);
            this.grpAreaAssento.Name = "grpAreaAssento";
            this.grpAreaAssento.Size = new System.Drawing.Size(834, 733);
            this.grpAreaAssento.TabIndex = 4;
            this.grpAreaAssento.TabStop = false;
            this.grpAreaAssento.Text = "[roteiro]";
            // 
            // btoDel
            // 
            this.btoDel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoDel.Image = global::Databasemanager.Properties.Resources.del;
            this.btoDel.Location = new System.Drawing.Point(720, 691);
            this.btoDel.Name = "btoDel";
            this.btoDel.Size = new System.Drawing.Size(35, 35);
            this.btoDel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoDel.TabIndex = 12;
            this.btoDel.TabStop = false;
            this.btoDel.Click += new System.EventHandler(this.btoDel_Click);
            // 
            // btoEdit
            // 
            this.btoEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoEdit.Image = global::Databasemanager.Properties.Resources.editblack;
            this.btoEdit.Location = new System.Drawing.Point(679, 691);
            this.btoEdit.Name = "btoEdit";
            this.btoEdit.Size = new System.Drawing.Size(35, 35);
            this.btoEdit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoEdit.TabIndex = 11;
            this.btoEdit.TabStop = false;
            this.btoEdit.Click += new System.EventHandler(this.btoEdit_Click);
            // 
            // btoAdd
            // 
            this.btoAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoAdd.Image = global::Databasemanager.Properties.Resources.add;
            this.btoAdd.Location = new System.Drawing.Point(638, 691);
            this.btoAdd.Name = "btoAdd";
            this.btoAdd.Size = new System.Drawing.Size(35, 35);
            this.btoAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoAdd.TabIndex = 10;
            this.btoAdd.TabStop = false;
            this.btoAdd.Click += new System.EventHandler(this.btoAdd_Click);
            // 
            // btoVoltar
            // 
            this.btoVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoVoltar.Image = global::Databasemanager.Properties.Resources.voltarblack;
            this.btoVoltar.Location = new System.Drawing.Point(788, 688);
            this.btoVoltar.Name = "btoVoltar";
            this.btoVoltar.Size = new System.Drawing.Size(38, 38);
            this.btoVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoVoltar.TabIndex = 9;
            this.btoVoltar.TabStop = false;
            this.btoVoltar.Click += new System.EventHandler(this.btoVoltar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btoFiltrar);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.edtChave);
            this.groupBox1.Location = new System.Drawing.Point(141, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(687, 64);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtro:";
            // 
            // btoFiltrar
            // 
            this.btoFiltrar.Location = new System.Drawing.Point(606, 23);
            this.btoFiltrar.Name = "btoFiltrar";
            this.btoFiltrar.Size = new System.Drawing.Size(75, 25);
            this.btoFiltrar.TabIndex = 8;
            this.btoFiltrar.Text = "Filtrar";
            this.btoFiltrar.UseVisualStyleBackColor = true;
            this.btoFiltrar.Click += new System.EventHandler(this.btoFiltrar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "Consultora:";
            // 
            // edtChave
            // 
            this.edtChave.Location = new System.Drawing.Point(131, 22);
            this.edtChave.Name = "edtChave";
            this.edtChave.Size = new System.Drawing.Size(469, 26);
            this.edtChave.TabIndex = 6;
            // 
            // lblQuantidadeRegistros
            // 
            this.lblQuantidadeRegistros.AutoSize = true;
            this.lblQuantidadeRegistros.Font = new System.Drawing.Font("Verdana", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidadeRegistros.Location = new System.Drawing.Point(6, 688);
            this.lblQuantidadeRegistros.Name = "lblQuantidadeRegistros";
            this.lblQuantidadeRegistros.Size = new System.Drawing.Size(164, 14);
            this.lblQuantidadeRegistros.TabIndex = 5;
            this.lblQuantidadeRegistros.Text = "[lblQuantidadeRegistros]";
            // 
            // gdwTable
            // 
            this.gdwTable.AllowUserToAddRows = false;
            this.gdwTable.AllowUserToResizeRows = false;
            this.gdwTable.BackgroundColor = System.Drawing.Color.White;
            this.gdwTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gdwTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdwTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Reconhecimento,
            this.DataImportacao,
            this.Quantidade,
            this.NomeCredencial,
            this.cdInscricao});
            this.gdwTable.Location = new System.Drawing.Point(6, 85);
            this.gdwTable.MultiSelect = false;
            this.gdwTable.Name = "gdwTable";
            this.gdwTable.RowHeadersVisible = false;
            this.gdwTable.RowTemplate.Height = 24;
            this.gdwTable.Size = new System.Drawing.Size(822, 600);
            this.gdwTable.TabIndex = 1;
            this.gdwTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdwTable_CellClick);
            // 
            // Reconhecimento
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Reconhecimento.DefaultCellStyle = dataGridViewCellStyle1;
            this.Reconhecimento.HeaderText = "cdReconhecimento";
            this.Reconhecimento.Name = "Reconhecimento";
            this.Reconhecimento.ReadOnly = true;
            this.Reconhecimento.Visible = false;
            // 
            // DataImportacao
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataImportacao.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataImportacao.HeaderText = "Consultora";
            this.DataImportacao.Name = "DataImportacao";
            this.DataImportacao.ReadOnly = true;
            this.DataImportacao.Width = 80;
            // 
            // Quantidade
            // 
            this.Quantidade.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Quantidade.DefaultCellStyle = dataGridViewCellStyle3;
            this.Quantidade.HeaderText = "Tipo de Inscrição";
            this.Quantidade.Name = "Quantidade";
            this.Quantidade.ReadOnly = true;
            // 
            // NomeCredencial
            // 
            this.NomeCredencial.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.NomeCredencial.DefaultCellStyle = dataGridViewCellStyle4;
            this.NomeCredencial.HeaderText = "Nome completo";
            this.NomeCredencial.Name = "NomeCredencial";
            this.NomeCredencial.ReadOnly = true;
            // 
            // cdInscricao
            // 
            this.cdInscricao.HeaderText = "cdInscricao";
            this.cdInscricao.Name = "cdInscricao";
            this.cdInscricao.ReadOnly = true;
            this.cdInscricao.Visible = false;
            // 
            // ctl_editarAssento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.grpAreaAssento);
            this.Name = "ctl_editarAssento";
            this.Size = new System.Drawing.Size(870, 768);
            this.Load += new System.EventHandler(this.ctl_editarAssento_Load);
            this.grpAreaAssento.ResumeLayout(false);
            this.grpAreaAssento.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btoDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoVoltar)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdwTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpAreaAssento;
        private System.Windows.Forms.Label lblQuantidadeRegistros;
        private System.Windows.Forms.DataGridView gdwTable;
        private System.Windows.Forms.TextBox edtChave;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btoFiltrar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox btoVoltar;
        private System.Windows.Forms.PictureBox btoDel;
        private System.Windows.Forms.PictureBox btoEdit;
        private System.Windows.Forms.PictureBox btoAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reconhecimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataImportacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomeCredencial;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdInscricao;
    }
}
