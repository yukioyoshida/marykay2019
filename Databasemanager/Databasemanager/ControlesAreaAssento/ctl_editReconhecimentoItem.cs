﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SPI;

namespace Databasemanager
{
    public partial class ctl_editReconhecimentoItem : UserControl
    {
        public Control AP_objControle;

        public string AP_cdReconhecimento { get; set; }
        public string AP_NomeCompleto { get; set; }
        public string AP_dsReconhecimento { get; set; }
        public string AP_ReteirosValido { get; set; }
        public string AP_TipoRoteiro { get; set; }

        // NECESSÁRIOS PARA A INCLUSÃO
        public string AP_Consultora { get; set; }
        public string AP_cdInscricao { get; set; }

        public ctl_editReconhecimentoItem()
        {
            InitializeComponent();
        }

        private void ctl_editReconhecimentoItem_Load(object sender, EventArgs e)
        {
            MI_CarregarTela();

            if (AP_cdReconhecimento == "0")
            {
                grpImportacao.Text = "Inclusão de roteiro";
                label2.Visible = false;
                edtNomeRoteiro.Visible = false;
                label3.Text = "Selecione o roteiro";
                btoVoltar.Visible = false;
            }

        }

        private void MI_CarregarTela()
        {
            edtNomeCompleto.Text = AP_NomeCompleto;
            edtNomeRoteiro.Text = AP_dsReconhecimento;

            MI_CarregarRoteiro();
        }

        private void MI_CarregarRoteiro()
        {
            DataTable dtRoteiro = new DAO_Login().MS_ObterRoteiro(AP_TipoRoteiro);

            string prQuery = string.Empty;
            if (AP_ReteirosValido != string.Empty)
                prQuery = "cdRoteiro IN (0" + AP_ReteirosValido + ")";

            ddlRoteiro.Items.Clear();
            foreach (DataRow Row in new DAO_Login().MS_ObterRoteiro(AP_TipoRoteiro).Select(prQuery))
            {
                ddlRoteiro.Items.Add(Row["dsReconhecimento"].ToString());
            }

        }

        private void btoVoltar_Click(object sender, EventArgs e)
        {
            MI_Voltar();
        }

        private void MI_Voltar()
        {
            this.Controls.Clear();
            this.Controls.Add(AP_objControle);
        }

        private bool MI_Consistencias()
        {
            bool boResult = true;

            if (ddlRoteiro.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Você deve selecionar um roteiro.", "Atenção!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return boResult = false;
            }

            string prCdRoteiro = new DAO_Login().MS_ObterRoteiroPorNome(ddlRoteiro.Text).Rows[0]["cdRoteiro"].ToString();


            return boResult;
        }

        private void btoSalvar_Click(object sender, EventArgs e)
        {
            if (MI_Consistencias())
            {
                if (AP_cdReconhecimento != "0")
                {
                    if (MessageBox.Show("Realmente deseja efetuar a troca de reteiro?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;

                    // REGISTRAR LOG
                    new CAT_CadastrarLogin().MS_RegistrarLog("TROCA DE ROTEIRO - " + AP_cdReconhecimento, Csv_Variaveis.AP_cdUsuario);

                    string prCdRoteiroEscolhido = new DAO_Login().MS_ObterRoteiroPorNome(ddlRoteiro.Text).Rows[0]["cdRoteiro"].ToString();
                    new DAO_Login().MS_AlterarRoteiroPessoa(AP_cdReconhecimento, prCdRoteiroEscolhido);

                    MessageBox.Show("Operação realizada com sucesso!", "Confirmação de operação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    MI_Voltar();
                }
                else
                {
                    string prCdRoteiroEscolhido = new DAO_Login().MS_ObterRoteiroPorNome(ddlRoteiro.Text).Rows[0]["cdRoteiro"].ToString();

                    // REGISTRAR LOG
                    new CAT_CadastrarLogin().MS_RegistrarLog("INCLUSÃO MANUAL DE ROTEIRO - " + prCdRoteiroEscolhido, Csv_Variaveis.AP_cdUsuario);

                    // INCLUSÃO DO ROTEIRO
                    CTO_Reconhecimento ITO_Reconhecimento = new CTO_Reconhecimento();

                    ITO_Reconhecimento.AP_cdUsuario = Csv_Variaveis.AP_cdUsuario;
                    ITO_Reconhecimento.AP_Consultora = AP_Consultora;
                    ITO_Reconhecimento.AP_dsMetodoInsert = "INCLUSÃO MANUAL";
                    ITO_Reconhecimento.AP_cdRoteiro = Convert.ToInt32(prCdRoteiroEscolhido);
                    ITO_Reconhecimento.AP_cdInscricao = Convert.ToInt32(AP_cdInscricao);
                    new DAO_Login().MS_IncluirReconhecimento(ITO_Reconhecimento);

                    MessageBox.Show("Operação realizada com sucesso!", "Confirmação de operação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    MI_Voltar();
                }
            }
        }
    }
}
