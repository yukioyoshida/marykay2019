﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SPI;

namespace Databasemanager
{
    public partial class ctl_editarAssento : UserControl
    {
        public DataGridViewRow RowSelected;

        public string AP_Seminario { get; set; }
        public string AP_cdRoteiro { get; set; }
        public string AP_dsReconhecimento { get; set; }
        public DataTable dtReconhecimento;

        public ctl_editarAssento(string prSeminario, string prRoteiro, string prReconhecimento)
        {
            InitializeComponent();
            AP_Seminario = prSeminario;
            AP_cdRoteiro = prRoteiro;
            AP_dsReconhecimento = prReconhecimento;
        }

        private void ctl_editarAssento_Load(object sender, EventArgs e)
        {
            grpAreaAssento.Text = AP_dsReconhecimento + " - SEMINÁRIO ("+ AP_Seminario + ")";
            MI_CarregarReconhecimento();
        }

        private void MI_CarregarReconhecimento()
        {
            dtReconhecimento = new DAO_Login().MS_ObterPessoasNoRoteiro(AP_Seminario, AP_cdRoteiro);
            MI_ExibirParticipantesRoteiro();
        }

        private void MI_ExibirParticipantesRoteiro()
        {
            gdwTable.Rows.Clear();

            foreach (DataRow Row in dtReconhecimento.Select("Consultora like '%" + edtChave.Text.Trim() + "%' OR NomedaConsultora LIKE '%"+ edtChave.Text.Trim() + "%'"))
            {
                gdwTable.Rows.Add(
                    Row["cdReconhecimento"].ToString(),
                    Row["Consultora"].ToString(),
                    Row["TipodeInscricao"].ToString(),
                    Row["NomedaConsultora"].ToString(),
                    Row["cdInscricao"].ToString()
                    );
            }

            lblQuantidadeRegistros.Text = gdwTable.Rows.Count.ToString() + " registros";
        }

        private void btoFiltrar_Click(object sender, EventArgs e)
        {
            MI_ExibirParticipantesRoteiro();
        }

        private void MI_Voltar()
        {
            this.Controls.Clear();
            ctl_AreaAssento open = new ctl_AreaAssento();
            open.AP_Seminario = AP_Seminario;
            this.Controls.Add(open);
        }

        private void btoVoltar_Click(object sender, EventArgs e)
        {
            MI_Voltar();
        }

        private void gdwTable_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewRow Row in gdwTable.Rows)
            {
                for (int i = 0; i <= Row.Cells.Count - 1; i++)
                    Row.Cells[i].Style.BackColor = System.Drawing.Color.White;
            }

            RowSelected = gdwTable.CurrentRow;

            for (int i = 0; i <= RowSelected.Cells.Count - 1; i++)
                RowSelected.Cells[i].Style.BackColor = System.Drawing.Color.LightBlue;
        }

        private void btoDel_Click(object sender, EventArgs e)
        {
            if (RowSelected != null)
            {
                if (MessageBox.Show("Realmente deseja excluir a área de assento deste participante:\n" + RowSelected.Cells[3].Value.ToString() + "?", "ATENÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    // REGISTRAR LOG
                    new CAT_CadastrarLogin().MS_RegistrarLog("EXCLUSÃO DE AREA DE ASSENTO - " + RowSelected.Cells[1].Value.ToString(), Csv_Variaveis.AP_cdUsuario);

                    // REALIZA OPERAÇÃO DE CANCELAMENTO
                    new DAO_Login().MI_ExcluirReconhecimentoPorIdReconhecimento(RowSelected.Cells[0].Value.ToString());

                    MessageBox.Show("Exclusão realizada com sucesso!", "Confirmação de operação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    MI_CarregarReconhecimento();
                }
            }
        }

        private void btoEdit_Click(object sender, EventArgs e)
        {
            ctl_editReconhecimentoItem open = new ctl_editReconhecimentoItem();

            ctl_editarAssento ctl_Atual = new ctl_editarAssento(AP_Seminario, AP_cdRoteiro, AP_dsReconhecimento);
            ctl_Atual.RowSelected = RowSelected;
            ctl_Atual.AP_Seminario = AP_Seminario;
            ctl_Atual.AP_cdRoteiro = AP_cdRoteiro;
            ctl_Atual.AP_dsReconhecimento = AP_dsReconhecimento;
            ctl_Atual.dtReconhecimento = dtReconhecimento;

            open.AP_objControle = ctl_Atual;
            open.AP_cdReconhecimento = RowSelected.Cells[0].Value.ToString();
            open.AP_NomeCompleto = RowSelected.Cells[3].Value.ToString();
            open.AP_dsReconhecimento = AP_dsReconhecimento;
            open.AP_TipoRoteiro = "I";

            string stRoteiroValidos = string.Empty;
            foreach (DataRow Row in new DAO_Login().MS_ObterRoteiro("I").Rows)
                if (Row["dsReconhecimento"].ToString().ToUpper().Contains("DE ASSENTO"))
                    stRoteiroValidos += "," + Row["cdRoteiro"].ToString();

            open.AP_ReteirosValido = stRoteiroValidos;

            this.Controls.Clear();
            this.Controls.Add(open);


        }

        private void btoAdd_Click(object sender, EventArgs e)
        {
            ctl_editReconhecimentoItem open = new ctl_editReconhecimentoItem();

            ctl_editarAssento ctl_Atual = new ctl_editarAssento(AP_Seminario, AP_cdRoteiro, AP_dsReconhecimento);
            ctl_Atual.RowSelected = RowSelected;
            ctl_Atual.AP_Seminario = AP_Seminario;
            ctl_Atual.AP_cdRoteiro = AP_cdRoteiro;
            ctl_Atual.AP_dsReconhecimento = AP_dsReconhecimento;
            ctl_Atual.dtReconhecimento = dtReconhecimento;

            open.AP_objControle = ctl_Atual;
            open.AP_cdReconhecimento = "0";
            open.AP_NomeCompleto = RowSelected.Cells[3].Value.ToString();
            open.AP_dsReconhecimento = "";
            open.AP_TipoRoteiro = "I";

            open.AP_cdInscricao = RowSelected.Cells[4].Value.ToString(); 
            open.AP_Consultora = RowSelected.Cells[1].Value.ToString();

            string stRoteiroValidos = string.Empty;
            foreach (DataRow Row in new DAO_Login().MS_ObterRoteiro("I").Rows)
                if (Row["dsReconhecimento"].ToString().ToUpper().Contains("DE ASSENTO"))
                    stRoteiroValidos += "," + Row["cdRoteiro"].ToString();

            open.AP_ReteirosValido = stRoteiroValidos;

            this.Controls.Clear();
            this.Controls.Add(open);
        }
    }
}
