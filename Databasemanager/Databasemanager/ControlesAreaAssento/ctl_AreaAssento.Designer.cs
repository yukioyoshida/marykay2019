﻿namespace Databasemanager
{
    partial class ctl_AreaAssento
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctl_AreaAssento));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpAreaAssento = new System.Windows.Forms.GroupBox();
            this.gropImportacao = new System.Windows.Forms.GroupBox();
            this.progress = new System.Windows.Forms.ProgressBar();
            this.btoGerarAreaAssento = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.gdwResultado = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblInscritosSemAssento = new System.Windows.Forms.Label();
            this.lblNumeroInscritos = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gdwConfiguracao = new System.Windows.Forms.DataGridView();
            this.cdRoteiro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataImportacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomeCredencial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Incluir = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.btoExportarParaExcel = new System.Windows.Forms.Button();
            this.grpAreaAssento.SuspendLayout();
            this.gropImportacao.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdwResultado)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdwConfiguracao)).BeginInit();
            this.SuspendLayout();
            // 
            // grpAreaAssento
            // 
            this.grpAreaAssento.Controls.Add(this.btoExportarParaExcel);
            this.grpAreaAssento.Controls.Add(this.gropImportacao);
            this.grpAreaAssento.Controls.Add(this.btoGerarAreaAssento);
            this.grpAreaAssento.Controls.Add(this.groupBox3);
            this.grpAreaAssento.Controls.Add(this.groupBox2);
            this.grpAreaAssento.Controls.Add(this.groupBox1);
            this.grpAreaAssento.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpAreaAssento.Location = new System.Drawing.Point(16, 16);
            this.grpAreaAssento.Name = "grpAreaAssento";
            this.grpAreaAssento.Size = new System.Drawing.Size(834, 733);
            this.grpAreaAssento.TabIndex = 4;
            this.grpAreaAssento.TabStop = false;
            this.grpAreaAssento.Text = "Definição da área de assento";
            this.grpAreaAssento.Visible = false;
            // 
            // gropImportacao
            // 
            this.gropImportacao.Controls.Add(this.progress);
            this.gropImportacao.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gropImportacao.Location = new System.Drawing.Point(240, 656);
            this.gropImportacao.Name = "gropImportacao";
            this.gropImportacao.Size = new System.Drawing.Size(372, 66);
            this.gropImportacao.TabIndex = 4;
            this.gropImportacao.TabStop = false;
            this.gropImportacao.Text = "Importação";
            this.gropImportacao.Visible = false;
            // 
            // progress
            // 
            this.progress.Location = new System.Drawing.Point(11, 27);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(352, 23);
            this.progress.TabIndex = 1;
            // 
            // btoGerarAreaAssento
            // 
            this.btoGerarAreaAssento.Location = new System.Drawing.Point(44, 664);
            this.btoGerarAreaAssento.Name = "btoGerarAreaAssento";
            this.btoGerarAreaAssento.Size = new System.Drawing.Size(184, 57);
            this.btoGerarAreaAssento.TabIndex = 3;
            this.btoGerarAreaAssento.Text = "Gerar área de assento";
            this.btoGerarAreaAssento.UseVisualStyleBackColor = true;
            this.btoGerarAreaAssento.Click += new System.EventHandler(this.btoGerarAreaAssento_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.gdwResultado);
            this.groupBox3.Location = new System.Drawing.Point(21, 274);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(792, 379);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Situação área de assento";
            // 
            // gdwResultado
            // 
            this.gdwResultado.AllowUserToAddRows = false;
            this.gdwResultado.AllowUserToResizeRows = false;
            this.gdwResultado.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.gdwResultado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gdwResultado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdwResultado.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewComboBoxColumn1});
            this.gdwResultado.Location = new System.Drawing.Point(23, 36);
            this.gdwResultado.MultiSelect = false;
            this.gdwResultado.Name = "gdwResultado";
            this.gdwResultado.RowHeadersVisible = false;
            this.gdwResultado.RowTemplate.Height = 24;
            this.gdwResultado.Size = new System.Drawing.Size(747, 319);
            this.gdwResultado.TabIndex = 3;
            this.gdwResultado.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdwResultado_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "cdRoteiro";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn2.FillWeight = 11.49425F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Roteiro";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F);
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn3.HeaderText = "Quantidade";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 250;
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F);
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle17.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle17.NullValue")));
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewComboBoxColumn1.HeaderText = "Editar";
            this.dataGridViewComboBoxColumn1.Image = global::Databasemanager.Properties.Resources.edit;
            this.dataGridViewComboBoxColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.Width = 60;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblInscritosSemAssento);
            this.groupBox2.Controls.Add(this.lblNumeroInscritos);
            this.groupBox2.Location = new System.Drawing.Point(597, 36);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(216, 222);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Informações";
            // 
            // lblInscritosSemAssento
            // 
            this.lblInscritosSemAssento.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblInscritosSemAssento.Location = new System.Drawing.Point(11, 88);
            this.lblInscritosSemAssento.Name = "lblInscritosSemAssento";
            this.lblInscritosSemAssento.Size = new System.Drawing.Size(194, 47);
            this.lblInscritosSemAssento.TabIndex = 1;
            this.lblInscritosSemAssento.Text = "Nº Inscritos sem área de assento: [ QTD ]";
            // 
            // lblNumeroInscritos
            // 
            this.lblNumeroInscritos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblNumeroInscritos.Location = new System.Drawing.Point(14, 35);
            this.lblNumeroInscritos.Name = "lblNumeroInscritos";
            this.lblNumeroInscritos.Size = new System.Drawing.Size(194, 47);
            this.lblNumeroInscritos.TabIndex = 0;
            this.lblNumeroInscritos.Text = "Nº Inscritos seminário [COR]";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gdwConfiguracao);
            this.groupBox1.Location = new System.Drawing.Point(21, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(570, 222);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Definição de limites";
            // 
            // gdwConfiguracao
            // 
            this.gdwConfiguracao.AllowUserToAddRows = false;
            this.gdwConfiguracao.AllowUserToResizeRows = false;
            this.gdwConfiguracao.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.gdwConfiguracao.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gdwConfiguracao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdwConfiguracao.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cdRoteiro,
            this.DataImportacao,
            this.Quantidade,
            this.NomeCredencial,
            this.Incluir});
            this.gdwConfiguracao.Location = new System.Drawing.Point(23, 35);
            this.gdwConfiguracao.MultiSelect = false;
            this.gdwConfiguracao.Name = "gdwConfiguracao";
            this.gdwConfiguracao.RowHeadersVisible = false;
            this.gdwConfiguracao.RowTemplate.Height = 24;
            this.gdwConfiguracao.Size = new System.Drawing.Size(524, 165);
            this.gdwConfiguracao.TabIndex = 2;
            // 
            // cdRoteiro
            // 
            this.cdRoteiro.HeaderText = "cdRoteiro";
            this.cdRoteiro.Name = "cdRoteiro";
            this.cdRoteiro.ReadOnly = true;
            this.cdRoteiro.Visible = false;
            this.cdRoteiro.Width = 70;
            // 
            // DataImportacao
            // 
            this.DataImportacao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataImportacao.DefaultCellStyle = dataGridViewCellStyle18;
            this.DataImportacao.FillWeight = 11.49425F;
            this.DataImportacao.HeaderText = "Roteiro";
            this.DataImportacao.Name = "DataImportacao";
            this.DataImportacao.ReadOnly = true;
            this.DataImportacao.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Quantidade
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F);
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Quantidade.DefaultCellStyle = dataGridViewCellStyle19;
            this.Quantidade.HeaderText = "QTD.";
            this.Quantidade.Name = "Quantidade";
            this.Quantidade.Width = 90;
            // 
            // NomeCredencial
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F);
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.NomeCredencial.DefaultCellStyle = dataGridViewCellStyle20;
            this.NomeCredencial.HeaderText = "Ordem";
            this.NomeCredencial.Name = "NomeCredencial";
            this.NomeCredencial.Width = 70;
            // 
            // Incluir
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F);
            dataGridViewCellStyle21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Incluir.DefaultCellStyle = dataGridViewCellStyle21;
            this.Incluir.HeaderText = "Incluir";
            this.Incluir.Items.AddRange(new object[] {
            "S",
            "N"});
            this.Incluir.Name = "Incluir";
            this.Incluir.Width = 65;
            // 
            // btoExportarParaExcel
            // 
            this.btoExportarParaExcel.Location = new System.Drawing.Point(629, 665);
            this.btoExportarParaExcel.Name = "btoExportarParaExcel";
            this.btoExportarParaExcel.Size = new System.Drawing.Size(184, 57);
            this.btoExportarParaExcel.TabIndex = 5;
            this.btoExportarParaExcel.Text = "Exportar para excel";
            this.btoExportarParaExcel.UseVisualStyleBackColor = true;
            this.btoExportarParaExcel.Click += new System.EventHandler(this.btoExportarParaExcel_Click);
            // 
            // ctl_AreaAssento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.grpAreaAssento);
            this.Name = "ctl_AreaAssento";
            this.Size = new System.Drawing.Size(870, 768);
            this.Load += new System.EventHandler(this.ctl_AreaAssento_Load);
            this.grpAreaAssento.ResumeLayout(false);
            this.gropImportacao.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gdwResultado)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gdwConfiguracao)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpAreaAssento;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView gdwConfiguracao;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblNumeroInscritos;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdRoteiro;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataImportacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomeCredencial;
        private System.Windows.Forms.DataGridViewComboBoxColumn Incluir;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView gdwResultado;
        private System.Windows.Forms.Button btoGerarAreaAssento;
        private System.Windows.Forms.Label lblInscritosSemAssento;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.GroupBox gropImportacao;
        private System.Windows.Forms.ProgressBar progress;
        private System.Windows.Forms.Button btoExportarParaExcel;
    }
}
