﻿namespace Databasemanager
{
    partial class ctl_editReconhecimentoItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpImportacao = new System.Windows.Forms.GroupBox();
            this.btoVoltar = new System.Windows.Forms.PictureBox();
            this.btoSalvar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ddlRoteiro = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.edtNomeRoteiro = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.edtNomeCompleto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grpImportacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btoVoltar)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpImportacao
            // 
            this.grpImportacao.Controls.Add(this.btoVoltar);
            this.grpImportacao.Controls.Add(this.btoSalvar);
            this.grpImportacao.Controls.Add(this.groupBox2);
            this.grpImportacao.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpImportacao.Location = new System.Drawing.Point(16, 16);
            this.grpImportacao.Name = "grpImportacao";
            this.grpImportacao.Size = new System.Drawing.Size(569, 333);
            this.grpImportacao.TabIndex = 5;
            this.grpImportacao.TabStop = false;
            this.grpImportacao.Text = "Alteração de reconhecimento";
            // 
            // btoVoltar
            // 
            this.btoVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoVoltar.Image = global::Databasemanager.Properties.Resources.voltarblack;
            this.btoVoltar.Location = new System.Drawing.Point(486, 278);
            this.btoVoltar.Name = "btoVoltar";
            this.btoVoltar.Size = new System.Drawing.Size(38, 38);
            this.btoVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoVoltar.TabIndex = 10;
            this.btoVoltar.TabStop = false;
            this.btoVoltar.Click += new System.EventHandler(this.btoVoltar_Click);
            // 
            // btoSalvar
            // 
            this.btoSalvar.Location = new System.Drawing.Point(16, 278);
            this.btoSalvar.Name = "btoSalvar";
            this.btoSalvar.Size = new System.Drawing.Size(132, 42);
            this.btoSalvar.TabIndex = 0;
            this.btoSalvar.Text = "Salvar";
            this.btoSalvar.UseVisualStyleBackColor = true;
            this.btoSalvar.Click += new System.EventHandler(this.btoSalvar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ddlRoteiro);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.edtNomeRoteiro);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.edtNomeCompleto);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(16, 18);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(532, 250);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // ddlRoteiro
            // 
            this.ddlRoteiro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlRoteiro.FormattingEnabled = true;
            this.ddlRoteiro.Items.AddRange(new object[] {
            "Credenciamento",
            "Palco",
            "Platéia",
            "Sala"});
            this.ddlRoteiro.Location = new System.Drawing.Point(15, 163);
            this.ddlRoteiro.Name = "ddlRoteiro";
            this.ddlRoteiro.Size = new System.Drawing.Size(493, 26);
            this.ddlRoteiro.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Alterar para:";
            // 
            // edtNomeRoteiro
            // 
            this.edtNomeRoteiro.Location = new System.Drawing.Point(15, 103);
            this.edtNomeRoteiro.Name = "edtNomeRoteiro";
            this.edtNomeRoteiro.ReadOnly = true;
            this.edtNomeRoteiro.Size = new System.Drawing.Size(493, 26);
            this.edtNomeRoteiro.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Roteiro atual";
            // 
            // edtNomeCompleto
            // 
            this.edtNomeCompleto.Location = new System.Drawing.Point(15, 44);
            this.edtNomeCompleto.Name = "edtNomeCompleto";
            this.edtNomeCompleto.ReadOnly = true;
            this.edtNomeCompleto.Size = new System.Drawing.Size(493, 26);
            this.edtNomeCompleto.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome completo:";
            // 
            // ctl_editReconhecimentoItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.grpImportacao);
            this.Name = "ctl_editReconhecimentoItem";
            this.Size = new System.Drawing.Size(870, 768);
            this.Load += new System.EventHandler(this.ctl_editReconhecimentoItem_Load);
            this.grpImportacao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btoVoltar)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpImportacao;
        private System.Windows.Forms.Button btoSalvar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox ddlRoteiro;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edtNomeRoteiro;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edtNomeCompleto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox btoVoltar;
    }
}
