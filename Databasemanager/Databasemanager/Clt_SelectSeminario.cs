﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Databasemanager
{
    public partial class Clt_SelectSeminario : Form
    {
        public string AP_RESULTADO { get; set; }
        public Clt_SelectSeminario()
        {
            InitializeComponent();
        }

        private void Clt_SelectSeminario_Load(object sender, EventArgs e)
        {
            AP_RESULTADO = "SELECIONE";
            ddlSeminario.Text = AP_RESULTADO;
        }

        private void btoOK_Click(object sender, EventArgs e)
        {
            AP_RESULTADO = ddlSeminario.Text;
            this.Close();
        }
    }
}
