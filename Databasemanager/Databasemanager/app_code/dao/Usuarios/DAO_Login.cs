using Databasemanager;
using System;
using System.Collections.Generic;
using System.Data;
namespace SPI
{
    public class DAO_Login : FWK_DAO_Login
    {
        public DAO_Login()
        {
        }

        public DataTable MS_ObterPremiacoes(string prCodigoConsultora)
        {
            MI_SetSql("SELECT " +
                        "	tblPremioConsultora.ObjRef, " +
                        "	tblPremioConsultora.dsReconhecimento, " +
                        "	ISNULL(tblRoteiro.dsReconhecimento, 'N�O ESCOLHEU') AS PREMIO " +
                        "FROM  " +
                        "	tblPremioConsultora " +
                        "		INNER JOIN tblPremioConsultora_Item ON " +
                        "			tblPremioConsultora.ObjRef = tblPremioConsultora_Item.cdReconhecimento " +
                        "				LEFT JOIN tblRoteiro ON " +
                        "					tblRoteiro.cdRoteiro = tblPremioConsultora_Item.dsTrocaLocal " +
                        "WHERE " +
                        "	tblPremioConsultora.NrConsultora = @Consultora AND " +
                        "	tblPremioConsultora.ATIVO = 'S'");
            MI_AddParameters("@Consultora", prCodigoConsultora);

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterRegistroBaseMK(string prCodigoConsultora)
        {
            MI_SetSql("SELECT TOP(1) nmNSD, NivelDeCarreira FROM tblGeralMaryKay WHERE Consultora = @Consultora");
            MI_AddParameters("@Consultora", prCodigoConsultora);

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterUsuarios()
        {
            MI_SetSql("SELECT ObjRef as 'CODIGO',   " +
                      "NomeUsuario as 'Nome',       " +
                      "EmailUsuario as 'Email',     " +
                      "CelularUsuario as 'Celular', " +
                      "NomeLogin as 'Usuario',      " +
                      "SenhaLogin as 'Senha',       " +
                      "NivelAcesso as 'Acesso'      " +
                      "From tblUsuarios             " +
                      "order by NomeUsuario         ");
            return MI_ExecuteDataSet();
        }

        public CTO_Login MS_Logar(string prstUsuario, string prstSenha)
        {

            CTO_Login ITO_Login = new CTO_Login();
            MI_SetSql(AP_SelectALL +
                       "FROM tblUsuarios                " +
                       "WHERE NomeLogin = @Usuario and  " +
                       "SenhaLogin = @Senha             ");
            MI_AddParameters("@Usuario", prstUsuario);
            MI_AddParameters("@Senha", prstSenha);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Login = null;
            else
                ITO_Login = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Login;
        }


        public CTO_Login MS_Obter(string prstID)
        {
            CTO_Login ITO_Login = new CTO_Login();
            MI_SetSql(AP_SelectALL +
                       "FROM tblUsuarios " +
                       "WHERE ObjRef = @ID   ");
            MI_AddParameters("@ID", prstID);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Login = null;
            else
                ITO_Login = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Login;
        }

        public void MI_ImportarBancoGeral(List<CTO_BancoGeral> lITO_BancoGeral)
        {
            MI_SetSql("");

            string prValue = string.Empty;
            int iQuantidade = 0;
            foreach (CTO_BancoGeral ITO_BancoGeral in lITO_BancoGeral)
            {
                if (iQuantidade == 0)
                    prValue += " (@Consultora" + iQuantidade.ToString() + ", @dsCPF" + iQuantidade + ", @nmConsultora" + iQuantidade + ", @nmNSD" + iQuantidade + ", @NivelDeCarreira" + iQuantidade + ")";
                else
                    prValue += " ,(@Consultora" + iQuantidade.ToString() + ", @dsCPF" + iQuantidade + ", @nmConsultora" + iQuantidade + ", @nmNSD" + iQuantidade + ", @NivelDeCarreira" + iQuantidade + ")";

                MI_AddParameters("@Consultora" + iQuantidade.ToString() + "", ITO_BancoGeral.AP_NrConsultora);
                MI_AddParameters("@dsCPF" + iQuantidade.ToString() + "", ITO_BancoGeral.AP_CPF);
                MI_AddParameters("@nmConsultora" + iQuantidade.ToString() + "", ITO_BancoGeral.AP_NmConsultora);
                MI_AddParameters("@nmNSD" + iQuantidade.ToString() + "", ITO_BancoGeral.AP_NmNSN);
                MI_AddParameters("@NivelDeCarreira" + iQuantidade.ToString() + "", ITO_BancoGeral.AP_NivelDeCarreira);

                iQuantidade++;
            }

            MI_SetSqlNoClear("INSERT INTO tblGeralMaryKay (Consultora, dsCPF, nmConsultora, nmNSD, NivelDeCarreira) VALUES " + prValue + "");

            MI_ExecuteNonQuery();
        }

        public void MS_IncluirReconhecimentoEmLote(List<CTO_Reconhecimento> ldadosReconhecimento)
        {
            MI_SetSql("");

            string prValue = string.Empty;
            int iQuantidade = 0;
            foreach (CTO_Reconhecimento ITO_Reconhecimento in ldadosReconhecimento)
            {
                if (iQuantidade == 0)
                    prValue += " (NEWID(), GETDATE(), @cdUsuario" + iQuantidade.ToString() + ", @Consultora" + iQuantidade + ", @cdRoteiro" + iQuantidade + ", 'ATIVO'" + ", @dsMetodoInsert" + iQuantidade + ", @cdInscricao" + iQuantidade + ")";
                else
                    prValue += " ,(NEWID(), GETDATE(), @cdUsuario" + iQuantidade.ToString() + ", @Consultora" + iQuantidade + ", @cdRoteiro" + iQuantidade + ", 'ATIVO'" + ", @dsMetodoInsert" + iQuantidade + ", @cdInscricao" + iQuantidade + ")";


                MI_AddParameters("@cdUsuario" + iQuantidade.ToString(), ITO_Reconhecimento.AP_cdUsuario);
                MI_AddParameters("@Consultora" + iQuantidade.ToString(), ITO_Reconhecimento.AP_Consultora);
                MI_AddParameters("@cdRoteiro" + iQuantidade.ToString(), ITO_Reconhecimento.AP_cdRoteiro);
                MI_AddParameters("@dsMetodoInsert" + iQuantidade.ToString(), ITO_Reconhecimento.AP_dsMetodoInsert);
                MI_AddParameters("@cdInscricao" + iQuantidade.ToString(), ITO_Reconhecimento.AP_cdInscricao);

                iQuantidade++;
            }
            MI_SetSqlNoClear("INSERT tblReconhecimento (cdReconhecimento, dtImportacao, cdUsuario, Consultora, cdRoteiro, dsSituacao, dsMetodoInsert,cdInscricao) VALUES " + prValue + "");

            MI_ExecuteNonQuery();
        }

        public void MI_ExcluirBancoGeral()
        {
            MI_SetSql("DELETE FROM tblGeralMaryKay");
            MI_ExecuteNonQuery();
        }

        public void MI_ExcluirRoteiro(string prTipo)
        {
            MI_SetSql("UPDATE tblRoteiro SET dsSituacao = 'CANCELADO' WHERE TipoRoteiro = '" + prTipo + "'");
            MI_ExecuteNonQuery();

            MI_SetSql("UPDATE " +
                        "    tblReconhecimento " +
                        "SET " +
                        "    dsSituacao = 'CANCELADO' " +
                        "FROM " +
                        "    tblReconhecimento " +
                        "        INNER JOIN tblRoteiro ON " +
                        "            tblRoteiro.cdRoteiro = tblReconhecimento.cdRoteiro " +
                        "WHERE " +
                        "    tblRoteiro.TipoRoteiro = '" + prTipo + "'");
            MI_ExecuteNonQuery();
        }

        public void MI_ExcluirInscricao(string prTipo)
        {
            MI_SetSql("DELETE FROM tblInscricao WHERE Tipo = '" + prTipo + "'");
            MI_ExecuteNonQuery();
        }

        public void MI_ExcluirReconhecimento(int prCdRoteiro)
        {
            MI_SetSql("UPDATE tblReconhecimento SET dsSituacao = 'CANCELADO', dtCancelamento = GETDATE(), cdUsuarioCancelamento = @cdUsuarioCancelamento WHERE cdRoteiro = @cdRoteiro AND dsSituacao = 'ATIVO'");
            MI_AddParameters("@cdRoteiro", prCdRoteiro);
            MI_AddParameters("@cdUsuarioCancelamento", Csv_Variaveis.AP_cdUsuario);
            MI_ExecuteNonQuery();
        }
        public void MI_ExcluirReconhecimentoAreaAssento(int prCdRoteiro, string prSeminario)
        {
            MI_SetSql("UPDATE " +
                        "	tblReconhecimento  " +
                        "SET  " +
                        "	dsSituacao = 'CANCELADO', dtCancelamento = GETDATE(), cdUsuarioCancelamento = @cdUsuarioCancelamento  " +
                        "FROM " +
                        "	tblReconhecimento INNER JOIN tblInscricao ON " +
                        "		tblInscricao.Consultora = tblReconhecimento.Consultora " +
                        "WHERE  " +
                        "	cdRoteiro = @cdRoteiro AND dsSituacao = 'ATIVO' AND " +
                        "	tblInscricao.TipodeInscricao LIKE '%" + prSeminario + "%'       ");
            MI_AddParameters("@cdRoteiro", prCdRoteiro);
            MI_AddParameters("@cdUsuarioCancelamento", Csv_Variaveis.AP_cdUsuario);
            MI_ExecuteNonQuery();
        }

        public void MI_ExcluirPremioJoias(string prReconhecimento)
        {
            MI_SetSql("UPDATE tblPremioConsultora SET ATIVO = 'CANCELADO' WHERE dsReconhecimento = @dsReconhecimento ");
            MI_AddParameters("@dsReconhecimento", prReconhecimento);
            MI_ExecuteNonQuery();
        }

        public void MI_ExcluirReconhecimentoPorIdReconhecimento(string prcdReconhecimento)
        {
            MI_SetSql("UPDATE tblReconhecimento SET dsSituacao = 'CANCELADO', dtCancelamento = GETDATE(), cdUsuarioCancelamento = @cdUsuarioCancelamento WHERE cdReconhecimento = @cdReconhecimento AND dsSituacao = 'ATIVO'");
            MI_AddParameters("@cdReconhecimento", prcdReconhecimento);
            MI_AddParameters("@cdUsuarioCancelamento", Csv_Variaveis.AP_cdUsuario);
            MI_ExecuteNonQuery();
        }


        public void MI_ExcluirRoteiroPorId(int prCdRoteiro)
        {
            MI_SetSql("UPDATE tblRoteiro SET dsSituacao = 'CANCELADO', dtCancelamento = GETDATE(), cdUsuarioCancelamento = @cdUsuarioCancelamento WHERE cdRoteiro = @cdRoteiro AND dsSituacao = 'ATIVO'");
            MI_AddParameters("@cdRoteiro", prCdRoteiro);
            MI_AddParameters("@cdUsuarioCancelamento", Csv_Variaveis.AP_cdUsuario);
            MI_ExecuteNonQuery();
        }

        public void MS_IncluirRoteiro(CTO_Roteiro ITO_Roteiro)
        {
            MI_SetSql("");

            MI_AddParameters("@Reconhecimento", ITO_Roteiro.AP_Reconhecimento);
            MI_AddParameters("@Local", ITO_Roteiro.AP_Local);
            MI_AddParameters("@NomeCredencial", ITO_Roteiro.AP_NomeCredencial);
            MI_AddParameters("@TipoRoteiro", ITO_Roteiro.AP_TipoRoteiro);
            MI_AddParameters("@TipoJoia", ITO_Roteiro.AP_TipoJoia);
            MI_AddParameters("@Dia", ITO_Roteiro.AP_Dia);
            MI_AddParameters("@CodigoBarras", ITO_Roteiro.AP_dsCodigoBarrasProduto);
            MI_AddParameters("@Quantidade", ITO_Roteiro.AP_dsQuantidadeEstoque);

            if (!string.IsNullOrEmpty(ITO_Roteiro.AP_dsCodigoBarrasProduto))
                MI_AddParameters("@ControleEstoque", "1");

            MI_ExecuteProcedure("SP_InserirRoteiro");
        }

        public void MS_IncluirInscricao(CTO_Inscricao ITO_Inscricao)
        {

            MI_SetSql("");

            MI_AddParameters("@Pedido", ITO_Inscricao.AP_Pedido);
            MI_AddParameters("@IDdoPedido", ITO_Inscricao.AP_IDdoPedido);
            MI_AddParameters("@Consultora", ITO_Inscricao.AP_Consultora);
            MI_AddParameters("@NomedaConsultora", ITO_Inscricao.AP_NomedaConsultora);
            MI_AddParameters("@NiveldeCarreira", ITO_Inscricao.AP_NiveldeCarreira);
            MI_AddParameters("@Unidade", ITO_Inscricao.AP_Unidade);
            MI_AddParameters("@EmaildeContato", ITO_Inscricao.AP_EmaildeContato);
            MI_AddParameters("@Telefone", ITO_Inscricao.AP_Telefone);
            MI_AddParameters("@Cidade", ITO_Inscricao.AP_Cidade);
            MI_AddParameters("@Estado", ITO_Inscricao.AP_Estado);
            MI_AddParameters("@TipodeInscricao", ITO_Inscricao.AP_TipodeInscricao);
            MI_AddParameters("@Status", ITO_Inscricao.AP_Status);
            MI_AddParameters("@ValordoPedido", ITO_Inscricao.AP_ValordoPedido);
            MI_AddParameters("@PagocomCartao", ITO_Inscricao.AP_PagocomCartao);
            MI_AddParameters("@BandeiraCC", ITO_Inscricao.AP_BandeiraCC);
            MI_AddParameters("@Parcelas", ITO_Inscricao.AP_Parcelas);
            MI_AddParameters("@PagocomSaldo", ITO_Inscricao.AP_PagocomSaldo);
            MI_AddParameters("@PagoviaLoterica", ITO_Inscricao.AP_PagoviaLoterica);
            MI_AddParameters("@DatadoPedido", ITO_Inscricao.AP_DatadoPedido);
            MI_AddParameters("@NomedaNSD", ITO_Inscricao.AP_NomedaNSD);
            MI_AddParameters("@Tipo", ITO_Inscricao.AP_Tipo);

            // ESTRUTURA PADR�O INTERA��O
            MI_AddParameters("@NumeroTerminal", ITO_Inscricao.AP_NumeroTerminal);
            MI_AddParameters("@CodigoBarras", ITO_Inscricao.AP_CodigoBarras);
            MI_AddParameters("@Chave", ITO_Inscricao.AP_Chave);
            MI_AddParameters("@EmissaoCracha", ITO_Inscricao.AP_EmissaoCracha);
            MI_AddParameters("@PresenteDia1", ITO_Inscricao.AP_PresenteDia1);
            MI_AddParameters("@PresenteDia2", ITO_Inscricao.AP_PresenteDia2);
            MI_AddParameters("@PresenteDia3", ITO_Inscricao.AP_PresenteDia3);
            MI_AddParameters("@PresenteDia4", ITO_Inscricao.AP_PresenteDia4);
            MI_AddParameters("@PresenteDia5", ITO_Inscricao.AP_PresenteDia5);
            MI_AddParameters("@OrigemInsercao", ITO_Inscricao.AP_OrigemInsercao);
            MI_AddParameters("@DataOrigemInsercao", ITO_Inscricao.AP_DataOrigemInsercao);
            MI_AddParameters("@HoraOrigemInsercao", ITO_Inscricao.AP_HoraOrigemInsercao);
            MI_AddParameters("@UsuarioOrigemInsercao", ITO_Inscricao.AP_UsuarioOrigemInsercao);
            MI_AddParameters("@OrigemAlteracao", ITO_Inscricao.AP_OrigemAlteracao);
            MI_AddParameters("@DataOrigemAlteracao", ITO_Inscricao.AP_DataOrigemAlteracao);
            MI_AddParameters("@HoraOrigemAlteracao", ITO_Inscricao.AP_HoraOrigemAlteracao);
            MI_AddParameters("@UsuarioOrigemAlteracao", ITO_Inscricao.AP_UsuarioOrigemAlteracao);
            MI_AddParameters("@MetodoInsercao", ITO_Inscricao.AP_MetodoInsercao);
            MI_AddParameters("@ParticipouEdicao", ITO_Inscricao.AP_ParticipouEdicao);
            MI_AddParameters("@OrigemConvite", ITO_Inscricao.AP_OrigemConvite);


            MI_ExecuteProcedure("SP_IncluirInscricao");

        }

        public void MS_IncluirReconhecimento(CTO_Reconhecimento ITO_Reconhecimento)
        {
            MI_SetSql("");

            MI_AddParameters("@cdUsuario", ITO_Reconhecimento.AP_cdUsuario);
            MI_AddParameters("@Consultora", ITO_Reconhecimento.AP_Consultora);
            MI_AddParameters("@cdRoteiro", ITO_Reconhecimento.AP_cdRoteiro);
            MI_AddParameters("@dsMetodoInsert", ITO_Reconhecimento.AP_dsMetodoInsert);
            MI_AddParameters("@cdInscricao", ITO_Reconhecimento.AP_cdInscricao);

            MI_ExecuteProcedure("SP_IncluirReconhecimento");
        }

        public void MS_AtualizarNivelCarreira(string prConsultora, string prNivelCarreira)
        {
            MI_SetSql("");

            MI_AddParameters("@prNivelCarreira", prNivelCarreira);
            MI_AddParameters("@prConsultora", prConsultora);

            MI_ExecuteProcedure("SP_AtualizarNivelCarreira");
        }

        public void MS_AtualizarDadosDoConvidado(string prConsultora, string prNome)
        {
            MI_SetSql("");

            MI_AddParameters("@prNome", prNome);
            MI_AddParameters("@prConsultora", prConsultora);

            MI_ExecuteProcedure("SP_AtualizarNomeConvidado");
        }

        public void MS_RegistrarLog(string prAcao, int prUsuario)
        {
            MI_SetSql("INSERT tblLogAcoes (dtLog, cdUsuario, dsAcao) VALUES (GETDATE(), @cdUsuario, @dsAcao)");

            MI_AddParameters("@cdUsuario", prUsuario);
            MI_AddParameters("@dsAcao", prAcao);
            MI_ExecuteNonQuery();
        }

        public DataTable MS_ObterRoteiro(string prTipo)
        {
            MI_SetSql("SELECT * FROM tblRoteiro WHERE dsSituacao = 'ATIVO' AND TipoRoteiro = '" + prTipo + "' ORDER BY dsReconhecimento");

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterRoteiroPorNome(string prNome)
        {
            MI_SetSql("SELECT * FROM tblRoteiro where dsSituacao = 'ATIVO' AND dsReconhecimento = @dsReconhecimento");
            MI_AddParameters("@dsReconhecimento", prNome);

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterQtdImportacaoControle()
        {
            MI_SetSql("SELECT " +
                        "    (SELECT COUNT(*) FROM tblGeralMaryKay) AS BdGeral, " +
                        "    (SELECT COUNT(*) FROM tblInscricao) AS Inscricao,  " +
                        "	 (SELECT COUNT(*) FROM tblRoteiro WHERE dsSituacao = 'ATIVO' AND TipoRoteiro = 'I') AS Roteiro, " +
                        "	 (SELECT COUNT(*) FROM tblRoteiro WHERE dsSituacao = 'ATIVO' AND TipoRoteiro = 'J') AS RoteiroJoias  ");

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterQtdImportacaoReconhecimento(string prTipo)
        {
            if (prTipo == "J")
            {
                MI_SetSql("SELECT " +
                "	ROT.dsReconhecimento, " +
                "	'dtImportacao' = '---', " +
                "	( " +
                "		SELECT  " +
                "			COUNT(*)  " +
                "		FROM  " +
                "			tblPremioConsultora  " +
                "				INNER JOIN tblPremioConsultora_Item on " +
                "					tblPremioConsultora.ObjRef = tblPremioConsultora_Item.cdReconhecimento " +
                "				INNER JOIN tblRoteiro ON " +
                "					tblRoteiro.cdRoteiro = tblPremioConsultora_Item.dsTrocaLocal " +
                "		WHERE " +
                "			ATIVO = 'S' AND " +
                "			tblRoteiro.dsReconhecimento = ROT.dsReconhecimento " +
                "		) AS Quantidade " +
                "FROM  " +
                "	tblRoteiro AS ROT " +
                "WHERE  " +
                "	ROT.TipoRoteiro = 'J'");
            }
            else
            {
                MI_SetSql("WITH Reconhecimento AS " +
                            "( " +
                            "SELECT " +
                            "    DISTINCT(tblRoteiro.dsReconhecimento) AS dsReconhecimento, " +
                            "    tblRoteiro.cdRoteiro " +
                            "FROM " +
                            "    tblReconhecimento " +
                            "        INNER JOIN tblRoteiro ON " +
                            "            tblRoteiro.cdRoteiro = tblReconhecimento.cdRoteiro " +
                            "WHERE " +
                            "    tblRoteiro.dsSituacao = 'ATIVO' AND " +
                            "    tblReconhecimento.dsSituacao = 'ATIVO' AND " +
                            "    tblRoteiro.TipoRoteiro = '" + prTipo + "'" +
                            ") " +
                            "SELECT " +
                            "    dsReconhecimento " +
                            "    ,(select TOP(1) REC.dtImportacao FROM tblReconhecimento AS REC WHERE REC.cdRoteiro = Reconhecimento.cdRoteiro AND dsSituacao = 'ATIVO' ORDER BY REC.dtImportacao DESC) AS dtImportacao " +
                            "    ,(SELECT COUNT(*) FROM tblReconhecimento AS REC WHERE REC.cdRoteiro = Reconhecimento.cdRoteiro AND dsSituacao = 'ATIVO') AS Quantidade " +
                            "FROM " +
                            "    Reconhecimento");
            }

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterReconhecimentoNaoImportado(string prTipo)
        {
            MI_SetSql("SELECT " +
                        "    dsReconhecimento, " +
                        "    '-' AS 'dtImportacao', " +
                        "    '-' AS 'Quantidade' " +
                        "FROM " +
                        "    tblRoteiro " +
                        "WHERE " +
                        "    dsSituacao = 'ATIVO' AND " +
                        "    tblRoteiro.TipoRoteiro = '" + prTipo + "' AND " +
                        "    cdRoteiro NOT IN(SELECT cdRoteiro FROM tblReconhecimento WHERE dsSituacao = 'ATIVO') ORDER BY dsReconhecimento");

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterPlanilhao(string prSeminario)
        {
            MI_SetSql("SELECT " +
                        "    Consultora, " +
                        "    Pedido, " +
                        "    NomedaConsultora, " +
                        "    NiveldeCarreira, " +
                        "    Unidade, " +
                        "    EmaildeContato, " +
                        "    Telefone, " +
                        "    Cidade, " +
                        "    Estado, " +
                        "    TipodeInscricao, " +
                        "    Status, " +
                        "    ValordoPedido, " +
                        "    PagocomCartao, " +
                        "    BandeiraCC, " +
                        "    Parcelas, " +
                        "    PagocomSaldo, " +
                        "    PagoviaLoterica, " +
                        "    DatadoPedido, " +
                        "    NomedaNSD, EmissaoCracha Credenciamento, PresenteEvento Presente " +
                        "FROM " +
                        "    tblInscricao " +
                        "WHERE            " +
                        "   TipodeInscricao LIKE '%" + prSeminario + "%' " +
                        "ORDER BY " +
                        "    NomedaConsultora ");

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterPlanilhaoJoias(string prSeminario)
        {
            MI_SetSql("SELECT " +
                        "	tblInscricao.Consultora, " +
                        "	tblInscricao.Pedido, " +
                        "	tblInscricao.NomedaConsultora,	 " +
                        "	tblInscricao.NiveldeCarreira, " +
                        "	tblInscricao.Unidade, " +
                        "	tblInscricao.EmaildeContato, " +
                        "	tblInscricao.Telefone, " +
                        "	tblInscricao.Cidade, " +
                        "	tblInscricao.Estado, " +
                        "	tblInscricao.TipodeInscricao, " +
                        "	tblInscricao.Status, " +
                        "	tblInscricao.ValordoPedido, " +
                        "	tblInscricao.PagocomCartao, " +
                        "	tblInscricao.BandeiraCC, " +
                        "	tblInscricao.Parcelas, " +
                        "	tblInscricao.PagocomSaldo, " +
                        "	tblInscricao.PagoviaLoterica, " +
                        "	tblInscricao.DatadoPedido, " +
                        "	tblInscricao.NomedaNSD, " +
                        "	tblPremioConsultora.dsReconhecimento AS RECONHECIMENTO, " +
                        "	ISNULL(tblRoteiro.dsReconhecimento, 'N�O ESCOLHEU') AS PREMIO, " +
                        "	ISNULL(tblRoteiro.TipoJoia, '') AS TIPO " +
                        "FROM  " +
                        "	tblPremioConsultora " +
                        "		INNER JOIN tblPremioConsultora_Item ON " +
                        "			tblPremioConsultora_Item.cdReconhecimento = tblPremioConsultora.ObjRef " +
                        "		LEFT JOIN tblRoteiro ON " +
                        "			tblRoteiro.cdRoteiro = tblPremioConsultora_Item.dsTrocaLocal " +
                        "		INNER JOIN tblInscricao ON " +
                        "			tblInscricao.Consultora = tblPremioConsultora.NrConsultora " +
                        "WHERE " +
                        "	ATIVO = 'S' AND " +
                        "	tblInscricao.TipodeInscricao NOT LIKE '%CONVIDADOS%' AND " +
                        "	tblInscricao.TipodeInscricao LIKE '%" + prSeminario + "%' " +
                        "ORDER BY " +
                        "	tblPremioConsultora.NrConsultora ");

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterReconhecimentoPorConsultoraPorNmReconhecimento(string prConsultora, string prReconhecimento)
        {
            MI_SetSql("SELECT " +
                        "    CASE WHEN(COUNT(*)) >= 1 " +
                        "    THEN " +
                        "        'X' " +
                        "    END AS RESULTADO " +
                        "FROM " +
                        "    tblReconhecimento " +
                        "        INNER JOIN tblRoteiro ON " +
                        "            tblRoteiro.cdRoteiro = tblReconhecimento.cdRoteiro " +
                        "WHERE " +
                        "    Consultora = @Consultora AND " +
                        "    tblRoteiro.dsReconhecimento = @dsReconhecimento AND " +
                        "    tblRoteiro.dsSituacao = 'ATIVO' AND " +
                        "    tblReconhecimento.dsSituacao = 'ATIVO'                    ");

            MI_AddParameters("@Consultora", prConsultora);
            MI_AddParameters("@dsReconhecimento", prReconhecimento);

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterTodosOsReconhecimentos()
        {
            MI_SetSql("SELECT " +
                        "    tblReconhecimento.Consultora, " +
                        "    tblRoteiro.dsReconhecimento   " +
                        "FROM  " +
                        "    tblReconhecimento  " +
                        "        INNER JOIN tblRoteiro ON  " +
                        "            tblRoteiro.cdRoteiro = tblReconhecimento.cdRoteiro  " +
                        "WHERE  " +
                        "    tblReconhecimento.dsSituacao = 'ATIVO' AND  " +
                        "    tblRoteiro.dsSituacao = 'ATIVO'             ");


            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterRoteiroPorId(int prCdRoteiro)
        {
            MI_SetSql("SELECT * FROM tblRoteiro WHERE cdRoteiro = @cdRoteiro");
            MI_AddParameters("@cdRoteiro", prCdRoteiro);

            return MI_ExecuteDataSet();
        }

        public void MS_AlterarRoteito(CTO_Roteiro ITO_Roteiro)
        {
            MI_SetSql("UPDATE " +
                        "    tblRoteiro " +
                        "SET " +
                        "    dsReconhecimento = @dsReconhecimento, " +
                        "    dsLocal = @dsLocal, " +
                        "    dsNomeCredencial = @dsNomeCredencial " +
                        "WHERE " +
                        "    cdRoteiro = @cdRoteiro");

            MI_AddParameters("@dsReconhecimento", ITO_Roteiro.AP_Reconhecimento);
            MI_AddParameters("@dsLocal", ITO_Roteiro.AP_Local);
            MI_AddParameters("@dsNomeCredencial", ITO_Roteiro.AP_NomeCredencial);
            MI_AddParameters("@cdRoteiro", ITO_Roteiro.AP_cdRoteiro);

            MI_ExecuteNonQuery();
        }

        public void MS_IncluirRoteito(CTO_Roteiro ITO_Roteiro)
        {
            MI_SetSql("INSERT tblRoteiro ( " +
                        "dsReconhecimento, dsLocal, dsNomeCredencial, dsSituacao, TipoRoteiro) " +
                        "VALUES( " +
                        "@dsReconhecimento, @dsLocal, @dsNomeCredencial, 'ATIVO', @TipoRoteiro)");

            MI_AddParameters("@dsReconhecimento", ITO_Roteiro.AP_Reconhecimento);
            MI_AddParameters("@dsLocal", ITO_Roteiro.AP_Local);
            MI_AddParameters("@dsNomeCredencial", ITO_Roteiro.AP_NomeCredencial);
            MI_AddParameters("@TipoRoteiro", ITO_Roteiro.AP_TipoRoteiro);

            MI_ExecuteNonQuery();
        }

        public void MS_IncluirFotoRoteiro(int prCdRoteiro, byte[] imgPessoa)
        {
            MI_SetSql("UPDATE tblRoteiro SET imgBinary = @imgBinary WHERE cdRoteiro = @cdRoteiro");

            MI_AddParameters("@imgBinary", imgPessoa);
            MI_AddParameters("@cdRoteiro", prCdRoteiro);

            MI_ExecuteNonQuery();
        }

        public byte[] MS_ObterFotoRoteiro(int prcdRoteiro)
        {
            byte[] resultado = new byte[100];
            MI_SetSql("SELECT TOP(1) imgBinary FROM tblRoteiro WHERE cdRoteiro = @cdRoteiro");
            MI_AddParameters("@cdRoteiro", prcdRoteiro);
            MI_ExecuteQuery();
            if (MI_EOF())
            {
            }
            else
            {
                try
                {
                    byte[] result = (byte[])AP_DataReader.GetSqlBinary(0);
                    resultado = result;
                }
                catch
                {
                }
            }

            MI_FecharConexao();

            return resultado;
        }

        public DataTable MS_ObterConfAreaAssento(string prSeminario)
        {
            MI_SetSql("SELECT * FROM tblAreaAssento INNER JOIN tblRoteiro ON tblRoteiro.cdRoteiro = tblAreaAssento.cdRoteiro WHERE tblAreaAssento.dsSeminario = @dsSeminario AND tblAreaAssento.dsSituacao = 'ATIVO' ORDER BY cdAreaAssento");
            MI_AddParameters("@dsSeminario", prSeminario);

            return MI_ExecuteDataSet();
        }

        public string MS_ObterQuantidadeInscritosPorSeminario(string prSeminario)
        {
            MI_SetSql("SELECT COUNT(*) FROM tblInscricao WHERE TipodeInscricao LIKE '%" + prSeminario + "%'");

            return MI_ExecuteDataSet().Rows[0][0].ToString();
        }

        public string MS_ObterQuantidadeInscritosSemReconhecimentoX(string prSeminario, string prRoteiros)
        {
            MI_SetSql("SELECT " +
                        "    COUNT(*) " +
                        "FROM " +
                        "    tblInscricao " +
                        "WHERE " +
                        "    TipodeInscricao LIKE '%" + prSeminario + "%' AND " +
                        "    Consultora NOT IN(SELECT Consultora FROM tblReconhecimento WHERE cdRoteiro IN(0" + prRoteiros + ") AND dsSituacao = 'ATIVO')");

            return MI_ExecuteDataSet().Rows[0][0].ToString();
        }

        public string MS_ObterQuantidadePorRoteiroSeminario(string prSeminario, string prCdRoteiro)
        {
            MI_SetSql("SELECT " +
                        "    COUNT(*) " +
                        "FROM " +
                        "    tblInscricao " +
                        "WHERE " +
                        "    TipodeInscricao LIKE '%" + prSeminario + "%' AND " +
                        "    Consultora IN(SELECT Consultora FROM tblReconhecimento WHERE cdRoteiro IN(" + prCdRoteiro + ") AND dsSituacao = 'ATIVO')");

            return MI_ExecuteDataSet().Rows[0][0].ToString();
        }

        public void MS_ExcluirConfiguracoesAssento(string prSeminario)
        {
            MI_SetSql("UPDATE tblAreaAssento SET dsSituacao = 'CANCELADO', dtCancelamento = GETDATE(), cdUsuarioCancelamento = @cdUsuarioCancelamento WHERE dsSituacao = 'ATIVO' AND dsSeminario = @dsSeminario");

            MI_AddParameters("@cdUsuarioCancelamento", Csv_Variaveis.AP_cdUsuario);
            MI_AddParameters("@dsSeminario", prSeminario);

            MI_ExecuteNonQuery();
        }

        public void MS_IncluirConfAreaAssento(CTO_AreaAssento ITO_AreaAssento)
        {
            MI_SetSql("INSERT tblAreaAssento(cdRoteiro, nuQuantidade, nuPrioridade, dsSeminario, dsSituacao, dsGerador) " +
                      "VALUES(@cdRoteiro, @nuQuantidade, @nuPrioridade, @dsSeminario, 'ATIVO', @dsGerador) ");

            MI_AddParameters("@cdRoteiro", ITO_AreaAssento.AP_cdRoteiro);
            MI_AddParameters("@nuQuantidade", ITO_AreaAssento.AP_nuQuantidade);
            MI_AddParameters("@nuPrioridade", ITO_AreaAssento.AP_nuPrioridade);
            MI_AddParameters("@dsSeminario", ITO_AreaAssento.AP_dsSeminario);
            MI_AddParameters("@dsGerador", ITO_AreaAssento.AP_dsGerador);

            MI_ExecuteNonQuery();
        }

        public DataTable MS_ObterSomenteConsultorasPorSeminarioQueNaoPossuemReconhecimentoX(string prSeminhario, string prRoteiros, int nuRegistrosDesejado)
        {
            MI_SetSql("SELECT " +
                        "    TOP(" + nuRegistrosDesejado + ") " +
                        "    tblInscricao.cdInscricao," +
                        "    tblInscricao.Consultora " +
                        "FROM " +
                        "    tblInscricao " +
                        "WHERE " +
                        "    TipodeInscricao LIKE '%" + prSeminhario + "%' AND " +
                        "    TipodeInscricao NOT LIKE('%Convidados%') AND " +
                        "    tblInscricao.Consultora NOT IN(SELECT Consultora FROM tblReconhecimento WHERE cdRoteiro IN(0" + prRoteiros + ") AND cdRoteiro<> '0' AND tblInscricao.Consultora = tblReconhecimento.Consultora AND dsSituacao = 'ATIVO') " +
                        //"    cdInscricao NOT IN(SELECT cdInscricao FROM tblReconhecimento WHERE cdRoteiro IN(0" + prRoteiros + ") AND tblInscricao.cdInscricao = tblReconhecimento.cdInscricao AND dsSituacao = 'ATIVO') " +
                        "ORDER BY " +
                        "    dtPedido ASC");

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterSomenteAcompanhantePorSeminarioQueNaoPossuemReconhecimentoX(string prSeminhario, string prRoteiros)
        {
            MI_SetSql("SELECT " +
                        "    tblInscricao.cdInscricao," +
                        "    Consultora " +
                        "FROM " +
                        "    tblInscricao " +
                        "WHERE " +
                        "    TipodeInscricao LIKE '%" + prSeminhario + "%' AND " +
                        "    TipodeInscricao LIKE('%Convidados%') AND " +
                        "    Consultora NOT IN(SELECT Consultora FROM tblReconhecimento WHERE cdRoteiro IN(0" + prRoteiros + ") AND cdRoteiro<> '0' AND dsSituacao = 'ATIVO')");
            //"    cdInscricao NOT IN(SELECT cdInscricao FROM tblReconhecimento WHERE cdRoteiro IN(0" + prRoteiros + ") AND dsSituacao = 'ATIVO')");

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterPessoasNoRoteiro(string prSeminario, string prRoteiro)
        {
            MI_SetSql("SELECT " +
                        "    tblReconhecimento.cdReconhecimento, " +
                        "    tblInscricao.Consultora, " +
                        "    tblInscricao.TipodeInscricao, " +
                        "    tblInscricao.NomedaConsultora, " +
                        "    tblInscricao.cdInscricao " +
                        "FROM " +
                        "    tblReconhecimento " +
                        "        INNER JOIN tblInscricao ON " +
                        "            tblInscricao.cdInscricao = tblReconhecimento.cdInscricao " +
                        "WHERE " +
                        "    tblInscricao.TipodeInscricao LIKE '%" + prSeminario + "%' AND " +
                        "    cdRoteiro = @cdRoteiro AND " +
                        "    dsSituacao = 'ATIVO' " +
                        "ORDER BY " +
                        "    tblInscricao.Consultora ");

            MI_AddParameters("@cdRoteiro", prRoteiro);

            return MI_ExecuteDataSet();
        }

        public void MS_AlterarRoteiroPessoa(string prCdReconhecimento, string prCdRoteiroNovo)
        {
            MI_SetSql("UPDATE tblReconhecimento SET cdRoteiro = @cdRoteiro WHERE cdReconhecimento = @cdReconhecimento AND dsSituacao = 'ATIVO'");
            MI_AddParameters("@cdRoteiro", prCdRoteiroNovo);
            MI_AddParameters("@cdReconhecimento", prCdReconhecimento);

            MI_ExecuteNonQuery();
        }

        public DataTable MS_ObterTodasAsConsultoras()
        {
            MI_SetSql("SELECT  " +
                "    cdInscricao, " +
                "    Consultora, " +
                "    TipodeInscricao, " +
                "    NomedaConsultora " +
                "FROM tblInscricao");

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterInscricao(string prNumeroInscricao)
        {
            MI_SetSql("SELECT * FROM tblInscricao WHERE cdInscricao = @cdInscricao");
            MI_AddParameters("@cdInscricao", prNumeroInscricao);

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterReconhecimentoPorConsultora(string prConsultora)
        {
            MI_SetSql("SELECT " +
                    "    tblReconhecimento.cdReconhecimento, " +
                    "    tblRoteiro.dsReconhecimento, " +
                    "    tblRoteiro.TipoRoteiro " +
                    "FROM " +
                    "    tblReconhecimento " +
                    "        INNER JOIN tblRoteiro ON " +
                    "            tblRoteiro.cdRoteiro = tblReconhecimento.cdRoteiro " +
                    "        INNER JOIN tblInscricao ON " +
                    "            tblInscricao.Consultora = tblReconhecimento.Consultora " +
                    "WHERE " +
                    "    tblReconhecimento.dsSituacao = 'ATIVO' AND " +
                    "    tblReconhecimento.Consultora = @Consultora AND " +
                    "    tblInscricao.TipodeInscricao NOT LIKE('%Convidados%') ");

            MI_AddParameters("@Consultora", prConsultora);

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterVersao()
        {
            MI_SetSql("SELECT TOP(1) cdVersao FROM tblVersaoDatabaseManager ORDER BY cdVersao DESC");
            return MI_ExecuteDataSet();
        }
    }
}

