using System;
using System.Data;
using SPI;
using System.Collections.Generic;

public class CAT_CadastrarLogin : CAB_CadastrarLogin
{
     public CAT_CadastrarLogin()
     {
     }
     public override void MS_Incluir(CTO_Login ITO_Login)
     {
          DAO_Login IDAO_Login = new DAO_Login();
          IDAO_Login.MS_Incluir(ITO_Login);
     }
     public override void MS_Alterar(CTO_Login ITO_Login)
     {
         DAO_Login IDAO_Login = new DAO_Login();
         IDAO_Login.MS_Alterar(ITO_Login);
     }
     public override void MS_Excluir(CTO_Login ITO_Login)
     {
         DAO_Login IDAO_Login = new DAO_Login();
         IDAO_Login.MS_Excluir(ITO_Login);
     }
     public override CTO_Login MS_Obter(int pritObjRef)
     {
           DAO_Login IDAO_Login = new DAO_Login();
           return IDAO_Login.MS_Obter(pritObjRef);
     }
     public override CTO_Login MS_Obter(string prstID)
     {
          DAO_Login IDAO_Login = new DAO_Login();
          return IDAO_Login.MS_Obter(prstID);
     }

    public override CTO_Login MS_Logar(string prstUsuario, string prstSenha)
    {
        DAO_Login IDAO_Login = new DAO_Login();
        return IDAO_Login.MS_Logar(prstUsuario, prstSenha);
    }

    public override DataTable MS_ObterUsuarios()
    {
        DAO_Login IDAO_Login = new DAO_Login();
        return IDAO_Login.MS_ObterUsuarios();
    }

    public void MI_ImportarBancoGeral(List<CTO_BancoGeral> lITO_BancoGeral)
    {
        DAO_Login IDAO_Login = new DAO_Login();
        IDAO_Login.MI_ImportarBancoGeral(lITO_BancoGeral);
    }

    public void MI_ExcluirBancoGeral()
    {
        DAO_Login IDAO_Login = new DAO_Login();
        IDAO_Login.MI_ExcluirBancoGeral();
    }

    public void MI_ExcluirRoteiro(string prTipo)
    {
        DAO_Login IDAO_Login = new DAO_Login();
        IDAO_Login.MI_ExcluirRoteiro(prTipo);
    }

    public void MI_ExcluirInscricao(string prTipo)
    {
        DAO_Login IDAO_Login = new DAO_Login();
        IDAO_Login.MI_ExcluirInscricao(prTipo);
    }

    public void MS_RegistrarLog(string prAcao, int prUsuario)
    {
        DAO_Login IDAO_Login = new DAO_Login();
        IDAO_Login.MS_RegistrarLog(prAcao, prUsuario);
    }


}

