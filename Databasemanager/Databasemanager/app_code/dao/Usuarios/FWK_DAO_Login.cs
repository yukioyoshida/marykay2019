using System.Data;
namespace SPI
{
    public class FWK_DAO_Login : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                           "          ObjRef, " +
                           "          NomeLogin, " +
                           "          SenhaLogin, " +
                           "          NomeUsuario, " +
                           "          EmailUsuario, " +
                           "          CelularUsuario, " +
                           "          NivelAcesso ";

        public FWK_DAO_Login()
        {
        }

        public void MS_Incluir(CTO_Login ITO_Login)
        {
            int itObjRef = MS_NovoObjRef();
            MI_SetSql ("INSERT INTO tblUsuarios (" +
                       "          ObjRef, " +
                       "          NomeLogin, " +
                       "          SenhaLogin, " +
                       "          NomeUsuario, " +
                       "          EmailUsuario, " +
                       "          CelularUsuario, " +
                       "          NivelAcesso) " + 
                       " VALUES (" + 
                       "          @ObjRef, " +
                       "          @NomeLogin, " +
                       "          @SenhaLogin, " +
                       "          @NomeUsuario, " +
                       "          @EmailUsuario, " +
                       "          @CelularUsuario, " +
                       "          @NivelAcesso) ");

            MI_AddParameters("@ObjRef", itObjRef);
            MI_AddParameters("@NomeLogin", ITO_Login.getNomeLogin());
            MI_AddParameters("@SenhaLogin", ITO_Login.getSenhaLogin());
            MI_AddParameters("@NomeUsuario", ITO_Login.getNomeUsuario());
            MI_AddParameters("@EmailUsuario", ITO_Login.getEmailUsuario());
            MI_AddParameters("@CelularUsuario", ITO_Login.getCelularUsuario());
            MI_AddParameters("@NivelAcesso", ITO_Login.getNivelAcesso());

            MI_ExecuteNonQuery();
        }

        public void MS_Alterar(CTO_Login ITO_Login)
        {
            MI_SetSql ("UPDATE tblUsuarios SET " +
                       "          ObjRef = @ObjRef, " +
                       "          NomeLogin = @NomeLogin, " +
                       "          SenhaLogin = @SenhaLogin, " +
                       "          NomeUsuario = @NomeUsuario, " +
                       "          EmailUsuario = @EmailUsuario, " +
                       "          CelularUsuario = @CelularUsuario, " +
                       "          NivelAcesso = @NivelAcesso " + 
                       "WHERE ObjRef = @ObjRef ");
            MI_AddParameters("@ObjRef", ITO_Login.getObjRef());
            MI_AddParameters("@NomeLogin", ITO_Login.getNomeLogin());
            MI_AddParameters("@SenhaLogin", ITO_Login.getSenhaLogin());
            MI_AddParameters("@NomeUsuario", ITO_Login.getNomeUsuario());
            MI_AddParameters("@EmailUsuario", ITO_Login.getEmailUsuario());
            MI_AddParameters("@CelularUsuario", ITO_Login.getCelularUsuario());
            MI_AddParameters("@NivelAcesso", ITO_Login.getNivelAcesso());

            MI_ExecuteNonQuery();
        }

        public void MS_Excluir(CTO_Login ITO_Login)
        {
            MI_SetSql ("DELETE FROM tblUsuarios WHERE ObjRef = @ObjRef");
            MI_AddParameters("@ObjRef", ITO_Login.getObjRef());
            
            MI_ExecuteNonQuery();
        }

        public CTO_Login MS_Obter(int pritObjRef)
        {
            CTO_Login ITO_Login = new CTO_Login();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          NomeLogin, " +
                       "          SenhaLogin, " +
                       "          NomeUsuario, " +
                       "          EmailUsuario, " +
                       "          CelularUsuario, " +
                       "          NivelAcesso " + 
                       "FROM tblUsuarios " + 
                       "WHERE Objref = @ObjRef ");
            MI_AddParameters("@Objref", pritObjRef);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Login = null;
            else
                ITO_Login = MI_DataSetToEntidade();
            return ITO_Login;
        }

        public CTO_Login MI_DataSetToEntidade()
        {
            CTO_Login ITO_Login = new CTO_Login();            
            try
            {
                ITO_Login.setObjRef(AP_DataReader.GetInt32(0));
            }
            catch
            {
                ITO_Login.setObjRef(0);
            }
            try
            {
                ITO_Login.setNomeLogin(AP_DataReader.GetString(1));
            }
            catch
            {
                ITO_Login.setNomeLogin("");
            }
            try
            {
                ITO_Login.setSenhaLogin(AP_DataReader.GetString(2));
            }
            catch
            {
                ITO_Login.setSenhaLogin("");
            }
            try
            {
                ITO_Login.setNomeUsuario(AP_DataReader.GetString(3));
            }
            catch
            {
                ITO_Login.setNomeUsuario("");
            }
            try
            {
                ITO_Login.setEmailUsuario(AP_DataReader.GetString(4));
            }
            catch
            {
                ITO_Login.setEmailUsuario("");
            }
            try
            {
                ITO_Login.setCelularUsuario(AP_DataReader.GetString(5));
            }
            catch
            {
                ITO_Login.setCelularUsuario("");
            }
            try
            {
                ITO_Login.setNivelAcesso(AP_DataReader.GetString(6));
            }
            catch
            {
                ITO_Login.setNivelAcesso("");
            }
            return ITO_Login;
        }

        public CTO_Login MS_ObterTodos()
        {
            CTO_Login ITO_Login = new CTO_Login();
            MI_SetSql ("SELECT " + 
                       "          ObjRef, " +
                       "          NomeLogin, " +
                       "          SenhaLogin, " +
                       "          NomeUsuario, " +
                       "          EmailUsuario, " +
                       "          CelularUsuario, " +
                       "          NivelAcesso " + 
                       "FROM tblUsuarios " );
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Login = null;
            else
                ITO_Login = MI_DataSetToEntidade();
            return ITO_Login;
        }

        public CTO_Login MS_ObterProximo()
        {
            CTO_Login ITO_Login = new CTO_Login();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_Login = null;
            else
                ITO_Login = MI_DataSetToEntidade();
                return ITO_Login;
        }

        public int MS_NovoObjRef()
        {
            int itObjRef;
            MI_SetSql("Select Max(ObjRef) from tblUsuarios");
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 1;
            }
            else
            {
                itObjRef = AP_DataReader.GetInt32(0);
                itObjRef++;
            }
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblUsuarios");
            return MI_ExecuteDataSet();
        }



    }
}

