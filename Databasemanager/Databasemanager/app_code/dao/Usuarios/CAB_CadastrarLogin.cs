using System;
using System.Data;
using SPI;

public abstract class CAB_CadastrarLogin : ICadastrarLogin
{
     public abstract void MS_Incluir(CTO_Login ITO_Login);
     public abstract void MS_Alterar(CTO_Login ITO_Login);
     public abstract void MS_Excluir(CTO_Login ITO_Login);
     public abstract CTO_Login MS_Obter(int pritObjRef);
     public abstract CTO_Login MS_Obter(string prstID);
     public abstract CTO_Login MS_Logar(string prstUsuario, string prstSenha);
     public abstract DataTable MS_ObterUsuarios();
}

