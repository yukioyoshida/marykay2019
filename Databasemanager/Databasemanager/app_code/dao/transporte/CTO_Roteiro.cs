﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPI;
namespace SPI
{
    public class CTO_Roteiro
    {
        public int    AP_cdRoteiro { get; set; }
        public string AP_Reconhecimento { get; set; }
        public string AP_Local { get; set; }
        public string AP_NomeCredencial { get; set; }
        public string AP_TipoRoteiro { get; set; }
        public string AP_TipoJoia { get; set; }
        public string AP_Dia { get; set; }
        public string AP_dsCodigoBarrasProduto { get; set; }
        public string AP_dsQuantidadeEstoque { get; set; }
    }
}
