﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPI;
namespace SPI
{
    public class CTO_BancoGeral
    {
        public string AP_NrConsultora { get; set; }
        public string AP_NmConsultora { get; set; }
        public string AP_CPF { get; set; }
        public string AP_Branco { get; set; }
        public string AP_NmNSN { get; set; }
        public string AP_NivelDeCarreira { get; set; }
    }
}
