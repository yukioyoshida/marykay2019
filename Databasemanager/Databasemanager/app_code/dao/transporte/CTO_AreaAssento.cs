﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPI;
namespace SPI
{
    public class CTO_AreaAssento
    {
        public int  AP_cdAreaAssento { get; set; }
        public int  AP_cdRoteiro { get; set; }
        public int  AP_nuQuantidade { get; set; }
        public int  AP_nuPrioridade { get; set; }
        public string AP_dsSeminario { get; set; }
        public string AP_dsGerador { get; set; }
    }
}
