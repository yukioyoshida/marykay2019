﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPI;
namespace SPI
{
    public class CTO_Reconhecimento
    {
        public int AP_cdUsuario { get; set; }
        public int AP_cdInscricao { get; set; }
        public string AP_Consultora { get; set; }
        public int AP_cdRoteiro { get; set; }
        public string AP_dsMetodoInsert { get; set; }
    }
}
