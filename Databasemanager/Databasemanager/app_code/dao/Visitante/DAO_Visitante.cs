using Dapper;
using System;
using System.Data;
namespace SPI
{
    public class DAO_Visitante : FWK_DAO_Visitante
    {
        public DAO_Visitante()
        {
        }

        public DataTable MS_ObterRegistroGrid(string prstChave)
        {   
            MI_SetSql(AP_SelectALL +
                      "FROM tblVisitante                                                       " +
                      "WHERE CodigoBarras Like @ID                                         or  " +
                      "NomeCompleto Like @ID                                               or  " +
                      "Localizese Like @ID                                                 or  " +
                      "CPF like @ID ");
            MI_AddParameters("@ID", prstChave+"%");
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterCampos()
        {
            MI_SetSql("SHOW FIELDS FROM tblvisitante");
            return MI_ExecuteDataSet();
        }

        public void MS_ConfirmaImpressao(Int64 prstCodigo)
        {
            MI_SetSql("Update tblVisitante set  " +
                      "EmissaoCracha = @Valor,  " +
                      "PresenteEvento = @Valor  " +
                      "Where CodigoBarras = @CodigoBarras  ");
            MI_AddParameters("@Valor", "S");
            MI_AddParameters("@CodigoBarras", prstCodigo);
            MI_ExecuteQuery();
            MI_FecharConexao();
        }

        public DataTable MS_ObterRegistro(string prstPalavra, string prContendo, string stTipoBusca)
        {
            MI_SetSql("Select CodigoBarras as 'CODIGO',   " +
                      "CPF,                               " +
                      "NomeCompleto,                      " +
                      "RazaoSocial as 'EMPRESA',          " +
                      "NomeFantasia as 'FANTASIA',        " +
                      "Cargo as 'CARGO',                  " +
                      "Res1 AS 'Cod. Consultora',         " +
                      "Res5 AS 'Conjuge'                  " +
                      "From tblVisitante Where            " +
                      "" + stTipoBusca + " like @Valor OR " +
                      "Localizese = @Valor ");
            if (prContendo == "1")
            {
                MI_AddParameters("@Valor", "%" + prstPalavra + "%");
            }
            else
            {
                MI_AddParameters("@Valor", prstPalavra + "%");
            }
            return MI_ExecuteDataSet();
        }

        public string MS_GerarCodigoBarras()
        {
            Int64 itCodigoBarras = 0;
            MI_SetSql("Select CAST(MAX(Chave) AS INT)Valor From tblInscricao");
            MI_ExecuteQuery();
            try
            {
                var TESTE = AP_DataReader.GetInt32(0);
                itCodigoBarras = Convert.ToInt64(AP_DataReader.GetInt32(0));
                itCodigoBarras++;
            }
            catch
            {
                itCodigoBarras = 7000000;
            }
            MI_FecharConexao();
            return itCodigoBarras.ToString();
        }

        public DataTable MS_LocalizarRegistroPorFiltro(string prstCriterio, string prstAtributo)
        {
            MI_SetSql("SELECT " +
           "          NumeroTerminal, " +
           "          Chave, " +
           "          CodigoBarras, " +
           "          EmissaoCracha, " +
           "          PresenteDia1, " +
           "          PresenteDia2, " +
           "          PresenteDia3, " +
           "          PresenteDia4, " +
           "          PresenteDia5, " +
           "          OrigemInsercao, " +
           "          DataOrigemInsercao, " +
           "          HoraOrigemInsercao, " +
           "          UsuarioOrigemInsercao, " +
           "          OrigemAlteracao, " +
           "          DataOrigemAlteracao, " +
           "          HoraOrigemAlteracao, " +
           "          UsuarioOrigemAlteracao, " +
           "          MetodoInsercao, " +
           "          ParticipouEdicao, " +
           "          CategoriaRegistro, " +
           "          OrigemConvite, " +
           "          EmailLogin, " +
           "          CPF, " +
           "          Senha, " +
           "          Tratamento, " +
           "          Nome, " +
           "          Sobrenome, " +
           "          NomeCompleto, " +
           "          NomeCracha, " +
           "          Profissao, " +
           "          Cargo, " +
           "          Sexo, " +
           "          DiaNascimento, " +
           "          MesNascimento, " +
           "          AnoNascimento, " +
           "          DDICelular, " +
           "          DDDCelular, " +
           "          TelefoneCelular, " +
           "          SmsCelularPessoal, " +
           "          MmsCelularPessoal, " +
           "          EmailPessoal, " +
           "          AutorizaEnvioEmailPessoal, " +
           "          NomeEnderecoPessoal, " +
           "          NumeroEnderecoPessoal, " +
           "          ComplementoEnderecoPessoal, " +
           "          CepEnderecoPessoal, " +
           "          BairroEnderecoPessoal, " +
           "          CidadeEnderecoPessoal, " +
           "          EstadoEnderecoPessoal, " +
           "          PaisEnderecoPessoal, " +
           "          DDITelefonePessoal, " +
           "          DDDTelefonePessoal, " +
           "          NumeroTelefonePessoal, " +
           "          DDIFaxPessoal, " +
           "          DDDFaxPessoal, " +
           "          NumeroFaxPessoal, " +
           "          RazaoSocial, " +
           "          NomeFantasia, " +
           "          EmpresaCracha, " +
           "          Cnpj, " +
           "          NomeEnderecoComercial, " +
           "          NumeroEnderecoComercial, " +
           "          ComplementoEnderecoComercial, " +
           "          CepEnderecoComercial, " +
           "          BairroEnderecoComercial, " +
           "          CidadeEnderecoComercial, " +
           "          EstadoEnderecoComercial, " +
           "          PaisEnderecoComercial, " +
           "          DDITelefoneComercial, " +
           "          DDDTelefoneComercial, " +
           "          NumeroTelefoneComercial, " +
           "          DDIFaxComercial, " +
           "          DDDFaxComercial, " +
           "          NumeroFaxComercial, " +
           "          PaginaWebComercial, " +
           "          EmailComercial, " +
           "          AutorizaEnvioEmailComercial, " +
           "          Res1, " +
           "          Res2, " +
           "          Res3, " +
           "          Res4, " +
           "          Res5, " +
           "          ObjRef, " +
           "          Departamento, " +
           "          TipoEnderecoPessoal, " +
           "          InscricaoEstadual, " +
           "          TipoEnderecoComercial, " +
           "          Pergunta1, " +
           "          Pergunta1Outro, " +
           "          Pergunta2, " +
           "          Pergunta2Outro, " +
           "          Pergunta3, " +
           "          Pergunta3Outro, " +
           "          Pergunta4, " +
           "          Pergunta4Outro, " +
           "          Pergunta5, " +
           "          Pergunta5Outro, " +
           "          Pergunta6, " +
           "          Pergunta6Outro, " +
           "          Pergunta7, " +
           "          Pergunta7Outro, " +
           "          Pergunta8, " +
           "          Pergunta8Outro, " +
           "          Pergunta9, " +
           "          Pergunta9Outro, " +
           "          Pergunta10, " +
           "          Pergunta10Outro, " +
           "          Pergunta11, " +
           "          Pergunta11Outro, " +
           "          Pergunta12, " +
           "          Pergunta12Outro, " +
           "          Pergunta13, " +
           "          Pergunta13Outro, " +
           "          Pergunta14, " +
           "          Pergunta14Outro, " +
           "          Pergunta15, " +
           "          Pergunta15Outro, " +
           "          Pergunta16, " +
           "          Pergunta16Outro, " +
           "          Pergunta17, " +
           "          Pergunta17Outro, " +
           "          Pergunta18, " +
           "          Pergunta18Outro, " +
           "          Pergunta19, " +
           "          Pergunta19Outro, " +
           "          Pergunta20, " +
           "          Pergunta20Outro " +
           "FROM tblVisitante " +
           "WHERE " + prstCriterio + " LIKE @VALOR  ");
            MI_AddParameters("@VALOR", prstAtributo+"%");
            return MI_ExecuteDataSet();
        }


        public CTO_Visitante MS_ObterPorCodigo(string prstCodigo)
        {
            if (prstCodigo == null) prstCodigo = "";
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql(AP_SelectALLFirst +
            "FROM tblVisitante " +
            "WHERE CodigoBarras = @CodigoBarras ");
            MI_AddParameters("@CodigoBarras", prstCodigo);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Visitante = null;
            else
                ITO_Visitante = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Visitante;
        }

        public DataTable MS_ObterPorCodigoData(string prstCodigo)
        {
            MI_SetSql("Select top(1) ObjRef, NomeCompleto, CodigoBarras, Res1  from tblVisitante Where CodigoBarras = @CodigoBarras ");
            MI_AddParameters("@CodigoBarras", prstCodigo);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterPorCodigoDataConsultora(string prstCodigo)
        {
            MI_SetSql("Select top(1) ObjRef, NomeCompleto, CodigoBarras, Res1  from tblVisitante Where CodigoBarras = @CodigoBarras AND Res5 <> 'C�njuge'");
            MI_AddParameters("@CodigoBarras", prstCodigo);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterPorCodigoConsultoraImportacao(string stNrConsultora, string prNomeCompleto, string TipoInscricao)
        {
            if (prNomeCompleto == "")
            {
                MI_SetSql("Select top(2) ObjRef, NomeCompleto, Res1, CodigoBarras  from tblVisitante Where Res1 = @Res1 AND Pergunta15 = @Pergunta15 ");
                MI_AddParameters("@Res1", stNrConsultora);
            }
            else
            {
                MI_SetSql("Select top(2) ObjRef, NomeCompleto, Res1, CodigoBarras  from tblVisitante Where Res1 = @Res1 AND NomeCompleto = @NomeCompleto  AND Pergunta15 = @Pergunta15");
                MI_AddParameters("@Res1", stNrConsultora);
                MI_AddParameters("@NomeCompleto", prNomeCompleto);
                MI_AddParameters("@Pergunta15", TipoInscricao);
            }

            return MI_ExecuteDataSet();    
        }

        public DataTable MS_ObterPorCodigoConsultora(string stNrConsultora, string prNomeCompleto)
        {
            if (prNomeCompleto == "")
            {
                MI_SetSql("Select top(2) ObjRef, NomeCompleto, Res1, CodigoBarras  from tblVisitante Where Res1 = @Res1 AND  Res5 <> 'C�njuge'");
                MI_AddParameters("@Res1", stNrConsultora);
            }
            else
            {
                MI_SetSql("Select top(2) ObjRef, NomeCompleto, Res1, CodigoBarras  from tblVisitante Where Res1 = @Res1 AND NomeCompleto = @NomeCompleto AND Res5 <> 'C�njuge'");
                MI_AddParameters("@Res1", stNrConsultora);
                MI_AddParameters("@NomeCompleto", prNomeCompleto);
            }
            
            return MI_ExecuteDataSet();        
        }

        public DataTable MS_ObterPorCodigoConsultoraConjuge(string stNrConsultora, string prNomeCompleto)
        {
            if (prNomeCompleto == "")
            {
                MI_SetSql("Select top(2) ObjRef, NomeCompleto, Res1  from tblVisitante Where Res1 = @Res1 AND Res5 = 'C�njuge' ");
                MI_AddParameters("@Res1", stNrConsultora);
            }
            else
            {
                MI_SetSql("Select top(2) ObjRef, NomeCompleto, Res1  from tblVisitante Where Res1 = @Res1 AND NomeCompleto = @NomeCompleto AND Res5 = 'C�njuge' ");
                MI_AddParameters("@Res1", stNrConsultora);
                MI_AddParameters("@NomeCompleto", prNomeCompleto);
            }
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterPremiacoes(string stNrConsultora)
        {
            MI_SetSql("SELECT     " +
                        "    *    " +
                        "FROM     " + 
                        "    tblPremioConsultora                        " +
                        //"        LEFT JOIN tbllistapremiacoes ON        " +
                        //"            tblPremioConsultora.dsTrocaLocal = tbllistapremiacoes.cdPremio " +
                        "WHERE                                          " +
                        "    tblPremioConsultora.NrConsultora = @NrConsultora AND dsReconhecimento <> 'Faixas das Cortes'");

            MI_AddParameters("@NrConsultora", stNrConsultora);
            return MI_ExecuteDataSet();          
        }

        internal void MS_AprovarBase(string stUsuarioAprovacao)
        {
            var strQuery = "UPDATE tblInscricao set BaseAprovada = 'S', dtBaseAprovada = GETDATE(), UsuarioAprovacaoBase = @UsuarioAprovacao";
            var cnn = AP_Connection.Execute(strQuery, new { @UsuarioAprovacao = stUsuarioAprovacao });
        }

        public DataTable MS_ObterPremiacoesMimo(string stNrConsultora)
        {
            MI_SetSql("SELECT                         " +
                        "    DISTINCT(Descricao)      " +
                        "FROM                         " +
                        "    tblPremioConsultora      " +
                        "        INNER JOIN tblPremioConsultora_Item ON                                         " +
                        "            tblPremioConsultora.ObjRef = tblPremioConsultora_Item.cdReconhecimento     " +
                        "        INNER JOIN tbllistapremiacoes ON                                               " +
                        "            tbllistapremiacoes.cdPremio = tblPremioConsultora_Item.dsTrocaLocal        " +
                        "WHERE                                                                                  " +    
                        "    tblPremioConsultora.NrConsultora = @NrConsultora AND                               " +
                        "    tblPremioConsultora_Item.dsTipo = 'MIMO'                                           ");    

            MI_AddParameters("@NrConsultora", stNrConsultora);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterReconhecimentos(string stCodigoBarras)
        {
            MI_SetSql("SELECT     " +
                        "    DISTINCT(dsReconhecimento),  " +
                        "    ObjRef                " +
                        "FROM                             " +
                        "    tblPremioConsultora          " +
                        //"        LEFT JOIN tbllistapremiacoes ON " +
                        //"            tblPremioConsultora.dsTrocaLocal = tbllistapremiacoes.cdPremio " +
                        "WHERE " +
                        "    tblPremioConsultora.CodigoBarras = @CodigoBarras AND dsReconhecimento <> 'Faixas das Cortes'");

            MI_AddParameters("@CodigoBarras", stCodigoBarras);
            return MI_ExecuteDataSet();
        }

        public CTO_Visitante MS_ObterPorCPF(string prstCPF)
        {
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql (AP_SelectALL + 
            "FROM tblVisitante " +
            "WHERE CPF = @CPF    ");
            MI_AddParameters("@CPF", prstCPF);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Visitante = null;
            else
                ITO_Visitante = MI_DataSetToEntidade();
            
            MI_FecharConexao();
            return ITO_Visitante;
        }

        public DataTable MS_ObterPorCPFTabela(string prstCPF)
        {
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql(AP_SelectALL +
            "FROM tblVisitante " +
            "WHERE CPF = @CPF  ");
            MI_AddParameters("@CPF", prstCPF);
            return MI_ExecuteDataSet();
        }

        public CTO_Visitante MS_ObterPorLocalizese(string prstLocalizese)
        {
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql(AP_SelectALL +
            "FROM tblVisitante " +
            "WHERE Localizese = @Localizese ");
            MI_AddParameters("@Localizese", prstLocalizese);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Visitante = null;
            else
                ITO_Visitante = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Visitante;
        }

        public DataTable MS_ObterPorLocalizeseTabela(string prstLocalizese)
        {
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql(AP_SelectALL +
            "FROM tblVisitante " +
            "WHERE Localizese = @Localizese ");
            MI_AddParameters("@Localizese", prstLocalizese);
            return MI_ExecuteDataSet();
        }

        public CTO_Visitante MS_ObterPorNome(string prstNome)
        {
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql(AP_SelectALL +
            "FROM tblVisitante           " +
            "WHERE NomeCompleto = @NOME ");
            MI_AddParameters("@NOME", prstNome);
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Visitante = null;
            else
                ITO_Visitante = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Visitante;
        }

        public DataTable MS_ObterPorNomeTabela(string prstNome)
        {
            MI_SetSql(AP_SelectALL +
            "FROM tblVisitante           " +
            "WHERE NomeCompleto = @NOME ");
            MI_AddParameters("@NOME", prstNome);
            return MI_ExecuteDataSet();
        }

        public CTO_Visitante MS_ObterPorEmail(string prstEmail)
        {
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql (AP_SelectALL + 
            "FROM tblVisitante " +
            "WHERE EmailLogin = @EmailLogin ");
            MI_AddParameters("@EmailLogin", prstEmail);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Visitante = null;
            else
                ITO_Visitante = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Visitante;
        }

        public DataTable MS_ObterCategorias()
        {
            MI_SetSql("Select * from tblCategorias Order by Categorias");
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterFormaPagamento()
        {
            MI_SetSql("Select * from tblformapagamento Order by FormaPagamento");
            return MI_ExecuteDataSet();
        }

        public DataTable MS_VerificaQtdCNPJ(string stCNPJ)
        {
            MI_SetSql("Select count(CPF) from tblVisitante Where CNPJ = @CNPJ");
            MI_AddParameters("@CNPJ", stCNPJ);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_Excessoes(string prCNPJ)
        {
            MI_SetSql("SELECT * FROM tblqtdcnpj WHERE " +
                      "CNPJ = @CNPJ                   ");
            MI_AddParameters("@CNPJ", prCNPJ);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterEvento()
        {
            MI_SetSql("Select * from tblevento Order by Evento");
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterTodosParticipantesCertificados(int stLimite)
        {
            MI_SetSql(" SELECT top(@Limite) *                                                                      " +
                        " FROM TBLVISITANTE, TBLPEDIDO                                                  " +
                        " WHERE (TBLVISITANTE.PERGUNTA20 = '' OR TBLVISITANTE.PERGUNTA20 IS NULL OR TBLVISITANTE.PERGUNTA20 = 'N') AND   " +
                        " TBLVISITANTE.OBJREF = TBLPEDIDO.CDCLIENTE AND                                 " +
                        " TBLPEDIDO.DSSITUACAO = 'PAGO' AND                                             " +
                        " TBLVISITANTE.OBJREF NOT IN (SELECT CDCLIENTE FROM TBLPEDIDO WHERE CDCLIENTE = TBLVISITANTE.OBJREF AND (DSSITUACAO <> 'PAGO')) " +
                        " GROUP BY TBLVISITANTE.CODIGOBARRAS                                            " +
                        " ORDER BY TBLVISITANTE.NOMECRACHA                                              ");
            MI_AddParameters("@Limite", stLimite);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterSenha(string stSenha)
        {
            MI_SetSql("SELECT SENHA FROM TBLSENHA");
            return MI_ExecuteDataSet();
        }

        // ===================================================================================================

        public DataTable MS_ObterProximoImportacao()
        {
            //MI_SetSql("Select top(1) * from _azulbranco WHERE Importado = 'N'  ");
            MI_SetSql("Select top(1) * from _verdeamarelo WHERE Importado = 'N' ");
            return MI_ExecuteDataSet();
        }

        public void MI_ConfirmarImportacao(string prCodigoBarras)
        {
            //MI_SetSql("Update _azulbranco set " + "Importado = @Valor  " +  "Where CodigoBarras = @CodigoBarras  ");
            MI_SetSql("Update _verdeamarelo set " + "Importado = @Valor  " + "Where CodigoBarras = @CodigoBarras  ");


            MI_AddParameters("@Valor", "S");
            MI_AddParameters("@CodigoBarras", prCodigoBarras);
            MI_ExecuteQuery();
            MI_FecharConexao();            
        }

        public DataTable MS_ObterPremioDaConsultora(string cdPremiacao)
        {
            MI_SetSql("Select * from tblpremiacaoconsultora WHERE ObjRef = @ObjRef ");

            MI_AddParameters("@ObjRef", cdPremiacao);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterPremio(string cdPremio)
        {
            MI_SetSql("Select PREMIO.*, (SELECT Descricao FROM tbllistapremiacoes WHERE cdPremio = PREMIO.VinculadoCom) AS Vinculado FROM tbllistapremiacoes AS PREMIO WHERE PREMIO.cdPremio = @cdPremio ");

            MI_AddParameters("@cdPremio", cdPremio);
            return MI_ExecuteDataSet();
        }
        
        
        public void MS_ConfirmarRetiradaPremio(string cdPremiacao, string prUsuario)
        {

            MI_SetSql("Update tblpremiacaoconsultora SET dsRetirado = 'S', DataRetirado = Now(), UsuarioOperacao = @Usuario WHERE ObjRef = @ObjRef  ");

            MI_AddParameters("@ObjRef", cdPremiacao);
            MI_AddParameters("@Usuario", prUsuario);
            MI_ExecuteQuery();
            MI_FecharConexao();  
        }

        public void MS_ConfirmarRetiradaPremioComEscolha(string cdPremiacao, string prUsuario, string prPremio)
        {

            MI_SetSql("Update tblpremiacaoconsultora SET dsRetirado = 'S', DataRetirado = Now(), UsuarioOperacao = @Usuario, dsEscolha = @dsEscolha WHERE ObjRef = @ObjRef  ");

            MI_AddParameters("@ObjRef", cdPremiacao);
            MI_AddParameters("@Usuario", prUsuario);
            MI_AddParameters("@dsEscolha", prPremio);
            MI_ExecuteQuery();
            MI_FecharConexao();          
        }

        public void MS_ConfirmarEsconhaDoPremio(string cdPremiacao, string prUsuario, string prPremio)
        {

            MI_SetSql("Update tblpremiacaoconsultora SET dsRetirado = 'E', DataRetirado = Now(), UsuarioOperacao = @Usuario, dsEscolha = @dsEscolha WHERE ObjRef = @ObjRef  ");

            MI_AddParameters("@ObjRef", cdPremiacao);
            MI_AddParameters("@Usuario", prUsuario);
            MI_AddParameters("@dsEscolha", prPremio);
            MI_ExecuteQuery();
            MI_FecharConexao();
        }

        public void MS_CancelarCodigosPararelos(string prCodigoBarras)
        {
            MI_SetSql("UPDATE tblCodigoBarrasImpressao SET Situacao = 'CANCELADO' WHERE CodigoBarrasOriginal = @CodigoBarrasOriginal ");

            MI_AddParameters("@CodigoBarrasOriginal", prCodigoBarras);
            MI_ExecuteQuery();
            MI_FecharConexao();
        }

        public DataTable MS_ObterQuantidadeImpressoes(string prCodigoOriginal)
        {
            MI_SetSql("SELECT COUNT(*)QT FROM tblCodigoBarrasImpressao WHERE CodigoBarrasOriginal = @CodigoBarrasOriginal ");

            MI_AddParameters("@CodigoBarrasOriginal", prCodigoOriginal);
            return MI_ExecuteDataSet();
        }

        public void MS_GravarCodigoPararelo(string prCodigoOriginal, string stCodigoPararelo)
        {
            MI_SetSql("INSERT tblCodigoBarrasImpressao (CodigoBarrasOriginal, CodigoBarrasImpresso, Situacao) VALUES (@CodigoBarrasOriginal, @CodigoBarrasPararelo, 'ATIVO') ");

            MI_AddParameters("@CodigoBarrasOriginal", prCodigoOriginal);
            MI_AddParameters("@CodigoBarrasPararelo", stCodigoPararelo);
            MI_ExecuteQuery();
            MI_FecharConexao();
        }

        public DataTable MS_ObterPremiosDaConsultora(string prCdReconhecimento, string prTipo)
        {
            MI_SetSql("SELECT * FROM tblPremioConsultora_Item WHERE cdReconhecimento = @cdReconhecimento AND dsTipo = @dsTipo ");

            MI_AddParameters("@cdReconhecimento", prCdReconhecimento);
            MI_AddParameters("@dsTipo", prTipo);

            return MI_ExecuteDataSet();
        }

        public void MS_EscolherJoiaLocal(string prCdItemJoia, string prJoia)
        {
            MI_SetSql("  UPDATE                               " +
                        "    tblPremioConsultora_Item         " +
                        "SET                                  " +
                        "    dsTrocaLocal = @Joia,            " +
                        "    dtTroca = GETDATE(),             " +
                        "    UsuarioTroca = @Usuario          " +
                        "WHERE                                " +
                        "    cdItem = @cdItem ");

            MI_AddParameters("@Joia", prJoia);
            MI_AddParameters("@Usuario", Databasemanager.Csv_Variaveis.AP_cdUsuario.ToString());
            MI_AddParameters("@cdItem", prCdItemJoia);

            MI_ExecuteQuery();
            MI_FecharConexao();
        }

        public DataTable MS_ObterReconhecimentosEspecifico(string prCdItemJoia)
        {
            MI_SetSql("SELECT                                           " +
                        "    *                                          " +
                        "FROM                                           " +
                        "    tblPremioConsultora_Item                   " +
                        "WHERE                                          " +
                        "    cdItem = @cdItem       ");

            MI_AddParameters("@cdItem", prCdItemJoia);
            return MI_ExecuteDataSet();
        }

        public void MS_SalvarObservacoes(string prCdItemJoia, string prObservacoes)
        {
            MI_SetSql("UPDATE tblPremioConsultora_Item SET dsObservacoes = @dsObservacoes WHERE cdItem = @cdItem ");

            MI_AddParameters("@cdItem", prCdItemJoia);
            MI_AddParameters("@dsObservacoes", prObservacoes);
            MI_ExecuteQuery();
            MI_FecharConexao();
        }

        public void MS_ConfirmarRetiradaPremioNew(string prCdItemJoia)
        {
            MI_SetSql("UPDATE tblPremioConsultora_Item SET dsRetirado = 'S', dtRetirado = GETDATE(), UsuarioEntregue = @UsuarioEntregue WHERE cdItem = @cdItem ");

            MI_AddParameters("@cdItem", prCdItemJoia);
            MI_AddParameters("@UsuarioEntregue", Databasemanager.Csv_Variaveis.AP_cdUsuario.ToString());

            MI_ExecuteQuery();
            MI_FecharConexao();
        }

        public DataTable MS_ObterPremioNew(string prCdJoia)
        {
            MI_SetSql("SELECT                                           " +
                        "    *                                          " +
                        "FROM                                           " +
                        "    tbllistapremiacoes                         " +
                        "WHERE                                          " +
                        "    cdPremio = @cdPremio                       ");

            MI_AddParameters("@cdPremio", prCdJoia);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterItemPremio(string prCdItem)
        {
            MI_SetSql("SELECT                                           " +
                        "    *                                          " +
                        "FROM                                           " +
                        "    tblPremioConsultora_Item                         " +
                        "WHERE                                          " +
                        "    cdItem = @cdItem                       ");

            MI_AddParameters("@cdItem", prCdItem);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterCodigoCredencial(string prCodigoBarras)
        {
            MI_SetSql("SELECT TOP(10) * FROM tblCodigoBarrasImpressao WHERE CodigoBarrasImpresso = @CodigoBarrasImpresso ");
            MI_AddParameters("@CodigoBarrasImpresso", prCodigoBarras);

            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterTodosOsItens(string prCodigoBarras)
        {
            MI_SetSql("SELECT       " +
                        "    *      " +
                        "FROM       " +
                        "    tblPremioConsultora                                                                 " +
                        "        INNER JOIN tblPremioConsultora_Item ON                                          " +   
                        "            tblPremioConsultora.ObjRef = tblPremioConsultora_Item.cdReconhecimento      " +
                        "        LEFT JOIN tbllistapremiacoes ON                                                 " +
                        "            tbllistapremiacoes.cdPremio = tblPremioConsultora_Item.dsTrocaLocal         " +
                        "WHERE                                                                                   " +
                        "    CodigoBarras = @CodigoBarras AND                                                    " +
                        "    tblPremioConsultora.dsReconhecimento <> 'Faixas das Cortes' AND                      " +
                        "    tblPremioConsultora_Item.dsRetirado = 'S'                                           ");
            MI_AddParameters("@CodigoBarras", prCodigoBarras);

            return MI_ExecuteDataSet();
        }

        public void MS_IncluirReconhecimento(string prNrConsultora, string prReconhecimento)
        {
            MI_SetSql("INSERT tblPremioConsultora (NrConsultora, dsReconhecimento, ATIVO) VALUES (@NrConsultora, @dsReconhecimento, 'S')");

            MI_AddParameters("@NrConsultora", prNrConsultora);
            MI_AddParameters("@dsReconhecimento", prReconhecimento);

            MI_ExecuteNonQuery();
        }

        public void MS_IncluirReconhecimento_Item(string prReconhecimento, string stPremio)
        {
            string stTipo = string.Empty;

            if (stPremio == string.Empty)
            {
                stTipo = "PREMIO";
            }
            else
            {
                stTipo = new DAO_Visitante().MS_ObterRoteiroPorIdRoteiro(stPremio).Rows[0]["TipoJoia"].ToString().ToUpper();
            }

            MI_SetSql("INSERT tblPremioConsultora_Item (cdReconhecimento, dsEscolhaPre, dsTrocaLocal, dsRetirado, dsTipo) VALUES (@cdReconhecimento, @dsEscolhaPre, @dsTrocaLocal, @dsRetirado, @dsTipo)");

            MI_AddParameters("@cdReconhecimento", prReconhecimento);
            MI_AddParameters("@dsEscolhaPre", stPremio);
            MI_AddParameters("@dsTrocaLocal", stPremio);
            MI_AddParameters("@dsRetirado", "N");
            MI_AddParameters("@dsTipo", stTipo);

            MI_ExecuteNonQuery();
        }

        public DataTable MS_ObterUltimoReconhecimentoInserido(string prNrConsultora, string prReconhecimento)
        {
            MI_SetSql("SELECT TOP (1) ObjRef FROM tblPremioConsultora WHERE NrConsultora = @NrConsultora AND dsReconhecimento = @dsReconhecimento ORDER BY ObjRef DESC");

            MI_AddParameters("@NrConsultora", prNrConsultora);
            MI_AddParameters("@dsReconhecimento", prReconhecimento);
          

            return MI_ExecuteDataSet();
        }

        public void MS_IncluirItemNaListaDePremios(string prNome)
        {
            MI_SetSql("INSERT tbllistapremiacoes (cdPremio,Descricao,dsTipo,dsFoto,Estoque) VALUES ((SELECT ISNULL(MAX(cdPremio+1), 1) FROM tbllistapremiacoes), '" + prNome + "', 'MIMO', '', 100)");
            MI_ExecuteNonQuery();
        }

        public DataTable MS_ObterPremioPorNome(string prNome)
        {
            MI_SetSql("SELECT * FROM tblRoteiro WHERE dsReconhecimento = @Descricao AND TipoRoteiro = 'J'");

            MI_AddParameters("@Descricao", prNome);

            return MI_ExecuteDataSet();
        }

        public void MS_TrocarJoia(string prCdPremio, string prCdJoia)
        {
            MI_SetSql("UPDATE tblPremioConsultora_Item SET dsTrocaLocal = @cdJoia, dtTroca = GETDATE(), UsuarioTroca = @UsuarioTroca WHERE cdItem = @cdItem");

            MI_AddParameters("@cdJoia", prCdJoia);
            MI_AddParameters("@UsuarioTroca", Databasemanager.Csv_Variaveis.AP_cdUsuario.ToString());
            MI_AddParameters("@cdItem", prCdPremio);

            MI_ExecuteNonQuery();
        }

        public DataTable MS_ObterTodosOsRegistros()
        {
            MI_SetSql("SELECT * FROM tblvisitante");
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterCodigoBarrasDaConsultora(string prConsultora)
        {
            MI_SetSql("SELECT " +
                        "	tblvisitante.CodigoBarras " +
                        "FROM  " +
                        "	tblvisitante " +
                        "		INNER JOIN tblInscricao ON " +
                        "			tblvisitante.Pedido = tblInscricao.Pedido " +
                        "WHERE " +
                        "	tblInscricao.TipodeInscricao NOT LIKE '%Convidados%' and " +
                        "	tblInscricao.Consultora = @Consultora ");

            MI_AddParameters("@Consultora", prConsultora);
            return MI_ExecuteDataSet();
        }

        public DataTable MS_ObterRoteiroPorIdRoteiro(string prCdRoteiro)
        {
            MI_SetSql("SELECT * FROM tblRoteiro WHERE cdRoteiro = '"+ prCdRoteiro + "'");
            return MI_ExecuteDataSet();
        }
    }
}

