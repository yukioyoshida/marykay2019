using System.Data;

namespace SPI
{
    public class FWK_DAO_Visitante : FWK_DAO_MySQL
    {
            public string AP_SelectALL = "SELECT  " + 
                           "          NumeroTerminal, " +
                           "          Chave, " +
                           "          CodigoBarras, " +
                           "          EmissaoCracha, " +
                           "          PresenteDia1, " +
                           "          PresenteDia2, " +
                           "          PresenteDia3, " +
                           "          PresenteDia4, " +
                           "          PresenteDia5, " +
                           "          OrigemInsercao, " +
                           "          DataOrigemInsercao, " +
                           "          HoraOrigemInsercao, " +
                           "          UsuarioOrigemInsercao, " +
                           "          OrigemAlteracao, " +
                           "          DataOrigemAlteracao, " +
                           "          HoraOrigemAlteracao, " +
                           "          UsuarioOrigemAlteracao, " +
                           "          MetodoInsercao, " +
                           "          ParticipouEdicao, " +
                           "          CategoriaRegistro, " +
                           "          OrigemConvite, " +
                           "          EmailLogin, " +
                           "          CPF, " +
                           "          Senha, " +
                           "          Tratamento, " +
                           "          Nome, " +
                           "          Sobrenome, " +
                           "          NomeCompleto, " +
                           "          NomeCracha, " +
                           "          Profissao, " +
                           "          Cargo, " +
                           "          Sexo, " +
                           "          DiaNascimento, " +
                           "          MesNascimento, " +
                           "          AnoNascimento, " +
                           "          DDICelular, " +
                           "          DDDCelular, " +
                           "          TelefoneCelular, " +
                           "          SmsCelularPessoal, " +
                           "          MmsCelularPessoal, " +
                           "          EmailPessoal, " +
                           "          AutorizaEnvioEmailPessoal, " +
                           "          NomeEnderecoPessoal, " +
                           "          NumeroEnderecoPessoal, " +
                           "          ComplementoEnderecoPessoal, " +
                           "          CepEnderecoPessoal, " +
                           "          BairroEnderecoPessoal, " +
                           "          CidadeEnderecoPessoal, " +
                           "          EstadoEnderecoPessoal, " +
                           "          PaisEnderecoPessoal, " +
                           "          DDITelefonePessoal, " +
                           "          DDDTelefonePessoal, " +
                           "          NumeroTelefonePessoal, " +
                           "          DDIFaxPessoal, " +
                           "          DDDFaxPessoal, " +
                           "          NumeroFaxPessoal, " +
                           "          RazaoSocial, " +
                           "          NomeFantasia, " +
                           "          EmpresaCracha, " +
                           "          Cnpj, " +
                           "          NomeEnderecoComercial, " +
                           "          NumeroEnderecoComercial, " +
                           "          ComplementoEnderecoComercial, " +
                           "          CepEnderecoComercial, " +
                           "          BairroEnderecoComercial, " +
                           "          CidadeEnderecoComercial, " +
                           "          EstadoEnderecoComercial, " +
                           "          PaisEnderecoComercial, " +
                           "          DDITelefoneComercial, " +
                           "          DDDTelefoneComercial, " +
                           "          NumeroTelefoneComercial, " +
                           "          DDIFaxComercial, " +
                           "          DDDFaxComercial, " +
                           "          NumeroFaxComercial, " +
                           "          PaginaWebComercial, " +
                           "          EmailComercial, " +
                           "          AutorizaEnvioEmailComercial, " +
                           "          Res1, " +
                           "          Res2, " +
                           "          Res3, " +
                           "          Res4, " +
                           "          Res5, " +
                           "          ObjRef, " +
                           "          Departamento, " +
                           "          TipoEnderecoPessoal, " +
                           "          InscricaoEstadual, " +
                           "          TipoEnderecoComercial, " +
                           "          Pergunta1, " +
                           "          Pergunta1Outro, " +
                           "          Pergunta2, " +
                           "          Pergunta2Outro, " +
                           "          Pergunta3, " +
                           "          Pergunta3Outro, " +
                           "          Pergunta4, " +
                           "          Pergunta4Outro, " +
                           "          Pergunta5, " +
                           "          Pergunta5Outro, " +
                           "          Pergunta6, " +
                           "          Pergunta6Outro, " +
                           "          Pergunta7, " +
                           "          Pergunta7Outro, " +
                           "          Pergunta8, " +
                           "          Pergunta8Outro, " +
                           "          Pergunta9, " +
                           "          Pergunta9Outro, " +
                           "          Pergunta10, " +
                           "          Pergunta10Outro, " +
                           "          Pergunta11, " +
                           "          Pergunta11Outro, " +
                           "          Pergunta12, " +
                           "          Pergunta12Outro, " +
                           "          Pergunta13, " +
                           "          Pergunta13Outro, " +
                           "          Pergunta14, " +
                           "          Pergunta14Outro, " +
                           "          Pergunta15, " +
                           "          Pergunta15Outro, " +
                           "          Pergunta16, " +
                           "          Pergunta16Outro, " +
                           "          Pergunta17, " +
                           "          Pergunta17Outro, " +
                           "          Pergunta18, " +
                           "          Pergunta18Outro, " +
                           "          Pergunta19, " +
                           "          Pergunta19Outro, " +
                           "          Pergunta20, " +
                           "          Pergunta20Outro, " +
                           "          Status, " +
                           "          Localizese, " +
                           "          Evento, " +
                           "          EventoPrincipal, " +
                           "          NumeroImportacao ";

        public string AP_SelectALLFirst = "SELECT top(1) " +
               "          NumeroTerminal, " +
               "          Chave, " +
               "          CodigoBarras, " +
               "          EmissaoCracha, " +
               "          PresenteDia1, " +
               "          PresenteDia2, " +
               "          PresenteDia3, " +
               "          PresenteDia4, " +
               "          PresenteDia5, " +
               "          OrigemInsercao, " +
               "          DataOrigemInsercao, " +
               "          HoraOrigemInsercao, " +
               "          UsuarioOrigemInsercao, " +
               "          OrigemAlteracao, " +
               "          DataOrigemAlteracao, " +
               "          HoraOrigemAlteracao, " +
               "          UsuarioOrigemAlteracao, " +
               "          MetodoInsercao, " +
               "          ParticipouEdicao, " +
               "          CategoriaRegistro, " +
               "          OrigemConvite, " +
               "          EmailLogin, " +
               "          CPF, " +
               "          Senha, " +
               "          Tratamento, " +
               "          Nome, " +
               "          Sobrenome, " +
               "          NomeCompleto, " +
               "          NomeCracha, " +
               "          Profissao, " +
               "          Cargo, " +
               "          Sexo, " +
               "          DiaNascimento, " +
               "          MesNascimento, " +
               "          AnoNascimento, " +
               "          DDICelular, " +
               "          DDDCelular, " +
               "          TelefoneCelular, " +
               "          SmsCelularPessoal, " +
               "          MmsCelularPessoal, " +
               "          EmailPessoal, " +
               "          AutorizaEnvioEmailPessoal, " +
               "          NomeEnderecoPessoal, " +
               "          NumeroEnderecoPessoal, " +
               "          ComplementoEnderecoPessoal, " +
               "          CepEnderecoPessoal, " +
               "          BairroEnderecoPessoal, " +
               "          CidadeEnderecoPessoal, " +
               "          EstadoEnderecoPessoal, " +
               "          PaisEnderecoPessoal, " +
               "          DDITelefonePessoal, " +
               "          DDDTelefonePessoal, " +
               "          NumeroTelefonePessoal, " +
               "          DDIFaxPessoal, " +
               "          DDDFaxPessoal, " +
               "          NumeroFaxPessoal, " +
               "          RazaoSocial, " +
               "          NomeFantasia, " +
               "          EmpresaCracha, " +
               "          Cnpj, " +
               "          NomeEnderecoComercial, " +
               "          NumeroEnderecoComercial, " +
               "          ComplementoEnderecoComercial, " +
               "          CepEnderecoComercial, " +
               "          BairroEnderecoComercial, " +
               "          CidadeEnderecoComercial, " +
               "          EstadoEnderecoComercial, " +
               "          PaisEnderecoComercial, " +
               "          DDITelefoneComercial, " +
               "          DDDTelefoneComercial, " +
               "          NumeroTelefoneComercial, " +
               "          DDIFaxComercial, " +
               "          DDDFaxComercial, " +
               "          NumeroFaxComercial, " +
               "          PaginaWebComercial, " +
               "          EmailComercial, " +
               "          AutorizaEnvioEmailComercial, " +
               "          Res1, " +
               "          Res2, " +
               "          Res3, " +
               "          Res4, " +
               "          Res5, " +
               "          ObjRef, " +
               "          Departamento, " +
               "          TipoEnderecoPessoal, " +
               "          InscricaoEstadual, " +
               "          TipoEnderecoComercial, " +
               "          Pergunta1, " +
               "          Pergunta1Outro, " +
               "          Pergunta2, " +
               "          Pergunta2Outro, " +
               "          Pergunta3, " +
               "          Pergunta3Outro, " +
               "          Pergunta4, " +
               "          Pergunta4Outro, " +
               "          Pergunta5, " +
               "          Pergunta5Outro, " +
               "          Pergunta6, " +
               "          Pergunta6Outro, " +
               "          Pergunta7, " +
               "          Pergunta7Outro, " +
               "          Pergunta8, " +
               "          Pergunta8Outro, " +
               "          Pergunta9, " +
               "          Pergunta9Outro, " +
               "          Pergunta10, " +
               "          Pergunta10Outro, " +
               "          Pergunta11, " +
               "          Pergunta11Outro, " +
               "          Pergunta12, " +
               "          Pergunta12Outro, " +
               "          Pergunta13, " +
               "          Pergunta13Outro, " +
               "          Pergunta14, " +
               "          Pergunta14Outro, " +
               "          Pergunta15, " +
               "          Pergunta15Outro, " +
               "          Pergunta16, " +
               "          Pergunta16Outro, " +
               "          Pergunta17, " +
               "          Pergunta17Outro, " +
               "          Pergunta18, " +
               "          Pergunta18Outro, " +
               "          Pergunta19, " +
               "          Pergunta19Outro, " +
               "          Pergunta20, " +
               "          Pergunta20Outro, " +
               "          Status, " +
               "          Localizese, " +
               "          Evento, " +
               "          EventoPrincipal, " +
               "          NumeroImportacao ";

        public FWK_DAO_Visitante()
        {
        }

        public void MS_Incluir(CTO_Visitante ITO_Visitante)
        {
            //int itObjRef = MS_NovoObjRef();
            MI_SetSql ("INSERT INTO tblVisitante (" +
                       "          NumeroTerminal, " +
                       "          Chave, " +
                       "          CodigoBarras, " +
                       "          EmissaoCracha, " +
                       "          PresenteDia1, " +
                       "          PresenteDia2, " +
                       "          PresenteDia3, " +
                       "          PresenteDia4, " +
                       "          PresenteDia5, " +
                       "          OrigemInsercao, " +
                       "          DataOrigemInsercao, " +
                       "          HoraOrigemInsercao, " +
                       "          UsuarioOrigemInsercao, " +
                       "          OrigemAlteracao, " +
                       "          DataOrigemAlteracao, " +
                       "          HoraOrigemAlteracao, " +
                       "          UsuarioOrigemAlteracao, " +
                       "          MetodoInsercao, " +
                       "          ParticipouEdicao, " +
                       "          CategoriaRegistro, " +
                       "          OrigemConvite, " +
                       "          EmailLogin, " +
                       "          CPF, " +
                       "          Senha, " +
                       "          Tratamento, " +
                       "          Nome, " +
                       "          Sobrenome, " +
                       "          NomeCompleto, " +
                       "          NomeCracha, " +
                       "          Profissao, " +
                       "          Cargo, " +
                       "          Sexo, " +
                       "          DiaNascimento, " +
                       "          MesNascimento, " +
                       "          AnoNascimento, " +
                       "          DDICelular, " +
                       "          DDDCelular, " +
                       "          TelefoneCelular, " +
                       "          SmsCelularPessoal, " +
                       "          MmsCelularPessoal, " +
                       "          EmailPessoal, " +
                       "          AutorizaEnvioEmailPessoal, " +
                       "          NomeEnderecoPessoal, " +
                       "          NumeroEnderecoPessoal, " +
                       "          ComplementoEnderecoPessoal, " +
                       "          CepEnderecoPessoal, " +
                       "          BairroEnderecoPessoal, " +
                       "          CidadeEnderecoPessoal, " +
                       "          EstadoEnderecoPessoal, " +
                       "          PaisEnderecoPessoal, " +
                       "          DDITelefonePessoal, " +
                       "          DDDTelefonePessoal, " +
                       "          NumeroTelefonePessoal, " +
                       "          DDIFaxPessoal, " +
                       "          DDDFaxPessoal, " +
                       "          NumeroFaxPessoal, " +
                       "          RazaoSocial, " +
                       "          NomeFantasia, " +
                       "          EmpresaCracha, " +
                       "          Cnpj, " +
                       "          NomeEnderecoComercial, " +
                       "          NumeroEnderecoComercial, " +
                       "          ComplementoEnderecoComercial, " +
                       "          CepEnderecoComercial, " +
                       "          BairroEnderecoComercial, " +
                       "          CidadeEnderecoComercial, " +
                       "          EstadoEnderecoComercial, " +
                       "          PaisEnderecoComercial, " +
                       "          DDITelefoneComercial, " +
                       "          DDDTelefoneComercial, " +
                       "          NumeroTelefoneComercial, " +
                       "          DDIFaxComercial, " +
                       "          DDDFaxComercial, " +
                       "          NumeroFaxComercial, " +
                       "          PaginaWebComercial, " +
                       "          EmailComercial, " +
                       "          AutorizaEnvioEmailComercial, " +
                       "          Pedido, " +
                       "          Res2, " +
                       "          Res3, " +
                       "          Res4, " +
                       "          Res5, " +
                       //"          ObjRef, " +
                       "          Departamento, " +
                       "          TipoEnderecoPessoal, " +
                       "          InscricaoEstadual, " +
                       "          TipoEnderecoComercial, " +
                       "          Pergunta1, " +
                       "          Pergunta1Outro, " +
                       "          Pergunta2, " +
                       "          Pergunta2Outro, " +
                       "          Pergunta3, " +
                       "          Pergunta3Outro, " +
                       "          Pergunta4, " +
                       "          Pergunta4Outro, " +
                       "          Pergunta5, " +
                       "          Pergunta5Outro, " +
                       "          Pergunta6, " +
                       "          Pergunta6Outro, " +
                       "          Pergunta7, " +
                       "          Pergunta7Outro, " +
                       "          Pergunta8, " +
                       "          Pergunta8Outro, " +
                       "          Pergunta9, " +
                       "          Pergunta9Outro, " +
                       "          Pergunta10, " +
                       "          Pergunta10Outro, " +
                       "          Pergunta11, " +
                       "          Pergunta11Outro, " +
                       "          Pergunta12, " +
                       "          Pergunta12Outro, " +
                       "          Pergunta13, " +
                       "          Pergunta13Outro, " +
                       "          Pergunta14, " +
                       "          Pergunta14Outro, " +
                       "          Pergunta15, " +
                       "          Pergunta15Outro, " +
                       "          Pergunta16, " +
                       "          Pergunta16Outro, " +
                       "          Pergunta17, " +
                       "          Pergunta17Outro, " +
                       "          Pergunta18, " +
                       "          Pergunta18Outro, " +
                       "          Pergunta19, " +
                       "          Pergunta19Outro, " +
                       "          Pergunta20, " +
                       "          Pergunta20Outro, " +
                       "          Status,          " +
                       "          Localizese, " +
                       "          Evento, " +
                       "          EventoPrincipal, " +
                       "          NumeroImportacao ) " +
                       " VALUES (" + 
                       "          @NumeroTerminal, " +
                       "          @Chave, " +
                       "          @CodigoBarras, " +
                       "          @EmissaoCracha, " +
                       "          @PresenteDia1, " +
                       "          @PresenteDia2, " +
                       "          @PresenteDia3, " +
                       "          @PresenteDia4, " +
                       "          @PresenteDia5, " +
                       "          @OrigemInsercao, " +
                       "          @DataOrigemInsercao, " +
                       "          @HoraOrigemInsercao, " +
                       "          @UsuarioOrigemInsercao, " +
                       "          @OrigemAlteracao, " +
                       "          @DataOrigemAlteracao, " +
                       "          @HoraOrigemAlteracao, " +
                       "          @UsuarioOrigemAlteracao, " +
                       "          @MetodoInsercao, " +
                       "          @ParticipouEdicao, " +
                       "          @CategoriaRegistro, " +
                       "          @OrigemConvite, " +
                       "          @EmailLogin, " +
                       "          @CPF, " +
                       "          @Senha, " +
                       "          @Tratamento, " +
                       "          @Nome, " +
                       "          @Sobrenome, " +
                       "          @NomeCompleto, " +
                       "          @NomeCracha, " +
                       "          @Profissao, " +
                       "          @Cargo, " +
                       "          @Sexo, " +
                       "          @DiaNascimento, " +
                       "          @MesNascimento, " +
                       "          @AnoNascimento, " +
                       "          @DDICelular, " +
                       "          @DDDCelular, " +
                       "          @TelefoneCelular, " +
                       "          @SmsCelularPessoal, " +
                       "          @MmsCelularPessoal, " +
                       "          @EmailPessoal, " +
                       "          @AutorizaEnvioEmailPessoal, " +
                       "          @NomeEnderecoPessoal, " +
                       "          @NumeroEnderecoPessoal, " +
                       "          @ComplementoEnderecoPessoal, " +
                       "          @CepEnderecoPessoal, " +
                       "          @BairroEnderecoPessoal, " +
                       "          @CidadeEnderecoPessoal, " +
                       "          @EstadoEnderecoPessoal, " +
                       "          @PaisEnderecoPessoal, " +
                       "          @DDITelefonePessoal, " +
                       "          @DDDTelefonePessoal, " +
                       "          @NumeroTelefonePessoal, " +
                       "          @DDIFaxPessoal, " +
                       "          @DDDFaxPessoal, " +
                       "          @NumeroFaxPessoal, " +
                       "          @RazaoSocial, " +
                       "          @NomeFantasia, " +
                       "          @EmpresaCracha, " +
                       "          @Cnpj, " +
                       "          @NomeEnderecoComercial, " +
                       "          @NumeroEnderecoComercial, " +
                       "          @ComplementoEnderecoComercial, " +
                       "          @CepEnderecoComercial, " +
                       "          @BairroEnderecoComercial, " +
                       "          @CidadeEnderecoComercial, " +
                       "          @EstadoEnderecoComercial, " +
                       "          @PaisEnderecoComercial, " +
                       "          @DDITelefoneComercial, " +
                       "          @DDDTelefoneComercial, " +
                       "          @NumeroTelefoneComercial, " +
                       "          @DDIFaxComercial, " +
                       "          @DDDFaxComercial, " +
                       "          @NumeroFaxComercial, " +
                       "          @PaginaWebComercial, " +
                       "          @EmailComercial, " +
                       "          @AutorizaEnvioEmailComercial, " +
                       "          @Res1, " +
                       "          @Res2, " +
                       "          @Res3, " +
                       "          @Res4, " +
                       "          @Res5, " +
                       //"          @ObjRef, " +
                       "          @Departamento, " +
                       "          @TipoEnderecoPessoal, " +
                       "          @InscricaoEstadual, " +
                       "          @TipoEnderecoComercial, " +
                       "          @Pergunta1, " +
                       "          @Pergunta1Outro, " +
                       "          @Pergunta2, " +
                       "          @Pergunta2Outro, " +
                       "          @Pergunta3, " +
                       "          @Pergunta3Outro, " +
                       "          @Pergunta4, " +
                       "          @Pergunta4Outro, " +
                       "          @Pergunta5, " +
                       "          @Pergunta5Outro, " +
                       "          @Pergunta6, " +
                       "          @Pergunta6Outro, " +
                       "          @Pergunta7, " +
                       "          @Pergunta7Outro, " +
                       "          @Pergunta8, " +
                       "          @Pergunta8Outro, " +
                       "          @Pergunta9, " +
                       "          @Pergunta9Outro, " +
                       "          @Pergunta10, " +
                       "          @Pergunta10Outro, " +
                       "          @Pergunta11, " +
                       "          @Pergunta11Outro, " +
                       "          @Pergunta12, " +
                       "          @Pergunta12Outro, " +
                       "          @Pergunta13, " +
                       "          @Pergunta13Outro, " +
                       "          @Pergunta14, " +
                       "          @Pergunta14Outro, " +
                       "          @Pergunta15, " +
                       "          @Pergunta15Outro, " +
                       "          @Pergunta16, " +
                       "          @Pergunta16Outro, " +
                       "          @Pergunta17, " +
                       "          @Pergunta17Outro, " +
                       "          @Pergunta18, " +
                       "          @Pergunta18Outro, " +
                       "          @Pergunta19, " +
                       "          @Pergunta19Outro, " +
                       "          @Pergunta20, " +
                       "          @Pergunta20Outro, " +
                       "          @Status,          " +
                       "          @Localizese, " +
                       "          @Evento, " +
                       "          @EventoPrincipal, " +
                       "          @NumeroImportacao ) ");

            //ITO_Visitante.setObjRef(itObjRef);
            MI_AddParameters("@NumeroTerminal", ITO_Visitante.getNumeroTerminal());
            MI_AddParameters("@Chave", ITO_Visitante.getChave());
            MI_AddParameters("@CodigoBarras", ITO_Visitante.getCodigoBarras());
            MI_AddParameters("@EmissaoCracha", ITO_Visitante.getEmissaoCracha());
            MI_AddParameters("@PresenteDia1", ITO_Visitante.getPresenteDia1());
            MI_AddParameters("@PresenteDia2", ITO_Visitante.getPresenteDia2());
            MI_AddParameters("@PresenteDia3", ITO_Visitante.getPresenteDia3());
            MI_AddParameters("@PresenteDia4", ITO_Visitante.getPresenteDia4());
            MI_AddParameters("@PresenteDia5", ITO_Visitante.getPresenteDia5());
            MI_AddParameters("@OrigemInsercao", ITO_Visitante.getOrigemInsercao());
            MI_AddParameters("@DataOrigemInsercao", ITO_Visitante.getDataOrigemInsercao());
            MI_AddParameters("@HoraOrigemInsercao", ITO_Visitante.getHoraOrigemInsercao());
            MI_AddParameters("@UsuarioOrigemInsercao", ITO_Visitante.getUsuarioOrigemInsercao());
            MI_AddParameters("@OrigemAlteracao", ITO_Visitante.getOrigemAlteracao());
            MI_AddParameters("@DataOrigemAlteracao", ITO_Visitante.getDataOrigemAlteracao());
            MI_AddParameters("@HoraOrigemAlteracao", ITO_Visitante.getHoraOrigemAlteracao());
            MI_AddParameters("@UsuarioOrigemAlteracao", ITO_Visitante.getUsuarioOrigemAlteracao());
            MI_AddParameters("@MetodoInsercao", ITO_Visitante.getMetodoInsercao());
            MI_AddParameters("@ParticipouEdicao", ITO_Visitante.getParticipouEdicao());
            MI_AddParameters("@CategoriaRegistro", ITO_Visitante.getCategoriaRegistro());
            MI_AddParameters("@OrigemConvite", ITO_Visitante.getOrigemConvite());
            MI_AddParameters("@EmailLogin", ITO_Visitante.getEmailLogin());
            MI_AddParameters("@CPF", ITO_Visitante.getCPF());
            MI_AddParameters("@Senha", ITO_Visitante.getSenha());
            MI_AddParameters("@Tratamento", ITO_Visitante.getTratamento());
            MI_AddParameters("@Nome", ITO_Visitante.getNome());
            MI_AddParameters("@Sobrenome", ITO_Visitante.getSobrenome());
            MI_AddParameters("@NomeCompleto", ITO_Visitante.getNomeCompleto());
            MI_AddParameters("@NomeCracha", ITO_Visitante.getNomeCracha());
            MI_AddParameters("@Profissao", ITO_Visitante.getProfissao());
            MI_AddParameters("@Cargo", ITO_Visitante.getCargo());
            MI_AddParameters("@Sexo", ITO_Visitante.getSexo());
            MI_AddParameters("@DiaNascimento", ITO_Visitante.getDiaNascimento());
            MI_AddParameters("@MesNascimento", ITO_Visitante.getMesNascimento());
            MI_AddParameters("@AnoNascimento", ITO_Visitante.getAnoNascimento());
            MI_AddParameters("@DDICelular", ITO_Visitante.getDDICelular());
            MI_AddParameters("@DDDCelular", ITO_Visitante.getDDDCelular());
            MI_AddParameters("@TelefoneCelular", ITO_Visitante.getTelefoneCelular());
            MI_AddParameters("@SmsCelularPessoal", ITO_Visitante.getSmsCelularPessoal());
            MI_AddParameters("@MmsCelularPessoal", ITO_Visitante.getMmsCelularPessoal());
            MI_AddParameters("@EmailPessoal", ITO_Visitante.getEmailPessoal());
            MI_AddParameters("@AutorizaEnvioEmailPessoal", ITO_Visitante.getAutorizaEnvioEmailPessoal());
            MI_AddParameters("@NomeEnderecoPessoal", ITO_Visitante.getNomeEnderecoPessoal());
            MI_AddParameters("@NumeroEnderecoPessoal", ITO_Visitante.getNumeroEnderecoPessoal());
            MI_AddParameters("@ComplementoEnderecoPessoal", ITO_Visitante.getComplementoEnderecoPessoal());
            MI_AddParameters("@CepEnderecoPessoal", ITO_Visitante.getCepEnderecoPessoal());
            MI_AddParameters("@BairroEnderecoPessoal", ITO_Visitante.getBairroEnderecoPessoal());
            MI_AddParameters("@CidadeEnderecoPessoal", ITO_Visitante.getCidadeEnderecoPessoal());
            MI_AddParameters("@EstadoEnderecoPessoal", ITO_Visitante.getEstadoEnderecoPessoal());
            MI_AddParameters("@PaisEnderecoPessoal", ITO_Visitante.getPaisEnderecoPessoal());
            MI_AddParameters("@DDITelefonePessoal", ITO_Visitante.getDDITelefonePessoal());
            MI_AddParameters("@DDDTelefonePessoal", ITO_Visitante.getDDDTelefonePessoal());
            MI_AddParameters("@NumeroTelefonePessoal", ITO_Visitante.getNumeroTelefonePessoal());
            MI_AddParameters("@DDIFaxPessoal", ITO_Visitante.getDDIFaxPessoal());
            MI_AddParameters("@DDDFaxPessoal", ITO_Visitante.getDDDFaxPessoal());
            MI_AddParameters("@NumeroFaxPessoal", ITO_Visitante.getNumeroFaxPessoal());
            MI_AddParameters("@RazaoSocial", ITO_Visitante.getRazaoSocial());
            MI_AddParameters("@NomeFantasia", ITO_Visitante.getNomeFantasia());
            MI_AddParameters("@EmpresaCracha", ITO_Visitante.getEmpresaCracha());
            MI_AddParameters("@Cnpj", ITO_Visitante.getCnpj());
            MI_AddParameters("@NomeEnderecoComercial", ITO_Visitante.getNomeEnderecoComercial());
            MI_AddParameters("@NumeroEnderecoComercial", ITO_Visitante.getNumeroEnderecoComercial());
            MI_AddParameters("@ComplementoEnderecoComercial", ITO_Visitante.getComplementoEnderecoComercial());
            MI_AddParameters("@CepEnderecoComercial", ITO_Visitante.getCepEnderecoComercial());
            MI_AddParameters("@BairroEnderecoComercial", ITO_Visitante.getBairroEnderecoComercial());
            MI_AddParameters("@CidadeEnderecoComercial", ITO_Visitante.getCidadeEnderecoComercial());
            MI_AddParameters("@EstadoEnderecoComercial", ITO_Visitante.getEstadoEnderecoComercial());
            MI_AddParameters("@PaisEnderecoComercial", ITO_Visitante.getPaisEnderecoComercial());
            MI_AddParameters("@DDITelefoneComercial", ITO_Visitante.getDDITelefoneComercial());
            MI_AddParameters("@DDDTelefoneComercial", ITO_Visitante.getDDDTelefoneComercial());
            MI_AddParameters("@NumeroTelefoneComercial", ITO_Visitante.getNumeroTelefoneComercial());
            MI_AddParameters("@DDIFaxComercial", ITO_Visitante.getDDIFaxComercial());
            MI_AddParameters("@DDDFaxComercial", ITO_Visitante.getDDDFaxComercial());
            MI_AddParameters("@NumeroFaxComercial", ITO_Visitante.getNumeroFaxComercial());
            MI_AddParameters("@PaginaWebComercial", ITO_Visitante.getPaginaWebComercial());
            MI_AddParameters("@EmailComercial", ITO_Visitante.getEmailComercial());
            MI_AddParameters("@AutorizaEnvioEmailComercial", ITO_Visitante.getAutorizaEnvioEmailComercial());
            MI_AddParameters("@Res1", ITO_Visitante.getRes1());
            MI_AddParameters("@Res2", ITO_Visitante.getRes2());
            MI_AddParameters("@Res3", ITO_Visitante.getRes3());
            MI_AddParameters("@Res4", ITO_Visitante.getRes4());
            MI_AddParameters("@Res5", ITO_Visitante.getRes5());
            //MI_AddParameters("@ObjRef", itObjRef);
            MI_AddParameters("@Departamento", ITO_Visitante.getDepartamento());
            MI_AddParameters("@TipoEnderecoPessoal", ITO_Visitante.getTipoEnderecoPessoal());
            MI_AddParameters("@InscricaoEstadual", ITO_Visitante.getInscricaoEstadual());
            MI_AddParameters("@TipoEnderecoComercial", ITO_Visitante.getTipoEnderecoComercial());
            MI_AddParameters("@Pergunta1", ITO_Visitante.getPergunta1());
            MI_AddParameters("@Pergunta1Outro", ITO_Visitante.getPergunta1Outro());
            MI_AddParameters("@Pergunta2", ITO_Visitante.getPergunta2());
            MI_AddParameters("@Pergunta2Outro", ITO_Visitante.getPergunta2Outro());
            MI_AddParameters("@Pergunta3", ITO_Visitante.getPergunta3());
            MI_AddParameters("@Pergunta3Outro", ITO_Visitante.getPergunta3Outro());
            MI_AddParameters("@Pergunta4", ITO_Visitante.getPergunta4());
            MI_AddParameters("@Pergunta4Outro", ITO_Visitante.getPergunta4Outro());
            MI_AddParameters("@Pergunta5", ITO_Visitante.getPergunta5());
            MI_AddParameters("@Pergunta5Outro", ITO_Visitante.getPergunta5Outro());
            MI_AddParameters("@Pergunta6", ITO_Visitante.getPergunta6());
            MI_AddParameters("@Pergunta6Outro", ITO_Visitante.getPergunta6Outro());
            MI_AddParameters("@Pergunta7", ITO_Visitante.getPergunta7());
            MI_AddParameters("@Pergunta7Outro", ITO_Visitante.getPergunta7Outro());
            MI_AddParameters("@Pergunta8", ITO_Visitante.getPergunta8());
            MI_AddParameters("@Pergunta8Outro", ITO_Visitante.getPergunta8Outro());
            MI_AddParameters("@Pergunta9", ITO_Visitante.getPergunta9());
            MI_AddParameters("@Pergunta9Outro", ITO_Visitante.getPergunta9Outro());
            MI_AddParameters("@Pergunta10", ITO_Visitante.getPergunta10());
            MI_AddParameters("@Pergunta10Outro", ITO_Visitante.getPergunta10Outro());
            MI_AddParameters("@Pergunta11", ITO_Visitante.getPergunta11());
            MI_AddParameters("@Pergunta11Outro", ITO_Visitante.getPergunta11Outro());
            MI_AddParameters("@Pergunta12", ITO_Visitante.getPergunta12());
            MI_AddParameters("@Pergunta12Outro", ITO_Visitante.getPergunta12Outro());
            MI_AddParameters("@Pergunta13", ITO_Visitante.getPergunta13());
            MI_AddParameters("@Pergunta13Outro", ITO_Visitante.getPergunta13Outro());
            MI_AddParameters("@Pergunta14", ITO_Visitante.getPergunta14());
            MI_AddParameters("@Pergunta14Outro", ITO_Visitante.getPergunta14Outro());
            MI_AddParameters("@Pergunta15", ITO_Visitante.getPergunta15());
            MI_AddParameters("@Pergunta15Outro", ITO_Visitante.getPergunta15Outro());
            MI_AddParameters("@Pergunta16", ITO_Visitante.getPergunta16());
            MI_AddParameters("@Pergunta16Outro", ITO_Visitante.getPergunta16Outro());
            MI_AddParameters("@Pergunta17", ITO_Visitante.getPergunta17());
            MI_AddParameters("@Pergunta17Outro", ITO_Visitante.getPergunta17Outro());
            MI_AddParameters("@Pergunta18", ITO_Visitante.getPergunta18());
            MI_AddParameters("@Pergunta18Outro", ITO_Visitante.getPergunta18Outro());
            MI_AddParameters("@Pergunta19", ITO_Visitante.getPergunta19());
            MI_AddParameters("@Pergunta19Outro", ITO_Visitante.getPergunta19Outro());
            MI_AddParameters("@Pergunta20", ITO_Visitante.getPergunta20());
            MI_AddParameters("@Pergunta20Outro", ITO_Visitante.getPergunta20Outro());
            MI_AddParameters("@Status", ITO_Visitante.getStatus());
            MI_AddParameters("@Localizese", ITO_Visitante.getLocalizese());
            MI_AddParameters("@Evento", ITO_Visitante.getEvento());
            MI_AddParameters("@EventoPrincipal", ITO_Visitante.getEventoPrincipal());
            MI_AddParameters("@NumeroImportacao", ITO_Visitante.getNumeroImportacao());
            //
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_Alterar(CTO_Visitante ITO_Visitante)
        {
            MI_SetSql ("UPDATE tblVisitante SET " +
                       "          EmissaoCracha = @EmissaoCracha, " +
                       "          PresenteDia1 = @PresenteDia1, " +
                       "          PresenteDia2 = @PresenteDia2, " +
                       "          PresenteDia3 = @PresenteDia3, " +
                       "          PresenteDia4 = @PresenteDia4, " +
                       "          PresenteDia5 = @PresenteDia5, " +
                       "          OrigemAlteracao = @OrigemAlteracao, " +
                       "          DataOrigemAlteracao = @DataOrigemAlteracao, " +
                       "          HoraOrigemAlteracao = @HoraOrigemAlteracao, " +
                       "          UsuarioOrigemAlteracao = @UsuarioOrigemAlteracao, " +
                       "          ParticipouEdicao = @ParticipouEdicao, " +
                       "          CategoriaRegistro = @CategoriaRegistro, " +
                       "          OrigemConvite = @OrigemConvite, " +
                       "          EmailLogin = @EmailLogin, " +
                       "          CPF = @CPF, " +
                       "          Senha = @Senha, " +
                       "          Tratamento = @Tratamento, " +
                       "          Nome = @Nome, " +
                       "          Sobrenome = @Sobrenome, " +
                       "          NomeCompleto = @NomeCompleto, " +
                       "          NomeCracha = @NomeCracha, " +
                       "          Profissao = @Profissao, " +
                       "          Cargo = @Cargo, " +
                       "          Sexo = @Sexo, " +
                       "          DiaNascimento = @DiaNascimento, " +
                       "          MesNascimento = @MesNascimento, " +
                       "          AnoNascimento = @AnoNascimento, " +
                       "          DDICelular = @DDICelular, " +
                       "          DDDCelular = @DDDCelular, " +
                       "          TelefoneCelular = @TelefoneCelular, " +
                       "          SmsCelularPessoal = @SmsCelularPessoal, " +
                       "          MmsCelularPessoal = @MmsCelularPessoal, " +
                       "          EmailPessoal = @EmailPessoal, " +
                       "          AutorizaEnvioEmailPessoal = @AutorizaEnvioEmailPessoal, " +
                       "          NomeEnderecoPessoal = @NomeEnderecoPessoal, " +
                       "          NumeroEnderecoPessoal = @NumeroEnderecoPessoal, " +
                       "          ComplementoEnderecoPessoal = @ComplementoEnderecoPessoal, " +
                       "          CepEnderecoPessoal = @CepEnderecoPessoal, " +
                       "          BairroEnderecoPessoal = @BairroEnderecoPessoal, " +
                       "          CidadeEnderecoPessoal = @CidadeEnderecoPessoal, " +
                       "          EstadoEnderecoPessoal = @EstadoEnderecoPessoal, " +
                       "          PaisEnderecoPessoal = @PaisEnderecoPessoal, " +
                       "          DDITelefonePessoal = @DDITelefonePessoal, " +
                       "          DDDTelefonePessoal = @DDDTelefonePessoal, " +
                       "          NumeroTelefonePessoal = @NumeroTelefonePessoal, " +
                       "          DDIFaxPessoal = @DDIFaxPessoal, " +
                       "          DDDFaxPessoal = @DDDFaxPessoal, " +
                       "          NumeroFaxPessoal = @NumeroFaxPessoal, " +
                       "          RazaoSocial = @RazaoSocial, " +
                       "          NomeFantasia = @NomeFantasia, " +
                       "          EmpresaCracha = @EmpresaCracha, " +
                       "          Cnpj = @Cnpj, " +
                       "          NomeEnderecoComercial = @NomeEnderecoComercial, " +
                       "          NumeroEnderecoComercial = @NumeroEnderecoComercial, " +
                       "          ComplementoEnderecoComercial = @ComplementoEnderecoComercial, " +
                       "          CepEnderecoComercial = @CepEnderecoComercial, " +
                       "          BairroEnderecoComercial = @BairroEnderecoComercial, " +
                       "          CidadeEnderecoComercial = @CidadeEnderecoComercial, " +
                       "          EstadoEnderecoComercial = @EstadoEnderecoComercial, " +
                       "          PaisEnderecoComercial = @PaisEnderecoComercial, " +
                       "          DDITelefoneComercial = @DDITelefoneComercial, " +
                       "          DDDTelefoneComercial = @DDDTelefoneComercial, " +
                       "          NumeroTelefoneComercial = @NumeroTelefoneComercial, " +
                       "          DDIFaxComercial = @DDIFaxComercial, " +
                       "          DDDFaxComercial = @DDDFaxComercial, " +
                       "          NumeroFaxComercial = @NumeroFaxComercial, " +
                       "          PaginaWebComercial = @PaginaWebComercial, " +
                       "          EmailComercial = @EmailComercial, " +
                       "          AutorizaEnvioEmailComercial = @AutorizaEnvioEmailComercial, " +
                       "          Res1 = @Res1, " +
                       "          Res2 = @Res2, " +
                       "          Res3 = @Res3, " +
                       "          Res4 = @Res4, " +
                       "          Res5 = @Res5, " +
                       "          Departamento = @Departamento, " +
                       "          TipoEnderecoPessoal = @TipoEnderecoPessoal, " +
                       "          InscricaoEstadual = @InscricaoEstadual, " +
                       "          TipoEnderecoComercial = @TipoEnderecoComercial, " +
                       "          Pergunta1 = @Pergunta1, " +
                       "          Pergunta1Outro = @Pergunta1Outro, " +
                       "          Pergunta2 = @Pergunta2, " +
                       "          Pergunta2Outro = @Pergunta2Outro, " +
                       "          Pergunta3 = @Pergunta3, " +
                       "          Pergunta3Outro = @Pergunta3Outro, " +
                       "          Pergunta4 = @Pergunta4, " +
                       "          Pergunta4Outro = @Pergunta4Outro, " +
                       "          Pergunta5 = @Pergunta5, " +
                       "          Pergunta5Outro = @Pergunta5Outro, " +
                       "          Pergunta6 = @Pergunta6, " +
                       "          Pergunta6Outro = @Pergunta6Outro, " +
                       "          Pergunta7 = @Pergunta7, " +
                       "          Pergunta7Outro = @Pergunta7Outro, " +
                       "          Pergunta8 = @Pergunta8, " +
                       "          Pergunta8Outro = @Pergunta8Outro, " +
                       "          Pergunta9 = @Pergunta9, " +
                       "          Pergunta9Outro = @Pergunta9Outro, " +
                       "          Pergunta10 = @Pergunta10, " +
                       "          Pergunta10Outro = @Pergunta10Outro, " +
                       "          Pergunta11 = @Pergunta11, " +
                       "          Pergunta11Outro = @Pergunta11Outro, " +
                       "          Pergunta12 = @Pergunta12, " +
                       "          Pergunta12Outro = @Pergunta12Outro, " +
                       "          Pergunta13 = @Pergunta13, " +
                       "          Pergunta13Outro = @Pergunta13Outro, " +
                       "          Pergunta14 = @Pergunta14, " +
                       "          Pergunta14Outro = @Pergunta14Outro, " +
                       "          Pergunta15 = @Pergunta15, " +
                       "          Pergunta15Outro = @Pergunta15Outro, " +
                       "          Pergunta16 = @Pergunta16, " +
                       "          Pergunta16Outro = @Pergunta16Outro, " +
                       "          Pergunta17 = @Pergunta17, " +
                       "          Pergunta17Outro = @Pergunta17Outro, " +
                       "          Pergunta18 = @Pergunta18, " +
                       "          Pergunta18Outro = @Pergunta18Outro, " +
                       "          Pergunta19 = @Pergunta19, " +
                       "          Pergunta19Outro = @Pergunta19Outro, " +
                       "          Pergunta20 = @Pergunta20, " +
                       "          Pergunta20Outro = @Pergunta20Outro, " +
                       "          Status = @Status,                   " +
                       "          Localizese = @Localizese, " +
                       "          Evento = @Evento,                     " +
                       "          EventoPrincipal = @EventoPrincipal,                     " +
                       "          NumeroImportacao = @NumeroImportacao    " +
                       "WHERE CodigoBarras = @CodigoBarras ");
            MI_AddParameters("@NumeroTerminal", ITO_Visitante.getNumeroTerminal());
            MI_AddParameters("@Chave", ITO_Visitante.getChave());
            MI_AddParameters("@CodigoBarras", ITO_Visitante.getCodigoBarras());
            MI_AddParameters("@EmissaoCracha", ITO_Visitante.getEmissaoCracha());
            MI_AddParameters("@PresenteDia1", ITO_Visitante.getPresenteDia1());
            MI_AddParameters("@PresenteDia2", ITO_Visitante.getPresenteDia2());
            MI_AddParameters("@PresenteDia3", ITO_Visitante.getPresenteDia3());
            MI_AddParameters("@PresenteDia4", ITO_Visitante.getPresenteDia4());
            MI_AddParameters("@PresenteDia5", ITO_Visitante.getPresenteDia5());
            MI_AddParameters("@OrigemInsercao", ITO_Visitante.getOrigemInsercao());
            MI_AddParameters("@DataOrigemInsercao", ITO_Visitante.getDataOrigemInsercao());
            MI_AddParameters("@HoraOrigemInsercao", ITO_Visitante.getHoraOrigemInsercao());
            MI_AddParameters("@UsuarioOrigemInsercao", ITO_Visitante.getUsuarioOrigemInsercao());
            MI_AddParameters("@OrigemAlteracao", ITO_Visitante.getOrigemAlteracao());
            MI_AddParameters("@DataOrigemAlteracao", ITO_Visitante.getDataOrigemAlteracao());
            MI_AddParameters("@HoraOrigemAlteracao", ITO_Visitante.getHoraOrigemAlteracao());
            MI_AddParameters("@UsuarioOrigemAlteracao", ITO_Visitante.getUsuarioOrigemAlteracao());
            MI_AddParameters("@MetodoInsercao", ITO_Visitante.getMetodoInsercao());
            MI_AddParameters("@ParticipouEdicao", ITO_Visitante.getParticipouEdicao());
            MI_AddParameters("@CategoriaRegistro", ITO_Visitante.getCategoriaRegistro());
            MI_AddParameters("@OrigemConvite", ITO_Visitante.getOrigemConvite());
            MI_AddParameters("@EmailLogin", ITO_Visitante.getEmailLogin());
            MI_AddParameters("@CPF", ITO_Visitante.getCPF());
            MI_AddParameters("@Senha", ITO_Visitante.getSenha());
            MI_AddParameters("@Tratamento", ITO_Visitante.getTratamento());
            MI_AddParameters("@Nome", ITO_Visitante.getNome());
            MI_AddParameters("@Sobrenome", ITO_Visitante.getSobrenome());
            MI_AddParameters("@NomeCompleto", ITO_Visitante.getNomeCompleto());
            MI_AddParameters("@NomeCracha", ITO_Visitante.getNomeCracha());
            MI_AddParameters("@Profissao", ITO_Visitante.getProfissao());
            MI_AddParameters("@Cargo", ITO_Visitante.getCargo());
            MI_AddParameters("@Sexo", ITO_Visitante.getSexo());
            MI_AddParameters("@DiaNascimento", ITO_Visitante.getDiaNascimento());
            MI_AddParameters("@MesNascimento", ITO_Visitante.getMesNascimento());
            MI_AddParameters("@AnoNascimento", ITO_Visitante.getAnoNascimento());
            MI_AddParameters("@DDICelular", ITO_Visitante.getDDICelular());
            MI_AddParameters("@DDDCelular", ITO_Visitante.getDDDCelular());
            MI_AddParameters("@TelefoneCelular", ITO_Visitante.getTelefoneCelular());
            MI_AddParameters("@SmsCelularPessoal", ITO_Visitante.getSmsCelularPessoal());
            MI_AddParameters("@MmsCelularPessoal", ITO_Visitante.getMmsCelularPessoal());
            MI_AddParameters("@EmailPessoal", ITO_Visitante.getEmailPessoal());
            MI_AddParameters("@AutorizaEnvioEmailPessoal", ITO_Visitante.getAutorizaEnvioEmailPessoal());
            MI_AddParameters("@NomeEnderecoPessoal", ITO_Visitante.getNomeEnderecoPessoal());
            MI_AddParameters("@NumeroEnderecoPessoal", ITO_Visitante.getNumeroEnderecoPessoal());
            MI_AddParameters("@ComplementoEnderecoPessoal", ITO_Visitante.getComplementoEnderecoPessoal());
            MI_AddParameters("@CepEnderecoPessoal", ITO_Visitante.getCepEnderecoPessoal());
            MI_AddParameters("@BairroEnderecoPessoal", ITO_Visitante.getBairroEnderecoPessoal());
            MI_AddParameters("@CidadeEnderecoPessoal", ITO_Visitante.getCidadeEnderecoPessoal());
            MI_AddParameters("@EstadoEnderecoPessoal", ITO_Visitante.getEstadoEnderecoPessoal());
            MI_AddParameters("@PaisEnderecoPessoal", ITO_Visitante.getPaisEnderecoPessoal());
            MI_AddParameters("@DDITelefonePessoal", ITO_Visitante.getDDITelefonePessoal());
            MI_AddParameters("@DDDTelefonePessoal", ITO_Visitante.getDDDTelefonePessoal());
            MI_AddParameters("@NumeroTelefonePessoal", ITO_Visitante.getNumeroTelefonePessoal());
            MI_AddParameters("@DDIFaxPessoal", ITO_Visitante.getDDIFaxPessoal());
            MI_AddParameters("@DDDFaxPessoal", ITO_Visitante.getDDDFaxPessoal());
            MI_AddParameters("@NumeroFaxPessoal", ITO_Visitante.getNumeroFaxPessoal());
            MI_AddParameters("@RazaoSocial", ITO_Visitante.getRazaoSocial());
            MI_AddParameters("@NomeFantasia", ITO_Visitante.getNomeFantasia());
            MI_AddParameters("@EmpresaCracha", ITO_Visitante.getEmpresaCracha());
            MI_AddParameters("@Cnpj", ITO_Visitante.getCnpj());
            MI_AddParameters("@NomeEnderecoComercial", ITO_Visitante.getNomeEnderecoComercial());
            MI_AddParameters("@NumeroEnderecoComercial", ITO_Visitante.getNumeroEnderecoComercial());
            MI_AddParameters("@ComplementoEnderecoComercial", ITO_Visitante.getComplementoEnderecoComercial());
            MI_AddParameters("@CepEnderecoComercial", ITO_Visitante.getCepEnderecoComercial());
            MI_AddParameters("@BairroEnderecoComercial", ITO_Visitante.getBairroEnderecoComercial());
            MI_AddParameters("@CidadeEnderecoComercial", ITO_Visitante.getCidadeEnderecoComercial());
            MI_AddParameters("@EstadoEnderecoComercial", ITO_Visitante.getEstadoEnderecoComercial());
            MI_AddParameters("@PaisEnderecoComercial", ITO_Visitante.getPaisEnderecoComercial());
            MI_AddParameters("@DDITelefoneComercial", ITO_Visitante.getDDITelefoneComercial());
            MI_AddParameters("@DDDTelefoneComercial", ITO_Visitante.getDDDTelefoneComercial());
            MI_AddParameters("@NumeroTelefoneComercial", ITO_Visitante.getNumeroTelefoneComercial());
            MI_AddParameters("@DDIFaxComercial", ITO_Visitante.getDDIFaxComercial());
            MI_AddParameters("@DDDFaxComercial", ITO_Visitante.getDDDFaxComercial());
            MI_AddParameters("@NumeroFaxComercial", ITO_Visitante.getNumeroFaxComercial());
            MI_AddParameters("@PaginaWebComercial", ITO_Visitante.getPaginaWebComercial());
            MI_AddParameters("@EmailComercial", ITO_Visitante.getEmailComercial());
            MI_AddParameters("@AutorizaEnvioEmailComercial", ITO_Visitante.getAutorizaEnvioEmailComercial());
            MI_AddParameters("@Res1", ITO_Visitante.getRes1());
            MI_AddParameters("@Res2", ITO_Visitante.getRes2());
            MI_AddParameters("@Res3", ITO_Visitante.getRes3());
            MI_AddParameters("@Res4", ITO_Visitante.getRes4());
            MI_AddParameters("@Res5", ITO_Visitante.getRes5());
            MI_AddParameters("@Departamento", ITO_Visitante.getDepartamento());
            MI_AddParameters("@TipoEnderecoPessoal", ITO_Visitante.getTipoEnderecoPessoal());
            MI_AddParameters("@InscricaoEstadual", ITO_Visitante.getInscricaoEstadual());
            MI_AddParameters("@TipoEnderecoComercial", ITO_Visitante.getTipoEnderecoComercial());
            MI_AddParameters("@Pergunta1", ITO_Visitante.getPergunta1());
            MI_AddParameters("@Pergunta1Outro", ITO_Visitante.getPergunta1Outro());
            MI_AddParameters("@Pergunta2", ITO_Visitante.getPergunta2());
            MI_AddParameters("@Pergunta2Outro", ITO_Visitante.getPergunta2Outro());
            MI_AddParameters("@Pergunta3", ITO_Visitante.getPergunta3());
            MI_AddParameters("@Pergunta3Outro", ITO_Visitante.getPergunta3Outro());
            MI_AddParameters("@Pergunta4", ITO_Visitante.getPergunta4());
            MI_AddParameters("@Pergunta4Outro", ITO_Visitante.getPergunta4Outro());
            MI_AddParameters("@Pergunta5", ITO_Visitante.getPergunta5());
            MI_AddParameters("@Pergunta5Outro", ITO_Visitante.getPergunta5Outro());
            MI_AddParameters("@Pergunta6", ITO_Visitante.getPergunta6());
            MI_AddParameters("@Pergunta6Outro", ITO_Visitante.getPergunta6Outro());
            MI_AddParameters("@Pergunta7", ITO_Visitante.getPergunta7());
            MI_AddParameters("@Pergunta7Outro", ITO_Visitante.getPergunta7Outro());
            MI_AddParameters("@Pergunta8", ITO_Visitante.getPergunta8());
            MI_AddParameters("@Pergunta8Outro", ITO_Visitante.getPergunta8Outro());
            MI_AddParameters("@Pergunta9", ITO_Visitante.getPergunta9());
            MI_AddParameters("@Pergunta9Outro", ITO_Visitante.getPergunta9Outro());
            MI_AddParameters("@Pergunta10", ITO_Visitante.getPergunta10());
            MI_AddParameters("@Pergunta10Outro", ITO_Visitante.getPergunta10Outro());
            MI_AddParameters("@Pergunta11", ITO_Visitante.getPergunta11());
            MI_AddParameters("@Pergunta11Outro", ITO_Visitante.getPergunta11Outro());
            MI_AddParameters("@Pergunta12", ITO_Visitante.getPergunta12());
            MI_AddParameters("@Pergunta12Outro", ITO_Visitante.getPergunta12Outro());
            MI_AddParameters("@Pergunta13", ITO_Visitante.getPergunta13());
            MI_AddParameters("@Pergunta13Outro", ITO_Visitante.getPergunta13Outro());
            MI_AddParameters("@Pergunta14", ITO_Visitante.getPergunta14());
            MI_AddParameters("@Pergunta14Outro", ITO_Visitante.getPergunta14Outro());
            MI_AddParameters("@Pergunta15", ITO_Visitante.getPergunta15());
            MI_AddParameters("@Pergunta15Outro", ITO_Visitante.getPergunta15Outro());
            MI_AddParameters("@Pergunta16", ITO_Visitante.getPergunta16());
            MI_AddParameters("@Pergunta16Outro", ITO_Visitante.getPergunta16Outro());
            MI_AddParameters("@Pergunta17", ITO_Visitante.getPergunta17());
            MI_AddParameters("@Pergunta17Outro", ITO_Visitante.getPergunta17Outro());
            MI_AddParameters("@Pergunta18", ITO_Visitante.getPergunta18());
            MI_AddParameters("@Pergunta18Outro", ITO_Visitante.getPergunta18Outro());
            MI_AddParameters("@Pergunta19", ITO_Visitante.getPergunta19());
            MI_AddParameters("@Pergunta19Outro", ITO_Visitante.getPergunta19Outro());
            MI_AddParameters("@Pergunta20", ITO_Visitante.getPergunta20());
            MI_AddParameters("@Pergunta20Outro", ITO_Visitante.getPergunta20Outro());
            MI_AddParameters("@Status", ITO_Visitante.getStatus());
            MI_AddParameters("@Localizese", ITO_Visitante.getLocalizese());
            MI_AddParameters("@Evento", ITO_Visitante.getEvento());
            MI_AddParameters("@EventoPrincipal", ITO_Visitante.getEventoPrincipal());
            MI_AddParameters("@NumeroImportacao", ITO_Visitante.getNumeroImportacao());

            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public void MS_Excluir(CTO_Visitante ITO_Visitante)
        {
            MI_SetSql ("DELETE FROM tblVisitante WHERE ObjRef = @ObjRef");
            MI_AddParameters("@ObjRef", ITO_Visitante.getObjRef());
            
            MI_ExecuteNonQuery();
            MI_FecharConexao();
        }

        public CTO_Visitante MS_Obter(int pritObjRef)
        {
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql ("SELECT " + 
                       "          NumeroTerminal, " +
                       "          Chave, " +
                       "          CodigoBarras, " +
                       "          EmissaoCracha, " +
                       "          PresenteDia1, " +
                       "          PresenteDia2, " +
                       "          PresenteDia3, " +
                       "          PresenteDia4, " +
                       "          PresenteDia5, " +
                       "          OrigemInsercao, " +
                       "          DataOrigemInsercao, " +
                       "          HoraOrigemInsercao, " +
                       "          UsuarioOrigemInsercao, " +
                       "          OrigemAlteracao, " +
                       "          DataOrigemAlteracao, " +
                       "          HoraOrigemAlteracao, " +
                       "          UsuarioOrigemAlteracao, " +
                       "          MetodoInsercao, " +
                       "          ParticipouEdicao, " +
                       "          CategoriaRegistro, " +
                       "          OrigemConvite, " +
                       "          EmailLogin, " +
                       "          CPF, " +
                       "          Senha, " +
                       "          Tratamento, " +
                       "          Nome, " +
                       "          Sobrenome, " +
                       "          NomeCompleto, " +
                       "          NomeCracha, " +
                       "          Profissao, " +
                       "          Cargo, " +
                       "          Sexo, " +
                       "          DiaNascimento, " +
                       "          MesNascimento, " +
                       "          AnoNascimento, " +
                       "          DDICelular, " +
                       "          DDDCelular, " +
                       "          TelefoneCelular, " +
                       "          SmsCelularPessoal, " +
                       "          MmsCelularPessoal, " +
                       "          EmailPessoal, " +
                       "          AutorizaEnvioEmailPessoal, " +
                       "          NomeEnderecoPessoal, " +
                       "          NumeroEnderecoPessoal, " +
                       "          ComplementoEnderecoPessoal, " +
                       "          CepEnderecoPessoal, " +
                       "          BairroEnderecoPessoal, " +
                       "          CidadeEnderecoPessoal, " +
                       "          EstadoEnderecoPessoal, " +
                       "          PaisEnderecoPessoal, " +
                       "          DDITelefonePessoal, " +
                       "          DDDTelefonePessoal, " +
                       "          NumeroTelefonePessoal, " +
                       "          DDIFaxPessoal, " +
                       "          DDDFaxPessoal, " +
                       "          NumeroFaxPessoal, " +
                       "          RazaoSocial, " +
                       "          NomeFantasia, " +
                       "          EmpresaCracha, " +
                       "          Cnpj, " +
                       "          NomeEnderecoComercial, " +
                       "          NumeroEnderecoComercial, " +
                       "          ComplementoEnderecoComercial, " +
                       "          CepEnderecoComercial, " +
                       "          BairroEnderecoComercial, " +
                       "          CidadeEnderecoComercial, " +
                       "          EstadoEnderecoComercial, " +
                       "          PaisEnderecoComercial, " +
                       "          DDITelefoneComercial, " +
                       "          DDDTelefoneComercial, " +
                       "          NumeroTelefoneComercial, " +
                       "          DDIFaxComercial, " +
                       "          DDDFaxComercial, " +
                       "          NumeroFaxComercial, " +
                       "          PaginaWebComercial, " +
                       "          EmailComercial, " +
                       "          AutorizaEnvioEmailComercial, " +
                       "          Res1, " +
                       "          Res2, " +
                       "          Res3, " +
                       "          Res4, " +
                       "          Res5, " +
                       "          ObjRef, " +
                       "          Departamento, " +
                       "          TipoEnderecoPessoal, " +
                       "          InscricaoEstadual, " +
                       "          TipoEnderecoComercial, " +
                       "          Pergunta1, " +
                       "          Pergunta1Outro, " +
                       "          Pergunta2, " +
                       "          Pergunta2Outro, " +
                       "          Pergunta3, " +
                       "          Pergunta3Outro, " +
                       "          Pergunta4, " +
                       "          Pergunta4Outro, " +
                       "          Pergunta5, " +
                       "          Pergunta5Outro, " +
                       "          Pergunta6, " +
                       "          Pergunta6Outro, " +
                       "          Pergunta7, " +
                       "          Pergunta7Outro, " +
                       "          Pergunta8, " +
                       "          Pergunta8Outro, " +
                       "          Pergunta9, " +
                       "          Pergunta9Outro, " +
                       "          Pergunta10, " +
                       "          Pergunta10Outro, " +
                       "          Pergunta11, " +
                       "          Pergunta11Outro, " +
                       "          Pergunta12, " +
                       "          Pergunta12Outro, " +
                       "          Pergunta13, " +
                       "          Pergunta13Outro, " +
                       "          Pergunta14, " +
                       "          Pergunta14Outro, " +
                       "          Pergunta15, " +
                       "          Pergunta15Outro, " +
                       "          Pergunta16, " +
                       "          Pergunta16Outro, " +
                       "          Pergunta17, " +
                       "          Pergunta17Outro, " +
                       "          Pergunta18, " +
                       "          Pergunta18Outro, " +
                       "          Pergunta19, " +
                       "          Pergunta19Outro, " +
                       "          Pergunta20, " +
                       "          Pergunta20Outro, " +
                       "          Status, " +
                       "          Localizese, " +
                       "          Evento, " +
                       "          EventoPrincipal, " +
                       "          NumeroImportacao " +
                       "FROM tblVisitante " + 
                       "WHERE Objref = @ObjRef ");
            MI_AddParameters("@Objref", pritObjRef);
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Visitante = null;
            else
                ITO_Visitante = MI_DataSetToEntidade();

            MI_FecharConexao();
            return ITO_Visitante;
        }

        public CTO_Visitante MI_DataSetToEntidade()
        {
            CTO_Visitante ITO_Visitante = new CTO_Visitante();            
            try
            {
                ITO_Visitante.setNumeroTerminal(AP_DataReader.GetString(0));
            }
            catch
            {
                ITO_Visitante.setNumeroTerminal("");
            }
            try
            {
                ITO_Visitante.setChave(AP_DataReader.GetInt32(1));
            }
            catch
            {
                ITO_Visitante.setChave(0);
            }
            try
            {
                ITO_Visitante.setCodigoBarras(AP_DataReader.GetString(2));
            }
            catch
            {
                ITO_Visitante.setCodigoBarras("");
            }
            try
            {
                ITO_Visitante.setEmissaoCracha(AP_DataReader.GetString(3));
            }
            catch
            {
                ITO_Visitante.setEmissaoCracha("");
            }
            try
            {
                ITO_Visitante.setPresenteDia1(AP_DataReader.GetString(4));
            }
            catch
            {
                ITO_Visitante.setPresenteDia1("");
            }
            try
            {
                ITO_Visitante.setPresenteDia2(AP_DataReader.GetString(5));
            }
            catch
            {
                ITO_Visitante.setPresenteDia2("");
            }
            try
            {
                ITO_Visitante.setPresenteDia3(AP_DataReader.GetString(6));
            }
            catch
            {
                ITO_Visitante.setPresenteDia3("");
            }
            try
            {
                ITO_Visitante.setPresenteDia4(AP_DataReader.GetString(7));
            }
            catch
            {
                ITO_Visitante.setPresenteDia4("");
            }
            try
            {
                ITO_Visitante.setPresenteDia5(AP_DataReader.GetString(8));
            }
            catch
            {
                ITO_Visitante.setPresenteDia5("");
            }
            try
            {
                ITO_Visitante.setOrigemInsercao(AP_DataReader.GetString(9));
            }
            catch
            {
                ITO_Visitante.setOrigemInsercao("");
            }
            try
            {
            ITO_Visitante.setDataOrigemInsercao(AP_DataReader.GetDateTime(10));
            }
            catch
            {
            }
            try
            {
                ITO_Visitante.setHoraOrigemInsercao(AP_DataReader.GetString(11));
            }
            catch
            {
                ITO_Visitante.setHoraOrigemInsercao("");
            }
            try
            {
                ITO_Visitante.setUsuarioOrigemInsercao(AP_DataReader.GetString(12));
            }
            catch
            {
                ITO_Visitante.setUsuarioOrigemInsercao("");
            }
            try
            {
                ITO_Visitante.setOrigemAlteracao(AP_DataReader.GetString(13));
            }
            catch
            {
                ITO_Visitante.setOrigemAlteracao("");
            }
            try
            {
            ITO_Visitante.setDataOrigemAlteracao(AP_DataReader.GetDateTime(14));
            }
            catch
            {
            }
            try
            {
                ITO_Visitante.setHoraOrigemAlteracao(AP_DataReader.GetString(15));
            }
            catch
            {
                ITO_Visitante.setHoraOrigemAlteracao("");
            }
            try
            {
                ITO_Visitante.setUsuarioOrigemAlteracao(AP_DataReader.GetString(16));
            }
            catch
            {
                ITO_Visitante.setUsuarioOrigemAlteracao("");
            }
            try
            {
                ITO_Visitante.setMetodoInsercao(AP_DataReader.GetString(17));
            }
            catch
            {
                ITO_Visitante.setMetodoInsercao("");
            }
            try
            {
                ITO_Visitante.setParticipouEdicao(AP_DataReader.GetString(18));
            }
            catch
            {
                ITO_Visitante.setParticipouEdicao("");
            }
            try
            {
                ITO_Visitante.setCategoriaRegistro(AP_DataReader.GetString(19));
            }
            catch
            {
                ITO_Visitante.setCategoriaRegistro("");
            }
            try
            {
                ITO_Visitante.setOrigemConvite(AP_DataReader.GetString(20));
            }
            catch
            {
                ITO_Visitante.setOrigemConvite("");
            }
            try
            {
                ITO_Visitante.setEmailLogin(AP_DataReader.GetString(21));
            }
            catch
            {
                ITO_Visitante.setEmailLogin("");
            }
            try
            {
                ITO_Visitante.setCPF(AP_DataReader.GetString(22));
            }
            catch
            {
                ITO_Visitante.setCPF("");
            }
            try
            {
                ITO_Visitante.setSenha(AP_DataReader.GetString(23));
            }
            catch
            {
                ITO_Visitante.setSenha("");
            }
            try
            {
                ITO_Visitante.setTratamento(AP_DataReader.GetString(24));
            }
            catch
            {
                ITO_Visitante.setTratamento("");
            }
            try
            {
                ITO_Visitante.setNome(AP_DataReader.GetString(25));
            }
            catch
            {
                ITO_Visitante.setNome("");
            }
            try
            {
                ITO_Visitante.setSobrenome(AP_DataReader.GetString(26));
            }
            catch
            {
                ITO_Visitante.setSobrenome("");
            }
            try
            {
                ITO_Visitante.setNomeCompleto(AP_DataReader.GetString(27));
            }
            catch
            {
                ITO_Visitante.setNomeCompleto("");
            }
            try
            {
                ITO_Visitante.setNomeCracha(AP_DataReader.GetString(28));
            }
            catch
            {
                ITO_Visitante.setNomeCracha("");
            }
            try
            {
                ITO_Visitante.setProfissao(AP_DataReader.GetString(29));
            }
            catch
            {
                ITO_Visitante.setProfissao("");
            }
            try
            {
                ITO_Visitante.setCargo(AP_DataReader.GetString(30));
            }
            catch
            {
                ITO_Visitante.setCargo("");
            }
            try
            {
                ITO_Visitante.setSexo(AP_DataReader.GetString(31));
            }
            catch
            {
                ITO_Visitante.setSexo("");
            }
            try
            {
                ITO_Visitante.setDiaNascimento(AP_DataReader.GetString(32));
            }
            catch
            {
                ITO_Visitante.setDiaNascimento("");
            }
            try
            {
                ITO_Visitante.setMesNascimento(AP_DataReader.GetString(33));
            }
            catch
            {
                ITO_Visitante.setMesNascimento("");
            }
            try
            {
                ITO_Visitante.setAnoNascimento(AP_DataReader.GetString(34));
            }
            catch
            {
                ITO_Visitante.setAnoNascimento("");
            }
            try
            {
                ITO_Visitante.setDDICelular(AP_DataReader.GetString(35));
            }
            catch
            {
                ITO_Visitante.setDDICelular("");
            }
            try
            {
                ITO_Visitante.setDDDCelular(AP_DataReader.GetString(36));
            }
            catch
            {
                ITO_Visitante.setDDDCelular("");
            }
            try
            {
                ITO_Visitante.setTelefoneCelular(AP_DataReader.GetString(37));
            }
            catch
            {
                ITO_Visitante.setTelefoneCelular("");
            }
            try
            {
                ITO_Visitante.setSmsCelularPessoal(AP_DataReader.GetString(38));
            }
            catch
            {
                ITO_Visitante.setSmsCelularPessoal("");
            }
            try
            {
                ITO_Visitante.setMmsCelularPessoal(AP_DataReader.GetString(39));
            }
            catch
            {
                ITO_Visitante.setMmsCelularPessoal("");
            }
            try
            {
                ITO_Visitante.setEmailPessoal(AP_DataReader.GetString(40));
            }
            catch
            {
                ITO_Visitante.setEmailPessoal("");
            }
            try
            {
                ITO_Visitante.setAutorizaEnvioEmailPessoal(AP_DataReader.GetString(41));
            }
            catch
            {
                ITO_Visitante.setAutorizaEnvioEmailPessoal("");
            }
            try
            {
                ITO_Visitante.setNomeEnderecoPessoal(AP_DataReader.GetString(42));
            }
            catch
            {
                ITO_Visitante.setNomeEnderecoPessoal("");
            }
            try
            {
                ITO_Visitante.setNumeroEnderecoPessoal(AP_DataReader.GetString(43));
            }
            catch
            {
                ITO_Visitante.setNumeroEnderecoPessoal("");
            }
            try
            {
                ITO_Visitante.setComplementoEnderecoPessoal(AP_DataReader.GetString(44));
            }
            catch
            {
                ITO_Visitante.setComplementoEnderecoPessoal("");
            }
            try
            {
                ITO_Visitante.setCepEnderecoPessoal(AP_DataReader.GetString(45));
            }
            catch
            {
                ITO_Visitante.setCepEnderecoPessoal("");
            }
            try
            {
                ITO_Visitante.setBairroEnderecoPessoal(AP_DataReader.GetString(46));
            }
            catch
            {
                ITO_Visitante.setBairroEnderecoPessoal("");
            }
            try
            {
                ITO_Visitante.setCidadeEnderecoPessoal(AP_DataReader.GetString(47));
            }
            catch
            {
                ITO_Visitante.setCidadeEnderecoPessoal("");
            }
            try
            {
                ITO_Visitante.setEstadoEnderecoPessoal(AP_DataReader.GetString(48));
            }
            catch
            {
                ITO_Visitante.setEstadoEnderecoPessoal("");
            }
            try
            {
                ITO_Visitante.setPaisEnderecoPessoal(AP_DataReader.GetString(49));
            }
            catch
            {
                ITO_Visitante.setPaisEnderecoPessoal("");
            }
            try
            {
                ITO_Visitante.setDDITelefonePessoal(AP_DataReader.GetString(50));
            }
            catch
            {
                ITO_Visitante.setDDITelefonePessoal("");
            }
            try
            {
                ITO_Visitante.setDDDTelefonePessoal(AP_DataReader.GetString(51));
            }
            catch
            {
                ITO_Visitante.setDDDTelefonePessoal("");
            }
            try
            {
                ITO_Visitante.setNumeroTelefonePessoal(AP_DataReader.GetString(52));
            }
            catch
            {
                ITO_Visitante.setNumeroTelefonePessoal("");
            }
            try
            {
                ITO_Visitante.setDDIFaxPessoal(AP_DataReader.GetString(53));
            }
            catch
            {
                ITO_Visitante.setDDIFaxPessoal("");
            }
            try
            {
                ITO_Visitante.setDDDFaxPessoal(AP_DataReader.GetString(54));
            }
            catch
            {
                ITO_Visitante.setDDDFaxPessoal("");
            }
            try
            {
                ITO_Visitante.setNumeroFaxPessoal(AP_DataReader.GetString(55));
            }
            catch
            {
                ITO_Visitante.setNumeroFaxPessoal("");
            }
            try
            {
                ITO_Visitante.setRazaoSocial(AP_DataReader.GetString(56));
            }
            catch
            {
                ITO_Visitante.setRazaoSocial("");
            }
            try
            {
                ITO_Visitante.setNomeFantasia(AP_DataReader.GetString(57));
            }
            catch
            {
                ITO_Visitante.setNomeFantasia("");
            }
            try
            {
                ITO_Visitante.setEmpresaCracha(AP_DataReader.GetString(58));
            }
            catch
            {
                ITO_Visitante.setEmpresaCracha("");
            }
            try
            {
                ITO_Visitante.setCnpj(AP_DataReader.GetString(59));
            }
            catch
            {
                ITO_Visitante.setCnpj("");
            }
            try
            {
                ITO_Visitante.setNomeEnderecoComercial(AP_DataReader.GetString(60));
            }
            catch
            {
                ITO_Visitante.setNomeEnderecoComercial("");
            }
            try
            {
                ITO_Visitante.setNumeroEnderecoComercial(AP_DataReader.GetString(61));
            }
            catch
            {
                ITO_Visitante.setNumeroEnderecoComercial("");
            }
            try
            {
                ITO_Visitante.setComplementoEnderecoComercial(AP_DataReader.GetString(62));
            }
            catch
            {
                ITO_Visitante.setComplementoEnderecoComercial("");
            }
            try
            {
                ITO_Visitante.setCepEnderecoComercial(AP_DataReader.GetString(63));
            }
            catch
            {
                ITO_Visitante.setCepEnderecoComercial("");
            }
            try
            {
                ITO_Visitante.setBairroEnderecoComercial(AP_DataReader.GetString(64));
            }
            catch
            {
                ITO_Visitante.setBairroEnderecoComercial("");
            }
            try
            {
                ITO_Visitante.setCidadeEnderecoComercial(AP_DataReader.GetString(65));
            }
            catch
            {
                ITO_Visitante.setCidadeEnderecoComercial("");
            }
            try
            {
                ITO_Visitante.setEstadoEnderecoComercial(AP_DataReader.GetString(66));
            }
            catch
            {
                ITO_Visitante.setEstadoEnderecoComercial("");
            }
            try
            {
                ITO_Visitante.setPaisEnderecoComercial(AP_DataReader.GetString(67));
            }
            catch
            {
                ITO_Visitante.setPaisEnderecoComercial("");
            }
            try
            {
                ITO_Visitante.setDDITelefoneComercial(AP_DataReader.GetString(68));
            }
            catch
            {
                ITO_Visitante.setDDITelefoneComercial("");
            }
            try
            {
                ITO_Visitante.setDDDTelefoneComercial(AP_DataReader.GetString(69));
            }
            catch
            {
                ITO_Visitante.setDDDTelefoneComercial("");
            }
            try
            {
                ITO_Visitante.setNumeroTelefoneComercial(AP_DataReader.GetString(70));
            }
            catch
            {
                ITO_Visitante.setNumeroTelefoneComercial("");
            }
            try
            {
                ITO_Visitante.setDDIFaxComercial(AP_DataReader.GetString(71));
            }
            catch
            {
                ITO_Visitante.setDDIFaxComercial("");
            }
            try
            {
                ITO_Visitante.setDDDFaxComercial(AP_DataReader.GetString(72));
            }
            catch
            {
                ITO_Visitante.setDDDFaxComercial("");
            }
            try
            {
                ITO_Visitante.setNumeroFaxComercial(AP_DataReader.GetString(73));
            }
            catch
            {
                ITO_Visitante.setNumeroFaxComercial("");
            }
            try
            {
                ITO_Visitante.setPaginaWebComercial(AP_DataReader.GetString(74));
            }
            catch
            {
                ITO_Visitante.setPaginaWebComercial("");
            }
            try
            {
                ITO_Visitante.setEmailComercial(AP_DataReader.GetString(75));
            }
            catch
            {
                ITO_Visitante.setEmailComercial("");
            }
            try
            {
                ITO_Visitante.setAutorizaEnvioEmailComercial(AP_DataReader.GetString(76));
            }
            catch
            {
                ITO_Visitante.setAutorizaEnvioEmailComercial("");
            }
            try
            {
                ITO_Visitante.setRes1(AP_DataReader.GetString(77));
            }
            catch
            {
                ITO_Visitante.setRes1("");
            }
            try
            {
                ITO_Visitante.setRes2(AP_DataReader.GetString(78));
            }
            catch
            {
                ITO_Visitante.setRes2("");
            }
            try
            {
                ITO_Visitante.setRes3(AP_DataReader.GetString(79));
            }
            catch
            {
                ITO_Visitante.setRes3("");
            }
            try
            {
                ITO_Visitante.setRes4(AP_DataReader.GetString(80));
            }
            catch
            {
                ITO_Visitante.setRes4("");
            }
            try
            {
                ITO_Visitante.setRes5(AP_DataReader.GetString(81));
            }
            catch
            {
                ITO_Visitante.setRes5("");
            }
            try
            {
                ITO_Visitante.setObjRef(AP_DataReader.GetInt32(82));
            }
            catch
            {
                ITO_Visitante.setObjRef(0);
            }
            try
            {
                ITO_Visitante.setDepartamento(AP_DataReader.GetString(83));
            }
            catch
            {
                ITO_Visitante.setDepartamento("");
            }
            try
            {
                ITO_Visitante.setTipoEnderecoPessoal(AP_DataReader.GetString(84));
            }
            catch
            {
                ITO_Visitante.setTipoEnderecoPessoal("");
            }
            try
            {
                ITO_Visitante.setInscricaoEstadual(AP_DataReader.GetString(85));
            }
            catch
            {
                ITO_Visitante.setInscricaoEstadual("");
            }
            try
            {
                ITO_Visitante.setTipoEnderecoComercial(AP_DataReader.GetString(86));
            }
            catch
            {
                ITO_Visitante.setTipoEnderecoComercial("");
            }
            try
            {
                ITO_Visitante.setPergunta1(AP_DataReader.GetString(87));
            }
            catch
            {
                ITO_Visitante.setPergunta1("");
            }
            try
            {
                ITO_Visitante.setPergunta1Outro(AP_DataReader.GetString(88));
            }
            catch
            {
                ITO_Visitante.setPergunta1Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta2(AP_DataReader.GetString(89));
            }
            catch
            {
                ITO_Visitante.setPergunta2("");
            }
            try
            {
                ITO_Visitante.setPergunta2Outro(AP_DataReader.GetString(90));
            }
            catch
            {
                ITO_Visitante.setPergunta2Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta3(AP_DataReader.GetString(91));
            }
            catch
            {
                ITO_Visitante.setPergunta3("");
            }
            try
            {
                ITO_Visitante.setPergunta3Outro(AP_DataReader.GetString(92));
            }
            catch
            {
                ITO_Visitante.setPergunta3Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta4(AP_DataReader.GetString(93));
            }
            catch
            {
                ITO_Visitante.setPergunta4("");
            }
            try
            {
                ITO_Visitante.setPergunta4Outro(AP_DataReader.GetString(94));
            }
            catch
            {
                ITO_Visitante.setPergunta4Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta5(AP_DataReader.GetString(95));
            }
            catch
            {
                ITO_Visitante.setPergunta5("");
            }
            try
            {
                ITO_Visitante.setPergunta5Outro(AP_DataReader.GetString(96));
            }
            catch
            {
                ITO_Visitante.setPergunta5Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta6(AP_DataReader.GetString(97));
            }
            catch
            {
                ITO_Visitante.setPergunta6("");
            }
            try
            {
                ITO_Visitante.setPergunta6Outro(AP_DataReader.GetString(98));
            }
            catch
            {
                ITO_Visitante.setPergunta6Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta7(AP_DataReader.GetString(99));
            }
            catch
            {
                ITO_Visitante.setPergunta7("");
            }
            try
            {
                ITO_Visitante.setPergunta7Outro(AP_DataReader.GetString(100));
            }
            catch
            {
                ITO_Visitante.setPergunta7Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta8(AP_DataReader.GetString(101));
            }
            catch
            {
                ITO_Visitante.setPergunta8("");
            }
            try
            {
                ITO_Visitante.setPergunta8Outro(AP_DataReader.GetString(102));
            }
            catch
            {
                ITO_Visitante.setPergunta8Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta9(AP_DataReader.GetString(103));
            }
            catch
            {
                ITO_Visitante.setPergunta9("");
            }
            try
            {
                ITO_Visitante.setPergunta9Outro(AP_DataReader.GetString(104));
            }
            catch
            {
                ITO_Visitante.setPergunta9Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta10(AP_DataReader.GetString(105));
            }
            catch
            {
                ITO_Visitante.setPergunta10("");
            }
            try
            {
                ITO_Visitante.setPergunta10Outro(AP_DataReader.GetString(106));
            }
            catch
            {
                ITO_Visitante.setPergunta10Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta11(AP_DataReader.GetString(107));
            }
            catch
            {
                ITO_Visitante.setPergunta11("");
            }
            try
            {
                ITO_Visitante.setPergunta11Outro(AP_DataReader.GetString(108));
            }
            catch
            {
                ITO_Visitante.setPergunta11Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta12(AP_DataReader.GetString(109));
            }
            catch
            {
                ITO_Visitante.setPergunta12("");
            }
            try
            {
                ITO_Visitante.setPergunta12Outro(AP_DataReader.GetString(110));
            }
            catch
            {
                ITO_Visitante.setPergunta12Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta13(AP_DataReader.GetString(111));
            }
            catch
            {
                ITO_Visitante.setPergunta13("");
            }
            try
            {
                ITO_Visitante.setPergunta13Outro(AP_DataReader.GetString(112));
            }
            catch
            {
                ITO_Visitante.setPergunta13Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta14(AP_DataReader.GetString(113));
            }
            catch
            {
                ITO_Visitante.setPergunta14("");
            }
            try
            {
                ITO_Visitante.setPergunta14Outro(AP_DataReader.GetString(114));
            }
            catch
            {
                ITO_Visitante.setPergunta14Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta15(AP_DataReader.GetString(115));
            }
            catch
            {
                ITO_Visitante.setPergunta15("");
            }
            try
            {
                ITO_Visitante.setPergunta15Outro(AP_DataReader.GetString(116));
            }
            catch
            {
                ITO_Visitante.setPergunta15Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta16(AP_DataReader.GetString(117));
            }
            catch
            {
                ITO_Visitante.setPergunta16("");
            }
            try
            {
                ITO_Visitante.setPergunta16Outro(AP_DataReader.GetString(118));
            }
            catch
            {
                ITO_Visitante.setPergunta16Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta17(AP_DataReader.GetString(119));
            }
            catch
            {
                ITO_Visitante.setPergunta17("");
            }
            try
            {
                ITO_Visitante.setPergunta17Outro(AP_DataReader.GetString(120));
            }
            catch
            {
                ITO_Visitante.setPergunta17Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta18(AP_DataReader.GetString(121));
            }
            catch
            {
                ITO_Visitante.setPergunta18("");
            }
            try
            {
                ITO_Visitante.setPergunta18Outro(AP_DataReader.GetString(122));
            }
            catch
            {
                ITO_Visitante.setPergunta18Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta19(AP_DataReader.GetString(123));
            }
            catch
            {
                ITO_Visitante.setPergunta19("");
            }
            try
            {
                ITO_Visitante.setPergunta19Outro(AP_DataReader.GetString(124));
            }
            catch
            {
                ITO_Visitante.setPergunta19Outro("");
            }
            try
            {
                ITO_Visitante.setPergunta20(AP_DataReader.GetString(125));
            }
            catch
            {
                ITO_Visitante.setPergunta20("");
            }
            try
            {
                ITO_Visitante.setPergunta20Outro(AP_DataReader.GetString(126));
            }
            catch
            {
                ITO_Visitante.setPergunta20Outro("");
            }
            try
            {
                ITO_Visitante.setStatus(AP_DataReader.GetString(127));
            }
            catch
            {
                ITO_Visitante.setStatus("");
            }
            try
            {
                ITO_Visitante.setLocalizese(AP_DataReader.GetString(128));
            }
            catch
            {
                ITO_Visitante.setLocalizese("");
            }
            //NOVOS
            try
            {
                ITO_Visitante.setEvento(AP_DataReader.GetString(129));
            }
            catch
            {
                ITO_Visitante.setEvento("");
            }
            try
            {
                ITO_Visitante.setEventoPrincipal(AP_DataReader.GetString(130));
            }
            catch
            {
                ITO_Visitante.setEventoPrincipal("");
            }
            try
            {
                ITO_Visitante.setNumeroImportacao(AP_DataReader.GetInt32(131));
            }
            catch
            {
                ITO_Visitante.setNumeroImportacao(0);
            }
            return ITO_Visitante;
        }

        public CTO_Visitante MS_ObterTodos()
        {
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql ("SELECT " + 
                       "          NumeroTerminal, " +
                       "          Chave, " +
                       "          CodigoBarras, " +
                       "          EmissaoCracha, " +
                       "          PresenteDia1, " +
                       "          PresenteDia2, " +
                       "          PresenteDia3, " +
                       "          PresenteDia4, " +
                       "          PresenteDia5, " +
                       "          OrigemInsercao, " +
                       "          DataOrigemInsercao, " +
                       "          HoraOrigemInsercao, " +
                       "          UsuarioOrigemInsercao, " +
                       "          OrigemAlteracao, " +
                       "          DataOrigemAlteracao, " +
                       "          HoraOrigemAlteracao, " +
                       "          UsuarioOrigemAlteracao, " +
                       "          MetodoInsercao, " +
                       "          ParticipouEdicao, " +
                       "          CategoriaRegistro, " +
                       "          OrigemConvite, " +
                       "          EmailLogin, " +
                       "          CPF, " +
                       "          Senha, " +
                       "          Tratamento, " +
                       "          Nome, " +
                       "          Sobrenome, " +
                       "          NomeCompleto, " +
                       "          NomeCracha, " +
                       "          Profissao, " +
                       "          Cargo, " +
                       "          Sexo, " +
                       "          DiaNascimento, " +
                       "          MesNascimento, " +
                       "          AnoNascimento, " +
                       "          DDICelular, " +
                       "          DDDCelular, " +
                       "          TelefoneCelular, " +
                       "          SmsCelularPessoal, " +
                       "          MmsCelularPessoal, " +
                       "          EmailPessoal, " +
                       "          AutorizaEnvioEmailPessoal, " +
                       "          NomeEnderecoPessoal, " +
                       "          NumeroEnderecoPessoal, " +
                       "          ComplementoEnderecoPessoal, " +
                       "          CepEnderecoPessoal, " +
                       "          BairroEnderecoPessoal, " +
                       "          CidadeEnderecoPessoal, " +
                       "          EstadoEnderecoPessoal, " +
                       "          PaisEnderecoPessoal, " +
                       "          DDITelefonePessoal, " +
                       "          DDDTelefonePessoal, " +
                       "          NumeroTelefonePessoal, " +
                       "          DDIFaxPessoal, " +
                       "          DDDFaxPessoal, " +
                       "          NumeroFaxPessoal, " +
                       "          RazaoSocial, " +
                       "          NomeFantasia, " +
                       "          EmpresaCracha, " +
                       "          Cnpj, " +
                       "          NomeEnderecoComercial, " +
                       "          NumeroEnderecoComercial, " +
                       "          ComplementoEnderecoComercial, " +
                       "          CepEnderecoComercial, " +
                       "          BairroEnderecoComercial, " +
                       "          CidadeEnderecoComercial, " +
                       "          EstadoEnderecoComercial, " +
                       "          PaisEnderecoComercial, " +
                       "          DDITelefoneComercial, " +
                       "          DDDTelefoneComercial, " +
                       "          NumeroTelefoneComercial, " +
                       "          DDIFaxComercial, " +
                       "          DDDFaxComercial, " +
                       "          NumeroFaxComercial, " +
                       "          PaginaWebComercial, " +
                       "          EmailComercial, " +
                       "          AutorizaEnvioEmailComercial, " +
                       "          Res1, " +
                       "          Res2, " +
                       "          Res3, " +
                       "          Res4, " +
                       "          Res5, " +
                       "          ObjRef, " +
                       "          Departamento, " +
                       "          TipoEnderecoPessoal, " +
                       "          InscricaoEstadual, " +
                       "          TipoEnderecoComercial, " +
                       "          Pergunta1, " +
                       "          Pergunta1Outro, " +
                       "          Pergunta2, " +
                       "          Pergunta2Outro, " +
                       "          Pergunta3, " +
                       "          Pergunta3Outro, " +
                       "          Pergunta4, " +
                       "          Pergunta4Outro, " +
                       "          Pergunta5, " +
                       "          Pergunta5Outro, " +
                       "          Pergunta6, " +
                       "          Pergunta6Outro, " +
                       "          Pergunta7, " +
                       "          Pergunta7Outro, " +
                       "          Pergunta8, " +
                       "          Pergunta8Outro, " +
                       "          Pergunta9, " +
                       "          Pergunta9Outro, " +
                       "          Pergunta10, " +
                       "          Pergunta10Outro, " +
                       "          Pergunta11, " +
                       "          Pergunta11Outro, " +
                       "          Pergunta12, " +
                       "          Pergunta12Outro, " +
                       "          Pergunta13, " +
                       "          Pergunta13Outro, " +
                       "          Pergunta14, " +
                       "          Pergunta14Outro, " +
                       "          Pergunta15, " +
                       "          Pergunta15Outro, " +
                       "          Pergunta16, " +
                       "          Pergunta16Outro, " +
                       "          Pergunta17, " +
                       "          Pergunta17Outro, " +
                       "          Pergunta18, " +
                       "          Pergunta18Outro, " +
                       "          Pergunta19, " +
                       "          Pergunta19Outro, " +
                       "          Pergunta20, " +
                       "          Pergunta20Outro, " +
                       "          Status, " +
                       "          Localizese, " +
                       "          Evento, " +
                       "          EventoPrincipal, " +
                       "          NumeroImportacao " +
                       "FROM tblVisitante " );
            MI_ExecuteQuery();
            if (MI_EOF()) 
                ITO_Visitante = null;
            else
                ITO_Visitante = MI_DataSetToEntidade();
            return ITO_Visitante;
        }

        public CTO_Visitante MS_ObterProximo()
        {
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_ProximoRegistro();
            if (MI_EOF())
                ITO_Visitante = null;
            else
                ITO_Visitante = MI_DataSetToEntidade();
                return ITO_Visitante;
        }

        public int MS_NovoObjRef()
        {
            int itObjRef;
            MI_SetSql("Select Max(ObjRef) from tblVisitante");
            MI_ExecuteQuery();
            if (AP_DataReader.IsDBNull(0))
            {
                itObjRef = 1;
            }
            else
            {
                itObjRef = AP_DataReader.GetInt32(0);
                itObjRef++;
            }
            MI_FecharConexao();
            return itObjRef;
        }

        public DataTable MS_ObterGrid()
        {
            MI_SetSql("Select * from tblVisitante");
            return MI_ExecuteDataSet();
        }

        public CTO_Visitante MS_Obter1()
        {
            CTO_Visitante ITO_Visitante = new CTO_Visitante();
            MI_SetSql("SELECT top(1)" +
                       "          NumeroTerminal, " +
                       "          Chave, " +
                       "          CodigoBarras, " +
                       "          EmissaoCracha, " +
                       "          PresenteDia1, " +
                       "          PresenteDia2, " +
                       "          PresenteDia3, " +
                       "          PresenteDia4, " +
                       "          PresenteDia5, " +
                       "          OrigemInsercao, " +
                       "          DataOrigemInsercao, " +
                       "          HoraOrigemInsercao, " +
                       "          UsuarioOrigemInsercao, " +
                       "          OrigemAlteracao, " +
                       "          DataOrigemAlteracao, " +
                       "          HoraOrigemAlteracao, " +
                       "          UsuarioOrigemAlteracao, " +
                       "          MetodoInsercao, " +
                       "          ParticipouEdicao, " +
                       "          CategoriaRegistro, " +
                       "          OrigemConvite, " +
                       "          EmailLogin, " +
                       "          CPF, " +
                       "          Senha, " +
                       "          Tratamento, " +
                       "          Nome, " +
                       "          Sobrenome, " +
                       "          NomeCompleto, " +
                       "          NomeCracha, " +
                       "          Profissao, " +
                       "          Cargo, " +
                       "          Sexo, " +
                       "          DiaNascimento, " +
                       "          MesNascimento, " +
                       "          AnoNascimento, " +
                       "          DDICelular, " +
                       "          DDDCelular, " +
                       "          TelefoneCelular, " +
                       "          SmsCelularPessoal, " +
                       "          MmsCelularPessoal, " +
                       "          EmailPessoal, " +
                       "          AutorizaEnvioEmailPessoal, " +
                       "          NomeEnderecoPessoal, " +
                       "          NumeroEnderecoPessoal, " +
                       "          ComplementoEnderecoPessoal, " +
                       "          CepEnderecoPessoal, " +
                       "          BairroEnderecoPessoal, " +
                       "          CidadeEnderecoPessoal, " +
                       "          EstadoEnderecoPessoal, " +
                       "          PaisEnderecoPessoal, " +
                       "          DDITelefonePessoal, " +
                       "          DDDTelefonePessoal, " +
                       "          NumeroTelefonePessoal, " +
                       "          DDIFaxPessoal, " +
                       "          DDDFaxPessoal, " +
                       "          NumeroFaxPessoal, " +
                       "          RazaoSocial, " +
                       "          NomeFantasia, " +
                       "          EmpresaCracha, " +
                       "          Cnpj, " +
                       "          NomeEnderecoComercial, " +
                       "          NumeroEnderecoComercial, " +
                       "          ComplementoEnderecoComercial, " +
                       "          CepEnderecoComercial, " +
                       "          BairroEnderecoComercial, " +
                       "          CidadeEnderecoComercial, " +
                       "          EstadoEnderecoComercial, " +
                       "          PaisEnderecoComercial, " +
                       "          DDITelefoneComercial, " +
                       "          DDDTelefoneComercial, " +
                       "          NumeroTelefoneComercial, " +
                       "          DDIFaxComercial, " +
                       "          DDDFaxComercial, " +
                       "          NumeroFaxComercial, " +
                       "          PaginaWebComercial, " +
                       "          EmailComercial, " +
                       "          AutorizaEnvioEmailComercial, " +
                       "          Res1, " +
                       "          Res2, " +
                       "          Res3, " +
                       "          Res4, " +
                       "          Res5, " +
                       "          ObjRef, " +
                       "          Departamento, " +
                       "          TipoEnderecoPessoal, " +
                       "          InscricaoEstadual, " +
                       "          TipoEnderecoComercial, " +
                       "          Pergunta1, " +
                       "          Pergunta1Outro, " +
                       "          Pergunta2, " +
                       "          Pergunta2Outro, " +
                       "          Pergunta3, " +
                       "          Pergunta3Outro, " +
                       "          Pergunta4, " +
                       "          Pergunta4Outro, " +
                       "          Pergunta5, " +
                       "          Pergunta5Outro, " +
                       "          Pergunta6, " +
                       "          Pergunta6Outro, " +
                       "          Pergunta7, " +
                       "          Pergunta7Outro, " +
                       "          Pergunta8, " +
                       "          Pergunta8Outro, " +
                       "          Pergunta9, " +
                       "          Pergunta9Outro, " +
                       "          Pergunta10, " +
                       "          Pergunta10Outro, " +
                       "          Pergunta11, " +
                       "          Pergunta11Outro, " +
                       "          Pergunta12, " +
                       "          Pergunta12Outro, " +
                       "          Pergunta13, " +
                       "          Pergunta13Outro, " +
                       "          Pergunta14, " +
                       "          Pergunta14Outro, " +
                       "          Pergunta15, " +
                       "          Pergunta15Outro, " +
                       "          Pergunta16, " +
                       "          Pergunta16Outro, " +
                       "          Pergunta17, " +
                       "          Pergunta17Outro, " +
                       "          Pergunta18, " +
                       "          Pergunta18Outro, " +
                       "          Pergunta19, " +
                       "          Pergunta19Outro, " +
                       "          Pergunta20, " +
                       "          Pergunta20Outro, " +
                       "          Status, " +
                       "          Localizese, " +
                       "          Evento, " +
                       "          EventoPrincipal, " +
                       "          NumeroImportacao " +
                       "FROM tblVisitante ");
            MI_ExecuteQuery();
            if (MI_EOF())
                ITO_Visitante = null;
            else
                ITO_Visitante = MI_DataSetToEntidade();
            return ITO_Visitante;
        }

    }
}

