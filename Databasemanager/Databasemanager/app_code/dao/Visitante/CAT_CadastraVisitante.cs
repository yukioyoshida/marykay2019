using System;
using System.Data;
using SPI;

public class CAT_CadastrarVisitante : CAB_CadastrarVisitante
{
     public CAT_CadastrarVisitante()
     {
     }
     public override void MS_Incluir(CTO_Visitante ITO_Visitante)
     {
          DAO_Visitante IDAO_Visitante = new DAO_Visitante();
          IDAO_Visitante.MS_Incluir(ITO_Visitante);
     }
     public override void MS_Alterar(CTO_Visitante ITO_Visitante)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         IDAO_Visitante.MS_Alterar(ITO_Visitante);
     }
     public override void MS_Excluir(CTO_Visitante ITO_Visitante)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         IDAO_Visitante.MS_Excluir(ITO_Visitante);
     }
     public override CTO_Visitante MS_Obter(int pritObjRef)
     {
           DAO_Visitante IDAO_Visitante = new DAO_Visitante();
           return IDAO_Visitante.MS_Obter(pritObjRef);
     }

     public override CTO_Visitante MS_Obter1()
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_Obter1();
     }

     public override CTO_Visitante MS_ObterPorCPF(string prstCPF)
     {
           DAO_Visitante IDAO_Visitante = new DAO_Visitante();
           return IDAO_Visitante.MS_ObterPorCPF(prstCPF);
     }

     public override DataTable MS_ObterPorCPFTabela(string prstCPF)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_ObterPorCPFTabela(prstCPF);
     }

     public override CTO_Visitante MS_ObterPorNome(string prstNome)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_ObterPorNome(prstNome);
     }
     
     public override DataTable MS_ObterPorNomeTabela(string prstNome)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_ObterPorNomeTabela(prstNome);
     }

     public override CTO_Visitante MS_ObterPorCodigo(string prstCodigo)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_ObterPorCodigo(prstCodigo);
     }

     public DataTable MS_ObterPorCodigoData(string prstCodigo)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_ObterPorCodigoData(prstCodigo);
     }

    public DataTable MS_ObterPorCodigoDataConsultora(string prstCodigo)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterPorCodigoDataConsultora(prstCodigo);
    }

    public DataTable MS_ObterPorCodigoConsultoraImportacao(string stNrConsultora, string prNomeCompleto, string TipoInscricao)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterPorCodigoConsultoraImportacao(stNrConsultora, prNomeCompleto, TipoInscricao);
    }

    public DataTable MS_ObterPorCodigoConsultora(string stNrConsultora, string prNomeCompleto)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_ObterPorCodigoConsultora(stNrConsultora, prNomeCompleto);
     }

    public DataTable MS_ObterPorCodigoConsultoraConjuge(string stNrConsultora, string prNomeCompleto)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterPorCodigoConsultoraConjuge(stNrConsultora, prNomeCompleto);
    }

    public DataTable MS_ObterPremiacoes(string stNrConsultora)
    {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_ObterPremiacoes(stNrConsultora);
    }

    public DataTable MS_ObterPremiacoesMimo(string stNrConsultora)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterPremiacoesMimo(stNrConsultora);
    }

    public DataTable MS_ObterReconhecimentos(string stNrConsultora)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterReconhecimentos(stNrConsultora);
    }


    public override CTO_Visitante MS_ObterPorEmail(string prstEmail)
     {
           DAO_Visitante IDAO_Visitante = new DAO_Visitante();
           return IDAO_Visitante.MS_ObterPorEmail(prstEmail);
     }
     public override DataTable MS_LocalizarRegistroPorFiltro(string prstCriterio, string prstAtributo)
     {
         DAO_Visitante IDAO_Visitante = new DAO_Visitante();
         return IDAO_Visitante.MS_LocalizarRegistroPorFiltro(prstCriterio,prstAtributo);
     }
     
    public override string MS_GerarCodigoBarras()
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_GerarCodigoBarras();
    }
    public override DataTable MS_ObterRegistro(string prstPalavra, string prContendo, string stTipoBusca)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterRegistro(prstPalavra, prContendo, stTipoBusca);
    }
    
    public override void MS_ConfirmaImpressao(Int64 prstCodigo)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_ConfirmaImpressao(prstCodigo);
    }

    public override DataTable MS_ObterCampos()
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterCampos();
    }
    
    public override DataTable MS_ObterRegistroGrid(string prstChave)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterRegistroGrid(prstChave);
    }

    public DataTable MS_ObterCategorias()
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterCategorias();
    }
    public DataTable MS_ObterFormaPagamento()
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterFormaPagamento();
    }

    public DataTable MS_ObterEvento()
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterEvento();
    }

    public override CTO_Visitante MS_ObterPorLocalizese(string prstLocalizese)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterPorLocalizese(prstLocalizese);
    }

    public DataTable MS_ObterPorLocalizeseTabela(string prstLocalizese)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterPorLocalizeseTabela(prstLocalizese);
    }

    public DataTable MS_VerificaQtdCNPJ(string stCNPJ)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_VerificaQtdCNPJ(stCNPJ);
    }

    public DataTable MS_Excessoes(string prCNPJ)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_Excessoes(prCNPJ);
    }

    public DataTable MS_ObterTodosParticipantesCertificados(int stLimite)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterTodosParticipantesCertificados(stLimite);
    }

    public DataTable MS_ObterSenha(string stSenha)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterSenha(stSenha);
    }

    // ========================================================================

    public DataTable MS_ObterProximoImportacao()
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterProximoImportacao();
    }

    public void MI_ConfirmarImportacao(string prCodigoBarras)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MI_ConfirmarImportacao(prCodigoBarras);
    }

    public DataTable MS_ObterPremioDaConsultora(string cdPremiacao)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterPremioDaConsultora(cdPremiacao);
    }
    public DataTable MS_ObterPremio(string cdPremio)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterPremio(cdPremio);
    }

    
    public void MS_ConfirmarRetiradaPremio(string cdPremiacao, string prUsuario)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_ConfirmarRetiradaPremio(cdPremiacao, prUsuario);
    }

    public void MS_ConfirmarRetiradaPremioComEscolha(string cdPremiacao, string prUsuario, string prPremio)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_ConfirmarRetiradaPremioComEscolha(cdPremiacao, prUsuario, prPremio);
    }

    public void MS_ConfirmarEsconhaDoPremio(string cdPremiacao, string prUsuario, string prPremio)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_ConfirmarEsconhaDoPremio(cdPremiacao, prUsuario, prPremio);
    }

    public void MS_CancelarCodigosPararelos(string prCodigoBarras)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_CancelarCodigosPararelos(prCodigoBarras);
    }

    public DataTable MS_ObterQuantidadeImpressoes(string prCodigoOriginal)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterQuantidadeImpressoes(prCodigoOriginal);
    }
    public void MS_GravarCodigoPararelo(string prCodigoOriginal, string stCodigoPararelo)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_GravarCodigoPararelo(prCodigoOriginal, stCodigoPararelo);
    }

    public DataTable MS_ObterPremiosDaConsultora(string prCdReconhecimento, string prTipo)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterPremiosDaConsultora(prCdReconhecimento, prTipo);
    }

    public void MS_EscolherJoiaLocal(string prCdItemJoia, string prJoia)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_EscolherJoiaLocal(prCdItemJoia, prJoia);
    }

    public DataTable MS_ObterReconhecimentosEspecifico(string prCdItemJoia)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterReconhecimentosEspecifico(prCdItemJoia);
    }

    public void MS_SalvarObservacoes(string prCdItemJoia, string prObservacoes)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_SalvarObservacoes(prCdItemJoia, prObservacoes);
    }

    public void MS_ConfirmarRetiradaPremioNew(string prCdItemJoia)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_ConfirmarRetiradaPremioNew(prCdItemJoia);
    }

    public DataTable MS_ObterPremioNew(string prCdJoia)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterPremioNew(prCdJoia);
    }

    public DataTable MS_ObterItemPremio(string prCdItem)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterItemPremio(prCdItem);
    }

    public DataTable MS_ObterCodigoCredencial(string prCodigoBarras)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterCodigoCredencial(prCodigoBarras);
    }

    public DataTable MS_ObterTodosOsItens(string prCodigoBarras)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterTodosOsItens(prCodigoBarras);
    }

    public void MS_IncluirReconhecimento(string prNrConsultora, string prReconhecimento)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_IncluirReconhecimento(prNrConsultora, prReconhecimento);
    }

    public DataTable MS_ObterUltimoReconhecimentoInserido(string prNrConsultora, string prReconhecimento)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterUltimoReconhecimentoInserido(prNrConsultora, prReconhecimento);
    }

    public void MS_IncluirReconhecimento_Item(string prReconhecimento, string stPremio)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_IncluirReconhecimento_Item(prReconhecimento, stPremio);
    }

    public void MS_IncluirItemNaListaDePremios(string prNome)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_IncluirItemNaListaDePremios(prNome);
    }

    public DataTable MS_ObterPremioPorNome(string prNome)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterPremioPorNome(prNome);
    }

    public void MS_TrocarJoia(string prCdPremio, string prCdJoia)
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        IDAO_Visitante.MS_TrocarJoia(prCdPremio, prCdJoia);
    }

    public DataTable MS_ObterTodosOsRegistros()
    {
        DAO_Visitante IDAO_Visitante = new DAO_Visitante();
        return IDAO_Visitante.MS_ObterTodosOsRegistros();
    }

    public void MS_AprovarBase(string stUsuarioAprovacao)
    {
        new DAO_Visitante().MS_AprovarBase(stUsuarioAprovacao);
    }
}

