using System;
namespace SPI 
{

    public class CTO_Visitante 
    {
        //  Declaracao dos atributos Puros
        string AP_NumeroTerminal = "";
        int AP_Chave;
        string AP_CodigoBarras = "";
        string AP_EmissaoCracha = "";
        string AP_PresenteDia1 = "";
        string AP_PresenteDia2 = "";
        string AP_PresenteDia3 = "";
        string AP_PresenteDia4 = "";
        string AP_PresenteDia5 = "";
        string AP_OrigemInsercao = "";
        DateTime AP_DataOrigemInsercao;
        string AP_HoraOrigemInsercao = "";
        string AP_UsuarioOrigemInsercao = "";
        string AP_OrigemAlteracao = "";
        DateTime AP_DataOrigemAlteracao;
        string AP_HoraOrigemAlteracao = "";
        string AP_UsuarioOrigemAlteracao = "";
        string AP_MetodoInsercao = "";
        string AP_ParticipouEdicao = "";
        string AP_CategoriaRegistro = "";
        string AP_OrigemConvite = "";
        string AP_EmailLogin = "";
        string AP_CPF = "";
        string AP_Senha = "";
        string AP_Tratamento = "";
        string AP_Nome = "";
        string AP_Sobrenome = "";
        string AP_NomeCompleto = "";
        string AP_NomeCracha = "";
        string AP_Profissao = "";
        string AP_Cargo = "";
        string AP_Sexo = "";
        string AP_DiaNascimento = "";
        string AP_MesNascimento = "";
        string AP_AnoNascimento = "";
        string AP_DDICelular = "";
        string AP_DDDCelular = "";
        string AP_TelefoneCelular = "";
        string AP_SmsCelularPessoal = "";
        string AP_MmsCelularPessoal = "";
        string AP_EmailPessoal = "";
        string AP_AutorizaEnvioEmailPessoal = "";
        string AP_NomeEnderecoPessoal = "";
        string AP_NumeroEnderecoPessoal = "";
        string AP_ComplementoEnderecoPessoal = "";
        string AP_CepEnderecoPessoal = "";
        string AP_BairroEnderecoPessoal = "";
        string AP_CidadeEnderecoPessoal = "";
        string AP_EstadoEnderecoPessoal = "";
        string AP_PaisEnderecoPessoal = "";
        string AP_DDITelefonePessoal = "";
        string AP_DDDTelefonePessoal = "";
        string AP_NumeroTelefonePessoal = "";
        string AP_DDIFaxPessoal = "";
        string AP_DDDFaxPessoal = "";
        string AP_NumeroFaxPessoal = "";
        string AP_RazaoSocial = "";
        string AP_NomeFantasia = "";
        string AP_EmpresaCracha = "";
        string AP_Cnpj = "";
        string AP_NomeEnderecoComercial = "";
        string AP_NumeroEnderecoComercial = "";
        string AP_ComplementoEnderecoComercial = "";
        string AP_CepEnderecoComercial = "";
        string AP_BairroEnderecoComercial = "";
        string AP_CidadeEnderecoComercial = "";
        string AP_EstadoEnderecoComercial = "";
        string AP_PaisEnderecoComercial = "";
        string AP_DDITelefoneComercial = "";
        string AP_DDDTelefoneComercial = "";
        string AP_NumeroTelefoneComercial = "";
        string AP_DDIFaxComercial = "";
        string AP_DDDFaxComercial = "";
        string AP_NumeroFaxComercial = "";
        string AP_PaginaWebComercial = "";
        string AP_EmailComercial = "";
        string AP_AutorizaEnvioEmailComercial = "";
        string AP_Res1 = "";
        string AP_Res2 = "";
        string AP_Res3 = "";
        string AP_Res4 = "";
        string AP_Res5 = "";
        int AP_ObjRef;
        string AP_Departamento = "";
        string AP_TipoEnderecoPessoal = "";
        string AP_InscricaoEstadual = "";
        string AP_TipoEnderecoComercial = "";
        string AP_Pergunta1 = "";
        string AP_Pergunta1Outro = "";
        string AP_Pergunta2 = "";
        string AP_Pergunta2Outro = "";
        string AP_Pergunta3 = "";
        string AP_Pergunta3Outro = "";
        string AP_Pergunta4 = "";
        string AP_Pergunta4Outro = "";
        string AP_Pergunta5 = "";
        string AP_Pergunta5Outro = "";
        string AP_Pergunta6 = "";
        string AP_Pergunta6Outro = "";
        string AP_Pergunta7 = "";
        string AP_Pergunta7Outro = "";
        string AP_Pergunta8 = "";
        string AP_Pergunta8Outro = "";
        string AP_Pergunta9 = "";
        string AP_Pergunta9Outro = "";
        string AP_Pergunta10 = "";
        string AP_Pergunta10Outro = "";
        string AP_Pergunta11 = "";
        string AP_Pergunta11Outro = "";
        string AP_Pergunta12 = "";
        string AP_Pergunta12Outro = "";
        string AP_Pergunta13 = "";
        string AP_Pergunta13Outro = "";
        string AP_Pergunta14 = "";
        string AP_Pergunta14Outro = "";
        string AP_Pergunta15 = "";
        string AP_Pergunta15Outro = "";
        string AP_Pergunta16 = "";
        string AP_Pergunta16Outro = "";
        string AP_Pergunta17 = "";
        string AP_Pergunta17Outro = "";
        string AP_Pergunta18 = "";
        string AP_Pergunta18Outro = "";
        string AP_Pergunta19 = "";
        string AP_Pergunta19Outro = "";
        string AP_Pergunta20 = "";
        string AP_Pergunta20Outro = "";
        string AP_Status = "";
        string AP_Localizese = "";
        string AP_Evento = "";
        string AP_EventoPrincipal = "";
        Int32 AP_NumeroImportacao = 0;

        public CTO_Visitante () 
        {                      
        }                      
          //  Declaracao dos Metodos Gets
        public string getNumeroTerminal()
        {
            return AP_NumeroTerminal;
        }
        public int getChave()
        {
            return AP_Chave;
        }
        public string getCodigoBarras()
        {
            return AP_CodigoBarras;
        }
        public string getEmissaoCracha()
        {
            return AP_EmissaoCracha;
        }
        public string getPresenteDia1()
        {
            return AP_PresenteDia1;
        }
        public string getPresenteDia2()
        {
            return AP_PresenteDia2;
        }
        public string getPresenteDia3()
        {
            return AP_PresenteDia3;
        }
        public string getPresenteDia4()
        {
            return AP_PresenteDia4;
        }
        public string getPresenteDia5()
        {
            return AP_PresenteDia5;
        }
        public string getOrigemInsercao()
        {
            return AP_OrigemInsercao;
        }
        public DateTime getDataOrigemInsercao()
        {
            return AP_DataOrigemInsercao;
        }
        public string getHoraOrigemInsercao()
        {
            return AP_HoraOrigemInsercao;
        }
        public string getUsuarioOrigemInsercao()
        {
            return AP_UsuarioOrigemInsercao;
        }
        public string getOrigemAlteracao()
        {
            return AP_OrigemAlteracao;
        }
        public DateTime getDataOrigemAlteracao()
        {
            return AP_DataOrigemAlteracao;
        }
        public string getHoraOrigemAlteracao()
        {
            return AP_HoraOrigemAlteracao;
        }
        public string getUsuarioOrigemAlteracao()
        {
            return AP_UsuarioOrigemAlteracao;
        }
        public string getMetodoInsercao()
        {
            return AP_MetodoInsercao;
        }
        public string getParticipouEdicao()
        {
            return AP_ParticipouEdicao;
        }
        public string getCategoriaRegistro()
        {
            return AP_CategoriaRegistro;
        }
        public string getOrigemConvite()
        {
            return AP_OrigemConvite;
        }
        public string getEmailLogin()
        {
            return AP_EmailLogin;
        }
        public string getCPF()
        {
            return AP_CPF;
        }
        public string getSenha()
        {
            return AP_Senha;
        }
        public string getTratamento()
        {
            return AP_Tratamento;
        }
        public string getNome()
        {
            return AP_Nome;
        }
        public string getSobrenome()
        {
            return AP_Sobrenome;
        }
        public string getNomeCompleto()
        {
            return AP_NomeCompleto;
        }
        public string getNomeCracha()
        {
            return AP_NomeCracha;
        }
        public string getProfissao()
        {
            return AP_Profissao;
        }
        public string getCargo()
        {
            return AP_Cargo;
        }
        public string getSexo()
        {
            return AP_Sexo;
        }
        public string getDiaNascimento()
        {
            return AP_DiaNascimento;
        }
        public string getMesNascimento()
        {
            return AP_MesNascimento;
        }
        public string getAnoNascimento()
        {
            return AP_AnoNascimento;
        }
        public string getDDICelular()
        {
            return AP_DDICelular;
        }
        public string getDDDCelular()
        {
            return AP_DDDCelular;
        }
        public string getTelefoneCelular()
        {
            return AP_TelefoneCelular;
        }
        public string getSmsCelularPessoal()
        {
            return AP_SmsCelularPessoal;
        }
        public string getMmsCelularPessoal()
        {
            return AP_MmsCelularPessoal;
        }
        public string getEmailPessoal()
        {
            return AP_EmailPessoal;
        }
        public string getAutorizaEnvioEmailPessoal()
        {
            return AP_AutorizaEnvioEmailPessoal;
        }
        public string getNomeEnderecoPessoal()
        {
            return AP_NomeEnderecoPessoal;
        }
        public string getNumeroEnderecoPessoal()
        {
            return AP_NumeroEnderecoPessoal;
        }
        public string getComplementoEnderecoPessoal()
        {
            return AP_ComplementoEnderecoPessoal;
        }
        public string getCepEnderecoPessoal()
        {
            return AP_CepEnderecoPessoal;
        }
        public string getBairroEnderecoPessoal()
        {
            return AP_BairroEnderecoPessoal;
        }
        public string getCidadeEnderecoPessoal()
        {
            return AP_CidadeEnderecoPessoal;
        }
        public string getEstadoEnderecoPessoal()
        {
            return AP_EstadoEnderecoPessoal;
        }
        public string getPaisEnderecoPessoal()
        {
            return AP_PaisEnderecoPessoal;
        }
        public string getDDITelefonePessoal()
        {
            return AP_DDITelefonePessoal;
        }
        public string getDDDTelefonePessoal()
        {
            return AP_DDDTelefonePessoal;
        }
        public string getNumeroTelefonePessoal()
        {
            return AP_NumeroTelefonePessoal;
        }
        public string getDDIFaxPessoal()
        {
            return AP_DDIFaxPessoal;
        }
        public string getDDDFaxPessoal()
        {
            return AP_DDDFaxPessoal;
        }
        public string getNumeroFaxPessoal()
        {
            return AP_NumeroFaxPessoal;
        }
        public string getRazaoSocial()
        {
            return AP_RazaoSocial;
        }
        public string getNomeFantasia()
        {
            return AP_NomeFantasia;
        }
        public string getEmpresaCracha()
        {
            return AP_EmpresaCracha;
        }
        public string getCnpj()
        {
            return AP_Cnpj;
        }
        public string getNomeEnderecoComercial()
        {
            return AP_NomeEnderecoComercial;
        }
        public string getNumeroEnderecoComercial()
        {
            return AP_NumeroEnderecoComercial;
        }
        public string getComplementoEnderecoComercial()
        {
            return AP_ComplementoEnderecoComercial;
        }
        public string getCepEnderecoComercial()
        {
            return AP_CepEnderecoComercial;
        }
        public string getBairroEnderecoComercial()
        {
            return AP_BairroEnderecoComercial;
        }
        public string getCidadeEnderecoComercial()
        {
            return AP_CidadeEnderecoComercial;
        }
        public string getEstadoEnderecoComercial()
        {
            return AP_EstadoEnderecoComercial;
        }
        public string getPaisEnderecoComercial()
        {
            return AP_PaisEnderecoComercial;
        }
        public string getDDITelefoneComercial()
        {
            return AP_DDITelefoneComercial;
        }
        public string getDDDTelefoneComercial()
        {
            return AP_DDDTelefoneComercial;
        }
        public string getNumeroTelefoneComercial()
        {
            return AP_NumeroTelefoneComercial;
        }
        public string getDDIFaxComercial()
        {
            return AP_DDIFaxComercial;
        }
        public string getDDDFaxComercial()
        {
            return AP_DDDFaxComercial;
        }
        public string getNumeroFaxComercial()
        {
            return AP_NumeroFaxComercial;
        }
        public string getPaginaWebComercial()
        {
            return AP_PaginaWebComercial;
        }
        public string getEmailComercial()
        {
            return AP_EmailComercial;
        }
        public string getAutorizaEnvioEmailComercial()
        {
            return AP_AutorizaEnvioEmailComercial;
        }
        public string getRes1()
        {
            return AP_Res1;
        }
        public string getRes2()
        {
            return AP_Res2;
        }
        public string getRes3()
        {
            return AP_Res3;
        }
        public string getRes4()
        {
            return AP_Res4;
        }
        public string getRes5()
        {
            return AP_Res5;
        }
        public int getObjRef()
        {
            return AP_ObjRef;
        }
        public string getDepartamento()
        {
            return AP_Departamento;
        }
        public string getTipoEnderecoPessoal()
        {
            return AP_TipoEnderecoPessoal;
        }
        public string getInscricaoEstadual()
        {
            return AP_InscricaoEstadual;
        }
        public string getTipoEnderecoComercial()
        {
            return AP_TipoEnderecoComercial;
        }
        public string getPergunta1()
        {
            return AP_Pergunta1;
        }
        public string getPergunta1Outro()
        {
            return AP_Pergunta1Outro;
        }
        public string getPergunta2()
        {
            return AP_Pergunta2;
        }
        public string getPergunta2Outro()
        {
            return AP_Pergunta2Outro;
        }
        public string getPergunta3()
        {
            return AP_Pergunta3;
        }
        public string getPergunta3Outro()
        {
            return AP_Pergunta3Outro;
        }
        public string getPergunta4()
        {
            return AP_Pergunta4;
        }
        public string getPergunta4Outro()
        {
            return AP_Pergunta4Outro;
        }
        public string getPergunta5()
        {
            return AP_Pergunta5;
        }
        public string getPergunta5Outro()
        {
            return AP_Pergunta5Outro;
        }
        public string getPergunta6()
        {
            return AP_Pergunta6;
        }
        public string getPergunta6Outro()
        {
            return AP_Pergunta6Outro;
        }
        public string getPergunta7()
        {
            return AP_Pergunta7;
        }
        public string getPergunta7Outro()
        {
            return AP_Pergunta7Outro;
        }
        public string getPergunta8()
        {
            return AP_Pergunta8;
        }
        public string getPergunta8Outro()
        {
            return AP_Pergunta8Outro;
        }
        public string getPergunta9()
        {
            return AP_Pergunta9;
        }
        public string getPergunta9Outro()
        {
            return AP_Pergunta9Outro;
        }
        public string getPergunta10()
        {
            return AP_Pergunta10;
        }
        public string getPergunta10Outro()
        {
            return AP_Pergunta10Outro;
        }
        public string getPergunta11()
        {
            return AP_Pergunta11;
        }
        public string getPergunta11Outro()
        {
            return AP_Pergunta11Outro;
        }
        public string getPergunta12()
        {
            return AP_Pergunta12;
        }
        public string getPergunta12Outro()
        {
            return AP_Pergunta12Outro;
        }
        public string getPergunta13()
        {
            return AP_Pergunta13;
        }
        public string getPergunta13Outro()
        {
            return AP_Pergunta13Outro;
        }
        public string getPergunta14()
        {
            return AP_Pergunta14;
        }
        public string getPergunta14Outro()
        {
            return AP_Pergunta14Outro;
        }
        public string getPergunta15()
        {
            return AP_Pergunta15;
        }
        public string getPergunta15Outro()
        {
            return AP_Pergunta15Outro;
        }
        public string getPergunta16()
        {
            return AP_Pergunta16;
        }
        public string getPergunta16Outro()
        {
            return AP_Pergunta16Outro;
        }
        public string getPergunta17()
        {
            return AP_Pergunta17;
        }
        public string getPergunta17Outro()
        {
            return AP_Pergunta17Outro;
        }
        public string getPergunta18()
        {
            return AP_Pergunta18;
        }
        public string getPergunta18Outro()
        {
            return AP_Pergunta18Outro;
        }
        public string getPergunta19()
        {
            return AP_Pergunta19;
        }
        public string getPergunta19Outro()
        {
            return AP_Pergunta19Outro;
        }
        public string getPergunta20()
        {
            return AP_Pergunta20;
        }
        public string getPergunta20Outro()
        {
            return AP_Pergunta20Outro;
        }
        public string getStatus()
        {
            return AP_Status;
        }
        public string getLocalizese()
        {
            return AP_Localizese;
        }
        public string getEvento()
        {
            return AP_Evento;
        }
        public string getEventoPrincipal()
        {
            return AP_EventoPrincipal;
        }
        public Int32 getNumeroImportacao()
        {
            return AP_NumeroImportacao;
        }
        //  Declaracao dos Metodos set
        public void setNumeroTerminal( string prNumeroTerminal)
        {
            AP_NumeroTerminal = prNumeroTerminal;
        }
        public void setChave( int prChave)
        {
            AP_Chave = prChave;
        }
        public void setCodigoBarras( string prCodigoBarras)
        {
            AP_CodigoBarras = prCodigoBarras;
        }
        public void setEmissaoCracha( string prEmissaoCracha)
        {
            AP_EmissaoCracha = prEmissaoCracha;
        }
        public void setPresenteDia1( string prPresenteDia1)
        {
            AP_PresenteDia1 = prPresenteDia1;
        }
        public void setPresenteDia2( string prPresenteDia2)
        {
            AP_PresenteDia2 = prPresenteDia2;
        }
        public void setPresenteDia3( string prPresenteDia3)
        {
            AP_PresenteDia3 = prPresenteDia3;
        }
        public void setPresenteDia4( string prPresenteDia4)
        {
            AP_PresenteDia4 = prPresenteDia4;
        }
        public void setPresenteDia5( string prPresenteDia5)
        {
            AP_PresenteDia5 = prPresenteDia5;
        }
        public void setOrigemInsercao( string prOrigemInsercao)
        {
            AP_OrigemInsercao = prOrigemInsercao;
        }
        public void setDataOrigemInsercao( DateTime prDataOrigemInsercao)
        {
            AP_DataOrigemInsercao = prDataOrigemInsercao;
        }
        public void setHoraOrigemInsercao( string prHoraOrigemInsercao)
        {
            AP_HoraOrigemInsercao = prHoraOrigemInsercao;
        }
        public void setUsuarioOrigemInsercao( string prUsuarioOrigemInsercao)
        {
            AP_UsuarioOrigemInsercao = prUsuarioOrigemInsercao;
        }
        public void setOrigemAlteracao( string prOrigemAlteracao)
        {
            AP_OrigemAlteracao = prOrigemAlteracao;
        }
        public void setDataOrigemAlteracao( DateTime prDataOrigemAlteracao)
        {
            AP_DataOrigemAlteracao = prDataOrigemAlteracao;
        }
        public void setHoraOrigemAlteracao( string prHoraOrigemAlteracao)
        {
            AP_HoraOrigemAlteracao = prHoraOrigemAlteracao;
        }
        public void setUsuarioOrigemAlteracao( string prUsuarioOrigemAlteracao)
        {
            AP_UsuarioOrigemAlteracao = prUsuarioOrigemAlteracao;
        }
        public void setMetodoInsercao( string prMetodoInsercao)
        {
            AP_MetodoInsercao = prMetodoInsercao;
        }
        public void setParticipouEdicao( string prParticipouEdicao)
        {
            AP_ParticipouEdicao = prParticipouEdicao;
        }
        public void setCategoriaRegistro( string prCategoriaRegistro)
        {
            AP_CategoriaRegistro = prCategoriaRegistro;
        }
        public void setOrigemConvite( string prOrigemConvite)
        {
            AP_OrigemConvite = prOrigemConvite;
        }
        public void setEmailLogin( string prEmailLogin)
        {
            AP_EmailLogin = prEmailLogin;
        }
        public void setCPF( string prCPF)
        {
            AP_CPF = prCPF;
        }
        public void setSenha( string prSenha)
        {
            AP_Senha = prSenha;
        }
        public void setTratamento( string prTratamento)
        {
            AP_Tratamento = prTratamento;
        }
        public void setNome( string prNome)
        {
            AP_Nome = prNome;
        }
        public void setSobrenome( string prSobrenome)
        {
            AP_Sobrenome = prSobrenome;
        }
        public void setNomeCompleto( string prNomeCompleto)
        {
            AP_NomeCompleto = prNomeCompleto;
        }
        public void setNomeCracha( string prNomeCracha)
        {
            AP_NomeCracha = prNomeCracha;
        }
        public void setProfissao( string prProfissao)
        {
            AP_Profissao = prProfissao;
        }
        public void setCargo( string prCargo)
        {
            AP_Cargo = prCargo;
        }
        public void setSexo( string prSexo)
        {
            AP_Sexo = prSexo;
        }
        public void setDiaNascimento( string prDiaNascimento)
        {
            AP_DiaNascimento = prDiaNascimento;
        }
        public void setMesNascimento( string prMesNascimento)
        {
            AP_MesNascimento = prMesNascimento;
        }
        public void setAnoNascimento( string prAnoNascimento)
        {
            AP_AnoNascimento = prAnoNascimento;
        }
        public void setDDICelular( string prDDICelular)
        {
            AP_DDICelular = prDDICelular;
        }
        public void setDDDCelular( string prDDDCelular)
        {
            AP_DDDCelular = prDDDCelular;
        }
        public void setTelefoneCelular( string prTelefoneCelular)
        {
            AP_TelefoneCelular = prTelefoneCelular;
        }
        public void setSmsCelularPessoal( string prSmsCelularPessoal)
        {
            AP_SmsCelularPessoal = prSmsCelularPessoal;
        }
        public void setMmsCelularPessoal( string prMmsCelularPessoal)
        {
            AP_MmsCelularPessoal = prMmsCelularPessoal;
        }
        public void setEmailPessoal( string prEmailPessoal)
        {
            AP_EmailPessoal = prEmailPessoal;
        }
        public void setAutorizaEnvioEmailPessoal( string prAutorizaEnvioEmailPessoal)
        {
            AP_AutorizaEnvioEmailPessoal = prAutorizaEnvioEmailPessoal;
        }
        public void setNomeEnderecoPessoal( string prNomeEnderecoPessoal)
        {
            AP_NomeEnderecoPessoal = prNomeEnderecoPessoal;
        }
        public void setNumeroEnderecoPessoal( string prNumeroEnderecoPessoal)
        {
            AP_NumeroEnderecoPessoal = prNumeroEnderecoPessoal;
        }
        public void setComplementoEnderecoPessoal( string prComplementoEnderecoPessoal)
        {
            AP_ComplementoEnderecoPessoal = prComplementoEnderecoPessoal;
        }
        public void setCepEnderecoPessoal( string prCepEnderecoPessoal)
        {
            AP_CepEnderecoPessoal = prCepEnderecoPessoal;
        }
        public void setBairroEnderecoPessoal( string prBairroEnderecoPessoal)
        {
            AP_BairroEnderecoPessoal = prBairroEnderecoPessoal;
        }
        public void setCidadeEnderecoPessoal( string prCidadeEnderecoPessoal)
        {
            AP_CidadeEnderecoPessoal = prCidadeEnderecoPessoal;
        }
        public void setEstadoEnderecoPessoal( string prEstadoEnderecoPessoal)
        {
            AP_EstadoEnderecoPessoal = prEstadoEnderecoPessoal;
        }
        public void setPaisEnderecoPessoal( string prPaisEnderecoPessoal)
        {
            AP_PaisEnderecoPessoal = prPaisEnderecoPessoal;
        }
        public void setDDITelefonePessoal( string prDDITelefonePessoal)
        {
            AP_DDITelefonePessoal = prDDITelefonePessoal;
        }
        public void setDDDTelefonePessoal( string prDDDTelefonePessoal)
        {
            AP_DDDTelefonePessoal = prDDDTelefonePessoal;
        }
        public void setNumeroTelefonePessoal( string prNumeroTelefonePessoal)
        {
            AP_NumeroTelefonePessoal = prNumeroTelefonePessoal;
        }
        public void setDDIFaxPessoal( string prDDIFaxPessoal)
        {
            AP_DDIFaxPessoal = prDDIFaxPessoal;
        }
        public void setDDDFaxPessoal( string prDDDFaxPessoal)
        {
            AP_DDDFaxPessoal = prDDDFaxPessoal;
        }
        public void setNumeroFaxPessoal( string prNumeroFaxPessoal)
        {
            AP_NumeroFaxPessoal = prNumeroFaxPessoal;
        }
        public void setRazaoSocial( string prRazaoSocial)
        {
            AP_RazaoSocial = prRazaoSocial;
        }
        public void setNomeFantasia( string prNomeFantasia)
        {
            AP_NomeFantasia = prNomeFantasia;
        }
        public void setEmpresaCracha( string prEmpresaCracha)
        {
            AP_EmpresaCracha = prEmpresaCracha;
        }
        public void setCnpj( string prCnpj)
        {
            AP_Cnpj = prCnpj;
        }
        public void setNomeEnderecoComercial( string prNomeEnderecoComercial)
        {
            AP_NomeEnderecoComercial = prNomeEnderecoComercial;
        }
        public void setNumeroEnderecoComercial( string prNumeroEnderecoComercial)
        {
            AP_NumeroEnderecoComercial = prNumeroEnderecoComercial;
        }
        public void setComplementoEnderecoComercial( string prComplementoEnderecoComercial)
        {
            AP_ComplementoEnderecoComercial = prComplementoEnderecoComercial;
        }
        public void setCepEnderecoComercial( string prCepEnderecoComercial)
        {
            AP_CepEnderecoComercial = prCepEnderecoComercial;
        }
        public void setBairroEnderecoComercial( string prBairroEnderecoComercial)
        {
            AP_BairroEnderecoComercial = prBairroEnderecoComercial;
        }
        public void setCidadeEnderecoComercial( string prCidadeEnderecoComercial)
        {
            AP_CidadeEnderecoComercial = prCidadeEnderecoComercial;
        }
        public void setEstadoEnderecoComercial( string prEstadoEnderecoComercial)
        {
            AP_EstadoEnderecoComercial = prEstadoEnderecoComercial;
        }
        public void setPaisEnderecoComercial( string prPaisEnderecoComercial)
        {
            AP_PaisEnderecoComercial = prPaisEnderecoComercial;
        }
        public void setDDITelefoneComercial( string prDDITelefoneComercial)
        {
            AP_DDITelefoneComercial = prDDITelefoneComercial;
        }
        public void setDDDTelefoneComercial( string prDDDTelefoneComercial)
        {
            AP_DDDTelefoneComercial = prDDDTelefoneComercial;
        }
        public void setNumeroTelefoneComercial( string prNumeroTelefoneComercial)
        {
            AP_NumeroTelefoneComercial = prNumeroTelefoneComercial;
        }
        public void setDDIFaxComercial( string prDDIFaxComercial)
        {
            AP_DDIFaxComercial = prDDIFaxComercial;
        }
        public void setDDDFaxComercial( string prDDDFaxComercial)
        {
            AP_DDDFaxComercial = prDDDFaxComercial;
        }
        public void setNumeroFaxComercial( string prNumeroFaxComercial)
        {
            AP_NumeroFaxComercial = prNumeroFaxComercial;
        }
        public void setPaginaWebComercial( string prPaginaWebComercial)
        {
            AP_PaginaWebComercial = prPaginaWebComercial;
        }
        public void setEmailComercial( string prEmailComercial)
        {
            AP_EmailComercial = prEmailComercial;
        }
        public void setAutorizaEnvioEmailComercial( string prAutorizaEnvioEmailComercial)
        {
            AP_AutorizaEnvioEmailComercial = prAutorizaEnvioEmailComercial;
        }
        public void setRes1( string prRes1)
        {
            AP_Res1 = prRes1;
        }
        public void setRes2( string prRes2)
        {
            AP_Res2 = prRes2;
        }
        public void setRes3( string prRes3)
        {
            AP_Res3 = prRes3;
        }
        public void setRes4( string prRes4)
        {
            AP_Res4 = prRes4;
        }
        public void setRes5( string prRes5)
        {
            AP_Res5 = prRes5;
        }
        public void setObjRef( int prObjRef)
        {
            AP_ObjRef = prObjRef;
        }
        public void setDepartamento( string prDepartamento)
        {
            AP_Departamento = prDepartamento;
        }
        public void setTipoEnderecoPessoal( string prTipoEnderecoPessoal)
        {
            AP_TipoEnderecoPessoal = prTipoEnderecoPessoal;
        }
        public void setInscricaoEstadual( string prInscricaoEstadual)
        {
            AP_InscricaoEstadual = prInscricaoEstadual;
        }
        public void setTipoEnderecoComercial( string prTipoEnderecoComercial)
        {
            AP_TipoEnderecoComercial = prTipoEnderecoComercial;
        }
        public void setPergunta1( string prPergunta1)
        {
            AP_Pergunta1 = prPergunta1;
        }
        public void setPergunta1Outro( string prPergunta1Outro)
        {
            AP_Pergunta1Outro = prPergunta1Outro;
        }
        public void setPergunta2( string prPergunta2)
        {
            AP_Pergunta2 = prPergunta2;
        }
        public void setPergunta2Outro( string prPergunta2Outro)
        {
            AP_Pergunta2Outro = prPergunta2Outro;
        }
        public void setPergunta3( string prPergunta3)
        {
            AP_Pergunta3 = prPergunta3;
        }
        public void setPergunta3Outro( string prPergunta3Outro)
        {
            AP_Pergunta3Outro = prPergunta3Outro;
        }
        public void setPergunta4( string prPergunta4)
        {
            AP_Pergunta4 = prPergunta4;
        }
        public void setPergunta4Outro( string prPergunta4Outro)
        {
            AP_Pergunta4Outro = prPergunta4Outro;
        }
        public void setPergunta5( string prPergunta5)
        {
            AP_Pergunta5 = prPergunta5;
        }
        public void setPergunta5Outro( string prPergunta5Outro)
        {
            AP_Pergunta5Outro = prPergunta5Outro;
        }
        public void setPergunta6( string prPergunta6)
        {
            AP_Pergunta6 = prPergunta6;
        }
        public void setPergunta6Outro( string prPergunta6Outro)
        {
            AP_Pergunta6Outro = prPergunta6Outro;
        }
        public void setPergunta7( string prPergunta7)
        {
            AP_Pergunta7 = prPergunta7;
        }
        public void setPergunta7Outro( string prPergunta7Outro)
        {
            AP_Pergunta7Outro = prPergunta7Outro;
        }
        public void setPergunta8( string prPergunta8)
        {
            AP_Pergunta8 = prPergunta8;
        }
        public void setPergunta8Outro( string prPergunta8Outro)
        {
            AP_Pergunta8Outro = prPergunta8Outro;
        }
        public void setPergunta9( string prPergunta9)
        {
            AP_Pergunta9 = prPergunta9;
        }
        public void setPergunta9Outro( string prPergunta9Outro)
        {
            AP_Pergunta9Outro = prPergunta9Outro;
        }
        public void setPergunta10( string prPergunta10)
        {
            AP_Pergunta10 = prPergunta10;
        }
        public void setPergunta10Outro( string prPergunta10Outro)
        {
            AP_Pergunta10Outro = prPergunta10Outro;
        }
        public void setPergunta11( string prPergunta11)
        {
            AP_Pergunta11 = prPergunta11;
        }
        public void setPergunta11Outro( string prPergunta11Outro)
        {
            AP_Pergunta11Outro = prPergunta11Outro;
        }
        public void setPergunta12( string prPergunta12)
        {
            AP_Pergunta12 = prPergunta12;
        }
        public void setPergunta12Outro( string prPergunta12Outro)
        {
            AP_Pergunta12Outro = prPergunta12Outro;
        }
        public void setPergunta13( string prPergunta13)
        {
            AP_Pergunta13 = prPergunta13;
        }
        public void setPergunta13Outro( string prPergunta13Outro)
        {
            AP_Pergunta13Outro = prPergunta13Outro;
        }
        public void setPergunta14( string prPergunta14)
        {
            AP_Pergunta14 = prPergunta14;
        }
        public void setPergunta14Outro( string prPergunta14Outro)
        {
            AP_Pergunta14Outro = prPergunta14Outro;
        }
        public void setPergunta15( string prPergunta15)
        {
            AP_Pergunta15 = prPergunta15;
        }
        public void setPergunta15Outro( string prPergunta15Outro)
        {
            AP_Pergunta15Outro = prPergunta15Outro;
        }
        public void setPergunta16( string prPergunta16)
        {
            AP_Pergunta16 = prPergunta16;
        }
        public void setPergunta16Outro( string prPergunta16Outro)
        {
            AP_Pergunta16Outro = prPergunta16Outro;
        }
        public void setPergunta17( string prPergunta17)
        {
            AP_Pergunta17 = prPergunta17;
        }
        public void setPergunta17Outro( string prPergunta17Outro)
        {
            AP_Pergunta17Outro = prPergunta17Outro;
        }
        public void setPergunta18( string prPergunta18)
        {
            AP_Pergunta18 = prPergunta18;
        }
        public void setPergunta18Outro( string prPergunta18Outro)
        {
            AP_Pergunta18Outro = prPergunta18Outro;
        }
        public void setPergunta19( string prPergunta19)
        {
            AP_Pergunta19 = prPergunta19;
        }
        public void setPergunta19Outro( string prPergunta19Outro)
        {
            AP_Pergunta19Outro = prPergunta19Outro;
        }
        public void setPergunta20( string prPergunta20)
        {
            AP_Pergunta20 = prPergunta20;
        }
        public void setPergunta20Outro( string prPergunta20Outro)
        {
            AP_Pergunta20Outro = prPergunta20Outro;
        }

        public void setStatus(string prStatus)
        {
            AP_Status = prStatus;
        }
        public void setLocalizese(string prLocalizese)
        {
            AP_Localizese = prLocalizese;
        }
        public void  setEvento(string prEvento)
        {
             AP_Evento = prEvento;
        }
        public void setEventoPrincipal(string prEventoPrincipal)
        {
            AP_EventoPrincipal = prEventoPrincipal;
        }
        public void setNumeroImportacao(Int32 prNumeroImportacao)
        {
            AP_NumeroImportacao = prNumeroImportacao;
        }
    }
}

