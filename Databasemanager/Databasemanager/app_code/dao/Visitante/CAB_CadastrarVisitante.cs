using System;
using System.Data;
using SPI;

public abstract class CAB_CadastrarVisitante : ICadastrarVisitante
{
     public abstract void MS_Incluir(CTO_Visitante ITO_Visitante);
     public abstract void MS_Alterar(CTO_Visitante ITO_Visitante);
     public abstract void MS_Excluir(CTO_Visitante ITO_Visitante);
     public abstract CTO_Visitante MS_Obter(int pritObjRef);
     public abstract CTO_Visitante MS_ObterPorCPF(string prstCPF);
     public abstract DataTable MS_ObterPorCPFTabela(string prstCPF);
     public abstract CTO_Visitante MS_ObterPorNome(string prstNome);
     public abstract DataTable MS_ObterPorNomeTabela(string prstNome);
     public abstract CTO_Visitante MS_ObterPorCodigo(string prstCodigo);
     public abstract CTO_Visitante MS_ObterPorEmail(string prstEmail);
     public abstract DataTable MS_LocalizarRegistroPorFiltro(string prstCriterio, string prstAtributo);
     public abstract string MS_GerarCodigoBarras();
     public abstract DataTable MS_ObterRegistro(string prstPalavra, string prContendo, string stTipoBusca);
     public abstract void MS_ConfirmaImpressao(Int64 prstCodigo);
     public abstract DataTable MS_ObterCampos();
     public abstract CTO_Visitante MS_Obter1();
     public abstract DataTable MS_ObterRegistroGrid(string prstChave);
     public abstract CTO_Visitante MS_ObterPorLocalizese(string prstLocalizese);
}

