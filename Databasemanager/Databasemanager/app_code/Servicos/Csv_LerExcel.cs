﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Databasemanager
{
    public class Csv_LerExcel
    {
        public DataTable MS_ObterExcel(string prArquivo)
        {
            OleDbConnection objConn = null;
            System.Data.DataTable dt = null;

            String connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + prArquivo + ";Extended Properties='Excel 12.0 Xml;HDR=YES'";

            objConn = new OleDbConnection(connString);
            objConn.Open();

            string stObterNomeDaPlan = String.Empty;
            DataTable Planilhas = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            foreach (DataRow row in Planilhas.Rows)
            {
                if ((row["TABLE_NAME"].ToString() != "_xlnm#_FilterDatabase") && (row["TABLE_NAME"].ToString() != "hiddenSheet$") && (row["TABLE_NAME"].ToString() != "_xlnm#Print_Titles"))
                {
                    stObterNomeDaPlan = row["TABLE_NAME"].ToString();
                    break;
                }
            }

            DataTable table = new DataTable();
            OleDbDataAdapter da = new OleDbDataAdapter("Select * From ["+ stObterNomeDaPlan +"]", connString);
            da.Fill(table);

            objConn.Close();

            return table;
        }
    }
}
