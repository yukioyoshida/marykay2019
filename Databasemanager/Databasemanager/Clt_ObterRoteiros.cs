﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SPI;

namespace Databasemanager
{
    public partial class Clt_ObterRoteiros : Form
    {
        public DataTable dtExcel;
        public bool AP_Resultado = false;


        public Clt_ObterRoteiros()
        {
            InitializeComponent();
            //gdwRoteiros.CellClick += GdwRoteiros_CellClick;
        }

        //private void GdwRoteiros_CellClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    if (gdwRoteiros.CurrentCell.ColumnIndex.Equals(1) && e.RowIndex != -1)
        //    {
        //        if (gdwRoteiros.CurrentCell != null && gdwRoteiros.CurrentCell.Value != null)
        //            gdwRoteiros.CurrentCell.Value = 9;
        //    }
        //}

        private void Clt_ObterRoteiros_Load(object sender, EventArgs e)
        {
            MI_CarregarDistintos();
        }

        private void MI_CarregarDistintos()
        {
            gdwRoteiros.Rows.Clear();

            var x = (from r in dtExcel.AsEnumerable()
                     select r["Nivel"]).Distinct().ToList();

            try
            {
                foreach (string stReconhecimento in x)
                {
                    gdwRoteiros.Rows.Add(stReconhecimento);
                }
            }
            catch { }

            MI_InserirRoteiroNasCombos();

            foreach (DataGridViewRow Row in gdwRoteiros.Rows)
            { 
                Row.Height = 28;
            }

            int totalrows = gdwRoteiros.Rows.Count;
            for (int index = 0; index < totalrows; index++)
            {
                if (index > 0)
                {
                    gdwRoteiros.Rows[index].Cells[1].Value = gdwRoteiros.Rows[index].Cells[0].Value;
                }
            }
        }

        private void MI_InserirRoteiroNasCombos()
        {
            DataTable dtRoteiro = new DAO_Login().MS_ObterRoteiro("I");
 
            Roteiro.ValueMember = "dsReconhecimento";
            Roteiro.DisplayMember = "dsReconhecimento";

            Roteiro.DataSource = dtRoteiro;
        }

        private bool MI_Consistencias()
        {
            bool boResult = true;

            foreach(DataGridViewRow Row in gdwRoteiros.Rows)
            {
                if (Row.Cells[1].Value==null)
                {
                    MessageBox.Show("Selecione o roteiro para o nível (" + Row.Cells[0].Value.ToString() + ")", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return boResult = false;
                }
            }

            return boResult;
        }

        private void btoConfirmar_Click(object sender, EventArgs e)
        {
            if (MI_Consistencias())
            {
                AP_Resultado = true;
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
