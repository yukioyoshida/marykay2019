﻿namespace Databasemanager
{
    partial class Clt_DetalhesConsultora
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpImportacao = new System.Windows.Forms.GroupBox();
            this.edtNomedaNSD = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.edtDatadoPedido = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.edtValordoPedido = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.edtStatus = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.edtTipodeInscricao = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.edtEstado = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.edtCidade = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.edtTelefone = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.edtEmaildeContato = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.edtUnidade = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.edtNiveldeCarreira = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.edtNomedaConsultora = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.edtConsultora = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.edtIDdoPedido = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.edtPedido = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gdwTable = new System.Windows.Forms.DataGridView();
            this.Reconhecimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataImportacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btoDel = new System.Windows.Forms.PictureBox();
            this.btoEdit = new System.Windows.Forms.PictureBox();
            this.btoAdd = new System.Windows.Forms.PictureBox();
            this.grpImportacao.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdwTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoAdd)).BeginInit();
            this.SuspendLayout();
            // 
            // grpImportacao
            // 
            this.grpImportacao.Controls.Add(this.edtNomedaNSD);
            this.grpImportacao.Controls.Add(this.label15);
            this.grpImportacao.Controls.Add(this.edtDatadoPedido);
            this.grpImportacao.Controls.Add(this.label14);
            this.grpImportacao.Controls.Add(this.edtValordoPedido);
            this.grpImportacao.Controls.Add(this.label13);
            this.grpImportacao.Controls.Add(this.edtStatus);
            this.grpImportacao.Controls.Add(this.label12);
            this.grpImportacao.Controls.Add(this.edtTipodeInscricao);
            this.grpImportacao.Controls.Add(this.label11);
            this.grpImportacao.Controls.Add(this.edtEstado);
            this.grpImportacao.Controls.Add(this.label10);
            this.grpImportacao.Controls.Add(this.edtCidade);
            this.grpImportacao.Controls.Add(this.label9);
            this.grpImportacao.Controls.Add(this.edtTelefone);
            this.grpImportacao.Controls.Add(this.label8);
            this.grpImportacao.Controls.Add(this.edtEmaildeContato);
            this.grpImportacao.Controls.Add(this.label7);
            this.grpImportacao.Controls.Add(this.edtUnidade);
            this.grpImportacao.Controls.Add(this.label6);
            this.grpImportacao.Controls.Add(this.edtNiveldeCarreira);
            this.grpImportacao.Controls.Add(this.label5);
            this.grpImportacao.Controls.Add(this.edtNomedaConsultora);
            this.grpImportacao.Controls.Add(this.label4);
            this.grpImportacao.Controls.Add(this.edtConsultora);
            this.grpImportacao.Controls.Add(this.label3);
            this.grpImportacao.Controls.Add(this.edtIDdoPedido);
            this.grpImportacao.Controls.Add(this.label2);
            this.grpImportacao.Controls.Add(this.edtPedido);
            this.grpImportacao.Controls.Add(this.label1);
            this.grpImportacao.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpImportacao.Location = new System.Drawing.Point(16, 16);
            this.grpImportacao.Name = "grpImportacao";
            this.grpImportacao.Size = new System.Drawing.Size(964, 403);
            this.grpImportacao.TabIndex = 4;
            this.grpImportacao.TabStop = false;
            this.grpImportacao.Text = "Dados cadastrais";
            // 
            // edtNomedaNSD
            // 
            this.edtNomedaNSD.Location = new System.Drawing.Point(34, 357);
            this.edtNomedaNSD.Name = "edtNomedaNSD";
            this.edtNomedaNSD.ReadOnly = true;
            this.edtNomedaNSD.Size = new System.Drawing.Size(489, 26);
            this.edtNomedaNSD.TabIndex = 29;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(31, 336);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 18);
            this.label15.TabIndex = 28;
            this.label15.Text = "Nome da NSD";
            // 
            // edtDatadoPedido
            // 
            this.edtDatadoPedido.Location = new System.Drawing.Point(779, 357);
            this.edtDatadoPedido.Name = "edtDatadoPedido";
            this.edtDatadoPedido.ReadOnly = true;
            this.edtDatadoPedido.Size = new System.Drawing.Size(159, 26);
            this.edtDatadoPedido.TabIndex = 27;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(776, 336);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(111, 18);
            this.label14.TabIndex = 26;
            this.label14.Text = "DatadoPedido";
            // 
            // edtValordoPedido
            // 
            this.edtValordoPedido.Location = new System.Drawing.Point(663, 357);
            this.edtValordoPedido.Name = "edtValordoPedido";
            this.edtValordoPedido.ReadOnly = true;
            this.edtValordoPedido.Size = new System.Drawing.Size(110, 26);
            this.edtValordoPedido.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(660, 336);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 18);
            this.label13.TabIndex = 24;
            this.label13.Text = "Valor";
            // 
            // edtStatus
            // 
            this.edtStatus.Location = new System.Drawing.Point(529, 357);
            this.edtStatus.Name = "edtStatus";
            this.edtStatus.ReadOnly = true;
            this.edtStatus.Size = new System.Drawing.Size(127, 26);
            this.edtStatus.TabIndex = 23;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(526, 336);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 18);
            this.label12.TabIndex = 22;
            this.label12.Text = "Status";
            // 
            // edtTipodeInscricao
            // 
            this.edtTipodeInscricao.Location = new System.Drawing.Point(34, 296);
            this.edtTipodeInscricao.Name = "edtTipodeInscricao";
            this.edtTipodeInscricao.ReadOnly = true;
            this.edtTipodeInscricao.Size = new System.Drawing.Size(904, 26);
            this.edtTipodeInscricao.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(31, 275);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 18);
            this.label11.TabIndex = 20;
            this.label11.Text = "Tipo de Inscrição";
            // 
            // edtEstado
            // 
            this.edtEstado.Location = new System.Drawing.Point(428, 231);
            this.edtEstado.Name = "edtEstado";
            this.edtEstado.ReadOnly = true;
            this.edtEstado.Size = new System.Drawing.Size(191, 26);
            this.edtEstado.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(425, 210);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 18);
            this.label10.TabIndex = 18;
            this.label10.Text = "Estado";
            // 
            // edtCidade
            // 
            this.edtCidade.Location = new System.Drawing.Point(231, 231);
            this.edtCidade.Name = "edtCidade";
            this.edtCidade.ReadOnly = true;
            this.edtCidade.Size = new System.Drawing.Size(191, 26);
            this.edtCidade.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(228, 210);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 18);
            this.label9.TabIndex = 16;
            this.label9.Text = "Cidade";
            // 
            // edtTelefone
            // 
            this.edtTelefone.Location = new System.Drawing.Point(34, 231);
            this.edtTelefone.Name = "edtTelefone";
            this.edtTelefone.ReadOnly = true;
            this.edtTelefone.Size = new System.Drawing.Size(191, 26);
            this.edtTelefone.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 210);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 18);
            this.label8.TabIndex = 14;
            this.label8.Text = "Telefone";
            // 
            // edtEmaildeContato
            // 
            this.edtEmaildeContato.Location = new System.Drawing.Point(231, 170);
            this.edtEmaildeContato.Name = "edtEmaildeContato";
            this.edtEmaildeContato.ReadOnly = true;
            this.edtEmaildeContato.Size = new System.Drawing.Size(707, 26);
            this.edtEmaildeContato.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(228, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 18);
            this.label7.TabIndex = 12;
            this.label7.Text = "Email de contato";
            // 
            // edtUnidade
            // 
            this.edtUnidade.Location = new System.Drawing.Point(34, 170);
            this.edtUnidade.Name = "edtUnidade";
            this.edtUnidade.ReadOnly = true;
            this.edtUnidade.Size = new System.Drawing.Size(191, 26);
            this.edtUnidade.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Unidade";
            // 
            // edtNiveldeCarreira
            // 
            this.edtNiveldeCarreira.Location = new System.Drawing.Point(529, 108);
            this.edtNiveldeCarreira.Name = "edtNiveldeCarreira";
            this.edtNiveldeCarreira.ReadOnly = true;
            this.edtNiveldeCarreira.Size = new System.Drawing.Size(409, 26);
            this.edtNiveldeCarreira.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(526, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Nivel de Carreira";
            // 
            // edtNomedaConsultora
            // 
            this.edtNomedaConsultora.Location = new System.Drawing.Point(34, 108);
            this.edtNomedaConsultora.Name = "edtNomedaConsultora";
            this.edtNomedaConsultora.ReadOnly = true;
            this.edtNomedaConsultora.Size = new System.Drawing.Size(489, 26);
            this.edtNomedaConsultora.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Nome Completo";
            // 
            // edtConsultora
            // 
            this.edtConsultora.Location = new System.Drawing.Point(675, 46);
            this.edtConsultora.Name = "edtConsultora";
            this.edtConsultora.ReadOnly = true;
            this.edtConsultora.Size = new System.Drawing.Size(263, 26);
            this.edtConsultora.TabIndex = 5;
            this.edtConsultora.TextChanged += new System.EventHandler(this.edtConsultora_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(672, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Consultora";
            // 
            // edtIDdoPedido
            // 
            this.edtIDdoPedido.Location = new System.Drawing.Point(231, 46);
            this.edtIDdoPedido.Name = "edtIDdoPedido";
            this.edtIDdoPedido.ReadOnly = true;
            this.edtIDdoPedido.Size = new System.Drawing.Size(438, 26);
            this.edtIDdoPedido.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(228, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "ID do Pedido";
            // 
            // edtPedido
            // 
            this.edtPedido.Location = new System.Drawing.Point(34, 46);
            this.edtPedido.Name = "edtPedido";
            this.edtPedido.ReadOnly = true;
            this.edtPedido.Size = new System.Drawing.Size(191, 26);
            this.edtPedido.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pedido:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btoDel);
            this.groupBox1.Controls.Add(this.btoEdit);
            this.groupBox1.Controls.Add(this.btoAdd);
            this.groupBox1.Controls.Add(this.gdwTable);
            this.groupBox1.Location = new System.Drawing.Point(16, 498);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(964, 321);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Roteito";
            // 
            // gdwTable
            // 
            this.gdwTable.AllowUserToAddRows = false;
            this.gdwTable.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.gdwTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gdwTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdwTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Reconhecimento,
            this.DataImportacao});
            this.gdwTable.Location = new System.Drawing.Point(34, 33);
            this.gdwTable.MultiSelect = false;
            this.gdwTable.Name = "gdwTable";
            this.gdwTable.RowHeadersVisible = false;
            this.gdwTable.RowTemplate.Height = 24;
            this.gdwTable.Size = new System.Drawing.Size(904, 233);
            this.gdwTable.TabIndex = 2;
            this.gdwTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdwTable_CellClick);
            // 
            // Reconhecimento
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Reconhecimento.DefaultCellStyle = dataGridViewCellStyle13;
            this.Reconhecimento.HeaderText = "cdRoteiro";
            this.Reconhecimento.Name = "Reconhecimento";
            this.Reconhecimento.ReadOnly = true;
            this.Reconhecimento.Visible = false;
            // 
            // DataImportacao
            // 
            this.DataImportacao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataImportacao.DefaultCellStyle = dataGridViewCellStyle14;
            this.DataImportacao.HeaderText = "Reconhecimento";
            this.DataImportacao.Name = "DataImportacao";
            this.DataImportacao.ReadOnly = true;
            // 
            // btoDel
            // 
            this.btoDel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoDel.Image = global::Databasemanager.Properties.Resources.del;
            this.btoDel.Location = new System.Drawing.Point(904, 272);
            this.btoDel.Name = "btoDel";
            this.btoDel.Size = new System.Drawing.Size(35, 35);
            this.btoDel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoDel.TabIndex = 7;
            this.btoDel.TabStop = false;
            this.btoDel.Click += new System.EventHandler(this.btoDel_Click);
            // 
            // btoEdit
            // 
            this.btoEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoEdit.Image = global::Databasemanager.Properties.Resources.editblack;
            this.btoEdit.Location = new System.Drawing.Point(863, 272);
            this.btoEdit.Name = "btoEdit";
            this.btoEdit.Size = new System.Drawing.Size(35, 35);
            this.btoEdit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoEdit.TabIndex = 6;
            this.btoEdit.TabStop = false;
            this.btoEdit.Click += new System.EventHandler(this.btoEdit_Click);
            // 
            // btoAdd
            // 
            this.btoAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoAdd.Image = global::Databasemanager.Properties.Resources.add;
            this.btoAdd.Location = new System.Drawing.Point(822, 272);
            this.btoAdd.Name = "btoAdd";
            this.btoAdd.Size = new System.Drawing.Size(35, 35);
            this.btoAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoAdd.TabIndex = 5;
            this.btoAdd.TabStop = false;
            this.btoAdd.Click += new System.EventHandler(this.btoAdd_Click);
            // 
            // Clt_DetalhesConsultora
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1006, 831);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpImportacao);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Clt_DetalhesConsultora";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Detalhes";
            this.Load += new System.EventHandler(this.Clt_DetalhesConsultora_Load);
            this.grpImportacao.ResumeLayout(false);
            this.grpImportacao.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gdwTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoAdd)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpImportacao;
        private System.Windows.Forms.TextBox edtNomedaNSD;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox edtDatadoPedido;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox edtValordoPedido;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox edtStatus;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox edtTipodeInscricao;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox edtEstado;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox edtCidade;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox edtTelefone;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox edtEmaildeContato;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox edtUnidade;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox edtNiveldeCarreira;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox edtNomedaConsultora;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox edtConsultora;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edtIDdoPedido;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edtPedido;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView gdwTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reconhecimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataImportacao;
        private System.Windows.Forms.PictureBox btoDel;
        private System.Windows.Forms.PictureBox btoEdit;
        private System.Windows.Forms.PictureBox btoAdd;
    }
}