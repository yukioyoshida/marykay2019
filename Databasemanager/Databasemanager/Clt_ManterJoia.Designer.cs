﻿namespace Databasemanager
{
    partial class Clt_ManterJoia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpImportacao = new System.Windows.Forms.GroupBox();
            this.btoVoltar = new System.Windows.Forms.PictureBox();
            this.btoSalvar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ddlRoteiro = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.edtNomeCompleto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.grpImportacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btoVoltar)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpImportacao
            // 
            this.grpImportacao.Controls.Add(this.btoVoltar);
            this.grpImportacao.Controls.Add(this.btoSalvar);
            this.grpImportacao.Controls.Add(this.groupBox2);
            this.grpImportacao.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpImportacao.Location = new System.Drawing.Point(12, 12);
            this.grpImportacao.Name = "grpImportacao";
            this.grpImportacao.Size = new System.Drawing.Size(569, 369);
            this.grpImportacao.TabIndex = 6;
            this.grpImportacao.TabStop = false;
            this.grpImportacao.Text = "Alteração de reconhecimento";
            // 
            // btoVoltar
            // 
            this.btoVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoVoltar.Image = global::Databasemanager.Properties.Resources.voltarblack;
            this.btoVoltar.Location = new System.Drawing.Point(486, 300);
            this.btoVoltar.Name = "btoVoltar";
            this.btoVoltar.Size = new System.Drawing.Size(38, 38);
            this.btoVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoVoltar.TabIndex = 10;
            this.btoVoltar.TabStop = false;
            // 
            // btoSalvar
            // 
            this.btoSalvar.Location = new System.Drawing.Point(31, 296);
            this.btoSalvar.Name = "btoSalvar";
            this.btoSalvar.Size = new System.Drawing.Size(132, 42);
            this.btoSalvar.TabIndex = 0;
            this.btoSalvar.Text = "Salvar";
            this.btoSalvar.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.ddlRoteiro);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.edtNomeCompleto);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(16, 18);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(532, 261);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // ddlRoteiro
            // 
            this.ddlRoteiro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlRoteiro.FormattingEnabled = true;
            this.ddlRoteiro.Items.AddRange(new object[] {
            "Credenciamento",
            "Palco",
            "Platéia",
            "Sala"});
            this.ddlRoteiro.Location = new System.Drawing.Point(15, 117);
            this.ddlRoteiro.Name = "ddlRoteiro";
            this.ddlRoteiro.Size = new System.Drawing.Size(493, 26);
            this.ddlRoteiro.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Roteiro:";
            // 
            // edtNomeCompleto
            // 
            this.edtNomeCompleto.Location = new System.Drawing.Point(15, 44);
            this.edtNomeCompleto.Name = "edtNomeCompleto";
            this.edtNomeCompleto.ReadOnly = true;
            this.edtNomeCompleto.Size = new System.Drawing.Size(493, 26);
            this.edtNomeCompleto.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome completo:";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Credenciamento",
            "Palco",
            "Platéia",
            "Sala"});
            this.comboBox1.Location = new System.Drawing.Point(15, 199);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(493, 26);
            this.comboBox1.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 18);
            this.label4.TabIndex = 7;
            this.label4.Text = "Nome do premio:";
            // 
            // Clt_ManterJoia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(819, 700);
            this.Controls.Add(this.grpImportacao);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Clt_ManterJoia";
            this.Text = "Reconhecimento";
            this.grpImportacao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btoVoltar)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpImportacao;
        private System.Windows.Forms.PictureBox btoVoltar;
        private System.Windows.Forms.Button btoSalvar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox ddlRoteiro;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edtNomeCompleto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}