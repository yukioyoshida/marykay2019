﻿namespace Databasemanager
{
    partial class ctl_importacaoPlanilha
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpImportacao = new System.Windows.Forms.GroupBox();
            this.ddlRoteiro = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ddlTipo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btoAcessar = new System.Windows.Forms.Button();
            this.btoDiretorio = new System.Windows.Forms.Button();
            this.edtArquivo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gropImportacao = new System.Windows.Forms.GroupBox();
            this.progress = new System.Windows.Forms.ProgressBar();
            this.lblLegenda = new System.Windows.Forms.Label();
            this.btoPausar = new System.Windows.Forms.Button();
            this.grpImportacao.SuspendLayout();
            this.gropImportacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpImportacao
            // 
            this.grpImportacao.Controls.Add(this.ddlRoteiro);
            this.grpImportacao.Controls.Add(this.label3);
            this.grpImportacao.Controls.Add(this.ddlTipo);
            this.grpImportacao.Controls.Add(this.label2);
            this.grpImportacao.Controls.Add(this.btoAcessar);
            this.grpImportacao.Controls.Add(this.btoDiretorio);
            this.grpImportacao.Controls.Add(this.edtArquivo);
            this.grpImportacao.Controls.Add(this.label1);
            this.grpImportacao.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpImportacao.Location = new System.Drawing.Point(12, 13);
            this.grpImportacao.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.grpImportacao.Name = "grpImportacao";
            this.grpImportacao.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.grpImportacao.Size = new System.Drawing.Size(467, 268);
            this.grpImportacao.TabIndex = 1;
            this.grpImportacao.TabStop = false;
            this.grpImportacao.Text = "Importação";
            // 
            // ddlRoteiro
            // 
            this.ddlRoteiro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlRoteiro.FormattingEnabled = true;
            this.ddlRoteiro.Location = new System.Drawing.Point(31, 154);
            this.ddlRoteiro.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ddlRoteiro.Name = "ddlRoteiro";
            this.ddlRoteiro.Size = new System.Drawing.Size(410, 22);
            this.ddlRoteiro.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 136);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 14);
            this.label3.TabIndex = 7;
            this.label3.Text = "ROTEIRO";
            // 
            // ddlTipo
            // 
            this.ddlTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlTipo.FormattingEnabled = true;
            this.ddlTipo.Items.AddRange(new object[] {
            "01 - PLANILHA GERAL DAS CONSULTORAS",
            "02 - ROTEIRO",
            "02.1 - ROTEIRO JOIAS",
            "03 - INSCRIÇÃO",
            "03.1 - INSCRIÇÃO ADICIONAL",
            "04 - JOIAS",
            "04.1 - JOIAS DIVIDIDO",
            "05 - CONTEST WINNER",
            "05.1 - CONTEST WINNER DIVIDIDO",
            "06 - NÍVEL DE CARREIRA - INDISPONIVEL",
            "07 - ATUALIZAÇÃO CONVIDADOS"});
            this.ddlTipo.Location = new System.Drawing.Point(31, 104);
            this.ddlTipo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ddlTipo.Name = "ddlTipo";
            this.ddlTipo.Size = new System.Drawing.Size(410, 22);
            this.ddlTipo.TabIndex = 6;
            this.ddlTipo.SelectedIndexChanged += new System.EventHandler(this.ddlTipo_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 87);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 14);
            this.label2.TabIndex = 5;
            this.label2.Text = "TIPO DE PLANILHA";
            // 
            // btoAcessar
            // 
            this.btoAcessar.Location = new System.Drawing.Point(30, 225);
            this.btoAcessar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btoAcessar.Name = "btoAcessar";
            this.btoAcessar.Size = new System.Drawing.Size(96, 31);
            this.btoAcessar.TabIndex = 3;
            this.btoAcessar.Text = "INICIAR";
            this.btoAcessar.UseVisualStyleBackColor = true;
            this.btoAcessar.Click += new System.EventHandler(this.btoAcessar_Click);
            // 
            // btoDiretorio
            // 
            this.btoDiretorio.Location = new System.Drawing.Point(415, 54);
            this.btoDiretorio.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btoDiretorio.Name = "btoDiretorio";
            this.btoDiretorio.Size = new System.Drawing.Size(25, 21);
            this.btoDiretorio.TabIndex = 2;
            this.btoDiretorio.Text = "...";
            this.btoDiretorio.UseVisualStyleBackColor = true;
            this.btoDiretorio.Click += new System.EventHandler(this.btoDiretorio_Click);
            // 
            // edtArquivo
            // 
            this.edtArquivo.Location = new System.Drawing.Point(31, 54);
            this.edtArquivo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.edtArquivo.Name = "edtArquivo";
            this.edtArquivo.ReadOnly = true;
            this.edtArquivo.Size = new System.Drawing.Size(380, 22);
            this.edtArquivo.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Arquivo";
            // 
            // gropImportacao
            // 
            this.gropImportacao.Controls.Add(this.progress);
            this.gropImportacao.Controls.Add(this.lblLegenda);
            this.gropImportacao.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gropImportacao.Location = new System.Drawing.Point(12, 286);
            this.gropImportacao.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gropImportacao.Name = "gropImportacao";
            this.gropImportacao.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gropImportacao.Size = new System.Drawing.Size(467, 90);
            this.gropImportacao.TabIndex = 2;
            this.gropImportacao.TabStop = false;
            this.gropImportacao.Text = "Importação";
            this.gropImportacao.Visible = false;
            // 
            // progress
            // 
            this.progress.Location = new System.Drawing.Point(31, 33);
            this.progress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(409, 19);
            this.progress.TabIndex = 1;
            // 
            // lblLegenda
            // 
            this.lblLegenda.Font = new System.Drawing.Font("Verdana", 7.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLegenda.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblLegenda.Location = new System.Drawing.Point(31, 54);
            this.lblLegenda.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLegenda.Name = "lblLegenda";
            this.lblLegenda.Size = new System.Drawing.Size(409, 15);
            this.lblLegenda.TabIndex = 0;
            this.lblLegenda.Text = "Importando, por favor aguarde...";
            this.lblLegenda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btoPausar
            // 
            this.btoPausar.Font = new System.Drawing.Font("Verdana", 9F);
            this.btoPausar.Location = new System.Drawing.Point(43, 381);
            this.btoPausar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btoPausar.Name = "btoPausar";
            this.btoPausar.Size = new System.Drawing.Size(96, 31);
            this.btoPausar.TabIndex = 3;
            this.btoPausar.Text = "Interromper";
            this.btoPausar.UseVisualStyleBackColor = true;
            this.btoPausar.Visible = false;
            this.btoPausar.Click += new System.EventHandler(this.btoPausar_Click);
            // 
            // ctl_importacaoPlanilha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.btoPausar);
            this.Controls.Add(this.gropImportacao);
            this.Controls.Add(this.grpImportacao);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "ctl_importacaoPlanilha";
            this.Size = new System.Drawing.Size(652, 519);
            this.grpImportacao.ResumeLayout(false);
            this.grpImportacao.PerformLayout();
            this.gropImportacao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpImportacao;
        private System.Windows.Forms.ComboBox ddlTipo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btoAcessar;
        private System.Windows.Forms.Button btoDiretorio;
        private System.Windows.Forms.TextBox edtArquivo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gropImportacao;
        private System.Windows.Forms.Label lblLegenda;
        private System.Windows.Forms.ProgressBar progress;
        private System.Windows.Forms.ComboBox ddlRoteiro;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btoPausar;
    }
}
