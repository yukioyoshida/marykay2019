﻿namespace Databasemanager
{
    partial class Clt_ObterRoteiros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gdwRoteiros = new System.Windows.Forms.DataGridView();
            this.Reconhecimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Roteiro = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.btoConfirmar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gdwRoteiros)).BeginInit();
            this.SuspendLayout();
            // 
            // gdwRoteiros
            // 
            this.gdwRoteiros.AllowUserToAddRows = false;
            this.gdwRoteiros.AllowUserToDeleteRows = false;
            this.gdwRoteiros.AllowUserToResizeColumns = false;
            this.gdwRoteiros.AllowUserToResizeRows = false;
            this.gdwRoteiros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdwRoteiros.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Reconhecimento,
            this.Roteiro});
            this.gdwRoteiros.Location = new System.Drawing.Point(0, 0);
            this.gdwRoteiros.Name = "gdwRoteiros";
            this.gdwRoteiros.RowTemplate.Height = 24;
            this.gdwRoteiros.Size = new System.Drawing.Size(1206, 402);
            this.gdwRoteiros.TabIndex = 0;
            // 
            // Reconhecimento
            // 
            this.Reconhecimento.HeaderText = "Nivel";
            this.Reconhecimento.Name = "Reconhecimento";
            this.Reconhecimento.ReadOnly = true;
            this.Reconhecimento.Width = 350;
            // 
            // Roteiro
            // 
            this.Roteiro.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Roteiro.DataPropertyName = "cidades";
            this.Roteiro.HeaderText = "Roteiro";
            this.Roteiro.Name = "Roteiro";
            // 
            // btoConfirmar
            // 
            this.btoConfirmar.Location = new System.Drawing.Point(1029, 408);
            this.btoConfirmar.Name = "btoConfirmar";
            this.btoConfirmar.Size = new System.Drawing.Size(177, 52);
            this.btoConfirmar.TabIndex = 1;
            this.btoConfirmar.Text = "Confirmar";
            this.btoConfirmar.UseVisualStyleBackColor = true;
            this.btoConfirmar.Click += new System.EventHandler(this.btoConfirmar_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(81, 422);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Clt_ObterRoteiros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1218, 472);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btoConfirmar);
            this.Controls.Add(this.gdwRoteiros);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Clt_ObterRoteiros";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Defina os roteiros";
            this.Load += new System.EventHandler(this.Clt_ObterRoteiros_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gdwRoteiros)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btoConfirmar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reconhecimento;
        private System.Windows.Forms.DataGridViewComboBoxColumn Roteiro;
        public System.Windows.Forms.DataGridView gdwRoteiros;
        private System.Windows.Forms.Button button1;
    }
}