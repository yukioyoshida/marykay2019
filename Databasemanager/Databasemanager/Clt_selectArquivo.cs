﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Databasemanager
{
    public partial class Clt_selectArquivo : Form
    {
        public string AP_ArquivoSelecionado { get; set; }

        public Clt_selectArquivo()
        {
            InitializeComponent();
        }

        private void Clt_selectArquivo_Load(object sender, EventArgs e)
        {
            AP_ArquivoSelecionado = string.Empty;
        }

        private void btoDiretorio_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog open = new OpenFileDialog();
                open.ShowDialog();

                edtArquivo.Text = open.FileName.ToString();
                string stExtencao = open.FileName.ToString().Substring(open.FileName.ToString().Length - 3, 3);
                if ((stExtencao.ToUpper() != "JPG") && (stExtencao.ToUpper() != "PNG"))
                {
                    MessageBox.Show("Selecione arquivo somente (PNG ou JPG)", "Atenção!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    edtArquivo.Text = string.Empty;
                }
            }
            catch { }
        }

        private void btoSelectArquivo_Click(object sender, EventArgs e)
        {
            if (edtArquivo.Text != string.Empty)
            {
                AP_ArquivoSelecionado = edtArquivo.Text;
                this.Close();
            }
        }
    }
}
