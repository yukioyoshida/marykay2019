﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Databasemanager
{
    public partial class InputBox : Form
    {
        public string AP_Titulo { get; set; }
        public string AP_Mensagem { get; set; }
        public bool AP_ExibirInput { get; set; }
        public string AP_Resultado { get; set; }

        public InputBox()
        {
            InitializeComponent();
        }

        private void InputBox_Load(object sender, EventArgs e)
        {
            this.Text = AP_Titulo;
            lblMensagem.Text = AP_Mensagem;
            edtResultado.Visible = AP_ExibirInput;
        }

        private void btoOK_Click(object sender, EventArgs e)
        {
            AP_Resultado = edtResultado.Text;
            this.Close();
        }
    }
}
