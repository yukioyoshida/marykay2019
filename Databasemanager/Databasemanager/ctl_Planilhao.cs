﻿using OfficeOpenXml;
using SPI;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace Databasemanager
{
    public partial class ctl_Planilhao : UserControl
    {
        public string AP_Seminario = "SELECIONE";
        public string AP_TipoDePlanilhao = "SELECIONE";
        public ctl_Planilhao()
        {
            InitializeComponent();
        }

        private void ctl_Planilhao_Load(object sender, EventArgs e)
        {
            if (AP_Seminario == "SELECIONE")
            {
                Clt_SelectSeminario open = new Clt_SelectSeminario();
                open.ShowDialog();

                AP_Seminario = open.AP_RESULTADO;

                if (AP_Seminario == "SELECIONE")
                {
                    this.Controls.Clear();
                    return;
                }
            }

            if (AP_TipoDePlanilhao == "SELECIONE")
            {
                Clt_TipoPlanilhao open = new Clt_TipoPlanilhao();
                open.ShowDialog();

                AP_TipoDePlanilhao = open.AP_RESULTADO;

                if (AP_TipoDePlanilhao == "SELECIONE")
                {
                    this.Controls.Clear();
                    return;
                }
            }

            progress.Visible = true;
            grpImportacao.Visible = true;
            lblSeminario.Visible = true;
            //btoValidarPlanilhao.Visible = true;
            btoExcel.Visible = true;
            lblQtdRegistros.Visible = true;

            lblSeminario.Text = "SEMINÁRIO: " + AP_Seminario;

            if (AP_TipoDePlanilhao == "PLANILHÃO DO CREDENCIAMENTO")
            {
                progress.Value = 0;
                this.Invoke(new MethodInvoker(MI_ExibirPlanilhao));
            }
            else
            {
                progress.Value = 0;
                this.Invoke(new MethodInvoker(MI_ExibirPlanilhaoJoias));
            }
        }

        private void MI_ExibirPlanilhaoJoias()
        {
            progress.Value = 0;
            DataTable dtPlanilhao = new DAO_Login().MS_ObterPlanilhaoJoias(AP_Seminario);

            progress.Maximum = dtPlanilhao.Rows.Count;

            gdwPlanilhao.Columns.Clear();
            gdwPlanilhao.Rows.Clear();
            gdwPlanilhao.DataSource = dtPlanilhao;
            lblQtdRegistros.Text = gdwPlanilhao.Rows.Count.ToString() + " registros";

            // CENTRALIZAR REGISTROS NO PLANILÃO
            for (int i = 19; i <= gdwPlanilhao.Columns.Count - 1; i++)
            {
                gdwPlanilhao.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
        }

        private void MI_ExibirPlanilhao()
        {
            progress.Value = 0;
            DataTable dtPlanilhao = new DAO_Login().MS_ObterPlanilhao(AP_Seminario);

            progress.Maximum = dtPlanilhao.Rows.Count;

            foreach (DataRow Row in new DAO_Login().MS_ObterRoteiro("I").Rows)
            {
                try
                {
                    dtPlanilhao.Columns.Add(Row["dsReconhecimento"].ToString());
                }
                catch
                {
                    dtPlanilhao.Columns.Add(Row["dsReconhecimento"].ToString() + "_1");
                }
            }

            DataTable dtTodosReconhecimentos = new DAO_Login().MS_ObterTodosOsReconhecimentos();

            // VERIFICAR OS RECONHECIMENTOS
            int itQuantidade = 0;
            foreach (DataRow Row in dtPlanilhao.Rows)
            {
                string prNrConsultora = Row["Consultora"].ToString();

                for (int i = 21; i <= dtPlanilhao.Columns.Count - 1; i++)
                {
                    string prNomeReconhecimento = "";
                    prNomeReconhecimento = dtPlanilhao.Columns[i].ToString();

                    if ((Row["TipodeInscricao"].ToString().Trim().Contains("Convidados")) || "Convidados".Contains(Row["TipodeInscricao"].ToString().Trim()))
                    {
                        switch (prNomeReconhecimento)
                        {
                            case "Convidado":
                                Row[i] = MI_ObterSePossuiReconhecimento(dtTodosReconhecimentos, prNrConsultora, prNomeReconhecimento);
                                break;
                            case "Atividade Especial Convidado":
                                Row[i] = MI_ObterSePossuiReconhecimento(dtTodosReconhecimentos, prNrConsultora, prNomeReconhecimento);
                                break;
                            case "Orientação Exclusiva ganhadoras Especial 20 anos":
                                Row[i] = MI_ObterSePossuiReconhecimento(dtTodosReconhecimentos, prNrConsultora, prNomeReconhecimento);
                                break;
                            case "Area de Assento: Golden Pass (somente)":
                                Row[i] = MI_ObterSePossuiReconhecimento(dtTodosReconhecimentos, prNrConsultora, prNomeReconhecimento);
                                break;
                            case "Coquetel da Realeza Rubi - Convidado":
                                Row[i] = MI_ObterSePossuiReconhecimento(dtTodosReconhecimentos, prNrConsultora, prNomeReconhecimento);
                                break;
                            case "Coquetel da Realeza Safira - Convidado":
                                Row[i] = MI_ObterSePossuiReconhecimento(dtTodosReconhecimentos, prNrConsultora, prNomeReconhecimento);
                                break;
                            case "Almoço Exclusivo Programa Especial 20 anos":
                                Row[i] = MI_ObterSePossuiReconhecimento(dtTodosReconhecimentos, prNrConsultora, prNomeReconhecimento);
                                break;
                            case "Area de Assento: Geral":
                                Row[i] = MI_ObterSePossuiReconhecimento(dtTodosReconhecimentos, prNrConsultora, prNomeReconhecimento);
                                break;
                            case "Entregar Convite da atividade especial do convidado":
                                Row[i] = MI_ObterSePossuiReconhecimento(dtTodosReconhecimentos, prNrConsultora, prNomeReconhecimento);
                                break;
                            default:
                                Row[i] = "";
                                break;
                        }
                    }
                    else
                    {
                        if ((prNomeReconhecimento != "Convidado") && (prNomeReconhecimento != "Convidados") && (prNomeReconhecimento != "Atividade Especial Convidado") && (prNomeReconhecimento != "Coquetel da Realeza Rubi - Convidado") && (prNomeReconhecimento != "Coquetel da Realeza Safira - Convidado") && (prNomeReconhecimento != "Cônjuge") && (prNomeReconhecimento != "Entregar Convite da atividade especial do convidado"))
                        {
                            Row[i] = MI_ObterSePossuiReconhecimento(dtTodosReconhecimentos, prNrConsultora, prNomeReconhecimento);
                        }
                        else
                        {
                            Row[i] = "";
                        }
                    }
                }

                itQuantidade++;
                progress.Value = itQuantidade;
            }

            gdwPlanilhao.Columns.Clear();
            gdwPlanilhao.Rows.Clear();
            gdwPlanilhao.DataSource = dtPlanilhao;
            lblQtdRegistros.Text = gdwPlanilhao.Rows.Count.ToString() + " registros";

            // CENTRALIZAR REGISTROS NO PLANILÃO
            for (int i = 19; i <= gdwPlanilhao.Columns.Count - 1; i++)
            {
                gdwPlanilhao.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }

        }

        private string MI_ObterSePossuiReconhecimento(DataTable dtReconhecimentos, string prConsultora, string prReconhecimento)
        {
            string stResult = string.Empty;

            DataRow[] dataRows = dtReconhecimentos.Select("Consultora='" + prConsultora + "' AND dsReconhecimento='" + prReconhecimento.Replace("_1", "") + "'");

            foreach (DataRow row in dataRows)
            {
                return stResult = "X";
            }

            return stResult;
        }

        private void btoExcel_Click(object sender, EventArgs e)
        {
            progress.Value = 0;
            this.Invoke(new MethodInvoker(MI_GerarExcel));
        }

        private void MI_GerarExcel()
        {
            if (!System.IO.Directory.Exists("C:\\Database Manager\\Gerador"))
            {
                System.IO.Directory.CreateDirectory("C:\\Database Manager\\Gerador\\");
            }

            FileInfo caminhoNomeArquivo = new FileInfo(@"C:\Database Manager\\Gerador\\" + AP_Seminario + "_" + System.DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".xlsx");
            ExcelPackage arquivoExcel = new ExcelPackage(caminhoNomeArquivo);

            // CRIANDO (ADD) uma planilha neste arquivo e obtendo a referência para meu código operá-la.
            ExcelWorksheet planilha = arquivoExcel.Workbook.Worksheets.Add("Planilhao" + AP_Seminario);

            for (int i = 1; i < gdwPlanilhao.Columns.Count + 1; i++)
            {
                planilha.Cells[1, i].Value = gdwPlanilhao.Columns[i - 1].HeaderText;
            }

            progress.Value = 0;
            progress.Maximum = gdwPlanilhao.Rows.Count;
            int itQuantidade = 0;
            for (int i = 0; i < gdwPlanilhao.Rows.Count; i++)
            {
                for (int j = 0; j < gdwPlanilhao.Columns.Count; j++)
                {
                    planilha.Cells[i + 2, j + 1].Value = gdwPlanilhao.Rows[i].Cells[j].Value.ToString();
                }
                itQuantidade++;
                progress.Value = itQuantidade;
            }

            arquivoExcel.Save();
            arquivoExcel.Dispose();

            System.Diagnostics.Process.Start(caminhoNomeArquivo.FullName.ToString());
        }

        private void btoValidarPlanilhao_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Regras não cadastradas", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
