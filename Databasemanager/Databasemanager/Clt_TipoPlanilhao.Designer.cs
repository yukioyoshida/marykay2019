﻿namespace Databasemanager
{
    partial class Clt_TipoPlanilhao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btoOK = new System.Windows.Forms.Button();
            this.ddlSeminario = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btoOK
            // 
            this.btoOK.Location = new System.Drawing.Point(280, 86);
            this.btoOK.Name = "btoOK";
            this.btoOK.Size = new System.Drawing.Size(75, 40);
            this.btoOK.TabIndex = 5;
            this.btoOK.Text = "OK";
            this.btoOK.UseVisualStyleBackColor = true;
            this.btoOK.Click += new System.EventHandler(this.btoOK_Click);
            // 
            // ddlSeminario
            // 
            this.ddlSeminario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSeminario.FormattingEnabled = true;
            this.ddlSeminario.Items.AddRange(new object[] {
            "SELECIONE",
            "PLANILHÃO DO CREDENCIAMENTO",
            "PLANILHÃO DE JOIAS"});
            this.ddlSeminario.Location = new System.Drawing.Point(21, 47);
            this.ddlSeminario.Name = "ddlSeminario";
            this.ddlSeminario.Size = new System.Drawing.Size(334, 24);
            this.ddlSeminario.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Selecione o tipo de planilhão";
            // 
            // Clt_TipoPlanilhao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 152);
            this.Controls.Add(this.btoOK);
            this.Controls.Add(this.ddlSeminario);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Clt_TipoPlanilhao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tipo de Planilhão";
            this.Load += new System.EventHandler(this.Clt_TipoPlanilhao_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btoOK;
        private System.Windows.Forms.ComboBox ddlSeminario;
        private System.Windows.Forms.Label label1;
    }
}