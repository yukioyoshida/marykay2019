﻿using System;
using System.Windows.Forms;

namespace Databasemanager
{
    public partial class Clt_Painel : Form
    {
        public Clt_Painel()
        {
            InitializeComponent();
        }

        private void Clt_Painel_Load(object sender, EventArgs e)
        {
            MI_SetNivelAcesso();
        }

        private void MI_SetNivelAcesso()
        {
            if (Csv_Variaveis.AP_NivelUsuario.ToString() == "2")
            {
                btoImportarPlanilha.Enabled = false;
                button1.Enabled = false;
                button2.Enabled = false;
                btoAreaAcentos.Enabled = false;
                btoResumoConsultora.Enabled = true;
                btoPremiacoes.Enabled = true;
                
            }
            else
            {

            }

            if(!Csv_Variaveis.AP_NivelUsuario.ToString().Equals("1"))
                btoAprovarBase.Enabled = false;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
        }

        private void btoImportarPlanilha_Click(object sender, EventArgs e)
        {
            pnlPalco.Controls.Clear();
            pnlPalco.Controls.Add(new ctl_importacaoPlanilha());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pnlPalco.Controls.Clear();
            pnlPalco.Controls.Add(new ctl_cheklist());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pnlPalco.Controls.Clear();
            pnlPalco.Controls.Add(new ctl_Planilhao());
        }

        private void btoAreaAcentos_Click(object sender, EventArgs e)
        {
            pnlPalco.Controls.Clear();
            pnlPalco.Controls.Add(new ctl_AreaAssento());
        }

        private void btoResumoConsultora_Click(object sender, EventArgs e)
        {
            pnlPalco.Controls.Clear();
            pnlPalco.Controls.Add(new ctl_ResumoConsultora("RECONHECIMENTOS"));
        }

        private void btoPremiacoes_Click(object sender, EventArgs e)
        {
            pnlPalco.Controls.Clear();
            pnlPalco.Controls.Add(new ctl_ResumoConsultora("JOIAS"));
        }

        private void btoAprovarBase_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("(ATENÇÃO) Deseja realmente aprovar a base de dados?", "ATENÇÃO!", MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            try
            {
                new CAT_CadastrarVisitante().MS_AprovarBase(Csv_Variaveis.AP_nmUsuario.ToString());

                MessageBox.Show("Base de dados aprovada com sucesso!", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Erro ao realizar a aprovação da base de dados. Entre em contato com o administrador do sistema.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
