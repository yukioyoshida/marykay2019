﻿namespace Databasemanager
{
    partial class ctl_cheklist
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpImportacao = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gdwReconhecimento = new System.Windows.Forms.DataGridView();
            this.Reconhecimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataImportacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btoEditRoteiro = new System.Windows.Forms.PictureBox();
            this.lblRoteiro = new System.Windows.Forms.Label();
            this.lblInscricao = new System.Windows.Forms.Label();
            this.lblBancoMaryKay = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btoEditRoteiroJoia = new System.Windows.Forms.PictureBox();
            this.lblRoteiroJoias = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbtInscricao = new System.Windows.Forms.RadioButton();
            this.rbtJoias = new System.Windows.Forms.RadioButton();
            this.grpImportacao.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdwReconhecimento)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btoEditRoteiro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoEditRoteiroJoia)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpImportacao
            // 
            this.grpImportacao.Controls.Add(this.groupBox2);
            this.grpImportacao.Controls.Add(this.groupBox1);
            this.grpImportacao.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpImportacao.Location = new System.Drawing.Point(16, 16);
            this.grpImportacao.Name = "grpImportacao";
            this.grpImportacao.Size = new System.Drawing.Size(623, 733);
            this.grpImportacao.TabIndex = 2;
            this.grpImportacao.TabStop = false;
            this.grpImportacao.Text = "Checklist";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.gdwReconhecimento);
            this.groupBox2.Location = new System.Drawing.Point(22, 232);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(573, 476);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Roteiro";
            // 
            // gdwReconhecimento
            // 
            this.gdwReconhecimento.AllowUserToAddRows = false;
            this.gdwReconhecimento.AllowUserToResizeColumns = false;
            this.gdwReconhecimento.AllowUserToResizeRows = false;
            this.gdwReconhecimento.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.gdwReconhecimento.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gdwReconhecimento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdwReconhecimento.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Reconhecimento,
            this.DataImportacao,
            this.Quantidade});
            this.gdwReconhecimento.Location = new System.Drawing.Point(6, 60);
            this.gdwReconhecimento.MultiSelect = false;
            this.gdwReconhecimento.Name = "gdwReconhecimento";
            this.gdwReconhecimento.RowHeadersVisible = false;
            this.gdwReconhecimento.RowTemplate.Height = 24;
            this.gdwReconhecimento.Size = new System.Drawing.Size(561, 410);
            this.gdwReconhecimento.TabIndex = 0;
            // 
            // Reconhecimento
            // 
            this.Reconhecimento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Reconhecimento.DefaultCellStyle = dataGridViewCellStyle1;
            this.Reconhecimento.HeaderText = "Reconhecimento";
            this.Reconhecimento.Name = "Reconhecimento";
            this.Reconhecimento.ReadOnly = true;
            // 
            // DataImportacao
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataImportacao.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataImportacao.HeaderText = "Importação";
            this.DataImportacao.Name = "DataImportacao";
            this.DataImportacao.ReadOnly = true;
            // 
            // Quantidade
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Quantidade.DefaultCellStyle = dataGridViewCellStyle3;
            this.Quantidade.HeaderText = "Quant.";
            this.Quantidade.Name = "Quantidade";
            this.Quantidade.ReadOnly = true;
            this.Quantidade.Width = 70;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btoEditRoteiroJoia);
            this.groupBox1.Controls.Add(this.lblRoteiroJoias);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btoEditRoteiro);
            this.groupBox1.Controls.Add(this.lblRoteiro);
            this.groupBox1.Controls.Add(this.lblInscricao);
            this.groupBox1.Controls.Add(this.lblBancoMaryKay);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(22, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(573, 192);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Planilhas de controle";
            // 
            // btoEditRoteiro
            // 
            this.btoEditRoteiro.BackColor = System.Drawing.Color.White;
            this.btoEditRoteiro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btoEditRoteiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoEditRoteiro.Image = global::Databasemanager.Properties.Resources.edit;
            this.btoEditRoteiro.Location = new System.Drawing.Point(205, 108);
            this.btoEditRoteiro.Name = "btoEditRoteiro";
            this.btoEditRoteiro.Size = new System.Drawing.Size(30, 30);
            this.btoEditRoteiro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoEditRoteiro.TabIndex = 3;
            this.btoEditRoteiro.TabStop = false;
            this.btoEditRoteiro.Click += new System.EventHandler(this.btoEditRoteiro_Click);
            // 
            // lblRoteiro
            // 
            this.lblRoteiro.BackColor = System.Drawing.Color.Gainsboro;
            this.lblRoteiro.Location = new System.Drawing.Point(205, 108);
            this.lblRoteiro.Name = "lblRoteiro";
            this.lblRoteiro.Size = new System.Drawing.Size(362, 30);
            this.lblRoteiro.TabIndex = 5;
            this.lblRoteiro.Text = "[lblRoteiro]";
            this.lblRoteiro.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblInscricao
            // 
            this.lblInscricao.BackColor = System.Drawing.Color.Gainsboro;
            this.lblInscricao.Location = new System.Drawing.Point(205, 69);
            this.lblInscricao.Name = "lblInscricao";
            this.lblInscricao.Size = new System.Drawing.Size(362, 30);
            this.lblInscricao.TabIndex = 4;
            this.lblInscricao.Text = "[lblInscricao]";
            this.lblInscricao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBancoMaryKay
            // 
            this.lblBancoMaryKay.BackColor = System.Drawing.Color.Gainsboro;
            this.lblBancoMaryKay.Location = new System.Drawing.Point(205, 33);
            this.lblBancoMaryKay.Name = "lblBancoMaryKay";
            this.lblBancoMaryKay.Size = new System.Drawing.Size(362, 30);
            this.lblBancoMaryKay.TabIndex = 3;
            this.lblBancoMaryKay.Text = "[lblBancoMaryKay]";
            this.lblBancoMaryKay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(6, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(193, 30);
            this.label3.TabIndex = 2;
            this.label3.Text = "Roteiro";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(6, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(193, 30);
            this.label2.TabIndex = 1;
            this.label2.Text = "Inscrição";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(6, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Banco de MaryKay";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btoEditRoteiroJoia
            // 
            this.btoEditRoteiroJoia.BackColor = System.Drawing.Color.White;
            this.btoEditRoteiroJoia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btoEditRoteiroJoia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoEditRoteiroJoia.Image = global::Databasemanager.Properties.Resources.edit;
            this.btoEditRoteiroJoia.Location = new System.Drawing.Point(205, 147);
            this.btoEditRoteiroJoia.Name = "btoEditRoteiroJoia";
            this.btoEditRoteiroJoia.Size = new System.Drawing.Size(30, 30);
            this.btoEditRoteiroJoia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoEditRoteiroJoia.TabIndex = 7;
            this.btoEditRoteiroJoia.TabStop = false;
            this.btoEditRoteiroJoia.Click += new System.EventHandler(this.btoEditRoteiroJoia_Click);
            // 
            // lblRoteiroJoias
            // 
            this.lblRoteiroJoias.BackColor = System.Drawing.Color.Gainsboro;
            this.lblRoteiroJoias.Location = new System.Drawing.Point(205, 147);
            this.lblRoteiroJoias.Name = "lblRoteiroJoias";
            this.lblRoteiroJoias.Size = new System.Drawing.Size(362, 30);
            this.lblRoteiroJoias.TabIndex = 8;
            this.lblRoteiroJoias.Text = "[lblRoteiro]";
            this.lblRoteiroJoias.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Gray;
            this.label5.Location = new System.Drawing.Point(6, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(193, 30);
            this.label5.TabIndex = 6;
            this.label5.Text = "Roteiro de jóias";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbtJoias);
            this.groupBox3.Controls.Add(this.rbtInscricao);
            this.groupBox3.Location = new System.Drawing.Point(356, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(211, 46);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Filtro";
            // 
            // rbtInscricao
            // 
            this.rbtInscricao.AutoSize = true;
            this.rbtInscricao.Checked = true;
            this.rbtInscricao.Location = new System.Drawing.Point(21, 18);
            this.rbtInscricao.Name = "rbtInscricao";
            this.rbtInscricao.Size = new System.Drawing.Size(95, 22);
            this.rbtInscricao.TabIndex = 0;
            this.rbtInscricao.TabStop = true;
            this.rbtInscricao.Text = "Inscrição";
            this.rbtInscricao.UseVisualStyleBackColor = true;
            this.rbtInscricao.CheckedChanged += new System.EventHandler(this.rbtInscricao_CheckedChanged);
            // 
            // rbtJoias
            // 
            this.rbtJoias.AutoSize = true;
            this.rbtJoias.Location = new System.Drawing.Point(131, 18);
            this.rbtJoias.Name = "rbtJoias";
            this.rbtJoias.Size = new System.Drawing.Size(66, 22);
            this.rbtJoias.TabIndex = 1;
            this.rbtJoias.Text = "Jóias";
            this.rbtJoias.UseVisualStyleBackColor = true;
            this.rbtJoias.CheckedChanged += new System.EventHandler(this.rbtJoias_CheckedChanged);
            // 
            // ctl_cheklist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.grpImportacao);
            this.Name = "ctl_cheklist";
            this.Size = new System.Drawing.Size(870, 768);
            this.Load += new System.EventHandler(this.ctl_cheklist_Load);
            this.grpImportacao.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gdwReconhecimento)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btoEditRoteiro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoEditRoteiroJoia)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpImportacao;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblRoteiro;
        private System.Windows.Forms.Label lblInscricao;
        private System.Windows.Forms.Label lblBancoMaryKay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView gdwReconhecimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reconhecimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataImportacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantidade;
        private System.Windows.Forms.PictureBox btoEditRoteiro;
        private System.Windows.Forms.PictureBox btoEditRoteiroJoia;
        private System.Windows.Forms.Label lblRoteiroJoias;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbtJoias;
        private System.Windows.Forms.RadioButton rbtInscricao;
    }
}
