﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SPI;

namespace Databasemanager
{
    public partial class ctl_ResumoConsultora : UserControl
    {
        string AP_ObjetivoBusca = string.Empty;
        DataGridViewRow RowSelected;
        DataTable dtConsultora;

        public ctl_ResumoConsultora(string prObjetivo)
        {
            InitializeComponent();
            AP_ObjetivoBusca = prObjetivo;
        }

        private void ctl_ResumoConsultora_Load(object sender, EventArgs e)
        {
            MI_CarregarConsultura();
        }

        private void MI_CarregarConsultura()
        {
            dtConsultora = new DAO_Login().MS_ObterTodasAsConsultoras();
            MI_CarregarGrid();
        }

        private void MI_CarregarGrid()
        {
            gdwTable.Rows.Clear();

            foreach (DataRow Row in dtConsultora.Select("Consultora like '%" + edtChave.Text.Trim() + "%' OR NomedaConsultora LIKE '%" + edtChave.Text.Trim() + "%'"))
            {
                bool boInsert = true;
                if (AP_ObjetivoBusca == "JOIAS")
                {
                    if ((Row["TipodeInscricao"].ToString().ToUpper().Contains("CONVIDADOS") || ("CONVIDADOS").Contains(Row["TipodeInscricao"].ToString().ToUpper())))
                    {
                        boInsert = false;
                    }
                }

                if (boInsert)
                {
                    gdwTable.Rows.Add(
                        Row["cdInscricao"].ToString(),
                        Row["Consultora"].ToString(),
                        Row["TipodeInscricao"].ToString(),
                        Row["NomedaConsultora"].ToString()
                        );
                }
            }

            lblQuantidadeRegistros.Text = gdwTable.Rows.Count.ToString() + " registros";

        }

        private void btoFiltrar_Click(object sender, EventArgs e)
        {
            MI_CarregarGrid();
        }

        private void gdwTable_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewRow Row in gdwTable.Rows)
            {
                for (int i = 0; i <= Row.Cells.Count - 1; i++)
                    Row.Cells[i].Style.BackColor = System.Drawing.Color.White;
            }

            RowSelected = gdwTable.CurrentRow;

            for (int i = 0; i <= RowSelected.Cells.Count - 1; i++)
                RowSelected.Cells[i].Style.BackColor = System.Drawing.Color.LightBlue;

            Clt_DetalhesConsultora open = new Clt_DetalhesConsultora();
            open.AP_Inscricao = RowSelected.Cells[0].Value.ToString();
            open.AP_Consultora = RowSelected.Cells[1].Value.ToString();
            open.AP_TipoDetalhes = AP_ObjetivoBusca;
            open.ShowDialog();
        }

        private void gdwTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }
    }
}
