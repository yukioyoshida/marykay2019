﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SPI;

namespace Databasemanager
{
    public partial class Clt_Autenticacao : Form
    {
        public Clt_Autenticacao()
        {
            InitializeComponent();
        }

        private void MI_SetVersao()
        {
            lblVersao.Text = "versão " + Csv_Variaveis.AP_Versao.ToString() + ".0 - 2019";
        }

        private void btoAcessar_Click(object sender, EventArgs e)
        {
            if (MI_Consistencias())
            {
                CTO_Login ITO_Login = new DAO_Login().MS_Logar(edtEmail.Text.Trim().ToString(), edtSenha.Text.Trim());
;               if (ITO_Login == null)
                {
                    MessageBox.Show("Usuário ou senha inválido!", "Autenticação", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (MI_AnalisarVersaoSistema() == false)
                    {
                        MessageBox.Show("A versão deste software esta desatualizada!", "Autenticação", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else
                    {

                        Csv_Variaveis.AP_cdUsuario = ITO_Login.getObjRef();
                        Csv_Variaveis.AP_nmUsuario = ITO_Login.getNomeUsuario();
                        Csv_Variaveis.AP_NivelUsuario = ITO_Login.getNivelAcesso();

                        // REGISTRAR LOG
                        new CAT_CadastrarLogin().MS_RegistrarLog("LOGOU NO SISTEMA", Csv_Variaveis.AP_cdUsuario);

                        this.Hide();

                        new Clt_Painel().ShowDialog();

                        // REGISTRAR LOG
                        new CAT_CadastrarLogin().MS_RegistrarLog("FECHOU O SISTEMA", Csv_Variaveis.AP_cdUsuario);

                        // FINALIZAR O SISTEMA
                        Application.ExitThread();
                    }
                }
            }
        }

        public bool MI_AnalisarVersaoSistema()
        {
            bool boResult = true;

            if (Csv_Variaveis.AP_Versao != Convert.ToInt32(new DAO_Login().MS_ObterVersao().Rows[0]["cdVersao"]))
                boResult = false;

            return boResult;
        }

        private bool MI_Consistencias()
        {
            bool boResult = true;

            if (edtEmail.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Informe seu usuário", "Autenticação", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return boResult = false;
            }
            if (edtEmail.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Informe seu usuário", "Autenticação", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return boResult = false;
            }

            return boResult;
        }

        private void Clt_Autenticacao_Load(object sender, EventArgs e)
        {
            MI_SetVersao();
            pnlPalco.Location = new Point((this.Width / 2) - pnlPalco.Width / 2, 20);
        }
    }
}
