﻿namespace Databasemanager
{
    partial class Clt_Painel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btoAprovarBase = new System.Windows.Forms.Button();
            this.btoPremiacoes = new System.Windows.Forms.Button();
            this.btoResumoConsultora = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btoAreaAcentos = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btoImportarPlanilha = new System.Windows.Forms.Button();
            this.pnlPalco = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Databasemanager.Properties.Resources.BackDBManager;
            this.pictureBox1.Location = new System.Drawing.Point(808, 422);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(209, 193);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btoAprovarBase);
            this.panel1.Controls.Add(this.btoPremiacoes);
            this.panel1.Controls.Add(this.btoResumoConsultora);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.btoAreaAcentos);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btoImportarPlanilha);
            this.panel1.Location = new System.Drawing.Point(0, -1);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(214, 724);
            this.panel1.TabIndex = 14;
            // 
            // btoAprovarBase
            // 
            this.btoAprovarBase.Location = new System.Drawing.Point(8, 515);
            this.btoAprovarBase.Margin = new System.Windows.Forms.Padding(2);
            this.btoAprovarBase.Name = "btoAprovarBase";
            this.btoAprovarBase.Size = new System.Drawing.Size(194, 73);
            this.btoAprovarBase.TabIndex = 22;
            this.btoAprovarBase.TabStop = false;
            this.btoAprovarBase.Text = "Aprovar base de dados";
            this.btoAprovarBase.UseVisualStyleBackColor = true;
            this.btoAprovarBase.Click += new System.EventHandler(this.btoAprovarBase_Click);
            // 
            // btoPremiacoes
            // 
            this.btoPremiacoes.Location = new System.Drawing.Point(8, 436);
            this.btoPremiacoes.Margin = new System.Windows.Forms.Padding(2);
            this.btoPremiacoes.Name = "btoPremiacoes";
            this.btoPremiacoes.Size = new System.Drawing.Size(194, 73);
            this.btoPremiacoes.TabIndex = 21;
            this.btoPremiacoes.TabStop = false;
            this.btoPremiacoes.Text = "Premiações";
            this.btoPremiacoes.UseVisualStyleBackColor = true;
            this.btoPremiacoes.Click += new System.EventHandler(this.btoPremiacoes_Click);
            // 
            // btoResumoConsultora
            // 
            this.btoResumoConsultora.Location = new System.Drawing.Point(8, 358);
            this.btoResumoConsultora.Margin = new System.Windows.Forms.Padding(2);
            this.btoResumoConsultora.Name = "btoResumoConsultora";
            this.btoResumoConsultora.Size = new System.Drawing.Size(194, 73);
            this.btoResumoConsultora.TabIndex = 20;
            this.btoResumoConsultora.TabStop = false;
            this.btoResumoConsultora.Text = "Inscrições";
            this.btoResumoConsultora.UseVisualStyleBackColor = true;
            this.btoResumoConsultora.Click += new System.EventHandler(this.btoResumoConsultora_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(8, 341);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(194, 6);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            // 
            // btoAreaAcentos
            // 
            this.btoAreaAcentos.Location = new System.Drawing.Point(8, 261);
            this.btoAreaAcentos.Margin = new System.Windows.Forms.Padding(2);
            this.btoAreaAcentos.Name = "btoAreaAcentos";
            this.btoAreaAcentos.Size = new System.Drawing.Size(194, 73);
            this.btoAreaAcentos.TabIndex = 18;
            this.btoAreaAcentos.TabStop = false;
            this.btoAreaAcentos.Text = "Definir área assento";
            this.btoAreaAcentos.UseVisualStyleBackColor = true;
            this.btoAreaAcentos.Click += new System.EventHandler(this.btoAreaAcentos_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(8, 244);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(194, 6);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(8, 166);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(194, 73);
            this.button2.TabIndex = 17;
            this.button2.TabStop = false;
            this.button2.Text = "Visualizar planilhão";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 88);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(194, 73);
            this.button1.TabIndex = 16;
            this.button1.TabStop = false;
            this.button1.Text = "Visualizar planilhas importadas";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btoImportarPlanilha
            // 
            this.btoImportarPlanilha.Location = new System.Drawing.Point(8, 10);
            this.btoImportarPlanilha.Margin = new System.Windows.Forms.Padding(2);
            this.btoImportarPlanilha.Name = "btoImportarPlanilha";
            this.btoImportarPlanilha.Size = new System.Drawing.Size(194, 73);
            this.btoImportarPlanilha.TabIndex = 15;
            this.btoImportarPlanilha.TabStop = false;
            this.btoImportarPlanilha.Text = "Importação de Planilha";
            this.btoImportarPlanilha.UseVisualStyleBackColor = true;
            this.btoImportarPlanilha.Click += new System.EventHandler(this.btoImportarPlanilha_Click);
            // 
            // pnlPalco
            // 
            this.pnlPalco.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPalco.BackColor = System.Drawing.Color.Transparent;
            this.pnlPalco.Location = new System.Drawing.Point(225, 10);
            this.pnlPalco.Margin = new System.Windows.Forms.Padding(2);
            this.pnlPalco.Name = "pnlPalco";
            this.pnlPalco.Size = new System.Drawing.Size(578, 605);
            this.pnlPalco.TabIndex = 15;
            // 
            // Clt_Painel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1100, 715);
            this.Controls.Add(this.pnlPalco);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Clt_Painel";
            this.Text = "Painel de Controle - Mary Kay 2019";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Clt_Painel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btoImportarPlanilha;
        private System.Windows.Forms.Panel pnlPalco;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btoAreaAcentos;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btoResumoConsultora;
        private System.Windows.Forms.Button btoPremiacoes;
        private System.Windows.Forms.Button btoAprovarBase;
    }
}