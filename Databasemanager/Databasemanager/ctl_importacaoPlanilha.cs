﻿using SPI;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Databasemanager
{
    public partial class ctl_importacaoPlanilha : UserControl
    {
        public string AP_LegendaStatus { get; set; }
        public bool boInterromper = false;

        public ctl_importacaoPlanilha()
        {
            InitializeComponent();
        }

        private void btoDiretorio_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog open = new OpenFileDialog();
                open.ShowDialog();

                edtArquivo.Text = open.FileName.ToString();
                string stExtencao = open.FileName.ToString().Substring(open.FileName.ToString().Length - 3, 3);
                if (stExtencao.ToUpper() == "TXT")
                {
                    ddlTipo.Enabled = false;
                    ddlTipo.Text = "01 - PLANILHA GERAL DAS CONSULTORAS";
                }
                else
                {
                    ddlTipo.Enabled = true;
                }
            }
            catch { }
        }

        private void btoAcessar_Click(object sender, EventArgs e)
        {
            progress.Value = 0;
            gropImportacao.Visible = true;
            this.Invoke(new MethodInvoker(MI_IniciarImportacao));
        }

        private void MI_LimparTela()
        {
            edtArquivo.Text = String.Empty;
            ddlTipo.Text = string.Empty;

            ddlRoteiro.Text = "SELECIONE";
        }

        private void MI_IniciarImportacao()
        {
            boInterromper = false;

            if (ddlTipo.Text == "01 - PLANILHA GERAL DAS CONSULTORAS")
            {
                MI_ImportarTxt();
                MI_LimparTela();
            }
            else if (ddlTipo.Text == "02 - ROTEIRO")
            {
                MI_ImportarRoteiro();
                MI_LimparTela();
            }
            else if (ddlTipo.Text == "02.1 - ROTEIRO JOIAS")
            {
                MI_ImportarRoteiroJoias();
                MI_LimparTela();
            }
            else if (ddlTipo.Text == "03 - INSCRIÇÃO")
            {
                MI_ImportarInscricao("N");
                MI_LimparTela();
            }
            else if (ddlTipo.Text == "03.1 - INSCRIÇÃO ADICIONAL")
            {
                MI_ImportarInscricao("A");
                MI_LimparTela();
            }
            else if (ddlTipo.Text == "04 - JOIAS")
            {
                MI_ImportarJoias();
                MI_LimparTela();
            }
            else if (ddlTipo.Text == "04.1 - JOIAS DIVIDIDO")
            {
                MI_ImportarJoiasDividido();
                MI_LimparTela();
            }
            else if (ddlTipo.Text == "05 - CONTEST WINNER")
            {
                MI_ImportarContestWinner();
                MI_LimparTela();
            }
            else if (ddlTipo.Text == "05.1 - CONTEST WINNER DIVIDIDO")
            {
                MI_ImportarContestWinnerDividido();
                MI_LimparTela();
            }
            else if (ddlTipo.Text == "06 - NÍVEL DE CARREIRA")
            {
                MI_ImportarNivelDeCarreira();
                MI_LimparTela();
            }
            else if (ddlTipo.Text == "07 - ATUALIZAÇÃO CONVIDADOS")
            {
                MI_ImportarConvidados();
                MI_LimparTela();
            }
        }

        private void MI_ImportarTxt()
        {
            if (MessageBox.Show("(ATENÇÃO) Esta operação ira excluir o banco geral existente.\nDeseja realmente continuar", "ATENÇÃO!", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            // REGISTRAR LOG
            new CAT_CadastrarLogin().MS_RegistrarLog("01 - PLANILHA GERAL DAS CONSULTORAS", Csv_Variaveis.AP_cdUsuario);

            // EXCLUIR BANCO MARYKAY ATUAL
            new CAT_CadastrarLogin().MI_ExcluirBancoGeral();

            FileStream textFile = new FileStream(edtArquivo.Text, FileMode.Open, FileAccess.ReadWrite);
            StreamReader srFile = new StreamReader(textFile);

            string line = string.Empty;
            int itQuantidade = 0;

            while ((line = srFile.ReadLine()) != null)
                itQuantidade++;

            progress.Maximum = itQuantidade;
            textFile.Close();

            //Dim ci1 As New CultureInfo("pt-BR")
            //Using objStreamReader As New StreamReader(fileName, Encoding.GetEncoding(ci1.TextInfo.ANSICodePage))

            textFile = new FileStream(edtArquivo.Text, FileMode.Open, FileAccess.ReadWrite);
            srFile = new StreamReader(textFile, Encoding.GetEncoding("iso-8859-1"));
            itQuantidade = 0;

            List<CTO_BancoGeral> lITO_BancoGeral = new List<CTO_BancoGeral>();
            int itInsert = 0;
            while ((line = srFile.ReadLine()) != null)
            {
                itQuantidade++;

                string[] linha = line.Split('	');
                CTO_BancoGeral ITO_BancoGeral = new CTO_BancoGeral();
                ITO_BancoGeral.AP_NrConsultora = MI_FormatarNrConsultora(linha[0].ToString().Trim().ToUpper());
                ITO_BancoGeral.AP_NmConsultora = linha[1].ToString().Trim().ToUpper();
                ITO_BancoGeral.AP_CPF = linha[2].ToString().Trim().ToUpper().PadLeft(11, '0');
                ITO_BancoGeral.AP_Branco = linha[3].ToString().Trim().ToUpper();
                ITO_BancoGeral.AP_NmNSN = linha[4].ToString().Trim().ToUpper();
                ITO_BancoGeral.AP_NivelDeCarreira = linha[5].ToString().Trim().ToUpper();

                lITO_BancoGeral.Add(ITO_BancoGeral);
                if (itInsert == 400)
                {
                    new CAT_CadastrarLogin().MI_ImportarBancoGeral(lITO_BancoGeral);
                    lITO_BancoGeral.Clear();
                    itInsert = 0;
                }
                else
                {
                    itInsert++;
                }
                progress.Value = itQuantidade;
            }

            // INSERIR O RESTANTE
            if (itInsert > 0)
            {
                new CAT_CadastrarLogin().MI_ImportarBancoGeral(lITO_BancoGeral);
                lITO_BancoGeral.Clear();
            }

            textFile.Close();
            MessageBox.Show("Operação realizada com sucesso!\n" + itQuantidade.ToString() + " registros importados", "Confirmação");
        }

        private void MI_ImportarRoteiro()
        {
            if (MessageBox.Show("(ATENÇÃO) Esta operação ira excluir o roteiro atual e todos os reconhecimentos.\nDeseja realmente continuar?", "ATENÇÃO!", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            // REGISTRAR LOG
            new CAT_CadastrarLogin().MS_RegistrarLog("02 - ROTEIRO", Csv_Variaveis.AP_cdUsuario);

            // EXCLUIR ROTEIRO ATUAL
            new CAT_CadastrarLogin().MI_ExcluirRoteiro("I");

            DataTable dtRegistros = new Csv_LerExcel().MS_ObterExcel(edtArquivo.Text);

            int itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Credenciamento"].ToString().Trim() != String.Empty)
                {
                    itQuantidade++;
                    progress.Maximum = itQuantidade;
                }
            }

            itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Credenciamento"].ToString().Trim() != String.Empty)
                {
                    CTO_Roteiro ITO_Roteiro = new CTO_Roteiro();
                    ITO_Roteiro.AP_Reconhecimento = Row["Credenciamento"].ToString().Trim();
                    ITO_Roteiro.AP_Local = Row["Local"].ToString().Trim();
                    ITO_Roteiro.AP_NomeCredencial = Row["Nome Credencial"].ToString().Trim();
                    ITO_Roteiro.AP_TipoRoteiro = "I";
                    ITO_Roteiro.AP_TipoJoia = "";
                    ITO_Roteiro.AP_Dia = Row["Dia"].ToString().Trim().Replace("-", String.Empty);

                    new DAO_Login().MS_IncluirRoteiro(ITO_Roteiro);
                    itQuantidade++;
                    progress.Value = itQuantidade;
                }
            }

            MessageBox.Show("Roteiro importado com sucesso!\n" + itQuantidade.ToString() + " registro(s)", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            MI_LimparTela();
        }

        private void MI_ImportarRoteiroJoias()
        {
            if (MessageBox.Show("(ATENÇÃO) Esta operação ira excluir o roteiro atual de jóias e todos os reconhecimentos.\nDeseja realmente continuar?", "ATENÇÃO!", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            // REGISTRAR LOG
            new CAT_CadastrarLogin().MS_RegistrarLog("02.1 - ROTEIRO JOIAS", Csv_Variaveis.AP_cdUsuario);

            // EXCLUIR ROTEIRO ATUAL
            new CAT_CadastrarLogin().MI_ExcluirRoteiro("J");

            DataTable dtRegistros = new Csv_LerExcel().MS_ObterExcel(edtArquivo.Text);

            int itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Roteiro sala de joias - premios"].ToString().Trim() != String.Empty)
                {
                    itQuantidade++;
                    progress.Maximum = itQuantidade;
                }
            }

            itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Roteiro sala de joias - premios"].ToString().Trim() != String.Empty)
                {
                    CTO_Roteiro ITO_Roteiro = new CTO_Roteiro();
                    ITO_Roteiro.AP_Reconhecimento = Row["Roteiro sala de joias - premios"].ToString().Trim();
                    ITO_Roteiro.AP_Local = "ENTREGA DE JOIAS";
                    ITO_Roteiro.AP_NomeCredencial = "";
                    ITO_Roteiro.AP_TipoRoteiro = "J";
                    ITO_Roteiro.AP_TipoJoia = Row["Tipo"].ToString().Trim();
                    ITO_Roteiro.AP_Dia = "";
                    ITO_Roteiro.AP_dsCodigoBarrasProduto = Row["CodigoBarras"].ToString().Trim();
                    ITO_Roteiro.AP_dsQuantidadeEstoque = Row["Quantidade"].ToString().Trim();

                    new DAO_Login().MS_IncluirRoteiro(ITO_Roteiro);
                    itQuantidade++;
                    progress.Value = itQuantidade;
                }
            }

            MessageBox.Show("Roteiro importado com sucesso!\n" + itQuantidade.ToString() + " registro(s)", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            MI_LimparTela();
        }

        private void MI_ImportarInscricao(string prTipo) // A = ADICIONAL N = NORMAL
        {
            if (MessageBox.Show("(ATENÇÃO) Esta operação ira excluir as inscrições.\nDeseja realmente continuar", "ATENÇÃO!", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            // REGISTRAR LOG
            if (prTipo == "A")
            {
                new CAT_CadastrarLogin().MS_RegistrarLog("03.1 - INSCRIÇÃO ADICIONAL", Csv_Variaveis.AP_cdUsuario);
            }
            else
            {
                new CAT_CadastrarLogin().MS_RegistrarLog("03 - INSCRIÇÃO", Csv_Variaveis.AP_cdUsuario);
            }

            // EXCLUIR ROTEIRO ATUAL
            new CAT_CadastrarLogin().MI_ExcluirInscricao(prTipo);

            DataTable dtRegistros = new Csv_LerExcel().MS_ObterExcel(edtArquivo.Text);

            int itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Pedido"].ToString().Trim() != String.Empty)
                {
                    itQuantidade++;
                    progress.Maximum = itQuantidade;
                }
            }

            try
            {
                itQuantidade = 0;
                foreach (DataRow Row in dtRegistros.Rows)
                {
                    if (Row["Pedido"].ToString().Trim() != String.Empty)
                    {
                        // CONSULTAR BASE GERAL MK -- PARA OBTER NSD E NIVEL DE CARREIRA
                        DataTable dtBaseMK = new DAO_Login().MS_ObterRegistroBaseMK(MI_FormatarNrConsultora(Row["Consultora"].ToString().Trim()));
                        string NSDMK = string.Empty;
                        string NivelMK = string.Empty;

                        if (dtBaseMK.Rows.Count > 0)
                        {
                            NSDMK = dtBaseMK.Rows[0]["nmNSD"].ToString().Trim();
                            NivelMK = dtBaseMK.Rows[0]["NivelDeCarreira"].ToString().Trim();
                        }

                        // INSERIR NA TABELA DE INSCRICAO
                        CTO_Inscricao ITO_Inscricao = new CTO_Inscricao();
                        ITO_Inscricao.AP_Pedido = Row["Pedido"].ToString().Trim();
                        ITO_Inscricao.AP_IDdoPedido = Row["ID do Pedido"].ToString().Trim();
                        ITO_Inscricao.AP_Consultora = MI_FormatarNrConsultora(Row["Consultora"].ToString().Trim());
                        ITO_Inscricao.AP_NomedaConsultora = Row["Nome da Consultora"].ToString().Trim();
                        ITO_Inscricao.AP_NiveldeCarreira = (NivelMK != string.Empty ? NivelMK : Row["Nível de Carreira"].ToString().Trim());
                        ITO_Inscricao.AP_Unidade = Row["Unidade"].ToString().Trim();
                        ITO_Inscricao.AP_EmaildeContato = Row["E-mail de Contato"].ToString().Trim();
                        ITO_Inscricao.AP_Telefone = Row["Telefone"].ToString().Trim();
                        ITO_Inscricao.AP_Cidade = Row["Cidade"].ToString().Trim();
                        ITO_Inscricao.AP_Estado = Row["Estado"].ToString().Trim();
                        ITO_Inscricao.AP_TipodeInscricao = String.Format("{0}{1}", Row["Tipo de Inscrição"].ToString().Trim(), " - UNICO");
                        ITO_Inscricao.AP_Status = Row["Status"].ToString().Trim();
                        ITO_Inscricao.AP_ValordoPedido = Row["Valor do Pedido"].ToString().Trim();
                        ITO_Inscricao.AP_PagocomCartao = Row["Pago com Cartão"].ToString().Trim();
                        ITO_Inscricao.AP_BandeiraCC = Row["Bandeira CC"].ToString().Trim();
                        ITO_Inscricao.AP_Parcelas = Row["Parcelas"].ToString().Trim();
                        ITO_Inscricao.AP_PagocomSaldo = Row["Pago com Saldo"].ToString().Trim();
                        ITO_Inscricao.AP_PagoviaLoterica = Row["Pago via Lotérica"].ToString().Trim();
                        ITO_Inscricao.AP_DatadoPedido = Row["Data do Pedido"].ToString().Trim();
                        ITO_Inscricao.AP_NomedaNSD = (NSDMK != string.Empty ? NSDMK : Row["Nome da NSD"].ToString().Trim());
                        ITO_Inscricao.AP_Tipo = prTipo;

                        // ESTRUTURA PADRÃO INTERAÇÃO
                        ITO_Inscricao.AP_NumeroTerminal = "100";
                        ITO_Inscricao.AP_CodigoBarras = MI_GerarCodigoBarras(ITO_Inscricao.AP_NumeroTerminal);
                        ITO_Inscricao.AP_Chave = Convert.ToInt32(ITO_Inscricao.AP_CodigoBarras.Substring(3));
                        ITO_Inscricao.AP_EmissaoCracha = "N";
                        ITO_Inscricao.AP_PresenteDia1 = "N";
                        ITO_Inscricao.AP_PresenteDia2 = "N";
                        ITO_Inscricao.AP_PresenteDia3 = "N";
                        ITO_Inscricao.AP_PresenteDia4 = "N";
                        ITO_Inscricao.AP_PresenteDia5 = "N";
                        ITO_Inscricao.AP_OrigemInsercao = "IMPORTAÇÃO PLANILHA EXCEL";
                        ITO_Inscricao.AP_DataOrigemInsercao = DateTime.Now;
                        ITO_Inscricao.AP_HoraOrigemInsercao = DateTime.Now.ToLongTimeString();
                        ITO_Inscricao.AP_UsuarioOrigemInsercao = Csv_Variaveis.AP_cdUsuario.ToString();
                        ITO_Inscricao.AP_OrigemAlteracao = "IMPORTAÇÃO PLANILHA EXCEL";
                        ITO_Inscricao.AP_DataOrigemAlteracao = DateTime.Today;
                        ITO_Inscricao.AP_HoraOrigemAlteracao = DateTime.Now.ToLongTimeString();
                        ITO_Inscricao.AP_UsuarioOrigemAlteracao = "IMPORTAÇÃO PLANILHA EXCEL";
                        ITO_Inscricao.AP_MetodoInsercao = "IMPORTAÇÃO PLANILHA EXCEL";
                        ITO_Inscricao.AP_ParticipouEdicao = "MK 2019";
                        ITO_Inscricao.AP_OrigemConvite = "";

                        new DAO_Login().MS_IncluirInscricao(ITO_Inscricao);

                        itQuantidade++;
                        progress.Value = itQuantidade;
                    }
                }
            }
            catch (Exception ex)
            {
                string stNomeArquivo = "C:\\Sistemas\\MK\\Log\\Erro.txt";

                StreamWriter stWriter = new StreamWriter(stNomeArquivo);

                stWriter.WriteLine("Mensagem: " + ex.Message.ToString());
                stWriter.WriteLine("StackTrace: " + ex.StackTrace.ToString());
                stWriter.WriteLine("InnerException: ", ex.InnerException.ToString());

                stWriter.Close();
            }
            MessageBox.Show("Operação de importação realizada com sucesso!\n" + itQuantidade.ToString() + " registro(s)", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            MI_LimparTela();
        }

        private void MI_ImportarJoias()
        {
            if (MessageBox.Show("(ATENÇÃO) Esta operação ira excluir a importação de joias realizada para o roteiro (" + ddlRoteiro.Text + ").\nDeseja realmente continuar?", "ATENÇÃO!", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            // REGISTRAR LOG
            new CAT_CadastrarLogin().MS_RegistrarLog("IMPORTAÇÃO DE PLANILHA DE JOIAS (" + ddlRoteiro.Text + ") nome do excel (" + edtArquivo.Text + ")", Csv_Variaveis.AP_cdUsuario);

            DataTable dtRegistros = new Csv_LerExcel().MS_ObterExcel(edtArquivo.Text);

            try
            {
                string prVerificacao = dtRegistros.Rows[0]["Código"].ToString();
            }
            catch
            {
                MessageBox.Show("Não foi localizado a coluna ( Código )", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int cdRoteiro = 0;
            DataTable dtReconhecimento = new DAO_Login().MS_ObterRoteiroPorNome(ddlRoteiro.Text);
            if (dtReconhecimento.Rows.Count == 0)
            {
                MessageBox.Show("Não foi localizado o roteiro selecionado", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                cdRoteiro = Convert.ToInt32(dtReconhecimento.Rows[0]["cdRoteiro"]);
                if (MessageBox.Show("Você confirma a importação de joias da planilha excel para o roteiro (" + dtReconhecimento.Rows[0]["dsReconhecimento"].ToString() + ")?", "ATENÇÃO!", MessageBoxButtons.YesNo) == DialogResult.No)
                    return;

                new DAO_Login().MI_ExcluirPremioJoias(ddlRoteiro.Text);
            }

            int itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Código"].ToString().Trim() != String.Empty)
                {
                    itQuantidade++;
                    progress.Maximum = itQuantidade;
                }
            }

            itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Código"].ToString().Trim() != String.Empty)
                {
                    //INSERIR O RECONHECIMENTO
                    new CAT_CadastrarVisitante().MS_IncluirReconhecimento(MI_FormatarNrConsultora(Row["Código"].ToString()), ddlRoteiro.Text);

                    //OBTER O ID DO RECONHECIMENTO
                    DataTable dtIDReconhecimento = new CAT_CadastrarVisitante().MS_ObterUltimoReconhecimentoInserido(MI_FormatarNrConsultora(Row["Código"].ToString()), ddlRoteiro.Text.ToString().Trim());

                    //INSERIR O ITEM DO RECONHECIMENTO
                    string stPremio = MI_ObterIdPremio(Row["prêmio"].ToString().Trim());
                    new DAO_Visitante().MS_IncluirReconhecimento_Item(dtIDReconhecimento.Rows[0]["ObjRef"].ToString(), stPremio);

                    itQuantidade++;
                    progress.Value = itQuantidade;
                }
            }

            MessageBox.Show("Planilha de joias importada com sucesso!\n" + itQuantidade.ToString() + " registro(s)", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            MI_LimparTela();
        }

        private void MI_ImportarJoiasDividido()
        {
            if (MessageBox.Show("(ATENÇÃO) Esta operação ira excluir a importação de joias realizada para os roteiros envolvidos.\nDeseja realmente continuar?", "ATENÇÃO!", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            DataTable dtRegistros = new Csv_LerExcel().MS_ObterExcel(edtArquivo.Text);

            try
            {
                string prVerificacao = dtRegistros.Rows[0]["Código"].ToString();
            }
            catch
            {
                MessageBox.Show("Não foi localizado a coluna ( Código )", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // OBTER ROTEIRO DISTINTOS DA PLANILHA
            Clt_ObterRoteiros open = new Clt_ObterRoteiros();
            open.dtExcel = dtRegistros;
            open.ShowDialog();

            if (open.AP_Resultado == false)
            {
                return;
            }

            // EXCLUIR TODOS OS RECONHECIMENTOS DOS ROTEIROS ENVOLVIDOS
            foreach (DataGridViewRow Row in open.gdwRoteiros.Rows)
            {
                new DAO_Login().MI_ExcluirPremioJoias(Row.Cells[1].Value.ToString());

                // REGISTRAR LOG
                new CAT_CadastrarLogin().MS_RegistrarLog("IMPORTAÇÃO DE PLANILHA DE JOIAS (" + Row.Cells[1].Value.ToString() + ") nome do excel (" + edtArquivo.Text + ")", Csv_Variaveis.AP_cdUsuario);
            }


            int itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Código"].ToString().Trim() != String.Empty)
                {
                    itQuantidade++;
                    progress.Maximum = itQuantidade;
                }
            }

            itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Código"].ToString().Trim() != String.Empty)
                {
                    // OBTER ID DO ROTEIRO
                    string dsRoteiro = "";
                    foreach (DataGridViewRow RowRoteiro in open.gdwRoteiros.Rows)
                    {
                        if (Row["Nivel"].ToString().Trim() == RowRoteiro.Cells[0].Value.ToString())
                        {
                            dsRoteiro = RowRoteiro.Cells[1].Value.ToString();
                            break;
                        }
                    }

                    //INSERIR O RECONHECIMENTO
                    new CAT_CadastrarVisitante().MS_IncluirReconhecimento(MI_FormatarNrConsultora(Row["Código"].ToString()), dsRoteiro);

                    //OBTER O ID DO RECONHECIMENTO
                    DataTable dtIDReconhecimento = new CAT_CadastrarVisitante().MS_ObterUltimoReconhecimentoInserido(MI_FormatarNrConsultora(Row["Código"].ToString()), dsRoteiro);

                    //INSERIR O ITEM DO RECONHECIMENTO
                    string stPremio = MI_ObterIdPremio(Row["Prêmio"].ToString().Trim());
                    new DAO_Visitante().MS_IncluirReconhecimento_Item(dtIDReconhecimento.Rows[0]["ObjRef"].ToString(), stPremio);

                    itQuantidade++;
                    progress.Value = itQuantidade;
                }
            }

            MessageBox.Show("Planilha de joias importada com sucesso!\n" + itQuantidade.ToString() + " registro(s)", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            MI_LimparTela();
        }

        private string MI_ObterIdPremio(string prNome)
        {
            string stResult = "";

            if (prNome.Trim() == string.Empty)
                return stResult = "";

            if ((prNome.Trim().ToUpper() == "NÃO ESCOLHEU") || (prNome.Trim().ToUpper() == "FAZER ESCOLHA DE PRÊMIO") || (prNome.Trim().ToUpper() == "ESCOLHER"))
                return prNome = "";

            if (prNome != string.Empty)
            {
                CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
                DataTable dtPremio = IAT_Visitante.MS_ObterPremioPorNome(prNome);
                return stResult = dtPremio.Rows[0]["cdRoteiro"].ToString();
            }
            else
            {
                return stResult;
            }
        }

        private void MI_ImportarContestWinner()
        {
            if (MessageBox.Show("(ATENÇÃO) Esta operação ira excluir a importação realizada para o roteiro (" + ddlRoteiro.Text + ").\nDeseja realmente continuar?", "ATENÇÃO!", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            // REGISTRAR LOG
            new CAT_CadastrarLogin().MS_RegistrarLog("IMPORTAÇÃO DE RECONHECIMENTO (" + ddlRoteiro.Text + ") nome do excel (" + edtArquivo.Text + ")", Csv_Variaveis.AP_cdUsuario);

            DataTable dtRegistros = new Csv_LerExcel().MS_ObterExcel(edtArquivo.Text);

            try
            {
                string prVerificacao = dtRegistros.Rows[0]["Código da Consultora"].ToString();
            }
            catch
            {
                MessageBox.Show("Não foi localizado a coluna ( Código da Consultora )", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int cdRoteiro = 0;
            DataTable dtReconhecimento = new DAO_Login().MS_ObterRoteiroPorNome(ddlRoteiro.Text);
            if (dtReconhecimento.Rows.Count == 0)
            {
                MessageBox.Show("Não foi localizado o roteiro selecionado", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                cdRoteiro = Convert.ToInt32(dtReconhecimento.Rows[0]["cdRoteiro"]);
                //if (MessageBox.Show("Você confirma a importação da planilha excel para o roteiro (" + dtReconhecimento.Rows[0]["dsReconhecimento"].ToString() + ")?", "ATENÇÃO!", MessageBoxButtons.YesNo) == DialogResult.No)
                //    return;

                //new DAO_Login().MI_ExcluirReconhecimento(cdRoteiro);
            }

            int itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Código da Consultora"].ToString().Trim() != String.Empty)
                {
                    itQuantidade++;
                    progress.Maximum = itQuantidade;
                }
            }

            CTO_Reconhecimento ITO_Reconhecimento;

            itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Código da Consultora"].ToString().Trim() != String.Empty)
                {
                    if (boInterromper == true) // Interrompe a operação
                    {
                        MessageBox.Show("Processo de importação interrompido", "Atenção!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    }

                    ITO_Reconhecimento = new CTO_Reconhecimento();

                    ITO_Reconhecimento.AP_cdUsuario = Csv_Variaveis.AP_cdUsuario;
                    ITO_Reconhecimento.AP_Consultora = MI_FormatarNrConsultora(Row["Código da Consultora"].ToString().Trim());
                    ITO_Reconhecimento.AP_dsMetodoInsert = "IMPORTAÇÃO PLANILHA";
                    ITO_Reconhecimento.AP_cdRoteiro = cdRoteiro;
                    ITO_Reconhecimento.AP_cdInscricao = 0;

                    //ldadosReconhecimento.Add(ITO_Reconhecimento);

                    //if (itQuantidadeInserir == 400)
                    //{
                    //    new DAO_Login().MS_IncluirReconhecimentoEmLote(ldadosReconhecimento);
                    //    ldadosReconhecimento.Clear();
                    //    itQuantidadeInserir = 0;
                    //}
                    //else
                    //    itQuantidadeInserir++;
                    new DAO_Login().MS_IncluirReconhecimento(ITO_Reconhecimento);
                    itQuantidade++;
                    progress.Value = itQuantidade;
                }
            }

            if (boInterromper == false)
            {
                MessageBox.Show("Roteiro importado com sucesso!\n" + itQuantidade.ToString() + " registro(s)", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            MI_LimparTela();

        }

        private void MI_ImportarContestWinnerDividido()
        {
            if (MessageBox.Show("(ATENÇÃO) Esta operação ira excluir a importação realizada para os roteiros envolvidos.\nDeseja realmente continuar?", "ATENÇÃO!", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            DataTable dtRegistros = new Csv_LerExcel().MS_ObterExcel(edtArquivo.Text);

            try
            {
                string prVerificacao = dtRegistros.Rows[0]["Código da Consultora"].ToString();
            }
            catch
            {
                MessageBox.Show("Não foi localizado a coluna ( Código da Consultora )", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // OBTER ROTEIRO DISTINTOS DA PLANILHA
            Clt_ObterRoteiros open = new Clt_ObterRoteiros();
            open.dtExcel = dtRegistros;
            open.ShowDialog();

            if (open.AP_Resultado == false)
            {
                return;
            }

            // EXCLUIR TODOS OS RECONHECIMENTOS DOS ROTEIROS ENVOLVIDOS
            foreach (DataGridViewRow Row in open.gdwRoteiros.Rows)
            {
                int cdRoteiroExcluir = 0;
                DataTable dtReconhecimento = new DAO_Login().MS_ObterRoteiroPorNome(Row.Cells[1].Value.ToString());
                cdRoteiroExcluir = Convert.ToInt32(dtReconhecimento.Rows[0]["cdRoteiro"]);

                new DAO_Login().MI_ExcluirReconhecimento(cdRoteiroExcluir);

                // REGISTRAR LOG
                new CAT_CadastrarLogin().MS_RegistrarLog("IMPORTAÇÃO DE RECONHECIMENTO (" + dtReconhecimento.Rows[0]["dsReconhecimento"] + ") nome do excel (" + edtArquivo.Text + ")", Csv_Variaveis.AP_cdUsuario);
            }


            int itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Código da Consultora"].ToString().Trim() != string.Empty)
                {
                    itQuantidade++;
                    progress.Maximum = itQuantidade;
                }
                else
                {
                    string teste = "0";
                }
            }

            itQuantidade = 0;
            CTO_Reconhecimento ITO_Reconhecimento;

            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Código da Consultora"].ToString().Trim() != String.Empty)
                {
                    if (boInterromper == true) // Interrompe a operação
                    {
                        MessageBox.Show("Processo de importação interrompido", "Atenção!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    }

                    // OBTER ID DO ROTEIRO
                    int cdRoteiro = 0;
                    foreach (DataGridViewRow RowRoteiro in open.gdwRoteiros.Rows)
                    {
                        if (Row["Nivel"].ToString().Trim() == RowRoteiro.Cells[0].Value.ToString())
                        {
                            DataTable dtReconhecimento = new DAO_Login().MS_ObterRoteiroPorNome(RowRoteiro.Cells[1].Value.ToString());

                            //CUSTOMIZAÇÃO APLICADA APENAS PARA OS IDS DE SEMINÁRIOS DA MARY KAY DEVIDO AO GRANDE VOLUME DE DADOS DA PLANILHA
                            //if (RowRoteiro.Cells[1].Value.ToString().Trim().Equals("Seminário Rubi"))
                            //    cdRoteiro = 245;

                            //if (RowRoteiro.Cells[1].Value.ToString().Trim().Equals("Seminário Safira"))
                            //    cdRoteiro = 244;
                            //================================================================================================================

                            cdRoteiro = Convert.ToInt32(dtReconhecimento.Rows[0]["cdRoteiro"]);
                            break;
                        }
                    }
                    
                    ITO_Reconhecimento = new CTO_Reconhecimento();
                    ITO_Reconhecimento.AP_cdUsuario = Csv_Variaveis.AP_cdUsuario;
                    ITO_Reconhecimento.AP_Consultora = MI_FormatarNrConsultora(Row["Código da Consultora"].ToString().Trim());
                    ITO_Reconhecimento.AP_dsMetodoInsert = "IMPORTAÇÃO PLANILHA";
                    ITO_Reconhecimento.AP_cdRoteiro = cdRoteiro;
                    ITO_Reconhecimento.AP_cdInscricao = 0;

                    //ldadosReconhecimento.Add(ITO_Reconhecimento);

                    //if(itQuantidadeImportar == 400)
                    //{
                    //    new DAO_Login().MS_IncluirReconhecimentoEmLote(ldadosReconhecimento);
                    //    itQuantidadeImportar = 0;
                    //    ldadosReconhecimento.Clear();
                    //}
                    //else
                    //{
                    //    itQuantidadeImportar++;
                    //}

                    new DAO_Login().MS_IncluirReconhecimento(ITO_Reconhecimento);
                    itQuantidade++;
                    progress.Value = itQuantidade;
                }
            }

            if (boInterromper == false)
            {
                MessageBox.Show("Roteiro importado com sucesso!\n" + itQuantidade.ToString() + " registro(s)", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            MI_LimparTela();

        }

        private void MI_ImportarNivelDeCarreira()
        {
            if (MessageBox.Show("(ATENÇÃO) Esta operação ira atualizar o nível de carreira.\nDeseja realmente continuar?", "ATENÇÃO!", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            // REGISTRAR LOG
            new CAT_CadastrarLogin().MS_RegistrarLog("ATUALIZAÇÃO DE NÍVEL DE CARREIRA nome do excel (" + edtArquivo.Text + ")", Csv_Variaveis.AP_cdUsuario);

            DataTable dtRegistros = new Csv_LerExcel().MS_ObterExcel(edtArquivo.Text);

            try
            {
                string prVerificacao = dtRegistros.Rows[0]["Consultora"].ToString();
            }
            catch
            {
                MessageBox.Show("Não foi localizado a coluna ( Consultora )", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                string prVerificacao = dtRegistros.Rows[0]["Nível de Carreira"].ToString();
            }
            catch
            {
                MessageBox.Show("Não foi localizado a coluna ( Nível de Carreira )", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Consultora"].ToString().Trim() != String.Empty)
                {
                    itQuantidade++;
                    progress.Maximum = itQuantidade;
                }
            }

            itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Consultora"].ToString().Trim() != String.Empty)
                {
                    if (boInterromper == true) // Interrompe a operação
                    {
                        MessageBox.Show("Processo de importação interrompido", "Atenção!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    }

                    new DAO_Login().MS_AtualizarNivelCarreira(MI_FormatarNrConsultora(Row["Consultora"].ToString().Trim()), Row["Nível de Carreira"].ToString().Trim());
                    itQuantidade++;
                    progress.Value = itQuantidade;
                }
            }

            if (boInterromper == false)
            {
                MessageBox.Show("Planilha de atualização importada com sucesso!\n" + itQuantidade.ToString() + " registro(s)", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            MI_LimparTela();
        }

        public void MI_ImportarConvidados()
        {
            if (MessageBox.Show("(ATENÇÃO) Esta operação ira atualizar os dados dos convidados.\nDeseja realmente continuar?", "ATENÇÃO!", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            // REGISTRAR LOG
            new CAT_CadastrarLogin().MS_RegistrarLog("ATUALIZAÇÃO DE DADOS DOS CONVIDADOS nome do excel (" + edtArquivo.Text + ")", Csv_Variaveis.AP_cdUsuario);

            DataTable dtRegistros = new Csv_LerExcel().MS_ObterExcel(edtArquivo.Text);

            try
            {
                string prVerificacao = dtRegistros.Rows[0]["Código de Consultora"].ToString();
            }
            catch
            {
                MessageBox.Show("Não foi localizado a coluna ( Código de Consultora )", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                string prVerificacao = dtRegistros.Rows[0]["Nome do convidado"].ToString();
            }
            catch
            {
                MessageBox.Show("Não foi localizado a coluna ( Nome do convidado )", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Código de Consultora"].ToString().Trim() != String.Empty)
                {
                    itQuantidade++;
                    progress.Maximum = itQuantidade;
                }
            }

            itQuantidade = 0;
            foreach (DataRow Row in dtRegistros.Rows)
            {
                if (Row["Código de Consultora"].ToString().Trim() != String.Empty)
                {
                    if (boInterromper == true) // Interrompe a operação
                    {
                        MessageBox.Show("Processo de importação interrompido", "Atenção!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    }

                    new DAO_Login().MS_AtualizarDadosDoConvidado(MI_FormatarNrConsultora(Row["Código de Consultora"].ToString().Trim()), Row["Nome do convidado"].ToString().Trim().ToUpper());
                    itQuantidade++;
                    progress.Value = itQuantidade;
                }
            }

            if (boInterromper == false)
            {
                MessageBox.Show("Planilha de atualização importada com sucesso!\n" + itQuantidade.ToString() + " registro(s)", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            MI_LimparTela();
        }

        private string MI_GerarCodigoBarras(string stNTerminal)
        {
            string stCodigoBarras = stNTerminal;

            CAT_CadastrarVisitante IAT_CadastrarVisitante = new CAT_CadastrarVisitante();
            stCodigoBarras = stCodigoBarras + IAT_CadastrarVisitante.MS_GerarCodigoBarras();

            return stCodigoBarras;
        }

        private string MI_FormatarNrConsultora(string prConsultora)
        {
            while (prConsultora.Length < 6)
                prConsultora = "0" + prConsultora;

            return prConsultora;
        }

        private void ddlTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlRoteiro.Items.Clear();
            ddlRoteiro.Enabled = false;

            if ((ddlTipo.Text == "05 - CONTEST WINNER") || (ddlTipo.Text == "04 - JOIAS"))
            {
                ddlRoteiro.Items.Add("SELECIONE");
                DataTable dtRoteiro = new DAO_Login().MS_ObterRoteiro("I");
                dtRoteiro.DefaultView.Sort = "dsReconhecimento ASC";
                dtRoteiro = dtRoteiro.DefaultView.ToTable();

                foreach (DataRow Row in dtRoteiro.Rows)
                {
                    ddlRoteiro.Items.Add(Row["dsReconhecimento"].ToString());
                    ddlRoteiro.Enabled = true;
                }
                ddlRoteiro.Text = "SELECIONE";
            }
            else
            {
                ddlRoteiro.Items.Clear();
                ddlRoteiro.Enabled = false;
            }
        }

        private void btoPausar_Click(object sender, EventArgs e)
        {
            boInterromper = true;
        }
    }
}
