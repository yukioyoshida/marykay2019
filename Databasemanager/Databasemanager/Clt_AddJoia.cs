﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SPI;

namespace Databasemanager
{
    public partial class Clt_AddJoia : Form
    {
        public string AP_NomeCompleto { get; set; }
        public string AP_Consultora { get; set; }

        public Clt_AddJoia()
        {
            InitializeComponent();
        }

        private void Clt_AddJoia_Load(object sender, EventArgs e)
        {
            edtNomeCompleto.Text = AP_NomeCompleto;
            MI_CarregarRoteiro();
            MI_CarregarJoias();
        }

        private void MI_CarregarRoteiro()
        {
            DataTable dtRoteiro = new DAO_Login().MS_ObterRoteiro("I");

            ddlRoteiro.Items.Clear();
            foreach (DataRow Row in new DAO_Login().MS_ObterRoteiro("I").Rows)
            {
                ddlRoteiro.Items.Add(Row["dsReconhecimento"].ToString());
            }
        }

        private void MI_CarregarJoias()
        {
            DataTable dtRoteiro = new DAO_Login().MS_ObterRoteiro("J");

            ddlPremio.Items.Clear();
            
            ddlPremio.Items.Add("NÃO ESCOLHEU");
            foreach (DataRow Row in new DAO_Login().MS_ObterRoteiro("J").Rows)
            {
                ddlPremio.Items.Add(Row["dsReconhecimento"].ToString());
            }
        }

        private bool MI_Consistencias()
        {
            bool boResult = true;

            if (ddlRoteiro.Text.Trim() == "")
            {
                MessageBox.Show("Selecione o roteiro", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return boResult = false;
            }

            if (ddlRoteiro.Text.Trim() == "")
            {
                MessageBox.Show("Selecione o premio", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return boResult = false;
            }

            return boResult;
        }

        private string MI_FormatarNrConsultora(string prConsultora)
        {
            while (prConsultora.Length < 6)
                prConsultora = "0" + prConsultora;

            return prConsultora;
        }

        private string MI_ObterIdPremio(string prNome)
        {
            string stResult = "";

            if (prNome.Trim() == string.Empty)
                return stResult = "";

            if ((prNome.Trim().ToUpper() == "NÃO ESCOLHEU") || (prNome.Trim().ToUpper() == "FAZER ESCOLHA DE PRÊMIO") || (prNome.Trim().ToUpper() == "ESCOLHER"))
                return prNome = "";

            if (prNome != string.Empty)
            {
                CAT_CadastrarVisitante IAT_Visitante = new CAT_CadastrarVisitante();
                DataTable dtPremio = IAT_Visitante.MS_ObterPremioPorNome(prNome);
                return stResult = dtPremio.Rows[0]["cdRoteiro"].ToString();
            }
            else
            {
                return stResult;
            }
        }

        private void btoSalvar_Click(object sender, EventArgs e)
        {
            if (MI_Consistencias())
            {
                // REGISTRAR LOG
                new CAT_CadastrarLogin().MS_RegistrarLog("INSERÇÃO DE JOIA MANUAL (" + ddlRoteiro.Text + ") nome da Joia (" + ddlPremio.Text + ")", Csv_Variaveis.AP_cdUsuario);

                int cdRoteiro = 0;
                DataTable dtReconhecimento = new DAO_Login().MS_ObterRoteiroPorNome(ddlRoteiro.Text);
                if (dtReconhecimento.Rows.Count == 0)
                {
                    MessageBox.Show("Não foi localizado o roteiro selecionado", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    cdRoteiro = Convert.ToInt32(dtReconhecimento.Rows[0]["cdRoteiro"]);
                    if (MessageBox.Show("Você confirma a inclusão de premio para o roteiro (" + dtReconhecimento.Rows[0]["dsReconhecimento"].ToString() + ")?", "ATENÇÃO!", MessageBoxButtons.YesNo) == DialogResult.No)
                        return;
                }


                //INSERIR O RECONHECIMENTO
                new CAT_CadastrarVisitante().MS_IncluirReconhecimento(MI_FormatarNrConsultora(AP_Consultora), ddlRoteiro.Text);

                //OBTER O ID DO RECONHECIMENTO
                DataTable dtIDReconhecimento = new CAT_CadastrarVisitante().MS_ObterUltimoReconhecimentoInserido(MI_FormatarNrConsultora(AP_Consultora), ddlRoteiro.Text.ToString().Trim());

                //INSERIR O ITEM DO RECONHECIMENTO
                string stPremio = MI_ObterIdPremio(ddlPremio.Text.Trim());
                new DAO_Visitante().MS_IncluirReconhecimento_Item(dtIDReconhecimento.Rows[0]["ObjRef"].ToString(), stPremio);

                MessageBox.Show("Prêmio cadastrado com sucesso!", "Confirmação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }
    }
}
