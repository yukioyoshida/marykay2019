﻿namespace Databasemanager
{
    partial class ctl_ResumoConsultora
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpResumo = new System.Windows.Forms.GroupBox();
            this.lblQuantidadeRegistros = new System.Windows.Forms.Label();
            this.gdwTable = new System.Windows.Forms.DataGridView();
            this.Reconhecimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataImportacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomeCredencial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btoFiltrar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.edtChave = new System.Windows.Forms.TextBox();
            this.grpResumo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdwTable)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpResumo
            // 
            this.grpResumo.Controls.Add(this.lblQuantidadeRegistros);
            this.grpResumo.Controls.Add(this.gdwTable);
            this.grpResumo.Controls.Add(this.groupBox1);
            this.grpResumo.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpResumo.Location = new System.Drawing.Point(16, 16);
            this.grpResumo.Name = "grpResumo";
            this.grpResumo.Size = new System.Drawing.Size(834, 733);
            this.grpResumo.TabIndex = 3;
            this.grpResumo.TabStop = false;
            this.grpResumo.Text = "Consultora MK";
            // 
            // lblQuantidadeRegistros
            // 
            this.lblQuantidadeRegistros.AutoSize = true;
            this.lblQuantidadeRegistros.Font = new System.Drawing.Font("Verdana", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidadeRegistros.Location = new System.Drawing.Point(19, 676);
            this.lblQuantidadeRegistros.Name = "lblQuantidadeRegistros";
            this.lblQuantidadeRegistros.Size = new System.Drawing.Size(164, 14);
            this.lblQuantidadeRegistros.TabIndex = 6;
            this.lblQuantidadeRegistros.Text = "[lblQuantidadeRegistros]";
            // 
            // gdwTable
            // 
            this.gdwTable.AllowUserToAddRows = false;
            this.gdwTable.AllowUserToResizeRows = false;
            this.gdwTable.BackgroundColor = System.Drawing.Color.White;
            this.gdwTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gdwTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdwTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Reconhecimento,
            this.DataImportacao,
            this.Quantidade,
            this.NomeCredencial});
            this.gdwTable.Location = new System.Drawing.Point(22, 106);
            this.gdwTable.MultiSelect = false;
            this.gdwTable.Name = "gdwTable";
            this.gdwTable.RowHeadersVisible = false;
            this.gdwTable.RowTemplate.Height = 24;
            this.gdwTable.Size = new System.Drawing.Size(788, 567);
            this.gdwTable.TabIndex = 2;
            this.gdwTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdwTable_CellClick);
            this.gdwTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdwTable_CellContentClick);
            // 
            // Reconhecimento
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Reconhecimento.DefaultCellStyle = dataGridViewCellStyle1;
            this.Reconhecimento.HeaderText = "cdReconhecimento";
            this.Reconhecimento.Name = "Reconhecimento";
            this.Reconhecimento.ReadOnly = true;
            this.Reconhecimento.Visible = false;
            // 
            // DataImportacao
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataImportacao.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataImportacao.HeaderText = "Consultora";
            this.DataImportacao.Name = "DataImportacao";
            this.DataImportacao.ReadOnly = true;
            this.DataImportacao.Width = 80;
            // 
            // Quantidade
            // 
            this.Quantidade.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Quantidade.DefaultCellStyle = dataGridViewCellStyle3;
            this.Quantidade.HeaderText = "Tipo de Inscrição";
            this.Quantidade.Name = "Quantidade";
            this.Quantidade.ReadOnly = true;
            // 
            // NomeCredencial
            // 
            this.NomeCredencial.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.NomeCredencial.DefaultCellStyle = dataGridViewCellStyle4;
            this.NomeCredencial.HeaderText = "Nome completo";
            this.NomeCredencial.Name = "NomeCredencial";
            this.NomeCredencial.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btoFiltrar);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.edtChave);
            this.groupBox1.Location = new System.Drawing.Point(22, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(788, 68);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtro";
            // 
            // btoFiltrar
            // 
            this.btoFiltrar.Location = new System.Drawing.Point(696, 25);
            this.btoFiltrar.Name = "btoFiltrar";
            this.btoFiltrar.Size = new System.Drawing.Size(75, 25);
            this.btoFiltrar.TabIndex = 10;
            this.btoFiltrar.Text = "Filtrar";
            this.btoFiltrar.UseVisualStyleBackColor = true;
            this.btoFiltrar.Click += new System.EventHandler(this.btoFiltrar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "Consultora:";
            // 
            // edtChave
            // 
            this.edtChave.Location = new System.Drawing.Point(135, 25);
            this.edtChave.Name = "edtChave";
            this.edtChave.Size = new System.Drawing.Size(552, 26);
            this.edtChave.TabIndex = 8;
            // 
            // ctl_ResumoConsultora
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.grpResumo);
            this.Name = "ctl_ResumoConsultora";
            this.Size = new System.Drawing.Size(870, 768);
            this.Load += new System.EventHandler(this.ctl_ResumoConsultora_Load);
            this.grpResumo.ResumeLayout(false);
            this.grpResumo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdwTable)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpResumo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edtChave;
        private System.Windows.Forms.Button btoFiltrar;
        private System.Windows.Forms.DataGridView gdwTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reconhecimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataImportacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomeCredencial;
        private System.Windows.Forms.Label lblQuantidadeRegistros;
    }
}
