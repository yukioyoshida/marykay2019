﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SPI;

namespace Databasemanager
{
    public partial class Clt_DetalhesConsultora : Form
    {
        public string AP_Consultora { get; set; }
        public string AP_Inscricao { get; set; }
        public string AP_TipoDetalhes { get; set; }

        DataGridViewRow RowSelected;

        public Clt_DetalhesConsultora()
        {
            InitializeComponent();
        }

        private void Clt_DetalhesConsultora_Load(object sender, EventArgs e)
        {
            MI_CarregarTela();
        }

        private void MI_CarregarTela()
        {
            DataTable dtConsultora = new DAO_Login().MS_ObterInscricao(AP_Inscricao);
            DataRow row = dtConsultora.Rows[0];

            edtPedido.Text = row["Pedido"].ToString(); 
            edtIDdoPedido.Text = row["IDdoPedido"].ToString();
            edtConsultora.Text = row["Consultora"].ToString();
            edtNomedaConsultora.Text = row["NomedaConsultora"].ToString();
            edtNiveldeCarreira.Text = row["NiveldeCarreira"].ToString();
            edtUnidade.Text = row["Unidade"].ToString();
            edtEmaildeContato.Text = row["EmaildeContato"].ToString();
            edtTelefone.Text = row["Telefone"].ToString();
            edtCidade.Text = row["Cidade"].ToString();
            edtEstado.Text = row["Estado"].ToString();
            edtTipodeInscricao.Text = row["TipodeInscricao"].ToString();
            edtStatus.Text = row["Status"].ToString();
            edtValordoPedido.Text = row["ValordoPedido"].ToString();
            edtDatadoPedido.Text = row["DatadoPedido"].ToString();
            edtNomedaNSD.Text = row["NomedaNSD"].ToString();

            gdwTable.Rows.Clear();

            if (AP_TipoDetalhes == "JOIAS")
            {
                foreach (DataRow Row in new DAO_Login().MS_ObterPremiacoes(AP_Consultora).Rows)
                {
                    gdwTable.Rows.Add(
                        Row["ObjRef"].ToString(),
                        Row["dsReconhecimento"].ToString() + " | " + Row["PREMIO"].ToString()
                        );
                }
            }
            else
            {
                foreach (DataRow Row in new DAO_Login().MS_ObterReconhecimentoPorConsultora(AP_Consultora).Rows)
                {
                    string stTipoInscricao = MI_DefinirTipoInscricao(edtTipodeInscricao.Text);
                    if (stTipoInscricao == "CONVIDADOS")
                    {
                        // PERMITE SOMENTE SE FOR OS ITENS DE CONVIDADO
                        // 1 AREA DE ASSENTOS
                        if ((Row["dsReconhecimento"].ToString().ToUpper().Contains("AREA DE ASSENTO:")) || ("AREA DE ASSENTO:".Contains(Row["dsReconhecimento"].ToString().ToUpper())))
                        {
                            gdwTable.Rows.Add(
                            Row["cdReconhecimento"].ToString(),
                            Row["dsReconhecimento"].ToString()
                            );
                        }

                        // 2 DEFINIDOS PELO CLIENTE
                        if (
                                (Row["dsReconhecimento"].ToString().Trim() == "Atividade Especial Convidado") ||
                                (Row["dsReconhecimento"].ToString().Trim() == "Orientação Exclusiva ganhadoras Especial 20 anos") ||
                                (Row["dsReconhecimento"].ToString().Trim() == "Coquetel da Realeza Rubi - Convidado") ||
                                (Row["dsReconhecimento"].ToString().Trim() == "Coquetel da Realeza Safira - Convidado") ||
                                (Row["dsReconhecimento"].ToString().Trim() == "Convidado") ||
                                (Row["dsReconhecimento"].ToString().Trim() == "Almoço Exclusivo Programa Especial 20 anos") ||
                                (Row["dsReconhecimento"].ToString().Trim() == "Entregar Convite da atividade especial do convidado")
                           )
                        {
                            gdwTable.Rows.Add(
                            Row["cdReconhecimento"].ToString(),
                            Row["dsReconhecimento"].ToString()
                            );
                        }
                    }
                    else
                    {
                        if (Row["TipoRoteiro"].ToString().ToUpper() == "I")
                        {
                            if (
                                (Row["dsReconhecimento"].ToString().Trim() == "Atividade Especial Convidado") ||
                                (Row["dsReconhecimento"].ToString().Trim() == "Coquetel da Realeza Rubi - Convidado") ||
                                (Row["dsReconhecimento"].ToString().Trim() == "Coquetel da Realeza Safira - Convidado") ||
                                (Row["dsReconhecimento"].ToString().Trim() == "Convidado") ||
                                (Row["dsReconhecimento"].ToString().Trim() == "Entregar Convite da atividade especial do convidado")
                            )
                            {
                                // OS ITENS ACIMA SÃO DO CONVIDADO NÃO APARECE PARA A CONSULTORA
                            }
                            else
                            {
                                gdwTable.Rows.Add(
                                Row["cdReconhecimento"].ToString(),
                                Row["dsReconhecimento"].ToString()
                                );
                            }
                        }
                    }
                }
            }
        }

        private string MI_DefinirTipoInscricao(string prTipoInscricao)
        {
            string stResult = string.Empty;

            if (("CONVIDADOS".Contains(prTipoInscricao.ToUpper())) || (prTipoInscricao.ToUpper()).Contains("CONVIDADOS"))
            {
                stResult = "CONVIDADOS";
            }
            else
            {
                stResult = "CONSULTORAS";
            }

            return stResult;
        }

        private void edtConsultora_TextChanged(object sender, EventArgs e)
        {
        }

        private void gdwTable_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewRow Row in gdwTable.Rows)
            {
                for (int i = 0; i <= Row.Cells.Count - 1; i++)
                    Row.Cells[i].Style.BackColor = System.Drawing.Color.White;
            }

            RowSelected = gdwTable.CurrentRow;

            for (int i = 0; i <= RowSelected.Cells.Count - 1; i++)
                RowSelected.Cells[i].Style.BackColor = System.Drawing.Color.LightBlue;
        }

        private void btoDel_Click(object sender, EventArgs e)
        {
            if (AP_TipoDetalhes == "JOIAS")
            {

            }
            else
            {
                if (RowSelected != null)
                {
                    if (MessageBox.Show("Realmente deseja excluir o roteiro (" + RowSelected.Cells[1].Value.ToString() + ") deste participante:\n" + edtNomedaConsultora.Text + "?", "ATENÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        // REGISTRAR LOG
                        new CAT_CadastrarLogin().MS_RegistrarLog("EXCLUSÃO DE ROTEIRO - " + RowSelected.Cells[0].Value.ToString(), Csv_Variaveis.AP_cdUsuario);

                        // REALIZA OPERAÇÃO DE CANCELAMENTO
                        new DAO_Login().MI_ExcluirReconhecimentoPorIdReconhecimento(RowSelected.Cells[0].Value.ToString());

                        MessageBox.Show("Exclusão realizada com sucesso!", "Confirmação de operação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        MI_CarregarTela();
                    }
                }
            }
        }

        private void btoEdit_Click(object sender, EventArgs e)
        {
            if (AP_TipoDetalhes == "JOIAS")
            {

            }
            else
            {
                ctl_editReconhecimentoItem open = new ctl_editReconhecimentoItem();

                open.AP_cdReconhecimento = RowSelected.Cells[0].Value.ToString();
                open.AP_NomeCompleto = edtNomedaConsultora.Text;
                open.AP_dsReconhecimento = RowSelected.Cells[1].Value.ToString();
                open.AP_TipoRoteiro = "I";

                string stRoteiroValidos = string.Empty;
                foreach (DataRow Row in new DAO_Login().MS_ObterRoteiro("I").Rows)
                    if (!Row["dsReconhecimento"].ToString().ToUpper().Contains("DE ASSENTO"))
                        stRoteiroValidos += "," + Row["cdRoteiro"].ToString();

                open.AP_ReteirosValido = stRoteiroValidos;

                Clt_ManterReconhecimento Controle = new Clt_ManterReconhecimento();
                Controle.objControle = open;
                Controle.ShowDialog();

                MI_CarregarTela();
            }
        }

        private void btoAdd_Click(object sender, EventArgs e)
        {
            if (AP_TipoDetalhes == "JOIAS")
            {
                Clt_AddJoia open = new Clt_AddJoia();
                open.AP_Consultora = edtConsultora.Text;
                open.AP_NomeCompleto = edtNomedaConsultora.Text;
                open.ShowDialog();

                MI_CarregarTela();
            }
            else
            {
                ctl_editReconhecimentoItem open = new ctl_editReconhecimentoItem();

                open.AP_cdReconhecimento = "0";
                open.AP_NomeCompleto = edtNomedaConsultora.Text;
                open.AP_dsReconhecimento = "";
                open.AP_TipoRoteiro = "I";

                open.AP_cdInscricao = AP_Inscricao;
                open.AP_Consultora = edtConsultora.Text;

                string stRoteiroValidos = string.Empty;
                foreach (DataRow Row in new DAO_Login().MS_ObterRoteiro("I").Rows)
                    if (!Row["dsReconhecimento"].ToString().ToUpper().Contains("DE ASSENTO"))
                        stRoteiroValidos += "," + Row["cdRoteiro"].ToString();

                open.AP_ReteirosValido = stRoteiroValidos;

                Clt_ManterReconhecimento Controle = new Clt_ManterReconhecimento();
                Controle.objControle = open;
                Controle.ShowDialog();

                MI_CarregarTela();
            }
        }
    }
}
