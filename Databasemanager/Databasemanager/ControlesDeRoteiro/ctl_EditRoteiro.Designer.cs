﻿namespace Databasemanager
{
    partial class ctl_EditRoteiro
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpImportacao = new System.Windows.Forms.GroupBox();
            this.lblQuantidadeRegistros = new System.Windows.Forms.Label();
            this.btoDel = new System.Windows.Forms.PictureBox();
            this.btoEdit = new System.Windows.Forms.PictureBox();
            this.btoAdd = new System.Windows.Forms.PictureBox();
            this.gdwTable = new System.Windows.Forms.DataGridView();
            this.Reconhecimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataImportacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomeCredencial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpImportacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btoDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gdwTable)).BeginInit();
            this.SuspendLayout();
            // 
            // grpImportacao
            // 
            this.grpImportacao.Controls.Add(this.lblQuantidadeRegistros);
            this.grpImportacao.Controls.Add(this.btoDel);
            this.grpImportacao.Controls.Add(this.btoEdit);
            this.grpImportacao.Controls.Add(this.btoAdd);
            this.grpImportacao.Controls.Add(this.gdwTable);
            this.grpImportacao.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpImportacao.Location = new System.Drawing.Point(16, 16);
            this.grpImportacao.Name = "grpImportacao";
            this.grpImportacao.Size = new System.Drawing.Size(834, 733);
            this.grpImportacao.TabIndex = 3;
            this.grpImportacao.TabStop = false;
            this.grpImportacao.Text = "Roteiro";
            // 
            // lblQuantidadeRegistros
            // 
            this.lblQuantidadeRegistros.AutoSize = true;
            this.lblQuantidadeRegistros.Font = new System.Drawing.Font("Verdana", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidadeRegistros.Location = new System.Drawing.Point(6, 688);
            this.lblQuantidadeRegistros.Name = "lblQuantidadeRegistros";
            this.lblQuantidadeRegistros.Size = new System.Drawing.Size(164, 14);
            this.lblQuantidadeRegistros.TabIndex = 5;
            this.lblQuantidadeRegistros.Text = "[lblQuantidadeRegistros]";
            // 
            // btoDel
            // 
            this.btoDel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoDel.Image = global::Databasemanager.Properties.Resources.del;
            this.btoDel.Location = new System.Drawing.Point(793, 691);
            this.btoDel.Name = "btoDel";
            this.btoDel.Size = new System.Drawing.Size(35, 35);
            this.btoDel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoDel.TabIndex = 4;
            this.btoDel.TabStop = false;
            this.btoDel.Click += new System.EventHandler(this.btoDel_Click);
            // 
            // btoEdit
            // 
            this.btoEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoEdit.Image = global::Databasemanager.Properties.Resources.editblack;
            this.btoEdit.Location = new System.Drawing.Point(752, 691);
            this.btoEdit.Name = "btoEdit";
            this.btoEdit.Size = new System.Drawing.Size(35, 35);
            this.btoEdit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoEdit.TabIndex = 3;
            this.btoEdit.TabStop = false;
            this.btoEdit.Click += new System.EventHandler(this.btoEdit_Click);
            // 
            // btoAdd
            // 
            this.btoAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoAdd.Image = global::Databasemanager.Properties.Resources.add;
            this.btoAdd.Location = new System.Drawing.Point(711, 691);
            this.btoAdd.Name = "btoAdd";
            this.btoAdd.Size = new System.Drawing.Size(35, 35);
            this.btoAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoAdd.TabIndex = 2;
            this.btoAdd.TabStop = false;
            this.btoAdd.Click += new System.EventHandler(this.btoAdd_Click);
            // 
            // gdwTable
            // 
            this.gdwTable.AllowUserToAddRows = false;
            this.gdwTable.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.gdwTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gdwTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdwTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Reconhecimento,
            this.DataImportacao,
            this.Quantidade,
            this.NomeCredencial});
            this.gdwTable.Location = new System.Drawing.Point(6, 25);
            this.gdwTable.MultiSelect = false;
            this.gdwTable.Name = "gdwTable";
            this.gdwTable.RowHeadersVisible = false;
            this.gdwTable.RowTemplate.Height = 24;
            this.gdwTable.Size = new System.Drawing.Size(822, 660);
            this.gdwTable.TabIndex = 1;
            this.gdwTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdwTable_CellClick);
            // 
            // Reconhecimento
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Reconhecimento.DefaultCellStyle = dataGridViewCellStyle1;
            this.Reconhecimento.HeaderText = "cdRoteiro";
            this.Reconhecimento.Name = "Reconhecimento";
            this.Reconhecimento.ReadOnly = true;
            this.Reconhecimento.Visible = false;
            // 
            // DataImportacao
            // 
            this.DataImportacao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataImportacao.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataImportacao.HeaderText = "Reconhecimento";
            this.DataImportacao.Name = "DataImportacao";
            this.DataImportacao.ReadOnly = true;
            // 
            // Quantidade
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Quantidade.DefaultCellStyle = dataGridViewCellStyle3;
            this.Quantidade.HeaderText = "Local";
            this.Quantidade.Name = "Quantidade";
            this.Quantidade.ReadOnly = true;
            this.Quantidade.Width = 150;
            // 
            // NomeCredencial
            // 
            this.NomeCredencial.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.NomeCredencial.DefaultCellStyle = dataGridViewCellStyle4;
            this.NomeCredencial.HeaderText = "Nome na credencial";
            this.NomeCredencial.Name = "NomeCredencial";
            this.NomeCredencial.ReadOnly = true;
            // 
            // ctl_EditRoteiro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.grpImportacao);
            this.Name = "ctl_EditRoteiro";
            this.Size = new System.Drawing.Size(870, 768);
            this.Load += new System.EventHandler(this.ctl_EditRoteiro_Load);
            this.grpImportacao.ResumeLayout(false);
            this.grpImportacao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btoDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gdwTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpImportacao;
        private System.Windows.Forms.DataGridView gdwTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reconhecimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataImportacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomeCredencial;
        private System.Windows.Forms.PictureBox btoAdd;
        private System.Windows.Forms.PictureBox btoEdit;
        private System.Windows.Forms.PictureBox btoDel;
        private System.Windows.Forms.Label lblQuantidadeRegistros;
    }
}
