﻿namespace Databasemanager
{
    partial class ctl_editRoteiroItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpImportacao = new System.Windows.Forms.GroupBox();
            this.btoSalvar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ddlLocal = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.edtNomeCredencial = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.edtNomeRoteiro = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.picFoto = new System.Windows.Forms.PictureBox();
            this.btoVoltar = new System.Windows.Forms.PictureBox();
            this.grpImportacao.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoVoltar)).BeginInit();
            this.SuspendLayout();
            // 
            // grpImportacao
            // 
            this.grpImportacao.Controls.Add(this.btoSalvar);
            this.grpImportacao.Controls.Add(this.groupBox2);
            this.grpImportacao.Controls.Add(this.groupBox1);
            this.grpImportacao.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpImportacao.Location = new System.Drawing.Point(16, 16);
            this.grpImportacao.Name = "grpImportacao";
            this.grpImportacao.Size = new System.Drawing.Size(834, 333);
            this.grpImportacao.TabIndex = 4;
            this.grpImportacao.TabStop = false;
            this.grpImportacao.Text = "Edição de roteiro";
            // 
            // btoSalvar
            // 
            this.btoSalvar.Location = new System.Drawing.Point(16, 278);
            this.btoSalvar.Name = "btoSalvar";
            this.btoSalvar.Size = new System.Drawing.Size(132, 42);
            this.btoSalvar.TabIndex = 0;
            this.btoSalvar.Text = "Salvar";
            this.btoSalvar.UseVisualStyleBackColor = true;
            this.btoSalvar.Click += new System.EventHandler(this.btoSalvar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ddlLocal);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.edtNomeCredencial);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.edtNomeRoteiro);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(16, 18);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(532, 250);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // ddlLocal
            // 
            this.ddlLocal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlLocal.FormattingEnabled = true;
            this.ddlLocal.Items.AddRange(new object[] {
            "Credenciamento",
            "ENTREGA",
            "ENTREGA DE JOIAS",
            "INFO",
            "Palco",
            "Platéia",
            "Sala"});
            this.ddlLocal.Location = new System.Drawing.Point(15, 163);
            this.ddlLocal.Name = "ddlLocal";
            this.ddlLocal.Size = new System.Drawing.Size(493, 26);
            this.ddlLocal.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Local:";
            // 
            // edtNomeCredencial
            // 
            this.edtNomeCredencial.Location = new System.Drawing.Point(15, 103);
            this.edtNomeCredencial.Name = "edtNomeCredencial";
            this.edtNomeCredencial.Size = new System.Drawing.Size(493, 26);
            this.edtNomeCredencial.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(161, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nome na credencial:";
            // 
            // edtNomeRoteiro
            // 
            this.edtNomeRoteiro.Location = new System.Drawing.Point(15, 44);
            this.edtNomeRoteiro.Name = "edtNomeRoteiro";
            this.edtNomeRoteiro.Size = new System.Drawing.Size(493, 26);
            this.edtNomeRoteiro.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome do roteiro:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.picFoto);
            this.groupBox1.Location = new System.Drawing.Point(567, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(250, 250);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Foto";
            // 
            // picFoto
            // 
            this.picFoto.BackColor = System.Drawing.Color.AliceBlue;
            this.picFoto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picFoto.Image = global::Databasemanager.Properties.Resources.no_photo1;
            this.picFoto.InitialImage = global::Databasemanager.Properties.Resources.no_photo;
            this.picFoto.Location = new System.Drawing.Point(8, 20);
            this.picFoto.Name = "picFoto";
            this.picFoto.Size = new System.Drawing.Size(234, 222);
            this.picFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFoto.TabIndex = 0;
            this.picFoto.TabStop = false;
            this.picFoto.Click += new System.EventHandler(this.picFoto_Click);
            // 
            // btoVoltar
            // 
            this.btoVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btoVoltar.Image = global::Databasemanager.Properties.Resources.voltarblack;
            this.btoVoltar.Location = new System.Drawing.Point(800, 355);
            this.btoVoltar.Name = "btoVoltar";
            this.btoVoltar.Size = new System.Drawing.Size(50, 50);
            this.btoVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btoVoltar.TabIndex = 5;
            this.btoVoltar.TabStop = false;
            this.btoVoltar.Click += new System.EventHandler(this.btoVoltar_Click);
            // 
            // ctl_editRoteiroItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.btoVoltar);
            this.Controls.Add(this.grpImportacao);
            this.Name = "ctl_editRoteiroItem";
            this.Size = new System.Drawing.Size(870, 768);
            this.Load += new System.EventHandler(this.ctl_editRoteiroItem_Load);
            this.grpImportacao.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picFoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btoVoltar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpImportacao;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox picFoto;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox ddlLocal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edtNomeCredencial;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edtNomeRoteiro;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btoSalvar;
        private System.Windows.Forms.PictureBox btoVoltar;
    }
}
