﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SPI;
using System.IO;

namespace Databasemanager
{
    public partial class ctl_editRoteiroItem : UserControl
    {
        int AP_cdRoteiro { get; set; }
        string AP_TipoRoteiro { get; set; }

        public ctl_editRoteiroItem(int prCdRoteiro, string prTipoRoteiro)
        {
            InitializeComponent();
            AP_cdRoteiro = prCdRoteiro;
            AP_TipoRoteiro = prTipoRoteiro;
        }

        private void MI_Voltar()
        {
            this.Controls.Clear();
            this.Controls.Add(new ctl_EditRoteiro(AP_TipoRoteiro));
        }

        private void btoVoltar_Click(object sender, EventArgs e)
        {
            MI_Voltar();
        }

        private void ctl_editRoteiroItem_Load(object sender, EventArgs e)
        {
            MI_CarregarRoteiro();
        }

        private void MI_CarregarRoteiro()
        {
            if (AP_cdRoteiro > 0)
            {
                DataTable dtRoteiro = new DAO_Login().MS_ObterRoteiroPorId(AP_cdRoteiro);
                edtNomeRoteiro.Text = dtRoteiro.Rows[0]["dsReconhecimento"].ToString();
                edtNomeCredencial.Text = dtRoteiro.Rows[0]["dsNomeCredencial"].ToString();
                ddlLocal.Text = dtRoteiro.Rows[0]["dsLocal"].ToString();

                MI_ObterFoto(AP_cdRoteiro);
            }

            if (AP_TipoRoteiro == "J")
            {
                edtNomeCredencial.Enabled = false;
                ddlLocal.Enabled = false;
            }

        }

        private void btoSalvar_Click(object sender, EventArgs e)
        {
            MI_Salvar();
        }

        private bool MI_Consistencias()
        {
            bool boResult = true;

            DataTable dtRoteiro = new DAO_Login().MS_ObterRoteiroPorNome(edtNomeRoteiro.Text.Trim());
            if (dtRoteiro.Rows.Count > 0)
            {
                if (AP_cdRoteiro == 0)
                {
                    MessageBox.Show("Este roteiro já esta cadastrado", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return boResult = false;
                }
                else
                {
                    if (AP_cdRoteiro != Convert.ToInt32(dtRoteiro.Rows[0]["cdRoteiro"]))
                    {
                        MessageBox.Show("Este roteiro já esta cadastrado", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return boResult = false;
                    }
                }
            }

            if (edtNomeRoteiro.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Informe o nome do roteiro", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return boResult = false;
            }

            if (AP_TipoRoteiro == "I")
            {
                if (edtNomeCredencial.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("Informe o nome do roteiro na credencial", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return boResult = false;
                }

                if (ddlLocal.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("Informe o local do roteiro", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return boResult = false;
                }
            }

            return boResult;
        }

        private void MI_Salvar()
        {
            if (MI_Consistencias())
            {
                if (AP_cdRoteiro > 0) // ALTERAÇÃO DE ROTEIRO
                {
                    if (MessageBox.Show("Realmente deseja alterar este roteiro?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        // REGISTRAR LOG
                        new CAT_CadastrarLogin().MS_RegistrarLog("ALTERAÇÃO DE ROTEIRO - " + AP_cdRoteiro.ToString(), Csv_Variaveis.AP_cdUsuario);

                        CTO_Roteiro ITO_Roteiro = new CTO_Roteiro();
                        ITO_Roteiro.AP_cdRoteiro = AP_cdRoteiro;
                        ITO_Roteiro.AP_Reconhecimento = edtNomeRoteiro.Text.Trim();
                        ITO_Roteiro.AP_NomeCredencial = edtNomeCredencial.Text.Trim();
                        ITO_Roteiro.AP_Local = ddlLocal.Text;

                        new DAO_Login().MS_AlterarRoteito(ITO_Roteiro);
                        MI_SalvarFoto(ITO_Roteiro.AP_cdRoteiro);

                        MessageBox.Show("Alteração realizada com sucesso.", "Confirmação de operação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        MI_Voltar();
                    }
                }
                else if (AP_cdRoteiro == 0) // INCLUSÃO DE NOVO ROTEIRO
                {
                    // REGISTRAR LOG
                    new CAT_CadastrarLogin().MS_RegistrarLog("INCLUSÃO DE ROTEIRO - " + edtNomeRoteiro.Text.Trim(), Csv_Variaveis.AP_cdUsuario);

                    CTO_Roteiro ITO_Roteiro = new CTO_Roteiro();
                    ITO_Roteiro.AP_Reconhecimento = edtNomeRoteiro.Text.Trim();
                    ITO_Roteiro.AP_NomeCredencial = edtNomeCredencial.Text.Trim();
                    ITO_Roteiro.AP_Local = ddlLocal.Text;
                    ITO_Roteiro.AP_TipoRoteiro = AP_TipoRoteiro;

                    new DAO_Login().MS_IncluirRoteito(ITO_Roteiro);
                    DataTable dtRoteiro = new DAO_Login().MS_ObterRoteiroPorNome(edtNomeRoteiro.Text.Trim());
                    MI_SalvarFoto(Convert.ToInt32(dtRoteiro.Rows[0]["cdRoteiro"]));

                    MessageBox.Show("Inclusão realizada com sucesso.", "Confirmação de operação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    MI_Voltar();
                }
            }
        }

        private void MI_ObterFoto(int prcdRoteiro)
        {
            try
            {
                var ms = new MemoryStream(new DAO_Login().MS_ObterFotoRoteiro(AP_cdRoteiro));
                PictureBox picFotoData = new PictureBox();
                picFotoData.Image = Image.FromStream(ms);

                picFoto.Image = picFotoData.Image;
            }
            catch
            {
                picFoto.Image = Properties.Resources.no_photo;
            }
        }

        private void MI_SalvarFoto(int prcdRoteiro)
        {
            Image bmap = picFoto.Image;
            MemoryStream ms = new MemoryStream();
            bmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            byte[] imageBytes = new byte[] { 1 };
            imageBytes = ms.ToArray();

            new DAO_Login().MS_IncluirFotoRoteiro(prcdRoteiro, imageBytes);
        }

        private void picFoto_Click(object sender, EventArgs e)
        {
            Clt_selectArquivo open = new Clt_selectArquivo();
            open.ShowDialog();

            if (open.AP_ArquivoSelecionado != string.Empty)
            {
                picFoto.ImageLocation = open.AP_ArquivoSelecionado;
            }
        }
    }
}
