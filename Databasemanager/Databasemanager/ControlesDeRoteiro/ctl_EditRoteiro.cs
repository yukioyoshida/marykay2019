﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SPI;

namespace Databasemanager
{
    public partial class ctl_EditRoteiro : UserControl
    {
        DataGridViewRow RowSelected;
        private string AP_TipoRoteiro { get; set; }

        public ctl_EditRoteiro(string prTipo)
        {
            InitializeComponent();
            AP_TipoRoteiro = prTipo;
        }

        private void ctl_EditRoteiro_Load(object sender, EventArgs e)
        {
            MI_CarregarRoteiro();
        }

        private void MI_CarregarRoteiro()
        {
            gdwTable.Rows.Clear();
            foreach(DataRow Row in new DAO_Login().MS_ObterRoteiro(AP_TipoRoteiro).Rows)
            {
                gdwTable.Rows.Add(
                    Row["cdRoteiro"].ToString(),
                    Row["dsReconhecimento"].ToString(),
                    Row["dsLocal"].ToString(),
                    Row["dsNomeCredencial"].ToString()
                    );
            }

            foreach (DataGridViewRow Row in gdwTable.Rows)
                Row.Height = 30;

            lblQuantidadeRegistros.Text = gdwTable.Rows.Count.ToString() + " registros";
        }

        private void gdwTable_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewRow Row in gdwTable.Rows)
            {
                for (int i = 0; i <= Row.Cells.Count - 1; i++)
                    Row.Cells[i].Style.BackColor = System.Drawing.Color.White;
            }

            RowSelected = gdwTable.CurrentRow;

            for (int i = 0; i <= RowSelected.Cells.Count - 1; i++)
                RowSelected.Cells[i].Style.BackColor = System.Drawing.Color.LightBlue;
        }

        private void btoDel_Click(object sender, EventArgs e)
        {
            if (RowSelected != null)
            {
                if (MessageBox.Show("Realmente deseja excluir o roteiro:\n"+ RowSelected.Cells[1].Value.ToString() + "?", "ATENÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (MessageBox.Show("Todas as importações/implementações para o roteiro:\n" + RowSelected.Cells[1].Value.ToString() + " serão excluídas.\n\nREALMENTE DESEJA CONTINUAR?", "ATENÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        // REGISTRAR LOG
                        new CAT_CadastrarLogin().MS_RegistrarLog("EXCLUSÃO DE ROTEIRO - "+ RowSelected.Cells[1].Value.ToString(), Csv_Variaveis.AP_cdUsuario);

                        // REALIZA OPERAÇÃO DE CANCELAMENTO
                        new DAO_Login().MI_ExcluirReconhecimento(Convert.ToInt32(RowSelected.Cells[0].Value.ToString()));
                        new DAO_Login().MI_ExcluirRoteiroPorId(Convert.ToInt32(RowSelected.Cells[0].Value.ToString()));

                        MessageBox.Show("Exclusão realizada com sucesso!", "Confirmação de operação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        MI_CarregarRoteiro();
                    }
                }
            }
        }

        private void btoEdit_Click(object sender, EventArgs e)
        {
            if (RowSelected != null)
            {
                this.Controls.Clear();
                this.Controls.Add(new ctl_editRoteiroItem(Convert.ToInt32(RowSelected.Cells[0].Value.ToString()), AP_TipoRoteiro));
            }
        }

        private void btoAdd_Click(object sender, EventArgs e)
        {
            this.Controls.Clear();
            this.Controls.Add(new ctl_editRoteiroItem(0, AP_TipoRoteiro));
        }
    }
}
