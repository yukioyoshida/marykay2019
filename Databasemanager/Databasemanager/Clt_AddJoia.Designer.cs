﻿namespace Databasemanager
{
    partial class Clt_AddJoia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpImportacao = new System.Windows.Forms.GroupBox();
            this.btoSalvar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ddlRoteiro = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.edtNomeCompleto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ddlPremio = new System.Windows.Forms.ComboBox();
            this.grpImportacao.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpImportacao
            // 
            this.grpImportacao.Controls.Add(this.btoSalvar);
            this.grpImportacao.Controls.Add(this.groupBox2);
            this.grpImportacao.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpImportacao.Location = new System.Drawing.Point(12, 12);
            this.grpImportacao.Name = "grpImportacao";
            this.grpImportacao.Size = new System.Drawing.Size(718, 333);
            this.grpImportacao.TabIndex = 6;
            this.grpImportacao.TabStop = false;
            this.grpImportacao.Text = "Alteração de reconhecimento";
            // 
            // btoSalvar
            // 
            this.btoSalvar.Location = new System.Drawing.Point(16, 278);
            this.btoSalvar.Name = "btoSalvar";
            this.btoSalvar.Size = new System.Drawing.Size(132, 42);
            this.btoSalvar.TabIndex = 0;
            this.btoSalvar.Text = "Salvar";
            this.btoSalvar.UseVisualStyleBackColor = true;
            this.btoSalvar.Click += new System.EventHandler(this.btoSalvar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ddlPremio);
            this.groupBox2.Controls.Add(this.ddlRoteiro);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.edtNomeCompleto);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(16, 18);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(682, 250);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // ddlRoteiro
            // 
            this.ddlRoteiro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlRoteiro.FormattingEnabled = true;
            this.ddlRoteiro.Items.AddRange(new object[] {
            "Credenciamento",
            "Palco",
            "Platéia",
            "Sala"});
            this.ddlRoteiro.Location = new System.Drawing.Point(15, 103);
            this.ddlRoteiro.Name = "ddlRoteiro";
            this.ddlRoteiro.Size = new System.Drawing.Size(650, 26);
            this.ddlRoteiro.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Selecione o prêmio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Selecione o Roteiro";
            // 
            // edtNomeCompleto
            // 
            this.edtNomeCompleto.Location = new System.Drawing.Point(15, 44);
            this.edtNomeCompleto.Name = "edtNomeCompleto";
            this.edtNomeCompleto.ReadOnly = true;
            this.edtNomeCompleto.Size = new System.Drawing.Size(650, 26);
            this.edtNomeCompleto.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome completo:";
            // 
            // ddlPremio
            // 
            this.ddlPremio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPremio.FormattingEnabled = true;
            this.ddlPremio.Items.AddRange(new object[] {
            "Credenciamento",
            "Palco",
            "Platéia",
            "Sala"});
            this.ddlPremio.Location = new System.Drawing.Point(15, 163);
            this.ddlPremio.Name = "ddlPremio";
            this.ddlPremio.Size = new System.Drawing.Size(650, 26);
            this.ddlPremio.TabIndex = 6;
            // 
            // Clt_AddJoia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(754, 383);
            this.Controls.Add(this.grpImportacao);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Clt_AddJoia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adicionar Premiação";
            this.Load += new System.EventHandler(this.Clt_AddJoia_Load);
            this.grpImportacao.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpImportacao;
        private System.Windows.Forms.Button btoSalvar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox ddlRoteiro;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edtNomeCompleto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ddlPremio;
    }
}