﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SPI;

namespace Databasemanager
{
    public partial class ctl_cheklist : UserControl
    {
        public ctl_cheklist()
        {
            InitializeComponent();
        }

        private void ctl_cheklist_Load(object sender, EventArgs e)
        {
            MI_CarregarTela();
        }

        private void MI_CarregarTela()
        {
            
            // QUANTIDADES DE CONTROLE
            DataTable dtDadosControle = new DAO_Login().MS_ObterQtdImportacaoControle();

            lblBancoMaryKay.Text = dtDadosControle.Rows[0]["BdGeral"].ToString();
            lblInscricao.Text = dtDadosControle.Rows[0]["Inscricao"].ToString();
            lblRoteiro.Text = dtDadosControle.Rows[0]["Roteiro"].ToString();
            lblRoteiroJoias.Text = dtDadosControle.Rows[0]["RoteiroJoias"].ToString();

            // QUANTIDADE DE RECONHECIMENTO
            gdwReconhecimento.Rows.Clear();
            foreach (DataRow Row in new DAO_Login().MS_ObterQtdImportacaoReconhecimento((rbtInscricao.Checked ? "I" : "J")).Rows)
            {
                gdwReconhecimento.Rows.Add(
                    Row["dsReconhecimento"].ToString(),
                    ((rbtInscricao.Checked ? Convert.ToDateTime(Row["dtImportacao"].ToString()).ToString() : Row["dtImportacao"].ToString())),
                    Row["Quantidade"].ToString()
                    );
            }
            if (rbtInscricao.Checked)
            {
                // OBTER RECONHECIMENTO NÃO IMPORTADO
                foreach (DataRow Row in new DAO_Login().MS_ObterReconhecimentoNaoImportado((rbtInscricao.Checked ? "I" : "J")).Rows)
                {
                    gdwReconhecimento.Rows.Add(
                        Row["dsReconhecimento"].ToString(),
                        Row["dtImportacao"].ToString(),
                        Row["Quantidade"].ToString()
                        );
                }
            }

            // AJUSTAR ALTURA DA LINHA
            foreach (DataGridViewRow Row in gdwReconhecimento.Rows)
                Row.Height = 35;
        }

        private void btoEditRoteiro_Click(object sender, EventArgs e)
        {
            this.Controls.Clear();
            this.Controls.Add(new ctl_EditRoteiro("I"));
        }

        private void btoEditRoteiroJoia_Click(object sender, EventArgs e)
        {
            this.Controls.Clear();
            this.Controls.Add(new ctl_EditRoteiro("J"));
        }

        private void rbtInscricao_CheckedChanged(object sender, EventArgs e)
        {
            MI_CarregarTela();
        }

        private void rbtJoias_CheckedChanged(object sender, EventArgs e)
        {
            MI_CarregarTela();
        }


    }
}
