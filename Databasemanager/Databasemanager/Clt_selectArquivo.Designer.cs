﻿namespace Databasemanager
{
    partial class Clt_selectArquivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btoDiretorio = new System.Windows.Forms.Button();
            this.edtArquivo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btoSelectArquivo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btoDiretorio
            // 
            this.btoDiretorio.Location = new System.Drawing.Point(524, 33);
            this.btoDiretorio.Name = "btoDiretorio";
            this.btoDiretorio.Size = new System.Drawing.Size(33, 26);
            this.btoDiretorio.TabIndex = 4;
            this.btoDiretorio.Text = "...";
            this.btoDiretorio.UseVisualStyleBackColor = true;
            this.btoDiretorio.Click += new System.EventHandler(this.btoDiretorio_Click);
            // 
            // edtArquivo
            // 
            this.edtArquivo.Location = new System.Drawing.Point(12, 35);
            this.edtArquivo.Name = "edtArquivo";
            this.edtArquivo.ReadOnly = true;
            this.edtArquivo.Size = new System.Drawing.Size(506, 22);
            this.edtArquivo.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Selecione o arquivo";
            // 
            // btoSelectArquivo
            // 
            this.btoSelectArquivo.Location = new System.Drawing.Point(12, 68);
            this.btoSelectArquivo.Name = "btoSelectArquivo";
            this.btoSelectArquivo.Size = new System.Drawing.Size(145, 47);
            this.btoSelectArquivo.TabIndex = 6;
            this.btoSelectArquivo.Text = "Utilizar arquivo";
            this.btoSelectArquivo.UseVisualStyleBackColor = true;
            this.btoSelectArquivo.Click += new System.EventHandler(this.btoSelectArquivo_Click);
            // 
            // Clt_selectArquivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 131);
            this.Controls.Add(this.btoSelectArquivo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btoDiretorio);
            this.Controls.Add(this.edtArquivo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Clt_selectArquivo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Selecionar arquivo";
            this.Load += new System.EventHandler(this.Clt_selectArquivo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btoDiretorio;
        private System.Windows.Forms.TextBox edtArquivo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btoSelectArquivo;
    }
}