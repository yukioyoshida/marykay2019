﻿namespace Databasemanager
{
    partial class ctl_Planilhao
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpImportacao = new System.Windows.Forms.GroupBox();
            this.lblSeminario = new System.Windows.Forms.Label();
            this.gdwPlanilhao = new System.Windows.Forms.DataGridView();
            this.progress = new System.Windows.Forms.ProgressBar();
            this.btoExcel = new System.Windows.Forms.Button();
            this.lblQtdRegistros = new System.Windows.Forms.Label();
            this.btoValidarPlanilhao = new System.Windows.Forms.Button();
            this.grpImportacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdwPlanilhao)).BeginInit();
            this.SuspendLayout();
            // 
            // grpImportacao
            // 
            this.grpImportacao.Controls.Add(this.lblSeminario);
            this.grpImportacao.Controls.Add(this.gdwPlanilhao);
            this.grpImportacao.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpImportacao.Location = new System.Drawing.Point(16, 16);
            this.grpImportacao.Name = "grpImportacao";
            this.grpImportacao.Size = new System.Drawing.Size(992, 733);
            this.grpImportacao.TabIndex = 3;
            this.grpImportacao.TabStop = false;
            this.grpImportacao.Text = "Planilhão";
            this.grpImportacao.Visible = false;
            // 
            // lblSeminario
            // 
            this.lblSeminario.AutoSize = true;
            this.lblSeminario.Location = new System.Drawing.Point(6, 33);
            this.lblSeminario.Name = "lblSeminario";
            this.lblSeminario.Size = new System.Drawing.Size(113, 18);
            this.lblSeminario.TabIndex = 1;
            this.lblSeminario.Text = "SEMINÁRIO: ?";
            this.lblSeminario.Visible = false;
            // 
            // gdwPlanilhao
            // 
            this.gdwPlanilhao.AllowUserToAddRows = false;
            this.gdwPlanilhao.AllowUserToResizeColumns = false;
            this.gdwPlanilhao.AllowUserToResizeRows = false;
            this.gdwPlanilhao.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.gdwPlanilhao.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gdwPlanilhao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdwPlanilhao.Location = new System.Drawing.Point(6, 66);
            this.gdwPlanilhao.MultiSelect = false;
            this.gdwPlanilhao.Name = "gdwPlanilhao";
            this.gdwPlanilhao.RowHeadersVisible = false;
            this.gdwPlanilhao.RowTemplate.Height = 24;
            this.gdwPlanilhao.Size = new System.Drawing.Size(980, 661);
            this.gdwPlanilhao.TabIndex = 0;
            // 
            // progress
            // 
            this.progress.Location = new System.Drawing.Point(463, 1);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(545, 23);
            this.progress.TabIndex = 5;
            this.progress.Visible = false;
            // 
            // btoExcel
            // 
            this.btoExcel.Location = new System.Drawing.Point(831, 755);
            this.btoExcel.Name = "btoExcel";
            this.btoExcel.Size = new System.Drawing.Size(171, 44);
            this.btoExcel.TabIndex = 6;
            this.btoExcel.Text = "Exportar para excel";
            this.btoExcel.UseVisualStyleBackColor = true;
            this.btoExcel.Visible = false;
            this.btoExcel.Click += new System.EventHandler(this.btoExcel_Click);
            // 
            // lblQtdRegistros
            // 
            this.lblQtdRegistros.AutoSize = true;
            this.lblQtdRegistros.Location = new System.Drawing.Point(22, 755);
            this.lblQtdRegistros.Name = "lblQtdRegistros";
            this.lblQtdRegistros.Size = new System.Drawing.Size(75, 17);
            this.lblQtdRegistros.TabIndex = 7;
            this.lblQtdRegistros.Text = "0 registros";
            this.lblQtdRegistros.Visible = false;
            // 
            // btoValidarPlanilhao
            // 
            this.btoValidarPlanilhao.Location = new System.Drawing.Point(654, 755);
            this.btoValidarPlanilhao.Name = "btoValidarPlanilhao";
            this.btoValidarPlanilhao.Size = new System.Drawing.Size(171, 44);
            this.btoValidarPlanilhao.TabIndex = 8;
            this.btoValidarPlanilhao.Text = "Homologar planilhão";
            this.btoValidarPlanilhao.UseVisualStyleBackColor = true;
            this.btoValidarPlanilhao.Visible = false;
            this.btoValidarPlanilhao.Click += new System.EventHandler(this.btoValidarPlanilhao_Click);
            // 
            // ctl_Planilhao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.btoValidarPlanilhao);
            this.Controls.Add(this.lblQtdRegistros);
            this.Controls.Add(this.btoExcel);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.grpImportacao);
            this.Name = "ctl_Planilhao";
            this.Size = new System.Drawing.Size(1024, 821);
            this.Load += new System.EventHandler(this.ctl_Planilhao_Load);
            this.grpImportacao.ResumeLayout(false);
            this.grpImportacao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdwPlanilhao)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpImportacao;
        private System.Windows.Forms.DataGridView gdwPlanilhao;
        private System.Windows.Forms.ProgressBar progress;
        private System.Windows.Forms.Button btoExcel;
        private System.Windows.Forms.Label lblSeminario;
        private System.Windows.Forms.Label lblQtdRegistros;
        private System.Windows.Forms.Button btoValidarPlanilhao;
    }
}
