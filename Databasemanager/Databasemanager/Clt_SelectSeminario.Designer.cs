﻿namespace Databasemanager
{
    partial class Clt_SelectSeminario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ddlSeminario = new System.Windows.Forms.ComboBox();
            this.btoOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Selecione o seminário:";
            // 
            // ddlSeminario
            // 
            this.ddlSeminario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSeminario.FormattingEnabled = true;
            this.ddlSeminario.Items.AddRange(new object[] {
            "SELECIONE",
            "UNICO"});
            this.ddlSeminario.Location = new System.Drawing.Point(15, 39);
            this.ddlSeminario.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ddlSeminario.Name = "ddlSeminario";
            this.ddlSeminario.Size = new System.Drawing.Size(335, 24);
            this.ddlSeminario.TabIndex = 1;
            // 
            // btoOK
            // 
            this.btoOK.Location = new System.Drawing.Point(275, 78);
            this.btoOK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btoOK.Name = "btoOK";
            this.btoOK.Size = new System.Drawing.Size(75, 39);
            this.btoOK.TabIndex = 2;
            this.btoOK.Text = "OK";
            this.btoOK.UseVisualStyleBackColor = true;
            this.btoOK.Click += new System.EventHandler(this.btoOK_Click);
            // 
            // Clt_SelectSeminario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(371, 160);
            this.Controls.Add(this.btoOK);
            this.Controls.Add(this.ddlSeminario);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Clt_SelectSeminario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Seminário";
            this.Load += new System.EventHandler(this.Clt_SelectSeminario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ddlSeminario;
        private System.Windows.Forms.Button btoOK;
    }
}